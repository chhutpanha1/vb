from flask import Flask
from flask.ext.sqlalchemy 	import SQLAlchemy
from app.config 			import *
from datetime 				import datetime, date, timedelta
import time

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = CONNECTION_STRING
db = SQLAlchemy(app)

d 			= 	datetime.now()
now 		= 	'%s-%s-%s'% (d.year, d.month, d.day)
ISODate 	= 	datetime.strptime(str(now), "%Y-%m-%d").date()

try:

	import create_models

	def createDefaultData():
		# Set Default Data
		print "===== Setup Bank Date ====="
		from app.Dates.models 	import MKT_DATES
		currendate 		= raw_input("Enter current bank date: ")
		lastdate 		= raw_input("Enter last bank date: ")
		nextdate 		= raw_input("Enter next bank date: ")
		nextweekend 	= raw_input("Enter next weekend date: ")
		nextmonthend 	= raw_input("Enter next month end: ")
		nextyearend 	= raw_input("Enter next year end: ")

		db.session.query(MKT_DATES.ID).delete()
		# if not db_date:

		db_date = MKT_DATES(
						Status 			=	'AUTH',
						Curr 			=	'0',
						Inputter 		=	'System',
						Createdon 		=	currendate,
						Authorizer		=	'System',
						Authorizeon 	=	currendate,
						ID				=	'SYSTEM',
						SystemDate 		= 	currendate,
						LastSystemDate 	=	lastdate,
						NextSystemDate 	=	nextdate,
						NextWeekend 	=	nextweekend,
						NextMonthEnd 	=	nextmonthend,
						NextYearEnd 	=	nextyearend
				  )

		db.session.add(db_date)
		# db.session.commit()

		from app.Branch.models 	import MKT_BRANCH

		BranchEntry = MKT_BRANCH(
					Status 		=	'AUTH',
					Curr 		=	'0',
					Inputter	=	'System',
					Createdon 	=	ISODate,
					Authorizer	=	'System',
					Authorizeon	=	ISODate,
					ID 			=	'HO',
					Description =	'Head Office'
				 )

		db.session.add(BranchEntry)
		print "Installing branch..."

		from app.Module.models 	import MKT_MODULE
		from app.Module.models 	import MKT_FORM

		ModuleEntry = MKT_MODULE(
					Status 		=	'AUTH',
					Curr 		=	'0',
					Inputter	=	'System',
					Createdon 	=	ISODate,
					Authorizer	=	'System',
					Authorizeon	=	ISODate,
					ID 			=	'COM',
					Description =	'Company'
				 )

		db.session.add(ModuleEntry)
		print "Installing module..."

		FormEntry = MKT_FORM(
					Status 		=	'AUTH',
					Curr 		=	'0',
					Inputter	=	'System',
					Createdon 	=	ISODate,
					Authorizer	=	'System',
					Authorizeon	=	ISODate,
					ID 			=	'COM',
					FormID 		=	'COM01',
					FormDesc 	=	'Company',
					URL 		=	'Company'
				 )

		db.session.add(FormEntry)
		print "Installing form..."

		from app.Role.models 	import MKT_ROLE
		from app.Role.models 	import MKT_ACCESS

		RoleEntry = MKT_ROLE(
					Status 		=	'AUTH',
					Curr 		=	'0',
					Inputter	=	'System',
					Createdon 	=	ISODate,
					Authorizer	=	'System',
					Authorizeon	=	ISODate,
					ID 			=	'99',
					Description	=	'Super Admin'
				 )

		db.session.add(RoleEntry)
		print "Installing role..."

		AccessEntry = MKT_ACCESS(
					Status 		=	'AUTH',
					Curr 		=	'0',
					Inputter	=	'System',
					Createdon 	=	ISODate,
					Authorizer	=	'System',
					Authorizeon	=	ISODate,
					ID 			=	'99',
					AccessID	=	'9901',
					Module 		=	'COM',
					Form 		=	'COM01',
					AccessRight =	'ALL'
				 )

		db.session.add(AccessEntry)
		print "Installing access right..."

		from app.Menu.models 	import MKT_MENU
		from app.Menu.models 	import MKT_MENU_ITEM

		MenuEntry = MKT_MENU(
					Status 		=	'AUTH',
					Curr 		=	'0',
					Inputter	=	'System',
					Createdon 	=	ISODate,
					Authorizer	=	'System',
					Authorizeon	=	ISODate,
					ID 			=	'99',
					Description	=	'Administration'
				 )

		db.session.add(MenuEntry)
		print "Installing menu..."

		MenuItemEntry = MKT_MENU_ITEM(
					Status 		=	'AUTH',
					Curr 		=	'0',
					Inputter	=	'System',
					Createdon 	=	ISODate,
					Authorizer	=	'System',
					Authorizeon	=	ISODate,
					ID 			=	'99',
					ItemID		=	'9901',
					ItemDesc 	=	'Administration',
					Icon 		=	'fa fa-gear',
					Form 		=	'COM:COM01',
					Parents 	=	'',
					Active 		=	'Y'
				 )

		db.session.add(MenuItemEntry)
		print "Installing menu item..."

		from app.User.models 	import MKT_USER

		UserEntry = MKT_USER(
					Status 			=	'AUTH',
					Curr 			=	'0',
					Inputter		=	'System',
					Createdon 		=	ISODate,
					Authorizer		=	'System',
					Authorizeon		=	ISODate,
					ID 				=	'ADMIN.S',
					LogInName 		=	'ADMIN.S',
					DisplayName 	=	'Super Admin',
					StartDate 		=	ISODate,
					ExpiryDate 		=	'2020-07-03',
					PassValidity 	=	'20200703 M0730',
					Attempts 		=	'4',
					Role 			=	'99',
					Branch 			=	'HO',
					Active 			=	'Yes',
					Menu 			=	'99',
					AccessBranch 	=	'HO ALL',
					RestrictBranch 	=	'ALL',
					Notification 	=	'',
					CashAccount 	=	''
			   )


		db.session.add(UserEntry)
		print "Installing user..."

		from app.Setting.models import MKT_SETTING

		SettingEntry = MKT_SETTING(
					Status 			=	'AUTH',
					Curr 			=	'0',
					Inputter		=	'System',
					Createdon 		=	ISODate,
					Authorizer		=	'System',
					Authorizeon		=	ISODate,
					ID 				=	'SYSTEM',
					ATTEMPS 		=	'6',
					TIME_OUT 		=	'15',
					PASSWORD_LENGTH =	'6',
					PASSWORD_CAP 	=	'0',
					PASSWORD_NUM 	=	'0',
					PASSWORD_LOW 	=	'0',
					Notification 	=	'FALSE',
					NotificationInterval 	=	'2'
			   )

		db.session.add(SettingEntry)
		print "Installing setting..."

		db.session.commit()
		print 'Configuration successfully...'

	createDefaultData()

except Exception, e:
	db.session.rollback()
	print e
	raise
