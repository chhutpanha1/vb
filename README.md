## Morakot VB

Summary: Simple and reliable core banking system for Microfinance and Bank.

Home-page: www.morakot.it

Author: Morakot Teams

Author-email: sophorth@morakot.it

### License:

		This program is protected by copyright law and international treaties. 
		Unauthorized reproduction or distribution of this program,
		or any portions of it, may result in severe civil and criminal penalties,
		and will be prosecuted to the maximum extent possible under the law.
		2014-2015 Morakot Technology Co.,Ltd. All Right Reserved. Liceinse and Warranty

### Description:

		Great user experience is our priority. We know what the pains are.
		Built from scratch by a team with more than 10-year experiences in microfinance and banking industry. 
		The simplicity and performance are our core philosophy.
		Stop losing money and your business opportunity with crappy system.

### Dependency Package

1. Flask-0.10.1.tar.gz
2. Flask-SQLAlchemy-2.0.tar.gz
3. WTForms-2.0.1.zip
4. Flask-Login-0.2.11.tar.gz
5. sudo apt-get build-dep python-psycopg2   ``` ( Driver of python with sqlaclchemy ) ```
6. pip install psycopg2 
7. pip install python-dateutil --upgrade  ``` (function calculator year in schedule) ```
8. pip install blinker    ``` ( Flask signal using with notification) ```
9. pip install XlsxWriter    ``` ( Extract report to excel) ```
10. pip install pysftp 		``` (Patch update) ```


## Version of Package

1. Flask==0.10.1
2. Flask-SQLAlchemy==2.0
3. Werkzeug==0.10.4
4. Flask-Login==0.2.11
5. Jinja2==2.7.3
6. psycopg2==2.6
7. python-dateutil==2.4.2
8. SQLAlchemy==0.9.9
9. WTForms==2.0.2
10. blinker==1.3
11. XlsxWriter==0.7.3
12. pysftp==0.2.8