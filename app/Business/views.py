from app.mktcore.imports import *
from .forms import *
from .models import *

registerCRUD(admin, '/Business', 'Business', FRM_BUSINESS, [MKT_BUSINESS])
