from app.mktcore.wtfimports import *
from app.Student.models 	import *
from app.Course.models 		import *
from app.Currency.models 	import *
import datetime

#load Students
def loadStudents():
	return MKT_STUDENT.query
#load Course
def loadCource():
	return MKT_COURSE.query
#load Currency
def loadCurrency():
	return MKT_CURRENCY.query
#validate Amount
def validate_Amount(form, field):
	if float(field.data) == 0.0:
		raise ValidationError("Amount must be greater than 0.")

class FRM_REGISTER(exform):
	d =  datetime.datetime.now()
	now = '%s-%s-%s'% (d.year, d.month, d.day)
	global CurrentDate
	CurrentDate 	= 	datetime.datetime.strptime(str(now), "%Y-%m-%d")
	Student 		=	QuerySelectField(requiredlabel('Student', '*'),
										 query_factory = loadStudents,
										 get_label = lambda a: "%s-%s %s" %(a.ID, a.LastName, a.FirstName),
										 allow_blank = False)
	Course 			=	QuerySelectField('Course',
										 query_factory = loadCource,
										 get_label = u'Description',
										 allow_blank = True)
	
	StartDate		=	DateField(requiredlabel('Start Date', '*'),
								  [validators.Required()])
	RegisterDate	=	DateField(requiredlabel('Register Date', '*'),
								  [validators.Required()] , default = CurrentDate)
	Note 			=	TextAreaField('Note',
									  [validators.Optional()])
	Shift			=	SelectField(requiredlabel("Shift","*"),
						choices=[('1','Morning'),
								 ('2','Afternoon'),
								 ('3','Evening')],
						coerce=str,
						validators=[validators.Required()])
	EndDate			=	DateField(requiredlabel('End Date', '*'),
								  [validators.Required()])

	# @staticmethod
	# def setWidth():
	# 	return [('Note', '200px')]

	# @staticmethod
	# def moneyField():
	# 	return[['Amount', 'Currency']]
	def validate_StartDate(form , field):
		getDate			=		request.form["StartDate"]
		startDate		=		datetime.datetime.strptime(getDate, '%Y-%m-%d')
		if startDate    <       CurrentDate:
			raise ValidationError('Your start date must be more than current date!')

	def validate_RegisterDate(form , field):
		getDate 		= 		request.form["RegisterDate"]
		registerDate	=		datetime.datetime.strptime(getDate, '%Y-%m-%d')
		print registerDate
		print CurrentDate
		if registerDate    !=      CurrentDate:
			raise ValidationError('Your register date must be current date!')
	def validate_EndDate(form , field):
		getDate			=		request.form["StartDate"]
		startDate		=		datetime.datetime.strptime(getDate, '%Y-%m-%d')
		endDate			=		datetime.datetime.strptime(str(field.data), '%Y-%m-%d')
		if endDate    <       startDate:
			raise ValidationError('Your start date must be more than start date!')

