from app.mktcore.mdlimports import *

class MKT_REGISTER(exmodel):
	ID                 =    db.Column(db.String(13), primary_key = True)
	Student            =    db.Column(db.String(13))
	Course             =    db.Column(db.String(13))
	StartDate     	   =    db.Column(db.String(10))
	RegisterDate	   =	db.Column(db.String(10))
	Note               =    db.Column(db.String(100))
	Shift              =    db.Column(db.String(15))
	EndDate            =    db.Column(db.String(10))

class MKT_REGISTER_INAU(exmodel):
	ID                 =    db.Column(db.String(13), primary_key = True)
	Student            =    db.Column(db.String(13))
	Course             =    db.Column(db.String(13))
	StartDate     	   =    db.Column(db.String(10))
	RegisterDate	   =	db.Column(db.String(10))
	Note               =    db.Column(db.String(100))
	Shift              =    db.Column(db.String(15))
	EndDate            =    db.Column(db.String(10))

class MKT_REGISTER_HIST(exmodel):
	ID                 =    db.Column(db.String(18), primary_key = True)
	Student            =    db.Column(db.String(13))
	Course             =    db.Column(db.String(13))
	StartDate     	   =    db.Column(db.String(10))
	RegisterDate	   =	db.Column(db.String(10))
	Note               =    db.Column(db.String(100))
	Shift              =    db.Column(db.String(15))
	EndDate            =    db.Column(db.String(10))