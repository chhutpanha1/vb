from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/Register', 'Register', FRM_REGISTER, [MKT_REGISTER])
