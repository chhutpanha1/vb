from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/Country', 'Country', FRM_COUNTRY, [MKT_COUNTRY])