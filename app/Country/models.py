from app.mktcore.mdlimports import *

class MKT_COUNTRY(exmodel):
	ID = db.Column(db.String(3), primary_key = True)
	Description = db.Column(db.String(35))

class MKT_COUNTRY_INAU(exmodel):
	ID = db.Column(db.String(3), primary_key = True)
	Description = db.Column(db.String(35))

class MKT_COUNTRY_HIST(exmodel):
	ID = db.Column(db.String(8), primary_key = True)
	Description = db.Column(db.String(35))