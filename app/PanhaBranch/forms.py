from app.mktcore.wtfimports import *
from .models 				import *
from app.tools.mktroute		import *

import time
import app.tools.mktaddress as mktaddress

from app.Province.models	import MKT_PROVINCE
from app.District.models	import MKT_DISTRICT
from app.Commune.models		import MKT_COMMUNE
from app.Village.models		import MKT_VILLAGE



def loadProvince():
	return MKT_PROVINCE.query

def loadDistrict(): #define function
	if request.method == 'POST':
		Province = request.form["Province"]
	else:
		if len(getRecord())>0:
			Record = getRecord()
			return MKT_DISTRICT.query.filter_by(ID=Record['District'])
		else:
			return MKT_DISTRICT.query.filter_by(ID='').all()
	return MKT_DISTRICT.query.filter_by(Province=Province) # tablename.query


def loadCommune(): #define function
	if request.method == 'POST':
		District=request.form["District"]
	else:
		if len(getRecord())>0:
			Record = getRecord()
			return MKT_COMMUNE.query.filter_by(ID=Record['Commune'])
		else:
			return MKT_COMMUNE.query.filter_by(ID='').all()
	return MKT_COMMUNE.query.filter_by(District=District) # tablename.query


def loadVillage():
	if request.method == 'POST':
		Commune=request.form["Commune"]
	else:
		if len(getRecord())>0:
			Record = getRecord()
			return MKT_VILLAGE.query.filter_by(ID=Record['Village'])
		else:
			return MKT_VILLAGE.query.filter_by(ID='').all()
	return MKT_VILLAGE.query.filter_by(Commune=Commune) # tablename.query



#function
#After Save record will be look up value from table store
def getRecord():
	try:
		result = MKT_PANHABRANCH.query.filter_by(ID=g.formID).all()
		if not result:
			result = MKT_PANHABRANCH_INAU.query.filter_by(ID=g.formID).all()
			Dict={}
		if result:
			for row in result:
				Dict = row.__dict__
			return Dict
		else:
			return Dict
	except:
		raise

class FRM_PANHABRANCH(exform):

	Description 		= 	TextField(requiredlabel("Description", "*"), [validators.Required()])
	BranchManagerName	=	TextField(requiredlabel("Branch Manager Name", "*"), [validators.Required()])
	ContactNumber		=	TextField(requiredlabel("Contact Number", "*"), [validators.Required()])
	Province 			= 	QuerySelectField(requiredlabel('Province', "*"),
							query_factory=loadProvince,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--Choose Province--',
							validators=[validators.Required()])
	District 			= 	QuerySelectField('District',
							query_factory=loadDistrict,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--Choose District--')
	Commune 			= 	QuerySelectField('Commune',
							query_factory=loadCommune,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--Choose Commune--')
	Village 			= 	QuerySelectField('Village',
							query_factory=loadVillage,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--Choose Village--')

	@staticmethod
	def hotSelectField():
		hotfield=[]
		fielddisplay="District, #Commune, #Village"
		varname="ProvinceID:$('#Province').val()"
		fun=["Province", varname ,fielddisplay, "/Morakot/DistrictID", "click"]
		hotfield.append(fun)
		fielddisplay="Commune, #Village"
		varname="DistrictID:$('#District').val()"
		fun=["District", varname ,fielddisplay, "/Morakot/CommuneID", "click"]
		hotfield.append(fun)
		fielddisplay="Village"
		varname="CommuneID:$('#Commune').val()"
		fun=["Commune", varname ,fielddisplay, "/Morakot/VillageID", "click"]
		hotfield.append(fun)
		return hotfield

	@staticmethod
	def beforeNewID () :
		a = 9
		if a == 2 :
			return True, "Need to check your input value"
		else :
			return False, ""

	@staticmethod
	def moneyField():
		return [["Description","CurrencyField1"]]