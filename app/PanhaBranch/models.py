from app.mktcore.mdlimports import *

#MKT_BRANCH
class MKT_PANHABRANCH(exmodel):
    ID					=	db.Column(db.String(35), primary_key=True)
    Description			=	db.Column(db.String(50))
    BranchManagerName 	=	db.Column(db.String(35))
    ContactNumber 		=	db.Column(db.String(35))
    Province 			=	db.Column(db.String(100))
    District 			=	db.Column(db.String(10))
    Commune 			=	db.Column(db.String(10))
    Village             =   db.Column(db.String(10))
    ReportLocked 		=	db.Column(db.String(1))

class MKT_PANHABRANCH_INAU(exmodel):
    ID					=	db.Column(db.String(35), primary_key=True)
    Description         =   db.Column(db.String(50))
    BranchManagerName   =   db.Column(db.String(35))
    ContactNumber       =   db.Column(db.String(35))
    Province            =   db.Column(db.String(100))
    District            =   db.Column(db.String(10))
    Commune             =   db.Column(db.String(10))
    Village             =   db.Column(db.String(10))
    ReportLocked        =   db.Column(db.String(1))

class MKT_PANHABRANCH_HIST(exmodel):
    ID					=	db.Column(db.String(40), primary_key=True)
    Description         =   db.Column(db.String(50))
    BranchManagerName   =   db.Column(db.String(35))
    ContactNumber       =   db.Column(db.String(35))
    Province            =   db.Column(db.String(100))
    District            =   db.Column(db.String(10))
    Commune             =   db.Column(db.String(10))
    Village             =   db.Column(db.String(10))
    ReportLocked        =   db.Column(db.String(1))