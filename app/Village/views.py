from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/Village', 'Village', FRM_VILLAGE, [MKT_VILLAGE])
