from app.mktcore.mdlimports import *

class MKT_VILLAGE(exmodel):
	ID = db.Column(db.String(9), primary_key = True)
	Description = db.Column(db.String(35))
	Commune = db.Column(db.String(7))

class MKT_VILLAGE_INAU(exmodel):
	ID = db.Column(db.String(9), primary_key = True)
	Description = db.Column(db.String(35))
	Commune = db.Column(db.String(7))

class MKT_VILLAGE_HIST(exmodel):
	ID = db.Column(db.String(14), primary_key = True)
	Description = db.Column(db.String(35))
	Commune = db.Column(db.String(7))