from app.mktcore.wtfimports import *
from app.Commune.models import *

def loadCommune():
	return MKT_COMMUNE.query

class FRM_VILLAGE(exform):
	Description = TextField(requiredlabel('Village', '*'), [validators.Required()])
	Commune = QuerySelectField('Commune', query_factory = loadCommune, get_label = u'Description', allow_blank = False, blank_text = u'None')