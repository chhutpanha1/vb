from app.mktcore.wtfimports 		import *
from .models 						import *

class FRM_DATES(exform):

	SystemDate 		= 	DateField(requiredlabel("Current System Date", "*"), [validators.Required()])
	LastSystemDate 	= 	TextField(requiredlabel("Last System Date", "*"), [validators.Required()])
	NextSystemDate 	= 	DateField(requiredlabel("Next System Date", "*"), [validators.Required()])
	NextWeekend 	= 	DateField(requiredlabel("Next Weekend Date", "*"), [validators.Required()])
	NextMonthEnd 	= 	DateField(requiredlabel("Next Month End", "*"), [validators.Required()])
	NextYearEnd 	= 	DateField(requiredlabel("Next Year End", "*"), [validators.Required()])
	JulianDate 		=	TextField(requiredlabel("Julian Date", "*"), [validators.Required()])

	@staticmethod
	def setDisable():
		form_control = [("LastSystemDate"), ("JulianDate")]

		return form_control

	@staticmethod
	def setWidth():
		form_control = [("SystemDate", len3), ("LastSystemDate", len3), ("NextWeekend", len3),
						("NextMonthEnd", len3), ("NextYearEnd", len3), ("JulianDate", len1),
						("NextSystemDate", len3)]

		return form_control