from app.mktcore.mdlimports import *

class MKT_DATES(exmodel):
	ID 				= 	db.Column(db.String(34), primary_key=True)
	SystemDate 		= 	db.Column(db.String(30))
	LastSystemDate 	= 	db.Column(db.String(30))
	NextSystemDate 	= 	db.Column(db.String(30))
	NextWeekend 	=	db.Column(db.String(10))
	NextMonthEnd 	=	db.Column(db.String(10))
	NextYearEnd 	=	db.Column(db.String(10))
	JulianDate 		=	db.Column(db.String(5))

class MKT_DATES_INAU(exmodel):
	ID 				= 	db.Column(db.String(34), primary_key=True)
	SystemDate 		= 	db.Column(db.String(30))
	LastSystemDate 	= 	db.Column(db.String(30))
	NextSystemDate 	= 	db.Column(db.String(30))
	NextWeekend 	=	db.Column(db.String(10))
	NextMonthEnd 	=	db.Column(db.String(10))
	NextYearEnd 	=	db.Column(db.String(10))
	JulianDate 		=	db.Column(db.String(5))

class MKT_DATES_HIST(exmodel):
	ID 				= 	db.Column(db.String(39), primary_key=True)
	SystemDate 		= 	db.Column(db.String(30))
	LastSystemDate 	= 	db.Column(db.String(30))
	NextSystemDate 	= 	db.Column(db.String(30))
	NextWeekend 	=	db.Column(db.String(10))
	NextMonthEnd 	=	db.Column(db.String(10))
	NextYearEnd 	=	db.Column(db.String(10))
	JulianDate 		=	db.Column(db.String(5))