from app.mktcore.wtfimports import *
from app.Position.models import *
from app.Officer.models import *
from app.Branch.models import *


# load position
def loadPosition():
	return MKT_POSITION.query

# load officer
def loadOfficer():
	return MKT_OFFICER.query

# load branch
def loadBranch():
	return MKT_BRANCH.query

class FRM_OFFICER(exform):
	FirstName = TextField(requiredlabel('First Name', '*'), [validators.Required()])
	LastName = TextField(requiredlabel('Last Name', '*'), [validators.Required()])
	Gender = SelectField(requiredlabel('Gender', '*'), choices = [('F', 'Female'),('M', 'Male'),('O', 'Other')], coerce = str, default = 'M')
	StartDate = DateField("Start Date", [validators.Required()])
	Position = QuerySelectField("Position", query_factory = loadPosition, get_label = u'Description', allow_blank = False, blank_text = u'None')
	ReportTo = QuerySelectField("Officer", query_factory = loadOfficer, get_label = lambda Obj: "%s-%s %s" %(Obj.ID, Obj.LastName, Obj.FirstName) , allow_blank = True, blank_text = u'None')
	Branch = QuerySelectField("Branch", query_factory = loadBranch, get_label = u'Description', allow_blank = False, blank_text = u'None')
	Active = SelectField("Active", choices = [('Y', 'Yes'), ('N', 'No')], coerce = str, default = 'Y')
