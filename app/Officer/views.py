from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/Officer', 'Officer', FRM_OFFICER, [MKT_OFFICER])