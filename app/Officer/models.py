from app.mktcore.mdlimports import *

class MKT_OFFICER(exmodel):
	ID = db.Column(db.String(10), primary_key = True)
	FirstName = db.Column(db.String(35))
	LastName = db.Column(db.String(35))
	Gender = db.Column(db.String(6))
	StartDate = db.Column(db.DateTime)
	Position = db.Column(db.String(3))
	ReportTo = db.Column(db.String(10))
	Branch = db.Column(db.String(3))
	Active = db.Column(db.String(3))


class MKT_OFFICER_INAU(exmodel):
	ID = db.Column(db.String(10), primary_key = True)
	FirstName = db.Column(db.String(35))
	LastName = db.Column(db.String(35))
	Gender = db.Column(db.String(6))
	StartDate = db.Column(db.DateTime)
	Position = db.Column(db.String(3))
	ReportTo = db.Column(db.String(10))
	Branch = db.Column(db.String(3))
	Active = db.Column(db.String(3))

class MKT_OFFICER_HIST(exmodel):
	ID = db.Column(db.String(15), primary_key = True)
	FirstName = db.Column(db.String(35))
	LastName = db.Column(db.String(35))
	Gender = db.Column(db.String(6))
	StartDate = db.Column(db.DateTime)
	Position = db.Column(db.String(3))
	ReportTo = db.Column(db.String(10))
	Branch = db.Column(db.String(3))
	Active = db.Column(db.String(3))