FileName = "language_EN"
# Language = __import__("app.Language.%s" %FileName)
from app.Language.language_EN import Language
import os
import glob

Language = Language

def getLangList():
	try:
		
		LangList 	=	[]

		path = '/var/www/vb/app/Language'

		for filename in os.listdir(path):

			StrFileName = filename.split('.')
			StrFileName = StrFileName[0]

			if 'language' in StrFileName:

				StrLanguage = StrFileName.split("_")

				if len(StrLanguage) > 1:

					LangList.append(StrLanguage[1])
		
		return LangList

	except Exception, e:
		raise

def getLanguage(Key):
	try:

		from app.Language.language_EN import Language

		if Key not in Language:
			Key = 'Undifined'

		return Language[str(Key)]

	except Exception, e:
		raise