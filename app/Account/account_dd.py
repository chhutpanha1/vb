from wtforms 							import DecimalField, IntegerField
from app.mktcore.wtfimports 			import *
from .models 							import *
from .. 								import app, db
from sqlalchemy 						import *
import time
from datetime 							import datetime, date, timedelta

from app.CategoryType.models 			import MKT_CATEG_TYPE
from app.AccProduct.models 				import MKT_ACC_PRODUCT
from app.Currency.models 				import MKT_CURRENCY
from app.JAccount.models 				import MKT_JACCOUNT
from app.InterestRate.models 			import MKT_INTEREST_RATE
from app.ChargeRate.models 				import MKT_CHARGE_RATE
from app.AccRuleDetail.models 			import MKT_ACC_RULE_DE
from app.User.models 					import MKT_USER
from app.Tax.models 					import MKT_TAX
from app.tools.mktcustomer 				import *
from app.tools.catetool 				import *
from decimal 							import *
import string
from app.tools.mktmoney 				import *
from app.tools.user 					import *
from app.tools.mktofficer 				import *
import app.tools.mktaccounting 			as mktaccounting
import app.tools.mktdate 				as mktdate
import app.tools.mktaccount 			as mktaccount
import app.tools.mktteller 				as mktteller
from app.tools.mktroute 				import *

def loadCategoryType():
	return MKT_CATEG_TYPE.query

def loadParent():
	return MKT_CATEGORY.query

def loadAccProduct():
	return MKT_ACC_PRODUCT.query.filter_by(ProductType = 'E').all()

def loadCurrency():
	return MKT_CURRENCY.query.order_by(asc(MKT_CURRENCY.ID)).all()

def loadJAccount():
	return MKT_JACCOUNT.query

def getAccountProductSetup():
	return MKT_ACC_PRODUCT.query.get('101')

def loadAccCategory():
	try:

		AccRecord 	= 	getAccountProductSetup()
		Category 	=	""
		if AccRecord:
			Category 	=	AccRecord.CategoryList

		return Category

	except:
		raise

def loadInterestRate():
	try:

		AccRecord 		= 	getAccountProductSetup()
		Rate 			=	"0"

		if AccRecord:
			# InterestKey = 	AccRecord.InterestKey
			InterestKey	= 	AccRecord.InterestKey
			RateID		= 	str(InterestKey) + 'KHR'
			AccRate 	= 	MKT_INTEREST_RATE.query.get(RateID)
			if AccRate:
				Rate 	= 	AccRate.Rate
				Rate 	=	Rate.split()
				if len(Rate) > 0:
					Rate 	= 	Rate[0]
				else:
					Rate 	=	'0'

		return str(Rate)

	except:
		raise

def loadInterestRateType():
	try:

		AccRecord 	= 	getAccountProductSetup()
		FixedRate 	=	"F"
		if AccRecord:
			Charge 	= 	AccRecord.Charge
			ID 		=	str(Charge) + "KHR"
			AccCha 	=  	MKT_CHARGE_RATE.query.get(ID)
			if AccCha:
				FixedRate = str(AccCha.Rate)

		return FixedRate

	except:
		raise

def loadAccCharge():
	try:

		AccRecord 	= 	getAccountProductSetup()
		ChargeRate 	=	"0"
		if AccRecord:
			Charge 	= 	AccRecord.Charge
			ID 		=	str(Charge) + "KHR"
			AccCha 	=  	MKT_CHARGE_RATE.query.get(ID)
			if AccCha:
				ChargeRate = str(AccCha.Value)

		return ChargeRate

	except:
		raise

def loadTax():
	AccRecord 	= 	getAccountProductSetup()
	Tax 		=	""
	if AccRecord:
		Tax 	= 	AccRecord.Tax

	return Tax

def loadCurrentDate():
	return mktdate.getBankDate()

class FRM_ACCOUNT_DD(exform):

	CustomerList 	= 	HiddenField(requiredlabel(getLanguage('Customer'), "*"), [validators.Required()])
	AccName 		= 	TextField(requiredlabel(getLanguage("Account Name"), "*"), [validators.Required()])
	Currency 		= 	QuerySelectField(requiredlabel(getLanguage("Currency"), "*"),
							query_factory=loadCurrency,
							get_label=u'ID',
							allow_blank=False,
							blank_text=u'None',
							validators=[validators.Required()]
						)

	JAccount 		=	TextField(getLanguage("Join Account"))
	JoinID 			=	TextField(getLanguage("Join ID"))
	AccProduct 		=	TextField(getLanguage("Account Product"), default='101')
	AccCategory 	=	TextField(getLanguage("Category"), default=loadAccCategory)
	InterestRate 	=	TextField(getLanguage("Interest Rate"), default=loadInterestRate)
	FixedRate 		=	TextField(getLanguage("Fixed/Rate"), default=loadInterestRateType)
	Charge 			=	TextField(getLanguage("Charge"), default=loadAccCharge)
	Tax 			=	TextField(getLanguage("Tax"), default=loadTax)

	OpenDate 		= 	DateField(requiredlabel(getLanguage("Opening Date"), "*"), [validators.Required()], default=loadCurrentDate)

	AccStatus 		= 	SelectField(getLanguage('Status'),
							choices=[('O', 'Open'),
									 ('C', 'Close')],
							coerce=str
						)

	ClosingDate 	= 	DateField(getLanguage("Closing Date"), [validators.Optional()])

	OfficerID 		=	TextField(getLanguage("Officer"), default="")

	AccrInterest 	= 	DecimalField(getLanguage("Accrual Interest"), [validators.Optional()], default=0)
	AccrCurMonth 	= 	DecimalField(getLanguage("Accr Int for Booking"), [validators.Optional()], default=0)
	AccrCurCapital 	= 	DecimalField(getLanguage("Accr Int for Capitalization"), [validators.Optional()], default=0)
	AccrIntBooked 	=	DecimalField(getLanguage("Accr Int was Booked"), [validators.Optional()], default=0)

	Balance 		= 	DecimalField(getLanguage("Balance"), [validators.Optional()], default=0)
	AvailableBal 	= 	DecimalField(getLanguage("Available Balance"), [validators.Optional()], default=0)
	UserID 			= 	HiddenField(getLanguage("User"))
	
	Bal 			= 	""
	TranDate 		= 	""

	def validate_AccName(form, field):
		global Bal, TranDate
		Bal = form.Balance.data
		TranDate = form.OpenDate.data

	def validate_Charge(form, field):
		Value = form.Charge.data
		if str(Value).find("%") != -1:
			Value = Value.split("%")[0]

		try:
			Value = Decimal(Value)
		except Exception as exe:
			raise ValidationError("The chare you have enter is wrong number value")

		form.Charge.data = Value

	def validate_AccStatus(form, field):
		try:

			AccountBalance = request.form['Balance']
			AccountStatus  = request.form['AccStatus']
			AccountCurrency= request.form['Currency']

			if AccountStatus.upper() == "C":
				# AccountBalance = float(AccountBalance) if AccountBalance else float(0)
				# if AccountBalance != float(0):
				# 	raise ValidationError("Account balance must be %s. Current balance is %s." %(mktmoney.toMoney(float(0), mktmoney.getCurrencyObj(AccountCurrency), 2), mktmoney.toMoney(float(AccountBalance), mktmoney.getCurrencyObj(AccountCurrency), 2)))
				Check = mktteller.isUserVaultAccount()
				if not Check[0]:
					raise ValidationError(Check[1])

		except:
			raise


	def validate_ClosingDate(form, field):

		AccountClosingDate 	= request.form['ClosingDate']
		AccountStatus  		= request.form['AccStatus']

		if AccountStatus.upper() == "C":
			try:
				datetime.strptime(str(AccountClosingDate),'%Y-%m-%d')
			except:
				raise ValidationError("This field must be in date format.")

	@staticmethod
	def moneyField():
		return [("Balance", "Currency"), ("Charge", "Currency"), ("AvailableBal", "Currency"), ("AccrInterest", "Currency"), ("AccrCurMonth", "Currency"), ("AccrCurCapital", "Currency"), ("AccrIntBooked", "Currency")]

	@staticmethod
	def formatMoney():
		return ["Balance", "AvailableBal", "AccrInterest", "AccrCurCapital", "AccrCurMonth", "AccrIntBooked"], "Currency"

	@staticmethod
	def setWidth():

		Fields = [('Currency', len1),
				  ('JAccount', len1),
				  ('JoinID', len2),
				  ('AccCategory', len2),
				  ('FixedRate', len1),
				  ('OpenDate', len3),
				  ('AccStatus', len1),
				  ('ClosingDate', len3),
				  ('Blocked', len1),
				  ('Normant', len1)]

		return Fields

	@staticmethod
	def setVisible():		
		control_list = ['JAccount', 'JoinID', 'AccProduct', 'AccCategory', 'Tax',
						'InterestRate', 'FixedRate', 'Charge', 'AccrInterest',
						'AccrCurMonth', 'AccrCurCapital', 'Balance', 'AvailableBal', 'UserID',
						'OfficerID', 'AccrIntBooked']
		return control_list

	@staticmethod
	def listField():

		Fields = ["ID", "CustomerList", "AccName", "Currency", "AccCategory",
				  "OpenDate", "Balance", "AvailableBal"]
		# return Fields
		return Fields, ["ID*LK*DD"]

	@staticmethod
	def hotField():
		hotfield = []

		fielddisplay 	= "$('#InterestRate').val(data.InterestKey); $('#Charge').val(data.ChargeRate); $('#FixedRate').val(data.FixedRate)"
		varname 		= "AccProductID:$('#AccProduct').val(), Currency:$('#Currency').val()"
		fun 			= ["Currency", varname, fielddisplay, "/Morakot/AccProductID", "change"]
		hotfield.append(fun)

		fielddisplay 	= 	"$('#ClosingDate').val(data.ClosingDate)"
		varname			=	"AccStatus:$('#AccStatus').val()"
		fun 			=	["AccStatus", varname, fielddisplay, "/Morakot/AccountClosingDate", "change"]
		hotfield.append(fun)

		fielddisplay 	= 	"$('#AccName').val(data.CustomerName)"
		varname			=	"CustomerList:$('#CustomerList').val()"
		fun 			=	["CustomerList", varname, fielddisplay, "/Morakot/CustomerNameBySelected", "change"]
		hotfield.append(fun)

		return hotfield

	@staticmethod
	def hotSelectField():
		hotfield = []

		fielddisplay = "$('input[name=JoinID]').attr('readonly', data.Bool)"
		varname = "JAccountID:$('#JAccount').val()"
		fun = ["JAccount", varname, fielddisplay, "/Morakot/JoinID", "change"]
		hotfield.append(fun)

		return hotfield

	@staticmethod
	def setDisable():
		return [('AccCategory'), ('FixedRate'), ('AccrInterest'), ('Balance'),
				('NextAccrDate'), ('Branch'), ('UserID'), ("AvailableBal"), ("AccrCurMonth"), ("AccrCurCapital")]

	@staticmethod
	def redirectAfterInsert():
		ID 				= 	request.form['ID']
		NumOfAuth 		= 	g.getNAuthorize
		BankDate 		=	mktdate.getBankDate()

		if NumOfAuth == 0:
			Acc = MKT_ACCOUNT.query.get(ID)

			if Acc:
				AccStatus = Acc.AccStatus
				if AccStatus == 'C':
					# Check if account closing action
					UserID 	=	Acc.Inputter
					Check 	= 	mktaccount.setCloseAccount(ID, UserID, "AUTH")

		else:
			Acc = MKT_ACCOUNT_INAU.query.get(ID)

		if not Acc:
			flash(msg_error + "Account not found.")
			return False

		Acc.NextAccrDate 	=	str(BankDate)
		db.session.add(Acc)
		db.session.commit()

		return None

	@staticmethod
	def reverseRec():
		return False,"Record not allow to reverse."

	@staticmethod
	def beforeAuthorize():
		ID 		= 	g.formID
		AccObj 	=	MKT_ACCOUNT_INAU.query.get(ID)

		if not AccObj:
			flash(msg_error + "Account %s not found." %ID)
			return False

		AccStatus = AccObj.AccStatus
		if AccStatus == 'C':
			# Check if account closing action
			UserID 	=	AccObj.Inputter
			Check 	= 	mktaccount.setCloseAccount(ID, UserID, "INAU")

			if not Check[0]:
				flash(msg_error + "%s" %Check[1])
				return False

		return True

	@staticmethod
	def IsAcceptOverride():

		Override 	= False
		Msg 		= ""
		ID 			= request.form['ID']
		AccStatus 	= request.form['AccStatus']
		
		if AccStatus == 'C':
			Override 	= True
			Msg 		= "You will be close this account %s." %ID

		Msg += " Do you want to procceed?"

		return Override, Msg