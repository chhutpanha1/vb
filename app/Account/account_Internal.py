from wtforms 							import DecimalField, IntegerField
from app.mktcore.wtfimports import *
from .models import *
import time

import app.tools.mktdate 				as mktdate

from app.tools.mktofficer 				import *
from app.tools.user 					import *
from app.CategoryType.models 			import MKT_CATEG_TYPE
from app.AccProduct.models 				import MKT_ACC_PRODUCT
from app.Currency.models 				import MKT_CURRENCY

# from app.JAccount.models 				import MKT_JACCOUNT
# from app.InterestRate.models 			import MKT_INTEREST_RATE
# from app.ChargeRate.models 				import MKT_CHARGE_RATE
# from app.AccBalance.models 				import MKT_ACC_BALANCE
# from app.AccRuleDetail.models 			import MKT_ACC_RULE_DE


def loadCategoryType():
	return MKT_CATEG_TYPE.query

def loadParent():
	return MKT_CATEGORY.query

def loadAccProduct():
	result = ''
	AccProduct = MKT_ACC_PRODUCT.query.filter_by(ProductType='I')
	if AccProduct:
		result=AccProduct
	return result

def loadCurrency():
	return MKT_CURRENCY.query.order_by(asc(MKT_CURRENCY.ID)).all()

def loadBranch():
	return getUser().Branch

def loadCurrentDate():
	return mktdate.getBankDate()

class FRM_ACCOUNT_INTERNAL(exform):

	AccName = TextField(requiredlabel("Account Name", "*"), [validators.Required()])
	Currency = QuerySelectField(requiredlabel("Currency", "*"),
					query_factory=loadCurrency,
					get_label=u'ID',
					allow_blank=False,
					blank_text=u'None',
					validators=[validators.Required()]
				)
	JAccount = SelectField('Joint Account',
					choices=[('N', 'No'),
							 ('Y', 'Yes')],
					coerce=str)

	AccProduct = QuerySelectField(requiredlabel('Account Product', '*'),
									query_factory=loadAccProduct,
									get_label=u'Description',
									allow_blank=True,
									blank_text=u'--None--',
									validators=[validators.Required()]
								)

	AccCategory = TextField(requiredlabel("Category", "*"), [validators.Required()])

	InterestRate = DecimalField("Interest Rate", [validators.Optional()])
	Charge = DecimalField("Charge", [validators.Optional()])
	OpenDate = DateField(requiredlabel("Opening Date", "*"), [validators.Required()], default=loadCurrentDate)
	AccrInterest = DecimalField("Accrual Interest", [validators.Optional()], default=0)
	Balance = DecimalField("Balance", [validators.Optional()], default=0)

	AccStatus = SelectField('Status',
					choices=[('O', 'Open'),
							 ('C', 'Close')],
					coerce=str)

	ClosingDate = DateField("Closing Date", [validators.Optional()])
	Blocked = SelectField('Blocked',
					choices=[('N', 'No'),
							 ('Y', 'Yes')],
					coerce=str)
	OfficerID = HiddenField("Officer")

	@staticmethod
	def setVisible():
		control_list=['Blocked','JAccount','InterestRate','Charge','AccrInterest','ClosingDate']
		return control_list

	@staticmethod
	def listField():

		Fields = ["ID", "CustomerList", "AccName", "Currency", "JAccount", "AccProduct", "AccCategory",
				  "OpenDate", "Balance", "AccrInterest", "AccStatus"]
		return Fields

	@staticmethod
	def formatMoney():
		return ["Balance","AccrInterest"],"Currency"
		# Balance and AccrInterest are the fields to be formatted based on Currency field

	@staticmethod
	def setWidth():

		Fields = [('Currency', len1),
				  ('JAccount', len1),
				  ('JoinID', len2),
				  ('AccCategory', len2),
				  ('FixedRate', len1),
				  ('OpenDate', len3),
				  ('AccStatus', len1),
				  ('ClosingDate', len3),
				  ('Blocked', len1),
				  ('Normant', len1)]

		return Fields

	@staticmethod
	def hotField():
		hotfield = []

		fielddisplay = "$('#InterestRate').val(data.InterestKey); $('#Charge').val(data.ChargeRate); $('#FixedRate').val(data.FixedRate)"
		varname = "AccProductID:$('#AccProduct').val(), Currency:$('#Currency').val()"
		fun = ["Currency", varname, fielddisplay, "/Morakot/AccProductID", "change"]
		hotfield.append(fun)

		fielddisplay = "$('#AccCategory').val(data.AccProCategory); $('#InterestRate').val(data.InterestKey); $('#Charge').val(data.ChargeRate); $('#FixedRate').val(data.FixedRate)"
		varname = "AccProductID:$('#AccProduct').val(), Currency:$('#Currency').val()"
		fun = ["AccProduct", varname, fielddisplay, "/Morakot/AccProductID", "change"]
		hotfield.append(fun)

		return hotfield



	@staticmethod
	def setDisable():
		return [('AccCategory'), ('FixedRate'), ('AccrInterest'), ('Balance'),('Branch')]