from wtforms 							import DecimalField, IntegerField
from app.mktcore.wtfimports 			import *
from .models 							import *
from .. 								import app, db
from sqlalchemy 						import *
import time
from datetime 							import datetime, date, timedelta

from app.CategoryType.models 			import MKT_CATEG_TYPE
from app.AccProduct.models 				import MKT_ACC_PRODUCT
from app.Currency.models 				import MKT_CURRENCY
from app.JAccount.models 				import MKT_JACCOUNT
from app.InterestRate.models 			import MKT_INTEREST_RATE
from app.ChargeRate.models 				import MKT_CHARGE_RATE
from app.AccRuleDetail.models 			import MKT_ACC_RULE_DE
from app.User.models 					import MKT_USER
from app.Tax.models 					import MKT_TAX
from app.Customer.models 				import MKT_CUSTOMER
from app.tools.mktcustomer 				import *
from app.tools.catetool 				import *
from decimal 							import *
import string
from app.tools.mktmoney 				import *
from app.tools.user 					import *
from app.tools.mktofficer 				import *
import app.tools.mktaccounting 			as mktaccounting
import app.tools.mktdate 				as mktdate
import app.tools.mktaccount 			as mktaccount
import app.tools.mktteller 				as mktteller
from app.tools.mktroute 				import *

def loadCategoryType():
	return MKT_CATEG_TYPE.query

def loadParent():
	return MKT_CATEGORY.query

def loadAccProduct():
	return MKT_ACC_PRODUCT.query.filter_by(ProductType = 'E').all()

def loadCurrency():
	return MKT_CURRENCY.query.order_by(asc(MKT_CURRENCY.ID)).all()

def loadJAccount():
	return MKT_JACCOUNT.query

def getAccountProductSetup():
	return MKT_ACC_PRODUCT.query.get('102')

def loadAccCategory():
	try:

		AccRecord 	= 	getAccountProductSetup()
		Category 	=	""
		if AccRecord:
			Category 	=	AccRecord.CategoryList

		return Category

	except:
		raise

def loadInterestRate():
	try:

		AccRecord 			= 	getAccountProductSetup()
		Rate 				=	"0"
		if AccRecord:
			ProID 			=	AccRecord.ID
			InterestKey		= 	AccRecord.InterestKey
			InterestRateID	= 	str(InterestKey) + 'KHR'
			Charge 			= 	AccRecord.Charge
			AccRate 		= 	MKT_INTEREST_RATE.query.get(InterestRateID)
			if AccRate:
				Rate 	= 	AccRate.Rate
				Rate 	=	Rate.split()
				if len(Rate) > 0:
					Rate 	= 	Rate[0]
				else:
					Rate 	=	'0'

		return str(Rate)

	except:
		raise

def loadInterestRateType():
	try:

		AccRecord 	= 	getAccountProductSetup()
		FixedRate 	=	"F"
		if AccRecord:
			Charge 	= 	AccRecord.Charge
			ID 		=	str(Charge) + "KHR"
			AccCha 	=  	MKT_CHARGE_RATE.query.get(ID)
			if AccCha:
				FixedRate = str(AccCha.Rate)

		return FixedRate

	except:
		raise

def loadAccCharge():
	try:

		AccRecord 	= 	getAccountProductSetup()
		ChargeRate 	=	"0"
		if AccRecord:
			Charge 	= 	AccRecord.Charge
			ID 		=	str(Charge) + "KHR"
			AccCha 	=  	MKT_CHARGE_RATE.query.get(ID)
			if AccCha:
				ChargeRate = str(AccCha.Value)

		return ChargeRate

	except:
		raise

def loadTax():
	
	AccRecord 	= 	getAccountProductSetup()
	Tax 		=	""
	if AccRecord:
		Tax 	= 	AccRecord.Tax

	return Tax

def loadCurrentDate():
	return mktdate.getBankDate()

class FRM_ACCOUNT_SV(exform):

	CustomerList 	= 	HiddenField(requiredlabel(getLanguage("Customer"), "*"), [validators.Required()])
	AccName 		= 	TextField(requiredlabel(getLanguage("Account Name"), "*"), [validators.Required()])
	Currency 		= 	QuerySelectField(requiredlabel(getLanguage("Currency"), "*"),
							query_factory=loadCurrency,
							get_label=u'ID',
							allow_blank=False,
							blank_text=u'None',
							validators=[validators.Required()]
						)

	AccProduct 		=	TextField(getLanguage("Account Product"), default='102')
	AccCategory 	=	TextField(getLanguage("Category"), default=loadAccCategory)
	InterestRate 	=	TextField(getLanguage("Interest Rate"), default=loadInterestRate)
	
	JAccount 		= 	SelectField(getLanguage('Joint Account'),
								choices=[('N', 'No'),
										 ('Y', 'Yes')],
								coerce=str
						)

	JoinID 			= 	QuerySelectField(getLanguage('Join ID'),
	                        query_factory=loadJAccount,
	                        get_label=u'ID',
	                        allow_blank=True,
	                        blank_text=u'--None--'
                        )

	FixedRate 		=	TextField(getLanguage("Fixed/Rate"), default=loadInterestRateType)
	Charge 			=	TextField(getLanguage("Charge"), default=loadAccCharge)
	Tax 			=	TextField(getLanguage("Tax"), default=loadTax)

	OpenDate 		= 	DateField(requiredlabel(getLanguage("Opening Date"), "*"), [validators.Required()], default=loadCurrentDate)

	AccStatus 		= 	SelectField(getLanguage('Status'),
							choices=[('O', 'Open'),
									 ('C', 'Close')],
							coerce=str
						)

	ClosingDate 	= 	DateField(getLanguage("Closing Date"), [validators.Optional()])
	# Blocked 		= 	SelectField('Blocked',
	# 						choices=[('N', 'No'),
	# 								 ('D', 'Block Debit'),
	# 								 ('C', 'Block Credit'),
	# 								 ('A', 'Block All')],
	# 						coerce=str
	# 					)
	Blocked 		= 	SelectField(getLanguage('Blocked'),
							choices=[('N', 'No'),
									 ('Y', 'Yes')],
							coerce=str,
							default='N'
						)

	BlockReason 	= 	TextAreaField(getLanguage("Block Reason"))
	
	Dormant 		=	TextField(getLanguage("Dormant"), default='N')

	OfficerID 		=	""

	AccrInterest 	= 	DecimalField(getLanguage("Accrual Interest"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Other Info"))
	AccrCurMonth 	= 	DecimalField(getLanguage("Accr Int for Booking"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Other Info"))
	AccrIntBooked 	= 	DecimalField(getLanguage("Accr Int was Booked"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Other Info"))
	AccrCurCapital 	= 	DecimalField(getLanguage("Accr Int for Capitalization"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Other Info"))

	Balance 		= 	DecimalField(getLanguage("Balance"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Other Info"))
	AvailableBal 	= 	DecimalField(getLanguage("Available Balance"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Other Info"))
	UserID 			= 	HiddenField(getLanguage("User"), description="1-%s" %getLanguage("Other Info"))
	
	Bal 			= 	""
	TranDate 		= 	""

	def validate_AccName(form, field):
		try:
			global Bal, TranDate
			Bal 		= form.Balance.data
			TranDate 	= form.OpenDate.data
		except:
			raise

	def validate_JoinID(form, field):
		try:

			JAccount 	=	request.form['JAccount']
			JoinID 		=	request.form['JoinID']

			if JAccount.upper() == 'Y':
				if JoinID == '' or JoinID == '__None':
					raise ValidationError("This field is required.")

		except:
			raise

	def validate_Charge(form, field):
		try:
			Value = form.Charge.data
			if str(Value).find("%") != -1:
				Value = Value.split("%")[0]

			try:
				Value = Decimal(Value)
			except Exception as exe:
				raise ValidationError("The chare you have enter is wrong number value")

			form.Charge.data = Value
		except:
			raise

	def validate_AccStatus(form, field):
		try:

			AccountBalance = request.form['Balance']
			AccountStatus  = request.form['AccStatus']
			AccountCurrency= request.form['Currency']

			if AccountStatus.upper() == "C":
				# AccountBalance = float(AccountBalance) if AccountBalance else float(0)
				# if AccountBalance != float(0):
				# 	raise ValidationError("Account balance must be %s. Current balance is %s." %(mktmoney.toMoney(float(0), mktmoney.getCurrencyObj(AccountCurrency), 2), mktmoney.toMoney(float(AccountBalance), mktmoney.getCurrencyObj(AccountCurrency), 2)))
				Check = mktteller.isUserVaultAccount()
				if not Check[0]:
					raise ValidationError(Check[1])

		except:
			raise

	def validate_ClosingDate(form, field):

		AccountClosingDate 	= request.form['ClosingDate']
		AccountStatus  		= request.form['AccStatus']

		if AccountStatus.upper() == "C":
			try:
				datetime.strptime(str(AccountClosingDate),'%Y-%m-%d')
			except:
				raise ValidationError("This field must be in date format.")

	def validate_InterestRate(form, field):
		AccPro = request.form['AccProduct']
		CurrencyKey = request.form['Currency']
		InputRate = form.InterestRate.data

		try:
			InputRate = float(InputRate)
		except:
			raise ValidationError("This field must be in numeric format.")

		Msg = mktaccount.checkInterestRate("AC", AccPro, InputRate, CurrencyKey)

		if Msg:
			raise ValidationError(Msg)

	@staticmethod
	def moneyField():
		return [("Balance", "Currency"), ("Charge", "Currency"), ("AvailableBal", "Currency"), ("AccrInterest", "Currency"), ("AccrCurMonth", "Currency"), ("AccrCurCapital", "Currency"), ("AccrIntBooked", "Currency")]

	@staticmethod
	def formatMoney():
		return ["Balance", "AvailableBal", "AccrInterest", "AccrCurCapital", "AccrCurMonth", "AccrIntBooked"], "Currency"

	@staticmethod
	def setWidth():

		Fields = [('Currency', len1),
				  ('OpenDate', len3),
				  ('AccStatus', len1),
				  ('ClosingDate', len3),
				  ('Blocked', len1),
				  ('Dormant', len1)]

		return Fields

	@staticmethod
	def reverseRec():
		return False,"Record not allow to reverse."

	@staticmethod
	def setVisible():		
		control_list = ['AccProduct', 'AccCategory',
						'FixedRate', 'Charge', 'Tax']
		return control_list

	@staticmethod
	def listField():

		Fields = ["ID", "CustomerList", "AccName", "Currency", "AccCategory",
				  "InterestRate", "OpenDate", "Balance", "AvailableBal"]
		# return Fields
		return Fields, ["ID*LK*SV"]

	@staticmethod
	def hotField():
		hotfield = []

		fielddisplay 	= "$('#InterestRate').val(data.InterestKey); $('#Charge').val(data.ChargeRate); $('#FixedRate').val(data.FixedRate)"
		varname 		= "AccProductID:$('#AccProduct').val(), Currency:$('#Currency').val()"
		fun 			= ["Currency", varname, fielddisplay, "/Morakot/AccProductID", "change"]
		hotfield.append(fun)

		fielddisplay 	= 	"$('#ClosingDate').val(data.ClosingDate)"
		varname			=	"AccStatus:$('#AccStatus').val()"
		fun 			=	["AccStatus", varname, fielddisplay, "/Morakot/AccountClosingDate", "change"]
		hotfield.append(fun)

		fielddisplay 	= 	"$('#AccName').val(data.CustomerName)"
		varname			=	"CustomerList:$('#CustomerList').val()"
		fun 			=	["CustomerList", varname, fielddisplay, "/Morakot/CustomerNameBySelected", "change"]
		hotfield.append(fun)

		return hotfield

	@staticmethod
	def hotSelectField():
		hotfield = []

		fielddisplay 	= "$('input[name=JoinID]').attr('readonly', data.Bool)"
		varname 		= "JAccountID:$('#JAccount').val()"
		fun 			= ["JAccount", varname, fielddisplay, "/Morakot/JoinID", "change"]
		hotfield.append(fun)

		return hotfield

	@staticmethod
	def setDisable():
		return [('Dormant'), ('AccrInterest'), ('Balance'), ("AccrIntBooked"),
				('NextAccrDate'), ('Branch'), ('UserID'), ("AvailableBal"),
				("AccrCurMonth"), ("AccrCurCapital")]

	@staticmethod
	def beforeAuthorize():
		ID 		= 	g.formID
		AccObj 	=	MKT_ACCOUNT_INAU.query.get(ID)

		if not AccObj:
			flash(msg_error + "Account %s not found." %ID)
			return False

		AccStatus = AccObj.AccStatus
		if AccStatus == 'C':
			# Check if account closing action
			UserID 	=	AccObj.Inputter
			Check 	= 	mktaccount.setCloseAccount(ID, UserID, "INAU")

			if not Check[0]:
				flash(msg_error + "%s" %Check[1])
				return False

		return True

	@staticmethod
	def redirectAfterInsert():
		ID 				= 	request.form['ID']
		NumOfAuth 		= 	g.getNAuthorize
		BankDate 		=	mktdate.getBankDate()

		if NumOfAuth == 0:
			Acc = MKT_ACCOUNT.query.get(ID)

			if Acc:
				AccStatus = Acc.AccStatus
				if AccStatus == 'C':
					# Check if account closing action
					UserID 	=	Acc.Inputter
					Check 	= 	mktaccount.setCloseAccount(ID, UserID, "AUTH")

		else:
			Acc = MKT_ACCOUNT_INAU.query.get(ID)

		if not Acc:
			flash(msg_error + "Account not found.")
			return False

		Acc.NextAccrDate 	=	str(BankDate)
		db.session.add(Acc)
		db.session.commit()

		return None

	@staticmethod
	def IsAcceptOverride():

		Override 	= False
		Msg 		= ""
		ID 			= request.form['ID']
		Blocked 	= request.form['Blocked']
		AccStatus 	= request.form['AccStatus']

		if Blocked.upper() == "Y":

			Override 	= True
			Msg 		= "You will be block this account %s." %ID
		
		if AccStatus == 'C':
			Override 	= True
			Msg 		= "You will be close this account %s." %ID

		Msg += " Do you want to procceed?"

		return Override, Msg