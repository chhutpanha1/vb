from app.mktcore.imports 	import *
from .forms 				import *
from .account_Internal 		import *
from .account_dd 			import *
from .account_saving 		import *
from .account_pf 			import *

registerCRUD(admin, '/Account', 'Account', FRM_ACCOUNT, [MKT_ACCOUNT])
registerCRUD(admin, '/Account_Internal', 'Account_Internal', FRM_ACCOUNT_INTERNAL, [MKT_ACCOUNT])
registerCRUD(admin, '/Account_DD', 'Account_DD', FRM_ACCOUNT_DD, [MKT_ACCOUNT])
registerCRUD(admin, '/Account_SV', 'Account_SV', FRM_ACCOUNT_SV, [MKT_ACCOUNT])
registerCRUD(admin, '/Account_PF', 'Account_PF', FRM_ACCOUNT_PF, [MKT_ACCOUNT])