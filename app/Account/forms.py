from wtforms 							import DecimalField, IntegerField
from app.mktcore.wtfimports 			import *
from .models 							import *
from .. 								import app, db
from sqlalchemy 						import *
import time
from datetime 							import datetime, date, timedelta

from app.CategoryType.models 			import MKT_CATEG_TYPE
from app.AccProduct.models 				import MKT_ACC_PRODUCT
from app.Currency.models 				import MKT_CURRENCY
from app.JAccount.models 				import MKT_JACCOUNT
from app.InterestRate.models 			import MKT_INTEREST_RATE
from app.ChargeRate.models 				import MKT_CHARGE_RATE
from app.AccRuleDetail.models 			import MKT_ACC_RULE_DE
from app.User.models 					import MKT_USER
from app.Tax.models 					import MKT_TAX
from app.tools.mktcustomer 				import *
from app.tools.catetool 				import *
from decimal 							import *
import string
from app.tools.mktmoney 				import *
from app.tools.user 					import *
from app.tools.mktofficer 				import *
import app.tools.mktaccounting 			as mktaccounting
import app.tools.mktdate 				as mktdate
import app.tools.mktaccount 			as mktaccount
import app.tools.mktteller 				as mktteller

from app.tools.mktroute 				import *

def loadCategoryType():
	return MKT_CATEG_TYPE.query

def loadParent():
	return MKT_CATEGORY.query

def loadAccProduct():
	return MKT_ACC_PRODUCT.query.filter_by(ProductType = 'E').all()

def loadCurrency():
	return MKT_CURRENCY.query.order_by(asc(MKT_CURRENCY.ID)).all()

def loadJAccount():
	return MKT_JACCOUNT.query

def loadTax():
	return MKT_TAX.query

def loadCurrentDate():
	return mktdate.getBankDate()

class FRM_ACCOUNT(exform):

	CustomerList 	= 	HiddenField(requiredlabel("Customer", "*"), [validators.Required()])
	AccName 		= 	TextField(requiredlabel("Account Name", "*"), [validators.Required()])
	Currency 		= 	QuerySelectField(requiredlabel("Currency", "*"),
							query_factory=loadCurrency,
							get_label=u'ID',
							allow_blank=False,
							blank_text=u'None',
							validators=[validators.Required()]
						)
	JAccount 		= 	SelectField('Joint Account',
							choices=[('N', 'No'),
									 ('Y', 'Yes')],
							coerce=str
						)

	JoinID 			= 	QuerySelectField('Join ID',
	                        query_factory=loadJAccount,
	                        get_label=u'ID',
	                        allow_blank=True,
	                        blank_text=u'--None--'
                        )

	AccProduct 		= 	QuerySelectField(requiredlabel('Account Product', '*'),
							query_factory=loadAccProduct,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--None--',
							validators=[validators.Required()]
						)

	AccCategory 	= TextField(requiredlabel("Category", "*"), [validators.Required()])

	InterestRate 	= TextField("Interest Rate", [validators.Optional()])
	FixedRate 		= TextField("Fixed/Rate")
	Charge 			= DecimalField("Charge", [validators.Optional()])

	OpenDate 		= DateField(requiredlabel("Opening Date", "*"), [validators.Required()], default=loadCurrentDate)

	AccStatus 		= SelectField('Status',
						choices=[('O', 'Open'),
								 ('C', 'Close')],
						coerce=str)

	ClosingDate 	= DateField("Closing Date", [validators.Optional()])
	Blocked 		= SelectField('Blocked',
						choices=[('N', 'No'),
								 ('Y', 'Yes')],
						coerce=str)

	BlockReason 	= TextField("Block Reason")
	Dormant 		= SelectField('Dormant',
						choices=[('N', 'No'),
								 ('Y', 'Yes')],
						coerce=str,
						default='N')

	OfficerID 		= HiddenField("Officer")

	AccrInterest 	= DecimalField("Accrual Interest", [validators.Optional()], default=0, description="1-Other Info")
	AccrCurMonth 	= DecimalField("Accr Int for Booking", [validators.Optional()], default=0, description="1-Other Info")
	AccrIntBooked 	= DecimalField("Accr Int was Booked", [validators.Optional()], default=0, description="1-Other Info")
	AccrCurCapital 	= DecimalField("Accr Int for Capitalization", [validators.Optional()], default=0, description="1-Other Info")

	Balance 		= DecimalField("Balance", [validators.Optional()], default=0, description="1-Other Info")
	AvailableBal 	= DecimalField("Available Balance", [validators.Optional()], default=0, description="1-Other Info")
	# UserID 			= HiddenField("User", description="1-Other Info")
	
	Bal = ""
	TranDate = ""

	def validate_Tax(form, field):

		InputRate = form.InterestRate.data

		try:
			if float(InputRate) > 0:
				Tax = request.form['Tax']
				if Tax == "__None" or Tax == "":
					raise ValidationError("This field is required.")
		except:
			raise

	def validate_AccName(form, field):
		global Bal, TranDate
		Bal = form.Balance.data
		TranDate = form.OpenDate.data

	def validate_Charge(form, field):
		Value = form.Charge.data
		if str(Value).find("%") != -1:
			Value = Value.split("%")[0]

		try:
			Value = Decimal(Value)
		except Exception as exe:
			raise ValidationError("The chare you have enter is wrong number value")

		form.Charge.data = Value

	def validate_InterestRate(form, field):
		AccPro = request.form['AccProduct']
		CurrencyKey = request.form['Currency']
		InputRate = form.InterestRate.data

		try:
			InputRate = float(InputRate)
		except:
			raise ValidationError("This field must be in numeric format.")

		Msg = mktaccount.checkInterestRate("AC", AccPro, InputRate, CurrencyKey)

		if Msg:
			raise ValidationError(Msg)
			
	def validate_AccStatus(form, field):
		try:

			AccountBalance = request.form['Balance']
			AccountStatus  = request.form['AccStatus']
			AccountCurrency= request.form['Currency']

			if AccountStatus.upper() == "C":
				# AccountBalance = float(AccountBalance) if AccountBalance else float(0)
				# if AccountBalance != float(0):
				# 	raise ValidationError("Account balance must be %s. Current balance is %s." %(mktmoney.toMoney(float(0), mktmoney.getCurrencyObj(AccountCurrency), 2), mktmoney.toMoney(float(AccountBalance), mktmoney.getCurrencyObj(AccountCurrency), 2)))
				Check = mktteller.isUserVaultAccount()
				if not Check[0]:
					raise ValidationError(Check[1])

		except:
			raise

	def validate_ClosingDate(form, field):

		AccountClosingDate 	= request.form['ClosingDate']
		AccountStatus  		= request.form['AccStatus']

		if AccountStatus.upper() == "C":
			try:
				datetime.strptime(str(AccountClosingDate),'%Y-%m-%d')
			except:
				raise ValidationError("This field must be in date format.")

	@staticmethod
	def moneyField():
		return [("Balance", "Currency"), ("Charge", "Currency"), ("AvailableBal", "Currency"), ("AccrInterest", "Currency"), ("AccrCurMonth", "Currency"), ("AccrCurCapital", "Currency"), ("AccrIntBooked", "Currency")]

	@staticmethod
	def formatMoney():
		return ["Balance", "AvailableBal", "AccrInterest", "AccrCurCapital", "AccrCurMonth", "AccrIntBooked"], "Currency"

	@staticmethod
	def setWidth():

		Fields = [('Currency', len1),
				  ('JAccount', len1),
				  ('JoinID', len2),
				  ('AccCategory', len2),
				  ('FixedRate', len1),
				  ('OpenDate', len3),
				  ('AccStatus', len1),
				  ('ClosingDate', len3),
				  ('Blocked', len1),
				  ('Normant', len1)]

		return Fields

	@staticmethod
	def listField():

		Fields = ["ID", "CustomerList", "AccName", "Currency", "AccCategory",
				  "OpenDate", "Balance", "AvailableBal"]
		return Fields

	@staticmethod
	def hotField():
		hotfield = []

		fielddisplay 	= 	"$('#InterestRate').val(data.InterestKey); $('#Charge').val(data.ChargeRate); $('#FixedRate').val(data.FixedRate)"
		varname 		= 	"AccProductID:$('#AccProduct').val(), Currency:$('#Currency').val()"
		fun 			= 	["Currency", varname, fielddisplay, "/Morakot/AccProductID", "change"]
		hotfield.append(fun)

		fielddisplay 	= 	"$('#AccCategory').val(data.AccProCategory); $('#InterestRate').val(data.InterestKey); $('#Charge').val(data.ChargeRate); $('#FixedRate').val(data.FixedRate)"
		varname 		= 	"AccProductID:$('#AccProduct').val(), Currency:$('#Currency').val()"
		fun 			= 	["AccProduct", varname, fielddisplay, "/Morakot/AccProductID", "change"]
		hotfield.append(fun)

		fielddisplay 	= 	"$('#ClosingDate').val(data.ClosingDate)"
		varname			=	"AccStatus:$('#AccStatus').val()"
		fun 			=	["AccStatus", varname, fielddisplay, "/Morakot/AccountClosingDate", "change"]
		hotfield.append(fun)

		fielddisplay 	= 	"$('#AccName').val(data.CustomerName)"
		varname			=	"CustomerList:$('#CustomerList').val()"
		fun 			=	["CustomerList", varname, fielddisplay, "/Morakot/CustomerNameBySelected", "change"]
		hotfield.append(fun)

		return hotfield

	@staticmethod
	def hotSelectField():
		hotfield = []

		fielddisplay = "$('input[name=JoinID]').attr('readonly', data.Bool)"
		varname = "JAccountID:$('#JAccount').val()"
		fun = ["JAccount", varname, fielddisplay, "/Morakot/JoinID", "change"]
		hotfield.append(fun)

		return hotfield

	@staticmethod
	def setDisable():
		return [('AccCategory'), ('FixedRate'), ('AccrInterest'), ('Balance'), ("AccrIntBooked"),
				('NextAccrDate'), ('Branch'), ("AvailableBal"), ("AccrCurMonth"), ("AccrCurCapital")]

	# @staticmethod
	# def redirectAfterInsert():
	# 	ID 				= 	request.form['ID']
	# 	NumOfAuth 		= 	g.getNAuthorize
	# 	BankDate 		=	mktdate.getBankDate()

	# 	if NumOfAuth == 0:
	# 		Acc = MKT_ACCOUNT.query.get(ID)
	# 	else:
	# 		Acc = MKT_ACCOUNT_INAU.query.get(ID)

	# 	if not Acc:
	# 		flash(msg_error + "Account not found.")
	# 		return False

	# 	Acc.NextAccrDate 	=	str(BankDate)
	# 	db.session.add(Acc)
	# 	db.session.commit()

	# 	return None
	@staticmethod
	def redirectAfterInsert():
		ID 				= 	request.form['ID']
		NumOfAuth 		= 	g.getNAuthorize
		BankDate 		=	mktdate.getBankDate()

		if NumOfAuth == 0:
			Acc = MKT_ACCOUNT.query.get(ID)

			if Acc:
				AccStatus = Acc.AccStatus
				if AccStatus == 'C':
					# Check if account closing action
					UserID 	=	Acc.Inputter
					Check 	= 	mktaccount.setCloseAccount(ID, UserID, "AUTH")

		else:
			Acc = MKT_ACCOUNT_INAU.query.get(ID)

		if not Acc:
			flash(msg_error + "Account not found.")
			return False

		Acc.NextAccrDate 	=	str(BankDate)
		db.session.add(Acc)
		db.session.commit()

		return None

	@staticmethod
	def beforeAuthorize():
		ID 		= 	g.formID
		AccObj 	=	MKT_ACCOUNT_INAU.query.get(ID)

		if not AccObj:
			flash(msg_error + "Account %s not found." %ID)
			return False

		AccStatus = AccObj.AccStatus
		if AccStatus == 'C':
			# Check if account closing action
			UserID 	=	AccObj.Inputter
			Check 	= 	mktaccount.setCloseAccount(ID, UserID, "INAU")

			if not Check[0]:
				flash(msg_error + "%s" %Check[1])
				return False

		return True

	@staticmethod
	def reverseRec():
		return False,"Record not allow to reverse."

	@staticmethod
	def IsAcceptOverride():

		Override 	= False
		Msg 		= ""
		ID 			= request.form['ID']
		Blocked 	= request.form['Blocked']
		AccStatus 	= request.form['AccStatus']

		if Blocked.upper() == "Y":

			Override 	= True
			Msg 		= "You will be block this account %s." %ID
		
		if AccStatus == 'C':
			Override 	= True
			Msg 		= "You will be close this account %s." %ID

		Msg += " Do you want to procceed?"

		return Override, Msg