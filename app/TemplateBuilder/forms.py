from app.mktcore.wtfimports 	import *
from .models 					import *

class FRM_TEMPLATE_BUILDER(exform):

	Title 	= TextField('Title', [validators.Required()])
	Content = TextAreaField('Content', [validators.Required()])