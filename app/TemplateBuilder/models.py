from app.mktcore.mdlimports import *

class MKT_TEMPLATE_BUILDER(exmodel):
	ID 			= db.Column(db.String(10), primary_key=True)
	Title 		= db.Column(db.String(55))
	Content		= db.Column(db.Text())


class MKT_TEMPLATE_BUILDER_HIST(exmodel):
	ID 			= db.Column(db.String(15), primary_key=True)
	Title 		= db.Column(db.String(55))
	Content		= db.Column(db.Text())
