# -*- coding: utf-8 -*-

from flask 						import request, url_for, render_template,flash,redirect,session, g, flash
from .. 						import app, db
from jinja2 					import Template
from app.Login.forms 			import *
from app.mktcore.imports 		import *
from app.mktcore.session 		import *
from .forms 					import *
from sqlalchemy 				import *

import re

import app.tools.mktautoid 		as mktautoid
import app.tools.mktaudit 		as mktaudit
import app.tools.mkttool 		as mkttool
import app.tools.mktapi 		as mktapi
import app.tools.mktmoney	 	as mktmoney
import app.tools.mktdate		as mktdate
import app.tools.user 			as mktuser
import app.tools.mktcustomer 	as mktcustomer
import app.tools.mktparam 		as mktparam
import app.tools.mktsetting 	as mktsetting
import app.tools.mktaddress 	as mktaddress

@app.route("/Morakot/TemplateBuilder/", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getTemplateEditor():
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/TemplateBuilder/","Search")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	classToolbar 	= mkttool.clsToolbar()
	li_html 		= classToolbar.getToolbarevent("Cancel")
	return render_template('templatebuilder/index.html', li_html=li_html)

@app.route("/Morakot/TemplateBuilder/ListLive/", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def listTemplateEditor():
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/TemplateBuilder/","ListLive")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	classToolbar = mkttool.clsToolbar()
	li_html 	 = classToolbar.getToolbarevent("Cancel")
	TE 			 = MKT_TEMPLATE_BUILDER.query.all()
	return render_template('templatebuilder/listlive.html', te = TE, li_html=li_html)

@app.route("/Morakot/TemplateBuilder/ListHist/", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def listHistTemplateEditor():
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/TemplateBuilder/","ListHist")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	classToolbar 	= mkttool.clsToolbar()
	li_html 		= classToolbar.getToolbarevent("Cancel")
	TE 				= MKT_TEMPLATE_BUILDER_HIST.query.all()
	return render_template('templatebuilder/listlive.html', te = TE, li_html=li_html, is_hist=True)

@app.route("/Morakot/TemplateBuilder/New/", methods=['GET', 'POST'])
@app.route("/Morakot/TemplateBuilder/New/<ID>", methods=['GET', 'POST'])
@checkLogOutSession
@checkLogOutTime
def saveTemplateEditor(ID=""):
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/TemplateBuilder/","New")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	if request.method == 'POST':
		Title 	= request.form.get('title')
		Content = request.form.get('template')

		if ID == '':
			ID = request.form.get('ID','')

		if Title == '' or Content == '':
			return 'Title and Content is required', 400

		if ID == '':
			return 'ID is required', 400

		Data 			= mktaudit.getAuditrail()
		Data['Title'] 	= Title
		Data['Content'] = Content
		Data['ID'] 		= ID

		TE = MKT_TEMPLATE_BUILDER(**Data)
		db.session.add(TE)
		db.session.commit();
		flash('New template was created.')
		return ''
	else:
		classToolbar = mkttool.clsToolbar()
		li_html 	 = classToolbar.getToolbarevent("New")
		FormName 	 = "FRM_TEMPLATE_BUILDER"
		g_id 		 = mktautoid.getAutoID(FormName) if not ID else ID
		IDObj		 = MKT_TEMPLATE_BUILDER.query.get(g_id)
		if IDObj :
			flash(msg_error+"ID '%s' is already in use!"%ID)
			return redirect('/Morakot/TemplateBuilder/')
		return render_template('templatebuilder/new.html', li_html=li_html, g_id=g_id, g_readonly='readonly')

@app.route("/Morakot/TemplateBuilder/ViewHist/<ID>", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def viewHistTemplateEditor(ID):
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/TemplateBuilder/","Search")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	classToolbar = mkttool.clsToolbar()
	li_html 	 = classToolbar.getToolbarevent("readonly")
	TE 			 = MKT_TEMPLATE_BUILDER_HIST.query.get(ID)
	if not TE :
		flash(msg_error+'Template with id %s not found!.'%ID)
	return render_template('templatebuilder/edit.html', te = TE, View = True, 
							li_html=li_html, g_id=ID, g_readonly='readonly')

@app.route("/Morakot/TemplateBuilder/<ID>", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def viewTemplateEditor(ID):
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/TemplateBuilder/","Search")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	classToolbar = mkttool.clsToolbar()
	li_html 	 = classToolbar.getToolbarevent("readonly")
	TE 			 = MKT_TEMPLATE_BUILDER.query.get(ID)
	if TE :
		return render_template('templatebuilder/edit.html', te = TE, View = True, 
							li_html=li_html, g_id=ID, g_readonly='readonly')
	flash(msg_error+'Template with id %s not found!.'%ID)
	return redirect('/Morakot/TemplateBuilder/')

@app.route("/Morakot/TemplateBuilder/Edit/<ID>", methods=['GET', 'POST'])
@checkLogOutSession
@checkLogOutTime
def editTemplateEditor(ID):
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/TemplateBuilder/","Edit")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	classToolbar = mkttool.clsToolbar()
	li_html 	 = classToolbar.getToolbarevent("Cancel")
	TE 			 = MKT_TEMPLATE_BUILDER.query.get(ID)
	if not TE:
		flash(msg_error+'Template with ID %s cannot found for updating.'%ID)
		return render_template('templatebuilder/index.html', li_html=li_html)

	if request.method == 'POST':
		Title 	= request.form.get('title')
		Content = request.form.get('template')
		if Title == '' or Content == '':
			return 'Title and Content is required', 400

		TE.Title 	= Title
		TE.Content 	= Content
		db.session.add(TE)
		db.session.commit();
		# flash('Template with ID %s was updated.'%ID)
		return ''
	else:
		li_html = classToolbar.getToolbarevent("Edit")
		return render_template('templatebuilder/edit.html', te = TE, 
								li_html=li_html, g_id=ID, g_readonly='readonly')

@app.route("/Morakot/TemplateBuilder/Delete/<ID>", methods=['POST'])
@checkLogOutSession
@checkLogOutTime
def deleteTemplateEditor(ID):
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/TemplateBuilder/","Delete")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	TE = MKT_TEMPLATE_BUILDER.query.filter_by(ID = ID)
	if TE.count():
		mktaudit.moveAUTHtoHIST(MKT_TEMPLATE_BUILDER,MKT_TEMPLATE_BUILDER_HIST,ID)
	 	TE.delete()
		db.session.commit();
		flash('Template with ID %s was deleted.'%ID)
	else:
		flash(msg_error+'Template with ID %s cannot found for deletion.'%ID)

	return redirect('/Morakot/TemplateBuilder/')

@app.route("/Morakot/Template/<ID>")
@checkLogOutSession
@checkLogOutTime
def viewTemplate(ID):
	ErrorMsg 	= []
	getCheck 	= checkAccess ("/Morakot/Template/"+ID,"Search")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)
	exportButton = """
	<div class="btn-group" >

		<button type="button" class="btn btn-flat btn-primary dropdown-toggle" data-toggle="dropdown">
	 	<span class="btn-label icon fa fa-upload"></span> &nbsp;Export
		</button>

		<ul class="dropdown-menu text-left" style="min-width: 85px;">
			<li><a href="javascript:void(0)" id="csv"> <i class='fa fa-files-o'></i> CSV </a></li> 
			<li><a href="javascript:void(0)" id="excel"> <i class='fa fa-file-excel-o'></i> Excel </a></li>
			<li><a href="javascript:void(0)" id="print"><i class='fa fa-print'></i>&nbsp;Print</a></li>
		</ul>
	</div> 
	"""
	exportButtonLink ="""
	<div id="exportButton">
		<b>Also available in:</b>
		<a id="csv" style="cursor:pointer; color:#1d89cf;" > <i class='fa fa-files-o'></i> CSV &nbsp;</a>|
		<a id="excel" style="cursor:pointer; color:#1d89cf;"> <i class='fa fa-file-excel-o'></i> Excel &nbsp;</a>|
		<a id="print" style="cursor:pointer; color:#1d89cf;"><i class='fa fa-print'></i>&nbsp;Print</a>
	</div>
	"""
	Dict = {
		'eval'						: eval,
		'int' 						: int,
		'float' 					: float,
		'len' 						: len,
		'str' 						: str,
		'url_for' 					: url_for,
		'getRecord'					: mktapi.getRecord,
		'mktapi' 					: __import__("app.urlregister"),
		'request' 					: request.args,
		'toMoney' 					: mktmoney.toMoney,
		'formatNumber'				: mktmoney.formatNumber,
		'CurrencyObj' 				: mktmoney.getCurrencyObj,
		'exportButtonLink' 			: exportButtonLink,
		'exportButton' 				: exportButton,
		'mktdate' 					: mktdate,
		'mktautoid'					: mktautoid,
		'mkt'						:{
										'getUser' 			 : mktuser.getUser,
										'getCurrentBranch' 	 : mktuser.getCurrentBranch,
										'getCustomerAddress' : mktcustomer.getCustomerAddress,
										'getAddress'		 : mktaddress.getAddress,
									 },
		'formatNumberToWord' 		: mkttool.formatNumberToWord,
		# 'formatLangNumber'			: mkttool.formatLangNumber,
	}

	Content = ""
	Temp = MKT_TEMPLATE_BUILDER.query.get(ID)
	if Temp:
		Dict.update({'TemplateTitle':Temp.Title})
		Content = Template(Temp.Content).render(**Dict)
		Content = re.subn(r'<(script).*?</\1>(?s)', '',  Content)[0]
	
	return render_template('templatebuilder/builder.html', Temp = Content)