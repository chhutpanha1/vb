from app.mktcore.wtfimports 	import *
from .models 					import *
from app.Module.models 			import *
import time
from app.tools.mktroute			import *


def loadModule():
	return MKT_MODULE.query

def loadForm():
	return MKT_FORM.query

def checkExistRole(Roles):
	ExistRoles = ['N', 'V', 'D', 'L','A','U', 'H', 'R', 'E', 'ALL']
	Message = '';
	ArrayList = Roles.split()
	for item in ArrayList:
		if item not in ExistRoles:
			Message += '%s ' %item

	if Message:
		raise ValidationError('The access right '+Message+'is not match')

#class role form
class FRM_ROLE(exform):

	Description = TextField(requiredlabel("Description", "*"), [validators.Required()])
	AccessID = TextField(requiredlabel("Access ID", "*"), [validators.Required()], description="1-Access Right")
	
	Module = QuerySelectField(requiredlabel('Module', "*"),
                        query_factory=loadModule,
                        get_label=u'Description',
                        allow_blank=True,
                        blank_text=u'--None--',
                        description="1-Access Right")

	Form = QuerySelectField(requiredlabel('Form', "*"),
                        query_factory=loadForm,
                        get_label=u'FormDesc',
                        allow_blank=True,
                        get_pk =lambda a: a.FormID,
                        blank_text=u'--None--',
                        description="1-Access Right")

	AccessRight = TextField(requiredlabel("Access Right", "*"), [validators.Required()], description="1-Access Right")
	FilterBy = TextField("Filter By", description="1-Access Right")
	

	def validate_AccessRight(form, field):
		checkExistRole(form.AccessRight.data)

	@staticmethod
	def setInau():
		return "0" #0 is self authorization, 1 is required next level authorizationse

	@staticmethod
	def isMultiValue():
		controls_list=["1-Access Right"]
		return controls_list


'''
@app.route("/Morakot/FormByModule", methods=['GET'])
def getFormByModule():
	try:

		ModuleID = request.args.get("Module")
		dic = {}
		query = MKT_FORM.query.filter_by(ID=ModuleID).all()
		if query:
			for row in query:
				dic[str(row.ID)+":"+str(row.FormID)] = row.FormDesc

		return jsonify(results=dic)

	except:
		raise

'''


