from app.mktcore.mdlimports import *

class MKT_ROLE(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	Description = db.Column(db.String(35))

class MKT_ROLE_INAU(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	Description = db.Column(db.String(35))

class MKT_ROLE_HIST(exmodel):
	ID = db.Column(db.String(15), primary_key=True)
	Description = db.Column(db.String(35))

class MKT_ACCESS(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	AccessID = db.Column(db.String(10), primary_key=True)
	Module = db.Column(db.String(6))
	Form = db.Column(db.String(10))
	AccessRight = db.Column(db.String(35))
	FilterBy = db.Column(db.String(300))

class MKT_ACCESS_INAU(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	AccessID = db.Column(db.String(10), primary_key=True)
	Module = db.Column(db.String(6))
	Form = db.Column(db.String(10))
	AccessRight = db.Column(db.String(35))
	FilterBy = db.Column(db.String(300))

class MKT_ACCESS_HIST(exmodel):
	ID = db.Column(db.String(15), primary_key=True)
	AccessID = db.Column(db.String(15), primary_key=True)
	Module = db.Column(db.String(11))
	Form = db.Column(db.String(15))
	AccessRight = db.Column(db.String(35))
	FilterBy = db.Column(db.String(300))