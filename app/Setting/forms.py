from wtforms 						import DecimalField, IntegerField
from app.mktcore.wtfimports 		import *
from .models 						import *
import time


class FRM_SETTING(exform):
	ATTEMPS 				= IntegerField(requiredlabel("Login Attemps", "*"), [validators.Required()])
	TIME_OUT 				= IntegerField(requiredlabel("System Timeout", "*"), [validators.Required()])
	PASSWORD_LENGTH 		= IntegerField(requiredlabel("Password Length", "*"), [validators.Required()])
	PASSWORD_NUM 			= IntegerField(requiredlabel("Password Number", "*"), [validators.Required()])
	PASSWORD_CAP 			= IntegerField(requiredlabel("Password Capital Letter", "*"), [validators.Required()])
	PASSWORD_LOW 			= IntegerField(requiredlabel("Password Lower Letter", "*"), [validators.Required()])

def loadLoanProduct():
	return MKT_LOAN_PRODUCT.query

class FRM_NBC_SETTING(exform):

	Language 		= 	SelectField("Default Language",
									choices=[('', '--None--'),
											 ('en', 'Englist'),
											 ('kh', 'Khmer')],
									coerce=str)

	GroupLoanPro 	= 	QuerySelectField('Group Loan Product',
					                        query_factory=loadLoanProduct,
					                        get_label=u'Description',
					                        allow_blank=True,
					                        blank_text=u'--None--'
				                        )

	IndivLoanPro 	= 	QuerySelectField('Individual Loan Product',
					                        query_factory=loadLoanProduct,
					                        get_label=u'Description',
					                        allow_blank=True,
					                        blank_text=u'--None--'
				                        )

	SmallLoanPro 	= 	QuerySelectField('Small Business Loan Product',
					                        query_factory=loadLoanProduct,
					                        get_label=u'Description',
					                        allow_blank=True,
					                        blank_text=u'--None--'
				                        )
