from app.mktcore.imports 		import *
from .forms 					import *
from .models 					import *

from app.Currency.models 		import MKT_CURRENCY
from app.mktcore.session 		import *
from .builder 					import *

import ast
import collections

registerCRUD(admin, '/AppSetting', 'AppSetting', FRM_APP_SETTING, [MKT_APP_SETTING])


@app.route("/Morakot/Setting", methods=['GET'])
@app.route("/Morakot/Setting/", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getSettingForm():

	fileName 	= request.args.get('file') if 'file' in request.args else ""
	Data 		= ""

	if fileName:

		f 		= open("app/Setting/" + fileName + ".py", 'r+')
		Data 	= f.read()
		Data 	= Data.split("\n")

		f.close()

	return render_template("setting/form.html",
							fileName 		=	fileName,
							Data 			= 	Data,
							generateForm 	= 	generateForm)

def generateForm(ObjForm):
	try:
		# print ObjForm
		VariableName 	= 	ObjForm.split("=")
		VariableName 	=	filter(None, VariableName)
		FormName 		= 	VariableName[0]
		FormAttr 		=	str(VariableName[1]).strip()
		FormAttr 		= 	ast.literal_eval(FormAttr)

		FormName 		= 	str(FormName).strip()

		Output 	=	"<div class='form-group'>"

		if FormAttr['Type'] == 'S':

			Output += "<select name='" + FormName + "' class='form-control'>"
			Output += "<option>A</option>"
			Output += "<option>B</option>"
			Output += "</select>"

		elif FormAttr['Type'] == 'I':

			Output += "<input type='text' name='" + FormName + "' class='form-control' value='" + FormAttr['Value'] + "'/>"

		Output 	+=	"</div>"

		return Output

	except:
		raise

@app.route("/Morakot/Settings", methods=['GET'])
@app.route("/Morakot/Settings/", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getAllSetting():
	
	return render_template("setting/Setting.html")

@app.route("/Morakot/GeneralSetting/", methods=['GET', 'POST'])
@checkLogOutSession
@checkLogOutTime
def getGeneralSetting():

	form 		= FRM_SETTING()
	currency 	= MKT_CURRENCY.query.all()
	setting 	= MKT_SETTING.query.get("SYSTEM")

	if request.method == 'POST':

		ATTEMPS 				= 	request.form['ATTEMPS']
		TIME_OUT 				= 	request.form['TIME_OUT']
		PASSWORD_LENGTH 		= 	request.form['PASSWORD_LENGTH']
		PASSWORD_NUM 			= 	request.form['PASSWORD_NUM']
		PASSWORD_CAP 			= 	request.form['PASSWORD_CAP']
		PASSWORD_LOW 			= 	request.form['PASSWORD_LOW']
		WEEKEND_SAT 			= 	'TRUE' if 'WEEKEND_SAT' in request.form else 'FALSE'
		WEEKEND_SUN 			= 	'TRUE' if 'WEEKEND_SUN' in request.form else 'FALSE'
		Notification 			=	'TRUE' if 'Notification' in request.form else 'FALSE'
		NotificationInterval 	= 	request.form['NotificationInterval']
		SYSTEM_STATUS 			=	'1' if 'SYSTEM_STATUS' in request.form else '0'
		JOURNAL_ENTRY_EXCLUDED 	=	request.form['JOURNAL_ENTRY_EXCLUDED']
		Msg 					=	""
		
		if JOURNAL_ENTRY_EXCLUDED:

			ExcludedCats 	=	JOURNAL_ENTRY_EXCLUDED.split()
			
			for item in ExcludedCats:

				CheckCat 	=	MKT_CATEGORY.query.get(item)

				if not CheckCat:
					Msg += item + ", "

		if Msg:

			flash(msg_warning + "Category " + Msg[:-2] + " not found.")

		else:

			if setting:
				setting.ATTEMPS 				= 	ATTEMPS
				setting.TIME_OUT 				= 	TIME_OUT
				setting.PASSWORD_LENGTH 		= 	PASSWORD_LENGTH
				setting.PASSWORD_NUM 			= 	PASSWORD_NUM
				setting.PASSWORD_CAP 			= 	PASSWORD_CAP
				setting.PASSWORD_LOW 			= 	PASSWORD_LOW
				setting.WEEKEND_SAT 			= 	WEEKEND_SAT
				setting.WEEKEND_SUN 			= 	WEEKEND_SUN
				setting.Notification 			= 	Notification
				setting.NotificationInterval 	= 	NotificationInterval
				setting.SYSTEM_STATUS 			=	SYSTEM_STATUS
				setting.JOURNAL_ENTRY_EXCLUDED 	=	JOURNAL_ENTRY_EXCLUDED

			else:
				setting = MKT_SETTING(
						  ID 						= 	"SYSTEM",
						  ATTEMPS 					= 	ATTEMPS,
						  TIME_OUT 					= 	TIME_OUT,
						  PASSWORD_LENGTH 			= 	PASSWORD_LENGTH,
						  PASSWORD_NUM 				= 	PASSWORD_NUM,
						  PASSWORD_CAP 				= 	PASSWORD_CAP,
						  PASSWORD_LOW 				= 	PASSWORD_LOW,
						  WEEKEND_SAT 				= 	WEEKEND_SAT,
						  WEEKEND_SUN 				= 	WEEKEND_SUN,
						  Notification 				= 	Notification,
						  NotificationInterval 		= 	NotificationInterval,
						  SYSTEM_STATUS 			= 	SYSTEM_STATUS,
						  JOURNAL_ENTRY_EXCLUDED 	=	JOURNAL_ENTRY_EXCLUDED

					)

			db.session.add(setting)
			db.session.commit()
			flash("Settings was changed successfully!")
			return redirect(url_for('getGeneralSetting'))

	return render_template("setting/GeneralSetting.html",
							form=form,
							setting=setting,
							currency=currency)

@app.route("/Morakot/AppSetting/Config/", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getAppSettingConfig():

	ID 			=	request.args.get('ID') if 'ID' in request.args else ""
	SettingObj 	=	MKT_APP_SETTING.query.\
					order_by(MKT_APP_SETTING.ID.asc()).\
					all()

	OneObj 		=	""
	if ID:
		OneObj 	=	MKT_APP_SETTING.query.get(ID)

		if not OneObj:
			flash(msg_warning + "Record not found.")
	
	return render_template("setting/setting_list.html",
								SettingObj 		=	SettingObj,
								OneObj 			=	OneObj,
								getValueByType 	=	getValueByType,
								ID 				=	ID
							)

@app.route("/Morakot/AppSetting/Config/Update/", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getAppSettingConfigUpdate():

	ID 			=	request.args.get('ID') if 'ID' in request.args else ""
	Value 		=	request.args.get('Value') if 'Value' in request.args else ""
	if ID:
		OneObj 	=	MKT_APP_SETTING.query.get(ID)
		if OneObj:
			OneObj.Value 	=	Value
			db.session.add(OneObj)
			db.session.commit()
			flash("Application setting was successfully updated.")
			return 'ok'

	return "Please define ID."

def getValueByType(Value, Selected):
	try:
		
		Output = ""

		if Value.find('*') != -1:
			NewValue = Value.splitlines()
			NewValue = filter(None, NewValue)
			if len(NewValue) > 0:
				for item in NewValue:
					item = item.split('*')
					if item[0] == Selected:
						Output += "<option selected value='%s'>%s</option>" %(item[0], item[1])
					else:
						Output += "<option value='%s'>%s</option>" %(item[0], item[1])

		if Value.find('MKT_') != -1:
			print 'Working with table.'
			
		return Output

	except Exception, e:
		raise