from wtforms 						import DecimalField, IntegerField
from app.mktcore.wtfimports 		import *
from .models 						import *
import time

from app.Module.models 				import MKT_FORM

def loadForm():
	return MKT_FORM.query

class FRM_APP_SETTING(exform):

	Description 	=	TextField(requiredlabel("Description", "*"), [validators.Required()])

	Application 	=	QuerySelectField(
							requiredlabel("Application", "*"),
							query_factory	= loadForm,
							get_label		= u'FormDesc',
							allow_blank		= True,
							blank_text		= u'--Choose Application--',
							validators		= [validators.Required()]
						)

	Type 			=	SelectField(
							requiredlabel("Type", "*"),
							choices = [('I', 'Input Box'),
									   ('S', 'Select Box'),
									   ('T', 'TextArea')],
							default = 'I',
							validators = [validators.Required()]
						)

	Value 			=	TextField("Value")
	LOV 			=	TextAreaField(requiredlabel("List of Value", "*"), [validators.Required()])