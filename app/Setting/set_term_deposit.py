from app.mktcore.wtfimports import *
from .models import *

from app.Transaction.models 		import MKT_TRANSACTION
from app.Category.models 			import MKT_CATEGORY

def loadCategory():
	return MKT_CATEGORY.query

def loadTransaction():
	return MKT_TRANSACTION.query

class FRM_TERM_DEPOSIT_SETTING(exform):

	SavingTermCat 		=	QuerySelectField(requiredlabel("Term Deposit Cat", "*"),
								query_factory=loadCategory,
								get_label=lambda a: a.ID + " - " + a.Description,
								allow_blank=True,
								blank_text=u'--None--',
								validators=[validators.Required()]
							)

	SavingTermTran 		=	QuerySelectField(requiredlabel("Term Deposit Booking Transaction", "*"),
								query_factory=loadTransaction,
								get_label=lambda a: a.ID + " - " + a.Description,
								allow_blank=True,
								blank_text=u'--None--',
								validators=[validators.Required()]
							)

	AIPBookingTran 		=	QuerySelectField(requiredlabel("Accr Int Payables Booking Transaction", "*"),
								query_factory=loadTransaction,
								get_label=lambda a: a.ID + " - " + a.Description,
								allow_blank=True,
								blank_text=u'--None--',
								validators=[validators.Required()]
							)

	IEBookingTran 		=	QuerySelectField(requiredlabel("Int Expenses Booking Transaction", "*"),
								query_factory=loadTransaction,
								get_label=lambda a: a.ID + " - " + a.Description,
								allow_blank=True,
								blank_text=u'--None--',
								validators=[validators.Required()]
							)

	MaturedTran 		=	QuerySelectField(requiredlabel("Matured Date Transaction", "*"),
								query_factory=loadTransaction,
								get_label=lambda a: a.ID + " - " + a.Description,
								allow_blank=True,
								blank_text=u'--None--',
								validators=[validators.Required()]
							)

	RollOverTran 		=	QuerySelectField(requiredlabel("Roll-over Transaction", "*"),
								query_factory=loadTransaction,
								get_label=lambda a: a.ID + " - " + a.Description,
								allow_blank=True,
								blank_text=u'--None--',
								validators=[validators.Required()]
							)

	@staticmethod
	def beforeInsert():
		try:
			ID = request.form['ID']
			if ID.upper() != "SYSTEM":
				raise ValidationError("The id is required only uppercase letter 'SYSTEM'")
				return False

			if ID == 'system' or ID == 'System':
				raise ValidationError("The id is required only uppercase letter 'SYSTEM'")
				return False

			return True
		except:
			raise