from app.mktcore.mdlimports import *

class MKT_SETTING(exmodel):
	ID 				= 	db.Column(db.String(6), primary_key=True)
	ATTEMPS 		= 	db.Column(db.String(2))
	TIME_OUT 		= 	db.Column(db.String(2))
	PASSWORD_LENGTH = 	db.Column(db.String(2))
	PASSWORD_CAP 	= 	db.Column(db.String(2))
	PASSWORD_NUM 	= 	db.Column(db.String(2))
	PASSWORD_LOW 	= 	db.Column(db.String(2))
	WEEKEND_SAT 	= 	db.Column(db.String(5))
	WEEKEND_SUN 	= 	db.Column(db.String(5))
	BaseCurrency 	= 	db.Column(db.String(3))
	Notification	=	db.Column(db.String(5))
	NotificationInterval	=	db.Column(db.String(10))
	GL_KEY1 		= 	db.Column(db.String(25))
	GL_KEY2 		= 	db.Column(db.String(25))
	GL_KEY3 		= 	db.Column(db.String(25))
	GL_KEY4 		= 	db.Column(db.String(25))
	GL_KEY5 		= 	db.Column(db.String(25))
	GL_KEY6 		= 	db.Column(db.String(25))
	GL_KEY7 		= 	db.Column(db.String(25))
	GL_KEY8 		= 	db.Column(db.String(25))
	GL_KEY9 		= 	db.Column(db.String(25))

	ACC_NEXT_MONTH_END 		=	db.Column(db.String(10))
	MONTH_END 				=	db.Column(db.String(3))

	SYSTEM_STATUS 			=	db.Column(db.String(1))
	JOURNAL_ENTRY_EXCLUDED 	=	db.Column(db.Text)

# class MKT_NBC_SETTING(exmodel):
# 	ID 				= 	db.Column(db.String(6), primary_key=True)
# 	Language 		=	db.Column(db.String(2))
# 	GroupLoanPro 	=	db.Column(db.String(3))
# 	IndivLoanPro 	=	db.Column(db.String(3))
# 	SmallLoanPro 	=	db.Column(db.String(3))

# class MKT_NBC_SETTING_INAU(exmodel):
# 	ID 				= 	db.Column(db.String(6), primary_key=True)
# 	Language 		=	db.Column(db.String(2))
# 	GroupLoanPro 	=	db.Column(db.String(3))
# 	IndivLoanPro 	=	db.Column(db.String(3))
# 	SmallLoanPro 	=	db.Column(db.String(3))

# class MKT_NBC_SETTING_HIST(exmodel):
# 	ID 				= 	db.Column(db.String(11), primary_key=True)
# 	Language 		=	db.Column(db.String(2))
# 	GroupLoanPro 	=	db.Column(db.String(3))
# 	IndivLoanPro 	=	db.Column(db.String(3))
# 	SmallLoanPro 	=	db.Column(db.String(3))

class MKT_TERM_DEPOSIT_SETTING(exmodel):

	ID 					=	db.Column(db.String(6), primary_key=True)
	SavingTermCat 		=	db.Column(db.String(15))
	SavingTermTran 		=	db.Column(db.String(3))

	AIPBookingTran 		=	db.Column(db.String(3))
	IEBookingTran 		=	db.Column(db.String(3))
	MaturedTran 		=	db.Column(db.String(3))
	RollOverTran 		=	db.Column(db.String(3))

class MKT_TERM_DEPOSIT_SETTING_INAU(exmodel):

	ID 					=	db.Column(db.String(6), primary_key=True)
	SavingTermCat 		=	db.Column(db.String(15))
	SavingTermTran 		=	db.Column(db.String(3))

	AIPBookingTran 		=	db.Column(db.String(3))
	IEBookingTran 		=	db.Column(db.String(3))
	MaturedTran 		=	db.Column(db.String(3))
	RollOverTran 		=	db.Column(db.String(3))

class MKT_TERM_DEPOSIT_SETTING_HIST(exmodel):

	ID 					=	db.Column(db.String(11), primary_key=True)
	SavingTermCat 		=	db.Column(db.String(15))
	SavingTermTran 		=	db.Column(db.String(3))

	AIPBookingTran 		=	db.Column(db.String(3))
	IEBookingTran 		=	db.Column(db.String(3))
	MaturedTran 		=	db.Column(db.String(3))
	RollOverTran 		=	db.Column(db.String(3))

class MKT_APP_SETTING(exmodel):
	
	ID 					=	db.Column(db.String(30), primary_key=True)
	Description 		=	db.Column(db.String(50))
	Application 		=	db.Column(db.String(50))
	Type 				=	db.Column(db.String(1))
	Value 				=	db.Column(db.String(50))
	LOV 				=	db.Column(db.Text)

class MKT_APP_SETTING_INAU(exmodel):
	
	ID 					=	db.Column(db.String(30), primary_key=True)
	Description 		=	db.Column(db.String(50))
	Application 		=	db.Column(db.String(50))
	Type 				=	db.Column(db.String(1))
	Value 				=	db.Column(db.String(50))
	LOV 				=	db.Column(db.Text)

class MKT_APP_SETTING_HIST(exmodel):
	
	ID 					=	db.Column(db.String(35), primary_key=True)
	Description 		=	db.Column(db.String(50))
	Application 		=	db.Column(db.String(50))
	Type 				=	db.Column(db.String(1))
	Value 				=	db.Column(db.String(50))
	LOV 				=	db.Column(db.Text)