from app.mktcore.imports import *
from .forms import *
from .models import *

registerCRUD(admin, '/Position', 'Position', FRM_POSITION, [MKT_POSITION])
