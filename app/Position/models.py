from app.mktcore.mdlimports import *

class MKT_POSITION(exmodel):
	ID = db.Column(db.String(3), primary_key = True)
	Description = db.Column(db.String(35))

class MKT_POSITION_INAU(exmodel):
	ID = db.Column(db.String(3), primary_key = True)
	Description = db.Column(db.String(35))

class MKT_POSITION_HIST(exmodel):
	ID = db.Column(db.String(8), primary_key = True)
	Description = db.Column(db.String(35))