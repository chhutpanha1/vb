from app.mktcore.mdlimports import *

class MKT_DISTRICT(exmodel):
	ID = db.Column(db.String(5), primary_key = True)
	Description = db.Column(db.String(35))
	Province = db.Column(db.String(3))

class MKT_DISTRICT_INAU(exmodel):
	ID = db.Column(db.String(5), primary_key = True)
	Description = db.Column(db.String(35))
	Province = db.Column(db.String(3))

class MKT_DISTRICT_HIST(exmodel):
	ID = db.Column(db.String(8), primary_key = True)
	Description = db.Column(db.String(35))
	Province = db.Column(db.String(3))