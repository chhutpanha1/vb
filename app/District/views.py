from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/District', 'District', FRM_DISTRICT, [MKT_DISTRICT])