from app.mktcore.wtfimports import *
from app.Province.models 	import *



def loadProvince():
	return MKT_PROVINCE.query

class FRM_DISTRICT(exform):
	Description = TextField(requiredlabel('District', '*'), [validators.Required()])
	Province = QuerySelectField('Province', query_factory = loadProvince, get_label = u'Description', allow_blank = False, blank_text = u'None')