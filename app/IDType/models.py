from app.mktcore.mdlimports import *

class MKT_ID_TYPE(exmodel):
	ID = db.Column(db.String(3), primary_key = True)
	Description = db.Column(db.String(35))

class MKT_ID_TYPE_INAU(exmodel):
	ID = db.Column(db.String(3), primary_key = True)
	Description = db.Column(db.String(35))

class MKT_ID_TYPE_HIST(exmodel):
	ID = db.Column(db.String(8), primary_key = True)
	Description = db.Column(db.String(35))