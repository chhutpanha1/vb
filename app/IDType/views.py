from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/IDType','IDType', FRM_ID_TYPE, [MKT_ID_TYPE])