from app.mktcore.imports import *
from .forms import *
from .models import *

registerCRUD(admin, '/Sector', 'Sector', FRM_SECTOR, [MKT_SECTOR])
