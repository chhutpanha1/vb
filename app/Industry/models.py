from app.mktcore.mdlimports import *

class MKT_INDUSTRY(exmodel):
	ID = db.Column(db.String(5), primary_key = True)
	Description = db.Column(db.String(35))
	Sector = db.Column(db.String(3))

class MKT_INDUSTRY_INAU(exmodel):
	ID = db.Column(db.String(5), primary_key = True)
	Description = db.Column(db.String(35))
	Sector = db.Column(db.String(3))

class MKT_INDUSTRY_HIST(exmodel):
	ID = db.Column(db.String(10), primary_key = True)
	Description = db.Column(db.String(35))
	Sector = db.Column(db.String(8))