from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/Industry', 'Industry', FRM_INDUSTRY, [MKT_INDUSTRY])