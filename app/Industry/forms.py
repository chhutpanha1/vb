from app.mktcore.wtfimports import *
from app.Sector.models import *

def loadSector():
	return MKT_SECTOR.query

class FRM_INDUSTRY(exform):
	Description = TextField(requiredlabel('Description', '*'), [validators.Required()])
	Sector = QuerySelectField('Sector', query_factory = loadSector, get_label = u'Description', allow_blank = False, blank_text = u'None')