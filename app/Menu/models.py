from app.mktcore.mdlimports import *

#class for table MKT_MENU
class MKT_MENU(exmodel):
	ID 			= db.Column(db.String(10), primary_key=True)
	Description = db.Column(db.String(35))

class MKT_MENU_INAU(exmodel):
	ID 			= db.Column(db.String(10), primary_key=True)
	Description = db.Column(db.String(35))

class MKT_MENU_HIST(exmodel):
	ID 			= db.Column(db.String(15), primary_key=True)
	Description = db.Column(db.String(35))

#class for table MKT_MENU_ITEM
class MKT_MENU_ITEM(exmodel):
	ID 			= db.Column(db.String(10), primary_key=True) #ID of MKT_MENU it's foriegn_key
	ItemID 		= db.Column(db.String(12), primary_key=True)
	ItemDesc 	= db.Column(db.String(35))
	Icon 		= db.Column(db.String(30))
	Form 		= db.Column(db.String(35))
	Parents 	= db.Column(db.String(20))
	Active 		= db.Column(db.String(1))

class MKT_MENU_ITEM_INAU(exmodel):
	ID 			= db.Column(db.String(10), primary_key=True) #ID of MKT_MENU_INAU it's foriegn_key
	ItemID 		= db.Column(db.String(12), primary_key=True)
	ItemDesc 	= db.Column(db.String(35))
	Icon 		= db.Column(db.String(30))
	Form 		= db.Column(db.String(35))
	Parents 	= db.Column(db.String(20))
	Active 		= db.Column(db.String(1))

class MKT_MENU_ITEM_HIST(exmodel):
	ID 			= db.Column(db.String(15), primary_key=True)  #ID of MKT_MENU_HIST it's foriegn_key
	ItemID 		= db.Column(db.String(12), primary_key=True)
	ItemDesc 	= db.Column(db.String(35))
	Icon 		= db.Column(db.String(30))
	Form 		= db.Column(db.String(35))
	Parents 	= db.Column(db.String(20))
	Active 		= db.Column(db.String(1))