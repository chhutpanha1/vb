from flask 						import g, flash
from app.mktcore.wtfimports 	import *
from .models 					import *
from app.Module.models 			import *
import time

@app.route("/Morakot/FormDescriptionByItem", methods=['GET'])
def getFormDescriptionByItem():
	try:

		ItemDesc 	= 	""
		ID 			=	""
		FormID 		=	""
		StrID 		=	request.args.get('FormID')

		if StrID.find(':') != -1:
			StrID 	=	StrID.split(':')
			if len(StrID) >= 1:
				ID 		=	StrID[0]
				FormID 	=	StrID[1]

		Form 		=	MKT_FORM.query.\
						filter(MKT_FORM.ID == ID).\
						filter(MKT_FORM.FormID == FormID).\
						first()

		# print Form
		if Form:
			ItemDesc = Form.FormDesc

		return jsonify(ItemDesc=ItemDesc)
		
	except:
		raise

@app.route("/Morakot/ParentsByItemID", methods=['GET'])
def getParentsByItemID():
	try:

		Parents 	=	""
		StrID 		=	request.args.get('ItemID')

		if StrID.find('-') != -1:
			StrID 	=	StrID.split('-')

			NewStr 	=	StrID[:-1]

			for Item in NewStr:

				Parents += Item + "-"

			Parents = Parents[:-1]

		# print Parents

		return jsonify(Parents=Parents)
		
	except:
		raise

def loadForm():
	return MKT_FORM.query

class FRM_MENU(exform):

	Description 	= 	TextField(requiredlabel("Description", "*"), [validators.Required()])
	ItemID 			= 	TextField(requiredlabel("Item ID", "*"), [validators.Required()], description="1-Menu Item")
	
	Form 			= 	QuerySelectField(requiredlabel('Form', "*"),
	                        query_factory=loadForm,
	                        get_label=u'FormDesc',
	                        allow_blank=True,
	                        blank_text=u'--None--',
	                        description="1-Menu Item"
	                    )

	ItemDesc 		= 	TextField(requiredlabel("Description", "*"), [validators.Required()], description="1-Menu Item")
	Icon 			= 	TextField("Icon", description="1-Menu Item")
	Parents 		= 	TextField("Parent", description="1-Menu Item")

	Active 			= 	SelectField(requiredlabel('Active', '*'),
							choices=[('Y', 'Yes'),
									 ('N', 'No')],
							coerce=str,
							default='Y',
							validators=[validators.Required()],
							description="1-Menu Item"
						)

	@staticmethod
	def setWidth():
		Fields = [('Parents', len2)]
		return Fields

	@staticmethod
	def isMultiValue():
		controls_list=["1-Menu Item"]
		return controls_list

	@staticmethod
	def isSort():
		return "ItemID"

	@staticmethod
	def hotField():

		hotfield 		= 	[]

		fielddisplay 	= 	("$('#ItemDesc').val(data.ItemDesc)")
		varname 		= 	("FormID:$('#Form').val()")
		fun 			= 	["Form", varname, fielddisplay, "/Morakot/FormDescriptionByItem", "change"]
		hotfield.append(fun)

		fielddisplay 	= 	("$('#Parents').val(data.Parents)")
		varname 		= 	("ItemID:$('#ItemID').val()")
		fun 			= 	["ItemID", varname, fielddisplay, "/Morakot/ParentsByItemID", "blur"]
		hotfield.append(fun)

		return hotfield