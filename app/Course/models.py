from app.mktcore.mdlimports 		import *

class MKT_COURSE(exmodel):
	ID                 =        db.Column(db.String(10), primary_key=True)
	Description    	   =        db.Column(db.String(50))
	Currency		   =		db.Column(db.String(4))
	AmountPerHours     =        db.Column(db.String(6))
	TotalHours         =        db.Column(db.String(4))

class MKT_COURSE_INAU(exmodel):
	ID                 =        db.Column(db.String(10), primary_key=True)
	Description    	   =        db.Column(db.String(50))
	Currency		   =		db.Column(db.String(4))
	AmountPerHours     =        db.Column(db.String(6))
	TotalHours         =        db.Column(db.String(4))

class MKT_COURSE_HIST(exmodel):
	ID                 =        db.Column(db.String(15), primary_key=True)
	Description    	   =        db.Column(db.String(50))
	Currency		   =		db.Column(db.String(4))
	AmountPerHours     =        db.Column(db.String(6))
	TotalHours         =        db.Column(db.String(4))