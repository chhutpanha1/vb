from 	app.mktcore.wtfimports 		import *
from 	app.Course.models 			import *
from app.Currency.models		import MKT_CURRENCY

def loadCurrency():
	return MKT_CURRENCY.query


class FRM_COURSE(exform):
	Description 		=		TextField(requiredlabel("Description" , "*") , [validators.Required()])
	Currency 		    = 		QuerySelectField(requiredlabel('Currency', "*"),
									query_factory = loadCurrency,
									get_label=u'Description',
									allow_blank=True,
									blank_text=u'--Choose Currency--',
									validators=[validators.Required()])
	AmountPerHours		=		TextField(requiredlabel("Amount per Hours" , "*") , [validators.Required()])
	TotalHours			=		TextField(requiredlabel("Total Hours" , "*") , [validators.Required()])
	


	def validate_AmountPerHours(form , field):
		number			=		(field.data).replace(" " , "")

		try:
			number    =   float(number)
		except Exception as e:
			raise ValidationError("Number only is allow!")

	def validate_TotalHours(form , field):
		number			=		(field.data).replace(" " , "")

		try:
			number    =   int(number)
		except Exception as e:
			raise ValidationError("Number only is allow!")

	@staticmethod
	def moneyField():
		return [["AmountPerHours","Currency"]]

