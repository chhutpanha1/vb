from app.mktcore.mdlimports import *

class MKT_COMPANY(exmodel):
	ID			=	db.Column(db.String(6), primary_key=True)
	CompanyName	=	db.Column(db.String(65))
	LocalName	=	db.Column(db.String(300))
	Address1	=	db.Column(db.String(100))
	Address2	=	db.Column(db.String(100))
	Contact		= 	db.Column(db.String(35))
	Tel			=	db.Column(db.String(15))
	Mobile1		=	db.Column(db.String(15))
	Mobile2		=	db.Column(db.String(15))
	Email		=	db.Column(db.String(35))
	Website		=	db.Column(db.String(35))
	Logo		=	db.Column(db.String(65))


class MKT_COMPANY_INAU(exmodel):
	ID			=	db.Column(db.String(6), primary_key=True)
	CompanyName	=	db.Column(db.String(65))
	LocalName	=	db.Column(db.String(300))
	Address1	=	db.Column(db.String(100))
	Address2	=	db.Column(db.String(100))
	Contact		= 	db.Column(db.String(35))
	Tel			=	db.Column(db.String(15))
	Mobile1		=	db.Column(db.String(15))
	Mobile2		=	db.Column(db.String(15))
	Email		=	db.Column(db.String(35))
	Website		=	db.Column(db.String(35))
	Logo		=	db.Column(db.String(65))


class MKT_COMPANY_HIST(exmodel):
	ID			=	db.Column(db.String(11), primary_key=True)
	CompanyName	=	db.Column(db.String(65))
	LocalName	=	db.Column(db.String(300))
	Address1	=	db.Column(db.String(100))
	Address2	=	db.Column(db.String(100))
	Contact		= 	db.Column(db.String(35))
	Tel			=	db.Column(db.String(15))
	Mobile1		=	db.Column(db.String(15))
	Mobile2		=	db.Column(db.String(15))
	Email		=	db.Column(db.String(35))
	Website		=	db.Column(db.String(35))
	Logo		=	db.Column(db.String(65))