from app.mktcore.wtfimports import *
from .models import *
import time

class FRM_COMPANY(exform):
	LocalName		=	TextField(requiredlabel("Company Local Name","*"),[validators.Required()])
	CompanyName		=	TextField(requiredlabel("Company Name","*"),[validators.Required()])
	Contact			=	TextField("Contact Name")
	Tel				=	TextField("Telephone")
	Mobile1			=	TextField("Mobile1")
	Mobile2			=	TextField("Mobile2")
	Email			=	TextField("Email")
	Website			=	TextField("Website")
	Logo 			=	TextField("Logo")
	Address1		=	TextAreaField("Address1")
	Address2		=	TextAreaField("Address2")
	

	@staticmethod
	def listField():

		Fields = ["ID", "LocalName", "CompanyName", "Contact", "Tel", "Mobile1", "Email", "Website","Address1"]
		return Fields

	@staticmethod
	def beforeInsert():

		ID = str(request.form['ID'])
		if ID.upper() !="SYSTEM":
			raise ValidationError("Must always use SYSTEM as ID")
			return False

		elif ID =='system':
			raise ValidationError("ID is Capital letter. Example SYSTEM")
			return False

		return True
