from app.mktcore.mdlimports 	import *



class MKT_STUDENT(exmodel):
	ID 				=		db.Column(db.String(35) , primary_key = True)
	FirstName		=		db.Column(db.String(35))
	LastName		=		db.Column(db.String(35))
	Gender			=		db.Column(db.String(6))
	DOB				=		db.Column(db.String(10))
	Phone			= 		db.Column(db.String(9))
	Province 		=		db.Column(db.String(20))
	District		=		db.Column(db.String(20))
	Commune			=		db.Column(db.String(30))
	Village			=		db.Column(db.String(30))

class MKT_STUDENT_INAU(exmodel):
	ID 				=		db.Column(db.String(35) , primary_key = True)
	FirstName		=		db.Column(db.String(35))
	LastName		=		db.Column(db.String(35))
	Gender			=		db.Column(db.String(6))
	DOB				=		db.Column(db.String(10))
	Phone			= 		db.Column(db.String(9))
	Province 		=		db.Column(db.String(20))
	District		=		db.Column(db.String(20))
	Commune			=		db.Column(db.String(30))
	Village			=		db.Column(db.String(30))

class MKT_STUDENT_HIST(exmodel):
	ID 				=		db.Column(db.String(40) , primary_key = True)
	FirstName		=		db.Column(db.String(35))
	LastName		=		db.Column(db.String(35))
	Gender			=		db.Column(db.String(6))
	DOB				=		db.Column(db.String(10))
	Phone			= 		db.Column(db.String(9))
	Province 		=		db.Column(db.String(20))
	District		=		db.Column(db.String(20))
	Commune			=		db.Column(db.String(30))
	Village			=		db.Column(db.String(30))