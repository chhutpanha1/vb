from app.mktcore.wtfimports 	import *
from .models 					import *
import datetime
import math
import re

from app.Province.models		import MKT_PROVINCE
from app.District.models		import MKT_DISTRICT
from app.Commune.models			import MKT_COMMUNE
from app.Village.models			import MKT_VILLAGE


def loadProvince():
	return MKT_PROVINCE.query

def loadDistrict(): #define function
	if request.method == 'POST':
		Province = request.form["Province"]
	else:
		if len(getRecord())>0:
			Record = getRecord()
			return MKT_DISTRICT.query.filter_by(ID=Record['District'])
		else:
			return MKT_DISTRICT.query.filter_by(ID='').all()
	return MKT_DISTRICT.query.filter_by(Province=Province) # tablename.query


def loadCommune(): #define function
	if request.method == 'POST':
		District=request.form["District"]
	else:
		if len(getRecord())>0:
			Record = getRecord()
			return MKT_COMMUNE.query.filter_by(ID=Record['Commune'])
		else:
			return MKT_COMMUNE.query.filter_by(ID='').all()
	return MKT_COMMUNE.query.filter_by(District=District) # tablename.query


def loadVillage():
	if request.method == 'POST':
		Commune=request.form["Commune"]
	else:
		if len(getRecord())>0:
			Record = getRecord()
			return MKT_VILLAGE.query.filter_by(ID=Record['Village'])
		else:
			return MKT_VILLAGE.query.filter_by(ID='').all()
	return MKT_VILLAGE.query.filter_by(Commune=Commune) # tablename.query



#function
#After Save record will be look up value from table store
def getRecord():
	try:
		result = MKT_STUDENT.query.filter_by(ID=g.formID).all()
		if not result:
			result = MKT_STUDENT_INAU.query.filter_by(ID=g.formID).all()
			Dict={}
		if result:
			for row in result:
				Dict = row.__dict__
			return Dict
		else:
			return Dict
	except:
		raise





class FRM_STUDENT(exform):
	FirstName		 = 		TextField(requiredlabel("First Name" , "*") , [validators.Required()])
	LastName 		 = 		TextField(requiredlabel("Last Name" , "*") , [validators.Required()])
	Gender			 =		SelectField(requiredlabel("Gender" , "*") , choices = [(1,"Male") , (2,"Female")] , coerce = int , validators = [validators.Required()])
	DOB 	 		 = 		DateField(requiredlabel("Date of Birth" , "*") , [validators.Required()])
	Phone			 =		TextField(requiredlabel("Phone" ,"*") , [validators.Required()])
	Province 			= 	QuerySelectField(requiredlabel('Province', "*"),
							query_factory=loadProvince,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--Choose Province--',
							validators=[validators.Required()])
	District 			= 	QuerySelectField('District',
							query_factory=loadDistrict,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--Choose District--')
	Commune 			= 	QuerySelectField('Commune',
							query_factory=loadCommune,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--Choose Commune--')
	Village 			= 	QuerySelectField('Village',
							query_factory=loadVillage,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--Choose Village--')

	@staticmethod
	def hotSelectField():
		hotfield 		=		[]
		fielddisplay 	=		"District, #Commune, #Village"
		varname 		= 		"ProvinceID:$('#Province').val()"
		fun 			= 		["Province", varname ,fielddisplay, "/Morakot/DistrictID", "click"]
		hotfield.append(fun)

		fielddisplay 	= 		"Commune, #Village"
		varname 		=  		"DistrictID:$('#District').val()"
		fun 			= 		["District", varname ,fielddisplay, "/Morakot/CommuneID", "click"]
		hotfield.append(fun)

		fielddisplay 	=		"Village"
		varname 		=		"CommuneID:$('#Commune').val()"
		fun 			= 		["Commune", varname ,fielddisplay, "/Morakot/VillageID", "click"]
		hotfield.append(fun)

		return hotfield

	def validate_DOB(form, field): # validate_field name. The validation happens on EndDate

		getDate 		= 		request.form["DOB"]
		before			=		datetime.datetime.strptime(getDate, '%Y-%m-%d')
		after 			= 		datetime.datetime.now()
		year  			=		math.floor(((after - before).days) / 365)
		if before > datetime.datetime.now() or year<18:
			raise ValidationError('Your age is not allow')

	def validate_Phone(form , field):
		sixDigit 				=		["098","099","010","011","012","013","015","016","017","018","060","061","066","067","068","069","070","077","078","080","081","083","084","085","086","087","089","090","092","093","095"]
		sevenDigit				=		["096","097","088","076","071","038","031"]

		getPhoneNumber			=		(field.data).replace(" " , "")

		try:
			number    			=   	int(getPhoneNumber)
		except Exception as e:
			raise ValidationError("Number only is allow!")
		
		if len(getPhoneNumber) > 10 or len(getPhoneNumber) < 9:
			raise ValidationError('Your phone number is invalid!')

		if getPhoneNumber[0:3] in sixDigit:
			if len(getPhoneNumber) is not 9 :
				raise ValidationError("Your phone number must be 9 digit")
			else:
				return
		elif getPhoneNumber[0:3] in sevenDigit: 
			if len(getPhoneNumber) is not 10 :
				raise ValidationError("Your phone number must be 10 digit")
			else:
				return
		else:
			raise ValidationError("System doesn't have this number!")

		
		
		
		# raise ValidationError('End date is required')

	# @staticmethod
	# def reverseRec():
	# 	a = 2
	# 	getGlobalID=request.form["ID"]
	# 	print getGlobalID
	# 	if a == 2:
	# 		db.session.query(MKT_STUDENT).filter_by(ID=g.formID).delete(synchronize_session="fetch")
	# 		# db.session.commit()
	# 		return
	# 	else :
	# 		return True,"your defined message"
	# @staticmethod
	# def listField():
	# 	Field = ["FirstName","LastName","DOB"]
	# 	return Field,["FirstName*BTW*Panha"]
	# @staticmethod
	# def findinHist () :
	# 	return False
	# @staticmethod
	# def beforeNewID () :
	# 	a = 2
	# 	if a == 2 :
	# 		return True, "Need to check your input value"
	# 	else :
	# 		return False, ""
	# @staticmethod
	# def formReadOnly():
	# 	return True