from 	app.mktcore.mdlimports 		import *



class MKT_TEST(exmodel):
	ID 				=		db.Column(db.String(35) , primary_key = True)
	Description 	=		db.Column(db.String(35))
	

class MKT_TEST_INAU(exmodel):
	ID 				=		db.Column(db.String(35) , primary_key = True)
	Description 	=		db.Column(db.String(35))
	

class MKT_TEST_HIST(exmodel):
	ID 				=		db.Column(db.String(40) , primary_key = True)
	Description 	=		db.Column(db.String(35))
	

class MKT_ACCESS_RIGHT(exmodel):
	ID 				=		db.Column(db.String(35) , primary_key = True)
	Currency		=		db.Column(db.String(4))
	Money			=		db.Column(db.String(10))

class MKT_ACCESS_RIGHT_INAU(exmodel):
	ID 				=		db.Column(db.String(35) , primary_key = True)
	Currency		=		db.Column(db.String(4))
	Money			=		db.Column(db.String(10))

class MKT_ACCESS_RIGHT_HIST(exmodel):
	ID 				=		db.Column(db.String(40) , primary_key = True)
	Currency		=		db.Column(db.String(4))
	Money			=		db.Column(db.String(10))
