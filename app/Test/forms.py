from app.mktcore.wtfimports 	import *
from .models 					import *
from app.Currency.models		import MKT_CURRENCY

def loadCurrency():
	return MKT_CURRENCY.query

class frmTest(exform):
	Description 		=		TextField(requiredlabel("Description","*") , [validators.Required()])
	Currency 			= 		QuerySelectField(requiredlabel('Currency', "*"),
									query_factory = loadCurrency,
									get_label=u'Description',
									allow_blank=True,
									blank_text=u'--Choose Currency--',
									validators=[validators.Required()], description = "1-Access Right")
	Money 				=		TextField(requiredlabel("Money","*"),[validators.Required()] , description = "1-Access Right")


	@staticmethod
	def isMultiValue():
		controls_list = ["1-Access Right"]
		return controls_list

	@staticmethod
	def moneyField():
		return [["Money","Currency"]]

	# @staticmethod
	# def IsAcceptOverride():
	# #write your stuff here and then return True if you want to raise accept override and put yourmessage
	# 	return True,"Hello Panha"

	@staticmethod
	def redirectAfterInsert():
		return "/Morakot?a="+ request.form["ID"] # you need to put full path of url:/Morakot/Province/ListLive/Operation/?a="+ request.form["ID"]