from app.mktcore.mdlimports import *

class MKT_NOTIFICATION(exmodel):
	ID=db.Column(db.Integer,primary_key=True, autoincrement=True)
	GlobalID=db.Column(db.Text)
	AppName=db.Column(db.String(50))
	Path=db.Column(db.String(50))
	Title=db.Column(db.String(50))
	Description=db.Column(db.String(50))
	Read=db.Column(db.Text)
	ReadDetail=db.Column(db.Text)