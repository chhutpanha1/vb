# -*- coding: utf-8 -*-
import sys
from app.mktcore.imports 	import *
from .. import app,db
from .models 				import *
from flask 					import g,Flask, render_template, request, session,jsonify,flash,template_rendered,Response
from flask.ext.login 		import LoginManager, login_user, logout_user, current_user, login_required
from sqlalchemy.sql 		import func
from datetime 				import datetime, timedelta

import app.tools.user 				as mktuser
import app.tools.mktreport 			as mktreport
import app.tools.mktsetting 		as mktsetting
import app.Login.login 				as mktlogin
import app.tools.mktdb 				as mktdb
import app.tools.mktnotification 	as mktnotification

from app.User.models 		import *
from app.LoginUser.models 	import *



# # Example call
@app.route("/Morakot/Push")
def publish():
	# mktnotification.setPushNotify()
	mktnotification.setClearNotify()
	return "OK"

# @app.route('/Morakot/SSE')
# def page():
#     debug_template = """
#     <!DOCTYPE html>
#     <html>

#         <head>
#             <script type="text/javascript" src="//code.jquery.com/jquery-1.8.0.min.js"></script>
#             <script type="text/javascript">
#                 $(document).ready(
#                         function() {
#                             sse = new EventSource('/Morakot/Stream');
#                             sse.onmessage = function(e) {
#                                 console.log('A message has arrived!');
#                                 var strdata = JSON.stringify(eval('('+e.data+')'));
#                                 var data = JSON.parse(strdata);
#                                 $('#output').append('<li>'+strdata+'</li>');

                            	
#                             }

#                         })

#             </script>
#         </head>

#         <body>

#             <h2>Demo</h2>
#             <ul id="output">

#             </ul>

#         </body>

#     </html>
#     """
#     return(debug_template)

# SettingObj 				= mktsetting.getSetting()
# SettingNotify 			= True if SettingObj.Notification == "TRUE" else False
# if SettingNotify:
# 	SettingNotifyInterval	= int (SettingObj.NotificationInterval) if SettingObj.NotificationInterval else 0
# else:
# 	SettingNotifyInterval 	= int(SettingObj.TIME_OUT if SettingObj.TIME_OUT else 15)*60

@app.route('/Morakot/Stream')
@login_required
def routeStream():#see_request
	Dic						= {}
	SettingObj 				= mktsetting.getSetting()
	SettingNotify 			= True if SettingObj.Notification == "TRUE" else False
	Interval	= int (SettingObj.NotificationInterval) if SettingObj.NotificationInterval else 0
	if SettingNotify:
		Dic.update(mktnotification.setPushNotify())
		time.sleep(Interval)
	else:
		Interval= int(SettingObj.TIME_OUT if SettingObj.TIME_OUT else 15)*60
		time.sleep(Interval)

	# Call Check session log-out
	Dic.update(sessionStreamLogOut())
	return Response(mktnotification.event_stream(Dic),mimetype='text/event-stream')

@app.route('/Morakot/NumberOfNotification', methods=['GET'])
@login_required
def getNumberOfNotification():

	Count 		= '0'
	Check 		= ''
	Session 	= 'False'
	if 'getLogInID' in session:

		DefaultBranch 	= mktuser.getCurrentBranch()
		ObjUser 		= mktuser.getUser()
		CurrentUser 	= ObjUser.ID
		MuteNotification= "Y" if ObjUser.Notification == None or ObjUser.Notification == "" else ObjUser.Notification
		getNow 			= datetime.now()
		TimeOut 		= int(mktsetting.getSetting().TIME_OUT if mktsetting.getSetting().TIME_OUT else 15)
		PlusExpireTime 	= getNow - timedelta(minutes=TimeOut)
		LoginUserObj 	= MKT_LOGIN_USER.query.filter(MKT_LOGIN_USER.LastTimeActivity <=  PlusExpireTime).\
										filter(MKT_LOGIN_USER.UserID==CurrentUser).all()
		if LoginUserObj:

			session.clear()
			flash("Session time-out.")
			Session 	= 'False'
		else:
			Session 	= 'True'
			Check 			= 'New'
			Notification 	= MKT_NOTIFICATION.query.\
							filter(MKT_NOTIFICATION.Branch==DefaultBranch).\
							filter(MKT_NOTIFICATION.Inputter!=CurrentUser)
			Count = 0
			for row in Notification:
				#if user read already doesn't count
				if not CurrentUser in row.Read.split():
					Count += 1
			Count = str(Count)
			if 'Notification' in session:
				if Count == session['Notification']:
					Check = 'Old'
			session['Notification']=Count

	return jsonify(Session=Session,Count=Count,Check=Check,MuteNotification=MuteNotification)

@app.route('/Morakot/NotificationMessageConfig/<getID>', methods=['GET'])
@login_required
def routeNotificationMessageConfig(getID=''):
	CurrentUser 	= mktuser.getUser().ID
	if getID:
		if CurrentUser:
			QueryObj = MKT_USER.query.\
						filter(MKT_USER.ID==CurrentUser).all()
			try:
				if QueryObj:
					for row in QueryObj:
						#update which user read
						if getID == "True":
							row.Notification = "Y"
							db.session.add(row)
						else:
							row.Notification = "N"
							db.session.add(row)
					db.session.commit()
					db.session.close()
					return "True"
			except Exception:
				db.session.rollback()
				db.session.close()
				return "False"
# When click one by one
@app.route('/Morakot/NotificationMessageRead/<getID>', methods=['GET'])
@login_required
def routeNotificationMessageRead(getID=''):
	CurrentUser 	= mktuser.getUser().ID
	try:
		if getID:
			if CurrentUser:
				QueryObj = MKT_NOTIFICATION.query.\
							filter(MKT_NOTIFICATION.ID==getID).all()
				if QueryObj:
					for row in QueryObj:
						ReadDetail ='' if  row.ReadDetail == None else str(row.ReadDetail)
						#update which user read
						if not CurrentUser in ReadDetail.split():
							row.ReadDetail = '%s %s'%(ReadDetail,CurrentUser)
							db.session.add(row)
					db.session.commit()
					db.session.close()
					return "True"
	except Exception, e:
		db.session.rollback()
		db.session.close()
		return "False"

# When click box update all
@app.route('/Morakot/NotificationMessageUpdateCount', methods=['GET'])
@login_required
def routeNotificationMessageUpdateCount(getID=''):

	DefaultBranch 	= mktuser.getCurrentBranch()
	CurrentUser 	= mktuser.getUser().ID
	Notification 	= MKT_NOTIFICATION.query.\
							filter(MKT_NOTIFICATION.Branch==DefaultBranch).\
							order_by(MKT_NOTIFICATION.Createdon.desc()).all()
	try:
		for row in Notification:
			Read = '' if  row.Read == None else str(row.Read)
			#update which user read
			if not CurrentUser in Read:
				row.Read = '%s %s'%(Read,CurrentUser)
				db.session.add(row)
		db.session.commit()
		db.session.close()
	except Exception, e:
		db.session.rollback()
		db.session.close()
	return ""

@app.route('/Morakot/NotificationMessageMore', methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getNotificationMessageMore():
	Notification=[]
	EventCode 		= mktnotification.getEventCode()
	DefaultBranch 	= mktuser.getCurrentBranch()
	CurrentUser 	= mktuser.getUser().ID
	UserNotification= mktuser.getUser().Notification
	QueryObj 		= db.session.query(MKT_NOTIFICATION.GlobalID,
										MKT_NOTIFICATION.Inputter,
										MKT_NOTIFICATION.Branch,
										MKT_NOTIFICATION.Createdon,
										MKT_NOTIFICATION.ID,
										MKT_NOTIFICATION.Title,
										MKT_NOTIFICATION.Path,
										MKT_NOTIFICATION.Description,
										MKT_NOTIFICATION.ReadDetail,
										MKT_USER.ProfilePicture,
										MKT_USER.DisplayName).\
					join(MKT_USER,MKT_USER.ID==MKT_NOTIFICATION.Inputter).\
					filter(MKT_NOTIFICATION.Branch==DefaultBranch).\
					order_by(MKT_NOTIFICATION.Createdon.desc()).limit(3000)
	return render_template('notification/more.html',
							UserNotification=UserNotification,
							QueryObj=QueryObj,
							EventCode=EventCode)

@app.route('/Morakot/NotificationMessageBox', methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getNotificationMessageBox():

	EventCode 		= mktnotification.getEventCode()
	DefaultBranch 	= mktuser.getCurrentBranch()
	CurrentUser 	= mktuser.getUser().ID
	Notification 	= MKT_NOTIFICATION.query.\
							filter(MKT_NOTIFICATION.Branch==DefaultBranch).\
							order_by(MKT_NOTIFICATION.Createdon.desc()).limit(6).all()

	return render_template('notification/notification.html',
							VLOOKUP=mktreport.VLOOKUP,
							Notification=Notification,
							EventCode=EventCode,
							CurrentUser=CurrentUser)
