from app.mktcore.wtfimports import *
from .models import *
#class AutoID form
class FRM_AUTOID(exform):
	#AppName=TextField(requiredlabel("Application Name","*"), [validators.Required()])
	Description	=	TextField(requiredlabel("Description","*"), [validators.Required()])
	PreFix      =   TextField("Prefix")
	CBranch		=   SelectField('Branch Prefix',
							 choices=[('N','No'),('Y', 'Yes')],
							 coerce=str)

	CustAC		=   SelectField('Customer/AC Number',
							 choices=[('','---None---'),('CustAC', 'Customer/AC Number')],
							 coerce=str)
	LastNum     =   TextField("Last Number", default = 0)
	StartNum	=	TextField("Start Number", default = 0)
	Cate1		=   SelectField('Cate 1',
							 choices=[('','---None---'),('daySequencial', 'Sequencial Number by Day'),
							 ('Sequencial','Sequencial Number'), ('YY','YY'), ('YYYY','YYYY'), 
							 ('MM', 'MM'), ('DD', 'DD'), ('juLienDate', 'Julien Date')],
							 coerce=str)

	Cate2		=   SelectField('Cate 2',
							 choices=[('','---None---'),('daySequencial', 'Sequencial Number by Day'),
							 ('Sequencial','Sequencial Number'), ('YY','YY'), ('YYYY','YYYY'), 
							 ('MM', 'MM'), ('DD', 'DD'), ('juLienDate', 'Julien Date')],
							 coerce=str)

	Cate3		=   SelectField('Cate 3',
							 choices=[('','---None---'),('daySequencial', 'Sequencial Number by Day'),
							 ('Sequencial','Sequencial Number'), ('YY','YY'), ('YYYY','YYYY'), 
							 ('MM', 'MM'), ('DD', 'DD'), ('juLienDate', 'Julien Date')],
							 coerce=str)

	Cate4		=   SelectField('Cate 4',
							 choices=[('','---None---'),('daySequencial', 'Sequencial Number by Day'),
							 ('Sequencial','Sequencial Number'), ('YY','YY'), ('YYYY','YYYY'), 
							 ('MM', 'MM'), ('DD', 'DD'), ('juLienDate', 'Julien Date')],
							 coerce=str)
	Format	=	TextField("Sequencial Format")



	

	


