from app.mktcore.mdlimports import *

class MKT_AUTOID(exmodel):
	ID			=	db.Column(db.String(35),primary_key=True)
	Description	=	db.Column(db.String(50))
	PreFix		=	db.Column(db.String(3))
	CustAC		=	db.Column(db.String(20))
	LastNum		=	db.Column(db.Integer, default = 0)
	LastDay		=	db.Column(db.Integer, default = 0)
	StartNum	=	db.Column(db.Integer, default = 0)
	Cate1		=	db.Column(db.String(20))
	Cate2		=	db.Column(db.String(20))
	Cate3		=	db.Column(db.String(20))
	Cate4		=	db.Column(db.String(20))
	Format		= 	db.Column(db.String(20))
	CBranch		=	db.Column(db.String(20))

class MKT_AUTOID_INAU(exmodel):
	ID			=	db.Column(db.String(35),primary_key=True)
	Description	=	db.Column(db.String(50))
	PreFix		=	db.Column(db.String(3))
	CustAC		=	db.Column(db.String(20))
	LastNum		=	db.Column(db.Integer, default = 0)
	LastDay		=	db.Column(db.Integer, default = 0)
	StartNum	=	db.Column(db.Integer, default = 0)
	Cate1		=	db.Column(db.String(20))
	Cate2		=	db.Column(db.String(20))
	Cate3		=	db.Column(db.String(20))
	Cate4		=	db.Column(db.String(20))
	Format		= 	db.Column(db.String(20))
	CBranch		=	db.Column(db.String(20))

class MKT_AUTOID_HIST(exmodel):
	ID			=	db.Column(db.String(35),primary_key=True)
	Description	=	db.Column(db.String(50))
	PreFix		=	db.Column(db.String(3))
	CustAC		=	db.Column(db.String(20))
	LastNum		=	db.Column(db.Integer, default = 0)
	LastDay		=	db.Column(db.Integer, default = 0)
	StartNum	=	db.Column(db.Integer, default = 0)
	Cate1		=	db.Column(db.String(20))
	Cate2		=	db.Column(db.String(20))
	Cate3		=	db.Column(db.String(20))
	Cate4		=	db.Column(db.String(20))
	Format		= 	db.Column(db.String(20))
	CBranch		=	db.Column(db.String(20))

class MKT_RECORD_LOCK(exmodel):
	RECORDID=db.Column(db.String(35),primary_key=True)
	AppName=db.Column(db.String(35))

class MKT_SEQUENCE(exmodel):
	ID 			=	db.Column(db.String(35), primary_key=True)
	Current		=	db.Column(db.Integer, default=0)
	Increment 	=	db.Column(db.Integer, default=1)