# -*- coding: utf-8 -*-
import sys
from flask 							import flash, session, redirect, url_for
from app.mktcore.imports 			import *
from .forms 						import *
from app.LoanApplication.models  	import MKT_GUARANTOR, MKT_LOAN_COLLATERAL
from app.RepaymentSchedule.models 	import MKT_REP_SCHEDULE
from app.PD.models 					import MKT_PAST_DUE, MKT_PD_DATE, MKT_PD_DATE_HIST
from app.Journal.models 			import MKT_JOURNAL
from app.AccEntry.models 			import MKT_ACC_ENTRY
from sqlalchemy 					import *
from app.mktcore.session 			import *
from decimal 						import *

import app.tools.mktmoney 			as mktmoney
import app.Customer.centre 			as customer
import app.tools.mktdate 			as mktdate
import app.tools.mktsetting 		as mktsetting
import app.tools.mktmessage 		as mktmessage
import app.tools.mktparam 			as mktparam
import app.tools.mktloanamendment	as mktloanamendment
import app.tools.mktpdcollection	as mktpdcollection

registerCRUD(admin, '/LoanContract', 'LoanContract', FRM_LOAN_CONTRACT, [MKT_LOAN_CONTRACT, MKT_LOAN_CHARGE, MKT_GUARANTOR, MKT_LOAN_COLLATERAL, MKT_LOAN_CO_BORROWER])

@app.route("/Morakot/LoanCentre/", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getLoanTermination():
	try:

		reload(sys)
		sys.setdefaultencoding('utf-8')

		ID = request.args.get('ID').strip() if 'ID' in request.args else ""
		Loan = MKT_LOAN_CONTRACT.query.get(ID)

		SystemDate = str(mktdate.getBankDate())

		PD = MKT_PAST_DUE.query.get("PD" + str(ID))
		TotODAmount 	= 0
		TotPrincipalDue = 0
		TotInterestDue 	= 0
		TotPenaltyDue 	= 0
		TotChargeDue 	= 0
		PreTermination	= 0
		Schedule 		= None
		BaseCurrencyObj = mktmoney.getCurrencyObj(mktsetting.getAccSetting().BaseCurrency)
		if Loan:
			PreTermination	= mktloanamendment.getTerminationPenalty(Loan.ID, Loan.Term, Loan.Installment, Loan.Frequency, Loan.FreqType, Loan.Disbursed, Loan.OutstandingAmount)
			PreTermination  = PreTermination[1]
			RepScheduleTable= MKT_REP_SCHEDULE
			Schedule = MKT_REP_SCHEDULE.query.\
					   order_by(MKT_REP_SCHEDULE.No.asc()).\
					   filter(MKT_REP_SCHEDULE.LoanID == ID).\
					   all()
		else:
			''' get loan info when loan was moved to history, get latest version '''
			RepScheduleTable= MKT_REP_SCHEDULE_HIST
			if ID.find('@') == -1: # if id is not hostory record id we will create history record id
				Count 	= 	MKT_LOAN_CONTRACT_HIST.query.\
							filter(MKT_LOAN_CONTRACT_HIST.ID.ilike(ID + '@%')).count()
				
				if Count:
					ID  = '%s@%s'%(ID,Count-1) # create history record id
			
			SplittedID 	= 	ID.split('@')
			if len(SplittedID) == 2:	# SplittedID lenght = 2 mean it is id of history record
				LoanID 	= 	SplittedID[0] # Loan ID
				Version = 	SplittedID[1] # version of loan history
				Loan 		= 	MKT_LOAN_CONTRACT_HIST.query.get(ID)
				Schedule 	= 	MKT_REP_SCHEDULE_HIST.query.\
						   		order_by(MKT_REP_SCHEDULE_HIST.No.asc()).\
						   		filter(MKT_REP_SCHEDULE_HIST.LoanID == LoanID).\
						   		filter(MKT_REP_SCHEDULE_HIST.ID.like('%@' + Version) ).\
						   		all()

		if PD:
			TotODAmount 	= PD.TotODAmount
			TotPrincipalDue = PD.TotPrincipalDue
			TotInterestDue 	= PD.TotInterestDue
			TotPenaltyDue 	= PD.TotPenaltyDue
			TotChargeDue 	= PD.TotChargeDue

		RepSchedule =	RepScheduleTable.query.\
						filter(RepScheduleTable.LoanID == ID).\
						filter(RepScheduleTable.RepStatus != "0")

		Installment 	= 	int(RepSchedule.count()) + 1
		Installment 	=	ID + str(Installment)
		NextInstallment =	RepScheduleTable.query.get(Installment)
		CurrentInterest =	0
		if NextInstallment:
			CurrentInterest = NextInstallment.Interest

		if ID and not Loan:
			flash(msg_warning + " Loan contract not found.")

		return render_template("schedule/loaninfo.html",
								ID 					=	ID,
								Loan 				=	Loan,
								toMoney 			=	mktmoney.toMoney,
								getCurrencyObj 		=	mktmoney.getCurrencyObj,
								getCurrencySymbol 	=	mktmoney.getCurrencySymbol,
								float 				=	float,
								int 				=	int,
								str 				=	str,
								CurrentInterest 	=	CurrentInterest,
								Schedule 			=	Schedule,
								TotODAmount 		=	TotODAmount,
								PreTermination 		=	PreTermination,
								TotPrincipalDue 	=	TotPrincipalDue,
								TotInterestDue 		=	TotInterestDue,
								TotPenaltyDue 		=	TotPenaltyDue,
								TotChargeDue 		=	TotChargeDue,
								lookupTable 		=	customer.lookupTable,
								SystemDate 			=	SystemDate,
								getLang 			= 	mktmessage.getLang,
								getPenaltyByDate 	=	mktpdcollection.getPenaltyByDate,
								BaseCurrencyObj 	= 	BaseCurrencyObj
							)

	except:
		raise