from flask 							import g, flash
from wtforms 						import DecimalField, IntegerField
from wtforms.validators 			import StopValidation
from app.mktcore.wtfimports 		import *
from .models 						import *
import time
from decimal 						import *
from .. 							import app, db
from datetime 						import datetime, date, timedelta
from sqlalchemy 					import *

import app.tools.mktloanapplication 		as mktloanapp
import app.tools.mktcustomer 				as mktcustomer
import app.tools.mktaccount					as mktaccount
import app.tools.user 						as mktuser
import app.tools.mktdate	 				as mktdate
import app.tools.mktofficer 				as mktofficer
import app.tools.mktmoney 					as mktmoney
import app.tools.mktsetting 				as setting
import app.tools.mktaccounting 				as mktaccounting
import app.tools.mktloan 					as mktloan
import app.tools.loantools.nonworkingday 	as mktDay
import app.tools.loantools.rescheduletools 	as mktreschedule
import app.tools.mktholiday 				as mktHoliday
import app.tools.mktkey 					as mktkey
import app.tools.mktpdcollection			as mktpd
import app.tools.mktrepayment 				as mktrepayment
import app.tools.mktaudit 					as mktaudit
import app.tools.mktnotification 			as mktnotification
import app.tools.mktcharge 					as mktcharge


from app.tools.mktvb 				import *
from app.tools.mktroute 			import *
from app.Account.models 			import MKT_ACCOUNT
from app.LoanProduct.models 		import MKT_LOAN_PRODUCT
from app.LoanPurpose.models 		import MKT_LOAN_PURPOSE
from app.VB.models 					import MKT_VB
from app.Officer.models 			import MKT_OFFICER
from app.Branch.models 				import MKT_BRANCH
from app.Charge.models 				import MKT_CHARGE
from app.LoanApplication.models 	import MKT_LOAN_APPLICATION, MKT_LOAN_COLLATERAL, MKT_GUARANTOR
from app.InterestRate.models 		import MKT_INTEREST_RATE
from app.ChargeRate.models 			import MKT_CHARGE_RATE
from app.LoanRuleDetail.models		import MKT_LOAN_RULE_DE
from app.Customer.models 			import MKT_CUSTOMER
from app.SourceOfFund.models 		import MKT_SOURCE_OF_FUND
from app.Currency.models 			import MKT_CURRENCY
from app.AssetClass.models 			import MKT_ASSET_CLASS
from app.Collateral.models 			import MKT_COLLATERAL
from app.RepaymentSchedule.models 	import *
from app.ScheduleDefine.models 		import *
from app.LoanApplication.forms 		import getLoadCollateral

def getRecord():
	try:
		result = MKT_LOAN_CONTRACT.query.get(g.formID)
		if not result:
			result = MKT_LOAN_CONTRACT_INAU.query.get(g.formID)
			if result:
				return [result.Account]

			else:
				return []
		else:
			return [result.Account]

	except:
		raise

def getRecordLoanApp():
	try:
		result = MKT_LOAN_CONTRACT.query.get(g.formID)
		if not result:
			result = MKT_LOAN_CONTRACT_INAU.query.get(g.formID)
			if result:
				return [result.LoanApplicationID]

			else:
				return []
		else:
			return [result.LoanApplicationID]

	except:
		raise

def getLoanAppInfo(LoanAppID):
	try:
		query = MKT_LOAN_APPLICATION.query.get(LoanAppID)
		return query
	except:
		raise

@app.route("/Morakot/LoanApplicationID", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def LoanApplicationID():
	return mktloanapp.getSearchLoanApplication()

@app.route("/Morakot/ContractCustomerID", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def ContractCustomerID():
	return mktcustomer.getSearchCustomer()

@app.route("/Morakot/ContractAccountID", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def ContractAccountID():
	return mktaccount.getSearchAccount()

@app.route("/Morakot/ContractVB", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def ContractVB():
	return getSearchVB()

@app.route("/Morakot/ContractOfficerID", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def ContractOfficerID():
	return mktofficer.getSearchOfficer()

@app.route("/Morakot/LoanApplicationInfo", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getLoanApplicationInfo():
	try:
		LoanAppID 		= request.args.get('LoanAppID')
		Amount 			= "0"
		Term 			= "0"
		FreqType 		= "1"
		Frequency 		= "0"
		FreqDesc 		= "1.Monthly"
		Installment 	= 0
		LNCustomerID 	= ""
		LoanProduct 	= ""
		Category 		= ""
		InterestRate 	= ""
		Currency 		= ""
		ProDesc 		= ""
		OnOff 			= ""
		OnOff1 			= ""
		Group 			= ""
		VBID 			= ""
		VB_Desc 		= ""
		CustomerName 	= ""
		LoanPurpose 	= ""
		PurDesc 		= ""
		SourceOfFund 	= ""
		SouDesc 		= ""
		MoreThanOneYear = "N"
		ContractOfficerID = ""
		OfficerName 	= ""

		Account 		= 	""
		AccountName 	= 	""

		query = MKT_LOAN_APPLICATION.query.get(LoanAppID)
		if query:

			CurrencyObj 	= MKT_CURRENCY.query.get(query.Currency)
			Amount 			= str(mktmoney.toMoney(float(query.Amount), CurrencyObj))
			Term 			= str(query.Term) if query.Term else "0"
			
			if int(Term) > 12:
				MoreThanOneYear = "Y"

			Frequency 		= str(query.Frequency)
			Installment 	= str(query.Installment)
			LNCustomerID 	= query.LNCustomerID

			Cus = MKT_CUSTOMER.query.get(LNCustomerID)
			if Cus:
				CustomerName 	= 	LNCustomerID + " - " + Cus.LastNameEn + " " + Cus.FirstNameEn

				AccountID 		= 	MKT_ACCOUNT.query.\
									filter(MKT_ACCOUNT.CustomerList == LNCustomerID)

				if AccountID.count() == 1:
					for item in AccountID:
						Account 		= 	item.ID
						AccountName 	=	item.ID

				ContractOfficerID 	= Cus.Officer
				Officer = MKT_OFFICER.query.get(ContractOfficerID)
				if Officer:
					OfficerName = "%s - %s - %s"%(Officer.ID,Officer.LastName,Officer.FirstName)

			LoanProduct  	= query.LoanProduct
			ProDesc 		= MKT_LOAN_PRODUCT.query.get(LoanProduct).Description
			Category 		= query.Category
			InterestRate 	= str(query.InterestRate)
			InterestRate 	= InterestRate.split()
			InterestRate 	= InterestRate[0]
			Currency 		= query.Currency

			# Guarantor 		= MKT_GUARANTOR.query.filter(MKT_GUARANTOR.ID == LoanAppID).all()
			# if Guarantor:
			# 	OnOff 		= "display: none;"

			# Collateral 		= MKT_LOAN_COLLATERAL.query.filter(MKT_LOAN_COLLATERAL.ID == LoanAppID).all()
			# if Collateral:
			# 	OnOff1 		= "display: none;"

			Group 			= query.Group
			
			VBID 			= query.VBID
			if not VBID:
				VBID 	=	"__None"
				VB_Desc =	"Select ContractVB"
			else:
				VB = MKT_VB.query.get(VBID)
				if VB:
					VB_Desc = VB.Description

			FreqType = query.FreqType
			if FreqType == "2":
				FreqDesc = "2.Weekly"
			else:
				FreqDesc = "1.Monthly"

			LoanPurpose = query.LoanPurpose
			if not LoanPurpose:
				LoanPurpose = 	"__None"
				PurDesc 	= 	"--None--"
			else:
				LP = MKT_LOAN_PURPOSE.query.get(LoanPurpose)
				if LP:
					PurDesc = LP.Description

			SourceOfFund = query.SourceOfFund
			if not SourceOfFund:
				SourceOfFund 	=	"__None"
				SouDesc 		=	"--None--"
			else:
				SF = MKT_SOURCE_OF_FUND.query.get(SourceOfFund)
				if SF:
					SouDesc = SF.Description


		return jsonify(Amount=Amount, Installment=Installment, Term=Term, Frequency=Frequency,
					   LNCustomerID=LNCustomerID, CustomerName=CustomerName,
					   LoanProduct=LoanProduct, ProDesc=ProDesc, Category=Category,
					   InterestRate=InterestRate, Currency=Currency, OnOff=OnOff,
					   Group=Group, VBID=VBID, VB_Desc=VB_Desc, FreqType=FreqType,
					   FreqDesc=FreqDesc, LoanPurpose=LoanPurpose, PurDesc=PurDesc,
					   SourceOfFund=SourceOfFund, SouDesc=SouDesc, MoreThanOneYear=MoreThanOneYear,
					   Account=Account, AccountName=AccountName,
					   ContractOfficerID=ContractOfficerID, OfficerName=OfficerName, OnOff1=OnOff1)
	except Exception, e:
		raise

# @app.route("/Morakot/ContractCustomerInfo", methods=['GET'])
# def getContractCustomerInfo():
# 	try:
# 		CustomerID 	= request.args.get('CustomerID')
# 		dic 		= {}
# 		query 		= MKT_ACCOUNT.query.\
# 					  filter(MKT_ACCOUNT.CustomerList == CustomerID).\
# 					  filter(MKT_ACCOUNT.AccStatus == "O").\
# 					  all()
# 		if query:
# 			for row in query:
# 				# dic[row.ID] = row.ID + "  - " + row.Currency+ " " + row.AccName
# 				dic[row.ID] = row.ID

# 		return jsonify(results=dic)
# 	except:
# 		raise

@app.route("/Morakot/ContractCurrencyInfo", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getContractCurrencyInfo():
	try:

		AccountID 		= request.args.get("AccountID")
		LoanProduct 	= request.args.get("LoanProduct")
		query 			= MKT_ACCOUNT.query.get(AccountID)
		Currency 		= ""
		InterestRate 	= "0"
		LoanAppID 		= request.args.get('LoanAppID') if 'LoanAppID' in request.args else ""

		if query:
			Currency 		= str(query.Currency)
			RateID 			= LoanProduct + Currency
			rate 			= MKT_INTEREST_RATE.query.get(RateID)
			InterestRate	= str(rate.Rate) if rate else "0"
			InterestRate 	= InterestRate.split()[0]

		LC_App = getLoanAppInfo(LoanAppID)
		if LC_App:
			InterestRate 	= str(LC_App.InterestRate)
			InterestRate 	= InterestRate.split()
			InterestRate 	= InterestRate[0]

		return jsonify(Currency=Currency, InterestRate=InterestRate)
	except:
		raise

@app.route("/Morakot/LoanProductInfo", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getLoanProductInfo():
	try:
		ProductID 		= request.args.get("LoanProduct")
		Currency 		= request.args.get("Currency")
		Category 		= ""
		InterestRate 	= ""
		LoanAppID 		= request.args.get('LoanAppID') if 'LoanAppID' in request.args else ""
		
		row = MKT_LOAN_PRODUCT.query.get(ProductID)
		if row:
			Category 		= str(row.LNCategory)
			RateID 			= str(row.InterestKey) + str(Currency)
			rate 			= MKT_INTEREST_RATE.query.get(RateID)
			InterestRate 	= str(rate.Rate) if rate else "0"
			InterestRate 	= InterestRate.split()
			
			if len(InterestRate) > 1:
				InterestRate 	= InterestRate[1]
			else:
				InterestRate 	= InterestRate[0]

		LC_App = getLoanAppInfo(LoanAppID)
		if LC_App:
			InterestRate 	= str(LC_App.InterestRate)

		return jsonify(Category=Category, InterestRate=InterestRate)
	except:
		raise

@app.route("/Morakot/ChargeKeyInfo", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getChargeKeyInfo():
	try:
		ChargeKeyID = request.args.get("ChargeKeyID")
		ChargeKey 	= ChargeKeyID
		Currency 	= request.args.get("Currency")
		RateFixed 	= ""
		Charge 		= ""
		ChargePerInstallment= 0
		ChargePerDay	= 0
		ChargeLastBooked	= 0
		ChargeEarned 	= 0
		ChargeUnearned 	= 0
		ChargeKeyID = str(ChargeKeyID) + str(Currency)
		print "%s" %ChargeKeyID
		row 		= MKT_CHARGE_RATE.query.get(ChargeKeyID)
		if row:
			RateFixed 	= str(row.RateFixed) if row else ""
			Charge 		= str(row.Value) if row else "0"

		return jsonify(RateFixed=RateFixed, Charge=Charge, ChargeLastBooked=str(ChargeLastBooked), ChargePerDay=str(ChargePerDay), ChargePerInstallment=str(ChargePerInstallment), ChargeEarned=str(ChargeEarned), ChargeUnearned=str(ChargeUnearned))
	except:
		raise

@app.route("/Morakot/SetOutStandingAmount", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def setOutStandingAmount():
	Amount = request.args.get("Disbursed")
	return jsonify(Amount=str(Amount))

@app.route("/Morakot/CheckMoreThanOneYear", methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def getMoreThanOneYear():
	Term = request.args.get("Term")
	if int(Term) > 12:
		MoreThanOneYear = "Y"
	else:
		MoreThanOneYear = "N"

	return jsonify(MoreThanOneYear=str(MoreThanOneYear))

def loadAccount():
	try:
		if request.method == 'POST':
			Account = request.form["ContractCustomerID"]
			return MKT_ACCOUNT.query.filter_by(CustomerList=Account).all()
		else:
			if len(getRecord()) > 0:
				return MKT_ACCOUNT.query.filter_by(ID=getRecord()[0]).filter(MKT_ACCOUNT.AccStatus != 'C').all()
			else:
				return MKT_ACCOUNT.query.filter_by(ID='0').filter(MKT_ACCOUNT.AccStatus != 'C').all()
	except:
		raise

def loadLoanProduct():
	return MKT_LOAN_PRODUCT.query

def laodLoanPurpose():
	return MKT_LOAN_PURPOSE.query

def loadVB():
	return MKT_VB.query

def loadOfficer():
	return mktofficer.loadOfficer()

def loadCharge():
	return MKT_CHARGE.query

def loadSourceOfFund():
	return MKT_SOURCE_OF_FUND.query

def loadAssetClass():
	Ass = MKT_ASSET_CLASS.query.\
		  order_by(MKT_ASSET_CLASS.ID.asc()).\
		  first()
	Asset = ""
	if Ass:
		Asset = Ass.ID

	return str(Asset)

def loanBankDate():
	return mktdate.getBankDate()

# def loadCollateral():
# 	return MKT_COLLATERAL.query
# 	# Branch 		= 	mktuser.getBranch(session["ChangeBranch"]).ID if mktuser.getBranch(session["ChangeBranch"]).ID else ""
# 	# AsGurantor 	= 	MKT_CUSTOMER.query.\
# 	# 	  			filter(MKT_CUSTOMER.AsGurantor == 'Y').\
# 	# 	  			filter(MKT_CUSTOMER.Branch == Branch)

# 	# return AsGurantor

def loadGuarantor():
	Branch 		= 	mktuser.getCurrentBranch()
	AsGurantor 	=	MKT_CUSTOMER.query.\
		  			filter(MKT_CUSTOMER.AsGurantor == 'Y').\
		  			filter(MKT_CUSTOMER.Branch == Branch).\
		  			all()
	return AsGurantor

def loadCoBorrower():
	Branch = mktuser.getCurrentBranch()
	CustomerObj = 	MKT_CUSTOMER.query.\
					filter(MKT_CUSTOMER.Branch == Branch).\
					order_by(MKT_CUSTOMER.ID.asc())
	return CustomerObj

def loadCollateral():
	Customer = request.form["ContractCustomerID"] if request.method == 'POST' else ""
	if Customer:
		return MKT_COLLATERAL.query.filter(MKT_COLLATERAL.CustomerID == Customer)
	else:
		return MKT_COLLATERAL.query

def custom_FirstCollectionDate(form,field):
	FirstCollection = request.form["FirstCollectionDate"] if request.method == 'POST' else ""
	ValueDate 		= request.form["ValueDate"] if request.method == 'POST' else ""
	# FirstCollection = form.FirstCollectionDate.data

	LoanProduct 	= form.LoanProduct.data
	if FirstCollection and ValueDate:
		CheckFormatDate = mktdate.isDateISO(FirstCollection)
		if not CheckFormatDate:
			raise ValidationError("Incorrect date format, should be YYYY-MM-DD")
			
		ValueDate 		= datetime.strptime(str(ValueDate),'%Y-%m-%d').date()
		FirstCollection = datetime.strptime(str(FirstCollection),'%Y-%m-%d').date()

		Holiday 	= mktHoliday.getHoliday()
		check 		= mktDay.isNonWorkingDay(FirstCollection, Holiday)
		if check:
			raise ValidationError("Date must be different holiday.")

		if FirstCollection <= ValueDate:
			raise ValidationError("Date must be bigger than value date.")

	else:
		
		# clear out processing errors
		field.errors[:] = []
		
		if hasattr(LoanProduct,'ID'):
			LoanProduct = LoanProduct.ID
			LoanProductObj = MKT_LOAN_PRODUCT.query.get(LoanProduct)
			if LoanProductObj:
				BaseDateKey = LoanProductObj.BaseDateKey
				if BaseDateKey == "3":
					raise ValidationError("This field is required.")
		
		# Stop further validators running
		raise StopValidation()

class FRM_LOAN_CONTRACT(exform):

	LoanApplicationID 	= 	HiddenField(getLanguage("Loan Application"))
	ContractCustomerID 	= 	HiddenField(requiredlabel(getLanguage("Customer"), "*"), [validators.Required()])
	Account 			= 	QuerySelectField(requiredlabel(getLanguage("Drawdown Account"), "*"),
								query_factory=loadAccount,
								get_label=u'ID',
								allow_blank=False,
								blank_text=u'--%s--' %getLanguage("None"),
								validators=[validators.Required()]
							)

	Currency 		= 	TextField(requiredlabel(getLanguage("Currency"), "*"))
	ApprovedAmount	= 	DecimalField("Approved Amount", default=0)
	Disbursed 		= 	DecimalField(requiredlabel(getLanguage("Disbursed"), "*"), [validators.Required()])
	Amount 			= 	TextField(requiredlabel(getLanguage("Loan Balance"), "*"), [validators.Required()], default=0)
	OutstandingAmount 	=	TextField(requiredlabel(getLanguage("Loan Outstanding"), "*"), [validators.Required()], default=0)
	ValueDate 		= 	DateField(requiredlabel(getLanguage("Value Date"), "*"), [validators.Required()], default=loanBankDate)
	FirstCollectionDate =	DateField("First Collection Date", [custom_FirstCollectionDate])
	Cycle 			= 	IntegerField(requiredlabel(getLanguage("Cycle"), "*"), [validators.Required()], default=1)
	MaturityDate 	= 	DateField(getLanguage("Maturity Date"), [validators.Optional()])

	LoanProduct 	= 	QuerySelectField(requiredlabel(getLanguage("Loan Product"), "*"),
							query_factory=loadLoanProduct,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--%s--' %getLanguage("None"),
							validators=[validators.Required()]
						)

	Category 		= 	TextField(requiredlabel(getLanguage("Category"), "*"), [validators.Required()])
	InterestRate 	= 	TextField(requiredlabel(getLanguage("Interest Rate"), "*"), [validators.Required()])
	IRR 			= 	TextField("IRR")
	FreqType 		= 	SelectField(requiredlabel(getLanguage("Frequency Type"), "*"),
							choices=[('1', '1. %s' %getLanguage('Monthly')), ('2', '2. %s' %getLanguage("Weekly"))],
							coerce=str,
							validators=[validators.Required()]
						)

	Frequency 		= 	IntegerField(requiredlabel(getLanguage("Frequency"), "*"), [validators.Required()], default=1)
	Term 			= 	IntegerField(requiredlabel("%s (%s)" %(getLanguage("Term"), getLanguage("Month")), "*"), [validators.Required()])
	Installment 	= 	IntegerField(requiredlabel(getLanguage("Installment"), "*"), [validators.Required()])

	DeliqMode 		= 	SelectField(requiredlabel(getLanguage('Deliquency Mode'), '*'),
							choices=[('3', '3. %s' %getLanguage("Semi-Automatic")),
									 ('2', '2. %s' %getLanguage("Automatic")),
									 ('1', '1. %s' %getLanguage("Manual"))],
							coerce=str,
							validators=[validators.Required()]
						)

	LoanPurpose 	= 	QuerySelectField(getLanguage("Loan Purpose"),
							query_factory=laodLoanPurpose,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--%s--' %getLanguage("None")
						)

	ContractVB 		= 	HiddenField(getLanguage("Village Bank"))
	Group 			= 	IntegerField(getLanguage("Group"), [validators.Optional()])

	SourceOfFund 	= 	QuerySelectField(getLanguage("Source of Fund"),
							query_factory=loadSourceOfFund,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--%s--' %getLanguage("None")
						)

	LoanType 		= 	SelectField(requiredlabel(getLanguage('Loan Type'), '*'),
							choices=[('N', '%s' %getLanguage("Normal")),
									 ('R', '%s' %getLanguage("Restructured"))],
							coerce=str,
							default='N'
						)
	
	AssetClass 		= 	TextField(requiredlabel(getLanguage("Asset Class"), "*"), [validators.Required()], default=loadAssetClass)

	MoreThanOneYear = 	TextField(requiredlabel(getLanguage("More Than One Year"), "*"), [validators.Required()], default='N')

	TotalInterest 	= 	DecimalField(getLanguage("Total Interest"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Interest"))
	IntIncEarned 	= 	DecimalField(getLanguage("Interest Income Earned"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Interest"))
	AccrIntPerDay 	= 	DecimalField(getLanguage("Accrued Interest Per Day"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Interest"))
	AccrInterest 	= 	DecimalField(getLanguage("Accrued Interest Receivable"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Interest"))
	AccrCurrentInt 	= 	DecimalField(getLanguage("Accr Current Installment"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Interest"))
	AccrIntCurrMonth = 	DecimalField(getLanguage("Accr Int Current Month"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Interest"))
	AccrIntPreMonth = 	DecimalField(getLanguage("Accr Int Previous Month"), [validators.Optional()], default=0, description="1-%s" %getLanguage("Interest"))
	Suspend 		= 	TextField(getLanguage("Suspend"), default='N', description="1-%s" %getLanguage("Interest"))
	DisbursedStat 	= 	TextField(requiredlabel(getLanguage("Disbursed Status"), "*"), [validators.Required()], default='N')

	# ContractOfficerID 	= 	QuerySelectField(requiredlabel("Officer", "*"),
	# 							query_factory=loadOfficer,
	# 							get_label=u'FirstName',
	# 							allow_blank=False,
	# 							blank_text=u'None',
	# 							validators=[validators.Required()]
	# 						)
	ContractOfficerID 	= 	QuerySelectField(requiredlabel(getLanguage('Officer'),'*'),
							query_factory=loadOfficer,
							get_label=lambda a: a.ID + " - " + a.LastName + " " +a.FirstName,
							allow_blank=True,
							blank_text=u'--None--',
							validators=[validators.Required()]
						)

	ChargeKey 	= 	QuerySelectField(getLanguage("Charge Key"),
						query_factory=loadCharge,
						get_label=u'Description',
						allow_blank=True,
						blank_text=u'--None--',
						description=u'2-%s' %getLanguage("Loan Charge")
					)
	# ChargeKey 			=	TextField(getLanguage("Charge Key"), description=u'2-%s' %getLanguage("Loan Charge"))
	# ChargeDescription 	=	TextField(getLanguage("Description"), description=u'2-%s' %getLanguage("Loan Charge"))
	RateFixed 			= 	TextField(getLanguage("Rate") + "/" + getLanguage("Fixed"), description=u'2-%s' %getLanguage("Loan Charge"))
	Charge 				= 	DecimalField(getLanguage("Charge"), [validators.Optional()], description=u'2-%s' %getLanguage("Loan Charge"))
	ChargePerInstallment= 	TextField("Charge Per Installment", default=0, description=u'2-%s' %getLanguage("Loan Charge"))
	ChargePerDay		=   TextField("Charge Per Day", default=0, description=u'2-%s' %getLanguage("Loan Charge"))
	ChargeLastBooked	=   TextField("Charge Last Booked", default=0, description=u'2-%s' %getLanguage("Loan Charge"))
	
	ChargeEarned 		= 	TextField("Charge Earned", default=0, description=u'2-%s' %getLanguage("Loan Charge"))
	ChargeUnearned 		= 	TextField("Charge Unearn", default=0, description=u'2-%s' %getLanguage("Loan Charge"))

	# Guarantor 	= 	HiddenField("Guarantor", description="3-Guarantor")
	Guarantor 		= 	QuerySelectField(getLanguage("Guarantor"),
							query_factory=loadGuarantor,
							get_label=lambda a: a.ID + " - " + a.LastNameEn + " " +a.FirstNameEn,
							allow_blank=True,
							blank_text=u'--None--',
							description="3-%s" %getLanguage("Guarantor")
						)

	Penalty 		=	TextField(getLanguage("Penalty"), default='Y')

	Collateral 		= 	QuerySelectField(getLanguage("Collateral"),
							query_factory=loadCollateral,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--%s--' %getLanguage("None"),
							description="4-%s" %getLanguage("Collateral")
						)

	CoBorrowerID 	= 	QuerySelectField(getLanguage("Co-Borrower"),
							query_factory=loadCoBorrower,
							get_label=lambda a: a.ID + " - " + a.LastNameEn + " " +a.FirstNameEn,
							allow_blank=True,
							blank_text=u'--%s--' %getLanguage("None"),
							description="5-%s" %getLanguage("Co-Borrower")
						)
	# CoBorrowerID 		= 	TextField(getLanguage("Co-Borrower"), description="5-%s" %getLanguage("Co-Borrower"))
	# CoBorrowerName 		= 	TextField("Co-Borrower Name", description="5-%s" %getLanguage("Co-Borrower"))

	RelationIndicator 	= 	SelectField("Relation Indicator",
							choices=[('__None', '--None--'), ('1', 'Spouse'), ('2', 'Son/Daughter'), ('3', 'Relative'), ('4', 'Parent'), ('5', 'Friend')],
							coerce=str,
							default='__None',
							description="5-%s" %getLanguage("Co-Borrower")
						)

	def validate_Guarantor(form, field):
		try:

			if request.form['Guarantor']:
				
				Guarantor 		= 	[]
				controls_list 	= 	request.form
				ListGuarantor	= 	[item for item in controls_list if "Guarantor" in item]
				ListGuarantor.append("ContractCustomerID")
				ListGuarantor.sort()
				TotalGuarantor =	len(ListGuarantor)

				for nub in range(0,TotalGuarantor):
					Guarantor.append(request.form[ListGuarantor[nub]])

				DuplicateAcc   = [x for x in Guarantor if Guarantor.count(x) > 1]
				if DuplicateAcc:
					raise ValidationError("Must not be the same as customer#%s"%DuplicateAcc[0])
				
		except Exception, e:
			raise ValidationError("%s" %e)
		

	def validate_RateFixed(form, field):
			
		ChargeKey 	=	request.form['ChargeKey']
		RateFixed 	=	request.form['RateFixed']
		
		if ChargeKey and ChargeKey != '__None':
			if not RateFixed:
				raise ValidationError('This field is required.')

	def validate_Charge(form, field):

		ChargeKey 	=	request.form['ChargeKey']
		Charge 		=	request.form['Charge']

		if ChargeKey :
			if not Charge:
				raise ValidationError("This field is required.")

	def validate_ContractCustomerID(form, field):
		try:

			CustomerID = form.ContractCustomerID.data
			mktcustomer.checkAge(CustomerID)

			CusObj 	=	MKT_CUSTOMER.query.get(CustomerID)

			if CusObj:

				Block 	=	CusObj.Block
				if Block == 'Y':
					BlockReason 	=	CusObj.BlockReason
					raise ValidationError("The customer %s was blocked, reason: %s." %(CustomerID, BlockReason))

		except:
			raise

	def validate_Disbursed(form, field):
		try:
			Amount 		= Decimal(form.Amount.data)
			Disbursed 	= Decimal(form.Disbursed.data)
			Currency 	= form.Currency.data
			LoanProduct = request.form['LoanProduct']
			Account 	= request.form['Account']
			# Call function to check Maximum and Minimum Disbursed Amount
			mktaccounting.checkMaxMinBalance(Account, Currency, Amount, 'Cr')
			mktloanapp.checkMaxMinAmount(Disbursed, Amount, Currency, LoanProduct)

		except:
			raise

	def validate_ValueDate(form, field):
		try:

			Holiday 	= mktHoliday.getHoliday()
			ValueDate 	= request.form['ValueDate']

			CheckFormatDate = mktdate.isDateISO(ValueDate)
			if not CheckFormatDate:
				raise ValidationError("Incorrect date format, should be YYYY-MM-DD")
				
			ValueDate 	= datetime.strptime(str(ValueDate),'%Y-%m-%d').date()
			check 		= mktDay.isNonWorkingDay(ValueDate, Holiday)
			
			if check:
				raise ValidationError("Value date must be different holiday.")

			systemDate 	= 	mktdate.getBankDate()
			StrSysteDate=	str(systemDate).split("-")
			StrValueDate=	str(ValueDate).split("-")

			SysMonth 	=	"%s%s" %(StrSysteDate[0], StrSysteDate[1])
			ValueMonth 	=	"%s%s" %(StrValueDate[0], StrValueDate[1])

			if int(ValueMonth) < int(SysMonth):
				raise ValidationError("Value date must be different previous month.")

		except:
			raise

	def validate_Term(form, field):
		try:

			Term 		= int(form.Term.data)
			Currency 	= form.Currency.data
			LoanProduct = request.form['LoanProduct']
			# Call function to check Maximum and Minimum Term
			mktloanapp.checkMaxMinTerm(Term, Currency, LoanProduct)

		except:
			raise

	def validate_Installment(form, field):

		Term 		= form.Term.data
		Installment = form.Installment.data

		if Installment:
			try:
				int(Installment)
			except:
				raise ValidationError("This field must be in numeric format.")

		Frequency 	= request.form['Frequency']
		FreqType 	= request.form['FreqType']
		AppDate 	= request.form['ValueDate']

		Check 		= mktloanapp.validateTermAndInstallment(Term, Installment, Frequency, FreqType, AppDate)
		
		if Check not in "OK":
			raise ValidationError(Check)

	def validate_LoanProduct(form, field):
		try:

			Currency 	= form.Currency.data
			LoanProduct = request.form['LoanProduct']
			LP 			= MKT_LOAN_PRODUCT.query.get(LoanProduct)
			
			if LP:

				RuleID 	= str(LP.Rule) + str(Currency)
				Rule 	= MKT_LOAN_RULE_DE.query.get(RuleID)
				
				if not Rule:
					raise ValidationError("Loan product rule detail not define.")
		
		except:
			raise

	def validate_InterestRate(form, field):
		LoanPro 	= request.form['LoanProduct']
		CurrencyKey = form.Currency.data
		InputRate 	= form.InterestRate.data

		try:

			InputRate = float(InputRate)

		except:

			raise ValidationError("This field must be in numeric format.")

		Msg = mktaccount.checkInterestRate("LC", LoanPro, InputRate, CurrencyKey)

		if Msg:
			raise ValidationError(Msg)

	def validate_CoBorrowerID(form, field):
		try:

			if request.form['CoBorrowerID']:
				
				CoBorrower 		= 	[]
				controls_list 	= 	request.form
				ListCoBorrower	= 	[item for item in controls_list if "CoBorrowerID" in item]
				ListCoBorrower.append("ContractCustomerID")
				ListCoBorrower.sort()
				TotalCoBorrower =	len(ListCoBorrower)

				for nub in range(0,TotalCoBorrower):
					CoBorrower.append(request.form[ListCoBorrower[nub]])

				DuplicateAcc   = [x for x in CoBorrower if CoBorrower.count(x) > 1]
				if DuplicateAcc:
					raise ValidationError("Must not be the same as customer#%s"%DuplicateAcc[0])
				
		except Exception, e:
			raise ValidationError("%s" %e)

	@staticmethod
	def setVisible():		
		control_list = ['Penalty']
		return control_list

	@staticmethod
	def hotField():
		hotfield = []

		fielddisplay 	= "$('#ContractCustomerID').select2('data', {'id':data.LNCustomerID,'text':data.CustomerName}),$('#Amount').val(data.Amount),$('#Disbursed').val(data.Amount),$('#OutstandingAmount').val(data.Amount),$('#Installment').val(data.Installment),$('#Term').val(data.Term),$('#Frequency').val(data.Frequency)"
		fielddisplay 	+= ",$('#LoanProduct').select2('data', {'id':data.LoanProduct,'text':data.ProDesc}),$('#Category').val(data.Category),$('#InterestRate').val(data.InterestRate),$('#ContractVB').select2('data', {'id':data.VBID,'text':data.VB_Desc})"
		fielddisplay 	+= ",$('#FreqType').select2('data', {'id':data.FreqType,'text':data.FreqDesc})"
		fielddisplay 	+= ",$('#LoanPurpose').select2('data', {'id':data.LoanPurpose,'text':data.PurDesc})"
		fielddisplay 	+= ",$('#SourceOfFund').select2('data', {'id':data.SourceOfFund,'text':data.SouDesc})"
		fielddisplay 	+= ",$('#ContractOfficerID').select2('data', {'id':data.ContractOfficerID,'text':data.OfficerName})"
		fielddisplay 	+= ",$('#Currency').val(data.Currency),$('#Group').val(data.Group),$('#ApprovedAmount').val(data.Amount)"
		fielddisplay 	+= ",$('a[href=#3-Guarantor]').attr('style', data.OnOff),$('div#3-Guarantor').attr('style', data.OnOff)"
		fielddisplay 	+= ",$('a[href=#4-Collateral]').attr('style', data.OnOff1),$('div#4-Collateral').attr('style', data.OnOff1)"
		fielddisplay 	+= ",$('#MoreThanOneYear').val(data.MoreThanOneYear)"
		# fielddisplay 	+= ",$('#Account').select2('data', {'id':data.Account,'text':data.AccountName})"
		varname 		= "LoanAppID:$('#LoanApplicationID').val()"
		fun 			= ["LoanApplicationID", varname, fielddisplay, "/Morakot/LoanApplicationInfo", "change", "ContractCustomerID" ]
		# hotfield.append(fun)
		# varname 		= "CustomerID:$('#ContractCustomerID').val()"
		# fun 			= ["LoanApplicationID", varname, fielddisplay, "/Morakot/LoanApplicationAccountInfo", "change", "ContractCustomerID" ]
		hotfield.append(fun)

		fielddisplay 	= "$('#Currency').val(data.Currency), $('#InterestRate').val(data.InterestRate)"
		varname 		= "AccountID:$('#Account').val(), LoanProduct:$('#LoanProduct').val(), LoanAppID:$('#LoanApplicationID').val()"
		fun 			= ["Account", varname, fielddisplay, "/Morakot/ContractCurrencyInfo", "change"]
		hotfield.append(fun)

		fielddisplay 	= "$('#Category').val(data.Category), $('#InterestRate').val(data.InterestRate)"
		varname 		= "LoanProduct:$('#LoanProduct').val(), Currency:$('#Currency').val(), LoanAppID:$('#LoanApplicationID').val()"
		fun 			= ["LoanProduct", varname, fielddisplay, "/Morakot/LoanProductInfo", "change"]
		hotfield.append(fun)

		fielddisplay 	= "$('#RateFixed').val(data.RateFixed); $('#Charge').val(data.Charge)"
		fielddisplay 	+=";$('#ChargePerInstallment').val(data.ChargePerInstallment)"
		fielddisplay 	+=";$('#ChargePerDay').val(data.ChargePerDay)"
		fielddisplay 	+=";$('#ChargeEarned').val(data.ChargeEarned)"
		fielddisplay 	+=";$('#ChargeUnearned').val(data.ChargeUnearned)"
		fielddisplay 	+=";$('#ChargeLastBooked').val(data.ChargeLastBooked)"
		varname 		= "ChargeKeyID:$('#ChargeKey').val(), Currency:$('#Currency').val()"
		fun 			= ["ChargeKey", varname, fielddisplay, "/Morakot/ChargeKeyInfo", "change"]
		hotfield.append(fun)

		fielddisplay 	= ("$('#Amount').val(data.Amount);$('#Amount').focus();$('#OutstandingAmount').val(data.Amount);$('#OutstandingAmount').focus();$('#ValueDate').focus();")
		varname 		= ("Disbursed:$('#Disbursed').val()")
		fun 			= ["Disbursed", varname, fielddisplay, "/Morakot/SetOutStandingAmount", "blur"]
		hotfield.append(fun)

		fielddisplay 	= ("$('#Disbursed').val(data.Amount);$('#Disbursed').focus();$('#Amount').val(data.Amount);$('#Amount').focus();$('#OutstandingAmount').val(data.Amount);$('#OutstandingAmount').focus();$('#ValueDate').focus();")
		varname 		= ("Disbursed:$('#ApprovedAmount').val()")
		fun 			= ["ApprovedAmount", varname, fielddisplay, "/Morakot/SetOutStandingAmount", "blur"]
		hotfield.append(fun)

		fielddisplay 	= ("$('#MoreThanOneYear').val(data.MoreThanOneYear)")
		varname 		= ("Term:$('#Term').val()")
		fun 			= ["Term", varname, fielddisplay, "/Morakot/CheckMoreThanOneYear", "blur"]
		hotfield.append(fun)

		fielddisplay 	= "$('#Installment').val(data.Installment)"
		varname 		= "Term:$('#Term').val(),ValueDate:$('#ValueDate').val(),FreqType:$('#FreqType').val(),Frequency:$('#Frequency').val()"
		fun 			= ["Term", varname, fielddisplay, "/Morakot/CalculateInstallment", "blur"]
		hotfield.append(fun)

		fielddisplay 	= "$('#Installment').val(data.Installment)"
		varname 		= "Term:$('#Term').val(),ValueDate:$('#ValueDate').val(),FreqType:$('#FreqType').val(),Frequency:$('#Frequency').val()"
		fun 			= ["Frequency", varname, fielddisplay, "/Morakot/CalculateInstallment", "blur"]
		hotfield.append(fun)

		fielddisplay 	= "$('#Installment').val(data.Installment)"
		varname 		= "Term:$('#Term').val(),ValueDate:$('#ValueDate').val(),FreqType:$('#FreqType').val(),Frequency:$('#Frequency').val()"
		fun 			= ["FreqType", varname, fielddisplay, "/Morakot/CalculateInstallment", "change"]
		hotfield.append(fun)

		fielddisplay 	= "$('#CoBorrowerName').val(data.CustomerName);$('#CoBorrowerID').val(data.Customer)"
		varname 		= "CoBorrowerID:$('#CoBorrowerID').val()"
		fun 			= ["CoBorrowerID", varname, fielddisplay, "/Morakot/CustomerFullNameByID", "blur"]
		hotfield.append(fun)

		return hotfield

	@staticmethod
	def hotSelectField():
		hotfield = []

		fielddisplay 	= "Account"
		varname 		= "CustomerID:$('#ContractCustomerID').val()"
		fun 			= ["ContractCustomerID", varname, fielddisplay, "/Morakot/ContractCustomerInfo", "change"]
		hotfield.append(fun)

		fielddisplay 	= "Collateral"
		varname 		= "CustomerID:$('#ContractCustomerID').val()"
		fun 			= ["ContractCustomerID", varname, fielddisplay, "/Morakot/CollateralByClient", "change"]
		hotfield.append(fun)

		return hotfield

	@staticmethod
	def setWidth():
		control_list= [('Currency', len1),
					   ('ValueDate', len3),
					   ('FirstCollectionDate',len3),
					   ('Term', len1),
					   ('Cycle', len1),
					   ('Installment', len1),
					   ('MaturityDate', len3),
					   ('FreqType', len2),
					   ('Frequency', len1),
					   ('DeliqMode', len2),
					   ('Group', len1),
					   ('NextAccrDate', len3),
					   ('DisbursedStat', len1),
					   ('MoreThanOneYear', len1),
					   ('IRR',len1),
					   ('AssetClass', len1)]

		return control_list

	@staticmethod
	def setDisable():
		Fields = [('Currency'), ('Category'),
				  ('AccrInterest'), ('NextAccrDate'), ('Branch'), ('RateFixed'), ('Amount'),
				  ('AccrIntPreMonth'), ('AccrCurrentInt'), ('AccrIntCurrMonth'),
				  ('DisbursedStat'), ('MoreThanOneYear'), ('AssetClass'), ('IntIncEarned'), ('AccrIntPerDay'),
				  ('Suspend'), ('Installment'), ('OutstandingAmount'), ("CoBorrowerName"),
				  ('ChargePerInstallment'), ('ChargePerDay'), ('ChargeLastBooked'), ('ChargeEarned'), ('ChargeUnearned'), ('ChargeDescription')]

		return Fields

	@staticmethod
	def moneyField():
		return [["Amount", "Currency"], ["ApprovedAmount", "Currency"], ["Disbursed", "Currency"], ["TotalInterest", "Currency"], ["AccrInterest", "Currency"], ["AccrIntPreMonth", "Currency"],
				["AccrCurrentInt", "Currency"], ["AccrIntCurrMonth", "Currency"], ["OutstandingAmount", "Currency"],
				["Charge", "Currency"], ["ChargePerInstallment", "Currency"], ["ChargeLastBooked", "Currency"], ["ChargeEarned", "Currency"], ["ChargeUnearned", "Currency"]]

	@staticmethod
	def formatMoney():
		return ["Amount", "OutstandingAmount"], "Currency"

	@staticmethod
	def isMultiValue():
		controls_list=["2-Loan Charge", "3-Guarantor", "5-Co-Borrower"]
		return controls_list

	@staticmethod
	def listField():
		return ["ID", "ContractCustomerID", "Account", "Currency", "Amount",
				"OutstandingAmount", "ValueDate", "Term", "LoanProduct"]
				
	@staticmethod
	def findinHist():
		return True

	@staticmethod
	def beforeAuthorize():
		try:

			ID 			= g.formID
			Disbursed 	= MKT_LOAN_CONTRACT_INAU.query.get(ID)

			# check repayment schedule for this loan
			RepSchedule = MKT_REP_SCHEDULE_INAU.query.\
						  filter(MKT_REP_SCHEDULE_INAU.LoanID == ID).\
						  all()
			if not Disbursed:
					flash(msg_error + "No record to authorize.")
					return False
			else:
				Audit 		= 	mktaudit.getAuditrail()
				Inputter	= 	Disbursed.Inputter
				Authorizer 	= 	Audit['Authorizer']
				Authorizeon = 	Audit['Authorizeon']
				Status 		= 	Disbursed.Status
				if Inputter.upper() == Authorizer.upper():
					flash(msg_error + " user who authorize must differ from input.")
					return False
				else:
					if Status == "RNAU":
						mktaudit.deleteAUTH(MKT_SCHED_DEFINE_INAU, ID)
						mktaudit.deleteAUTH(MKT_REP_SCHEDULE_INAU, ID, "LoanID")
						return True

					if not RepSchedule:
						flash(msg_warning + " Repayment schedule not found for loan %s." %ID)
						return False
					else:

						BankDate 	= 	mktdate.getBankDate()
						# BankDate 	= 	str(BankDate).replace("-", "")

						ValueDate 	=	Disbursed.ValueDate
						# ValueDate 	=	str(ValueDate).replace("-", "")
						StrSysteDate=	str(BankDate).split("-")
						StrValueDate=	str(ValueDate).split("-")

						SysMonth 	=	"%s%s" %(StrSysteDate[0], StrSysteDate[1])
						ValueMonth 	=	"%s%s" %(StrValueDate[0], StrValueDate[1])

						if int(ValueMonth) < int(SysMonth):
							flash(msg_error + "Not allow to authorize, loan %s is created since %s." %(ID, ValueDate))
							return False
						else:
							# Authorize Schedule Difine and Repayment Schedule
							mktrepayment.setAuthorizeScheduleDefine(ID)
							LoanDisburseObj = mktloan.setLCDisbursement(ID, "INAU", 0,
													Authorizer=Authorizer,
													Authorizeon=Authorizeon)
							if not LoanDisburseObj[0]:
								db.session.rollback()
								flash(msg_warning+LoanDisburseObj[1])
								return False

			return True

		except:
			db.session.rollback()
			return False

	@staticmethod
	def formReadOnly():
		return "AUTH"

	@staticmethod
	def beforeDelete():
		LoanID = g.formID
		mktaudit.deleteAUTH(MKT_SCHED_DEFINE_INAU, LoanID)
		mktaudit.deleteAUTH(MKT_REP_SCHEDULE_INAU, LoanID, "LoanID")
		return True

	@staticmethod
	def reverseRec():
		ID = g.formID
		LoanObj = MKT_LOAN_CONTRACT.query.get(ID)
		if LoanObj:
			ValueDate 	= LoanObj.ValueDate
			BankDate 	= str(mktdate.getBankDate())

			ValueDate 		= datetime.strptime(str(ValueDate),'%Y-%m-%d').date()
			BankDate 		= datetime.strptime(str(BankDate),'%Y-%m-%d').date()

			if BankDate < ValueDate :
				Audit 		= mktaudit.getAuditrail()
				Inputter	= Audit['Inputter']
				Createdon 	= Audit['Createdon']
				mktaudit.moveAUTHtoINAU(MKT_LOAN_CONTRACT,MKT_LOAN_CONTRACT_INAU,ID,Inputter,Createdon, 'RNAU')
				Message = "The record was reversed successfully, record id:%sR in INAU." % ID
				db.session.commit()
				return True,Message

		return True, "%s Record not allow to reverse." %(msg_warning)

	@staticmethod
	def IsAcceptOverride():

		Override 	= False
		Msg 		= ""
		LoanType 	= request.form['LoanType']

		if LoanType.upper() == "R":
			Override 	= True
			Msg 		= "This is a restructured loan. "

		Msg 		+= "Do you want to procceed?"

		return Override, Msg

	@staticmethod
	def redirectAfterInsert():

		ID 				= 	request.form['ID']
		ValueDate 		= 	request.form['ValueDate']
		LoanType 		= 	request.form['LoanType']
		MoreThanOneYear =	request.form['MoreThanOneYear']
		Installment 	=	request.form['Installment']
		Disbursed 		=	request.form['Disbursed']
		Currency 		=	request.form['Currency']
		AssClass 		=	mktpd.getLoanAssetClass(0, LoanType, MoreThanOneYear)
		NumOfAuth 		= 	g.getNAuthorize
		Resource 		= 	"INAU"
		event_code 		=	"0"

		# Take update for back date
		IntPerDay 			=	float(0)
		TotalInt 			=	float(0)
		SystemBankDate 		=	ValueDate
		NextSystemDate 		=	str(mktdate.getBankDate())

		LoanProduct 		= 	request.form['LoanProduct']
		Product 			= 	MKT_LOAN_PRODUCT.query.get(LoanProduct)

		NextRunDate 		=	ValueDate
		IntNextRunDate 		= 	NextRunDate.replace("-", "")
		IntDate 			= 	NextSystemDate.replace("-", "")

		if int(IntNextRunDate) <= int(IntDate):
			NextRunDate 	=	NextSystemDate

		if Product:
			InterestDayBasis=	Product.IntDayBasis
			NumOfDay 	= 	mktreschedule.getNumberOfDay(InterestDayBasis, SystemBankDate, NextSystemDate)

			if int(NumOfDay) > 0:
				RatePerYear 		=	request.form['InterestRate']
				OutstandingAmount 	=	request.form['Amount']
				IntPerDay 			= 	mktreschedule.getInterestPerDay(float(RatePerYear), float(OutstandingAmount), int(InterestDayBasis), SystemBankDate)
				TotalInt 			= 	float(IntPerDay) * float(NumOfDay)
		
		if NumOfAuth == 0:
			
			Resource = ""
			# Update Loan Contract
			Contract = MKT_LOAN_CONTRACT.query.get(ID)
			if Contract:
				Contract.NextRunDate 		= 	NextRunDate
				Contract.AssetClass 		=	AssClass
				Contract.AccrIntPerDay 		=	float(IntPerDay)
				Contract.AccrCurrentInt 	=	float(TotalInt)
				Contract.AccrIntCurrMonth 	=	float(TotalInt)
				db.session.add(Contract)

			# Update Loan Charge
			mktcharge.updateLoanCharge(ID, Installment, Disbursed, Currency, "AUTH")
			Audit 		= mktaudit.getAuditrail()
			Authorizer	= Audit['Authorizer']
			Authorizeon = Audit['Authorizeon']
			Auth = mktloan.setLCDisbursement(ID, Resource, 0,
													Authorizer=Authorizer,
													Authorizeon=Authorizeon)
			if not LoanDisburseObj[0]:
				flash(msg_warning+LoanDisburseObj[1])
				return None
			db.session.commit()
			#Add notification
			Status 			= request.form['Status']
			event_code 		= mktnotification.getCode(Status,NumOfAuth)
			notification 	= mktnotification.getSignal(ID,MKT_LOAN_CONTRACT,Status,event_code,'LoanContract') #ID,Model,Code
			mktnotification.insertNotify(notification)

		else:
			# Update Loan Contract
			Contract = MKT_LOAN_CONTRACT_INAU.query.get(ID)
			if Contract:
				Contract.NextRunDate 		= 	NextRunDate
				Contract.AssetClass 		=	AssClass
				Contract.AccrIntPerDay 		=	float(IntPerDay)

				db.session.add(Contract)

			# Update Loan Charge
			mktcharge.updateLoanCharge(ID, Installment, Disbursed, Currency, "INAU")
			
			# Commit all record
			db.session.commit()
			#Add notification
			Status 			= request.form['Status']
			event_code 		= mktnotification.getCode(Status,NumOfAuth)
			notification 	= mktnotification.getSignal(ID,MKT_LOAN_CONTRACT,Status,event_code,'LoanContract') #ID,Model,Code
			mktnotification.insertNotify(notification)

		if int(event_code) == 300:

			Manual 	= 	MKT_SCHED_MANUAL.query.\
						filter(MKT_SCHED_MANUAL.LoanID == ID).\
						all()

			if Manual:

				return "/Morakot/ScheduleManual/"+ request.form['ID'] +"?Amendment=No&Resource=" + Resource + "&Edit=Yes"
		
		return "/Morakot/ScheduleDefine/New/" + request.form['ID'] + "?Resource=" + Resource