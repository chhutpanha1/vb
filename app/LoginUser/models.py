from app.mktcore.mdlimports import *

class MKT_LOGIN_USER(exmodel):

	ID 					= db.Column(db.Integer,  autoincrement=True)
	UserID 				= db.Column(db.String(35), primary_key=True)
	Role 				= db.Column(db.String(10))
	Branch 				= db.Column(db.String(10))
	LogInTime 			= db.Column(db.String(15))
	LogInDate 			= db.Column(db.String(10))
	LastTimeActivity 	= db.Column(db.DateTime)