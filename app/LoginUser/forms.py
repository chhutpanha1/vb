from flask 						import request, url_for, render_template,flash,redirect,session
from .. 						import app, db
from app.mktcore.wtfimports 	import *
from .models 					import *
from sqlalchemy 				import *
from sqlalchemy.orm 			import aliased
from app.mktcore.session 		import *
import time
from app.User.models 			import MKT_USER
from app.Role.models 			import MKT_ROLE
from app.Branch.models 			import MKT_BRANCH

from app.mktcore.wtfimports 	import *
from wtforms 					import StringField, BooleanField, TextField, PasswordField, validators
import app.Login.login 			as mktlogin
import app.tools.mktsetting 	as mktsetting

class FRM_LOGIN_USER(Form):
	ID = BooleanField('ID', default=False)

@app.route('/Morakot/LoginUser')
@checkLogOutSession
@checkLogOutTime
def loginUser():
	try:
		Form = FRM_LOGIN_USER()
		
		TimeOut = int(mktsetting.getSetting().TIME_OUT if mktsetting.getSetting().TIME_OUT else 15)
		mktlogin.removeUserFromOnlineList (TimeOut) #remove user from online list

		records = db.session.query(MKT_LOGIN_USER.ID, MKT_LOGIN_USER.LogInDate, MKT_LOGIN_USER.UserID, MKT_LOGIN_USER.LogInTime, MKT_USER.DisplayName, MKT_ROLE.Description.label("Role"), MKT_BRANCH.Description.label("Branch")).\
					join(MKT_USER, MKT_LOGIN_USER.UserID == MKT_USER.ID).\
					join(MKT_ROLE, MKT_ROLE.ID == MKT_LOGIN_USER.Role).\
					join(MKT_BRANCH, MKT_BRANCH.ID == MKT_LOGIN_USER.Branch).\
					all()
		return render_template("loginuserlist.html", records=records, Form=Form)
	except Exception, exe:
		flash(exe)
		return render_template("errors.html")

@app.route('/Morakot/LogOutUser', methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def LogOutUser():
	try:
		if request.args.get('UserID'):
			UserID = request.args.get('UserID')
			logOut = MKT_LOGIN_USER.query.filter_by(UserID=UserID).first()
			db.session.delete(logOut)
			db.session.commit()
			# session.clear()
			flash("The user has been logged out")
		else:
			flash("The user does not exist")

		return redirect(url_for("loginUser"))
	except Exception, exe:
		flash(exe)
		return render_template("loginuserlist.html")

@app.route("/Morakot/MultiLogout", methods=['GET', 'POST'])
@checkLogOutSession
@checkLogOutTime
def MultiLogout():
	try:
		Form = FRM_LOGIN_USER()
		if request.method == 'POST':
			if Form.ID.data:
				dd = []
				for val in Form.ID.data:
					dd.append(val)
				flash(val)
			else:
				flash("No data selected")

		return render_template("loginuserlist.html")
	except Exception, exe:
		flash(exe)
		return render_template("loginuserlist.html")