from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/Province', 'Province', FRM_PROVINCE, [MKT_PROVINCE])