from app.mktcore.mdlimports 	import *

class MKT_MOTOR_BIKE(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	Name = db.Column(db.String(100))
	Year = db.Column(db.String(10))

class MKT_MOTOR_BIKE_INAU(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	Name = db.Column(db.String(100))
	Year = db.Column(db.String(10))

class MKT_MOTOR_BIKE_HIST(exmodel):
	ID = db.Column(db.String(15), primary_key=True)
	Name = db.Column(db.String(100))
	Year = db.Column(db.String(10))
