from app.mktcore.wtfimports 	import *
from .models					import *

class FRM_MOTOR_BIKE(exform):
	Name = TextField(requiredlabel("Name", "*"), 
							[validators.Required()])
	Year = TextField("Year")

