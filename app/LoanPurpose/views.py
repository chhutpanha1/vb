from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/LoanPurpose', 'LoanPurpose', FRM_LOAN_PURPOSE, [MKT_LOAN_PURPOSE])