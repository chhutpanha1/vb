from app.mktcore.wtfimports import *
from .models import *
import time

#class branch form
class FRM_MODULE(exform):

	Description = TextField(requiredlabel("Description", "*"), [validators.Required()])
	FormID = TextField(requiredlabel("Form ID", "*"), [validators.Required()], description="1-Form")
	FormDesc = TextField(requiredlabel("Description", "*"), [validators.Required()], description="1-Form")
	URL = TextField(requiredlabel("URL", "*"), [validators.Required()], description="1-Form")
	NAuthorize = TextField(requiredlabel("No of Authorize", "*"), [validators.Required()],default=0, description="1-Form")

	# def validate_Description(form, field):
	# 	check = MKT_MODULE.query.filter_by(Description=form.Description.data).first()
	# 	if check:
	# 		raise ValidationError("The description already exist")

	@staticmethod
	def setInau():
		return "0" #0 is self authorization, 1 is required next level authorization

	@staticmethod
	def isMultiValue():
		controls_list=["1-Form"]
		return controls_list