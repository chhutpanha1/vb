from app.mktcore.mdlimports import *

class MKT_MODULE(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	Description = db.Column(db.String(35))

class MKT_MODULE_INAU(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	Description = db.Column(db.String(35))

class MKT_MODULE_HIST(exmodel):
	ID = db.Column(db.String(15), primary_key=True)
	Description = db.Column(db.String(35))

class MKT_FORM(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	FormID = db.Column(db.String(10), primary_key=True)
	FormDesc = db.Column(db.String(35))
	URL = db.Column(db.String(35))
	NAuthorize = db.Column(db.Integer)

class MKT_FORM_INAU(exmodel):
	ID = db.Column(db.String(10), primary_key=True)
	FormID = db.Column(db.String(10), primary_key=True)
	FormDesc = db.Column(db.String(35))
	URL = db.Column(db.String(35))
	NAuthorize = db.Column(db.Integer)

class MKT_FORM_HIST(exmodel):
	ID = db.Column(db.String(15), primary_key=True)
	FormID = db.Column(db.String(15), primary_key=True)
	FormDesc = db.Column(db.String(35))
	URL = db.Column(db.String(35))
	NAuthorize = db.Column(db.Integer)