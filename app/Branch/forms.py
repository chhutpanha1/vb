from app.mktcore.wtfimports import *
from .models 				import *
from app.tools.mktroute		import *

import time
import app.tools.mktaddress as mktaddress


class FRM_BRANCH(exform):

	Description 		=	TextField(requiredlabel("Description", "*"), [validators.Required()])
	BranchManagerName 	=	TextField(requiredlabel("Manager Name", "*"), [validators.Required()])
	ContactNumber 		=	TextField("Contact Number")
	
	Province 			= 	TextField(requiredlabel("Province", "*"), [validators.Required()])

	District 			= 	TextField(requiredlabel("District", "*"), [validators.Required()])

	Address 			=	TextAreaField("Address")

	@staticmethod
	def hotSelectField():

		hotfield 		=	[]

		fielddisplay 	=	"District"
		varname			=	"ProvinceID:$('#Province').val()"
		fun				=	["Province", varname ,fielddisplay, "/Morakot/DistrictID", "change"]
		
		hotfield.append(fun)

		return hotfield