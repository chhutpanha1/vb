from app.mktcore.mdlimports import *

#MKT_BRANCH
class MKT_BRANCH(exmodel):
    ID					=	db.Column(db.String(6), primary_key=True)
    Description			=	db.Column(db.String(50))
    BranchManagerName 	=	db.Column(db.String(35))
    ContactNumber 		=	db.Column(db.String(35))
    Address 			=	db.Column(db.String(100))
    District 			=	db.Column(db.String(9))
    Province 			=	db.Column(db.String(3))
    ReportLocked 		=	db.Column(db.String(1))

class MKT_BRANCH_INAU(exmodel):
    ID					=	db.Column(db.String(6), primary_key=True)
    Description			=	db.Column(db.String(50))
    BranchManagerName 	=	db.Column(db.String(35))
    ContactNumber 		=	db.Column(db.String(35))
    Address 			=	db.Column(db.String(100))
    District 			=	db.Column(db.String(9))
    Province 			=	db.Column(db.String(3))
    ReportLocked 		=	db.Column(db.String(1))

class MKT_BRANCH_HIST(exmodel):
    ID					=	db.Column(db.String(6), primary_key=True)
    Description			=	db.Column(db.String(50))
    BranchManagerName 	=	db.Column(db.String(35))
    ContactNumber 		=	db.Column(db.String(35))
    Address 			=	db.Column(db.String(100))
    District 			=	db.Column(db.String(9))
    Province 			=	db.Column(db.String(3))
    ReportLocked 		=	db.Column(db.String(1))