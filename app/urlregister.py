from AutoID.views 			import *
from Login.login 			import *
from Branch.views 			import *
from Menu.views 			import *
from Role.views 			import *
from User.views 			import *
from Module.views 			import *
from LoginUser.views 		import *
from Setting.setting 		import *
from Dates.views 			import *
from Dashboard.views 		import *
from Currency.views			import *
from AccessBranch.views		import *

# Customer
from Sector.views			import *
from Industry.views			import *
from LoanPurpose.views		import *
from Province.views 		import *
from District.views			import *
from Commune.views			import *
from Village.views 			import *
from IDType.views 			import *
from Country.views			import *
from Business.views 		import *
from Position.views			import *
from Officer.views 			import *
from Company.views 			import *
from Report.views 			import *
from Report.master 			import *
from TemplateBuilder.views  import *
from Register.views 		import *
from Course.views 			import *


from PanhaBranch.views		import *
from Test.views 			import *
from Report.Student.views import *
from Student.views 			import *