from app.mktcore.wtfimports 	import *
from app.Branch.models 			import *
from app.Currency.models 		import *
from app.Student.models 		import *

#load Branch
def loadBranch():
	return MKT_BRANCH.query
#load Currency
def loadCurrency():
	return MKT_CURRENCY.query
#load Student
def loadStudent():
	return MKT_STUDENT.query
#validate Amount
def validate_Amount(form, field):
	if float(field.data) == 0.0:
		raise ValidationError("Amount must be greater than 0.")


class FRM_JOURNAL(exform):
	Description 		=	TextField(requiredlabel('Description', '*'),
									  [validators.Required()])
	Category			=	TextField(requiredlabel('Category', '*'),
									  [validators.Required()])
	# where do i get this?
	StudentID			=	QuerySelectField(requiredlabel('Student ID', '*'),
										 query_factory = loadStudent,
										 get_label = u'ID',
										 allow_blank = False)
	DebitCredit			=	SelectField(requiredlabel('Debit/ Credit', "*"),
										choices = [('Dr', 'Debit'), ('Cr', 'Credit')],
										coerce = str,
										default = 'Dr')
	Currency			=	QuerySelectField(requiredlabel('Currency', '*'),
										 query_factory = loadCurrency,
										 get_label = u'Description',
										 allow_blank = True, 
										 default = 'USD')
	Amount 				=	TextField(requiredlabel('Amount', '*'),
									   [validators.Required(), validate_Amount])
	# what is this?
	Module 				=	TextField(requiredlabel('Module', '*'),
									  [validators.Required()])
	TransactionDate		=	DateField(requiredlabel('Transaction Date', '*'),
									  [validators.Required()])
	Branch				=	QuerySelectField('Branch',
											 query_factory = loadBranch,
											 get_label = 'Description',
											 allow_blank = True)
	@staticmethod
	def moneyField():
		return[['Amount', 'Currency']]
