from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/Journal', 'Journal', FRM_JOURNAL, [MKT_JOURNAL])
