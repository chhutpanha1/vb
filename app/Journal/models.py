from app.mktcore.mdlimports import *

class MKT_JOURNAL(exmodel):
	ID 				=	db.Column(db.String(5), primary_key = True)
	Description 	=	db.Column(db.String(35))
	Category 		=	db.Column(db.String(15))
	StudentID		=	db.Column(db.String(13))
	DebitCredit		=	db.Column(db.String(2))
	Amount			=	db.Column(db.Numeric(25,9))
	Currency		=	db.Column(db.String(3))
	Module 			=	db.Column(db.String(10))
	TransactionDate	=	db.Column(db.String(10))
	Branch			=	db.Column(db.String(6))

class MKT_JOURNAL_INAU(exmodel):
	ID 				=	db.Column(db.String(5), primary_key = True)
	Description 	=	db.Column(db.String(35))
	Category 		=	db.Column(db.String(15))
	StudentID		=	db.Column(db.String(13))
	DebitCredit		=	db.Column(db.String(2))
	Amount			=	db.Column(db.Numeric(25,9))
	Currency		=	db.Column(db.String(3))
	Module 			=	db.Column(db.String(10))
	TransactionDate	=	db.Column(db.String(10))
	Branch			=	db.Column(db.String(6))

class MKT_JOURNAL_HIST(exmodel):
	ID 				=	db.Column(db.String(10), primary_key = True)
	Description 	=	db.Column(db.String(35))
	Category 		=	db.Column(db.String(15))
	StudentID		=	db.Column(db.String(13))
	DebitCredit		=	db.Column(db.String(2))
	Amount			=	db.Column(db.Numeric(25,9))
	Currency		=	db.Column(db.String(3))
	Module 			=	db.Column(db.String(10))
	TransactionDate	=	db.Column(db.String(10))
	Branch			=	db.Column(db.String(6))