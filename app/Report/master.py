#!/usr/bin/python
# -*- coding: utf-8 -*-
from app.mktcore.imports 					import *
from .forms 								import *
from decimal 								import *
from flask 									import session
import app.tools.user 						as mktuser
import app.tools.mktmoney 					as mktmoney
import app.tools.mktdate 					as mktdate
import app.tools.mktreport 					as mktreport
import app.resource.report.mktuserfunction  as mktuserfunction
from collections 							import defaultdict
import ast
from sqlalchemy 							import *
import importlib
from math 									import ceil
from sqlalchemy.sql 						import func
from flask.ext.sqlalchemy 					import BaseQuery

def paginate(sa_query, page, per_page=20, error_out=True):
	sa_query.__class__ = BaseQuery
	# We can now use BaseQuery methods like .paginate on our SA query

	return sa_query.paginate(page, per_page, error_out)

def setBaseQuery(query):
	query.__class__ = BaseQuery
	return query

def url_for_other_page(page):
	args = request.view_args.copy()
	args['page'] = page
	return url_for(request.endpoint, **args)
app.jinja_env.globals['url_for_other_page'] = url_for_other_page


@app.route("/Morakot/Report/")
@app.route("/Morakot/Report/<getID>", methods=['GET', 'POST'],defaults={'page': 1})
@app.route('/Morakot/Report/<getID>/Page/<int:page>')
@checkLogOutSession
@checkLogOutTime
def routeMasterReport(getID='',page=1):
	# Block check access right
	ErrorMsg 	= []
	getCheck 	= checkAccess("/Morakot/Report/%s"%getID,"Search")
	if getCheck != True: 
		ErrorMsg.append(msg_error+msg_permission)
		return render_template("permission.html",ErrorMsg=ErrorMsg)

	Module = __import__("app.urlregister")
	# from app.urlregister 	import *
	Report 			=''
	kwargs 			=''
	HeaderTable 	= ''
	ReportRow 		= []
	DataTable   	= ''
	Criteria    	= ''
	FunctionObj 	= []
	OrderByObj  	= ''
	GroupByField 	= ''
	Oprs 			= ''
	GrandTotal  	= False
	CriteriaRequired= False
	TotalRecord 	= ''
	StartNumRecord 	= ''
	EndNumRecord 	= ''
	pagination 		= ''
	urlRequest 		= ''
	DataRaw 		= ''
	GrandTotalObj 	= {}
	SourceType 		= ''
	Formula    		= ''
	FormulaObj 		= ''
	SearchAll 		= ''
	ListError 		= []	
	DicTable 		= {}
	mapJoinTable 	= []
	FieldInTable 	= []
	mapOrderBy 		= []##--- [MKT_ACC_ENTRY.Currency.desc(),MKT_ACC_ENTRY.Category.asc()]
	mapFixCondition = []

	if getID:
		Report = MKT_REPORT.query.get(str(getID))
		if Report:
			Source 			= str(Report.Source)
			SourceType  	= str(Report.Source)[:3]
			HeaderTable 	= Report.ReportHeader.replace('\r','').split('\n')
			HeaderTable 	= filter(None,HeaderTable) #Remove empty strings from a list
			RowObj   		= str(Report.Row).replace('\r','')
			GrandTotal  	= True if Report.GrandTotal =='Y' else False
			GroupByField 	= str(Report.GroupByField)
			FuntionName 	= str(Report.FuntionName).replace('\r','')
			OrderByField 	= str(Report.OrderByField).replace('\r','')
			Formula 		= str(Report.Formula).replace('\r','').split('\n')
			DefaultBranch 	= mktuser.getCurrentBranch()


			kwargs 	= {}
			Oprs 	= {}

			# Call FormulaObj
			FormulaObj 	= mktreport.getFormulaObj(Formula)
			# Check Join Table 

			# Call FunctionObj
			FunctionObj = mktreport.getFuntion(FuntionName)
			# Call Row
			ReportRow 	= mktreport.getRowObj(RowObj)
			
			# Call Order By
			OrderBy 	= mktreport.getOrderBy(OrderByField)
			# Call Group By
			GroupBy 	= mktreport.getGroupBy(GroupByField)
			GroupByField= GroupBy[1]
			GroupBy 	= GroupBy[0]
			
			# append Order By and Group By to mapOrderBy
			mapOrderBy = mktreport.getMapOrderBy(OrderBy,GroupBy)
			
			# Block Join
			mapJoinTableObj = mktreport.getJoinTable(FormulaObj,RowObj)
			mapJoinTable 	= mapJoinTableObj[0]
			DicTable 		= mapJoinTableObj[1]
			FieldInTable 	= mapJoinTableObj[2]
			# Reset ReportRow if have JoinTable
			ReportRow 		= mapJoinTableObj[3] if mapJoinTableObj[3] else ReportRow
			# Block Criteria
			Criteria 		= str(Report.Criteria)
			CriteriaRequired= True if Report.CriteriaRequired =='Y' else False
			CriteriaObj 	= mktreport.getCriteria(Criteria)
			Criteria 		= CriteriaObj[0] 				# Change Criteria from list to dictionary
			DictCriteria 	= CriteriaObj[1]
			print DictCriteria
			mapFixCondition = CriteriaObj[2]
			
			if CriteriaRequired:
				if request.args:
					CheckShow=True
				else:
					CheckShow=False
			else:
				CheckShow=True

			''' Start changes '''
			# Block Get kwargs
			# print 'mhere in master.py for printing Criteria:', Criteria
			for itemCriteria in Criteria:
				# itemCriteria before passing CriteriaObj from mktreport.py as list
				# itemCriteria[0] after passing CriteriaObj from mktreport.py as list of tuple //line 504
				# print Criteria[0]
				if request.args.get(itemCriteria[0]):
					urlRequest=str(request.url).split('?')[1] if str(request.url).find('?Opr') != -1 else ''
					Oprs.update({itemCriteria[1]   :  str(request.args.get('Opr'+itemCriteria[0])) })
					print 'request.args', request.args
					print 'DictCriteria', DictCriteria
					kwargs.update({itemCriteria[1] :  [( DictCriteria[itemCriteria[0]],\
							str(request.args.get(itemCriteria[0])).strip() ) ] if request.args.get(itemCriteria[0]) else '' })
					# print 'mhere in master.py printing kwargs:', kwargs

					#When user search all will be show all Branch
					if itemCriteria[0] == 'Branch':
						if str(request.args.get(itemCriteria[0])).strip().upper()=='ALL':
							if isAccessAllBranch(str(request.args.get(itemCriteria[0])).strip().upper()):
								Oprs.pop('Branch',None)
								kwargs.pop('Branch', None)
								SearchAll='ALL'
				else:	
					if not request.args:
						if itemCriteria[0] == 'Branch':
							#Set Deafault Branch
							Oprs.update({'Branch':'EQ'})
							kwargs.update({itemCriteria[1] :  [(DictCriteria['Branch'],str(DefaultBranch).strip())] })
					# print 'print kwargs:', kwargs

				''' End changes '''

			# Block get condition
			mapCondition = mktreport.getConditionMultiTable(Oprs,kwargs)

			# Block Validation AccessBranch
			if request.args:
				if SearchAll:

					CheckAccessBranch = validationAccessBranch({'Branch': [('', 'ALL')]},Criteria)
					# should change Criteria?
				else:
					CheckAccessBranch = validationAccessBranch(kwargs,Criteria)
				# print 'CheckAccessBranch:%s'%CheckAccessBranch
				if CheckAccessBranch:
					ListError.append(CheckAccessBranch)

			# if Criteria:
			# 	if not kwargs and not SearchAll:
			# 		ListError.append(['Criteria',"This field is required."])

			if SourceType == 'rpt':
				##--- Import module 
				ImportModule = importlib.import_module("app.resource.report.%s" %Source)
				##--- Get Query Object from source
				DataTable = ImportModule.getReport()
				if CheckShow:
					# Server request in rpt
					DataTable = DataTable.filter(or_(*mapCondition[0])).\
															filter(*mapCondition[1]).\
															filter(*mapFixCondition[0]).\
															filter(*mapFixCondition[1]).\
															order_by(*mapOrderBy).\
															yield_per(100).\
															limit(8000)
				##--- Start clear kwargs to store only value for Output kwargs in jinja												
				kwargs = {}
				for itemCriteria in Criteria:
					if request.args.get(itemCriteria[0]):
						kwargs.update({itemCriteria[1] : str(request.args.get(itemCriteria[0])).strip() \
														if request.args.get(itemCriteria[0]) else '' })	

			else:
				if CheckShow:
					# Use Function Join
					if mapJoinTable:

						DataTable = db.session.query(*FieldInTable).filter(or_(*mapCondition[0])).\
																	filter(*mapCondition[1]).\
																	filter(*mapFixCondition[0]).\
																	filter(*mapFixCondition[1]).\
																	join(*mapJoinTable).\
																	order_by(*mapOrderBy).\
																	yield_per(100).\
																	limit(8000)
					else:
						DataTable = getattr(Module,Source).query.filter(or_(*mapCondition[0])).\
															filter(*mapCondition[1]).\
															filter(*mapFixCondition[0]).\
															filter(*mapFixCondition[1]).\
															order_by(*mapOrderBy).\
															yield_per(100).\
															limit(8000)

				##--- Start clear kwargs to store only value for Output kwargs in jinja												
				kwargs = {}
				for itemCriteria in Criteria:
					if request.args.get(itemCriteria[0]):
						kwargs.update({itemCriteria[1] : str(request.args.get(itemCriteria[0])).strip() if request.args.get(itemCriteria[0]) else '' })
				
			# Add Default Value of Branch to Criteria
			for criteria in Criteria:
				if not request.args:
					if 'Branch' in criteria[1]:
						Oprs.update({'Branch':'OprBranch'})
						kwargs.update({'Branch':DefaultBranch})

			# if not DataTable:
			# 	flash(msg_error+"Record not found!")
		
		else:
			flash(msg_error+"Report not found!")

	return render_template("report/master.html",
						TotalRecord 	= TotalRecord,
						StartNumRecord 	= StartNumRecord,
						EndNumRecord 	= EndNumRecord,
						pagination 		= pagination,
						UrlPage 		= page,
						SourceField = SourceType if SourceType != '' else '',
						urlRequest 	= urlRequest,
						ListError 	= ListError,
						getID 		= getID,
						getattr     = getattr,
						hasattr 	= hasattr,
						setattr 	= setattr,
						float       = float,
						str 		= str,
						eval 		= eval,
						mktfunction = mktuserfunction,
						toMoney 	= mktmoney.toMoney,
						CurrencyObj = mktmoney.getCurrencyObj,
						formatDate  = mktdate.formatDate,
						kwargs      = kwargs,
						Oprs 		= Oprs,
						Report 		= Report,
						Criteria    = Criteria,
						HeaderTable = HeaderTable,
						ReportRow   = ReportRow,
						Formula		= Formula,
						DataTable	= DataTable,
						DataRaw 	= DataRaw,
						FunctionObj = FunctionObj,
 						GroupByField    = GroupByField,
 						GrandTotal  	= GrandTotal,
 						GrandTotalObj 	= GrandTotalObj,
 						FormulaObj  	= FormulaObj,
 						CriteriaRequired=CriteriaRequired
						)

def validationAccessBranch(kwargs,Criteria):
	AccessBranch  	= str(mktuser.getUser().AccessBranch).split()
	# Validation AccessBranch
	ListSearchBranch = []
	print 'in vab-k', Criteria
	print 'in vab-c', kwargs
	if 'Branch' in kwargs:
		for key in kwargs:
			if key == "Branch":
				ListSearchBranch=str(kwargs[key][0][1]).split()
		
		if ListSearchBranch:
			for row in ListSearchBranch:

				if not row in AccessBranch:
					if 'ALL' in AccessBranch:
						return []
					else:
						return ["Branch",msg_permission]
	for criteria in Criteria:
		print criteria[1]
		if 'Branch' in criteria[1]:
			if not 'Branch' in kwargs:
				return["Branch","This field is required."]
	
	return []

def isAccessAllBranch(Branch):
	AccessBranch  	= str(mktuser.getUser().AccessBranch).split()
	if Branch in AccessBranch:
		return True
	else:
		return False
		
# @app.errorhandler(DatabaseError)
# def special_exception_handler(error):
#     return 'Database connection failed', 500
