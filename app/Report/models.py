from app.mktcore.mdlimports import *

class MKT_REPORT(exmodel):
	ID = db.Column(db.String(6), primary_key=True)
	Source 			= db.Column(db.String(35))
	ReportTitile 	= db.Column(db.Text)
	ReportHeader 	= db.Column(db.Text)
	Row 			= db.Column(db.Text)
	Formula 		= db.Column(db.Text)
	Criteria		= db.Column(db.Text)
	CriteriaRequired= db.Column(db.String(1))
	GroupByField 	= db.Column(db.Text)
	FuntionName 	= db.Column(db.Text)
	GrandTotal 		= db.Column(db.String(1))
	OrderByField 	= db.Column(db.Text)

class MKT_REPORT_INAU(exmodel):
	ID = db.Column(db.String(6), primary_key=True)
	Source 			= db.Column(db.String(35))
	ReportTitile 	= db.Column(db.Text)
	ReportHeader 	= db.Column(db.Text)
	Row 			= db.Column(db.Text)
	Formula 		= db.Column(db.Text)
	Criteria		= db.Column(db.Text)
	CriteriaRequired= db.Column(db.String(1))
	GroupByField 	= db.Column(db.Text)
	FuntionName 	= db.Column(db.Text)
	GrandTotal 		= db.Column(db.String(1))
	OrderByField 	= db.Column(db.Text)

class MKT_REPORT_HIST(exmodel):
	ID = db.Column(db.String(11), primary_key=True)
	Source 			= db.Column(db.String(35))
	ReportTitile 	= db.Column(db.Text)
	ReportHeader 	= db.Column(db.Text)
	Row 			= db.Column(db.Text)
	Formula 		= db.Column(db.Text)
	Criteria		= db.Column(db.Text)
	CriteriaRequired= db.Column(db.String(1))
	GroupByField 	= db.Column(db.Text)
	FuntionName 	= db.Column(db.Text)
	GrandTotal 		= db.Column(db.String(1))
	OrderByField 	= db.Column(db.Text)

class MKT_REPORT_PRODUCTIVITY(exmodel):
	ID 					= db.Column(db.String(6), primary_key=True)
	NumberOfCustomer 	= db.Column(db.String(25))
	NumberOfLoan 		= db.Column(db.String(25))
	TotalPortfolio		= db.Column(db.Text)