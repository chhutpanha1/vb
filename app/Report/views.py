from app.mktcore.imports 	import *
from .forms 				import *
from .master 				import *

registerCRUD(admin, '/ReportBuilder', 'ReportBuilder', FRM_REPORT, [MKT_REPORT])

