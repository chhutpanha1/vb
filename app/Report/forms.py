from app.mktcore.wtfimports import *
from .models import *
from .. import app, db
import time
from flask 		import Markup
from sqlalchemy import inspect
import app.tools.mktreport as mktreport

class FRM_REPORT(exform):
	Source 			= TextField(requiredlabel("Source", "*"), [validators.Required(), validators.length(max=35)])
	ReportTitile 	= TextField(requiredlabel("Report Titile", "*"), [validators.Required()])
	ReportHeader 	= TextAreaField(requiredlabel("Report Header", "*"), [validators.Required()])
	Row 			= TextAreaField(requiredlabel("Row", "*"), [validators.Required()])
	Formula 		= TextAreaField("Formula")
	FuntionName     = TextAreaField("Funtion")
	Criteria 		= TextAreaField("Criteria")
	CriteriaRequired= SelectField("Criteria Required",
							 choices=[('N', 'No'),('Y', 'Yes')],
							 coerce=str)
	GroupByField    = TextField("GroupBy")
	FuntionName     = TextAreaField("Funtion")
	GrandTotal 		= SelectField("GrandTotal",
							 choices=[('N', 'No'),('Y', 'Yes')],
							 coerce=str)
	OrderByField 	= TextAreaField("OrderBy")

	# def validate_Source(form, field):
	# 	Source = form.Source.data
	# 	if Source:
	# 		if Source[:3] == 'MKT':
	# 			try:
	# 				DataTable 	= eval(Source).query
	# 			except:
	# 				raise ValidationError('%s table not found!' % Source)
	# def validate_ReportHeader(form, field):
	# 	try:
	# 		HeaderTable 	= str(form.ReportHeader.data).replace('\r','').split('\n')
	# 		HeaderTable 	= filter(None,HeaderTable) #Remove empty strings from a list
			
	# 	except Exception, e:
	# 		raise ValidationError('%s'%e)

	def validate_Row(form, field):
		try:
			DataType = ['S','C','N','D','H']
			RowObj   = str(form.Row.data).replace('\r','')
			ReportRow 	= mktreport.getRowObj(RowObj)
			for row in ReportRow:
				if len(row)==2:
					if not row[1] in DataType:
						raise ValidationError('Incorrect format. %s*%s'% (row[0],row[1]))					
		except Exception, e:
			raise ValidationError('%s'%e)
		

	@staticmethod
	def setWidth():
		control_list = [("GrandTotal", len1)]
		return control_list



