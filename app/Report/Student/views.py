from app.mktcore.imports	import *
from app.Student.models		import MKT_STUDENT
from app.Province.models	import MKT_PROVINCE
from app.District.models	import MKT_DISTRICT
from app.Commune.models		import MKT_COMMUNE
from app.Village.models		import MKT_VILLAGE


@app.route('/Morakot/Report/Student', methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def rptStudent():
# you can write any function or query to get report

	ReportName = db.session.query(MKT_STUDENT.ID,
								  MKT_STUDENT.FirstName,
								  MKT_STUDENT.LastName,
								  MKT_STUDENT.Gender,
								  MKT_STUDENT.DOB,
								  MKT_STUDENT.Phone,
								  MKT_PROVINCE.Description.label('Province'),
								  MKT_DISTRICT.Description.label('District'),
								  MKT_COMMUNE.Description.label('Commune'),
								  MKT_VILLAGE.Description.label('Village')).\
								  join(MKT_PROVINCE, MKT_STUDENT.Province == MKT_PROVINCE.ID).\
								  join(MKT_DISTRICT, MKT_STUDENT.District == MKT_DISTRICT.ID).\
								  join(MKT_COMMUNE, MKT_STUDENT.Commune == MKT_COMMUNE.ID).\
								  join(MKT_VILLAGE, MKT_STUDENT.Village == MKT_VILLAGE.ID)
	return render_template("report/students/student.html" , ReportName = ReportName , Name = "Student Report")