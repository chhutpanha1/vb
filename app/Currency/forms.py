from wtforms 					import DecimalField
from app.mktcore.wtfimports import *
from .models import *
import time
import app.tools.mkttool	as mkttool
#class CURRENCY form
class FRM_CURRENCY(exform):

	Description=TextField(requiredlabel("Description","*"),[validators.Required()])
	BuyRate=TextField(requiredlabel("Buy Rate","*"),[validators.Required()])
	SellRate=TextField(requiredlabel("Sell Rate","*"),[validators.Required()])
	MidRate=TextField(requiredlabel("Mid Rate","*"),[validators.Required()])
	ReportingRate=TextField(requiredlabel("Reporting Rate","*"),[validators.Required()])
	RndMode=SelectField(requiredlabel('Rounding Mode',"*"),
							 choices=[('',''),('1','Up'),('2', 'Down'),('3','Natural')],
							 coerce=str,
							 validators=[validators.Required()])

	SmallestRndTo=SelectField(requiredlabel('Smallest Round To',"*"),
							 choices=[('1','1'),('10','10'),('100', '100'),
							          ('1000','1000'),('10000','10000')],
							 coerce=str,
							 validators=[validators.Required()])

	DecimalPlace=SelectField(requiredlabel('Decimal Place',"*"),
							 choices=[('',''),('0', '0'),('1', '1'),('2', '2'),('3','3'),('4', '4'),('5','5'),
							 ('6', '6'),('7', '7'),('8','8'),('9', '9'),('10','10'),('11', '11'),
							 ('12', '12'),('13','13'),('14', '14'),('15','15')],
							 coerce=str,
							 validators=[validators.Required()])
	ThoSeparator=TextField(requiredlabel("Thousand Separator","*"),[validators.Required()])
	DecSeparator=TextField(requiredlabel("Decimal Separator","*"),[validators.Required()])
	CurrencySign=TextField(requiredlabel("Currency Sign","*"),[validators.Required()])
	

	@staticmethod
	def listField():

		Fields = ["ID", "Description", "BuyRate", "SellRate", "MidRate",
				  "ReportingRate", "RndMode", "SmallestRndTo","DecimalPlace"]
		return Fields

	@staticmethod
	def setWidth():
		control_list = [("BuyRate", len3),
						("SellRate", len3),
						("MidRate", len3),
						("RndMode", len1),
						("SmallestRndTo", len1),
						("DecimalPlace", len1),
						("ThoSeparator", len1),
						("DecSeparator", len1),
						("CurrencySign", len1),
						("ReportingRate", len1)]
		return control_list

	def validate_BuyRate(form,field):
		Value = form.BuyRate.data
		if mkttool.isString(Value):
			raise ValidationError("%s This field must be in numeric format."%Value)

	def validate_SellRate(form,field):
		Value = form.SellRate.data
		if mkttool.isString(Value):
			raise ValidationError("%s This field must be in numeric format."%Value)

	def validate_MidRate(form,field):
		Value = form.MidRate.data
		if mkttool.isString(Value):
			raise ValidationError("%s This field must be in numeric format."%Value)
			
	def validate_ReportingRate(form,field):
		Value = form.ReportingRate.data
		if mkttool.isString(Value):
			raise ValidationError("%s This field must be in numeric format."%Value)

	def validate_ThoSeparator(form,field):

		if len(form.ThoSeparator.data)>1:
			raise ValidationError("%s Thousand Separator is only one letter." % form.ThoSeparator.data)
		else:
			if (form.ThoSeparator.data)==',' or (form.ThoSeparator.data)=='.':
				pass
			else:
				raise ValidationError('%s Thousand Separator can be "," or "." ' % form.ThoSeparator.data)

	def validate_CurrencySign(form,field):
		
		if len(form.CurrencySign.data)>1:
			raise ValidationError("%s Currency Sign is only one letter." % form.CurrencySign.data)