from flask 					import Flask, render_template, request, session
from datetime 				import timedelta
from flask.ext.sqlalchemy 	import SQLAlchemy
from app.mktcore.jinja 		import *
import os
from config 				import *
from app.Language.register 	import getLanguage


app=Flask('app')


app.config['SQLALCHEMY_DATABASE_URI'] = CONNECTION_STRING

db=SQLAlchemy(app)
app.config['SECRET_KEY'] = SECRET_KEY

from urlregister import *
app.register_blueprint(admin , url_prefix='/Morakot')

db.session.close()

#register jinja function 
app.jinja_env.globals.update(delimiterdic=delimiterdic)
app.jinja_env.globals.update(removestring=removestring)
app.jinja_env.globals.update(contain=contain)
app.jinja_env.globals.update(maxValue=maxValue)
app.jinja_env.globals.update(getLanguage=getLanguage)
