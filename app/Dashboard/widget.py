from app.mktcore.imports 			import *
from app.urlregister 				import *
import app.tools.user 				as mktuser


def setRegisterWidget():
	# NameofWidget, Label of Widget , Name of Function ,Part and File html, MacroName
	ListUserWidget 	=	[	('OnlineUser',	'Online User',	getWidgetOnlineUser,	'widget/widget_1.html',	'UserOnline')]

	return ListUserWidget


def getWidgetOnlineUser():
	UserObj = db.session.query(MKT_LOGIN_USER.UserID,
							MKT_USER.DisplayName,
							MKT_USER.ProfilePicture,
							MKT_LOGIN_USER.Branch,
							MKT_BRANCH.Description,
							MKT_LOGIN_USER.LogInTime,
							MKT_LOGIN_USER.LogInDate).\
							join(MKT_USER,MKT_USER.ID==MKT_LOGIN_USER.UserID).\
							join(MKT_BRANCH,MKT_BRANCH.ID==MKT_LOGIN_USER.Branch).\
							order_by(MKT_LOGIN_USER.Createdon.desc())
	return UserObj