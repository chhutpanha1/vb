from app.mktcore.mdlimports import *


class MKT_DASHBOARD(exmodel):
	ID 				= 	db.Column(db.String(6), primary_key=True)
	Description		= 	db.Column(db.String(50))

class MKT_DASHBOARD_INAU(exmodel):
	ID 				= 	db.Column(db.String(6), primary_key=True)
	Description		= 	db.Column(db.String(50))

class MKT_DASHBOARD_HIST(exmodel):
	ID 				= 	db.Column(db.String(11), primary_key=True)
	Description		= 	db.Column(db.String(50))

class MKT_DASHBOARD_DE(exmodel):
	ID 				= db.Column(db.String(6), primary_key=True)
	WGID 			= db.Column(db.Integer, primary_key=True, autoincrement=True)
	Widget1 		= db.Column(db.String(20))
	Size1 			= db.Column(db.String(3))
	Widget2 		= db.Column(db.String(20))
	Size2 			= db.Column(db.String(3))
	Widget3 		= db.Column(db.String(20))
	Size3 			= db.Column(db.String(3))

class MKT_DASHBOARD_DE_INAU(exmodel):
	ID 				= db.Column(db.String(6), primary_key=True)
	WGID 			= db.Column(db.Integer, primary_key=True, autoincrement=True)
	Widget1 		= db.Column(db.String(20))
	Size1 			= db.Column(db.String(3))
	Widget2 		= db.Column(db.String(20))
	Size2 			= db.Column(db.String(3))
	Widget3 		= db.Column(db.String(20))
	Size3 			= db.Column(db.String(3))

class MKT_DASHBOARD_DE_HIST(exmodel):
	ID 				= db.Column(db.String(11), primary_key=True)
	WGID 			= db.Column(db.Integer, primary_key=True, autoincrement=True)
	Widget1 		= db.Column(db.String(20))
	Size1 			= db.Column(db.String(3))
	Widget2 		= db.Column(db.String(20))
	Size2 			= db.Column(db.String(3))
	Widget3 		= db.Column(db.String(20))
	Size3 			= db.Column(db.String(3))