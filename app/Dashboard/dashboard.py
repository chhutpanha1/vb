from app.mktcore.imports 	import *
from .models 				import *
from sqlalchemy 			import *

import app.tools.user 				as mktuser
import app.tools.mktdashboard 		as mktdashboard
import app.Dashboard.widget 		as UserWidget
import app.tools.mktreport 			as mktreport

from app.urlregister 		import *

@app.route('/Morakot/CashPositionRefresh', methods=['GET', 'POST'])
@checkLogOutSession
@checkLogOutTime
def routeCashPositionRefresh():
	Obj = mktdashboard.getWidgetCashPosition()
	return render_template("dashboard/widget_cashposition.html",
							Obj=Obj)


@app.route('/Morakot/UnauthorizeRefresh', methods=['GET', 'POST'])
@checkLogOutSession
@checkLogOutTime
def routeUnauthorizeRefresh():
	QueryObj = mktdashboard.getWidgetUnauthorize()
	return render_template("dashboard/unauthorize.html",
							QueryObj=QueryObj)
	
# Unauthorize Detail
@app.route('/Morakot/Unauthorize/<getID>', methods=['GET', 'POST'])
@checkLogOutSession
@checkLogOutTime
def routeListUnauthorize(getID=''):
	# from app.urlregister import *
	Module = __import__("app.urlregister")
	QueryObj 		= []
	Unauthorize 	= []

	if getID:
		Role = mktuser.getUser().Role
		DefaultBranch 	= mktuser.getBranch(session["ChangeBranch"]).ID
		for row in mktdashboard.WIDGET_UNAUTHORIZE:
			# print row[2]
			if getID == row[1]:
				Unauthorize 	= row
				Table			= getattr(Module,row[2])
				if Role == "99":
					# QueryObj 		= Table.query.order_by(Table.ID.desc())
					QueryObj 		= db.session.query(	Table.ID,
														Table.Status,
														Table.Inputter,
														Table.Createdon,
														Table.Authorizer,
														Table.Authorizeon,
														Table.Branch,
														MKT_BRANCH.Description).\
												join(MKT_BRANCH,MKT_BRANCH.ID==Table.Branch).\
												order_by(Table.ID.desc())
				else:
					QueryObj 		= db.session.query(	Table.ID,
														Table.Status,
														Table.Inputter,
														Table.Createdon,
														Table.Authorizer,
														Table.Authorizeon,
														Table.Branch,
														MKT_BRANCH.Description).\
												join(MKT_BRANCH,MKT_BRANCH.ID==Table.Branch).\
												filter(Table.Branch == DefaultBranch).\
												order_by(Table.ID.desc())
				if len(row)==4:
					Condition=row[3]
					QueryObj = QueryObj.filter(*Condition).limit(3000)

	return render_template("dashboard/unauthorize_detail.html",
							QueryObj=QueryObj,
							Unauthorize=Unauthorize)

@app.route('/Morakot/Dashboard', methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def routeDashboard():
	
	# Get record dashboard from user login
	DashboardObj 	= mktdashboard.getDashboardObj()
	WidgetObj 		= []

	if DashboardObj:
		# Get all widget 
		WidgetObj 	= mktdashboard.getWidgetObj()

	return render_template("dashboard/dashboard.html",
							float=float,
							int=int,
							getattr=getattr,
							DashboardObj=DashboardObj,
							WidgetObj=WidgetObj)

@app.route('/Morakot/BranchProductivityRefresh', methods=['GET'])
@checkLogOutSession
@checkLogOutTime
def routeUpdateBranchProductivity():
	
	# Get record dashboard from user login
	ObjUpdate 	= mktdashboard.updateBranchProductivity()

	Obj = mktdashboard.getWidgetBranchProductivity()

	return render_template("dashboard/widget_branchproductivity.html",
							Obj=Obj)

@app.route("/Morakot/CourseByStudentRefresh" , methods = ['GET' , 'POST'])
@checkLogOutSession
@checkLogOutTime
def routeCourseByStudentRefresh():
	List 		=		mktdashboard.getCourseByStudent()
	return 				render_template("dashboard/course_by_student.html" , List = List)