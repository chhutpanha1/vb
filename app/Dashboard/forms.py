from app.mktcore.wtfimports import *
from .models import *
import time
import app.tools.mktdashboard as mktdashboard

class FRM_DASHBOARD(exform):

	ListWidget = mktdashboard.getListWidget()

	Description=TextField(requiredlabel("Description","*"),[validators.Required()])

	Widget1=SelectField(requiredlabel('Widget 1',"*"),
							 choices=ListWidget,
							 coerce=str,
							 validators=[validators.Required()],
							 description="1-DETAILS")
	Size1=SelectField(requiredlabel('Size 1',"*"),
							 choices=[('','--None--'),('1','1'),('2', '2'),('3','3')],
							 coerce=str,
							 validators=[validators.Required()],
							 description="1-DETAILS")

	Widget2=SelectField('Widget 2',
							 choices=ListWidget,
							 coerce=str,
							 description="1-DETAILS")
	Size2=SelectField('Size 2',
							 choices=[('','--None--'),('1','1'),('2', '2'),('3','3')],
							 coerce=str,
							 description="1-DETAILS")

	Widget3=SelectField('Widget 3',
							 choices=ListWidget,
							 coerce=str,
							 description="1-DETAILS")
	Size3=SelectField('Size 3',
							 choices=[('','--None--'),('1','1'),('2', '2'),('3','3')],
							 coerce=str,
							 description="1-DETAILS")

	@staticmethod
	def isMultiValue():
		controls_list=["1-DETAILS"]
		return controls_list