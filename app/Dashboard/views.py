from app.mktcore.imports 			import *
from .forms 	import *
from .models 	import *
from .widget 	import *
from .dashboard import *

registerCRUD(admin, '/DashboardBuilder', 'DashboardBuilder', FRM_DASHBOARD, [MKT_DASHBOARD, MKT_DASHBOARD_DE])
