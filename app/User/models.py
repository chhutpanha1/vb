from app.mktcore.mdlimports import *

class MKT_USER(exmodel):
    ID                  =   db.Column(db.String(35), primary_key=True)
    LogInName           =   db.Column(db.String(35), unique=True)
    DisplayName         =   db.Column(db.String(50))
    Password            =   db.Column(db.String(200))
    StartDate           =   db.Column(db.String(10))
    ExpiryDate          =   db.Column(db.String(10))
    PassValidity        =   db.Column(db.String(15))
    Attempts            =   db.Column(db.Integer)
    StartTime           =   db.Column(db.String(10))
    EndTime             =   db.Column(db.String(10))
    Role                =   db.Column(db.String(10))
    Branch              =   db.Column(db.String(10))
    Mobile1             =   db.Column(db.String(15))
    Mobile2             =   db.Column(db.String(15))
    Email               =   db.Column(db.String(35))
    Active              =   db.Column(db.String(5))
    Menu                =   db.Column(db.String(10))
    DateLastLogin       =   db.Column(db.String(10))
    TimeLastLogin       =   db.Column(db.String(10))
    PwdChangeDate       =   db.Column(db.String(10))
    ProfilePicture      =   db.Column(db.String(60))
    Dashboard           =   db.Column(db.String(6))
    is_active           =   db.Column(db.Integer)
    is_authenticated    =   db.Column(db.String(100))
    Language            =   db.Column(db.String(3))
    AccessBranch        =   db.Column(db.String(100))
    RestrictBranch      =   db.Column(db.String(100))
    Notification        =   db.Column(db.String(1))
    CashAccount         =   db.Column(db.String(8))

    # def __init__(self):
    #     return None

    def is_active(self):
        return True

    def is_authenticated(self):
        return True

    # def is_anonymous(self):
    #     return False

    def get_id(self):
        try:
            return unicode(self.ID)
        except NameError:
            return str(self.ID)  # python 3
    
    # def __repr__(self):
    #     return '<User %r>' % (self.LogInName)
        
class MKT_USER_INAU(exmodel):
    ID                  =   db.Column(db.String(35), primary_key=True)
    LogInName           =   db.Column(db.String(35))
    DisplayName         =   db.Column(db.String(50))
    Password            =   db.Column(db.String(200))
    StartDate           =   db.Column(db.String(10))
    ExpiryDate          =   db.Column(db.String(10))
    PassValidity        =   db.Column(db.String(15))
    Attempts            =   db.Column(db.Integer)
    StartTime           =   db.Column(db.String(10))
    EndTime             =   db.Column(db.String(10))
    Role                =   db.Column(db.String(10))
    Branch              =   db.Column(db.String(10))
    Mobile1             =   db.Column(db.String(15))
    Mobile2             =   db.Column(db.String(15))
    Email               =   db.Column(db.String(35))
    Active              =   db.Column(db.String(5))
    Menu                =   db.Column(db.String(10))
    DateLastLogin       =   db.Column(db.String(10))
    TimeLastLogin       =   db.Column(db.String(10))
    PwdChangeDate       =   db.Column(db.String(10))
    ProfilePicture      =   db.Column(db.String(60))
    Dashboard           =   db.Column(db.String(6))
    is_active           =   db.Column(db.Integer)
    is_authenticated    =   db.Column(db.String(100))
    Language            =   db.Column(db.String(3))
    AccessBranch        =   db.Column(db.String(100))
    RestrictBranch      =   db.Column(db.String(100))
    Notification        =   db.Column(db.String(1))
    CashAccount         =   db.Column(db.String(8))

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.ID)  # python 2
        except NameError:
            return str(self.ID)  # python 3
    
    def __repr__(self):
        return '<User %r>' % (self.username)

class MKT_USER_HIST(exmodel):
    ID                  =   db.Column(db.String(40), primary_key=True)
    LogInName           =   db.Column(db.String(35))
    DisplayName         =   db.Column(db.String(50))
    Password            =   db.Column(db.String(200))
    StartDate           =   db.Column(db.String(10))
    ExpiryDate          =   db.Column(db.String(10))
    PassValidity        =   db.Column(db.String(15))
    Attempts            =   db.Column(db.Integer)
    StartTime           =   db.Column(db.String(10))
    EndTime             =   db.Column(db.String(10))
    Role                =   db.Column(db.String(10))
    Branch              =   db.Column(db.String(10))
    Mobile1             =   db.Column(db.String(15))
    Mobile2             =   db.Column(db.String(15))
    Email               =   db.Column(db.String(35))
    Active              =   db.Column(db.String(5))
    Menu                =   db.Column(db.String(10))
    DateLastLogin       =   db.Column(db.String(10))
    TimeLastLogin       =   db.Column(db.String(10))
    PwdChangeDate       =   db.Column(db.String(10))
    ProfilePicture      =   db.Column(db.String(60))
    Dashboard           =   db.Column(db.String(6))
    is_active           =   db.Column(db.Integer)
    is_authenticated    =   db.Column(db.String(100))
    Language            =   db.Column(db.String(3))
    AccessBranch        =   db.Column(db.String(100))
    RestrictBranch      =   db.Column(db.String(100))
    Notification        =   db.Column(db.String(1))
    CashAccount         =   db.Column(db.String(8))
    
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.ID)  # python 2
        except NameError:
            return str(self.ID)  # python 3
    
    def __repr__(self):
        return '<User %r>' % (self.username)
