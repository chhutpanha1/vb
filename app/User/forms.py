from flask 						import flash
from werkzeug.security 			import generate_password_hash
from app.mktcore.wtfimports     import *
from .models                    import *
from app.Role.models            import *
from app.Branch.models          import *
from app.Menu.models            import *
import time

from datetime                   import datetime
from sqlalchemy                 import *

from app.tools.mktreverse       import clsReverse
from app.Dashboard.models       import *


import app.tools.user           as mktuser 
import app.tools.mktsetting     as mktsetting
import app.Language.register 	as mktlanguage



def loadDashboard():
	return MKT_DASHBOARD.query

def loadRole():  #define function
	return MKT_ROLE.query    # return records from Branch table


def checkBranch (branchlist):
	for item in branchlist :
		if item !='ALL':
			result = MKT_BRANCH.query.get(item)
			if result == None :
				return item

	return True

def loadMenu():
	return MKT_MENU.query    # return records from Menu table

def reverseRec(Table, ID, ForeingTable=None):
	return mktreverse.setReverse(Table, ID)

def checkTimeFormat(Data):

	if Data != "":
		if Data.find(':') != -1:

			try:
				datetime.strptime(str(Data), "%H:%M")
				
			except Exception as exe:
				raise ValidationError("Can't convert to format %H:%M. See ex: 08:30")

		else:
			raise ValidationError("The entry must be hour and minute(H:M). ex: 8:30")

def loadAttemps():
	try:

		Setting = mktsetting.getSetting()

		Attempts = "3"

		if Setting:

			Attempts = Setting.ATTEMPS

		return int(Attempts)

	except:
		raise

def loadDefaultPassword():
	return generate_password_hash('123456')

#class user form
class FRM_USER(exform):
	d = datetime.now()
	now = '%s-%s-%s'% (d.year, d.month, d.day)
	CurrentDate 	= 	datetime.strptime(str(now), "%Y-%m-%d")
	LogInName 		= 	TextField(requiredlabel("Log-In Name", "*"), [validators.Required()])
	Password 		=	TextField("Password", default=loadDefaultPassword)
	DisplayName 	= 	TextField("Display Name")
	StartDate 		= 	DateField(requiredlabel("Start Date", "*"), [validators.Required()], default=CurrentDate)
	ExpiryDate 		= 	DateField(requiredlabel("Expiry Date", "*"),[validators.Required()])
	PassValidity 	= 	TextField(requiredlabel("Password Validity", "*"), [validators.Required()])
	StartTime 		= 	TextField("Start Time")
	EndTime 		= 	TextField("End Time")

	Role 			= 	QuerySelectField(requiredlabel('Role', "*"),
							query_factory=loadRole,
							get_label=u'Description',
							allow_blank=False,
							blank_text=u'None'
						)

	Mobile1 		= 	TextField("Mobile Phone 1")
	Mobile2 		= 	TextField("Mobile Phone 2")
	Email 			= 	TextField("Email", [validators.Optional(), validators.Email(message=u'Invalid email address.')]) #, [validators.Email(message=u'Invalid email address.')]
	
	Active 			= 	SelectField(requiredlabel('Active', "*"),
							choices=[('Yes', 'Yes'),
									 ('No', 'No')],
							coerce=str,
							validators=[validators.Required()]
						)

	Menu 			= 	QuerySelectField(requiredlabel('Menu', "*"),
							query_factory=loadMenu,
							get_label=u'Description',
							allow_blank=False,
							blank_text=u'--None--'
						)


	Dashboard 		= 	QuerySelectField('Dashboard',
							query_factory=loadDashboard,
							get_label=u'Description',
							allow_blank=True,
							blank_text=u'--None--'
						)

	Language 		= 	TextField("Attempts", default="KH")
	Attempts 		= 	TextField("Attempts", default=loadAttemps)
	AccessBranch 	= 	TextField(requiredlabel("Access Branch", "*"), [validators.Required()])
	RestrictBranch 	= 	TextField(requiredlabel("Restrict Branch", "*"), [validators.Required()])

	
	def validate_AccessBranch(form, field):
		listbranch = form.AccessBranch.data.split()
		getCheck = checkBranch (listbranch)
		if getCheck != True :
			raise ValidationError("Branch "+ getCheck + " not found.")

	def validate_RestrictBranch(form, field):
		listbranch = form.RestrictBranch.data.split()
		getCheck = checkBranch (listbranch)
		if getCheck != True :
			raise ValidationError("Branch "+ getCheck + " not found.")

	def validate_LogInName(form, field):
		ID = request.form['ID']
		checkID = MKT_USER.query.get(ID)
		if not checkID:
			user = MKT_USER.query.\
				   filter(MKT_USER.LogInName == form.LogInName.data).\
				   first()
		else:
			user = MKT_USER.query.\
				   filter(MKT_USER.ID == ID).\
				   filter(MKT_USER.LogInName == form.LogInName.data).\
				   filter(MKT_USER.LogInName != checkID.LogInName).\
				   first()

		if user:
			raise ValidationError("The user is already exists.")
		else:
			checkID = MKT_USER_INAU.query.get(ID)
			if not checkID:
				inau = MKT_USER_INAU.query.\
					   filter(MKT_USER_INAU.LogInName == form.LogInName.data).\
					   first()
			else:
				inau = MKT_USER_INAU.query.\
					   filter(MKT_USER_INAU.ID == ID).\
					   filter(MKT_USER_INAU.LogInName == form.LogInName.data).\
					   filter(MKT_USER_INAU.LogInName != checkID.LogInName).\
					   first()
			if inau:
				raise ValidationError("The user is already exists.")

	def validate_PassValidity(form, field):
		PassVali = form.PassValidity.data
		newval = PassVali.split()
		if len(newval[0]) != 8 or len(newval[1]) != 5:
			raise ValidationError("You input wrong, Please check with formatted 20150130 M0330")
		else:
			try:
				FirstStr = datetime.strptime(str(newval[0]), "%Y%m%d")
			except Exception as exe:
				raise ValidationError("Your input first 8 digit not match to value date")

			SecondStr = newval[1]
			if SecondStr[:1] != "M":
				raise ValidationError("The letter %s must change to M" %SecondStr[:1])
			try:
				SecondStr = newval[1]
				SecondStr = datetime.strptime(str(SecondStr[-4:]), "%m%d")
			except Exception as exe:
				raise ValidationError("Your input last 4 digit not match to value month and day")

	def validate_StartTime(form, field):
		StartT = form.StartTime.data
		checkTimeFormat(StartT)

	def validate_EndTime(form, field):
		EndT = form.EndTime.data
		checkTimeFormat(EndT)

	@staticmethod
	def setVisible():
		return ['Password']

	@staticmethod
	def setInau():
		return "0" #0 is self authorization, 1 is required next level authorization

	@staticmethod
	def setDisable():
		return [('Attempts')]

	@staticmethod
	def setDisableforEdit():  # override base class method 
	  return [('LogInName'), ('Attempts')]

	@staticmethod
	def setWidth():
		control_list= [('StartDate', len3),
						('ExpiryDate', len3),
						('Mobile1', len3),
						('Mobile2', len3),
						('Active', len1),
						('DateLastLogin', len3),
						('TimeLastLogin', len3),
						('PwdChangeDate', len3),
						('Attempts', len1),
						('StartTime', len1),
						('EndTime', len1)]
						
		return control_list

	@staticmethod
	def listField():
		Fields = ["ID", "LogInName", "DisplayName", "Role", "AccessBranch", 
				  "StartDate", "CashAccount", "PassValidity", "Active"]
		return Fields

	@staticmethod
	def reverseRec(): 
		try:

			ID 			= 	g.formID
			UserObj 	=	MKT_USER.query.get(ID)
			
			if UserObj:

				LogInName 	=	UserObj.LogInName
				CheckList 	=	['ADMIN.S', 'MORAKOT.IT']

				if LogInName in CheckList:

					return False, "This user not allow to reverse."

			return True, ""

		except Exception, e:
			db.session.rollback()
			return False, "%s" %e