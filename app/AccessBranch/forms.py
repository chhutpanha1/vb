from app.mktcore.wtfimports import *
from .models import *


class FRM_ACCESSBRANCH(exform):
	Model = TextField(requiredlabel("Data Model", "*"), [validators.Required(), validators.Length(min=1, max=100)])
	DebitAcc = TextField("Debit Acc")
	CreditAcc = TextField("Credit Acc")