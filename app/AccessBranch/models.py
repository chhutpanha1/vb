from app.mktcore.mdlimports import *

class MKT_ACCESSBRANCH(exmodel):
	ID 				= db.Column(db.String(15), primary_key=True)
	Model 	= db.Column(db.String(100))
	DebitAcc = db.Column(db.String(100))
	CreditAcc = db.Column(db.String(100))
	
class MKT_ACCESSBRANCH_INAU(exmodel):
	ID 				= db.Column(db.String(15), primary_key=True)
	Model 	= db.Column(db.String(100))
	DebitAcc = db.Column(db.String(100))
	CreditAcc = db.Column(db.String(100))

class MKT_ACCESSBRANCH_HIST(exmodel):
	ID 				= db.Column(db.String(15), primary_key=True)
	Model 	= db.Column(db.String(100))
	DebitAcc = db.Column(db.String(100))
	CreditAcc = db.Column(db.String(100))