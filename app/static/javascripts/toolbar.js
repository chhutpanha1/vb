
function fn_fadeIn(){
  var $getParentEle = window.parent.$("#animated")
  $getParentEle.fadeIn("slow");

}

function toolbar(url){
//{{'/'+action_url.split('/')[1]+'/'+action_url.split('/')[2]+'/
//cancel button
$("#Cancel").on("click",function(e){
  value_id=$('#ID').val()
  //$("#animated").addClass ('se-pre-con')
  fn_fadeIn();
  //$(".se-pre-con").fadeOut("slow");
  if (value_id==""){
    window.location.href=url+"/Cancel/Operation/"
    return 0;
  }
    window.location.href=url+"/Cancel/"+ value_id +"/";
    return 0;
  
});

//new button
$("#New").on("click",function(e){
  value_id=$('#ID').val()
  fn_fadeIn();
  if (value_id==""){
    window.location.href=url+"/New/Operation/"
    return 0;
  }
    window.location.href=url+"/New/"+ value_id +"/";
    return 0;
});

//save button
$("#Save").on("click",function(e){
  value_id=$('#ID').val()
  fn_fadeIn();
  if (isModified()) {
    
    window.location.href=url+"/Cancel/"+ value_id +"/";
    return 0;
  }
  $(':focus').blur();
  removeComma();
  document.forms["frm"].action=url+"/";
  document.forms["frm"].submit(); 
  //return false; 
});

//edit button
$("#Edit").on("click",function(e){
  value_id=$('#ID').val()
  if (value_id==""){
    alert("No record to edit"); 
    return 0
  }


  fn_fadeIn();
  window.location.href=url+"/Edit/"+value_id+"/";
  //return false; 
});

//verify button
$('#Verify').on("click",function(e){
   value_id=$('#ID').val()
    if (value_id==""){
     value_id="Operation"
    }
    fn_fadeIn();
    window.location.href=url+"/Verify/"+value_id+"/";
  });

//disable enter key from submit
$("#ID").on("keypress", function(e) {
  $("#frm").attr("method", "post");
  $("#frm").attr("method", "get");
  if (e.keyCode == 13){
      value_id=$('#ID').val()
      if (value_id==""){
        value_id="Operation"
      }
      fn_fadeIn();
      //window.location.href="/Morakot/User/?ID1=1"
      //window.location.href="{{'/'+ action_url.split('/')[1]+'/'+action_url.split('/')[2]+'/Search/' }}"+ value_id+"/";
       document.forms['frm'].action=url+"/?ID="+ value_id +"&Operation=Search";
       document.forms['frm'].submit();
       //obj_loaded(); 
       return false;
  
 }
});
//add link to authorize
//Add link to delete button
$('#Authorize').on("click",function(e){
   value_id=$('#ID').val()
    if (value_id==""){
     value_id="Operation"
    }
    fn_fadeIn();
    window.location.href=url+"/Authorize/"+value_id+"/";
  });


//Add link to delete button
$('#Delete').on("click",function(e){
   value_id=$('#ID').val()
   bootbox.confirm({
          message: "Are you sure to delete " +value_id+"?",
          callback: function(result) {
                  if (result==true){                    
                    if (value_id==""){
                        value_id="Operation"
                    }
                    fn_fadeIn()
                    window.location.href=url+"/Delete/"+value_id+"/";
                  }
                },
                className: "bootbox-sm"
              });
  });

//Add link to delete button
$('#listLive').on("click",function(e){
   fn_fadeIn();
   //$("#animated").addClass ('se-pre-con')
   window.location.href=url+"/ListLive/Operation/";
  });

//Add link to delete button
$('#listAuth').on("click",function(e){
   fn_fadeIn();
   window.location.href=url+"/ListAuth/Operation/";
  });

//Add link to delete button
$('#listHist').on("click",function(e){
   fn_fadeIn();
   window.location.href=url+"/ListHist/Operation/";
  });


//Add link to delete button
$('#Reverse').on("click",function(e){
   fn_fadeIn();
   window.location.href=url+"/Reverse/"+$('#ID').val()+"/";
  });
//Disable attribute
//$('a.disabled').removeAttr('href')
//$('a.disabled').removeAttr('onclick')
//set controls' width
$('#Status').css('width','60px')
$('#Curr').css('width','50px')
$('#Inputter').css('width','150px')
$('#Createdon').css('width','150px')
$('#Authorizer').css('width','150px')
$('#Authorizeon').css('width','150px')
$('#Branch').css('width','150px')
//$('#FNAuthorize').css('width','50px')
//end



//advanced_selector_initialize()
advanced_selector_initialize()


//remove delete and authorize button when record is in AUTH
var status = $('#Status').val()
var searchID = $('#ID').val()

if ( status == "AUTH"){
  $("#Delete").remove();
  $("#Authorize").remove();
}else if (status =="INAU" || status =="RNAU"){
  $("#Reverse").remove();  
}

//check history record
if (searchID.indexOf("@") >0)
{
  $("#Save").remove();
  $("#Delete").remove();
  $("#Authorize").remove();
  $("#Edit").remove();
  $("#Reverse").remove();
  $('#frm input').attr('readonly', 'readonly');
  $('#frm select').attr('disabled', 'disabled');
}

$("div.select2-container").attr({"style": "display: block"});

}




 


