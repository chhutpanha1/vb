var id=[]
var advanced_selector_initialize=function(){
	//$('.select2-container').remove()
	//$('div.select2-container:even').remove()
	$('select').each(function(){
        $("#"+$(this).attr('id')).select2();
    });	
};

//add selector to selectlist
var advanced_selector=function(obj_to_cloned){
	
	$("div").closest('#'+obj_to_cloned).find('.select2-container').remove()
	
	//$(this).closest('.list_product').remove()
	//$('.select2-container').last().remove()
	$('select').each(function(){

        $("#"+$(this).attr('id')).select2();
    });	

};

var update_index=function(group_control_id,i){
	       // $('label').text($('label').text())
	       
			$(group_control_id).find('[class]').each(function() {
			    if ($(this).hasClass('mspan')){
			    	if(i==1){
			    		i=2
			    	}
			    	else{


			    		$arrcloneid=$(this).attr('id').split('_')[0]
			    		$(this).attr('id',$arrcloneid+'_'+i)
			    		$getid=$(this).attr('id')
			    		
			    		
						$(this).find('[id]').each(function(){

							
							$arrid=$(this).attr('id').split('_')
							$(this).attr('id',$arrid[0]+"_"+i)
	          				$(this).attr('name',$arrid[0]+"_"+i)
	          				
	          				
	          			});
	          			
	          			updateHotField(i)
	          			toMoneyHotField(i)
          				i++
					}
				}

			});
			
			advanced_selector($getid)
}

//create add button
var add_button=function(group_control_id){
	var htmlattrib="<div class='form-group'><div class='col-sm-offset-2 col-sm-10' id="+group_control_id+"btn_1><button type='button' class='btn btn-default " +group_control_id+"_add'>+</button></div></div>"
  $("#"+group_control_id).append(htmlattrib)
 
};

//create add event to element
var click_add=function(group_control_id,obj_to_cloned){

	$("#"+group_control_id).on('click','.'+group_control_id+'_add',function(){
	  	
	  	$(this).remove()
	  	$('#'+group_control_id+'btn_1').parent().remove()
	  	var $org_obj=$("#"+obj_to_cloned)

	    var $clone_obj=$org_obj.clone()
	    var $arrid=obj_to_cloned.split('_')[0]
	        $clone_obj.attr('id',$arrid+'_0')
	        $clone_obj.find("input[type='text'], select").val("");

	    text=$clone_obj
	    $('#'+group_control_id).append(text)
	    $('#'+$clone_obj.attr('id')).append("<div class='form-group'><div class='col-sm-offset-2 col-sm-10' id="+group_control_id+"btn_><button type='button' class='btn btn-default "+group_control_id+"_remove'>-</button>&nbsp;<button type='button' class='btn btn-default "+group_control_id+"_add'>+</button></div></div>")
	   
	    update_index("#"+group_control_id,1)

	    //set up date picker after adding new control
         $('.mdatepicker').datepicker({
                format:"yyyy-mm-dd"
            });//end of format

        //recall remote data
        callremotedata();

  });

};

//add remove event
var click_remove=function(group_control_id){
$("#"+group_control_id).on('click','.'+group_control_id+'_remove',function(){
    var div=$(this).parent().parent().parent().attr('id') //get current parent element
    var pre=$(this).parent().parent().parent().prev().attr('id') //get previous parent element
    $('#'+div).remove()
    var button_col=group_control_id+"btn_"+pre.substring(pre.lastIndexOf("_")+1)
    
    
    if($('.'+group_control_id+'_add').length<1){
    	if($('.'+group_control_id+'_remove').length<1){
    		add_button (group_control_id)
    	}else{
    	$('#'+button_col).append("<button type='button' class='btn btn-default "+group_control_id+"_add'>+</button>")
    }
}

    update_index("#"+group_control_id,1)
    //call money funtion
    sumMoney();
  });
};

//document ready and then clone object

var path=location.pathname.split('/')[2]
 $( document ).ready(function() {
 	if (location.pathname.split('/')[3]!="New"){
	    var $clone = $("form").clone().serialize();
	    localStorage.setItem("cloneobj"+path, $clone);
	   	
	}
    });
	
//is data modified	
function isModified(){
		if (location.pathname.split('/')[3]=="Edit"){
			var clone=localStorage.getItem("cloneobj"+path);
			if (clone!=null){
				 if ($("form").serialize()==clone) {
				 	alert("Data has no change. Will not submit to server.")
				 	return true
				 }else{
				 	return false
				 }

			}
		}
}

//save form data into local storage
function copy(objectcopied){

	if (location.pathname.split('/')[3]!="Edit"){
		alert("No data to copy...")
		return 0;	
	}
	$("input").each(function(){
    $(this).attr("value", $(this).val());
	});

	$("select").each(function(){
		if ($(this).val() != ""){
    		$("option[value= "+ $(this).val()+"]").attr("selected",true);
    	}
	});

	$val=$("#"+objectcopied).clone().html();	

	//$("#objclone").append($val)
	localStorage.setItem(path, $val);
	alert("Data has been saved in localStorage successfully");
}

//restore data to form
function paste(objectcopied){
	if (location.pathname.split('/')[3]!="New"){
		alert("No data to paste...")
		return 0;	
	}
	path=location.pathname.split('/')[2]
	divs=localStorage.getItem(path);
	if (divs!= null){

			$id=$('#ID').val() //get new id
            $("#"+objectcopied).html(divs)
            $('div.select2-container:even').remove() // remove odd index
            $('#ID').val($id)  //replace old id by new id

    }else{
    	alert("No data to restore")
    }
	
}

//get parameter from url
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function updateHotField(j){
	
	var fielddisplay=""

	getdata=JSON.parse(localStorage.getItem("multivalue"+location.pathname.split('/')[2]))
	if (getdata ==null){ return 0;}
	
	$.each( getdata, function( key, value ) {
	
		if ($('#'+value[0]+"_"+j).length>0){
	
					getvalue1=value[2].split(";")
					
					$.each(getvalue1,function(key1,value1){
						getall=value1.split(".")
						getv=getall[0]
    					getv1=getv.replace("')","_"+j.toString()+"')")

    					//check object if exits?
						var objdisplay = getv1
						if (typeof eval(objdisplay).val() === 'undefined'){
							getv1 = getv1.replace("_" + j.toString(),'')
							
							
						}


    					if (fielddisplay ==""){
    						
    						fielddisplay = getv1+"."+getall[1]+"."+getall[2]
    					}else{
    						
    						fielddisplay += ","+getv1+"."+getall[1]+"."+getall[2]
    					}

					}); //end of query display

				
				$(function() {
				$('#'+value[0]+'_'+j.toString()).bind(value[4], function() {
				
				//get query string
				querystring=""
				getparam=value[1].split(",")
				$.each(getparam,function(key2,value2){

					getp=value2.split(":")

					if (querystring==""){
						querystring=getp[0]+"="+ eval(getp[1].split(".")[0].replace("')","_"+j.toString()+"')")).val()
						
					}else{
						querystring +="&"+getp[0]+"="+eval(getp[1].split(".")[0].replace("')","_"+j.toString()+"')")).val()
					}

				});//end of query string
					
					$.getJSON(value[3]+"?"+querystring, function(data) {

					 eval(fielddisplay)
					
					});
						return false;
				});
			});	
			
		} //end of control check exist or not
		
	});	

}

//take from jinja
function updateHotField_1(getdata,j){

	var fielddisplay=""

				//get field display
					getvalue1=getdata[2].split(";")
					
					$.each(getvalue1,function(key1,value1){
						getall=value1.split(".")
						getv=getall[0]
    					getv1=getv.replace("')","_"+j.toString()+"')")
    					//check object if exits?
						var objdisplay = getv1
						if (typeof eval(objdisplay).val() === 'undefined'){
							getv1 = getv1.replace("_" + j.toString(),'')
							
							
						}
						
    					if (fielddisplay ==""){
    						fielddisplay = getv1+"."+getall[1]+"."+getall[2]
    					}else{

    						fielddisplay += ","+getv1+"."+getall[1]+"."+getall[2]
    					}

					}); //end of query display

				$(function() {
				//$('#'+value[0]+'_'+j.toString()).bind(value[4], function()
				$('#'+getdata[0]+'_'+j.toString()).bind(getdata[4], function() {
				
				    //get query string
					querystring=""
					getparam=getdata[1].split(",")
					$.each(getparam,function(key2,value2){

						getp=value2.split(":")

						if (querystring==""){
							querystring=getp[0]+"="+ eval(getp[1].split(".")[0].replace("')","_"+j.toString()+"')")).val()
							
						}else{
							querystring +="&"+getp[0]+"="+eval(getp[1].split(".")[0].replace("')","_"+j.toString()+"')")).val()
						}

					});//end of query string

					
					$.getJSON(getdata[3]+"?"+querystring, function(data) {
					
					 eval(fielddisplay)
					
					});
						return false;
				});
	});	

}

//add remote control with javascript
function addRemoteData(fieldid,url){

             $('#'+fieldid).select2({
                  placeholder:'Select '+fieldid,
                  minimumInputLength: 3,
                  triggerChange: true,
                  allowClear: true,
                  ajax: {
                      url: url,
                      dataType: 'json',
                      data: function (term, page) {
                          return {
                              q: term
                          };
                      },
                      results: function (data, page) {
                          return {
                              results: data.items
                          };
                      }
                  },
                  initSelection: function (element, callback) {
                      var id = $(element).val();
                      if (id !== "") {
                          $.ajax(url+"?action=view&q="+id, {
                              dataType: "json"
                          }).done(function(data) { callback({id: id, text: data.items[0].text}); });
                      }
                      
                  }
              });
}// end of function

//call remote data

function callremotedata(){
getUrl=""
$.each($('input[type=hidden]'),function(i,val){
    if ($(this).attr('id').split("_").length<2){
      getUrl=$(this).attr('id')
      addRemoteData($(this).attr('id'), "/"+location.pathname.split('/')[1]+"/"+getUrl)
    }else{
      addRemoteData($(this).attr('id'),"/"+location.pathname.split('/')[1]+"/"+getUrl)
    }
});
}

function toMoneyHotField(k){

	getdata=JSON.parse(localStorage.getItem("moneyvalue"+location.pathname.split('/')[2]))
	
	if (getdata ==null){ return 0;}
	$.each( getdata, function( key, value ) {

			if ($('#'+value[0]+'_'+k).length>0){
				$(function() {
					$('#'+value[0]+'_'+k.toString()).bind('blur', function() {
						var CurrencyValue = $('#'+value[1]+'_'+k.toString()).val();
						if(typeof CurrencyValue === 'undefined'){
							var CurrencyValue = $('#'+value[1]).val();
						}
						$.getJSON('/Morakot/toMktmoney', {

							Currency:CurrencyValue,
							Money:$('#'+value[0]+'_'+k.toString()).val()

						}, function(data) {
							$('#'+value[0]+'_'+k.toString()).val(data.value)
							
							if (typeof value[2] !='undefined'){
								
								sumAmount(value[2],value[0],CurrencyValue)

							}
							
						});
						return false;
				});
    });
			
}
});
}

//function sum
function sumAmount(showValue,sumField,CurrencyValue){

	totalAmt = 0;
	$("input[id^='"+sumField+"']").each(function (i, el){
	$getValue=el.value 
	$getValue=$getValue.replace(/[,]/g,"")
	totalAmt = totalAmt + parseFloat ($getValue)
	});

	$.getJSON('/Morakot/toMktmoney', {

		Currency:CurrencyValue,
		Money:totalAmt

		}, function(data) {

			$('#'+showValue).val(data.value)
			
	});

}

function sumMoney(){

//call money funtion
    getdata=JSON.parse(localStorage.getItem("moneyvalue"+location.pathname.split('/')[2]))
    if (getdata ==null){ return 0;}
    $.each( getdata, function( key, value ) {
    	
    	if (typeof value[2] !='undefined'){
    		CurrencyValue = $('#'+value[1]).val()
			sumAmount(value[2],value[0],CurrencyValue)

		}

    });
}

function toMoneyHotFieldforEdit(k, moneyfield, currencyfield,totalAmt){

				$(function() {


						$('#'+ moneyfield).bind('blur', function() {
						
						$.getJSON('/Morakot/toMktmoney', {

							Currency:$('#'+currencyfield).val(),
							Money:$('#'+moneyfield).val()

						}, function(data) {
							$('#'+moneyfield).val(data.value)
							if (totalAmt !=""){
								
								sumAmount(totalAmt,moneyfield,$('#'+currencyfield).val())
								sumMoney();
							}
						});
						return false;
				});
				
   });

}

// this function is used to display amount in format based on currency setup
function toMoneyformat(k, moneyfield, currencyfield,totalAmt){

	$(function() {
		$.getJSON('/Morakot/toMktmoney', {
      
			Currency: $('#'+ currencyfield).val(),
			Money:$('#'+ moneyfield).val()
			              
		}, function(data) {
			$('#'+moneyfield).val(data.value)
			if (totalAmt !=""){

				sumAmount(totalAmt,moneyfield,$('#'+currencyfield).val())
				sumMoney();
			}
			
		});
				
   });

}


//Export record to csv

function exportCSV (objectClone,recordName){
	if (location.pathname.split('/')[3]!="Edit"){
		alert("No record to export...")
		return 0;	
	}
	csv =""
     $('input, select').each(function() {
     	   if (typeof ($(this).attr('name')) !='undefined'){
     	   	  
     	   	 	if ($(this).is("input") == true){
           			csv= csv + '\r\n'+($(this).attr('name') + "= " + $(this).val()); 
           		}
           		else if ($(this).is("select") == true) {
           			csv= csv + '\r\n'+($(this).attr('name') + "= " + $(this).select2('data').text);
           		}
       		}
        });
	// Data URI
    csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

    $(this)
            .attr({
            'download': recordName,
                'href': csvData,
                'target': '_blank'
        });
}


function ClickView(Title,Url,ID) {
	// you're in an iframe
	if ( self !== top ) {
		window.parent.addTab(Title+"-"+ID, Url+"/?ID="+ID);
	}else{
		window.parent.location.href="/Morakot/"+Url+"/?ID="+ID
	}
}
function ClickEdit(Title,Url,ID) {
	if ( self !== top ) {
		// you're in an iframe
		window.parent.addTab(Title+"-"+ID, Url+"/Edit/"+ID);
	}else{
		window.parent.location.href="/Morakot/"+Url+"/Edit/"+ID
	}
}

function setShortcutKey(){
	$(window).keydown(function(event) {
	
	// //Ctrl + C to Copy record to localStorage
	// if(event.ctrlKey && event.keyCode == 67) { 
	// 	copy('frm');
	// 	event.preventDefault(); 
	// }
	// //Ctrl + V to paste record from localStorage
	// if(event.ctrlKey && event.keyCode == 86) { 
	// 	paste('frm');
	// 	event.preventDefault(); 
	// }
	
	//ESC to back
	if(event.keyCode == 27) {
		var isBackVisible = $('#Back').length;
		if(isBackVisible==1){
			$("#Back").trigger("click");
		}else{
			$("#Cancel").trigger("click");
		}
		event.preventDefault(); 
	}
	//Ctrl + S to Save record
	if(event.ctrlKey && event.keyCode == 83) { 
		//alert("Hey! Ctrl+S event captured! ID="+url);
		$(':focus').blur();
		setTimeout(function () {
			$("#Save").trigger("click");
		}, 1000); // 1000 ==> 1 second
		
		event.preventDefault(); 
	}
	//Ctrl + E to Edit record
	if(event.ctrlKey && event.keyCode == 69) {
		$("#Edit").trigger("click");
		event.preventDefault(); 

	}

	// Ctrl + R Reverse
	if(event.ctrlKey && event.keyCode == 82) {
		$("#Reverse").trigger("click");
		event.preventDefault(); 

	}
	//Ctrl + L listLive
	if(event.ctrlKey && event.keyCode == 76) {
		$("#listLive").trigger("click");
		event.preventDefault(); 

	}
	//Ctrl + U list UnAuthorize
	if(event.ctrlKey && event.keyCode == 85) {
		$("#listAuth").trigger("click");
		event.preventDefault(); 

	}
	//Ctrl + H list Hisotry 72
	if(event.ctrlKey && event.keyCode == 72) {
		$("#listHist").trigger("click");
		event.preventDefault(); 

	}
	// Ctrl +N  New
	if(event.ctrlKey && event.keyCode == 78) {
		$("#New").trigger("click");
		event.preventDefault(); 
	}

});
}