from app.mktcore.imports import *
from .models import *
from .forms import *

registerCRUD(admin, '/Commune', 'Commune', FRM_COMMUNE, [MKT_COMMUNE])
