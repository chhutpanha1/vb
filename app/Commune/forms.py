from app.mktcore.wtfimports import *
from app.District.models import *

def loadDistrict():
	return MKT_DISTRICT.query

class FRM_COMMUNE(exform):
	Description = TextField(requiredlabel('Commune', '*'), [validators.Required()])
	District = QuerySelectField('District', query_factory = loadDistrict, get_label = u'Description', allow_blank = False, blank_text = u'None')