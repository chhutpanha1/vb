from app.mktcore.mdlimports import *

class MKT_COMMUNE(exmodel):
	ID = db.Column(db.String(7), primary_key = True)
	Description = db.Column(db.String(35))
	District = db.Column(db.String(5))

class MKT_COMMUNE_INAU(exmodel):
	ID = db.Column(db.String(7), primary_key = True)
	Description = db.Column(db.String(35))
	District = db.Column(db.String(5))

class MKT_COMMUNE_HIST(exmodel):
	ID = db.Column(db.String(12), primary_key = True)
	Description = db.Column(db.String(35))
	District = db.Column(db.String(5))