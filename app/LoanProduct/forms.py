from wtforms 					import DecimalField, IntegerField
from app.mktcore.wtfimports 	import *
from .models 					import *
import time
from app.tools.catetool 		import *
from app.tools.mktroute 		import *
from app.Interest.models 		import MKT_INTEREST
from app.Charge.models 			import MKT_CHARGE
from app.LoanPurpose.models 	import MKT_LOAN_PURPOSE
from app.Sector.models 			import MKT_SECTOR
from app.Industry.models 		import MKT_INDUSTRY
from app.LoanRule.models 		import MKT_LOAN_RULE
from app.Category.models 		import MKT_CATEGORY

def loadInterest():
	return MKT_INTEREST.query

def loadLoanPurpose():
	return MKT_LOAN_PURPOSE.query

def loadSector():
	return MKT_SECTOR.query

def loadIndustry():
	return MKT_INDUSTRY.query

def loadCategory():
	return MKT_CATEGORY.query

def loadLoanRule():
	return MKT_LOAN_RULE.query

class FRM_LOAN_PRODUCT(exform):

	Description 	= 	TextField(requiredlabel("Description", "*"), [validators.Required()])

	LNCategory 		= 	QuerySelectField(requiredlabel('Category', '*'),
	                        query_factory=loadCategory,
	                        get_label=lambda a: a.ID + " - " + a.Description,
	                        allow_blank=True,
	                        blank_text=u'--None--',
	                        validators=[validators.Required()]
                        )

	InterestKey = QuerySelectField(requiredlabel('Interest Key', '*'),
                        query_factory=loadInterest,
                        get_label=u'Description',
                        allow_blank=True,
                        blank_text=u'--None--',
                        validators=[validators.Required()])

	Rule = QuerySelectField(requiredlabel('Loan Rule', '*'),
                        query_factory=loadLoanRule,
                        get_label=u'Description',
                        allow_blank=True,
                        blank_text=u'--None--',
                        validators=[validators.Required()])

	MinAge = TextField("Minimum Age", [validators.Optional()])
	MaxAge = TextField("Maximum Age", [validators.Optional()])

	IntDayBasis = SelectField(requiredlabel('Interest Day Basis', '*'),
					choices=[('', '--None--'),
							 ('1', '360/360'),
							 ('2', '365/360'),
							 ('3', '365/365')],
					coerce=str,
					validators=[validators.Required()])

	IntAccrBasis = SelectField(requiredlabel('Interest Accrual Basis', '*'),
					choices=[('', '--None--'),
							 ('1', '1. Daily'),
							 ('2', '2. Weekly'),
							 ('3', '3. Monthly'),
							 ('4', '4. Yearly')],
					coerce=str,
					validators=[validators.Required()])

	LoanPurpose = QuerySelectField('Loan Purpose',
                        query_factory=loadLoanPurpose,
                        get_label=u'Description',
                        allow_blank=True,
                        blank_text=u'--None--')

	Sector = QuerySelectField('Sector',
                        query_factory=loadSector,
                        get_label=u'Description',
                        allow_blank=True,
                        blank_text=u'--None--')

	Industry = QuerySelectField('Industry',
                        query_factory=loadIndustry,
                        get_label=u'Description',
                        allow_blank=True,
                        blank_text=u'--None--')

	DeliqMode = SelectField(requiredlabel('Deliquency Mode', '*'),
					choices=[('', '--None--'),
							 ('1', '1. Manual'),
							 ('2', '2. Automatic'),
							 ('3', '3. Semi-Automatic')],
					coerce=str,
					validators=[validators.Required()])

	RepMode = SelectField(requiredlabel('Repayment Mode', '*'),
					choices=[('', '--None--'),
							 ('1', '1. Declining'),
							 ('2', '2. Annuity'),
							 ('3', '3. Flat'),
							 ('4', '4. IRR')],
					coerce=str,
					validators=[validators.Required()])
	
	BaseDateKey = SelectField(requiredlabel('Base Date Key', '*'),
					choices=[('', '--None--'),
							 ('1', '1. Value Date'),
							 ('2', '2. Previous Date'),
							 ('3', '3. First Date')],
					coerce=str,
					validators=[validators.Required()])

	FWDBWDKey = SelectField(requiredlabel('Forward/Backward Key', '*'),
					choices=[('', '--None--'),
							 ('1', '1. Backward within month'),
							 ('2', '2. Forward within month'),
							 ('3', '3. Backward'),
							 ('4', '4. Forward')],
					coerce=str,
					validators=[validators.Required()])

	# IntReceivableCate = HiddenField(requiredlabel("Accrued Interest Receivable Category", "*"), [validators.Required()])
	# IntIncomeCate = HiddenField(requiredlabel("Interest Income Category", "*"), [validators.Required()])
	IntReceivableCate 	= 	QuerySelectField(requiredlabel('Accrued Interest Receivable Category', '*'),
		                        query_factory=loadCategory,
		                        get_label=lambda a: a.ID + " - " + a.Description,
		                        allow_blank=True,
		                        blank_text=u'--None--',
		                        validators=[validators.Required()]
	                        )

	IntIncomeCate 		= 	QuerySelectField(requiredlabel('Interest Income Category', '*'),
		                        query_factory=loadCategory,
		                        get_label=lambda a: a.ID + " - " + a.Description,
		                        allow_blank=True,
		                        blank_text=u'--None--',
		                        validators=[validators.Required()]
	                        )

	ReduceAmount 		=	TextField("Minimum Reduce Perc") # Percentage of reduce amount on loan amendment

	def validate_MinAge(form, field):
		try:

			Min = form.MinAge.data
			if Min:
				float(Min)

		except:
			raise ValidationError("This field must be in numeric format.")

	def validate_MaxAge(form, field):
		Min = form.MinAge.data
		Max = form.MaxAge.data

		try:
			if Max:
				Min = float(Min)
				Max = float(Max)

		except:
			raise ValidationError("This field must be in numeric format.")

		if Max <= Min:
			raise ValidationError("Maximum age must bigger than minimum age.")

	def validate_ReduceAmount(form, field):

		ReduceAmount = request.form['ReduceAmount']
		
		if ReduceAmount:
			try:
				ReduceAmount = float(ReduceAmount)
			except:
				raise ValidationError("This field must be in numeric format.")

			if ReduceAmount > 100:
				raise ValidationError("Reduce amount must not be bigger than 100.")

	@staticmethod
	def setWidth():
		control_list= [('MinTerm', len1),
					   ('MaxTerm', len1),
					   ('MinAge', len1),
					   ('MaxAge', len1),
					   ('IntDayBasis', len2),
					   ('IntAccrBasis', len2),
					   ('DeliqMode', len2),
					   ('BaseDateKey', len2),
					   ('FWDBWDKey', len3)]

		return control_list

	@staticmethod
	def listField():
		Fields = [
					"ID",
					"Description",
					"LNCategory",
					"InterestKey",
					"Rule",
					"MinAge",
					"MaxAge"
				]
		return Fields