from app.mktcore.imports 			import *
from .. 							import app

from app.AutoID.models 				import MKT_SEQUENCE

import app.tools.mktdate 			as mktdate
import app.mktcore.constant

def setAutoID(AppName="", Digit=1, Table=""):
	try:
		Auto 			= MKT_SEQUENCE.query.get(Table)
		BankDateObj 	= mktdate.getBankDateObj()
		Julian 			= BankDateObj.JulianDate
		if not Julian :
			Julian = mktdate.getJulianDay(BankDateObj.SystemDate)
		if not Auto:
			Auto 	= 	MKT_SEQUENCE(
							ID 			=	str(Table),
							Current		=	0,
							Increment	=	1
					  	)
			db.session.add(Auto)
			Auto 	= 	MKT_SEQUENCE.query.get(Table)

		newVal 			= 	int(Auto.Current) + int(Auto.Increment)
		Auto.Current 	= 	newVal
		db.session.add(Auto)

		strFormat 		= 	'{:0>' + str(Digit) + 'd}'
		newVal 			= 	strFormat.format(newVal)
		ID 				= 	str(AppName) + str(Julian) + str(newVal)

		return ID

	except:
		db.session.rollback()
		raise

def resetSequence():
	try:
		Auto = MKT_SEQUENCE.query.all()
		if Auto:
			for row in Auto:
				reset = MKT_SEQUENCE.query.get(row.ID)
				reset.Current = 0

				db.session.add(reset)

		return "Sequence is updated."
	except:
		db.session.rollback()
		raise