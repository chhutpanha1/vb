from .. 							import app, db
from decimal 						import *
from datetime 						import datetime, date, timedelta
from sqlalchemy 					import *

import mktaccounting	 			as mktaccounting
import mktmoney 					as mktmoney
import mktdate						as mktdate
import mktparam 					as mktParam
import mktsetting 					as mktsetting
import mktloan 						as mktloan
import mktautoid 					as mktAutoID
import mktkey 						as mktkey
import mktmessage 					as mktmessage
import loantools.rescheduletools 	as mktreschedule
import mktbjstat 					as mktbjstat

from app.mktcore.constant			import *

def getTotODAmount(PDID):
	try:

		Amount 	= 0
		Obj 	= MKT_PAST_DUE.query.get(PDID)

		if Obj:
			Amount = Obj.TotPrincipalDue

		return Amount

	except:
		mktbjstat.makeLogFileOnError('Y')
		raise

def transferPD(ID="", PDID="", DueDate="", NumDayDue=0, ODStatus="PRE", TotODAmount="",
			   OutAmount="", PrincipalDue="", OutPriAmount="", InterestDue="", OutIntAmount="",
			   PenaltyDue="", OutPenAmount="", ChargeDue="", OutChgAmount=""
			  ):
	try:

		PD_DATE_TRAN = MKT_PD_DATE_HIST(
					ID 				= 	ID,
					PDID 			=	PDID,
					DueDate 		=	DueDate,
					NumDayDue		=	NumDayDue,
					ODStatus 		=	ODStatus,
					TotODAmount 	=	TotODAmount,
					OutAmount 		=	OutAmount,
					PrincipalDue 	=	PrincipalDue,
					OutPriAmount 	=	OutPriAmount,
					InterestDue 	=	InterestDue,
					OutIntAmount 	=	OutIntAmount,
					PenaltyDue 		=	PenaltyDue,
					OutPenAmount 	=	OutPenAmount,
					ChargeDue 		=	ChargeDue,
					OutChgAmount 	=	OutChgAmount
				  )

		db.session.add(PD_DATE_TRAN)
		db.session.query(MKT_PD_DATE).filter(MKT_PD_DATE.PDID == PDID).delete()
		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def updatePD(ID, PDID, PD_Penalty, PD_Charge, PD_Interest, PD_Principal):
	try:
		
		Total = float(PD_Penalty) + float(PD_Charge) + float(PD_Interest) + float(PD_Principal)
	
		if Total > 0:
			PD_UPDATE = MKT_PAST_DUE.query.get(ID)
			if PD_UPDATE:
				PD_UPDATE.TotODAmount 		= float(PD_UPDATE.TotODAmount) - Total
				PD_UPDATE.TotPrincipalDue 	= float(PD_UPDATE.TotPrincipalDue) - float(PD_Principal)
				PD_UPDATE.TotInterestDue 	= float(PD_UPDATE.TotInterestDue) - float(PD_Interest)
				PD_UPDATE.TotPenaltyDue 	= float(PD_UPDATE.TotPenaltyDue) - float(PD_Penalty)
				PD_UPDATE.TotChargeDue 		= float(PD_UPDATE.TotChargeDue) - float(PD_Charge)

				db.session.add(PD_UPDATE)

			PD_DATE_UPDATE = MKT_PD_DATE.query.\
					  filter(MKT_PD_DATE.PDID == PDID).\
					  filter(MKT_PD_DATE.ID == ID).\
					  first()

			if PD_DATE_UPDATE:
				PD_DATE_UPDATE.OutAmount 		= float(PD_DATE_UPDATE.OutAmount) - float(Total)
				PD_DATE_UPDATE.OutPriAmount 	= float(PD_DATE_UPDATE.OutPriAmount) - float(PD_Principal)
				PD_DATE_UPDATE.OutIntAmount 	= float(PD_DATE_UPDATE.OutIntAmount) - float(PD_Interest)
				PD_DATE_UPDATE.OutPenAmount 	= float(PD_DATE_UPDATE.OutPenAmount) - float(PD_Penalty)
				PD_DATE_UPDATE.OutChgAmount 	= float(PD_DATE_UPDATE.OutChgAmount) - float(PD_Charge)

				db.session.add(PD_DATE_UPDATE)

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setNumDayDue(PDID=""):
	try:

		PD_Date_Num = MKT_PD_DATE.query.\
					  filter(MKT_PD_DATE.ID == PDID).\
					  all()

		if PD_Date_Num:
			for row in PD_Date_Num:
				From 			= row.DueDate
				To 				= mktdate.getBankDate()
				NumOfDay 		= mktdate.getDateDiff(From, To)
				row.NumDayDue 	= NumOfDay + 1
				db.session.add(row)

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def checkPDStatus(ID="", PDID=""):
	try:

		CheckNumDayDue = MKT_PD_DATE.query.\
						 order_by(MKT_PD_DATE.DueDate.asc()).\
						 filter(MKT_PD_DATE.ID == ID).\
						 first()

		PRE  = mktParam.getPDParam().PRE
		GRA  = mktParam.getPDParam().GRA
		PDO  = mktParam.getPDParam().PDO
		NAB  = mktParam.getPDParam().NAB

		NumberOfOverDue = 0
		if CheckNumDayDue:
			NumberOfOverDue = int(CheckNumDayDue.NumDayDue) if CheckNumDayDue.NumDayDue else int(0)

		if int(NumberOfOverDue) <= int(NAB):

			PD_DATE_PEN = MKT_PD_DATE.query.\
					  filter(MKT_PD_DATE.PDID == PDID).\
					  first()

			if PD_DATE_PEN:

				NumDayDue 	= int(PD_DATE_PEN.NumDayDue)

				NumOfDay = mktdate.getNumOfNonWorkingDay()

				if int(NumDayDue) > 0 and int(NumDayDue) <= int(PRE):
					Status = "PRE"

				elif int(NumDayDue) >= int(GRA) and int(NumDayDue) < int(PDO):
					Status = "GRA"

				elif int(NumDayDue) >= int(PDO) and int(NumDayDue) < int(NAB):
					Status = "PDO"

				else:
					Status = "NAB"

				PD_DATE_PEN.ODStatus = Status
				db.session.add(PD_DATE_PEN)

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setPenalty(ID="", PDID=""):
	try:
		
		LoanID 		=	ID[2:]
		DoPenalty 	=	"Y"
		LC 			= 	MKT_LOAN_CONTRACT.query.get(LoanID)

		if LC:

			DoPenalty = LC.Penalty


		if DoPenalty == 'Y':
			
			CheckNumDayDue 	= 	MKT_PD_DATE.query.\
								order_by(MKT_PD_DATE.DueDate.asc()).\
								filter(MKT_PD_DATE.ID == ID).\
								first()

			NumberOfOverDue = 0
			if CheckNumDayDue:
				NumberOfOverDue = int(CheckNumDayDue.NumDayDue) if CheckNumDayDue.NumDayDue else int(0)

			PDParamObj 		=	mktParam.getPDParam()
			PRE  			= 	PDParamObj.PRE
			GRA  			= 	PDParamObj.GRA
			PDO  			= 	PDParamObj.PDO
			NAB  			= 	PDParamObj.NAB
			PenCalBase 		= 	PDParamObj.PenCalBase
			Type 			= 	PDParamObj.PenType.strip()
			Rate 			= 	PDParamObj.PenaltyRate.strip()
			SystemBankDate 	= 	mktdate.getBankDate()
			
			if int(NumberOfOverDue) <= int(NAB):

				PD_DATE_PEN = MKT_PD_DATE.query.\
						  filter(MKT_PD_DATE.PDID == PDID).\
						  first()

				if PD_DATE_PEN:
					ID 			= 	PD_DATE_PEN.ID
					PD_PEN 		= 	MKT_PAST_DUE.query.get(ID)
					# Type 		= 	PD_PEN.PenaltyType.strip()
					# Rate 		= 	PD_PEN.PenaltyRate.strip()
					Currency 	= 	PD_PEN.Currency.strip()

					NumDayDue 	= 	int(PD_DATE_PEN.NumDayDue)
					Amount 		= 	0
					Principal 	= 	float(PD_DATE_PEN.OutPriAmount)
					Interest 	= 	float(PD_DATE_PEN.OutIntAmount)
					Penalty 	= 	float(PD_DATE_PEN.OutPenAmount)
					Charge 		= 	float(PD_DATE_PEN.OutChgAmount)

					NumOfDay 	= 	mktdate.getNumOfNonWorkingDay()

					if int(NumDayDue) > 0 and int(NumDayDue) <= int(PRE):
						Status = "PRE"

					elif int(NumDayDue) >= int(GRA) and int(NumDayDue) < int(PDO):
						Status = "GRA"

					elif int(NumDayDue) >= int(PDO) and int(NumDayDue) < int(NAB):
						Status = "PDO"

					else:
						Status = "NAB"

					Rate 		= Rate.split()
					Filter_Rate = []

					if len(Rate) > 0:
						Filter_Rate	= [item for item in Rate if str(Currency) in item]

					if len(Filter_Rate) > 0:
						Amount = str(Filter_Rate[0])
						Amount = float(Amount[3:])
					else:
						Amount = float(0)

					TotalPrincipal 	= float(0)
					TotalInterest 	= float(0)
					TotalCharge 	= float(0)
					TotalPenalty 	= float(0)

					if Type == 'F':

						TotalAmount = float(Amount)

					elif Type == 'I':

						InterestDayBasis 	=	int(1)
						RatePerYear 		= 	float(0)
						TotalAmount = ((float(Interest) * float(Amount)) / float(100)) / float(30)

					elif Type == 'P':

						if PenCalBase:
							
							PenCalBase = PenCalBase.split()
							
							for pen in PenCalBase:
								if pen == 'PR':
									TotalPrincipal 	= float(Principal) 	* float(Amount) / float(100)
								elif pen == 'IN':
									TotalInterest 	= float(Interest) 	* float(Amount) / float(100)
								elif pen == 'CH':
									TotalCharge 	= float(Charge) 	* float(Amount) / float(100)
								else:
									TotalPenalty 	= float(Penalty) 	* float(Amount) / float(100)
							
							TotalAmount = TotalPrincipal + TotalInterest + TotalCharge + TotalPenalty
						
						else:
							TotalAmount = float(0)

					TotalAmount = float(TotalAmount) * float(NumOfDay)
					# print "Total Penalty: %s." %TotalAmount
					# print "Number of day: %s." %NumOfDay

					# Update MKT_PD_DATE Status
					PD_DATE_PEN.ODStatus = Status
					db.session.add(PD_DATE_PEN)
					SubTotal = float(0)
					if int(NumDayDue) >= int(GRA):

						StrTotalAmount = mktmoney.toMoney(float(TotalAmount), mktmoney.getCurrencyObj(Currency))
						
						if StrTotalAmount and StrTotalAmount.find(',') != -1:

							StrTotalAmount = str(StrTotalAmount).replace(',', '')

						TotalAmount = float(StrTotalAmount)
						# Update MKT_PAST_DUE after Penalty is calculated
						PD_PEN.TotODAmount 			= 	float(PD_PEN.TotODAmount) + float(TotalAmount)
						PD_PEN.TotPenaltyDue 		= 	float(PD_PEN.TotPenaltyDue) + float(TotalAmount)
						db.session.add(PD_PEN)

						PD_DATE_PEN_I = MKT_PD_DATE.query.\
										filter(MKT_PD_DATE.PDID == PDID).\
										first()
										
						# Update MKT_PD_DATE after Penalty is calculated
						PD_DATE_PEN_I.TotODAmount 	= 	float(PD_DATE_PEN_I.TotODAmount) + float(TotalAmount)
						PD_DATE_PEN_I.OutAmount 	= 	float(PD_DATE_PEN_I.OutAmount) + float(TotalAmount)
						PD_DATE_PEN_I.PenaltyDue 	= 	float(PD_DATE_PEN_I.PenaltyDue) + float(TotalAmount)
						PD_DATE_PEN_I.OutPenAmount 	= 	float(PD_DATE_PEN_I.OutPenAmount) + float(TotalAmount)
						
						SubTotal = PD_DATE_PEN_I.OutPenAmount

						db.session.add(PD_DATE_PEN_I)
						# print "Total penalty amount: " + str(TotalAmount) + " for loan " + str(ID[2:])

					# Booking Penalty Income and Receivable
					# if int(NumDayDue) == int(PDO):
					# 	LC_ID = PD_PEN.ID[2:]
					# 	# get single Loan Contract record
					# 	LC = MKT_LOAN_CONTRACT.query.get(LC_ID)
					# 	Account = ""
					# 	if LC:
					# 		ID 			= PD_PEN.ID
					# 		AssClass 	= LC.AssetClass
					# 		LC_PRODUCT 	= LC.LoanProduct
					# 		Branch 		= LC.Branch
					# 		Account 	= LC.Account
					# 		CustomerID 	= LC.ContractCustomerID
					# 		Currency 	= LC.Currency
					# 		Amount 		= SubTotal
					# 		TranDate 	= SystemBankDate

					# 		# Define parameter for Debit Penalty Receivable Category
					# 		DrCr  		= 'Dr'
					# 		Category 	= mktParam.getPDParam().PenaltyRecCat
					# 		if not Category:
					# 			print "Penalty receivable category not found."
					# 			db.session.rollback()

					# 		Transaction = mktsetting.getAccSetting().PenaltyTran
					# 		GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
					# 		DateTimeNow = mktdate.getDateTimeNow()
					# 		Mode 		= "Direct"

					# 		if Transaction:
					# 			if float(Amount) > 0:
					# 				# Debit Penalty Receivable Category
					# 				mktaccounting.postAccounting(
					# 					"AUTH", 				# Status
					# 					"0", 					# Curr
					# 					"system",				# Inputter
					# 					DateTimeNow, 			# Createdon
					# 					"system",				# Authorizer
					# 					DateTimeNow,			# Authorizeon
					# 					"", 					# AEID
					# 					Account,				# Account
					# 					Category,				# Category
					# 					Currency,				# Currency
					# 					DrCr,					# DrCr
					# 					Decimal(Amount), 		# Amount
					# 					"LC",					# Module
					# 					Transaction, 			# Transaction
					# 					TranDate, 				# TransactionDate
					# 					ID, 					# Reference
					# 					"", 					# Note
					# 					"", 					# JNID
					# 					Branch,					# Branch
					# 					GL_KEYS,				# GL_KEYS
					# 					Mode 					# Mode check to insert Journal for category
					# 				)

					# 				# Define new parameter for Credit Penalty Income Category
					# 				DateTimeNow = mktdate.getDateTimeNow()
					# 				DrCr 		= "Cr"
					# 				Mode 		= "Direct"
					# 				Category 	= mktParam.getPDParam().ODPenaltyCat
					# 				if not Category:
					# 					print "Penalty income category not found."
					# 					db.session.rollback()

					# 				GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
					# 				# Credit Penalty Income Category
					# 				mktaccounting.postAccounting(
					# 					"AUTH", 				# Status
					# 					"0", 					# Curr
					# 					"system",				# Inputter
					# 					DateTimeNow, 			# Createdon
					# 					"system",				# Authorizer
					# 					DateTimeNow,			# Authorizeon
					# 					"", 					# AEID
					# 					Account,				# Account
					# 					Category,				# Category
					# 					Currency,				# Currency
					# 					DrCr,					# DrCr
					# 					Decimal(Amount), 		# Amount
					# 					"LC",					# Module
					# 					Transaction, 			# Transaction
					# 					TranDate, 				# TransactionDate
					# 					ID, 					# Reference
					# 					"", 					# Note
					# 					"", 					# JNID
					# 					Branch,					# Branch
					# 					GL_KEYS,				# GL_KEYS
					# 					Mode 					# Mode check to insert Journal for category
					# 				)

					# 		else:
					# 			print "Penalty collection transaction not found."
					# 			db.session.rollback()

					# # Debit Customer Account and Credit Penalty Income Category When Matched the Setting
					# if int(NumDayDue) > int(PDO) and int(NumDayDue) < int(NAB):
						
					# 	LC_ID = PD_PEN.ID[2:]
					# 	# get single Loan Contract record
					# 	LC = MKT_LOAN_CONTRACT.query.get(LC_ID)
					# 	Account = ""
					# 	if LC:
					# 		ID 			= PD_PEN.ID
					# 		AssClass 	= LC.AssetClass
					# 		LC_PRODUCT 	= LC.LoanProduct
					# 		Branch 		= LC.Branch
					# 		Account 	= LC.Account
					# 		CustomerID 	= LC.ContractCustomerID
					# 		Currency 	= LC.Currency
					# 		Amount 		= TotalAmount
					# 		TranDate 	= SystemBankDate

					# 		# Define parameter for Debit Customer Account
					# 		DrCr  		= 'Dr'
					# 		Category 	= mktParam.getPDParam().PenaltyRecCat
					# 		if not Category:
					# 			db.session.rollback()
					# 			print "Penalty income category not found."

					# 		Transaction = mktsetting.getAccSetting().PenaltyTran
					# 		GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
					# 		DateTimeNow = mktdate.getDateTimeNow()
					# 		Mode 		= "Direct"

					# 		if Transaction:
					# 			if float(Amount) > 0:
					# 				# Debit Customer Account
					# 				mktaccounting.postAccounting(
					# 					"AUTH", 				# Status
					# 					"0", 					# Curr
					# 					"system",				# Inputter
					# 					DateTimeNow, 			# Createdon
					# 					"system",				# Authorizer
					# 					DateTimeNow,			# Authorizeon
					# 					"", 					# AEID
					# 					Account,				# Account
					# 					Category,				# Category
					# 					Currency,				# Currency
					# 					DrCr,					# DrCr
					# 					Decimal(Amount), 		# Amount
					# 					"LC",					# Module
					# 					Transaction, 			# Transaction
					# 					TranDate, 				# TransactionDate
					# 					ID, 					# Reference
					# 					"", 					# Note
					# 					"", 					# JNID
					# 					Branch,					# Branch
					# 					GL_KEYS,				# GL_KEYS
					# 					Mode 					# Mode check to insert Journal for category
					# 				)

					# 				# Define new parameter for Credit Penalty Category
					# 				DateTimeNow = mktdate.getDateTimeNow()
					# 				DrCr 		= "Cr"
					# 				Mode 		= "Direct"
					# 				Category 	= mktParam.getPDParam().ODPenaltyCat
					# 				if not Category:
					# 					db.session.rollback()
					# 					print "Penalty receivable category not found."

					# 				GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
					# 				# Credit Penalty Category
					# 				mktaccounting.postAccounting(
					# 					"AUTH", 				# Status
					# 					"0", 					# Curr
					# 					"system",				# Inputter
					# 					DateTimeNow, 			# Createdon
					# 					"system",				# Authorizer
					# 					DateTimeNow,			# Authorizeon
					# 					"", 					# AEID
					# 					Account,				# Account
					# 					Category,				# Category
					# 					Currency,				# Currency
					# 					DrCr,					# DrCr
					# 					Decimal(Amount), 		# Amount
					# 					"LC",					# Module
					# 					Transaction, 			# Transaction
					# 					TranDate, 				# TransactionDate
					# 					ID, 					# Reference
					# 					"", 					# Note
					# 					"", 					# JNID
					# 					Branch,					# Branch
					# 					GL_KEYS,				# GL_KEYS
					# 					Mode 					# Mode check to insert Journal for category
					# 				)

					# 		else:
					# 			print "Penalty collection transaction not found."
					# 			db.session.rollback()

				else:
					# print "No PD record."
					print_msg = "No PD record."
					mktmessage.msgOutputMsg(print_msg)
			# else:
			# 	print "Loan %s no penalty." %LoanID

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def getProvisioningAmount(AssClass, TotPrincipalDue, LoanType, MoreThanOneYear, NumDayDue):
	try:
		
		Rate 	= 0
		Check 	= None
		ASS_PRO = MKT_ASSET_CLASS_PRO.query.\
				  filter(MKT_ASSET_CLASS_PRO.ID == str(AssClass)).\
				  filter(MKT_ASSET_CLASS_PRO.LoanType == str(LoanType)).\
				  filter(MKT_ASSET_CLASS_PRO.MoreThanOneYear == str(MoreThanOneYear)).\
				  all()

		if ASS_PRO:

			for item in ASS_PRO:
				
				Ass_OverDueFr 	= 	item.OverdueFr if item.OverdueFr else "0"
				Ass_OverdueTo	= 	item.OverdueTo if item.OverdueTo else "0"
				PercentagRate 	= 	float(item.ProvPerc) if item.ProvPerc else float(0)

				if str(Ass_OverdueTo).upper() == "UP" or str(Ass_OverdueTo) == "":
					if int(NumDayDue) >= int(Ass_OverDueFr):
						Rate 	= float(TotPrincipalDue) * float(PercentagRate) / float(100)
						Check 	= 1
				else:
					if int(NumDayDue) >= int(Ass_OverDueFr) and int(NumDayDue) <= int(Ass_OverdueTo):
						Rate 	= float(TotPrincipalDue) * float(PercentagRate) / float(100)
						Check 	= 1

				if Check:
					break

		return Rate

	except:
		mktbjstat.makeLogFileOnError('Y')
		raise

def getLoanAssetClass(NumDayDue, LoanType, MoreThanOneYear):
	try:

		AssetClass = ""
		AssetPro = MKT_ASSET_CLASS_PRO.query.\
				   order_by(MKT_ASSET_CLASS_PRO.ID.asc()).\
				   filter(MKT_ASSET_CLASS_PRO.LoanType == str(LoanType)).\
				   filter(MKT_ASSET_CLASS_PRO.MoreThanOneYear == str(MoreThanOneYear)).\
				   all()
				   
		if AssetPro:
			for item in AssetPro:
				Ass_ID 			= 	item.ID
				Ass_OverDueFr 	= 	item.OverdueFr if item.OverdueFr else "0"
				Ass_OverdueTo	= 	item.OverdueTo if item.OverdueTo else "0"

				if str(Ass_OverdueTo).upper() == "UP" or str(Ass_OverdueTo) == "":
					if int(NumDayDue) >= int(Ass_OverDueFr):
						AssetClass = Ass_ID
				else:
					if int(NumDayDue) >= int(Ass_OverDueFr) and int(NumDayDue) <= int(Ass_OverdueTo):
						AssetClass = Ass_ID

				if AssetClass != "":
					break

		return AssetClass

	except:
		mktbjstat.makeLogFileOnError('Y')
		raise

def getMaxNumDayDue(ID):
	NumDayDue = ''
	
	PDDateObj 	= MKT_PD_DATE.query.filter(MKT_PD_DATE.ID==ID).\
									order_by(MKT_PD_DATE.NumDayDue.desc()).\
									limit(1).first()
	if PDDateObj:
		NumDayDue=str(PDDateObj.NumDayDue)
	# print 'iam Here'
	# print PDDateObj
	# print ID
	return NumDayDue

def getNumDayDue(PD_ID):
	try:

		NumDayDue 	= '0'
		ASS_PD 		= MKT_PAST_DUE.query.\
					  filter(MKT_PAST_DUE.ID == PD_ID).\
					  filter(MKT_PAST_DUE.TotODAmount > 0).\
					  first()

		# ======== Asign Loan Classification =========
		
		ASS_CLASS = MKT_ASSET_CLASS.query.\
					order_by(asc(MKT_ASSET_CLASS.Authorizeon)).\
					all()

		if ASS_PD:
			
			if ASS_CLASS:

				ASS_PD_DATE = MKT_PD_DATE.query.\
							  order_by(MKT_PD_DATE.DueDate.asc()).\
							  filter(MKT_PD_DATE.ID == str(ASS_PD.ID)).\
							  all()

				for ass_row in ASS_CLASS:

					if ASS_PD_DATE:

						for row in ASS_PD_DATE:
									
							Ass_AmountType = str(ass_row.AmountType)
							if Ass_AmountType:
								
								Ass_AmountType = Ass_AmountType.split()
								if len(Ass_AmountType) > 0:
									for item in Ass_AmountType:
										if str(item).upper() == 'PR':
											if float(row.OutPriAmount) > 0:
												NumDayDue = row.NumDayDue

										elif str(item).upper() == 'IN':
											if float(row.OutIntAmount) > 0:
												NumDayDue = row.NumDayDue

										elif str(item).upper() == 'CH':
											if float(row.OutChgAmount) > 0:
												NumDayDue = row.NumDayDue

										else:
											if float(row.OutPenAmount) > 0:
												NumDayDue = row.NumDayDue

							if NumDayDue != '0':
								break

					if NumDayDue != '0':
						break
				
			else:
				print "No asset class record."

		return NumDayDue

	except:
		mktbjstat.makeLogFileOnError('Y')
		raise

def provisioningBooking(ProvisionAmount, ProvExpCat, ProResvCat, Account, Currency, TranDate, ID, Branch, AssClass):
	try:

		if float(ProvisionAmount) == 0:
			print "%s provision is now %s." %(ID, mktmoney.toMoney(float(0), mktmoney.getCurrencyObj(Currency), 2))
		else:
			DrCr  		= 'Dr'
			Category 	= ProvExpCat
			Transaction = mktsetting.getAccSetting().ProvisionTran
			GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "", "", "", "", "", "", "", "", "", "")
			DateTimeNow = mktdate.getDateTimeNow()
			Mode 		= "Direct"

			if Transaction:

				# Debit Customer Account
				mktaccounting.postAccounting(
					"AUTH", 				# Status
					"0", 					# Curr
					"system",				# Inputter
					DateTimeNow, 			# Createdon
					"system",				# Authorizer
					DateTimeNow,			# Authorizeon
					"", 					# AEID
					Account,				# Account
					Category,				# Category
					Currency,				# Currency
					DrCr,					# DrCr
					Decimal(ProvisionAmount), # Amount
					"LC",					# Module
					Transaction, 			# Transaction
					TranDate, 				# TransactionDate
					ID, 					# Reference
					"", 					# Note
					"", 					# JNID
					Branch,					# Branch
					GL_KEYS,				# GL_KEYS
					Mode 					# Mode check to insert Journal for category
				)

				# Define new parameter for Credit Penalty Category
				DateTimeNow = mktdate.getDateTimeNow()
				DrCr 		= "Cr"
				Mode 		= "Direct"
				Category 	= ProResvCat
				GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "", "", "", "", "", "", "", "", "", "")
				# Credit Penalty Category
				mktaccounting.postAccounting(
					"AUTH", 				# Status
					"0", 					# Curr
					"system",				# Inputter
					DateTimeNow, 			# Createdon
					"system",				# Authorizer
					DateTimeNow,			# Authorizeon
					"", 					# AEID
					Account,				# Account
					Category,				# Category
					Currency,				# Currency
					DrCr,					# DrCr
					Decimal(ProvisionAmount), # Amount
					"LC",					# Module
					Transaction, 			# Transaction
					TranDate, 				# TransactionDate
					ID, 					# Reference
					"", 					# Note
					"", 					# JNID
					Branch,					# Branch
					GL_KEYS,				# GL_KEYS
					Mode 					# Mode check to insert Journal for category
				)

				# print "%s provision is now %s." %(ID, mktmoney.toMoney(float(ProvisionAmount), mktmoney.getCurrencyObj(Currency), 2))
				print_msg = "%s provision is now %s." %(ID, mktmoney.toMoney(float(ProvisionAmount), mktmoney.getCurrencyObj(Currency), 2))
				mktmessage.msgOutputMsg(print_msg)

				PROV = MKT_PROVISION.query.\
						filter(MKT_PROVISION.LOANID == ID).\
						filter(MKT_PROVISION.ASSETID == AssClass).\
						first()

				if not PROV:

					PROV = MKT_PROVISION(
								LOANID 		= 	ID,
								ASSETID 	=	AssClass,
								TranDate 	=	TranDate,
								Amount 		=	ProvisionAmount,
								Branch 		=	Branch
						   )

				PROV.ASSETID 	= AssClass
				PROV.TranDate 	= TranDate
				PROV.Amount 	= ProvisionAmount
				db.session.add(PROV)

			else:
				print "Provision booking transaction not found."
				db.session.rollback()

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def reverseProvisioning(ID, PrevAss, ProResvCat, ProvExpCat, Currency, Account, TranDate, Branch):
	try:

		ProvToReverse = MKT_PROVISION.query.\
								filter(MKT_PROVISION.LOANID == ID).\
								filter(MKT_PROVISION.ASSETID == PrevAss).\
								first()
								
		if ProvToReverse:
			# Define parameter for Debit Customer Account
			DrCr  		= 'Dr'
			Category 	= ProResvCat
			Transaction = mktsetting.getAccSetting().ProvReverTran
			GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "", "", "", "", "", "", "", "", "", "")
			DateTimeNow = mktdate.getDateTimeNow()
			Mode 		= "Direct"
			PrevAmount 	= float(ProvToReverse.Amount) if ProvToReverse.Amount else float(0)

			if Transaction:

				if float(PrevAmount) > 0:
						
					# Debit Provision Receivable Category
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"system",				# Inputter
						DateTimeNow, 			# Createdon
						"system",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						Account,				# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(PrevAmount), 	# Amount
						"LC",					# Module
						Transaction, 			# Transaction
						TranDate, 				# TransactionDate
						ID, 					# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)

					# Define new parameter for Credit Penalty Category
					DateTimeNow = mktdate.getDateTimeNow()
					DrCr 		= "Cr"
					Mode 		= "Direct"
					Category 	= ProvExpCat
					GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "", "", "", "", "", "", "", "", "", "")
					# Credit Provision Reversed Category
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"system",				# Inputter
						DateTimeNow, 			# Createdon
						"system",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						Account,				# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(PrevAmount), 	# Amount
						"LC",					# Module
						Transaction, 			# Transaction
						TranDate, 				# TransactionDate
						ID, 					# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)
					
			else:
				print "Provision reversal transaction not found."
				db.session.rollback()


			if float(PrevAmount) > 0:

				PROV_Reverse = MKT_PROVISION.query.\
						filter(MKT_PROVISION.LOANID == ID).\
						filter(MKT_PROVISION.ASSETID == PrevAss).\
						first()

				PROV_Reverse.Amount = 0
				db.session.add(PROV_Reverse)

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def getAssetClassReg(ID):
	try:

		Asset 		= MKT_ASSET_CLASS.query.get(ID)
		IncomeRecog = ""
		if Asset:
			IncomeRecog = Asset.IncomeRecog

		return IncomeRecog

	except:
		mktbjstat.makeLogFileOnError('Y')
		raise

def loanClassification(LoanID="", EOD=1, Stat="", Date="", Check="Yes"):
	try:
		
		# SystemBankDate = mktdate.getBankDate()
		if Date:
			SystemBankDate 	= Date
		else:
			SystemBankDate 	= mktdate.getBankDate()

		LC 	= 	db.session.query(MKT_LOAN_CONTRACT.ID, MKT_LOAN_CONTRACT.LoanType, MKT_LOAN_CONTRACT.MoreThanOneYear, MKT_LOAN_CONTRACT.AssetClass, MKT_LOAN_CONTRACT.LoanProduct, MKT_LOAN_CONTRACT.Branch, MKT_LOAN_CONTRACT.Account, MKT_LOAN_CONTRACT.ContractCustomerID, MKT_LOAN_CONTRACT.Currency, MKT_LOAN_CONTRACT.Amount, MKT_LOAN_CONTRACT.Term, MKT_ASSET_CLASS.ProResvCat, MKT_ASSET_CLASS.ProvExpCat).\
				join(MKT_ASSET_CLASS, MKT_ASSET_CLASS.ID == MKT_LOAN_CONTRACT.AssetClass).\
				order_by(asc(MKT_LOAN_CONTRACT.Authorizeon)).\
				filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
				filter(MKT_LOAN_CONTRACT.DisbursedStat == 'Y')

				# filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\

		if LoanID:
			LC 	=	LC.\
					filter(MKT_LOAN_CONTRACT.ID == LoanID)

		LC_Count = LC.count()
		if Check == 'Yes':
			if Stat == "1":

				return LC_Count

		LC 	=	LC.\
				all()

		if Check == 'Yes':
			# Check if no record set progress bar done.
			mktbjstat.calCompletedPer('CL', 0, 1, 0)

		if LC:

			NumberOfCompleted 	= 	0
			RecordNumber 		=	0
			NumOfCompletedPerc 	=	0
			NumOfTransaction 	=	mktbjstat.setNumOfTransaction(LC_Count)

			for lc_row in LC:
				
				if Check == 'Yes':
					# Block update BjStat
					RecordNumber 		+= 1
					NumberOfCompleted 	+= 1

					if int(NumberOfCompleted) == int(NumOfTransaction) or int(RecordNumber) == int(LC_Count):
						NumOfCompletedPerc += float(mktbjstat.getPercPerTransaction(NumberOfCompleted, LC_Count))
						mktbjstat.calCompletedPer('CL', NumberOfCompleted, LC_Count, NumOfCompletedPerc)
						NumberOfCompleted = 0
				
				# End Block

				ID 					= 	lc_row.ID
				PrevAss 			= 	lc_row.AssetClass
				LoanType 			=	lc_row.LoanType
				MoreThanOneYear 	=	lc_row.MoreThanOneYear
				PDID 				= 	"PD" + str(ID)
				NumDayDue 			= 	getNumDayDue(PDID)
				ASS_ID 				= 	getLoanAssetClass(NumDayDue, LoanType, MoreThanOneYear)
				ASS_PD 				= 	MKT_PAST_DUE.query.\
										filter(MKT_PAST_DUE.ID == PDID).\
										filter(MKT_PAST_DUE.TotODAmount > 0).\
										first()

				LC = MKT_LOAN_CONTRACT.query.get(ID)

				if ASS_ID:
					LC.AssetClass = ASS_ID
					db.session.add(LC)

				# print "%s is currently in class %s." %(ID, ASS_ID)
				print_msg = "%s is currently in class %s." %(ID, ASS_ID)
				mktmessage.msgOutputMsg(print_msg)

				LC_ASS = MKT_LOAN_CONTRACT.query.get(ID)

				AssClass 			= 	LC_ASS.AssetClass
				LC_Amount 			= 	LC_ASS.Amount
				LC_Category 		= 	LC_ASS.Category
				LC_PRODUCT 			= 	LC_ASS.LoanProduct
				Branch 				= 	LC_ASS.Branch
				Account 			= 	LC_ASS.Account
				CustomerID 			= 	LC_ASS.ContractCustomerID
				Currency 			= 	LC_ASS.Currency
				Suspend 			=	LC_ASS.Suspend
				Term 				= 	int(LC_ASS.Term)
				TranDate 			= 	SystemBankDate
				TotalAccrIntRec 	= 	float(LC_ASS.AccrInterest) if LC_ASS.AccrInterest else float(0)
				TotalIntIncEarned 	= 	float(LC_ASS.IntIncEarned) if LC_ASS.IntIncEarned else float(0)

				Tran = mktsetting.getAccSetting()
				
				if not Tran:
					# Call method for error message
					error_msg = "Please setting up accounting setting."
					mktmessage.msgError(EOD, error_msg)

				k1 = Tran.GL_KEY1
				k2 = Tran.GL_KEY2
				k3 = Tran.GL_KEY3
				k4 = Tran.GL_KEY4
				k5 = Tran.GL_KEY5
				k6 = Tran.GL_KEY6
				k7 = Tran.GL_KEY7
				k8 = Tran.GL_KEY8
				k9 = Tran.GL_KEY9

				k1 = mktkey.getResultKey(k1, ID, CustomerID)
				k2 = mktkey.getResultKey(k2, ID, CustomerID)
				k3 = mktkey.getResultKey(k3, ID, CustomerID)
				k4 = mktkey.getResultKey(k4, ID, CustomerID)
				k5 = mktkey.getResultKey(k5, ID, CustomerID)
				k6 = mktkey.getResultKey(k6, ID, CustomerID)
				k7 = mktkey.getResultKey(k7, ID, CustomerID)
				k8 = mktkey.getResultKey(k8, ID, CustomerID)
				k9 = mktkey.getResultKey(k9, ID, CustomerID)

				# Transfer Loan Outstading from old class to new one.
				TotPrincipalDue = 0
				if ASS_PD:
					TotPrincipalDue = float(ASS_PD.TotPrincipalDue) if ASS_PD.TotPrincipalDue else float(0)
				
				OutStandingAmount = float(TotPrincipalDue) + float(LC_Amount)

				if PrevAss != AssClass:
					
					if OutStandingAmount > 0:
						Category 	= LC_Category
						if not Category:
							# Call method for error message
							error_msg = "Loan category not found."
							mktmessage.msgError(EOD, error_msg)

						else:
							Transaction = mktsetting.getAccSetting().TerminateTran
							Transaction = Transaction.strip()
							if Transaction:

								DrCr 		= "Dr"
								Mode 		= "Direct"
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								DateTimeNow = mktdate.getDateTimeNow()

								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									OutStandingAmount, 			# Amount
									"PD",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									ID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

								DrCr 		= "Cr"
								Mode 		= "Direct"
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, PrevAss, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								DateTimeNow = mktdate.getDateTimeNow()

								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									OutStandingAmount, 		# Amount
									"PD",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									ID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

					# End Transfer Loan Outstanding from Previous Class to New Class

					ProResvCat 	= lc_row.ProResvCat
					if not ProResvCat:
						# Call method for error message
						error_msg = "Loan loss receivable category not found."
						mktmessage.msgError(EOD, error_msg)

					ProvExpCat 	= lc_row.ProvExpCat
					if not ProvExpCat:
						# Call method for error message
						error_msg = "Loan loss reversal category not found."
						mktmessage.msgError(EOD, error_msg)

					# Provisioning Transfer from old Class
					reverseProvisioning(ID, PrevAss, ProResvCat, ProvExpCat, Currency, Account, TranDate, Branch)

					# Provisioning Booking to New Class

					ProvisionAmount = 	getProvisioningAmount(AssClass, OutStandingAmount, LoanType, MoreThanOneYear, NumDayDue)
					ProvExpCat 		= 	lc_row.ProvExpCat
					if not ProvExpCat:
						# Call method for error message
						error_msg = "Loan lost reserve category not found."
						mktmessage.msgError(EOD, error_msg)

					ProResvCat 		= lc_row.ProResvCat
					if not ProResvCat:
						# Call method for error message
						error_msg = "Loan lost receivable category not found."
						mktmessage.msgError(EOD, error_msg)

					# Booking Provisioning To New Class
					provisioningBooking(ProvisionAmount, ProvExpCat, ProResvCat, Account, Currency, TranDate, ID, Branch, AssClass)
				
					# ======== Reverse and Booking AIR, Interest Income =========

					LC_Pro = MKT_LOAN_PRODUCT.query.get(LC_PRODUCT)
					
					if LC_Pro:

						
						if float(TotalIntIncEarned) > 0:
							
							Category = LC_Pro.IntIncomeCate.strip()
							if not Category:
								# Call method for error message
								error_msg = "Interest income category not found."
								mktmessage.msgError(EOD, error_msg)

							# ============== Interest Income | Reverse Old Class and Booking New Class ================
							Transaction 	= mktsetting.getAccSetting().TerminateTran
							if Transaction:
								DateTimeNow = mktdate.getDateTimeNow()
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, PrevAss, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								Mode 		= "Direct"
								DrCr 		= "Dr"
								# Debit Interest Income Category
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(TotalIntIncEarned), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									ID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

								DateTimeNow = mktdate.getDateTimeNow()
								Mode 		= "Direct"
								DrCr 		= "Cr"

								GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								# Credit Interest Income Category
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(TotalIntIncEarned), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									ID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

							else:
								# Call method for error message
								error_msg = "Interest collection transaction not found."
								mktmessage.msgError(EOD, error_msg)

						if float(TotalAccrIntRec) > 0:
							# ============== AIR | Reverse Old Class and Booking New Class ================
							Category = LC_Pro.IntReceivableCate.strip()
							if not Category:
								# Call method for error message
								error_msg = "Interest receivable category not found."
								mktmessage.msgError(EOD, error_msg)
								
							Transaction = mktsetting.getAccSetting().TerminateTran
							if Transaction:
								DateTimeNow = mktdate.getDateTimeNow()
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								Mode 		= "Direct"
								DrCr 		= "Dr"
								# Dredit AIR Category
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(TotalAccrIntRec), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									ID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

								DateTimeNow = mktdate.getDateTimeNow()
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, PrevAss, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								Mode 		= "Direct"
								DrCr 		= "Cr"
								# Credit AIR Category
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(TotalAccrIntRec), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									ID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

							else:
								# Call method for error message
								error_msg = "Class changed transaction not found."
								mktmessage.msgError(EOD, error_msg)

						IncomeRecog = getAssetClassReg(AssClass)
						Transaction = mktsetting.getAccSetting().CrAccrIntTran
						if not Transaction:
							# Call method for error message
							error_msg = "Accraed interest reversal transaction not found."
							mktmessage.msgError(EOD, error_msg)
						
						else:
							Loan = MKT_LOAN_CONTRACT.query.get(ID)
							
							if IncomeRecog.upper() == 'N':

								if Suspend.upper() == 'N':

									Category = LC_Pro.IntIncomeCate.strip()
									if not Category:
										# Call method for error message
										error_msg = "Interest income category not found."
										mktmessage.msgError(EOD, error_msg)

									DateTimeNow = mktdate.getDateTimeNow()
									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
									Mode 		= "Direct"
									DrCr 		= "Dr"
									# Debit Interest Income Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										Account,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(TotalAccrIntRec), 	# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									Category 	= mktParam.getPDParam().SuspendCrCat
									if not Category:
										# Call method for error message
										error_msg = "Suspend credit category not found."
										mktmessage.msgError(EOD, error_msg)

									DateTimeNow = mktdate.getDateTimeNow()
									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
									Mode 		= "Direct"
									DrCr 		= "Cr"
									# Debit Interest Income Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										Account,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(TotalAccrIntRec), 	# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									Loan.Suspend = 'Y'
									Loan.IntIncEarned = (float(Loan.IntIncEarned) if Loan.IntIncEarned else float(0)) - float(TotalAccrIntRec)
									db.session.add(Loan)

									print_msg = "%s Interest income has been moved to suspend account. Suspend amount %s." %(ID, mktmoney.toMoney(float(TotalAccrIntRec), mktmoney.getCurrencyObj(Currency)))
									mktmessage.msgOutputMsg(print_msg)
									# print "%s Interest income has been moved to suspend account. Suspend amount %s." %(ID, mktmoney.toMoney(float(TotalAccrIntRec), mktmoney.getCurrencyObj(Currency)))

							else:

								if Suspend.upper() == 'Y':

									Category 	= mktParam.getPDParam().SuspendCrCat
									if not Category:
										# Call method for error message
										error_msg = "Suspend credit category not found."
										mktmessage.msgError(EOD, error_msg)

									DateTimeNow = mktdate.getDateTimeNow()
									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
									Mode 		= "Direct"
									DrCr 		= "Dr"
									# Debit Interest Income Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										Account,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(TotalAccrIntRec), 	# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									Category = LC_Pro.IntIncomeCate.strip()
									if not Category:
										# Call method for error message
										error_msg = "Interest income category not found."
										mktmessage.msgError(EOD, error_msg)

									DateTimeNow = mktdate.getDateTimeNow()
									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
									Mode 		= "Direct"
									DrCr 		= "Cr"
									# Debit Interest Income Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										Account,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(TotalAccrIntRec), 	# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									Loan.Suspend = 'N'
									Loan.IntIncEarned = (float(Loan.IntIncEarned) if Loan.IntIncEarned else float(0)) + float(TotalAccrIntRec)
									db.session.add(Loan)

									# print "%s Interest income has been moved from suspend account. Amount %s." %(ID, mktmoney.toMoney(float(TotalAccrIntRec), mktmoney.getCurrencyObj(Currency)))

									print_msg = "%s Interest income has been moved from suspend account. Amount %s." %(ID, mktmoney.toMoney(float(TotalAccrIntRec), mktmoney.getCurrencyObj(Currency)))
									mktmessage.msgOutputMsg(print_msg)
					else:
						# Call method for error message
						error_msg = "Account product not found for loan-" + str(ID) +"."
						mktmessage.msgError(EOD, error_msg)
								
						# ======== End Reverse Interest, Penalty, Charge in PD =========
					
					# Commit one transaction
					db.session.commit()

				else:

					Provision = MKT_PROVISION.query.\
								filter(MKT_PROVISION.LOANID == ID).\
								filter(MKT_PROVISION.ASSETID == PrevAss).\
								first()

					if Provision:
						ProvisionOldBookedAmount 	= 	float(Provision.Amount) if Provision.Amount else float(0)
						ProvisionAmount 			= 	getProvisioningAmount(AssClass, OutStandingAmount, LoanType, MoreThanOneYear, NumDayDue)
						ProvisionAmountStr 			= 	mktmoney.toMoney(ProvisionAmount, mktmoney.getCurrencyObj(Currency))
						ProvisionOldBookedAmountStr = 	mktmoney.toMoney(ProvisionOldBookedAmount, mktmoney.getCurrencyObj(Currency))
						
						if ProvisionAmountStr != ProvisionOldBookedAmountStr:

							ProResvCat = lc_row.ProResvCat
							if not ProResvCat:
								# Call method for error message
								error_msg = "Loan loss receivable category not found."
								mktmessage.msgError(EOD, error_msg)

							ProvExpCat 	= lc_row.ProvExpCat
							if not ProvExpCat:
								# Call method for error message
								error_msg = "Loan loss reversal category not found."
								mktmessage.msgError(EOD, error_msg)

							# Provisioning Transfer from old Class
							reverseProvisioning(ID, PrevAss, ProResvCat, ProvExpCat, Currency, Account, TranDate, Branch)

							# Provisioning Booking to New Class
							ProvExpCat 		= 	lc_row.ProvExpCat
							if not ProvExpCat:
								# Call method for error message
								error_msg = "Loan lost reserve category not found."
								mktmessage.msgError(EOD, error_msg)

							ProResvCat 		= 	lc_row.ProResvCat
							if not ProResvCat:
								# Call method for error message
								error_msg = "Loan lost receivable category not found."
								mktmessage.msgError(EOD, error_msg)

							# Booking Provisioning To New Class
							provisioningBooking(ProvisionAmount, ProvExpCat, ProResvCat, Account, Currency, TranDate, ID, Branch, AssClass)
						
							# Commit one transaction
							db.session.commit()

		else:
			# print "No loan contract for class update."
			print_msg = "No loan contract for class update."
			mktmessage.msgOutputMsg(print_msg)

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def calculateTotalPD(ID):
	try:
		
		TotalPD = MKT_PAST_DUE.query.get(ID)

		if TotalPD:
			# for row in TotalPD:

			Total_Amount 	= float(0)
			Total_Principal = float(0)
			Total_Interest 	= float(0)
			Total_Penalty 	= float(0)
			Total_Charge 	= float(0)

			TotalPD_DATE = MKT_PD_DATE.query.\
						   order_by(asc(MKT_PD_DATE.Authorizeon)).\
						   filter(MKT_PD_DATE.ID == ID).\
						   all()

			if TotalPD_DATE:
				for PD in TotalPD_DATE:

					Total_Amount 	+= float(PD.OutAmount)
					Total_Principal += float(PD.OutPriAmount)
					Total_Interest 	+= float(PD.OutIntAmount)
					Total_Penalty 	+= float(PD.OutPenAmount)
					Total_Charge 	+= float(PD.OutChgAmount)

			PD_TOTAL = MKT_PAST_DUE.query.get(ID)
			PD_TOTAL.TotODAmount 		= 	Total_Amount
			PD_TOTAL.TotPrincipalDue 	= 	Total_Principal
			PD_TOTAL.TotInterestDue 	= 	Total_Interest
			PD_TOTAL.TotPenaltyDue 		= 	Total_Penalty
			PD_TOTAL.TotChargeDue 		= 	Total_Charge

			db.session.add(PD_TOTAL)

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setPDCollection(PastDueID="", EOD=1, Stat=""):
	try:

		currentDate 	= 	mktdate.getDateISO()
		SystemBankDate 	= 	mktdate.getBankDate()
		PDO_PARAM  		= 	mktParam.getPDParam().PDO
		NAB_PARAM  		= 	mktParam.getPDParam().NAB
		PenaltyOption 	=	mktParam.getPDParam().PenOption
		# get MKT_PAST_DUE records
		LC = ""
		PD = MKT_PAST_DUE.query.\
			 order_by(asc(MKT_PAST_DUE.ID)).\
			 filter(MKT_PAST_DUE.TotODAmount > 0)

		if PastDueID:
			PD = PD.\
				 filter(MKT_PAST_DUE.ID == PastDueID)

		PD_Count = PD.count()
		if Stat == "1":
			# Count record before run eod.
			return PD_Count

		PD = PD.all()

		# Check if no record set progress bar done.
		mktbjstat.calCompletedPer('PD', 0, 1, 0)

		if PD:

			NumberOfCompleted 	= 	0
			RecordNumber 		=	0
			NumOfCompletedPerc 	=	0
			NumOfTransaction 	=	mktbjstat.setNumOfTransaction(PD_Count)
			Tran 				= 	mktsetting.getAccSetting()
			getPDParam 			=	mktParam.getPDParam()
			
			for row in PD:

				# Block update BjStat
				RecordNumber 		+= int(1)
				NumberOfCompleted 	+= int(1)

				if int(NumberOfCompleted) == int(NumOfTransaction) or int(RecordNumber) == int(PD_Count):
					NumOfCompletedPerc += float(mktbjstat.getPercPerTransaction(NumberOfCompleted, PD_Count))
					mktbjstat.calCompletedPer('PD', NumberOfCompleted, PD_Count, NumOfCompletedPerc)
					NumberOfCompleted = 0

				# End Block

				PDID 		= 	row.ID
				Principal 	= 	0
				Interest 	= 	0
				Penalty 	= 	0
				Charge 		= 	0
				TranDate 	=	SystemBankDate
				AccBal 		= 	0

				LC_ID = row.ID[2:]
				# get single Loan Contract record
				LC = MKT_LOAN_CONTRACT.query.get(LC_ID)
				Account = ""
				if LC:
					ID 					= 	PDID
					AssClass 			= 	LC.AssetClass
					LC_PRODUCT 			= 	LC.LoanProduct
					Branch 				= 	LC.Branch
					Account 			= 	LC.Account
					CustomerID 			= 	LC.ContractCustomerID
					LC_Amount 			=	LC.Amount
					MoreThanOneYear 	=	LC.MoreThanOneYear
					LoanType 			=	LC.LoanType
					LoanID 				=	LC.ID
					Suspend 			=	LC.Suspend

					if not Tran:
						# Call method for error message
						error_msg = "Please setting up accounting setting."
						mktmessage.msgError(EOD, error_msg)

					k1 = Tran.GL_KEY1
					k2 = Tran.GL_KEY2
					k3 = Tran.GL_KEY3
					k4 = Tran.GL_KEY4
					k5 = Tran.GL_KEY5
					k6 = Tran.GL_KEY6
					k7 = Tran.GL_KEY7
					k8 = Tran.GL_KEY8
					k9 = Tran.GL_KEY9

					k1 = mktkey.getResultKey(k1, LC_ID, CustomerID)
					k2 = mktkey.getResultKey(k2, LC_ID, CustomerID)
					k3 = mktkey.getResultKey(k3, LC_ID, CustomerID)
					k4 = mktkey.getResultKey(k4, LC_ID, CustomerID)
					k5 = mktkey.getResultKey(k5, LC_ID, CustomerID)
					k6 = mktkey.getResultKey(k6, LC_ID, CustomerID)
					k7 = mktkey.getResultKey(k7, LC_ID, CustomerID)
					k8 = mktkey.getResultKey(k8, LC_ID, CustomerID)
					k9 = mktkey.getResultKey(k9, LC_ID, CustomerID)

					# get Account information
					AccountObj = MKT_ACCOUNT.query.get(Account)
					if AccountObj:
						AccBal 		= float(AccountObj.AvailableBal) if AccountObj.AvailableBal else float(0)
						Currency 	= AccountObj.Currency
						# Filter MKT_PD_DATE Object Record
						PD_DATE = 	MKT_PD_DATE.query.\
									order_by(asc(MKT_PD_DATE.DueDate)).\
						  			filter(MKT_PD_DATE.ID == PDID)

						if not PastDueID:
							PD_DATE = PD_DATE.\
									  filter(MKT_PD_DATE.NextRunDate == str(SystemBankDate))

						PD_DATE = PD_DATE.all()

						if PD_DATE:
							Param = getPDParam.RepOrder
							Param = Param.split()

							NumOfPD = 1
							for PD_ROW in PD_DATE:

								NewLoanObj = MKT_LOAN_CONTRACT.query.get(LC_ID)
								if NewLoanObj:
									AssClass 			= 	NewLoanObj.AssetClass
									Suspend 			=	NewLoanObj.Suspend

								NextRunDate = mktdate.getBankDateObj().NextSystemDate
								NextRunDate = NextRunDate.strip()
								
								if not NextRunDate:
									# Call method for error message
									error_msg = "The record PD date %s is failed to update, Please setting bank date." %PD_ID
									mktmessage.msgError(EOD, error_msg)

								if not PastDueID:
									PD_ROW.NextRunDate = NextRunDate
									db.session.add(PD_ROW)

								PD_ID 			= 	PD_ROW.PDID
								DueDate 		= 	PD_ROW.DueDate
								NumDayDue 		= 	PD_ROW.NumDayDue
								ODStatus 		= 	PD_ROW.ODStatus
								TotODAmount 	= 	PD_ROW.TotODAmount
								OutAmount 		= 	PD_ROW.OutAmount
								PrincipalDue 	= 	PD_ROW.PrincipalDue
								OutPriAmount 	= 	PD_ROW.OutPriAmount
								InterestDue 	= 	PD_ROW.InterestDue
								OutIntAmount 	= 	PD_ROW.OutIntAmount
								PenaltyDue 		= 	PD_ROW.PenaltyDue
								OutPenAmount 	= 	PD_ROW.OutPenAmount
								ChargeDue 		= 	PD_ROW.ChargeDue
								OutChgAmount 	= 	PD_ROW.OutChgAmount
								
								# Define Principal, Interest, Penalty, Charge from MKT_PD_DATE
								Principal 		= 	float(OutPriAmount) if OutPriAmount else float(0) 	# Define Principal
								Interest 		= 	float(OutIntAmount) if OutIntAmount else float(0)		# Define Interest
								Penalty 		= 	float(OutPenAmount) if OutPenAmount else float(0)		# Define Penalty
								Charge 			= 	float(OutChgAmount) if OutChgAmount else float(0)		# Define Charge
								PD_Total_Amount = 	float(PD_ROW.OutAmount) if PD_ROW.OutAmount else float(0)

								# Variable for get left over amount after settlement
								PD_Penalty 		= Penalty
								PD_Charge 		= Charge
								PD_Interest 	= Interest
								PD_Principal 	= Principal

								# Variable for get settlement amount
								PenaltyDue_UPDATE 	= 0
								ChargeDue_UPDATE 	= 0
								InterestDue_UPDATE 	= 0
								PrincipalDue_UPDATE = 0

								for p in Param:

									if p == 'PE':

										if PenaltyOption and PenaltyOption.upper() == 'A':

											if float(Penalty) > 0:

												if float(AccBal) >= float(Penalty):
													Amount = float(Penalty)
												else:
													Amount = float(AccBal)
													PD_Penalty = float(Penalty) - float(Amount)

												if int(NumDayDue) >= int(PDO_PARAM):

													AccBal = float(AccBal) - float(Amount)
													PenaltyDue_UPDATE = Amount
													if Amount > 0:
														# Define parameter for Debit Customer Account
														DrCr  		= 'Dr'
														Category 	= AccountObj.AccCategory
														if not Category:
															# Call method for error message
															error_msg = "Account category not found."
															mktmessage.msgError(EOD, error_msg)

														Transaction = Tran.PenaltyTran
														GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
														DateTimeNow = mktdate.getDateTimeNow()
														Mode 		= ""

														if Transaction:
															# Debit Customer Account
															mktaccounting.postAccounting(
																"AUTH", 				# Status
																"0", 					# Curr
																"system",				# Inputter
																DateTimeNow, 			# Createdon
																"system",				# Authorizer
																DateTimeNow,			# Authorizeon
																"", 					# AEID
																Account,				# Account
																Category,				# Category
																Currency,				# Currency
																DrCr,					# DrCr
																Decimal(Amount), 		# Amount
																"LC",					# Module
																Transaction, 			# Transaction
																TranDate, 				# TransactionDate
																ID, 					# Reference
																"", 					# Note
																"", 					# JNID
																Branch,					# Branch
																GL_KEYS,				# GL_KEYS
																Mode 					# Mode check to insert Journal for category
															)

															# Define new parameter for Credit Penalty Category
															DateTimeNow = mktdate.getDateTimeNow()
															DrCr 		= "Cr"
															Mode 		= "Direct"
															Category 	= getPDParam.ODPenaltyCat
															if not Category:
																# Call method for error message
																error_msg = "Penalty income category not found."
																mktmessage.msgError(EOD, error_msg)

															GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
															# Credit Penalty Category
															mktaccounting.postAccounting(
																"AUTH", 				# Status
																"0", 					# Curr
																"system",				# Inputter
																DateTimeNow, 			# Createdon
																"system",				# Authorizer
																DateTimeNow,			# Authorizeon
																"", 					# AEID
																Account,				# Account
																Category,				# Category
																Currency,				# Currency
																DrCr,					# DrCr
																Decimal(Amount), 		# Amount
																"LC",					# Module
																Transaction, 			# Transaction
																TranDate, 				# TransactionDate
																ID, 					# Reference
																"", 					# Note
																"", 					# JNID
																Branch,					# Branch
																GL_KEYS,				# GL_KEYS
																Mode 					# Mode check to insert Journal for category
															)

														else:
															# Call method for error message
															error_msg = "Penalty collection transaction not found."
															mktmessage.msgError(EOD, error_msg)

									elif p == 'CH':

										if float(Charge) != 0:

											if AccBal >= Charge:
												Amount = float(Charge)
											else:
												Amount 		= float(AccBal)
												PD_Charge 	= float(Charge) - float(Amount)

											ChargeDue_UPDATE 	= Amount
											AccBal 				= float(AccBal) - float(Amount)

											if Amount > 0:
												
												DrCr  		= 'Dr'
												Transaction = Tran.ChargeTran
												Category 	= AccountObj.AccCategory
												if not Category:
													# Call method for error message
													error_msg = "Account category not found."
													mktmessage.msgError(EOD, error_msg)

												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
												DateTimeNow = mktdate.getDateTimeNow()
												Mode 		= ""

												if Transaction:
													# Debit Customer Account
													mktaccounting.postAccounting(
														"AUTH", 				# Status
														"0", 					# Curr
														"system",				# Inputter
														DateTimeNow, 			# Createdon
														"system",				# Authorizer
														DateTimeNow,			# Authorizeon
														"", 					# AEID
														Account,				# Account
														Category,				# Category
														Currency,				# Currency
														DrCr,					# DrCr
														Decimal(Amount), 		# Amount
														"LC",					# Module
														Transaction, 			# Transaction
														TranDate, 				# TransactionDate
														ID, 					# Reference
														"", 					# Note
														"", 					# JNID
														Branch,					# Branch
														GL_KEYS,				# GL_KEYS
														Mode 					# Mode check to insert Journal for category
													)

													DateTimeNow = mktdate.getDateTimeNow()
													Category 	= getPDParam.ChargeIncCat
													if not Category:
														# Call method for error message
														error_msg = "Charge category not found."
														mktmessage.msgError(EOD, error_msg)

													GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
													DrCr 	= "Cr"
													Mode 	= "Direct"

													# Credit Charge Category
													mktaccounting.postAccounting(
														"AUTH", 				# Status
														"0", 					# Curr
														"system",				# Inputter
														DateTimeNow, 			# Createdon
														"system",				# Authorizer
														DateTimeNow,			# Authorizeon
														"", 					# AEID
														Account,				# Account
														Category,				# Category
														Currency,				# Currency
														DrCr,					# DrCr
														Decimal(Amount), 		# Amount
														"LC",					# Module
														Transaction, 			# Transaction
														TranDate, 				# TransactionDate
														ID, 					# Reference
														"", 					# Note
														"", 					# JNID
														Branch,					# Branch
														GL_KEYS,				# GL_KEYS
														Mode 					# Mode check to insert Journal for category
													)

												else:
													# Call method for error message
													error_msg = "Charge collection transaction not found."
													mktmessage.msgError(EOD, error_msg)

									elif p == 'IN':
										if float(Interest) > 0:
											# print "Interest = " + str(Interest)
											if AccBal >= Interest:
												Amount = float(Interest)
											else:
												Amount 		= float(AccBal)
												PD_Interest = float(Interest) - float(Amount)

											InterestDue_UPDATE = Amount

											AccBal = float(AccBal) - float(Amount)

											if Amount > 0:
												DrCr  		= 'Dr'
												Category 	= AccountObj.AccCategory
												if not Category:
													# Call method for error message
													error_msg = "Account category not found."
													mktmessage.msgError(EOD, error_msg)

												Transaction = Tran.InterestTran
												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
												DateTimeNow = mktdate.getDateTimeNow()
												Mode 		= ""

												LC_Pro = MKT_LOAN_PRODUCT.query.get(LC_PRODUCT)
												if LC_Pro:

													if Transaction:
														# Debit Customer Account
														mktaccounting.postAccounting(
															"AUTH", 				# Status
															"0", 					# Curr
															"system",				# Inputter
															DateTimeNow, 			# Createdon
															"system",				# Authorizer
															DateTimeNow,			# Authorizeon
															"", 					# AEID
															Account,				# Account
															Category,				# Category
															Currency,				# Currency
															DrCr,					# DrCr
															Decimal(Amount), 		# Amount
															"LC",					# Module
															Transaction, 			# Transaction
															TranDate, 				# TransactionDate
															ID, 					# Reference
															"", 					# Note
															"", 					# JNID
															Branch,					# Branch
															GL_KEYS,				# GL_KEYS
															Mode 					# Mode check to insert Journal for category
														)

														DrCr  		= 'Cr'
														Category 	= LC_Pro.IntReceivableCate
														if not Category:
															# Call method for error message
															error_msg = "Interest income category not found."
															mktmessage.msgError(EOD, error_msg)

														Transaction = Tran.InterestTran
														GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "PD", k1, k2, k3, k4, k5, k6, k7, k8, k9)
														DateTimeNow = mktdate.getDateTimeNow()
														Mode 		= "Direct"

														# Credit Interest Income Category
														mktaccounting.postAccounting(
															"AUTH", 				# Status
															"0", 					# Curr
															"system",				# Inputter
															DateTimeNow, 			# Createdon
															"system",				# Authorizer
															DateTimeNow,			# Authorizeon
															"", 					# AEID
															Account,				# Account
															Category,				# Category
															Currency,				# Currency
															DrCr,					# DrCr
															Decimal(Amount), 		# Amount
															"LC",					# Module
															Transaction, 			# Transaction
															TranDate, 				# TransactionDate
															ID, 					# Reference
															"", 					# Note
															"", 					# JNID
															Branch,					# Branch
															GL_KEYS,				# GL_KEYS
															Mode 					# Mode check to insert Journal for category
														)

														LC.AccrInterest = (float(LC.AccrInterest) if LC.AccrInterest else float(0)) - float(Amount)
														db.session.add(LC)

													else:
														# Call method for error message
														error_msg = "Interest collection transaction not found."
														mktmessage.msgError(EOD, error_msg)

													# Check if Loan is Suspend
													if Suspend.upper() == 'Y':
														Transaction = Tran.CrAccrIntTran
														if not Transaction:
															# Call method for error message
															error_msg = "Accraed interest reversal transaction not found."
															mktmessage.msgError(EOD, error_msg)

														else:

															Category 	= getPDParam.SuspendCrCat
															if not Category:
																# Call method for error message
																error_msg = "Suspend credit category not found."
																mktmessage.msgError(EOD, error_msg)

															DateTimeNow = mktdate.getDateTimeNow()
															GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
															Mode 		= "Direct"
															DrCr 		= "Dr"
															# Debit Interest Income Category
															mktaccounting.postAccounting(
																"AUTH", 				# Status
																"0", 					# Curr
																"system",				# Inputter
																DateTimeNow, 			# Createdon
																"system",				# Authorizer
																DateTimeNow,			# Authorizeon
																"", 					# AEID
																Account,				# Account
																Category,				# Category
																Currency,				# Currency
																DrCr,					# DrCr
																Decimal(Amount), 		# Amount
																"PD",					# Module
																Transaction, 			# Transaction
																TranDate, 				# TransactionDate
																ID, 					# Reference
																"", 					# Note
																"", 					# JNID
																Branch,					# Branch
																GL_KEYS,				# GL_KEYS
																Mode 					# Mode check to insert Journal for category
															)

															Category = LC_Pro.IntIncomeCate.strip()
															if not Category:
																# Call method for error message
																error_msg = "Interest income category not found."
																mktmessage.msgError(EOD, error_msg)

															DateTimeNow = mktdate.getDateTimeNow()
															GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
															Mode 		= "Direct"
															DrCr 		= "Cr"
															# Debit Interest Income Category
															mktaccounting.postAccounting(
																"AUTH", 				# Status
																"0", 					# Curr
																"system",				# Inputter
																DateTimeNow, 			# Createdon
																"system",				# Authorizer
																DateTimeNow,			# Authorizeon
																"", 					# AEID
																Account,				# Account
																Category,				# Category
																Currency,				# Currency
																DrCr,					# DrCr
																Decimal(Amount), 		# Amount
																"PD",					# Module
																Transaction, 			# Transaction
																TranDate, 				# TransactionDate
																ID, 					# Reference
																"", 					# Note
																"", 					# JNID
																Branch,					# Branch
																GL_KEYS,				# GL_KEYS
																Mode 					# Mode check to insert Journal for category
															)

															LC.IntIncEarned 	=	(float(LC.IntIncEarned) if LC.IntIncEarned else float(0)) + float(Amount)
															db.session.add(LC)

												else:
													# Call method for error message
													error_msg = "Account product not found for contract#" + str(ID) + "."
													mktmessage.msgError(EOD, error_msg)

									else:
										if float(Principal) > 0:
											# print "Principal = " + str(Principal)
											if AccBal >= Principal:
												Amount = float(Principal)
											else:
												Amount 			= float(AccBal)
												PD_Principal 	= float(Principal) - float(Amount)

											PrincipalDue_UPDATE = Amount
											AccBal 				= float(AccBal) - float(Amount)

											if Amount > 0:
												DrCr  		= 'Dr'
												Category 	= AccountObj.AccCategory
												if not Category:
													# Call method for error message
													error_msg = "Account category not found."
													mktmessage.msgError(EOD, error_msg)

												Transaction = Tran.PrincipalTran
												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
												DateTimeNow = mktdate.getDateTimeNow()
												Mode 		= ""

												LC_Pro = MKT_LOAN_PRODUCT.query.get(LC_PRODUCT)
												if LC_Pro:

													if Transaction:
														# Debit Customer Account
														mktaccounting.postAccounting(
															"AUTH", 				# Status
															"0", 					# Curr
															"system",				# Inputter
															DateTimeNow, 			# Createdon
															"system",				# Authorizer
															DateTimeNow,			# Authorizeon
															"", 					# AEID
															Account,				# Account
															Category,				# Category
															Currency,				# Currency
															DrCr,					# DrCr
															Decimal(Amount), 		# Amount
															"LC",					# Module
															Transaction, 			# Transaction
															TranDate, 				# TransactionDate
															ID, 					# Reference
															"", 					# Note
															"", 					# JNID
															Branch,					# Branch
															GL_KEYS,				# GL_KEYS
															Mode 					# Mode check to insert Journal for category
														)

														DrCr  		= 'Cr'
														Category 	= LC.Category
														if not Category:
															# Call method for error message
															error_msg = "Loan category not found."
															mktmessage.msgError(EOD, error_msg)

														Transaction = Tran.PrincipalTran
														GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "PD", k1, k2, k3, k4, k5, k6, k7, k8, k9)
														DateTimeNow = mktdate.getDateTimeNow()
														Mode 		= "Direct"

														# Credit LC Category
														mktaccounting.postAccounting(
															"AUTH", 				# Status
															"0", 					# Curr
															"system",				# Inputter
															DateTimeNow, 			# Createdon
															"system",				# Authorizer
															DateTimeNow,			# Authorizeon
															"", 					# AEID
															Account,				# Account
															Category,				# Category
															Currency,				# Currency
															DrCr,					# DrCr
															Decimal(Amount), 		# Amount
															"LC",					# Module
															Transaction, 			# Transaction
															TranDate, 				# TransactionDate
															ID, 					# Reference
															"", 					# Note
															"", 					# JNID
															Branch,					# Branch
															GL_KEYS,				# GL_KEYS
															Mode 					# Mode check to insert Journal for category
														)

													else:
														# Call method for error message
														error_msg = "Principal collection transaction not found."
														mktmessage.msgError(EOD, error_msg)

												else:
													# Call method for error message
													error_msg = "Account product not found for contract#" + str(ID) + "."
													mktmessage.msgError(EOD, error_msg)

								# Here is todo update PD
								Total = PD_Penalty + PD_Charge + PD_Interest + PD_Principal
								if float(Total) == float(0):
									# Transfer MKT_PD_DATE record to MKT_PD_DATE_HIST
									transferPD(ID, PD_ID, DueDate, NumDayDue, ODStatus, TotODAmount,
											   OutAmount, PrincipalDue, OutPriAmount, InterestDue, OutIntAmount,
											   PenaltyDue, OutPenAmount, ChargeDue, OutChgAmount
											  )

								# Update MKT_PD_DATE and MKT_PAST_DUE
								updatePD(ID, PD_ID, PenaltyDue_UPDATE, ChargeDue_UPDATE, InterestDue_UPDATE, PrincipalDue_UPDATE)

								# Update PD Status
								checkPDStatus(ID, PD_ID)

								TotalPaid = float(PenaltyDue_UPDATE) + float(ChargeDue_UPDATE) + float(InterestDue_UPDATE) + float(PrincipalDue_UPDATE)
								if float(TotalPaid) > float(0):
									Rep_Schedule = MKT_REP_SCHEDULE.query.\
												   filter(MKT_REP_SCHEDULE.LoanID == str(ID[2:])).\
												   filter(MKT_REP_SCHEDULE.CollectionDate == str(DueDate)).\
												   first()

									if Rep_Schedule:
										if float(PD_Total_Amount) == float(TotalPaid):
											Rep_Schedule.RepStatus 	= 3
										else:
											Rep_Schedule.RepStatus 	= 2

										Rep_Schedule.PartPaidAmt 	= float(Rep_Schedule.PartPaidAmt) + float(TotalPaid)
										db.session.add(Rep_Schedule)

								# Calculate all MKT_PD_DATE depend on MKT_PAST_DUE
								calculateTotalPD(ID)

								if float(LC_Amount) == 0:
									Provision = MKT_PROVISION.query.\
												filter(MKT_PROVISION.LOANID == LoanID).\
												filter(MKT_PROVISION.ASSETID == AssClass).\
												first()

									if Provision:
										PastDue 						= 	MKT_PAST_DUE.query.get(ID)
										if PastDue:
											ProvisionOldBookedAmount 	= 	float(Provision.Amount) if Provision.Amount else float(0)
											PrincipalDueAmount 			= 	float(PastDue.TotPrincipalDue) if PastDue.TotPrincipalDue else float(0)
											ProvisionAmount 			= 	getProvisioningAmount(AssClass, PrincipalDueAmount, LoanType, MoreThanOneYear, NumDayDue)
											ProvisionAmountStr 			= 	mktmoney.toMoney(ProvisionAmount, mktmoney.getCurrencyObj(Currency))
											ProvisionOldBookedAmountStr = 	mktmoney.toMoney(ProvisionOldBookedAmount, mktmoney.getCurrencyObj(Currency))

											if ProvisionAmountStr != ProvisionOldBookedAmountStr:
												AssetClass = MKT_ASSET_CLASS.query.get(AssClass)
												ProResvCat = AssetClass.ProResvCat
												if not ProResvCat:
													# Call method for error message
													error_msg = "Loan loss receivable category not found."
													mktmessage.msgError(EOD, error_msg)

												ProvExpCat 	= AssetClass.ProvExpCat
												if not ProvExpCat:
													# Call method for error message
													error_msg = "Loan loss reversal category not found."
													mktmessage.msgError(EOD, error_msg)

												# Provisioning Transfer from old Class
												reverseProvisioning(LoanID, AssClass, ProResvCat, ProvExpCat, Currency, Account, TranDate, Branch)

												# Provisioning Booking to New Class
												ProvExpCat 		= 	AssetClass.ProvExpCat
												if not ProvExpCat:
													# Call method for error message
													error_msg = "Loan lost reserve category not found."
													mktmessage.msgError(EOD, error_msg)

												ProResvCat 		= 	AssetClass.ProResvCat
												if not ProResvCat:
													# Call method for error message
													error_msg = "Loan lost receivable category not found."
													mktmessage.msgError(EOD, error_msg)

												# Booking Provisioning To New Class
												provisioningBooking(ProvisionAmount, ProvExpCat, ProResvCat, Account, Currency, TranDate, LoanID, Branch, AssClass)
										
								# Update NumDayDue in MKT_PD_DATE
								setNumDayDue(ID)
								# Update Last Outstanding Amount
								mktloan.setUpdateOutstandingAmount(LC_ID)
								# Update Class
								loanClassification(LC_ID, 1, "", "", "No")
								# Update Penalty
								if 'PE' in Param:
									setPenalty(ID, PD_ID)
								# commit() one dependency record
								db.session.commit()
								# print "Past due collected successfully."
								TotalPDAmount = float(Principal) + float(Interest) + float(Charge) + float(Penalty)
								if float(TotalPDAmount) == float(TotalPaid):
									# print "%s #%s Full amount was collected: %s." %(ID, str(NumOfPD), str(TotalPaid))
									print_msg = "%s #%s Full amount was collected: %s." %(ID, str(NumOfPD), str(TotalPaid))
									mktmessage.msgOutputMsg(print_msg)
								elif float(TotalPaid) < float(TotalPDAmount) and float(TotalPaid) > 0:
									# print "%s #%s Partial amount was collected: %s." %(ID, str(NumOfPD), str(TotalPaid))
									print_msg = "%s #%s Partial amount was collected: %s." %(ID, str(NumOfPD), str(TotalPaid))
									mktmessage.msgOutputMsg(print_msg)
								else:
									# print "%s #%s No amount was collected." %(ID, str(NumOfPD))
									print_msg = "%s #%s No amount was collected." %(ID, str(NumOfPD))
									mktmessage.msgOutputMsg(print_msg)

								NumOfPD += 1

						# Clear PD_DATE query object
						del PD_DATE

					else:
						# Call method for error message
						error_msg = "Account not found."
						mktmessage.msgError(EOD, error_msg)

				else:
					# Call method for error message
					error_msg = "Loan contract not found."
					mktmessage.msgError(EOD, error_msg)

		else:
			# print "No PD record."
			print_msg = "No PD record."
			mktmessage.msgOutputMsg(print_msg)

		# Clear PD; LC query object
		del PD
		del LC

		if EOD == 1:
			return ""
		else:
			return True

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise