from app.mktcore.wtfimports 	import *
from app.mktcore.imports 		import *
from flask 						import flash


from sqlalchemy 				import create_engine,inspect

def getTellerParam():
	
	engine 		= create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
	inspector 	= inspect(engine)
	ListTable 	= []
	
	for table_name in inspector.get_table_names():
		ListTable.append(str(table_name))
	
	result=''

	if 'MKT_TELLER_PARAM' in ListTable:

		TellerParam = MKT_TELLER_PARAM.query.get('SYSTEM')

		if TellerParam:

			result = TellerParam

	return result

def getPDParam():
	try:

		from app.PDParameter.models import MKT_PD_PARAM
		PD = MKT_PD_PARAM.query.get("SYSTEM")

		if not PD:
			Insert = MKT_PD_PARAM(
						Status 			=	'AUTH',
						Curr 			=	'0',
						Inputter		=	'System',
						Createdon 		=	'2015-04-03',
						Authorizer		=	'System',
						Authorizeon		=	'2015-04-03',
						ID 				= 	'SYSTEM',
						PRE 			=	'',
						GRA 			=	'',
						PDO 			=	'',
						NAB 			=	'',
						WOF 			=	'',
						FWOF 			=	'',
						RepOrder 		=	'',
						FWOFRepOrder 	=	'',
						PenType 		=	'',
						PenaltyRate 	=	'',
						PenCalBase 		=	'',
						SuspPLNAB 		=	'',
						RevPLNAB 		=	'',
						RepMethod 		=	'',
						ODPenaltyCat 	=	'',
						PenaltyRecCat 	=	'',
						RecoveryCat 	=	'',
						WOFClass 		=	'',
						WOFDay 			=	''
					 )
			
			db.session.add(Insert)
			db.session.commit()

			PD = MKT_PD_PARAM.query.get("SYSTEM")

		return PD

	except:
		raise