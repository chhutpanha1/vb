'''
Created Date: 07 May 2015
Author: Sovankiry Rim

Modified by: Sovankiry
Modified Date: 15 July 2015
All Right Reserved Morakot Technology
Description : mktteller refer to function for Teller Function
Notes : 
'''
from flask 						import flash,session,jsonify,Markup
from app.mktcore.wtfimports 	import *
from .. 						import app, db
from decimal 					import *
from sqlalchemy 				import *


import mktaudit 				as mktaudit
import mktaccount 				as mktaccount
import mktaccounting 			as mktaccounting
import app.tools.mktparam 		as mktparam
import app.tools.mktdb			as mktdb
import app.tools.mktreport 		as mktreport
import app.tools.user 			as mktuser

#------------------------ Teller Income / Expense ---------------------------

def isSuspendConfig():
	CurrentBranch 	= mktuser.getCurrentBranch()
	TellerParam 	= mktparam.getTellerParam()
	if TellerParam:
		SuspendAccPro = TellerParam.SuspendAccPro
		if not SuspendAccPro:
			return False,"Please configuration Teller Parameter first."
		
		Acc 	= 	MKT_ACCOUNT.query.filter_by(
											AccProduct 	= 	TellerParam.SuspendAccPro,
											Branch 		= 	CurrentBranch ).\
											first()
		if not Acc:

			return False,"Please create Suspend account first."

		if not TellerParam.IncomeTran:
			return False,"Teller Income transcations not found."

		if not TellerParam.ExpenseTran:
			return False,"Teller Expense transcations not found."

		#Successfull for validate
		return True, TellerParam

	else:
		return False,"Please configuration Teller Parameter first."

def getIncomeExpenseHotField(Type=""):

	DicType = {'I':'Income','E':'Expense'}
	DrAccount	= ""
	DrCategory 	= ""
	Currency 	= ""
	CrAccount 	= ""
	CrCategory 	= ""
	TranCode 	= ""
	Message  	= ""
	TellerParam = mktparam.getTellerParam()
	if TellerParam:
		if Type in DicType:
			if Type == "I":
				print "here in get IncomeTran %s"%TellerParam.IncomeTran
				TranCode 	= TellerParam.IncomeTran
			else:
				print "here in get ExpenseTran"
				TranCode 	= TellerParam.ExpenseTran

			if not TranCode:
				Message = "Teller %s transaction not found."% DicType[Type]

			#Get Suspend Account
			CurrentBranch 	= mktuser.getCurrentBranch()	
			Currency 		= request.args.get('Currency') if 'Currency' in request.args else ''
			SuspendAccObj 	= mktaccount.getSuspendAccount(Currency,CurrentBranch)
			if SuspendAccObj:

				if DicType[Type] == "Income":
					CrAccount 	= SuspendAccObj.ID
					CrCategory 	= SuspendAccObj.AccCategory
					
				elif DicType[Type] == "Expense":
					DrAccount 	= SuspendAccObj.ID
					DrCategory 	= SuspendAccObj.AccCategory
			else:
				Message+="Suspend Account not found."
			
			#Get Teller Account
			TellerAcc = getTillAccountObj("",Currency)
			if TellerAcc:

				if DicType[Type] == "Income":

					DrAccount 	= TellerAcc.ID
					DrCategory 	= TellerAcc.AccCategory

				elif DicType[Type] == "Expense":

					CrAccount 	= TellerAcc.ID
					CrCategory 	= TellerAcc.AccCategory
			else:
				Message+="Till Account not found."
			
		else:
			Message="Parameter not found in Income /Expense."
	else:
		Message="Please set Teller Parameter first."

	return jsonify(TranCode=TranCode,DrAccount=DrAccount,DrCategory=DrCategory,CrAccount=CrAccount,CrCategory=CrCategory,Currency=Currency)



def getTellerIncomeExpense():
	ExpenseTran 		= ""
	IncomeTran 			= ""
	TellerParam 		= mktparam.getTellerParam()
	ListTellerID 		= []
	ListIncomeExpense 	= []
	CurrentBranch		= mktuser.getCurrentBranch()

	if TellerParam:
		ExpenseTran = TellerParam.ExpenseTran
		IncomeTran 	= TellerParam.IncomeTran
	IncomeExpenseObj=MKT_INCOME_EXPENSE_BOOKING.query
	QueryTellerID = db.session.query(MKT_TELLER.ID).\
						filter(~MKT_TELLER.ID.in_(db.session.query(MKT_INCOME_EXPENSE_BOOKING.ID).all())).\
						filter(or_(MKT_TELLER.ID.like('%'+'IP'+'%'),MKT_TELLER.ID.like('%'+'EP'+'%'))).\
						filter(MKT_TELLER.Branch == CurrentBranch)

	for row in QueryTellerID:
		ID = str(row.ID)
		CheckAccounting = isIncomeExpenseBooking(IncomeExpenseObj,ID)
		CheckReverse 	= isRecordReverse(ID)
		if not CheckAccounting and not CheckReverse:
			ListTellerID.append(ID)

	QueryObj = MKT_TELLER.query.filter(MKT_TELLER.ID.in_(ListTellerID))
	return QueryObj,ListTellerID

def isIncomeExpenseBooking(QueryObj,ID):
	QueryObj = QueryObj.get(ID)
	if QueryObj:
		return True
	else:
		return False

def isRecordReverse(ID):
	ObjReverse = validationReverse(MKT_TELLER,MKT_TELLER_INAU,ID)
	return ObjReverse[0]

def setAuthorizeIncomeExpenseBooking(ID):
	try:
		print "start authorize"
		IncomeExpenseObj = MKT_INCOME_EXPENSE_BOOKING_INAU.query.get(ID)
		if ID[-1:] == 'R':
			Opearation = "Reverse"
		else:
			Opearation = "Authorize"
		print "you are authorize the %s record"%Opearation
		Audit 		= mktaudit.getAuditrail()
		Inputter	= IncomeExpenseObj.Inputter
		Createdon 	= IncomeExpenseObj.Createdon
		Branch 		= IncomeExpenseObj.Branch
		Authorizer 	= Audit['Authorizer']
		Authorizeon	= Audit['Authorizeon']
		
		IncomeExpenseCat = IncomeExpenseObj.IncomeExpenseCat
		Type 		= IncomeExpenseObj.Type
		Currency 	= IncomeExpenseObj.Currency
		Account 	= IncomeExpenseObj.Account
		Category 	= IncomeExpenseObj.Category
		Amount  	= Decimal(IncomeExpenseObj.Amount)
		Module 		= 'IE'
		Transaction = IncomeExpenseObj.Transaction
		TranDate 	= IncomeExpenseObj.TranDate
		Reference 	=  IncomeExpenseObj.Reference
		Note 		= IncomeExpenseObj.Note
		CheckAccounting =  setIncomeExpenseBookingAccouting(Opearation,Inputter,Createdon,Authorizer,Authorizeon,Branch,IncomeExpenseCat,Type,Currency,Account,Category,Amount,Module,Transaction,TranDate,ID,Note)
		if not CheckAccounting[0]:
			return False,CheckAccounting[1]
		print "final authorize"
		return True,""

	except Exception, e:
		db.session.rollback()
		return False," %s"%e

def setReverseIncomeExpenseBooking(AuthLevel,ID,Inputter='',Createdon=''):
	try:
		Message = ""
		TellerParam = mktparam.getTellerParam()
		print "start setReverseIncomeExpenseBooking"
		if AuthLevel == 1:
			print "AuthLevel 1"
			mktaudit.moveAUTHtoINAU(MKT_INCOME_EXPENSE_BOOKING,MKT_INCOME_EXPENSE_BOOKING_INAU,ID,Inputter,Createdon, 'INAU', '-1')
			Message = "The record was reversed successfully, record id:%sR in INAU." % ID
			print Message
		else:
			print "AuthLevel : 0"
			IncomeExpenseObj = MKT_INCOME_EXPENSE_BOOKING.query.get(ID)
			ID = ID+"R"
			Opearation 	= "Reverse"
			Audit 		= mktaudit.getAuditrail()
			Inputter	= Audit['Inputter']
			Createdon 	= Audit['Createdon']
			Authorizer 	= Audit['Authorizer']
			Authorizeon	= Audit['Authorizeon']
			Branch 		= Audit['Branch']
			
			IncomeExpenseCat = IncomeExpenseObj.IncomeExpenseCat
			Type 		= IncomeExpenseObj.Type
			Currency 	= IncomeExpenseObj.Currency
			Account 	= IncomeExpenseObj.Account
			Category 	= IncomeExpenseObj.Category
			Amount  	= Decimal(IncomeExpenseObj.Amount)
			Module 		= 'IE'
			Transaction = IncomeExpenseObj.Transaction
			TranDate 	= IncomeExpenseObj.TranDate
			Reference 	= IncomeExpenseObj.Reference
			Note 		= IncomeExpenseObj.Note
			RevTransaction = TellerParam.RevTransaction
			print "before setIncomeExpenseBookingAccouting"
			CheckAccounting =  setIncomeExpenseBookingAccouting(Opearation,Inputter,Createdon,Authorizer,Authorizeon,Branch,IncomeExpenseCat,Type,Currency,Account,Category,Amount,Module,RevTransaction,TranDate,ID,Note)
			if not CheckAccounting[0]:
				db.session.rollback()
				return False,CheckAccounting[1]

			# print "Adding record reverse to db session "
			InsertObj = MKT_INCOME_EXPENSE_BOOKING(
						Status			= 'AUTH',
						Curr 			= '0',
						Inputter 		= Inputter,
						Createdon 		= Createdon,
						Authorizer 		= Authorizer,
						Authorizeon 	= Authorizeon,
						Branch			= Branch,
						ID 				= ID,
						IncomeExpenseCat= IncomeExpenseCat,
						Type 			= Type,
						Account 		= Account,
						Category 		= Category,
						Currency 		= Currency,
						Amount 			= Amount,
						Transaction 	= RevTransaction,
						TranDate 		= TranDate,
						Reference 		= Reference,
						Note 			= Note )

			db.session.add(InsertObj)
			Message = "The record was reversed successfully, record id:%s"%ID
			print "finish reversed"
		print "before return"
		print "Message:"+Message
		return True,Message
	except Exception, e:
		raise
		db.session.rollback()
		return False," %s"%e		
def setIncomeExpenseBookingAccouting(Opearation,Inputter,Createdon,Authorizer,Authorizeon,Branch,IncomeExpenseCat,Type,Currency,Account,Category,Amount,Module,Transaction,TranDate,Reference,Note):
	try:
		tmpCategory = Category

		#Commit Authorize record
		if Opearation == "Authorize":
			for i in range(0, 2):
				Category = tmpCategory
				if i == 0:

					if Type == "Income":
							#Debit Suspend A/C
							DrCr 			= "Dr"
							Mode 			= ""
							UpdateLastTran 	= "YES"

					if Type == "Expense":

							#Debit Expense Category
							DrCr 			= "Dr"
							Mode 			= "Direct"
							UpdateLastTran 	= "NO"
							Category 		= IncomeExpenseCat
				else:
					if Type == "Income":

							#Credit Income Category
							DrCr 			= "Cr"
							Mode 			= "Direct"
							UpdateLastTran 	= "NO"
							Category 		= IncomeExpenseCat

					if Type == "Expense":

							#Credit Suspend A/C
							DrCr 			= "Cr"
							Mode 			= ""
							UpdateLastTran 	= "YES"

				#get Gl key	
				GL_KEYS 		= mktaccounting.getConsolKey(Category,Currency,"","")
				ObjAccounting 	= mktaccounting.postAccounting(		

									"AUTH", 				# Status
									"0", 					# Curr
									Inputter,				# Inputter
									Createdon, 				# Createdon
									Authorizer,				# Authorizer
									Authorizeon,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Amount, 				# Amount
									Module,					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									Reference, 				# Reference
									Note, 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# ConsolKey
									Mode,					# Mode
									UpdateLastTran)			# Tell function to update LastTransaction Customer's Account

		#Commit Reverse record
		else:

			for i in range(0, 2):
				Category = tmpCategory
				if i == 0:
					if Type == "Income":
						#Credit Income Category
						DrCr 			= "Dr"
						Mode 			= "Direct"
						UpdateLastTran 	= "NO"
						Category 		= IncomeExpenseCat
						
					if Type == "Expense":
						
						#Credit Suspend A/C
						DrCr 			= "Dr"
						Mode 			= ""
						UpdateLastTran 	= "YES"

				else:
					if Type == "Income":
						
						#Debit Suspend A/C
						DrCr 			= "Cr"
						Mode 			= ""
						UpdateLastTran 	= "YES"
						
					if Type == "Expense":
						
						#Debit Expense Category
						DrCr 			= "Cr"
						Mode 			= "Direct"
						UpdateLastTran 	= "NO"
						Category 		= IncomeExpenseCat

				#get Gl key	
				GL_KEYS 		= mktaccounting.getConsolKey(Category,Currency,"","")
				ObjAccounting 	= mktaccounting.postAccounting(		

										"AUTH", 				# Status
										"0", 					# Curr
										Inputter,				# Inputter
										Createdon, 				# Createdon
										Authorizer,				# Authorizer
										Authorizeon,			# Authorizeon
										"", 					# AEID
										Account,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Amount, 				# Amount
										Module,					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										Reference, 				# Reference
										Note, 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# ConsolKey
										Mode,					# Mode
										UpdateLastTran)			# Tell function to update LastTransaction Customer's Account

		return True,""
	except Exception, e:
		db.session.rollback()
		return False," %s"%e


def getValidationRequired(kwargs):
	for key,value in kwargs.iteritems():
		if not value:
			return False,"%s is not found for post accounting."%key
	return True,""

#----------------------------- Block Cash Account ---------------------------

def insertCashAccount(AuthLevel,Inputter,Createdon,Authorizer,Authorizeon,Branch,ID,ListCurrency,ListAccount,Type):
	# print 'I am here in function insertCashAccount'

	Param = mktparam.getTellerParam()
	if Param:
		QueryObj 		= MKT_CASH_ACCOUNT.query.get(ID)
		if Type == "V":
			LabelType 		= "VAULT"
			AccProduct 		= Param.VaultAccPro
			AccCategory 	= Param.VaultCategory

		elif Type == "T":
			LabelType 		= "TILL"
			AccProduct 		= Param.TillAccPro
			AccCategory 	= Param.TillCategory

		elif Type == "W":
			LabelType 		= "WALLET"
			AccProduct 		= Param.WalletAccPro
			AccCategory 	= Param.WalletCategory

		# Check Cash ID already exists mean that u are update Cash Account or not if not mean that add new account
		if QueryObj:

			LiveCurrency 	= QueryObj.Currency.split()
			LiveBranch 		= QueryObj.CashBranch
			LiveAccount 	= QueryObj.Account.split()

			# Add More Currency
			for Currency in ListCurrency :
				AccName = LabelType+"-"+Currency+"-"+ID
				# Change Branch
				if LiveBranch != Branch:

					# Call function to get old account belong
					for AccID in LiveAccount:
						AccObj = mktaccount.getAccount(AccID)
						if AccObj:
							# Move record LIVE to HIST
							mktaudit.moveAUTHtoHIST(MKT_ACCOUNT,MKT_ACCOUNT_HIST,AccObj.ID)
							# Update Account to new User ID 
							mktdb.updateRecord(MKT_ACCOUNT,
												[MKT_ACCOUNT.ID==AccObj.ID],
												{"Branch":Branch})

				# Check find which one is new currency
				if Currency not in LiveCurrency:
					AccID = Currency+AccCategory+ID
					checkRestoreID = mktaudit.isRestoreID(MKT_ACCOUNT,MKT_ACCOUNT_INAU,MKT_ACCOUNT_HIST,AccID)
					if checkRestoreID[0]:
						Curr=checkRestoreID[1]
					else:
						Curr="0"
					# add more account by new currency
					mktaccounting.insert_MKT_ACC(
							Curr,					#Curr
							Inputter, 				#Inputter
							Createdon,				#Createdon
							Authorizer,				#Authorizer
							Authorizeon,			#Authorizeon
							Branch,					#Branch
							Currency+AccCategory+ID,# ID
							AccName,				# AccName
							Currency,				# Currency
							AccProduct, 			# AccProduct
							AccCategory				# AccCategory
							)

			# Remove Currency
			for Currency in LiveCurrency:

				# Check find which currency is remove 
				if Currency not in ListCurrency:

					AccID = Currency+AccCategory+ID
					# Call function to get old account belong 
					AccObj = mktaccount.getAccount(AccID)
					
					if AccObj:

						# Check Vault Account Balance == 0 can be close account
						if float(AccObj.Balance) == float(0):
							# Move record LIVE to HIST
							mktaudit.moveAUTHtoHIST(MKT_ACCOUNT,MKT_ACCOUNT_HIST,AccObj.ID)
							# Delete LIVE 
							mktaudit.deleteAUTH(MKT_ACCOUNT,AccObj.ID)

						else:
							
							return False,Markup('Cash Account# %s still open with some balance %s <br /> You cannot remove currncy %s'%(AccObj.ID,AccObj.Balance,AccObj.Currency))

		else:# New record
			for Currency in ListCurrency :
				AccName = LabelType+"-"+Currency+"-"+ID
				AccID = Currency+AccCategory+ID
				checkRestoreID = mktaudit.isRestoreID(MKT_ACCOUNT,MKT_ACCOUNT_INAU,MKT_ACCOUNT_HIST,AccID)
				if checkRestoreID[0]:
					Curr=checkRestoreID[1]
				else:
					Curr="0"
				mktaccounting.insert_MKT_ACC(
						Curr,					#Curr
						Inputter, 				#Inputter
						Createdon,				#Createdon
						Authorizer,				#Authorizer
						Authorizeon,			#Authorizeon
						Branch,					#Branch
						AccID, 					# ID
						AccName,				# AccName
						Currency,				# Currency
						AccProduct, 			# AccProduct
						AccCategory 			# AccCategory
						)
		return True,""
	else:
		return False,"Cannot create cash account! Please set Teller Parameter first."
	
	return True,""

def setAuthCashAccount(AuthLevel,ID):
	# Authorize have two transcations INAU, RNAU 
	TellerParam = mktparam.getTellerParam()
	# print 'beforeAuthorize: %s'% AuthLevel
	if AuthLevel == 1:

		ObjINAU 	= MKT_CASH_ACCOUNT_INAU.query.get(ID)

		if ObjINAU:

			ListCurrency= ObjINAU.Currency.split()
			ListAccount = ObjINAU.Account.split()
			Branch 		= ObjINAU.CashBranch
			Type 		= ObjINAU.Type
			Audit 		= mktaudit.getAuditrail()
			Inputter	= ObjINAU.Inputter
			Createdon 	= ObjINAU.Createdon
			Authorizer 	= Audit['Authorizer']
			Authorizeon	= Audit['Authorizeon']
			# Cannot authorize by urseft
			if Inputter != Authorizer:

				if ObjINAU.Status == 'RNAU':
					# print "Authorize Record RNAU"
					for AccID in ListAccount :
						CashAccount = mktaccount.getAccount(AccID)
						if CashAccount:
							# print 'before call moveAUTHtoHIST'
							# Remove account to history 
							mktaudit.moveAUTHtoHIST(MKT_ACCOUNT,MKT_ACCOUNT_HIST,CashAccount.ID)
							# print 'After '
							# Delete live record
							mktaudit.deleteAUTH(MKT_ACCOUNT,CashAccount.ID)
							# print 'After call deleteAUTH'
				else:
					# print "Authorize Record INAU"				
					# Call function to commit add account by currency 
					CheckCommit = insertCashAccount(AuthLevel,Inputter,Createdon,Authorizer,Authorizeon,Branch,ID,ListCurrency,ListAccount,Type)
					if not CheckCommit[0]:
						return False,CheckCommit[1]
				# Authorize was successfully.
				return True,""

def getValidationCashAccountBranch(ID,Currency,Branch):

	ListCurrency 		= Currency.split()
	
	QueryObj = MKT_CASH_ACCOUNT.query.get(ID)

	BranchDescription = mktreport.VLOOKUP(["MKT_BRANCH","ID",Branch,"Description"])
	
	if QueryObj:
		CheckChangeBranch = QueryObj.CashBranch

		if CheckChangeBranch!= Branch:
			LiveAccount = QueryObj.Account.split()
			for item in LiveAccount:
				AccObj = mktaccount.getAccount(item)
				
				if AccObj:

					if float(AccObj.Balance) != float(0):
					
						return False,Markup('Cash Account# %s still open with balance : %s <br /> You cannot change branch to %s.'%
												(AccObj.ID,float(AccObj.Balance),BranchDescription))
		return True,""
	else:
		return True,""
						
def getValidationCashAccountReverse(AuthLevel,ID):
	ListAccClose = []
	ListAccOpen  = []
	Msg 			= ""
	QueryObj 		= MKT_CASH_ACCOUNT.query.get(ID)
	if QueryObj:
		ListCurrency= QueryObj.Currency.split()
		ListAccount = QueryObj.Account.split()

		for AccID in ListAccount:
			# Check Balance Account
			AccObj = mktaccount.getAccount(AccID)
			if AccObj:
				# Balance = 0 mean can reverse
				if float(AccObj.Balance) == float(0):
					ListAccClose.append((AccObj.ID,AccObj.Currency,float(AccObj.Balance)))
				else:
					ListAccOpen.append((AccObj.ID,AccObj.Currency,float(AccObj.Balance)))
	if ListAccOpen:
		for item in ListAccOpen:

			Msg+=Markup('Cash Account# %s still open with balance: %s <br />'%(item[0],item[2]))

		if Msg:

			return True,msg_error+Msg+"You cannot reverse record"
	else:
		# u can reverseRec record
		if AuthLevel == 0:
			for item in ListAccClose :
				AccID = item[0]
				AccObj = mktaccount.getAccount(AccID)
				if AccObj:
					# print 'before call moveAUTHtoHIST'
					# Remove account to history 
					mktaudit.moveAUTHtoHIST(MKT_ACCOUNT,MKT_ACCOUNT_HIST,AccID)
					# print 'After '
					# Delete live record
					mktaudit.deleteAUTH(MKT_ACCOUNT,AccID)
					# print 'After call deleteAUTH'
		return True,""

def getValidationDuplicateCurreny(ListCurrency):
	CurrencyObj = MKT_CURRENCY.query
	for item in ListCurrency:
		ChkCurrencyObj 	=CurrencyObj.filter(MKT_CURRENCY.ID==str(item)).all()
		if not ChkCurrencyObj:
			return False,"%s currency not foud!"%item

	DuplicateCur   = [x for x in ListCurrency if ListCurrency.count(x) > 1]
	if DuplicateCur:
		return False,"Duplicate %s currency"%DuplicateCur[0]
	return True,""

def getValidationTellerParam():
	TellerParam = mktparam.getTellerParam()
	if TellerParam:
		return True,""
	else:
		return False,"Please configuration Teller Parameter first."


def getChangeAccount(ID,CurrentAccount):

	ChangeAccount	= []
	QueryObj 		= MKT_CASH_ACCOUNT.query.get(ID)
	if QueryObj:

		LiveAccount = QueryObj.Account.split()

		for item in LiveAccount:
			if item not in CurrentAccount:
				ChangeAccount.append(item)

	return ChangeAccount

def getAccountClose(ListAccount):
	ListAccClose 	= []
	if ListAccount:
		for item in ListAccount:
			AccObj = mktaccount.getAccount(item)
			if AccObj:
				if float(AccObj.Balance) == float(0):
					ListAccClose.append((AccObj.ID,AccObj.Currency,float(AccObj.Balance),AccObj.AccCategory))
	return ListAccClose

def getAccountOpen(ListAccount):

	ListAccOpen 	= []
	if ListAccount:
		for item in ListAccount:
			AccObj = mktaccount.getAccount(item)
			if AccObj:
				if float(AccObj.Balance) != float(0):
					ListAccOpen.append((AccObj.ID,AccObj.Currency,float(AccObj.Balance),AccObj.AccCategory))
	return ListAccOpen
from app.User.models import *
def getUserCashAccount(**kwargs):

	Account 	= ""
	CashID 		= ""
	Currency 	= ""
	ListUser 	= []
	if "CashID" in kwargs:
		CashID = kwargs['CashID']
		UserObj = MKT_USER.query.filter(MKT_USER.CashAccount==CashID)
		for row in UserObj:
			ListUser.append(str(row.ID))

	return ListUser

# Hot Field in Fund Withdrawal and Fund Deposit
def getFundAcount(Resource,AccID=""):

	TellerParam = mktparam.getTellerParam()
	TranCode 	= ""
	CrAccount 	= ""
	CrCategory 	= ""
	CrCurrency 	= ""
	DrAccount 	= ""
	DrCurrency 	= ""
	DrCategory 	= ""
	Message 	= ""
	LabelType	= ""
	CashAccount = ""

	if TellerParam:
		
		UserObj  		= mktuser.getUser()
		DefaultBranch 	= mktuser.getCurrentBranch()
		VaultCategory 	= TellerParam.VaultCategory
		TillCategory 	= TellerParam.TillCategory

		if Resource=="DP" :
			if AccID=='DP':
				session['TELLER']='DP'

		if Resource=="WD":
			if AccID=='WD':
				session['TELLER']='WD'

		# app.logger.debug(request.args.get('TCrAccount'))
		# app.logger.debug('session name'+session['TELLER'])
		if AccID:
			AccObj = MKT_ACCOUNT.query
			
			AccountExternal = AccObj.filter(MKT_ACCOUNT.ID==AccID).first()
			if AccountExternal:

				Currency = AccountExternal.Currency
				if Resource in ["FD","DP"]:
					CrCategory 	= str(AccountExternal.AccCategory)
					CrCurrency 	= str(AccountExternal.Currency)
					TranCode 	= TellerParam.DepTransaction
					LabelType	= "Vault" if Resource == "FD" else "Till"

				elif Resource in ["FW","WD"]:
					DrCategory 	= str(AccountExternal.AccCategory)
					DrCurrency 	= str(AccountExternal.Currency)
					TranCode 	= TellerParam.WitTransaction
					LabelType	= "Vault" if Resource == "FW" else "Till"

				CashID  		= UserObj.CashAccount
				CashObj  		= MKT_CASH_ACCOUNT.query.get(CashID)
				if CashObj:
					Type = CashObj.Type
					if Type=="V":
						CashAccount = "%s%s%s"%(Currency,VaultCategory,CashID)
					elif Type == "T":
						CashAccount = "%s%s%s"%(Currency,TillCategory,CashID)

					if CashObj.CashBranch == DefaultBranch:
						
						FilterAccObj = AccObj.get(CashAccount)

						if FilterAccObj:
							if Resource in ["FD","DP"]:
								DrAccount  = FilterAccObj.ID
								DrCurrency = FilterAccObj.Currency
								DrCategory = FilterAccObj.AccCategory

							elif Resource in ["FW","WD"]:
								CrAccount 	= FilterAccObj.ID
								CrCurrency 	= FilterAccObj.Currency
								CrCategory 	= FilterAccObj.AccCategory
						else:
							Message = "Cash account# %s not found"%CashAccount
					else:
						Message = "User don't have %s Account in Branch #%s"%(LabelType,DefaultBranch)

				else:
					Message = "User don't have cash account."
				
			else:
				Message = "Account #%s not found."%AccID
	else:
		Message= "Please set Teller Parameter first."

	return jsonify(TranCode=TranCode,CrAccount=CrAccount,CrCategory=CrCategory, CrCurrency=CrCurrency,DrAccount=DrAccount,DrCurrency=DrCurrency,DrCategory=DrCategory,Message=Message)



def isUserVaultAccount():
	UserObj 		= mktuser.getUser()
	if UserObj:
		CashID 	= UserObj.CashAccount
		CurrentBranch 	= mktuser.getCurrentBranch()
		if CashID:
			CashObj = MKT_CASH_ACCOUNT.query.get(CashID)
			if CashObj:
				CashBranch 	= CashObj.CashBranch
				Type 		= CashObj.Type
				if Type != "V":
					return False,"User don't have Vault Account."
				if CashBranch != CurrentBranch :
					Branch = MKT_BRANCH.query.get(CurrentBranch)
					return False,"You don't have Vault Account in %s branch."%Branch.Description
				else:
					# Pass
					return True,CashID
			else:
				return False,"Vault Account not found."
		else:
			return False,"User don't have Vault Account."

def isUserTillAccount():

	UserObj 		= mktuser.getUser()
	if UserObj:
		CashID 	= UserObj.CashAccount
		CurrentBranch 	= mktuser.getCurrentBranch()
		if CashID:
			CashObj = MKT_CASH_ACCOUNT.query.get(CashID)
			if CashObj:
				CashBranch 	= CashObj.CashBranch
				Type 		= CashObj.Type
				if Type != "T":
					return False,"User don't have Till Account."
				if CashBranch != CurrentBranch :
					Branch = MKT_BRANCH.query.get(CurrentBranch)
					return False,"You don't have Till Account in %s branch."%Branch.Description
				else:
					# Pass
					return True,CashID
			else:
				return False,"Till Account not found."
		else:
			return False,"User don't have Till Account."

def getCashCategory():
	TellerParam 	= mktparam.getTellerParam()
	CashCategory 	= []
	if TellerParam:
		CashCategory.append(TellerParam.VaultCategory)
		CashCategory.append(TellerParam.TillCategory)
		CashCategory.append(TellerParam.WalletCategory)
	return CashCategory

def isCashCateogry(Category):

	CashCategory = getCashCategory()
	CashCategory = filter(None,CashCategory)
	if Category in CashCategory:
		return True
	else:
		return False

def getCashAccountByCategory(Category,Currency,Inputter,Branch):

	ID 		= mktuser.getUser(Inputter).CashAccount
	Account = str(Currency+Category+ID)
	Acc 	= MKT_ACCOUNT.query.get(Account)
	if Acc:
		if Acc.Branch == Branch:
			return Acc
	return []

def getTillAccountObj(UserID="",Currency=""):

	AccObj = []
	if UserID:
		UserObj 		= mktuser.getUser(UserID)
	else:
		UserObj 		= mktuser.getUser()
		
	if UserObj:
		CashID 		= UserObj.CashAccount
		CashObj 	= MKT_CASH_ACCOUNT.query.get(CashID)
		if CashObj:
			CashAccount = CashObj.Account.split()
			if Currency:
				AccObj = MKT_ACCOUNT.query.filter(MKT_ACCOUNT.ID.in_(CashAccount)).\
											filter(MKT_ACCOUNT.Currency==Currency).first()

	return AccObj

def getCashAccountObj(Currency=""):
	AccObj = []
	UserObj 		= mktuser.getUser()
	if UserObj:
		CashID 		= UserObj.CashAccount
		CashObj 	= MKT_CASH_ACCOUNT.query.get(CashID)
		if CashObj:
			CashAccount = CashObj.Account.split()
			if Currency:
				AccObj = MKT_ACCOUNT.query.filter(MKT_ACCOUNT.ID.in_(CashAccount)).\
											filter(MKT_ACCOUNT.Currency==Currency).first()
			else:
				AccObj = MKT_ACCOUNT.query.filter(MKT_ACCOUNT.ID.in_(CashAccount))
											
	return AccObj

def validationReverse(AUTH,INAU,ID):

	if ID[-1:] == 'R':
		return True, msg_error+" The record was reversed already. Cannot reverse this record"
	else:
		Query = AUTH.query.get(ID+'R')
		# print " can reverse"
		if not Query :
			ObjINAU = INAU.query.get(ID+'R')
			if ObjINAU:
				return True, msg_error+" The record was already reversed and in list un-authorize. Cannot reverse this record"
			else:
				return False,''
		else:
			return True,msg_error+" The record was reversed already. Cannot reverse this record"


def setAuthorize(TypeModule,AUTH,INAU,ID):
	# When reverse record we need to post another record which opposites between debit and credit of the old one
	try:
		Message 	= ""
		ObjINAU 	= INAU.query.get(ID)

		if ObjINAU:

			# If Status RNAU mean that Authorize record reverse
			if ID[-1:] == 'R':
				# print "Authorize Record RNAU"
				# Deposit
				if TypeModule == 'DP':

					DrAcc 		= ObjINAU.TCrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.TDrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				# Withdrawal
				elif TypeModule == 'WD':

					DrAcc 		= ObjINAU.TCrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.TDrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule
				
				# Till To Till
				elif TypeModule == 'TT':

					DrAcc 		= ObjINAU.TCrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.TDrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				# Income Posting
				elif TypeModule == 'IP':

					DrAcc 		= ObjINAU.TCrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.TDrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				# Expense Posting
				elif TypeModule == 'EP':

					DrAcc 		= ObjINAU.TCrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.TDrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				# Fund Transfer Deposit
				elif TypeModule == 'FD':

					DrAcc 		= ObjINAU.CrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.DrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				# Fund Transfer Withdrawal
				elif TypeModule == 'FW':

					DrAcc 		= ObjINAU.CrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.DrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				# Fund Transfer FundAdvance
				elif TypeModule == 'FA':

					DrAcc 		= ObjINAU.CrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.DrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				# Fund Transfer
				elif TypeModule == 'FT':

					DrAcc 		= ObjINAU.CrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.DrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				# Fund Settlement
				elif TypeModule == 'FS':

					DrAcc 		= ObjINAU.CrAccount
					DrCat 		= ObjINAU.CrCategory
					DrCur 		= ObjINAU.CrCurrency

					CrAcc 		= ObjINAU.DrAccount
					CrCat 		= ObjINAU.DrCategory
					CrCur 		= ObjINAU.DrCurrency
					Module 		= TypeModule

				Amo  		= Decimal(ObjINAU.Amount)
				Tran  		= ObjINAU.Transaction
				TranDate 	= ObjINAU.TranDate
				Ref 		= ObjINAU.ID
				Note 		= ObjINAU.Note


				Audit 		= mktaudit.getAuditrail()
				Inputter	= ObjINAU.Inputter
				Createdon 	= ObjINAU.Createdon
				Branch 		= ObjINAU.Branch
				Authorizer 	= Audit['Authorizer']
				Authorizeon	= Audit['Authorizeon']
		

				if Inputter != Authorizer:
					
					# Call function to commit add account by currency 
					CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note)
					#Block validation
					if not CheckAccounting[0]:
						#Catch error
						db.session.rollback()
						return False,msg_error+CheckAccounting[1]

					# Successfully
					Message = msg_authorize_0
					return True,Message
				else:
					#Catch error
					db.session.rollback()
					return False,msg_error+msg_cannot_authorize
			else:
				# Mean that Authorize INAU
				# print "Authorize Record INAU"

				if TypeModule == 'DP':
				
					DrAcc 		= ObjINAU.TDrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.TCrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Withdrawal
				elif TypeModule == 'WD':

					DrAcc 		= ObjINAU.TDrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.TCrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Till To Till
				elif TypeModule == 'TT':

					DrAcc 		= ObjINAU.TDrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.TCrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Income Posting
				elif TypeModule == 'IP':

					DrAcc 		= ObjINAU.TDrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.TCrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Expense Posting
				elif TypeModule == 'EP':

					DrAcc 		= ObjINAU.TDrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.TCrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Fund Transfer Deposit
				elif TypeModule == 'FD':
				
					DrAcc 		= ObjINAU.DrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.CrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Fund Transfer Withdrawal
				elif TypeModule == 'FW':

					DrAcc 		= ObjINAU.DrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.CrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Fund Transfer FundAdvance
				elif TypeModule == 'FA':

					DrAcc 		= ObjINAU.DrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.CrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Fund Transfer
				elif TypeModule == 'FT':

					DrAcc 		= ObjINAU.DrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.CrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				# Fund Transfer
				elif TypeModule == 'FS':

					DrAcc 		= ObjINAU.DrAccount
					DrCat 		= ObjINAU.DrCategory
					DrCur 		= ObjINAU.DrCurrency

					CrAcc 		= ObjINAU.CrAccount
					CrCat 		= ObjINAU.CrCategory
					CrCur 		= ObjINAU.CrCurrency
					Module 		= TypeModule

				Amo  		= Decimal(ObjINAU.Amount)
				Tran  		= ObjINAU.Transaction
				TranDate 	= ObjINAU.TranDate
				Ref 		= ObjINAU.ID
				Note 		= ObjINAU.Note

				Audit 		= mktaudit.getAuditrail()
				Inputter	= ObjINAU.Inputter
				Createdon 	= ObjINAU.Createdon
				Branch 		= ObjINAU.Branch
				Authorizer 	= Audit['Authorizer']
				Authorizeon	= Audit['Authorizeon']

				if Inputter != Authorizer:
					
					# Call function to commit add account by currency 
					CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note)
					#Block validation
					if not CheckAccounting[0]:
						#Catch error
						db.session.rollback()
						return False,msg_error+CheckAccounting[1]

					# Successfully
					Message = msg_authorize_0
					return True,Message
				else:
					#Catch error
					db.session.rollback()
					return False,msg_error+msg_cannot_authorize
		else:
			db.session.rollback()
			Message = msg_warning+"Record not found..."
			return False,Message

	except Exception, e:
		db.session.rollback()
		return False,msg_error+' %s'%e

def setReverse(TypeProcess,AuthLevel,TypeModule,AUTH,INAU,ID,Inputter='',Createdon=''):
	try:
		Message = ""
		TellerParam = mktparam.getTellerParam()
		ObjAUTH 	= AUTH.query.get(ID)
		# LiveInputter = ObjAUTH.Inputter
		# if Inputter != LiveInputter:

		# 	Message = msg_warning+"User %s cannot reverse record for user %s."%(Inputter,LiveInputter)
		# 	return False,Message
			
		if AuthLevel == 1:

			mktaudit.moveAUTHtoINAU(AUTH,INAU,ID,Inputter,Createdon, 'INAU', '-1')
			Message = "The record was reversed successfully, record id:%sR in INAU."%ID
			return True,Message
		else:
			# print "Start reverse record seft Authorize"
			# Seft Authorize need to reverse
			
			# print ObjAUTH
			# print 'AUTH :%s'% AUTH
			# print 'INAU :%s'% INAU
			# print 'ID :%s'%ID
			Mode = ""
			if ObjAUTH:

				# Teller Deposit 
				if TypeModule == 'DP':

					DrAcc 		= ObjAUTH.TCrAccount 
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.TDrAccount 
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# Teller Withdrawal
				elif TypeModule == 'WD':

					DrAcc 		= ObjAUTH.TCrAccount
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.TDrAccount
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""
				
				# Till To Till
				elif TypeModule == 'TT':

					DrAcc 		= ObjAUTH.TCrAccount
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.TDrAccount
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# Income Posting
				elif TypeModule == 'IP':

					DrAcc 		= ObjAUTH.TCrAccount
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.TDrAccount
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# Expense Posting
				elif TypeModule == 'EP':

					DrAcc 		= ObjAUTH.TCrAccount
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.TDrAccount
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# FundTransfer Deposit
				elif TypeModule == 'FD':

					DrAcc 		= ObjAUTH.CrAccount 
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.DrAccount 
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# FundTransfer Withdrawal
				elif TypeModule == 'FW':

					DrAcc 		= ObjAUTH.CrAccount
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.DrAccount
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# FundTransfer FundAdvance
				elif TypeModule == 'FA':

					DrAcc 		= ObjAUTH.CrAccount
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.DrAccount
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# FundTransfer
				elif TypeModule == 'FT':

					DrAcc 		= ObjAUTH.CrAccount
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.DrAccount
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# FundSettlement
				elif TypeModule == 'FS':

					DrAcc 		= ObjAUTH.CrAccount
					DrCat 		= ObjAUTH.CrCategory
					DrCur 		= ObjAUTH.CrCurrency

					CrAcc 		= ObjAUTH.DrAccount
					CrCat 		= ObjAUTH.DrCategory
					CrCur 		= ObjAUTH.DrCurrency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TranDate
					Mode 		= ""

				# Journal Entry
				elif TypeModule == 'JE':

					DrAcc 		= ""
					DrCat 		= ObjAUTH.CreditCategory
					DrCur 		= ObjAUTH.Currency

					CrAcc 		= ""
					CrCat 		= ObjAUTH.DebitCategory
					CrCur 		= ObjAUTH.Currency
					Module 		= TypeModule
					TranDate 	= ObjAUTH.TransactionDate
					Mode 		= "Direct"

				Amo  		= Decimal(ObjAUTH.Amount)
				Tran  		= TellerParam.RevTransaction
				Ref 		= ObjAUTH.Reference
				ID			= ObjAUTH.ID+'R'
				Note 		= ObjAUTH.Note

				Audit 		= mktaudit.getAuditrail()
				Inputter	= Audit['Inputter']
				Createdon 	= Audit['Createdon']
				Authorizer 	= Audit['Authorizer']
				Authorizeon	= Audit['Authorizeon']
				Branch 		= Audit['Branch']

				if TypeProcess == "Teller":

					# print "Adding record reverse TT to db session "
					InsertObjAUTH = AUTH(
						Status			= 'AUTH',
						Curr 			= '0',
						Inputter 		= Inputter,
						Createdon 		= Createdon,
						Authorizer 		= Authorizer,
						Authorizeon 	= Authorizeon,
						Branch			= Branch,
						ID 				= ID,
						TDrAccount 		= DrAcc,
						DrCategory 		= DrCat,
						DrCurrency 		= DrCur,
						TCrAccount 		= CrAcc,
						CrCategory 		= CrCat,
						CrCurrency 		= CrCur,
						Amount 			= Amo,
						Transaction 	= Tran,
						TranDate 		= TranDate,
						Reference 		= Ref,
						Note 			= Note )

				elif TypeProcess == "JournalEntry":

					# print "Adding record reverse FT to db session "
					InsertObjAUTH = AUTH(
						Status			= 'AUTH',
						Curr 			= '0',
						Inputter 		= Inputter,
						Createdon 		= Createdon,
						Authorizer 		= Authorizer,
						Authorizeon 	= Authorizeon,
						Branch			= Branch,
						ID 				= ID,
						DebitCategory 	= DrCat,
						CreditCategory 	= CrCat,
						Currency 		= CrCur,
						Amount 			= Amo,
						Transaction 	= Tran,
						TransactionDate = TranDate,
						Reference 		= Ref,
						Note 			= Note )

				else: 
					
					# print "Adding record reverse FT to db session "
					InsertObjAUTH = AUTH(
						Status			= 'AUTH',
						Curr 			= '0',
						Inputter 		= Inputter,
						Createdon 		= Createdon,
						Authorizer 		= Authorizer,
						Authorizeon 	= Authorizeon,
						Branch			= Branch,
						ID 				= ID,
						DrAccount 		= DrAcc,
						DrCategory 		= DrCat,
						DrCurrency 		= DrCur,
						CrAccount 		= CrAcc,
						CrCategory 		= CrCat,
						CrCurrency 		= CrCur,
						Amount 			= Amo,
						Transaction 	= Tran,
						TranDate 		= TranDate,
						Reference 		= Ref,
						Note 			= Note )

				db.session.add(InsertObjAUTH)
				# print "Added record to session "				
				# Call function to commit add account by currency 
				CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,ID,Note,Mode)
				#Block validation
				if not CheckAccounting[0]:
					#Catch error
					db.session.rollback()
					return False,msg_error+CheckAccounting[1]

			else:
				db.session.rollback()
				Message = msg_warning+"Record not found..."
				return False,Message

			Message = "The record was reversed successfully, record id:%s"%ID
			return True,Message

	except Exception, e:
		db.session.rollback()
		return False,msg_error+" %s"%e

#----- Till Opearation

def setAuthorizeTillOperation(TypeModule,ID):
	# When reverse record we need to post another record which opposites between debit and credit of the old one
	try:
		Message 	= ""
		MainObj		= MKT_TILL_OPERATION_INAU.query.get(ID)
		DetailObj 	= MKT_TO_DETAIL_INAU.query.filter(MKT_TO_DETAIL_INAU.ID==ID)
		TellerParam = mktparam.getTellerParam()
		Tran  		= MainObj.Transaction
		TranDate 	= MainObj.TranDate
		Ref 		= MainObj.ID
		Note 		= MainObj.Note
		Mode 		= ""

		Audit 		= mktaudit.getAuditrail()
		Inputter	= MainObj.Inputter
		Createdon 	= MainObj.Createdon
		Branch 		= MainObj.Branch
		Authorizer 	= Audit['Authorizer']
		Authorizeon	= Audit['Authorizeon']

		if MainObj:
			if Inputter == Authorizer:
				#Catch error
				db.session.rollback()
				return False,msg_error+msg_cannot_authorize

			# If Status RNAU mean that Authorize record reverse
			if ID[-1:] == 'R':
				# print "Authorize Record RNAU"
				# Till Open
				if TypeModule == 'TO':

					Module 		= TypeModule

					for row in DetailObj:

						Amo 		= Decimal(row.Amount)
						Currency 	= row.Currency
						VaultObj 	= mktuser.getVaultInfo(Currency,Inputter)

						if VaultObj:
							DrAcc 	= VaultObj.ID
							DrCat	= VaultObj.AccCategory
							DrCur 	= VaultObj.Currency

						CrAcc 	=	row.TillAccount
						CrCur 	=	Currency
						CrCat 	=	row.Category

						CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
						if CheckBalanceDr:
							return False,msg_error + CheckBalanceDr
						
						CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
						if CheckBalanceCr:
							return False,msg_error + CheckBalanceCr

					
						# print "Added record to session "
						CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
						#Block validation
						if not CheckAccounting[0]:
							#Catch error
							db.session.rollback()
							return False,msg_error+CheckAccounting[1]

				#Till Close
				if TypeModule == 'TC':

					Module 		= TypeModule
					UnBalance 	= TellerParam.UnBalance
					AccObj 		= MKT_ACCOUNT.query

					for row in DetailObj:

						TillAccount = str(row.TillAccount)
						Amo   		= Decimal(row.Amount)
						FilterAcc 	= AccObj.get(TillAccount)

						if FilterAcc:
							
							Balance 		= Decimal(row.Balance)
							TillAccount 	= FilterAcc.ID
							TillCategory	= FilterAcc.AccCategory
							TillCurrency 	= FilterAcc.Currency

						else:
							return False,msg_error+"Till Account not found."

						VaultObj   	= mktuser.getVaultInfo(TillCurrency,Inputter)
						if VaultObj:
							VaultAccount 	=	VaultObj.ID
							VaultCategory 	=	VaultObj.AccCategory
							VaultCurrency 	=	VaultObj.Currency
						else:
							return False,msg_error+"Vault Account not found."
						# check Balance and Amount (Shortage and Surplus)
						# Shortage
						if Amo < Balance:
							if UnBalance == "Y":

								DrAcc = TillAccount
								DrCat = TillCategory
								DrCur = TillCurrency

								CrAcc = VaultAccount
								CrCat = VaultCategory
								CrCur = VaultCurrency

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]

								#Post Shortage Accout - 
								ShortageAcc  = mktaccount.getShortageAccount(CrCur,Branch)
								if ShortageAcc:
									Amo = Balance - Amo
									DrAcc = TillAccount
									DrCat = TillCategory
									DrCur = TillCurrency

								else:
									return False,msg_error+"Shortage Account not found."

								CrAcc = ShortageAcc.ID
								CrCat = ShortageAcc.AccCategory
								CrCur = ShortageAcc.Currency

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]
							
							else:
								return False,msg_error+"Cannot close till with amount less than balance."

						# check Balance and Amount (Shortage and Surplus)
						# Surplus
						elif Amo > Balance:

							if UnBalance == "Y":

								DrAcc = TillAccount
								DrCat = TillCategory
								DrCur = TillCurrency

								CrAcc = VaultAccount
								CrCat = VaultCategory
								CrCur = VaultCurrency

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]
									
								#Post Surplus Accout + :
								Surplus  = mktaccount.getSurplusAccount(CrCur,Branch)
								Amo = Amo - Balance

								DrAcc = Surplus.ID 
								DrCat = Surplus.AccCategory 
								DrCur = Surplus.Currency

								if Surplus:
									CrAcc = TillAccount
									CrCat = TillCategory
									CrCur = TillCurrency
								else:
									return False,msg_error+"Surplus Account not found."

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]
							else:
								return False,msg_error+"Cannot close till with amount more than balance."
						else:

							DrAcc = TillAccount
							DrCat = TillCategory
							DrCur = TillCurrency

							CrAcc = VaultAccount
							CrCat = VaultCategory
							CrCur = VaultCurrency

							# Call function to commit add account by currency 
							CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
							if not CheckAccounting[0]:
								return False,CheckAccounting[1]
						

				# Successfully
				Message="The record was authorized successfully for reversed, record id:%s"%ID
				return True,Message
			else:
				# Mean that Authorize INAU
				# print "Authorize Record INAU"
				
				#Till Open
				if TypeModule == 'TO':

					Module 		= TypeModule
					for row in DetailObj:
						
						Amo 		= Decimal(row.Amount)
						Currency 	= row.Currency
						VaultObj 	= mktuser.getVaultInfo(Currency,Inputter)

						if VaultObj:
							CrAcc 	= VaultObj.ID
							CrCat	= VaultObj.AccCategory
							CrCur 	= VaultObj.Currency

						DrAcc 	=	row.TillAccount
						DrCur 	=	Currency
						DrCat 	=	row.Category

						CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
						if CheckBalanceDr:
							return False,msg_error + CheckBalanceDr
						
						CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
						if CheckBalanceCr:
							return False,msg_error + CheckBalanceCr

						if Inputter != Authorizer:

							# print "Added record to session "
							CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
							#Block validation
							if not CheckAccounting[0]:
								#Catch error
								db.session.rollback()
								return False,msg_error+CheckAccounting[1]
						else:
							#Catch error
							db.session.rollback()
							return False,msg_error+msg_cannot_authorize

				#Till Close
				elif TypeModule == 'TC':

					Module 		= TypeModule
					UnBalance 	= TellerParam.UnBalance
					AccObj 		= MKT_ACCOUNT.query

					for row in DetailObj:

						TillAccount = str(row.TillAccount)
						Amo   		= Decimal(row.Amount)
						FilterAcc 	= AccObj.get(TillAccount)

						if FilterAcc:
							
							Balance 		= Decimal(FilterAcc.Balance)
							TillAccount 	= FilterAcc.ID
							TillCategory	= FilterAcc.AccCategory
							TillCurrency 	= FilterAcc.Currency

						else:
							return False,msg_error+"Till Account not found."

						VaultObj   	= mktuser.getVaultInfo(TillCurrency,Inputter)
						if VaultObj:
							VaultAccount 	=	VaultObj.ID
							VaultCategory 	=	VaultObj.AccCategory
							VaultCurrency 	=	VaultObj.Currency
						else:
							return False,msg_error+"Vault Account not found."
						# check Balance and Amount (Shortage and Surplus)
						# Shortage
						if Amo < Balance:
							if UnBalance == "Y":

								DrAcc = VaultAccount
								DrCat = VaultCategory
								DrCur = VaultCurrency

								CrAcc = TillAccount
								CrCat = TillCategory
								CrCur = TillCurrency

								CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
								if CheckBalanceDr:
									return False,msg_error + CheckBalanceDr
								
								CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
								if CheckBalanceCr:
									return False,msg_error + CheckBalanceCr

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]

								#Post Shortage Accout - 
								ShortageAcc  = mktaccount.getShortageAccount(CrCur,Branch)
								if ShortageAcc:
									Amo = Balance - Amo
									DrAcc = ShortageAcc.ID
									DrCat = ShortageAcc.AccCategory
									DrCur = ShortageAcc.Currency
								else:
									return False,msg_error+"Shortage Account not found in currency %s."%CrCur

								CrAcc = TillAccount
								CrCat = TillCategory
								CrCur = TillCurrency

								CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
								if CheckBalanceDr:
									return False,msg_error + CheckBalanceDr
								
								CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
								if CheckBalanceCr:
									return False,msg_error + CheckBalanceCr

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]
							
							else:
								return False,msg_error+"Cannot close till with amount less than balance."

						# check Balance and Amount (Shortage and Surplus)
						# Surplus
						elif Amo > Balance:

							if UnBalance == "Y":
								

								DrAcc = TillAccount
								DrCat = TillCategory
								DrCur = TillCurrency
								
								# if Surplus need post first Surplus Accout + :
								Surplus  = mktaccount.getSurplusAccount(TillCurrency,Branch)
								SurplusAmount = Amo - Balance
								
								if Surplus:
									CrAcc = Surplus.ID 
									CrCat = Surplus.AccCategory 
									CrCur = Surplus.Currency
								else:
									return False,msg_error+"Surplus Account not found in currency %s."%DrCur

								CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(SurplusAmount), "Dr")
								if CheckBalanceDr:
									return False,msg_error + CheckBalanceDr
								
								CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(SurplusAmount), "Cr")
								if CheckBalanceCr:
									return False,msg_error + CheckBalanceCr

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,SurplusAmount,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]

								#Post close till 
								DrAcc = VaultAccount
								DrCat = VaultCategory
								DrCur = VaultCurrency

								CrAcc = TillAccount
								CrCat = TillCategory
								CrCur = TillCurrency

								CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
								if CheckBalanceDr:
									return False,msg_error + CheckBalanceDr
								
								CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
								if CheckBalanceCr:
									return False,msg_error + CheckBalanceCr

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]

							else:
								return False,msg_error+"Cannot close till with amount more than balance."
						else:

							DrAcc = VaultAccount
							DrCat = VaultCategory
							DrCur = VaultCurrency

							CrAcc = TillAccount
							CrCat = TillCategory
							CrCur = TillCurrency

							CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
							if CheckBalanceDr:
								return False,msg_error + CheckBalanceDr
							
							CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
							if CheckBalanceCr:
								return False,msg_error + CheckBalanceCr

							# Call function to commit add account by currency 
							CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
							if not CheckAccounting[0]:
								return False,CheckAccounting[1]

				# Successfully
				Message="The record was authorized successfully, record id:%s"%ID
				return True,Message
		else:
			Message = msg_warning+"Record not found..."
			return False,Message

	except Exception, e:
		db.session.rollback()
		return False,msg_error+' %s'%e

def setReverseMultiForm(AuthLevel,TypeModule,MianAUTH,MainINAU,DetailAUTH,DetailINAU,ID,Inputter='',Createdon=''):
	
	try:
		Message = ""
		TellerParam = mktparam.getTellerParam()
		MainObj 	= MianAUTH.query.get(ID)
		DetailObj 	= DetailAUTH.query.filter(DetailAUTH.ID==ID)
		LiveInputter = MainObj.Inputter
		if Inputter != LiveInputter:
			Message = msg_warning+"User %s cannot reverse record for user %s."%(Inputter,LiveInputter)
			return False,Message

		if AuthLevel == 1:

			mktaudit.moveAUTHtoINAU(DetailAUTH,DetailINAU,ID,Inputter,Createdon, 'INAU')
			mktaudit.moveAUTHtoINAU(MianAUTH,MainINAU,ID,Inputter,Createdon, 'INAU', '-1')
			Message = "The record was reversed successfully, record id:%sR in INAU."%ID
			return True,Message
		else:
			
			# print "Start reverse record seft Authorize"
			# Seft Authorize need to reverse
			MainObj 	= MianAUTH.query.get(ID)

			DetailObj 	= DetailAUTH.query.filter(DetailAUTH.ID==ID)
			Audit 		= mktaudit.getAuditrail()
			Authorizer 	= Audit['Authorizer']
			Authorizeon	= Audit['Authorizeon']
			Branch 		= Audit['Branch']

			# print MainObj
			# print 'ID :%s'%ID
			if MainObj:

				# Till Open
				if TypeModule == 'TO':

					Tran  		= TellerParam.RevTransaction
					TranDate 	= MainObj.TranDate
					Ref 		= "%sR"%MainObj.ID
					Note 		= MainObj.Note
					Mode 		= ""
					Module 		= TypeModule
					

					#Detail Table
					mktaudit.setReverseToLive(DetailAUTH,DetailINAU,ID,Inputter,Createdon,Authorizer,Authorizeon)

					#Main Table
					mktaudit.setReverseToLive(MianAUTH,MainINAU,ID,Inputter,Createdon,Authorizer,Authorizeon)

					for row in DetailObj:

						Amo 		= Decimal(row.Amount)
						Currency 	= row.Currency
						VaultObj 	= mktuser.getVaultInfo(Currency,LiveInputter)

						if VaultObj:
							DrAcc 	= VaultObj.ID
							DrCat	= VaultObj.AccCategory
							DrCur 	= VaultObj.Currency

						CrAcc 	=	row.TillAccount
						CrCur 	=	Currency
						CrCat 	=	row.Category

						CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
						if CheckBalanceDr:
							return False,msg_error + CheckBalanceDr
						
						CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
						if CheckBalanceCr:
							return False,msg_error + CheckBalanceCr

						# print "Added record to session "				
						# Call function to commit add account by currency 
						CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
						#Block validation
						if not CheckAccounting[0]:
							#Catch error
							db.session.rollback()
							return False,msg_error+CheckAccounting[1]

				if TypeModule == 'TC':

					UnBalance 	= TellerParam.UnBalance
					Tran  		= TellerParam.RevTransaction
					TranDate 	= MainObj.TranDate
					Ref 		= "%sR"%MainObj.ID
					Note 		= MainObj.Note
					Mode 		= ""
					Module 		= TypeModule
					

					#Detail Table
					mktaudit.setReverseToLive(DetailAUTH,DetailINAU,ID,Inputter,Createdon,Authorizer,Authorizeon)

					#Main Table
					mktaudit.setReverseToLive(MianAUTH,MainINAU,ID,Inputter,Createdon,Authorizer,Authorizeon)
					
					AccObj = MKT_ACCOUNT.query
					for row in DetailObj:

						TillAccount = row.TillAccount
						Amo 		= Decimal(row.Amount)

						FilterAcc = AccObj.get(TillAccount)
						if FilterAcc:

							Balance 		= Decimal(row.Balance)
							TillAccount 	= FilterAcc.ID
							TillCategory	= FilterAcc.AccCategory
							TillCurrency 	= FilterAcc.Currency

						VaultObj   	= mktuser.getVaultInfo(TillCurrency,LiveInputter)
						if VaultObj:
							VaultAccount 	=	VaultObj.ID
							VaultCategory 	=	VaultObj.AccCategory
							VaultCurrency 	=	VaultObj.Currency

						# check Balance and Amount (Shortage and Surplus)
						# Shortage
						if Amo < Balance:
							if UnBalance == "Y":

								DrAcc = TillAccount
								DrCat = TillCategory
								DrCur = TillCurrency

								CrAcc = VaultAccount
								CrCat = VaultCategory
								CrCur = VaultCurrency

								CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
								if CheckBalanceDr:
									return False,msg_error + CheckBalanceDr
								
								CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
								if CheckBalanceCr:
									return False,msg_error + CheckBalanceCr

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]

								#Post Shortage Accout - 
								ShortageAcc  = mktaccount.getShortageAccount(CrCur,Branch)
								if ShortageAcc:
									Amo = Balance - Amo
									CrAcc = ShortageAcc.ID
									CrCat = ShortageAcc.AccCategory
									CrCur = ShortageAcc.Currency
								else:
									return False,msg_error+"Shortage Account not found."

								DrAcc = TillAccount
								DrCat = TillCategory
								DrCur = TillCurrency
								

								CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
								if CheckBalanceDr:
									return False,msg_error + CheckBalanceDr
						
								CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
								if CheckBalanceCr:
									return False,msg_error + CheckBalanceCr

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]
							
							else:
								return False,msg_error+"Cannot close till with amount less than balance."

						# check Balance and Amount (Shortage and Surplus)
						# Surplus
						elif Amo > Balance:

							if UnBalance == "Y":

								DrAcc = TillAccount
								DrCat = TillCategory
								DrCur = TillCurrency

								CrAcc = VaultAccount
								CrCat = VaultCategory
								CrCur = VaultCurrency

								CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
								if CheckBalanceDr:
									return False,msg_error + CheckBalanceDr
								
								CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
								if CheckBalanceCr:
									return False,msg_error + CheckBalanceCr

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]
									
								#Post Surplus Accout + :
								Surplus  = mktaccount.getSurplusAccount(CrCur,Branch)
								Amo = Amo - Balance

								DrAcc = Surplus.ID 
								DrCat = Surplus.AccCategory 
								DrCur = Surplus.Currency

								if Surplus:
									CrAcc = TillAccount
									CrCat = TillCategory
									CrCur = TillCurrency
								else:
									return False,msg_error+"Surplus Account not found."

								CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
								if CheckBalanceDr:
									return False,msg_error + CheckBalanceDr
								
								CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
								if CheckBalanceCr:
									return False,msg_error + CheckBalanceCr

								# Call function to commit add account by currency 
								CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
								if not CheckAccounting[0]:
									return False,CheckAccounting[1]

							else:
								return False,msg_error+"Cannot close till with amount more than balance."
						else:
							DrAcc = TillAccount
							DrCat = TillCategory
							DrCur = TillCurrency

							CrAcc = VaultAccount
							CrCat = VaultCategory
							CrCur = VaultCurrency

							CheckBalanceDr 	= 	mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amo), "Dr")
							if CheckBalanceDr:
								return False,msg_error + CheckBalanceDr
							
							CheckBalanceCr 	= 	mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amo), "Cr")
							if CheckBalanceCr:
								return False,msg_error + CheckBalanceCr

							# Call function to commit add account by currency 
							CheckAccounting = setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
							if not CheckAccounting[0]:
								return False,CheckAccounting[1]
							

			else:
				db.session.rollback()
				Message = msg_warning+"Record not found..."
				return False,Message

			Message = "The record was reversed successfully, record id:%sR"%ID
			return True,Message

	except Exception, e:
		db.session.rollback()
		return False,msg_error+" %s"%e

	
def setCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode=""):
	try:
		
		Message = ""
		CheckAccounting = mktaccounting.getValidationAccounting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amo,Module,Tran,TranDate,Ref,Note,Mode)
		
		if CheckAccounting[0]:
			for i in range(0, 2):
			
				if i == 0:
					Account 	= DrAcc
					Category	= DrCat
					Currency 	= DrCur
					DrCr 		= "Dr"
				else:
					Account		= CrAcc
					Category	= CrCat
					Currency 	= CrCur
					DrCr 		= "Cr"


				#get Gl key	
				GL_KEYS 		= mktaccounting.getConsolKey(Category,Currency,"","")
					
				ObjAccounting 	= mktaccounting.postAccounting(		

											"AUTH", 				# Status
											"0", 					# Curr
											Inputter,				# Inputter
											Createdon, 				# Createdon
											Authorizer,				# Authorizer
											Authorizeon,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Amo, 					# Amount
											Module,					# Module
											Tran, 					# Transaction
											TranDate, 				# TransactionDate
											Ref, 					# Reference
											Note, 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# ConsolKey
											Mode,					# Mode
											"YES")					# Tell function to update LastTransaction Customer's Account

			#Check after post accounting
			CheckPointObj = mktaccounting.setAccoutningCheckPoint(Module,Ref)
			if not CheckPointObj[0]:
				db.session.rollback()
				return False,CheckPointObj[1]
			#Pass
			return True,""
		else:
			db.session.rollback()
			return False,CheckAccounting[1]

	except Exception, e:
		db.session.rollback()
		return False," %s"%e


