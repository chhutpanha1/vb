'''
Created Date: 23 Sep 2015
Author: Sovankiry Rim

Modified by: Sovankiry
Modified Date: 01 Oct 2015
All Right Reserved Morakot Technology
Description : mktloancollection refer to generate collection sheet.

'''

from app.mktcore.imports 	import *
from decimal 				import *



from sqlalchemy import *
import app.mktcore.autoid 		as mktautoid
import mktdate 					as mktdate
import user 					as mktuser
import mktsetting 				as mktsetting
import mktparam 				as mktparam
import mktdb 					as mktdb
import mktaudit 				as mktaudit
import mktaccounting 			as mktaccounting
import mktmoney 				as mktmoney
import mktsetting 				as mktsetting
import mktteller 				as mktteller

def getLoanCollectionSheet(Officer,Currency,CollectionDate,ContractVB="",Group="",ListExcludeRepID=[]):

	Condition =[]
	CollectionDate 	= CollectionDate.strip()
	CollectionRange = CollectionDate.split()
	CurrentBranch 	= mktuser.getCurrentBranch()
	if len(CollectionRange)==2:
		ColLeft, ColRight = CollectionRange[0],CollectionRange[1]
		Condition.append(MKT_REP_SCHEDULE.CollectionDate.between(ColLeft, ColRight) )
	else:
		Condition.append(MKT_REP_SCHEDULE.CollectionDate==CollectionDate)

	if ContractVB:
		Condition.append(MKT_LOAN_CONTRACT.ContractVB==ContractVB)

	if Group:
		Condition.append(MKT_LOAN_CONTRACT.Group==Group)
	if ListExcludeRepID:
		Condition.append(~MKT_REP_SCHEDULE.ID.in_(ListExcludeRepID))

	Condition.append(MKT_LOAN_CONTRACT.ContractOfficerID==Officer)
	Condition.append(MKT_LOAN_CONTRACT.OutstandingAmount > 0)
	Condition.append(MKT_LOAN_CONTRACT.DisbursedStat=="Y")
	Condition.append(MKT_LOAN_CONTRACT.Branch==CurrentBranch)
	Condition.append(MKT_LOAN_CONTRACT.Currency==Currency)
	

	ScheduleObj = db.session.query(	MKT_REP_SCHEDULE.ID,
									MKT_REP_SCHEDULE.CollectionDate,
									MKT_REP_SCHEDULE.Principal,
									MKT_REP_SCHEDULE.Interest,
									MKT_REP_SCHEDULE.Charge,
									MKT_REP_SCHEDULE.LoanID,
									MKT_LOAN_CONTRACT.ContractCustomerID,
									MKT_LOAN_CONTRACT.Account,
									MKT_LOAN_CONTRACT.ContractVB,
									MKT_LOAN_CONTRACT.Group,
									MKT_LOAN_CONTRACT.Currency,
									MKT_CUSTOMER.FirstNameEn,
									MKT_CUSTOMER.LastNameEn,
									MKT_VILLAGE.Description.label('Village')).\
								filter(*Condition).\
								join(MKT_LOAN_CONTRACT,MKT_LOAN_CONTRACT.ID==MKT_REP_SCHEDULE.LoanID).\
								join(MKT_CUSTOMER,MKT_CUSTOMER.ID==MKT_LOAN_CONTRACT.ContractCustomerID).\
								join(MKT_VILLAGE,MKT_VILLAGE.ID==MKT_CUSTOMER.Village).\
								order_by(MKT_REP_SCHEDULE.CollectionDate.asc(),MKT_LOAN_CONTRACT.ID.asc())

	return ScheduleObj

def getListAuthLoanCollection(Option):
	CurrentBranch = mktuser.getCurrentBranch()
	Transaction = mktsetting.getAccSetting().LoanCollectionTran if mktsetting.getAccSetting() else ""
	if Option == "ListLive":
		FundTable= MKT_FUND_TRANSFER
	elif Option == "ListAuth":
		FundTable= MKT_FUND_TRANSFER_INAU
	elif Option == "ListHist":
		FundTable= MKT_FUND_TRANSFER_HIST

	QueryObj 	= db.session.query(	FundTable.Reference.label('ID'),
									FundTable.DrAccount.label('OfficerAccount'),
									FundTable.DrCategory.label('Category'),
									FundTable.DrCurrency.label('Currency'),
									FundTable.Transaction,
									FundTable.TranDate,
									func.sum(FundTable.Amount).label('TotalCollection')
									).\
							filter(FundTable.Reference.like('%'+"CO"+'%')).\
							filter(FundTable.Transaction==Transaction).\
							filter(FundTable.Branch == CurrentBranch).\
							distinct(FundTable.Reference).\
							group_by(
									FundTable.Reference,
									FundTable.DrAccount,
									FundTable.DrCategory,
									FundTable.DrCurrency,
									FundTable.Transaction,
									FundTable.TranDate)
	return QueryObj

def getSearchLoanCollection(CollectionID,Action):

	kwargs 				= {}
	ListRepID 			= []
	AuditObj 			= []
	Customer 			= ""
	Account 			= ""
	LoanID 				= ""
	RepID 				= ""
	CollectionDate 		= ""
	OfficerID 			= ""
	OfficerAccount 		= ""
	OfficerCategory 	= ""
	Currency 			= ""
	Village 			= ""
	
	Amount 				= 0
	Principal 			= 0
	Interest 			= 0
	Charge 				= 0
	TotalActualAmount 	= 0
	TotalCollection 	= 0
	CountRow 			= 0
	FundObj 			= MKT_FUND_TRANSFER.query.filter(MKT_FUND_TRANSFER.Reference==CollectionID)
	FundINAUObj 		= MKT_FUND_TRANSFER_INAU.query.filter(MKT_FUND_TRANSFER_INAU.Reference==CollectionID)
	FundHistObj 		= MKT_FUND_TRANSFER_HIST.query.filter(MKT_FUND_TRANSFER_HIST.Reference==CollectionID)

	if FundINAUObj.all():
		QueryObj = FundINAUObj
	else:
		QueryObj = FundObj

		if not QueryObj.all():
			QueryObj =FundHistObj

	RepaymentObj 	= MKT_REP_SCHEDULE.query
	LoanObj 		= MKT_LOAN_CONTRACT.query
	CustomerObj 	= MKT_CUSTOMER.query
	VillageObj		= MKT_VILLAGE.query

	for row in QueryObj:
		CountRow+=1
		Reference 			= str(row.ID)
		RepID 				= str(row.Note)
		OfficerAccount		= row.DrAccount
		OfficerCategory		= row.DrCategory
		Currency 			= row.DrCurrency
		Account 			= row.CrAccount
		Amount 				= float(row.Amount)
		TotalActualAmount 	+= Amount
		FilterScheduleObj 	= RepaymentObj.get(RepID)
		ListRepID.append(RepID)
		if FilterScheduleObj:
			Principal 			= float(FilterScheduleObj.Principal)
			Interest 			= float(FilterScheduleObj.Interest)
			Charge 				= float(FilterScheduleObj.Charge)
			TotalAmount 		= Principal+Interest+Charge
			TotalCollection+=TotalAmount
			LoanID 				= FilterScheduleObj.LoanID
			CollectionDate 		= FilterScheduleObj.CollectionDate
			FilterLoanObj 		= LoanObj.get(LoanID)
			
			if FilterLoanObj:
				OfficerID 			= FilterLoanObj.ContractOfficerID	
				CustomerID 			= FilterLoanObj.ContractCustomerID
				FilterCustomerObj 	= CustomerObj.get(CustomerID)
				if FilterCustomerObj:
					FilterVillage  = VillageObj.get(FilterCustomerObj.Village)
					if FilterVillage:
						Village=FilterVillage.Description
					Customer = "%s - %s %s"%(FilterCustomerObj.ID,FilterCustomerObj.LastNameEn,FilterCustomerObj.FirstNameEn)
		kwargs.update({  
				str('LoanID_%s'%RepID) 			:LoanID,
				str('Customer_%s'%RepID)		:Customer,
				str('Village_%s'%RepID)			:Village,
				str('Account_%s'%RepID) 		:Account,
				str('CollectionDate_%s'%RepID) 	:CollectionDate,
				str('Principal_%s'%RepID) 		:mktmoney.toMoney(Principal,mktmoney.getCurrencyObj(Currency)),
				str('Interest_%s'%RepID) 		:mktmoney.toMoney(Interest,mktmoney.getCurrencyObj(Currency)),
				str('Charge_%s'%RepID) 			:mktmoney.toMoney(Charge,mktmoney.getCurrencyObj(Currency)),
				str('TotalAmount_%s'%RepID) 	:mktmoney.toMoney(TotalAmount,mktmoney.getCurrencyObj(Currency)),
				str('Amount_%s'%RepID) 			:mktmoney.toMoney(Amount,mktmoney.getCurrencyObj(Currency)),
				str('Reference_%s'%RepID) 		:Reference
				})
		AuditObj= {'Status':row.Status,
					'Curr':row.Curr,
					'Inputter':row.Inputter,
					'Createdon':row.Createdon,
					'Authorizer':row.Authorizer,
					'Authorizeon':row.Authorizeon,
					'Branch':row.Branch

					}
	kwargs.update({
					'Officer':getOfficerByAccount(OfficerAccount) if getOfficerByAccount(OfficerAccount) else OfficerID,
					'OfficerAccount':OfficerAccount,
					'OfficerCategory':OfficerCategory,
					'Currency':Currency,
					'CountRow':mktmoney.formatNumber(CountRow,1,0),
					'TotalCollection':mktmoney.toMoney(TotalCollection,mktmoney.getCurrencyObj(Currency)),
					'TotalActualAmount':mktmoney.toMoney(TotalActualAmount,mktmoney.getCurrencyObj(Currency))
					})

	return {'ListRepID':ListRepID,'kwargs':kwargs,'AuditObj':AuditObj}

def getOfficerByAccount(OfficerAccount):
	CashID = ""
	CashObj = MKT_CASH_ACCOUNT.query
	for row in CashObj:
		ListAccount = row.Account.split()
		if OfficerAccount in ListAccount:
			CashID = str(row.ID)
	UserObj = MKT_USER.query.filter(MKT_USER.CashAccount==CashID).first()
	if UserObj:
		OfficerObj = MKT_OFFICER.query.filter(MKT_OFFICER.UserID==UserObj.ID).first()
		if OfficerObj:
			return 	OfficerObj.ID
	return ""
def insertLoanCollection(AuthLevel,CollectionID,kwargs,ListRepID):
	try:
		Message 			= ""
		Transaction  		= mktsetting.getAccSetting().LoanCollectionTran if mktsetting.getAccSetting() else ""
		TranDate 			= str(mktdate.getBankDate())
		Audit 				= mktaudit.getAuditrail()
		Inputter 			= Audit['Inputter']
		Createdon 			= Audit['Createdon']
		Authorizer 			= Audit['Authorizer']
		Authorizeon			= Audit['Authorizeon']
		Branch 				= Audit['Branch']
		Module 				= "FD"
		Note 				= "Loan Collection"
		AccObj 				= MKT_ACCOUNT.query
		# FundObj 			= MKT_FUND_TRANSFER.query.filter(MKT_FUND_TRANSFER.Reference==CollectionID)
		FundINAUObj 		= MKT_FUND_TRANSFER_INAU.query.filter(MKT_FUND_TRANSFER_INAU.Reference==CollectionID)
		if FundINAUObj:
			mktaudit.deleteAUTH(MKT_FUND_TRANSFER_INAU, CollectionID,'Reference')

		if AuthLevel==1:
			OfficerAccount 	= kwargs['OfficerAccount']
			OfficerCategory = kwargs['OfficerCategory']
			Currency 		= kwargs['Currency']
			for RepID in ListRepID:
				#Function getAutoID has db.session.commit so we need to get first before submit
				Reference 		= mktautoid.getAutoID('FRM_FUND_DEPOSIT')
				LoanID 			= kwargs['LoanID_%s'%RepID]
				Account 		= kwargs['Account_%s'%RepID]
				Amount 			= kwargs['Amount_%s'%RepID]
				Amount 			= float(Amount.replace(',',''))

				if Amount > 0 :
					FilterAcc  		= AccObj.get(Account)
					if FilterAcc:
						# Debit Co Account 
						DrAcc 			= OfficerAccount
						DrCat 			= OfficerCategory
						DrCur 			= Currency

						# Credit Client Account
						CrAcc 			= FilterAcc.ID
						CrCat 			= FilterAcc.AccCategory
						CrCur 			= Currency

						# Validation Max Min Balance
						CheckBalanceDrAcc = mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(Amount), "Dr")
						if CheckBalanceDrAcc:
							return False,CheckBalanceDrAcc

						CheckBalanceCrAcc = mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(Amount), "Cr")
						if CheckBalanceCrAcc:								
							return False,CheckBalanceCrAcc

						# Insert record to Fund Transfer
						InsertRecord = {	
											'Status'		:"INAU",
											'Authorizer'	:"",
											'Authorizeon'	:"",
											'ID'			:Reference,
											'DrAccount'		:DrAcc,
											'DrCategory'	:DrCat,
											'DrCurrency'	:DrCur,
											'CrAccount'		:CrAcc,
											'CrCategory'	:CrCat,
											'CrCurrency' 	:CrCur,
											'Amount'		:Amount,
											'Transaction'	:Transaction,
											'TranDate'		:TranDate,
											'Reference'		:CollectionID,
											'Note'			:RepID
										}
						mktdb.insertTable(MKT_FUND_TRANSFER_INAU,InsertRecord)

					else:
						Message = "Account#%s not found."%Account
						return False,Message
				else:
					Message = "Loan#%s amount must be more than zero."%LoanID
					return False,Message
		flash("Record was added successfully, record id: %s in INAU"%CollectionID)
		return True,Message
	except Exception, e:
		db.session.rollback()
		return False," %s"%e

def setAuthorizeLoanCollection(AuthLevel,CollectionID):
	try:
		Message 			= ""
		Account 			= ""
		LoanID 				= ""
		CollectionDate 		= ""
		Amount 				= 0
		Principal 			= 0
		Interest 			= 0
		Charge 				= 0
		TotalActualAmount 	= 0
		TotalCollection 	= 0
		Audit 				= mktaudit.getAuditrail()
		Authorizer 			= Audit['Authorizer']
		Authorizeon			= Audit['Authorizeon']
		Module 				= "FD"
		CurrentBranch 		= str(mktuser.getCurrentBranch())
		if AuthLevel == 1:

			FundINAUObj 	= MKT_FUND_TRANSFER_INAU.query.filter(MKT_FUND_TRANSFER_INAU.Reference==CollectionID)
			if not FundINAUObj.all():
				return False,msg_warning+"Record not found..."

			for row in FundINAUObj:
				Inputter 	= row.Inputter
				Createdon 	= row.Createdon
				Branch 		= str(row.Branch)
				Transaction = row.Transaction
				TranDate 	= row.TranDate
				Note 		= row.Note

				if Inputter != Authorizer:
					if Branch == CurrentBranch:
						ID 			= str(row.ID)
						Amount 		= row.Amount if row.Amount else 0
						DrAccount 	= row.DrAccount
						DrCategory 	= row.DrCategory
						DrCurrency 	= row.DrCurrency

						CheckBalance1 = mktaccounting.checkMaxMinBalance(DrAccount, DrCurrency, Decimal(Amount), "Dr")
						if CheckBalance1:
							return False,msg_error+CheckBalance1

						CrAccount 	=	row.CrAccount
						CrCategory 	= 	row.CrCategory
						CrCurrency 	=	row.CrCurrency

						CheckBalance2 = mktaccounting.checkMaxMinBalance(CrAccount, CrCurrency, Decimal(Amount), "Cr")
						if CheckBalance2:
							return False,msg_error+CheckBalance2

						CheckAccounting = mktteller.setAuthorize("FD",MKT_FUND_TRANSFER,MKT_FUND_TRANSFER_INAU,ID)# mktaccounting.getValidationAccounting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAccount,DrCategory,DrCurrency,CrAccount,CrCategory,CrCurrency,Amount,Module,Transaction,TranDate,ID,Note)
						if not CheckAccounting[0]:
							return False,CheckAccounting[1]

						
						mktaudit.moveINAUtoAUTH(MKT_FUND_TRANSFER, MKT_FUND_TRANSFER_INAU, ID, Inputter, Createdon, Authorizer, Authorizeon)
						mktaudit.deleteAUTH(MKT_FUND_TRANSFER_INAU, ID)
					else:
						return False,msg_error+" Not allow authorize different branch."
				else:
					return False,msg_cannot_authorize

		Message = "%s %s."%(CollectionID,msg_authorize_0)		
		return True,Message
	except Exception, e:
		return False,msg_error+" %s"%e

def setDeleteLoanCollection(CollectionID):
	try:
		QueryObj = MKT_FUND_TRANSFER_INAU.query.filter(MKT_FUND_TRANSFER_INAU.Reference==CollectionID).all()
		if QueryObj:
			for row in QueryObj:
				Status = row.Status
				if Status == "INAU":
					mktdb.deleteRecord(MKT_FUND_TRANSFER_INAU,[MKT_FUND_TRANSFER_INAU.Reference==CollectionID])
				else:
					return False,msg_warning+"The record was authorized and could not be deleted."
			
			return True,"%s %s"%(CollectionID,msg_delete_0)
		else:
			return False," %s"%msg_not_found_0
	except Exception, e:
		return False,msg_error+" %s"%e