'''
Created Date: 01 Apr 2015
Author: Sovankiry Rim

Modified by: 
Modified Date: 
All Right Reserved Morakot Technology
Description : mktdb refer to function for insert,update,delete in table 
Notes : Don't use db.session.commit() in function.
'''

from app.mktcore.wtfimports import *
from .. import app, db
import mktaudit as mktaudit
from app.urlregister 	import *

def insertTable(TableName,kwargs):

	try:
		Dic = {}
		Dic.update(mktaudit.getAuditrail())
		Dic.update(kwargs)
		ObjInsert 		= TableName(**Dic)
		db.session.add(ObjInsert)
	except:
		db.session.rollback()
		raise

# it can update only one record per call
# when session.commit() it will be update
# how to use:
# mktdb.updateRecord(MKT_ACCOUNT,
# 					[MKT_ACCOUNT.ID==VaultAcc.ID],
# 					{"Branch":Branch})
# arg = condition
# kwargs = field to update value
def updateRecord(TableName,arg,kwargs):

	try:
		Dic = {}
		# Dic.update(mktaudit.getAuditrail())
		Dic.update(kwargs)
		ObjUpdate 		= TableName.query.\
							filter(*arg).update(Dic)

	except:
		db.session.rollback()
		raise		
def deleteRecord(TableName,arg):

	try:
		ObjDelete 		= TableName.query.\
							filter(*arg).delete()
	except:
		db.session.rollback()
		raise