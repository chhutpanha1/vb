# -*- coding: utf-8 -*-

# Created by Kiry 
# Date: 29 January 2015
# Modify Date : 05 February 2015
# All Right Reserved Morakot Technology
# Description :
#			getDateNow()
#			getDatediff() calculator only 360/360

from flask 						import flash
from app.mktcore.imports 		import *
from .. 						import db
from app.mktcore.constant 		import *
from datetime 					import datetime, date, timedelta
import time
import calendar

import mktsetting 				as mktsetting
import loantools.nonworkingday 	as mktDay
import mktholiday 				as mktHoliday

from app.Dates.models 			import MKT_DATES

class Struct(object):

	def __init__(self, **entries):
		self.__dict__.update(entries)

def getDatediff360(DateFrom,DateTo): #YearA must be  datetime.date(2015, 1, 1)

	YearFrom	= int(DateFrom.year)
	MonthFrom	= int(DateFrom.month)
	DayFrom		= int(DateFrom.day)

	YearTo		= int(DateTo.year)
	MonthTo		= int(DateTo.month)
	DayTo		= int(DateTo.day)

	if DateTo >= DateFrom:

		DayResult	=	DayTo - DayFrom
	else:
		DayResult 	= 	(DayTo + 30) - DayFrom 
		MonthTo-=1


	if MonthTo >= MonthFrom:

		MonthResult	=	MonthTo - MonthFrom
	else:
		MonthResult 	= 	(MonthTo + 12) - MonthFrom 
		YearTo-=1


	if YearTo >= YearFrom:

		YearResult	=	YearTo - YearFrom
		
	if DateFrom > DateTo:

		print msg_error+"DateTo %s must be greater then DateFrom %s." % (DateTo,DateFrom)
		flash(msg_error+"DateTo %s must be greater then DateFrom %s." % (DateTo,DateFrom))
		NumberOfDays 	= 0
	else:
		NumberOfDays=(YearResult*12*30)+(MonthResult*30)+DayResult

	return NumberOfDays

def isLeapYear(Date):
# Date must be is yyyy-mm-dd
    if Date:
        year    = int(Date.split("-")[0])
        Leap    = year % 4
        
        if Leap == 0: # year is leap 1-29
            return True
        else: #year is non leap 1-28
            return False
            
def formatDate(Date):
#Date must be is yyyy-mm-dd
#After formatting dd-mm-yyyy
	try:
		ddmmyyyy= Date.split("-")[2] + "-" + Date.split("-")[1] + "-" + Date.split("-")[0] 
		return ddmmyyyy
	except:
		raise

def getDateISO(Date=None):
	try:
		d = datetime.now() if Date==None else Date
		now = '%s-%s-%s'% (d.year, d.month, d.day)
		CurrentDate = datetime.strptime(str(now), "%Y-%m-%d").date()
		return CurrentDate

	except:
		raise

def isDate(Date=''):
#Date must be is yyyy-mm-dd
#if data return True else False
	try:
		if Date:
			Date = str(Date)
			if Date.find('-') != -1: # the value search is list

				# Date
				d=int(Date.split("-")[2])
				# Month
				m=int(Date.split("-")[1])
				# Year
				y=int(Date.split("-")[0])
				# Validate Date
				Leap 	= y % 4
				if Leap == 0: # year is leap 1-29
					if m == 2:
						if d > 29:
							return False
				else: #year is non leap 1-28
					if m == 2:
						if d > 28:
							return False
				if d > 31:
					return False
				elif d <=0 :
					return False

				# Validate Month
				if m > 12:
					return False
				elif m <= 0:
					return False
					
				# Validate Year
				if y <= 100:
					return False
				return True
			else:
				return False
		
	except:
		return False
		
def getHour():
	try:
		dat = datetime.now()
		Hour = '%s:%s:%s'% (dat.hour, dat.minute, dat.second)

		return Hour
	except:
		raise

def getDateTimeNow():

	CurrentDateTime = str(time.strftime('%Y-%m-%d %H:%M:%S'))

	return CurrentDateTime

def getAge(DOB=None):
	CurrentDate = datetime.today()
	DateOfBirth = datetime.strptime(str(DOB),'%Y-%m-%d')

	Age = CurrentDate - DateOfBirth
	Age = Age.days / 365

	return Age

def toDateEngLong(Date):
	FormatDate = datetime.strptime(str(Date),'%Y-%m-%d').date()
	DateEng = FormatDate.strftime("%A %d-%B-%Y")

	return DateEng

def toDateShort(Date,Locale='EN'):
	KhmerDate=[u'ច័ន្ទ',u'អង្គារ',u'ពុធ',u'ព្រហ',u'សុក្រ',u'សៅរ៍',u'អាទិត្យ']
	KhmerMonth=['',u'មករា',u'កុម្ភៈ',u'មិនា',u'មេសា',u'ឧសភា',u'មិថុនា',u'កក្កដា',u'សីហា',u'កញ្ញា',u'តុលា',u'វិចិ្ឆកា',u'ធ្នូ']
	MyDate = datetime.strptime(str(Date),'%Y-%m-%d').date()

	if Locale=='EN': #default EN

		result = MyDate.strftime('%a %d-%b-%Y')

	elif Locale=='KH':

		KH 	= KhmerDate[MyDate.weekday()]
		d 	= MyDate.strftime('%d')
		m 	= KhmerMonth[int(MyDate.strftime('%m'))]
		y   = MyDate.strftime('%Y')

		KH  = u'{:<{}s}'.format(KH, 10)

		result=u'%s, %s %s %s'%(KH,d,m,y)

	return result

def getFirstDay(dt, d_years=0, d_months=0):

	y, m = dt.year + d_years, dt.month + d_months
	a, m = divmod(m-1, 12)

	return date(y+a, m+1, 1)


def getLastDay(dt,y='',m=''):
    return getFirstDay(dt, 0, 1) + timedelta(-1)

def getNextMonth(dt,NumberOfNext=1):
 
    mm = dt.month - 1 + NumberOfNext #Frequency is number of next month
    yy = dt.year + mm / 12
    mm = mm % 12 + 1
    dd = min(dt.day,calendar.monthrange(yy,mm)[1])
    NextMonth = date(yy,mm,dd)
    
    return NextMonth

def getLastDayPrevMonth(dt):
	return dt.replace(day=1) - timedelta(days=1)

# Example :
	# DateNow = mktdate.date.today()	
	# FirstDayCurMonth = mktdate.getDateISO(mktdate.getFirstDay(DateNow))		
	# LastDayPrevMonth = mktdate.getDateISO(mktdate.getLastDayPrevMonth(DateNow))
	# FirstDayPrevMonth = mktdate.getDateISO(mktdate.getFirstDay(LastDayPrevMonth))	

def getJulianDay(Date):

	CurrentDate = datetime.strptime(str(Date),'%Y-%m-%d').date()
	D = CurrentDate.timetuple()
	JulianDay = D.tm_yday
	Year = str(CurrentDate.year)[-2:]
	strFormat = '{:0>3d}'
	JulianDay = strFormat.format(JulianDay)
	JulianDay = Year + str(JulianDay)

	return JulianDay

def getBankDate():
	try:
		from app.Dates.models import MKT_DATES

		BankDate = MKT_DATES.query.get("SYSTEM")
		if BankDate:
			CurrentDate = BankDate.SystemDate
		else:
			BankDate = {'SystemDate':str(getDateISO())}
			CurrentDate = Struct(**BankDate)

		CurrentDate = datetime.strptime(str(CurrentDate), "%Y-%m-%d").date()
		
		return CurrentDate

	except:
		db.session.rollback()
		raise

def getBankDateObj():
	try:
		from app.Dates.models import MKT_DATES

		BankDate = MKT_DATES.query.get("SYSTEM")
		return BankDate

	except:
		db.session.rollback()
		raise

def getNumOfNonWorkingDay():
	try:

		obj = getBankDateObj()

		SystemDate = datetime.strptime(str(obj.SystemDate),'%Y-%m-%d')
		LastSystemDate = datetime.strptime(str(obj.LastSystemDate),'%Y-%m-%d')

		NumOfDay = SystemDate - LastSystemDate
		NumOfDay = NumOfDay.days

		return NumOfDay

	except:
		raise

def getDateDiff(From, To):
	try:
		DateFrom 	= 	datetime.strptime(str(From),'%Y-%m-%d')
		DateTo 		= 	datetime.strptime(str(To),'%Y-%m-%d')

		NumOfDay 	=	DateTo - DateFrom

		return NumOfDay.days

	except:
		raise

def getNextWeekEndDay(StartDate, NumberOfNextWeek, Day, DayEndOfMonth, Holiday):
	try:
		
		ConvertToDate 	= datetime.strptime(str(StartDate),'%Y-%m-%d').date() #Convert string to like this datetime.date(2015, 1, 1)
		Weekend 		= timedelta(days = 7) * NumberOfNextWeek 
		NextWeek 		= ConvertToDate + Weekend
		
		return NextWeek

	except:
		raise

def getCurrentMonthEnd(SystemDate):
	try:

		CurrentMonthEnd = 	""
		Setting 		= 	mktsetting.getBankDateObj()
		CurrentDate 	= 	datetime.strptime(str(SystemDate),'%Y-%m-%d').date()
		Year 			= 	CurrentDate.year
		Month 			=	CurrentDate.month
		DayEndOfMonth 	= 	mktsetting.getAccSetting().MONTH_END.strip()

		if DayEndOfMonth.upper() == "END" or DayEndOfMonth.upper() == "":
			NewDate = getLastDay(CurrentDate)
		else:
			NewDate = Setting.NextMonthEnd
		
		NewDate = datetime.strptime(str(NewDate),'%Y-%m-%d').date()
		Day 	= NewDate.day
		Day 	= "%02d" %int(Day)
		Month 	= "%02d" %int(Month)
		CurrentMonthEnd = "%s-%s-%s" %(Year, Month, Day)

		return CurrentMonthEnd

	except Exception, e:
		raise

def getPeriodic(Frequency):
	try:

		Periodic 		= 	""
		BankDateObj 	=	mktsetting.getBankDateObj()
		TreyMeasValue 	=	{'01':['01', '02', '03'], '02':['04', '05', '06'], '03':['07', '08', '09'], '04':['10', '11', '12']}
		TreyMeasObj 	=	['01', '02', '03', '04']

		BankDate 	= 	getBankDate()
		Year 		= 	BankDate.year
		Month 		= 	BankDate.month
		Month 		= 	'%02d' %Month

		if int(Frequency) == 1:

			Periodic = BankDateObj.NextWeekend

		elif int(Frequency) == 2:

			Periodic = BankDateObj.NextMonthEnd

		elif int(Frequency) == 3:

			Quarterly = ''
			for item in TreyMeasObj:
				if Month in TreyMeasValue[item]:
					Quarterly = item
					break

			if Quarterly == "01":
				CurrentDate = "%s-%s-01" %(Year, "03")
				

			elif Quarterly == "02":
				CurrentDate = "%s-%s-01" %(Year, "06")

			elif Quarterly == "03":
				CurrentDate = "%s-%s-01" %(Year, "09")

			else:
				CurrentDate = "%s-%s-01" %(Year, "12")

			CurrentDate = 	datetime.strptime(str(CurrentDate),'%Y-%m-%d').date()
			QuaDate 	= 	getCurrentMonthEnd(CurrentDate)
			Periodic 	= 	QuaDate

		else:
			Periodic = BankDateObj.NextYearEnd

		return Periodic

	except Exception, e:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def getNextMonthDate(CurrentEndMonth, LastSystemDate):
	try:

		Holiday 		= mktHoliday.getHoliday()
		ConvertDate 	= CurrentEndMonth.split("-")
		NextMonthEnd 	= CurrentEndMonth

		EndYear 		= ConvertDate[0]
		EndMonth 		= ConvertDate[1]
		EndDay 			= ConvertDate[2]

		ConvertDate 	= LastSystemDate.split("-")
		CurDay 			= ConvertDate[2]
		one_day 		= timedelta(days=1) # one day

		if int(CurDay) == int(EndDay):
			NextMonthEnd 	= getNextMonth(date(int(EndYear), int(EndMonth), int(EndDay)))
			DayEndOfMonth 	= mktsetting.getAccSetting().MONTH_END.strip()
			if DayEndOfMonth.upper() == "END" or DayEndOfMonth.upper() == "":
				NextMonthEnd = getLastDay(NextMonthEnd)

			NextMonthEnd 	= datetime.strptime(str(NextMonthEnd),'%Y-%m-%d').date()
			# check 			= True
			# while check:
			# 	check = mktDay.isNonWorkingDay(NextMonthEnd, Holiday)
			# 	if check:
			# 		NextMonthEnd = NextMonthEnd - one_day

		return NextMonthEnd

	except:
		raise

def getNextYearDay(LastSystemDate, NextYearEnd, Holiday):
	try:

		NewYear = NextYearEnd
		Year1 = str(LastSystemDate).replace("-", "")
		Year2 = str(NextYearEnd).replace("-", "")
		one_day = timedelta(days=1) # one day

		if int(Year1) == int(Year2):
			CurYear = str(NextYearEnd).split("-")[0]
			CurYear = int(CurYear) + 1
			NewYear = str(CurYear)+ "-" + str(NextYearEnd).split("-")[1] + "-" + str(NextYearEnd).split("-")[2]

		NewYear = datetime.strptime(str(NewYear),'%Y-%m-%d').date()

		check = True
		while check:
			check = mktDay.isNonWorkingDay(NewYear, Holiday)
			if check:
				NewYear = NewYear - one_day


		return NewYear

	except:
		raise

def getDayEndOfMonth(LastSystemDate):

	DayEndOfMonth = mktsetting.getAccSetting().MONTH_END.strip()
	if DayEndOfMonth.upper() == "END" or DayEndOfMonth.upper() == "":
		# Current Month End
		CurrentDate = datetime.strptime(str(LastSystemDate),'%Y-%m-%d').date()
		
		D 			= str(CurrentDate).split("-")
		Year 		= D[0]
		Month 		= D[1]
		CurrentDay 	= D[2]
		# Next Month
		CurrentDate 	= getNextMonth(date(int(Year), int(Month), int(CurrentDay)))

		DateEndOfMonth 	= getLastDay(CurrentDate)
		Day 			= str(DateEndOfMonth).split("-")
		DayEndOfMonth 	= int(Day[2])

	return DayEndOfMonth

def setBankDate():
	try:
		
		Holiday = mktHoliday.getHoliday()

		BankDate = MKT_DATES.query.get("SYSTEM")
		if BankDate:
			one_day 		= timedelta(days=1) # one day
			LastSystemDate 	= BankDate.SystemDate
			SystemDate 		= BankDate.NextSystemDate
			NextSystemDate 	= datetime.strptime(str(BankDate.NextSystemDate),'%Y-%m-%d').date() + one_day #datetime.date(2015, 1, 1)
			NextMonthEnd 	= BankDate.NextMonthEnd

			NextWeekend 	= BankDate.NextWeekend
			NextYearEnd 	= BankDate.NextYearEnd

			LastDay = timedelta(days=30)
			Julian  = getJulianDay(SystemDate)

			BankDate.LastSystemDate 	=	LastSystemDate
			BankDate.SystemDate 		= 	SystemDate
			BankDate.NextSystemDate 	= 	NextSystemDate
			BankDate.JulianDate			=	Julian

			CurrentEndMonth = BankDate.NextMonthEnd

			DayEndOfMonth = getDayEndOfMonth(LastSystemDate)

			D 	= str(LastSystemDate).split("-")
			Day = D[2]

			E 		= str(NextWeekend).split("-")
			DayEnd 	= E[2]

			if int(Day) == int(DayEnd):
				NextWeekend = getNextWeekEndDay(NextWeekend, 1, Day, DayEndOfMonth, Holiday)

			NextMonthEnd 	= getNextMonthDate(CurrentEndMonth, LastSystemDate)
			NextYearEnd 	= getNextYearDay(LastSystemDate, NextYearEnd, Holiday)

			BankDate.NextWeekend 	=	NextWeekend
			BankDate.NextMonthEnd 	=	NextMonthEnd
			BankDate.NextYearEnd 	=	NextYearEnd
			# Commit data update
			db.session.add(BankDate)
			db.session.commit()
			# Output message to screen
			print "Current System Date is 	%s." %SystemDate
			print "Next Weekend is 	%s." %NextWeekend
			print "Next Month End is 	%s." %NextMonthEnd
			print "Next Year End is 	%s." %NextYearEnd

		else:
			print "System date not found."
		
		return ""

	except:
		db.session.rollback()
		raise

def getWeekendDay(CurrentDate):
	try:

		Holiday = mktHoliday.getHoliday()
		one_day = timedelta(days=1) # one day

		check = True
		# check Next Date with Holiday and Weekend
		while check:
			check = mktDay.isNonWorkingDay(CurrentDate, Holiday)
			if check:
				CurrentDate = CurrentDate - one_day

		return int(Holiday)

	except:
		db.session.rollback()
		raise