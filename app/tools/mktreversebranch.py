from flask 							import flash, session
from app.mktcore.imports 			import *
from .. 							import app, db
from sqlalchemy 					import *
from decimal 						import *
from datetime 						import datetime, date, timedelta
import time
import calendar



def getReverseBranch(Branch):
	try:
		
		# Delete Customer
		db.session.query(MTK_CUSTOMER).filter(MTK_CUSTOMER.Branch == Branch).delete()
		db.session.query(MTK_CUSTOMER_INAU).filter(MTK_CUSTOMER_INAU.Branch == Branch).delete()
		db.session.query(MTK_CUSTOMER_HIST).filter(MTK_CUSTOMER_HIST.Branch == Branch).delete()
		print "Customer is deletting ..."

		# Delete Account
		db.session.query(MTK_ACCOUNT).filter(MTK_ACCOUNT.Branch == Branch).delete()
		db.session.query(MTK_ACCOUNT_INAU).filter(MTK_ACCOUNT_INAU.Branch == Branch).delete()
		db.session.query(MTK_ACCOUNT_HIST).filter(MTK_ACCOUNT_HIST.Branch == Branch).delete()
		print "Account is deletting ..."

		# Delete Account Entry
		db.session.query(MKT_ACC_ENTRY).filter(MKT_ACC_ENTRY.Branch == Branch).delete()
		db.session.query(MKT_ACC_ENTRY_INAU).filter(MKT_ACC_ENTRY_INAU.Branch == Branch).delete()
		db.session.query(MKT_ACC_ENTRY_HIST).filter(MKT_ACC_ENTRY_HIST.Branch == Branch).delete()
		print "Acount entry is deletting ..."

		# Delete Cash Account
		db.session.query(MKT_CASH_ACCOUNT).filter(MKT_CASH_ACCOUNT.CashBranch == Branch).delete()
		db.session.query(MKT_CASH_ACCOUNT_INAU).filter(MKT_CASH_ACCOUNT_INAU.CashBranch == Branch).delete()
		db.session.query(MKT_CASH_ACCOUNT_HIST).filter(MKT_CASH_ACCOUNT_HIST.CashBranch == Branch).delete()
		print "Cash account is deletting ..."

		# Delete Loan Application
		db.session.query(MTK_LOAN_APPLICATION).filter(MTK_LOAN_APPLICATION.Branch == Branch).delete()
		db.session.query(MTK_LOAN_APPLICATION_INAU).filter(MTK_LOAN_APPLICATION_INAU.Branch == Branch).delete()
		db.session.query(MTK_LOAN_APPLICATION_HIST).filter(MTK_LOAN_APPLICATION_HIST.Branch == Branch).delete()
		print "Loan application is deletting ..."

		# Delete Loan Contract
		db.session.query(MTK_LOAN_CONTRACT).filter(MTK_LOAN_CONTRACT.Branch == Branch).delete()
		db.session.query(MTK_LOAN_CONTRACT_INAU).filter(MTK_LOAN_CONTRACT_INAU.Branch == Branch).delete()
		db.session.query(MTK_LOAN_CONTRACT_HIST).filter(MTK_LOAN_CONTRACT_HIST.Branch == Branch).delete()
		print "Loan contract is deletting ..."

		# Delete Loan Amendment
		db.session.query(MKT_LOAN_AMENDMENT).filter(MKT_LOAN_AMENDMENT.Branch == Branch).delete()
		db.session.query(MKT_LOAN_AMENDMENT_INAU).filter(MKT_LOAN_AMENDMENT_INAU.Branch == Branch).delete()
		db.session.query(MKT_LOAN_AMENDMENT_HIST).filter(MKT_LOAN_AMENDMENT_HIST.Branch == Branch).delete()
		print "Loan amendment is deletting ..."

		# Delete Loan Charge
		db.session.query(MKT_LOAN_CHARGE).filter(MKT_LOAN_CHARGE.Branch == Branch).delete()
		db.session.query(MKT_LOAN_CHARGE_INAU).filter(MKT_LOAN_CHARGE_INAU.Branch == Branch).delete()
		db.session.query(MKT_LOAN_CHARGE_HIST).filter(MKT_LOAN_CHARGE_HIST.Branch == Branch).delete()
		print "Loan charge is deletting ..."

		# Delete PAST DUE
		db.session.query(MTK_PAST_DUE).filter(MTK_PAST_DUE.Branch == Branch).delete()
		print "Past due is deletting ..."

		# Delete PD Date
		db.session.query(MTK_PD_DATE).filter(MTK_PD_DATE.Branch == Branch).delete()
		db.session.query(MTK_PD_DATE_INAU).filter(MTK_PD_DATE_INAU.Branch == Branch).delete()
		db.session.query(MTK_PD_DATE_HIST).filter(MTK_PD_DATE_HIST.Branch == Branch).delete()
		print "Past due detail is deletting ..."

		# Delete Schedule Define (Auto and Save)
		db.session.query(MKT_SCHED_DEFINE).filter(MKT_SCHED_DEFINE.Branch == Branch).delete()
		db.session.query(MKT_SCHED_DEFINE_INAU).filter(MKT_SCHED_DEFINE_INAU.Branch == Branch).delete()
		db.session.query(MKT_SCHED_DEFINE_HIST).filter(MKT_SCHED_DEFINE_HIST.Branch == Branch).delete()
		db.session.query(MKT_SCHED_MANUAL).filter(MKT_SCHED_MANUAL.Branch == Branch).delete()
		print "Schedule define is deletting ..."

		# Delete Repayment Schedule
		db.session.query(MKT_REP_SCHEDULE).filter(MKT_REP_SCHEDULE.Branch == Branch).delete()
		db.session.query(MKT_REP_SCHEDULE_INAU).filter(MKT_REP_SCHEDULE_INAU.Branch == Branch).delete()
		db.session.query(MKT_REP_SCHEDULE_HIST).filter(MKT_REP_SCHEDULE_HIST.Branch == Branch).delete()
		print "Repayment schedule is deletting ..."

		# Delete Fund Transfer
		db.session.query(MKT_FUND_TRANSFER).filter(MKT_FUND_TRANSFER.Branch == Branch).delete()
		db.session.query(MKT_FUND_TRANSFER_INAU).filter(MKT_FUND_TRANSFER_INAU.Branch == Branch).delete()
		db.session.query(MKT_FUND_TRANSFER_HIST).filter(MKT_FUND_TRANSFER_HIST.Branch == Branch).delete()
		print "Fund transfer is deletting ..."

		# Delete Multi Debit Credit
		db.session.query(MKT_MULTI_DEBIT).filter(MKT_MULTI_DEBIT.Branch == Branch).delete()
		db.session.query(MKT_MULTI_DEBIT_INAU).filter(MKT_MULTI_DEBIT_INAU.Branch == Branch).delete()
		db.session.query(MKT_MULTI_DEBIT_HIST).filter(MKT_MULTI_DEBIT_HIST.Branch == Branch).delete()
		print "Multi debit is deletting ..."

		db.session.query(MKT_MULTI_CREDIT).filter(MKT_MULTI_CREDIT.Branch == Branch).delete()
		db.session.query(MKT_MULTI_CREDIT_INAU).filter(MKT_MULTI_CREDIT_INAU.Branch == Branch).delete()
		db.session.query(MKT_MULTI_CREDIT_HIST).filter(MKT_MULTI_CREDIT_HIST.Branch == Branch).delete()
		print "Multi credit is deletting ..."

		# Delete Journal
		db.session.query(MKT_JOURNAL).filter(MKT_JOURNAL.Branch == Branch).delete()
		print "Journal is deletting ..."

		# Delete Multi-Journal Entry
		db.session.query(MKT_MULTI_JOURNAL).filter(MKT_MULTI_JOURNAL.Branch == Branch).delete()
		db.session.query(MKT_MULTI_JOURNAL_INAU).filter(MKT_MULTI_JOURNAL_INAU.Branch == Branch).delete()
		db.session.query(MKT_MULTI_JOURNAL_HIST).filter(MKT_MULTI_JOURNAL_HIST.Branch == Branch).delete()
		print "Mulit-journal entry is deletting ..."

		# Delete Journal Entry
		db.session.query(MKT_JOURNAL_ENTRY).filter(MKT_JOURNAL_ENTRY.Branch == Branch).delete()
		db.session.query(MKT_JOURNAL_ENTRY_INAU).filter(MKT_JOURNAL_ENTRY_INAU.Branch == Branch).delete()
		db.session.query(MKT_JOURNAL_ENTRY_HIST).filter(MKT_JOURNAL_ENTRY_HIST.Branch == Branch).delete()
		print "Journal entry is deletting ..."

		# Delete Consol Balance
		db.session.query(MKT_CONSOL_BALANCE).filter(MKT_CONSOL_BALANCE.Branch == Branch).delete()
		print "Consolidate balance is deletting ..."

		# Delete GL Balance
		db.session.query(MKT_GL_BALANCE).filter(MKT_GL_BALANCE.Branch == Branch).delete()
		print "GL balance is deletting ..."

		db.session.commit()
		print "Branch %s was successfully deleted from the system."
		
		return True

	except Exception, e:
		db.session.rollback()
		return e

try:
	
	getReverseBranch("000004")
	getReverseBranch("000005")

except Exception, e:
	raise