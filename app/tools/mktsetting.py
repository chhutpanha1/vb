from app.mktcore.wtfimports 	import *
from app.Setting.models 		import MKT_SETTING, MKT_TERM_DEPOSIT_SETTING
from app.Dates.models 			import MKT_DATES
from sqlalchemy 				import or_
from .. 						import app, db



import mktdate 		as mktdate
from sqlalchemy 	import create_engine,inspect

class Struct(object):

	def __init__(self, **entries):
		self.__dict__.update(entries)

def getSetting():
	
	engine 		= create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
	inspector 	= inspect(engine)
	ListTable 	= []

	for table_name in inspector.get_table_names():
		ListTable.append(str(table_name))
	
	Setting = ""
	if 'MKT_SETTING' in ListTable:
		Setting = MKT_SETTING.query.get("SYSTEM")

	return Setting

def getTASetting():
	
	engine 		= create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
	inspector 	= inspect(engine)
	ListTable 	= []

	for table_name in inspector.get_table_names():
		ListTable.append(str(table_name))
	
	Setting = ""
	if 'MKT_TERM_DEPOSIT_SETTING' in ListTable:
		Setting = MKT_TERM_DEPOSIT_SETTING.query.get("SYSTEM")

	return Setting

def getBankDate():
	try:

		BankDate = MKT_DATES.query.get("SYSTEM")
		if BankDate:
			CurrentDate = BankDate.SystemDate
		else:
			BankDate = {'SystemDate':str(mktdate.getDateISO())}
			CurrentDate = Struct(**BankDate)
		
		return CurrentDate

	except:
		db.session.rollback()
		raise

def setFirstRecordDate():
	try:

		BankDate = MKT_DATES(
					ID 				=	'SYSTEM'
				)
		return BankDate
		
	except:
		raise

def setFirstRecordAccSetting():
	try:

		DateTime = mktdate.getDateTimeNow()

		BankDate = MKT_ACCOUNTING_SETTING(
					Status			= 	'AUTH',
					Curr 			= 	'0',
					Inputter 		= 	'System',
					Createdon 		= 	DateTime,
					Authorizer 		= 	'System',
					Authorizeon 	= 	DateTime,
					ID 				=	'SYSTEM'
				)
		return BankDate
		
	except:
		raise

def getBankDateObj():
	try:

		BankDate = MKT_DATES.query.get("SYSTEM")
		if not BankDate:
			BankDate = setFirstRecordDate()
			db.session.add(BankDate)
			db.session.commit()

			BankDate = MKT_ACCOUNTING_SETTING.query.get("SYSTEM")
		
		return BankDate

	except:
		raise

def getAccSetting():
	try:

		engine 		= create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
		inspector 	= inspect(engine)
		ListTable 	= []
		
		for table_name in inspector.get_table_names():
			ListTable.append(str(table_name))
		
		Setting = ""

		if 'MKT_ACCOUNTING_SETTING' in ListTable:

			Setting = MKT_ACCOUNTING_SETTING.query.get("SYSTEM")
			
		return Setting

	except:
		raise

def getExcludedCategories():
	try:

		Setting 		=	getSetting()
		ExcludedCat 	=	Setting.JOURNAL_ENTRY_EXCLUDED
		ExcludedCat 	=	ExcludedCat.split()

		Results 		=	[]

		if len(ExcludedCat) > 0:

			for item in ExcludedCat:
				
				Results.append(item)

		CheckGL 		=	db.session.query(
								MKT_GL_MAPPING.ID
							).\
							join(
								MKT_CATEGORY,
								MKT_CATEGORY.ID == MKT_GL_MAPPING.ID
							).\
							filter(
								MKT_GL_MAPPING.SubGL != ''
							).\
							all()

		if CheckGL:

			for item in CheckGL:

				Results.append(item.ID)

		return Results

	except:
		raise