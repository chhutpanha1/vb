from flask 						import session, redirect, render_template, url_for, flash
from werkzeug.security 			import generate_password_hash, check_password_hash
from app.User.models 			import MKT_USER
from app.Menu.models 			import MKT_MENU_ITEM, MKT_MENU
from app.Module.models 			import MKT_FORM
from sqlalchemy 				import *
from app.Role.models 			import MKT_ROLE
from app.Branch.models 			import MKT_BRANCH
from app.LoginUser.models 		import *
from app.mktcore.wtfimports 	import *
from app import db

import mktparam 				as mktparam
import mktsetting 				as mktsetting
import mktdate 					as mktdate

import time, pprint, collections, calendar
from datetime 					import datetime

def isLogIn(LogInName, Password = None):
	if Password:
		user = MKT_USER.query.filter_by(LogInName=LogInName).first()
		if user.Password != '':
			if check_password_hash(user.Password, Password):
				return user
			else:
				return False
		else:
			return False
	else:
		user = MKT_USER.query.filter_by(LogInName=LogInName).first()
		if user:
			session['getLogInName'] = user.LogInName
			session['getID'] = user.ID	
			checkResetPassword()
			return user
		else:
			session.clear()
			return False

def checkToResetPassword():
	if session.get('getID'):
		return True
	else:
		return False

def checkResetPassword():
	if session.has_key('getID'):
		return True
	else:
		session.clear()
		flash("Please check your username")
		return render_template("login.html")

def setLogOut():
	if 'getLogInID' in session:
		session.pop('getLogInID', None)

	if 'getLogInName' in session:
		session.pop('getLogInName', None)

	if 'getID' in session:
		session.pop('getID', None)

	session.clear()
	flash('You were logged out')
	return redirect("Morakot/Login")

def getUser(UserID=''):
	if UserID:
		user = MKT_USER.query.get(UserID)
	else:
		user = MKT_USER.query.get(session['getLogInID'])

	session["RestrictBranch"] = user.RestrictBranch
	session["AccessBranch"] = user.AccessBranch
	return user

def getVaultInfo(Currency, UserID=""):
	try:
		
		AccID 			= ""
		TellerParam 	= mktparam.getTellerParam()
		if 'getLogInID' in session:

			if UserID:
				VaultID = getUser(UserID).CashAccount
			else:
				VaultID = getUser().CashAccount

			CashObj = MKT_CASH_ACCOUNT.query.get(VaultID)

			if CashObj:
				Category 	= 	TellerParam.VaultCategory
				AccID 		= 	str(Currency) + str(Category) + str(VaultID)

		VaultObj = MKT_ACCOUNT.query.get(AccID)

		return VaultObj

	except Exception, e:
		return "%s" %e

def getMenuOutput():
	MenuItem = getMenuItem()
	return MenuItem

def getAccessMenu():

	Menu 		= 	getUser().Menu
	Menu 		=	Menu.split()

	if 'SwitchMenu' in session:
		ID = session['SwitchMenu']
	else:
		ID = Menu[0]

	return ID

def getAccessMenuList():

	ID 	=	getUser().Menu
	ID 	=	ID.split()

	MenuObj = 	MKT_MENU.query.\
				filter(MKT_MENU.ID.in_(ID)).\
				all()

	return [MenuObj, ID]

def getMenuItem():

	ID 	=	getAccessMenu()
	# ID 	=	ID[0]

	MenuItem 	= 	MKT_MENU_ITEM.query.\
					order_by(asc(MKT_MENU_ITEM.ItemID)).\
					filter(MKT_MENU_ITEM.ID == ID).\
					filter(MKT_MENU_ITEM.Parents == "").\
					filter(MKT_MENU_ITEM.Active == 'Y')

	return MenuItem.all()

def getSubMenuItem():

	ID 	=	getAccessMenu()
	# ID 	=	ID[0]

	MenuItem 	= 	MKT_MENU_ITEM.query.\
					order_by(asc(MKT_MENU_ITEM.ItemID)).\
					filter(MKT_MENU_ITEM.ID == ID).\
					filter(MKT_MENU_ITEM.Active == 'Y')

	return MenuItem.all()

def getFormURL():
	form = MKT_FORM.query.all()
	return form

def getPosition():
	RoleID = getUser().Role
	Role = MKT_ROLE.query.get(RoleID).Description
	return Role

def getBranch(ChangeBranch =""):

	if ChangeBranch != "" : # execute when user changes branch
		return MKT_BRANCH.query.get(ChangeBranch)

	BranchID = getUser().AccessBranch.split() # CHANGE
	if len(BranchID) > 0:
		BranchID = BranchID[0]
		if BranchID == "ALL" :
			Branch = MKT_BRANCH.query.first()
		else :
			Branch = MKT_BRANCH.query.get(BranchID)
	else:
		Branch = MKT_BRANCH.query.first()

	session["ChangeBranch"] = Branch.ID

	return Branch

def getMultiBranch(BranchList):
	try:
		BranchList = BranchList.split()
		return 	MKT_BRANCH.query.\
				order_by(MKT_BRANCH.ID.asc()).\
				filter(MKT_BRANCH.ID.in_(BranchList)).\
				all()

	except:
		raise

def saveLoginUser(UserID="", Role="", Branch="", LogInTime="", LogInDate=""):

	try:
		Status 			= "AUTH"
		Curr 			= "0"

		DateTimeNow 	= mktdate.getDateTimeNow()
		user = MKT_LOGIN_USER.query.filter_by(UserID=UserID).first()

		if user:
			user.Status 		= Status
			user.Curr 			= Curr
			user.Inputter 		= UserID
			user.Createdon 		= DateTimeNow
			user.Authorizer 	= UserID
			user.Authorizeon 	= DateTimeNow
			user.Branch 		= Branch
			user.LogInTime 		= LogInTime
			user.LogInDate 		= LogInDate
		else:
			user 	= 	MKT_LOGIN_USER(
							Status=Status,
							Curr=Curr,
							Inputter=UserID,
							Createdon=DateTimeNow,
							Authorizer=UserID,
							Authorizeon=DateTimeNow,
							Branch=Branch,
							UserID=UserID,
							Role=Role,
							LogInTime=LogInTime,
							LogInDate=LogInDate
						)

		db.session.add(user)
		db.session.commit()

	except:
		db.session.rollback()
		raise

def updateLastLogin(UserID=None):
	try:
		user = MKT_USER.query.get(UserID)

		if user:

			attempt = mktsetting.getSetting().ATTEMPS if mktsetting.getSetting().ATTEMPS else int(3)
			dat 	= datetime.now()
			tim 	= '%s:%s:%s'% (dat.hour, dat.minute, dat.second)
			d 		= '%s-%s-%s'% (dat.year, dat.month, dat.day)

			user.DateLastLogin 	= d
			user.TimeLastLogin 	= tim
			user.Attempts 		= attempt

			db.session.add(user)
			db.session.commit()

	except:
		db.session.rollback()
		raise

def checkAttemptsPasswordWrong(LogInName=None):
	user = MKT_USER.query.filter_by(LogInName=LogInName).first()
	numOfAttempt = 0
	if user:
		user.Attempts = user.Attempts - 1
		db.session.add(user)
		db.session.commit()
		numOfAttempt = user.Attempts
	return numOfAttempt

def addMonth(source, months):
	Month = source.month - 1 + months
	Year = source.year + Month / 12
	Month = Month % 12 + 1
	Day = min(source.day, calendar.monthrange(Year, Month)[1])
	return datetime(Year, Month, Day)

def getSearchUser():
	search = request.args.get('q')
	NAMES = []
	a = MKT_USER.query.\
		filter(or_(MKT_USER.ID.like('%'+search.upper()+'%'), MKT_USER.DisplayName.like('%'+search+'%'))).\
		all()
	for row in a:
		dic = { "id":row.ID, "text":row.ID + " - " + row.DisplayName }
		NAMES.append(dic)

	app.logger.debug(NAMES)
	return jsonify(items = NAMES)

def setDBCommit():
	return db.session.commit()

def checkLoginTime(From=None, To=None):
	
	try:
		CurrentDate = datetime.now()
		CurrentTime = '%s:%s:00'% (CurrentDate.hour, CurrentDate.minute)
		# StrCurrDate = '%s-%s-%s'% (CurrentDate.year, CurrentDate.month, CurrentDate.day)
		CurrTime 	= 	str(CurrentTime)
		(H, M, S) 	= 	CurrTime.split(":")
		TimeNow 	= 	int(H) * 3600 + int(M) * 60
		
		if not From and not To:
			return True

		else:
			
			if From:
				(H1, M1) 	= 	From.split(":")
				intFrom 	= int(H1) * 3600 + int(M1) * 60
				
				if int(intFrom) > int(TimeNow):
					# flash(str(intFrom) + " " + str(TimeNow))
					flash("Now you can't login, Please wait until %s" %From)
					return False

			if To:
				(H2, M2) 	= 	To.split(":")
				intTo 		= int(H2) * 3600 + int(M2) * 60

				if int(intTo) < int(TimeNow):
					# flash(str(intTo) + " " + str(TimeNow))
					flash("Time is over for login. You can login from %s to %s" %(From, To))
					return False

			return True

	except:
		flash("Something wrong, Please check...")
		return False

# Validate password with administrator setting
def checkPasswordRule(password):
	try:

		PasswordLength = mktsetting.getSetting().PASSWORD_LENGTH if mktsetting.getSetting().PASSWORD_LENGTH else "6"
		PasswordLength = PasswordLength.strip()
		PasswordLength = int(PasswordLength)

		UpperLater = mktsetting.getSetting().PASSWORD_CAP if mktsetting.getSetting().PASSWORD_CAP else "0"
		UpperLater = UpperLater.strip()
		UpperLater = int(UpperLater)

		LowerLater = mktsetting.getSetting().PASSWORD_LOW if mktsetting.getSetting().PASSWORD_LOW else "0"
		LowerLater = LowerLater.strip()
		LowerLater = int(LowerLater)

		NumberLater = mktsetting.getSetting().PASSWORD_NUM if mktsetting.getSetting().PASSWORD_NUM else "0"
		NumberLater = NumberLater.strip()
		NumberLater = int(NumberLater)
		
		check = True

		if len(password) < int(PasswordLength):
			flash(msg_error + "Your password must be bigger than or equal %s digits" %PasswordLength)
			# return redirect(url_for("setPassword"))
			check = False

		Result = [row.isupper() for row in str(password)]
		UpperNum = 0
		for row in Result:
			if row:
				UpperNum += 1

		if int(UpperNum) < int(UpperLater):
			flash(msg_error + "Password required upper later %s character(s)." %UpperLater)
			# return redirect(url_for("setPassword"))
			check = False

		Result = [row.islower() for row in str(password)]
		LowerNum = 0
		for row in Result:
			if row:
				LowerNum += 1

		if int(LowerNum) < int(LowerLater):
			flash(msg_error + "Password required lower later %s character(s)." %LowerLater)
			# return redirect(url_for("setPassword"))
			check = False

		Result = [row.isdigit() for row in str(password)]
		DigitNum = 0
		for row in Result:
			if row:
				DigitNum += 1

		if int(DigitNum) < int(NumberLater):
			flash(msg_error + "Password required number %s digit(s)." %NumberLater)
			# return redirect(url_for("setPassword"))
			check = False

		return check

	except:
		flash(msg_error + "Something wrong, Please check...")
		return False

def getAccessBranch():

	#get branch access from user
	BranchAccess = getUser().AccessBranch
	if 'ALL' in BranchAccess :
		query = MKT_BRANCH.query.order_by(MKT_BRANCH.ID.asc()).all()
	else :
		itemsplit = BranchAccess.split()

		query = MKT_BRANCH.query.filter(MKT_BRANCH.ID.in_(itemsplit)).order_by(MKT_BRANCH.ID.asc()).all()

	return query

def checkAccessBrach(BranchList):
	try:

		BranchAccess 	= 	getUser().AccessBranch
		OOPBranch 		=	""
		if 'ALL' in BranchAccess:
			{"Check":True, "Branch":OOPBranch}
		else:
			MyAccBranch = 	BranchAccess.split()
			BranchList 	=	BranchList.split()
			
			for Branch in BranchList:
				# for Access in MyAccBranch:
				if Branch.upper() in MyAccBranch:
					OOPBranch = ""
				else:
					OOPBranch = Branch
					break

		if OOPBranch:
			return {"Check":False, "Branch":OOPBranch}
		else:
			return {"Check":True, "Branch":OOPBranch}

	except:
		raise

def getCurrentBranch():
	if "ChangeBranch" in session:

		return session["ChangeBranch"]
	else:
		return getBranch().ID

def getSystemStatus():
	try:

		Status = "1"

		Seting = mktsetting.getSetting()

		if Seting:

			Status = Seting.SYSTEM_STATUS if Seting.SYSTEM_STATUS else "1"

		return Status

	except:
		raise

def getUserRole(ID):
	try:

		Role = ""
		User = getUser(ID)

		return User.Role

	except:
		raise

# def checkAllowBranch(Branch, BackLink):
# 	try:

# 		CheckBranch = checkAccessBrach(Branch)

# 		if not CheckBranch["Check"]:

# 			flash(msg_error + " You can't access this %s branch." %CheckBranch["Branch"])
# 			return redirect(url_for('getNotAllowBranch', BackLink=BackLink))

# 		else:

# 			return True

# 	except:
# 		raise

# @app.route("/Morakot/NotAllowBranch")
# def getNotAllowBranch():
# 	try:

# 		return render_template('changebranch.html')

# 	except:
# 		raise