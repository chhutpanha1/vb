# -*- coding: utf-8 -*-
'''
Created Date: 01 Apr 2015
Author: Sovankiry Rim

Modified by: 
Modified Date:
All Right Reserved Morakot Technology
Description : mkttool refer to function for import, export file 
'''

import app.mktcore.globalfunction as globalfunction
import collections, zipfile, shutil, csv, os, re

from app.mktcore.wtfimports import *
from app                    import db
from sys                    import platform
from flask                  import flash
from wtforms                import validators
from app.Company.models     import MKT_COMPANY
from datetime               import datetime, date, timedelta
from app.config             import *
import app.tools.user           as mktuser


def getAllowedFile(filename):
    #if allow funtion return True
    AllowFile = set(['csv','CSV'])
    return '.' in filename and filename.rsplit('.', 1)[1] in AllowFile

def getAllowedFileZip(filename):
    #if allow funtion return True
    AllowFile = set(['zip'])
    return '.' in filename and filename.rsplit('.', 1)[1] in AllowFile

def getAllowedFileMorakot(filename):
    #if allow funtion return True
    AllowFile = set(['mkt'])
    return '.' in filename and filename.rsplit('.', 1)[1] in AllowFile  
# the first row in file is contains headers.
# return : {'Title 1': '1', 'Title 3': '08/18/07', 'Title 2': 'a'}
def getDictCSV(FileCSV):
    DictCSV = list(csv.DictReader(FileCSV))
    return DictCSV

# return : 1,"a",08/18/07
def getListCSV(FileCSV):
    ListCSV = list(csv.reader(FileCSV))
    return ListCSV

#ListObj = List of string formation csv 
#return List Of dictionary
#Output: [{1:'a'},{1:'b'}]
#Notes: don't use ',' in column value
def getListDict(StringSplitlines):
    ListDicCSV  = []
    DicCol      = {}
    index = 1
    for row in StringSplitlines.splitlines():
        row =  row.split(',')
        DicValue    = {}
        if index==1:
            for colid, col in enumerate(row):
                DicCol.update({colid:col})
        else:
            for colid, col in enumerate(row):
                ColumnName = DicCol[colid]
                DicValue.update({ColumnName:col})
            ListDicCSV.append(DicValue)
        index+=1
    return ListDicCSV

def isString(value=''):
    try:
        if value:
            value = float(value)
            return False
        elif value==0:
            return False
        else:
            return True
    except ValueError:
        return True

def isCharacter(value):
    # Return true if all characters in the string are alphabetic 
    # and there is at least one character, false otherwise.
    try:
        value = str(value)
        return value.isalpha()
    except Exception, e:
        return False

def isAlphaNumeric(value):
    # Return true if all characters in the string are alphanumeric 
    # and there is at least one character, false otherwise.
    try:
        value = str(value)
        return value.isalnum()
    except Exception, e:
        return False

def isContainWhitespace(value):
    # Return true if the string contain white space like space tab and new line
    # else false otherwise.
    search=re.compile(r'[ \n\r\t]').search
    if bool(search(value)):
        return True
    else:
        return False

def isSpecialCharacter(value):
    # Return true if all specail character in the string
    # else false otherwise.
    search=re.compile(r'[^a-z0-9.]').search
    if bool(search(value)):
        return True
    else:
        return False

def isInteger(value):
    try:
        if value:
            value = int(value)
            return True
        else:
            return False
    except ValueError:
        return False

def isFloat(value):
    try:
        if value:
            value = float(value)
            return True
        else:
            return False
    except ValueError:
        return False

def isMoney(value=''):
    try:
        if value:
            value = str(value)
            value = value.replace(',','')
            value = float(value)
            return True
        else:
            return False
    except ValueError:
        return False

def getCompany():
    try:

        Record = MKT_COMPANY.query.get("SYSTEM")

        return Record
        
    except:
        raise

class clsToolbar:
    def getToolbarevent(self,Operation=""):
        if Operation in ('New','Edit'):
            ul_list=[('Save',''),('Cancel','')]

        elif Operation in ('Search'):
            ul_list=[('Delete',''),('Authorize',''),('Edit',''),('Cancel','')]

        elif Operation in ('Show'):
            ul_list=[('Show',''),('Edit',''),('Cancel','')]

        elif Operation in ('readonly'):
            ul_list=[('Cancel','')]

        else:
            ul_list=[('New',''),('Edit',''),('Cancel','')]
            
        return ul_list

#return param result
def render_param(get_maxfield,model):
    param=""
    get_maxfield=0
    if len(request.args)>0:
        ListArgs    = [item for item in request.args if "fieldname" in item] 
        get_maxfield=len(ListArgs)
        for i in range(1,int(get_maxfield)+1):

            if param=="":
                param=globalfunction.searchString(model,request.args.get("fieldname_1"),request.args.get("operator_1"),request.args.get("searchtext_1"))
            else:
                param = param+","+globalfunction.searchString(model,request.args.get("fieldname_"+str(i)),request.args.get("operator_"+str(i)),request.args.get("searchtext_"+str(i)))
    return param


def setChangeLog(Type,Msg):
    try:
        filename    = "%slog/change.log"%VB_PATH
        dat         = datetime.now()
        CurrentTime = '%s:%s:%s'% (dat.hour, dat.minute, dat.second)
        currentDate = '%s-%s-%s'% (dat.year, dat.month, dat.day)
        DateTime    = str(currentDate) + " " + CurrentTime
        Msg = "[%s] %s : %s"%(Type,DateTime,Msg)

        setWriteFile(filename,Msg,False)
        
    except Exception, e:
        raise

def isExistDirectory(directory):
    if os.path.exists(directory):
        return True
    return False    

def setDirectory(Directory):
    try:
        if not isExistDirectory(Directory):
            os.makedirs(Directory)
            # print "Directory was created."
        return True,""
    except Exception, e:
        setChangeLog("Error","%s"%e)
        return False,e

def setUnZip(ZipFile,ExtractTo):
    # zfile = zipfile.ZipFile('%s%s.zip'%(TmpPath,value))
    zfile = zipfile.ZipFile(ZipFile)        
    zfile.extractall(ExtractTo)

def setDeleteDirectory(Directory):
    #will delete a directory and all its contents.
    if isExistDirectory(Directory):
        shutil.rmtree(Directory)
    return True

def setDeleteFile(File):
    #will remove a file.
    if isExistDirectory(File):
        os.remove(File)
    return True

# from os import listdir
# from os.path import isfile, join

def getListDirectory(Path,Mode=1):
    ListObj = []
    if isExistDirectory(Path): 
        if Mode == 1:# List get you everything that's in a directory - files and directories
            ListObj = os.listdir(Path)

        elif Mode == 2: # List only files

            ListObj = [f for f in os.listdir(Path) if os.path.isfile(os.path.join(Path, f))]
        
        elif Mode == 3: # List only directories
            ListObj = [f for f in os.listdir(Path) if not os.path.isfile(os.path.join(Path, f))]

    return ListObj

def setCreateFile(PathFile) :
    if not isExistDirectory(PathFile):
        ObjFile = open(PathFile, 'w+')
        ObjFile.close()
                
def getReadFile(PathFile,ReadLine=False):
    Result = ''
    if isExistDirectory(PathFile):
        ObjFile     = open(PathFile, 'r+')
        Result      = ObjFile.readlines()  if ReadLine else ObjFile.read() 
        ObjFile.close() # you can omit in most cases as the destructor will call it
    return Result

def setWriteLineFile(PathFile,Data):
    FileWrite = open(PathFile, 'w')
    FileWrite.writelines(Data)
    FileWrite.close()

def setWriteFile(File,Text,Overwirte=True):
    try:
        if Overwirte:
            ObjFile = open(File,'r+')
            ObjFile.truncate()
        else:
            ObjFile = open(File,'ab')
        try:
            Text = str(Text)
        except Exception, e:
            Text = u'%s'%Text
        ObjFile.write(Text+'\n')# python will convert \n to os.linesep
        ObjFile.close() # you can omit in most cases as the destructor will call it
        return True
    except Exception, e:
        raise
    
def getPlatform():
    if platform in ["linux","linux2","darwin"]:
        return 1 #Linxu Server
    elif platform == "win32":
        return 2 #Windows
    else:
        return 0 #Not found

def setCopyFile(SourcePath,DestinationPath):
    try:

        if os.path.isfile(SourcePath):
            
            shutil.copy2(SourcePath,DestinationPath)
        else:
            
            shutil.move(SourcePath,DestinationPath)
        # if os.path.isdir(SourcePath):

        return True,""
    except Exception, e:
        return False,"%s"%e

def setRunSQL(RawSQL):
    Conn = db.engine.connect()
    Session = Conn.begin()
    try:
        
        Conn.execute(RawSQL)
        Session.commit()
    except Exception, e:
        Session.rollback()
        raise

def formatSize(num, suffix='B'):
    for unit in ['','K','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Y', suffix)

def isIDNumber(Type,ID):
    '''
    Type :
            N National ID 
            F Family Book 
            P Passport
            D Drivers Licence 
            G Government Issued ID 
            B Birth Certificate
            V Voter Reg. Card 
            T Tax Number
            R Resident Book
    '''
    # Check first charater
    FirstChar   = ID[:1]
    SecondChar  = ID[:2][-1:]
    ThirdChar   = ID[:3][-1:]

    DicFunction = {'N':isInteger,'B':isAlphaNumeric,'*':isSpecialCharacter,'P*':isPassportID}

    DicValidate = { 'N':{'MinLength' : 9 ,'MaxLength' : 9  ,'FirstChar':'B' ,'SecondChar':'N' ,'ThirdChar':'N'},
                    'F':{'MinLength' : 1 ,'MaxLength' : 20 ,'FirstChar':'B' ,'SecondChar':'B' ,'ThirdChar':'B'},
                    'P':{'MinLength' : 5 ,'MaxLength' : 15 ,'FirstChar':'B' ,'SecondChar':'P*','ThirdChar':'B'},
                    "D":{'MinLength' : 5 ,'MaxLength' : 20 ,'FirstChar':'B' ,'SecondChar':'B' ,'ThirdChar':'B'},
                    "G":{'MinLength' : 1 ,'MaxLength' : 20 ,'FirstChar':'B' ,'SecondChar':'B' ,'ThirdChar':'B'},
                    "B":{'MinLength' : 1 ,'MaxLength' : 20 ,'FirstChar':'B' ,'SecondChar':'B' ,'ThirdChar':'B'},
                    "V":{'MinLength' : 1 ,'MaxLength' : 20 ,'FirstChar':'B' ,'SecondChar':'B' ,'ThirdChar':'B'},
                    "T":{'MinLength' : 1 ,'MaxLength' : 20 ,'FirstChar':'B' ,'SecondChar':'B' ,'ThirdChar':'B'},
                    "R":{'MinLength' : 1 ,'MaxLength' : 20 ,'FirstChar':'B' ,'SecondChar':'B' ,'ThirdChar':'B'}
                  }

    if Type in DicValidate:
        Option          = DicValidate[Type]
        MinLength       = Option['MinLength']
        MaxLength       = Option['MaxLength']
        OptFirstChar    = Option['FirstChar']
        OptSecondChar   = Option['SecondChar']
        OptThirdChar    = Option['ThirdChar']
        # Check length min
        if MinLength:
            if len(ID) < MinLength:
                return False,Option
        # Check length max
        if MaxLength:
            if len(ID) > MaxLength:
                return False,Option

        if OptFirstChar:
            FunctionObj = DicFunction[OptFirstChar]
            # Check first character
            if not FunctionObj(FirstChar):
                return False,Option

        if Type == 'P':
            if OptSecondChar:
                FunctionObj = DicFunction[OptSecondChar]
                # check passport ID
                if not FunctionObj(FirstChar, SecondChar):
                    return False,Option
        else:
            if OptSecondChar:
                FunctionObj = DicFunction[OptSecondChar]
                # Check second charater
                if not FunctionObj(SecondChar):
                    return False,Option

        if OptThirdChar:
            FunctionObj = DicFunction[OptThirdChar]
            # Check third charater
            if not FunctionObj(ThirdChar):
                return False,Option

    return True,''

def isPassportID(FirstChar, SecondChar):

    if FirstChar == 'N':
        if SecondChar.upper() != 'O' :
            return True
        else:
            return False
    else:
        if isAlphaNumeric(SecondChar):
            if isInteger(FirstChar) or isInteger(SecondChar):
                return True
            else:
                return False
        else:
            return False


def isMobile(Number):

    MobilePrefix = {'010':  6,  '011':  6,  '012':  6,  '013':  6,  '015':  6,  '016':  6,
                    '017':  6,  '018':  6,  '031':  7,  '038':  7,  '060':  6,  '061':  6,
                    '066':  6,  '067':  6,  '068':  6,  '069':  6,  '070':  6,  '071':  7,
                    '076':  7,  '077':  6,  '078':  6,  '080':  6,  '081':  6,  '083':  6,
                    '084':  6,  '085':  6,  '086':  6,  '087':  6,  '088':  7,  '089':  6,
                    '090':  6,  '092':  6,  '093':  6,  '095':  6,  '096':  7,  '097':  7,
                    '098':  6,  '099':  6}
                    
    if isInteger(Number):
        Number = str(Number)
        Prefix = Number[:3]
        if Prefix in MobilePrefix:
            TotalLength = MobilePrefix[Prefix] + 3
            if len(Number) != TotalLength:
                return False,"Invalid mobile phone number. Ex: 012xxxxxx"
            else:
                return True,""
        else:
            return False,"Allow only mobile phone number."
    else:
        return False,"Invalid mobile phone number. Ex: 012xxxxxx"
        
import xml.etree.cElementTree as ET
def setXML(XMLDic,**kwarg):
    '''
        * Formatting :
            'Index' :{
                    'ElementName'   :'value'
                    'ElementType'   :'value',
                    'ElementParent' :'value',
                    'Text'          :'value',
                    'Set'           :[('key','value'),('key1','value1')....]
                    }
            }
        * Attribute :
            ElementName     : Name of Element
            ElementType     : Element, SubElement
            ElementParent   : Refer to Parent of Element
            Text            : The value of Element
            Set             : Set the attribute key on the element to value.
                              The value of Set is list.
        * Notes :
            First Element must be Main Element.
            return ElementTree Objects of xml.
        * Example :
            XMLDic = {
                    1       :{
                                'ElementName':'REQUEST',
                                'ElementType':'Element',
                                'Set':[('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance'),('xsi:noNamespaceSchemaLocation','Enquiry.xsd')]
                                },
                    2       :{
                                'ElementName':'SERVICE',
                                'ElementType':'SubElement',
                                'ElementParent':'REQUEST',
                                'Text':'ENQUIRYV3'
                                },
                    3       :{
                                'ElementName':'ACTION',
                                'ElementType':'SubElement',
                                'ElementParent':'REQUEST',
                                'Text':'A_SC'
                            }

                    }
    '''
    try:
        XMLDic = collections.OrderedDict(sorted(XMLDic.items()))
        DicAttr = {}
        MainElementName = ''
        for key,value in XMLDic.iteritems():

            ElementName     = value['ElementName']      if 'ElementName'    in value else ''
            ElementType     = value['ElementType']      if 'ElementType'    in value else ''
            ElementParent   = value['ElementParent']    if 'ElementParent'  in value else ''
            Text            = value['Text']             if 'Text'           in value else ''
            Set             = value['Set']              if 'Set'            in value else ''
            if ElementName:

                if ElementType == "Element":
                    MainElementName = ElementName
                    # build a tree structure
                    MainElement         = ET.Element(ElementName)
                    MainElement.text    = Text
                    if Set:
                        for row in Set:
                            MainElement.set(row[0],row[1])
                    DicAttr.update({ElementName:MainElement})

                else:

                    if ElementParent in DicAttr:
                        MainElementObj = DicAttr[ElementParent]
                    else:
                        return False,'%s Element parent not found in xml.'%ElementParent
                    SubElement  = ET.SubElement(MainElementObj,ElementName)
                    SubElement.text = Text
                    if Set:
                        for row in Set:
                            SubElement.set(row[0],row[1])

                    DicAttr.update({ElementName:SubElement})

                # wrap it in an ElementTree instance, and save as XML
                tree = ET.ElementTree(DicAttr[MainElementName])
                # tree.write("first.xml",xml_declaration=True, encoding='UTF-8')

            else:
                return False,"Attribute Element Name not found in parameter"
        return True,tree
    except Exception, e:
        return False,"Exception: %s"%e

def isUnicode(text=''):
    try:
        return isinstance(text, unicode)
    except Exception, e:
        return False

def getDictionaryObj(Value):
    DicObj = {}
    if Value:
        if Value.find('\n') != -1:
            ListValue = Value.split('\n')
            ListValue = filter(None,ListValue)

        else:
            ListValue = Value.split()
            ListValue = filter(None,ListValue)

        for item in ListValue:
            row = item.split('*')
            if len(row) == 2 :
                DicObj.update({str(row[0]):str(row[1])})

    return DicObj


num_en = {

    '0':'zero',
    '1': 'one',
    '2': 'two',
    '3': 'three',
    '4': 'four',
    '5': 'five',
    '6': 'six',
    '7': 'seven',
    '8': 'eight',
    '9': 'nine',
    '10': 'ten',
    '11': 'eleven',
    '12': 'twelve',
    '13': 'thirteen',
    '14': 'fourteen',
    '15': 'fifteen',
    '16': 'sixteen',
    '17': 'seventeen',
    '18': 'eighteen',
    '19': 'nineteen',
    '20': 'twenty',
    '30': 'thirty',
    '40': 'forty',
    '50': 'fifty',
    '60': 'sixty',
    '70': 'seventy',
    '80': 'eighty',
    '90': 'ninety',
    'H' : 'hundred',
    'K' : 'thousand',
    'M' : 'million',
    'B' : 'billion',
    '.' : 'point'
}

num_kh = {
    '0': u'សូន្យ',
    '1': u'មួយ',
    '2': u'ពីរ',
    '3': u'បី',
    '4': u'បួន',
    '5': u'ប្រាំ',
    '6': u'ប្រាំមួយ',
    '7': u'ប្រាំពីរ',
    '8': u'ប្រាំបី',
    '9': u'ប្រាំបួន',
    '10': u'ដប់',
    '20': u'ម្ភៃ',
    '30': u'សាមសិប',
    '40': u'សែសិប',
    '50': u'ហាសិប',
    '60': u'ហុកសិប',
    '70': u'ចិតសិប',
    '80': u'ប៉ែសិប',
    '90': u'កៅសិប',
    'H' : u'រយ',
    'K' : u'ពាន់',
    '10K' : u'មឺុន',
    '100K' : u'សែន',
    'M' : u'លាន',
    'B' : u'កោដិ',
    '.' : u'ក្បៀស'
}

# convert hundred
def convertHundred(number, lang = 'en'):

    result = []

    if int(number) < 1000:

        if lang == 'en':

            num = number

            while num >= 20:

                if num >= 100:
                    result.append(num_en[str(num/100)])
                    result.append(num_en['H'])

                    num %= 100

                elif num < 100:
                    result.append(num_en[str((num/10) * 10)])
                    num %= 10

            if num != 0:
                result.append(num_en[str(num)])

        #EOF lang = en

        elif lang == 'kh':

            index = 1
            numLength = len(number)

            while index <= numLength:

                if index == 1 and number[-index] != '0':
                    result.append(num_kh[number[-index]])

                elif index == 2 and number[-index] != '0':
                    result.insert(0,num_kh[number[-index] + '0'])

                elif index == 3 and number[-index] != '0':

                    result.insert(0, num_kh['H'])
                    result.insert(0, num_kh[number[-index]])

                index += 1

        #EOF lang = kh

    return result

def formatNumberToWord(number, lang = 'en', currency=None):

    try:
        # remove comma
        number = str(number).replace(',','')
        checkNumber = float(number)
    except:
        raise ValueError("Invalid number")

    result = []

    if lang == 'en':

        value = number.split('.')

        MAX_NUM = 999999999999
        BILLION = 1000000000
        MILLION = 1000000
        THOUSAND = 1000

        if int(value[0]) <= MAX_NUM:

            num = int(value[0])

            while num >= 1000:
                if num >= BILLION:
                    billion_num = int(num / BILLION)
                    result += convertHundred(billion_num)
                    result.append(num_en['B'])

                    num = int(num % BILLION)
                #end if BILLION

                elif num >= MILLION:
                    million_num = int(num / MILLION)
                    result += convertHundred(million_num)
                    result.append(num_en['M'])

                    num = int(num % MILLION)
                #end if MILLION

                elif num >= THOUSAND:
                    thousand_num = int(num / THOUSAND)
                    result += convertHundred(thousand_num)
                    result.append(num_en['K'])

                    num = int(num % THOUSAND)
                #end if THOUSAND

            result += convertHundred(num)

            if currency is not None:

                if currency == 'USD':

                    if int(value[0]) > 0:
                        result.append('dollars ')

                    # check if have floating point
                    if len(value) == 2:
                        fNum = value[1]
                        if len(fNum) > 2:
                            fNum = fNum[:2]
                        elif len(fNum) == 1:
                            fNum += "0"

                        if int(fNum) > 0:
                            if int(value[0]) > 0:
                                result.append('and')

                            result += convertHundred(int(fNum))
                            result.append('cents')

                elif currency == 'KHR':
                    result.append(' riels')

                elif currency == 'THB':
                    result.append(' baht')
            else:

                if int(value[0]) == 0:
                    result.append(num_en['0'])

                # check if have floating point
                if len(value) == 2:
                    fNum = value[1]
                    if len(value[1]) > 2:
                        fNum = fNum[:2]

                    if int(fNum) > 0:
                        result.append(num_en['.'])
                        if fNum[0] == '0':
                            result.append(num_en['0'])
                        result += convertHundred(int(fNum))

            return " ".join(result)

        else:
            print "number is too big, can not convert."

    #EOF lang = en
    elif lang == 'kh':

        value = number.split('.')

        num = value[0]

        # count number
        numLength = len(num)
        index = 1

        while index <= numLength:

            if index == 1 and int(num[-index]) > 0:

                result.insert(0, num_kh[num[-index]])

            elif index == 2 and int(num[-index]) > 0:

                result.insert(0, num_kh[num[-index] + '0'])

            elif index == 3 and int(num[-index]) > 0:

                result.insert(0, num_kh['H'])
                result.insert(0, num_kh[num[-index]])

            elif index == 4 and int(num[-index]) > 0:

                result.insert(0, num_kh['K'])
                result.insert(0, num_kh[num[-index]])

            elif index == 5 and int(num[-index]) > 0:

                result.insert(0, num_kh['10K'])
                result.insert(0, num_kh[num[-index]])

            elif index == 6 and int(num[-index]) > 0:

                result.insert(0, num_kh['100K'])
                result.insert(0, num_kh[num[-index]])

            elif index == 7:

                if numLength < 9:

                    convHun = convertHundred(num[-numLength-1:-index+1], lang)
                    if len(convHun) > 0:
                        convHun.append(num_kh['M'])
                        result = convHun + result
                else:

                    convHun = convertHundred(num[(-index-2):-index+1], lang)
                    if len(convHun) > 0:
                        convHun.append(num_kh['M'])
                        result = convHun + result

            elif index == 10:
                convHun = convertHundred(num[-numLength-1:-index+1], lang)
                if len(convHun) > 0:
                    convHun.append(num_kh['B'])
                    result = convHun + result

            index += 1


        if currency is not None:

            if currency == 'USD':

                if int(num) > 0:
                    result.append(u' ដុល្លារអាមេរិក')

                if len(value) == 2:
                    fNum = value[1]
                    if len(fNum) > 2:
                        fNum = fNum[:2]
                    elif len(fNum) == 1:
                        fNum += "0"

                    if int(fNum) > 0:
                        result.append(' ')
                        result += convertHundred(fNum, lang)

                        result.append(u' សេន')
            elif currency == 'KHR':
                if int(num) > 0:
                    result.append(u' រៀល')

            elif currency == 'THB':
                if int(num) > 0:
                    result.append(u' បាត')
        else:

            if int(num) == 0:
                result.append(num_kh['0'])

            if len(value) == 2:
                fNum = value[1]
                if len(fNum) > 2:
                    fNum = fNum[:2]

                if int(fNum) > 0:
                    result.append(num_kh['.'])
                    if fNum[0] == '0':
                        result.append(num_kh['0'])
                    result += convertHundred(fNum, lang)

        return "".join(result)

def alterModel(Models):
    try:
        
        for Model in Models:
            
            ModelName = Model().__class__.__name__
            
            if ModelName[-5:] in ["_INAU", '_HIST']:
                ModelName = ModelName.replace(ModelName[-5:],"")

            Records =   MKT_ALTERTABLE.query.filter(MKT_ALTERTABLE.TableName == ModelName)

            for Row in Records:

                # check if field already exists in Model
                # if true, skip
                # false, add field to Model
                if hasattr(Model, Row.FieldName):
                    continue
                else:
                    DataType = Row.DataType
                    if Row.DataType in ["String", "Numeric"]:
                        DataType = eval("db.%s(%s)" % (Row.DataType, Row.Length))

                    else:
                        DataType = eval("db.%s" % Row.DataType)
                    
                    setattr(Model, Row.FieldName, db.Column(DataType))

        return Models
    except Exception, e:
        raise

def getAlterFormData(FormName):
    Result = db.session.query(
                                MKT_ALTERFORM.ID,
                                MKT_ALTERFORM_FIELD.FieldID,
                                MKT_ALTERFORM_FIELD.FieldLabel,
                                MKT_ALTERFORM_FIELD.Description,
                                MKT_ALTERFORM_FIELD.IsRequired,
                                MKT_ALTERFORM_FIELD.Type,
                                MKT_ALTERFORM_FIELD.DefaultValue,
                                MKT_ALTERFORM_FIELD.ListValue).\
                            join(MKT_ALTERFORM_FIELD, MKT_ALTERFORM_FIELD.ID == MKT_ALTERFORM.ID).\
                            filter(
                                MKT_ALTERFORM.Form == FormName
                            )

    return Result

class clsLoadQuery(object):
    pass
def getDefault(ID='',TableName=''):
    try:
        Module      =   __import__("app.urlregister")
        RecordObj   =   getattr(Module, TableName)
        Obj         = RecordObj.filter(RecordObj.ID==str(ID)).first()
        return Obj
    except Exception, e:
        return ''

def alterForm(Objform):
    try:
        
        Form = Objform
        FormName = Form.__name__

        # get form field from database
        AlterFormFieldObj = getAlterFormData(FormName)
        for row in AlterFormFieldObj:
            FieldLabel  = row.FieldLabel
            Validations = []
            if row.IsRequired == 'Y':
                FieldLabel = requiredlabel(FieldLabel, "*")
                Validations.append(validators.Required())  
            # DateField require validation optional when it is not require field
            if row.Type == 'DateField': 
                Validations.append(validators.Optional())  

            if row.Type in ['TextField', 'TextAreaField', 'DateField']:
                Args    =   {'default':row.DefaultValue, 'validators':Validations}

                if row.Description != "":
                    Args.update({'description':row.Description})
                    
                field = eval('%s(FieldLabel, **Args)'%row.Type)

            elif row.Type == 'SelectField':
                ValueObj = row.ListValue.split('*')
                try:
                    # code for load data from table
                    Module      =   __import__("app.urlregister")
                    RecordObj   =   getattr(Module, ValueObj[0])
                    TableQuery  =   True
                    
                    def createDynamicFunction(cls,RecordObj,FieldID, ValueObj):
                        BranchFilter    =   ValueObj[2] if len(ValueObj) > 2 else 'No'
                        if BranchFilter == 'Yes':
                            Branch      =   mktuser.getCurrentBranch()
                            Obj         =   RecordObj.query.filter(RecordObj.Branch == Branch)
                        else:
                            Obj         =   RecordObj.query
                        
                        def tempFunction(self):
                            return Obj

                        tempFunction.__doc__ = "docstring for getData%s" % FieldID
                        tempFunction.__name__ = "getData%s" % FieldID
                        setattr(cls,tempFunction.__name__,tempFunction)

                    createDynamicFunction(clsLoadQuery, RecordObj, str(row.FieldID), ValueObj)
                    
                    LoadQuery   =   clsLoadQuery()
                    
                except Exception, e:
                    TableQuery  =   False

                if not TableQuery:
                    
                    choices = []
                    for item in row.ListValue.splitlines():
                        splitObj = item.split('*')
                        choices.append(('%s' % splitObj[0], '%s' %splitObj[1]))

                    Kwargs = {'choices' : choices, 'validators' : Validations}

                    if row.Description != "":
                        Kwargs.update({'description':row.Description})

                    if row.DefaultValue != '':
                        Kwargs.update({'default':row.DefaultValue})

                    field = SelectField(FieldLabel, **Kwargs)

                else:

                    List       =   row.DefaultValue.split('*')
                    BlankText  =   ''
                    DefaultID  =   ''

                    if List:
                        BlankText   =   List[0]

                    if len(List) > 1 :
                        DefaultID   =   List[1]
                        
                    AllowBlank      =   1 if BlankText else 0
                    DisplayFields   =   ValueObj[1].split()
                    DisplayLabel    =   'lambda a: a.ID'

                    if DisplayFields:
                        DisplayLabel    =   ''
                                  
                        for f in DisplayFields:
                            DisplayLabel += '+" "+ a.%s'%f if hasattr(RecordObj,f) else '+" %s "+'%f

                        DisplayLabel = 'lambda a:%s'%DisplayLabel.replace('"++"','').lstrip('+" "+')
                    
                    if row.Description != "":
                        field = QuerySelectField(FieldLabel,
                                                        query_factory = eval('LoadQuery.getData%s'%str(row.FieldID)),
                                                        get_label = eval(DisplayLabel),
                                                        allow_blank = bool(AllowBlank),
                                                        blank_text = BlankText,
                                                        description=row.Description, 
                                                        # default = eval('lambda:getDefault(%s,%s)'%(str(DefaultID),str(ValueObj[0]))),
                                                        validators= Validations)

                    else:
                        field = QuerySelectField(FieldLabel,
                                                        query_factory = eval('LoadQuery.getData%s'%str(row.FieldID)),
                                                        get_label = eval(DisplayLabel),
                                                        allow_blank = bool(AllowBlank),
                                                        blank_text = BlankText, 
                                                        # default = eval('lambda:getDefault(%s,%s)'%(str(DefaultID),str(ValueObj[0]))),
                                                        validators= Validations)

            # get alter field obj
            FieldObj = MKT_ALTERTABLE.query.get(row.FieldID)
            # add form field on the fly
            setattr(Form, FieldObj.FieldName, field)

        
        AlterFormObj = MKT_ALTERFORM.query.filter(MKT_ALTERFORM.Form == FormName).first()
        # Hide form field by overiding form methoth        
        if AlterFormObj and AlterFormObj.HiddenField:
            HiddenFieldList = AlterFormObj.HiddenField.splitlines()
            # remove empty list value from list
            HiddenFieldList = filter(None,HiddenFieldList)

            if HiddenFieldList:
                FormList = Form().setVisible()
                # merge list field to get unique field list value
                HiddenFieldList = list(set(FormList) | set(HiddenFieldList))
                # define new method to overide form method
                def setHiddenField(self):
                    return HiddenFieldList
                # overide setVisible method by setHiddenField method
                Form.setVisible = setHiddenField
        
        # Reorder Form Field order by reset creatin_counter attribute value
        if AlterFormObj and AlterFormObj.FieldListOrder:
            Counter = 1
            # reset all form field creation_counter attribute
            for f in Objform():
                eval('Form.%s'%f.name).creation_counter = Counter
                Counter += 1
                
            # reset all form field creation_counter attribute by FieldListOrder 
            for l in AlterFormObj.FieldListOrder.splitlines():
                if l.split():
                    print 'Counter',Counter,l
                    eval('Form.%s'%l).creation_counter = Counter
                    Counter += 1
            for f in Objform():
                print eval('Form.%s'%f.name).creation_counter,f.name


        return Form
    except Exception, e:
        raise 