'''
Created Date: 04 Sep 2015
Author: Sovankiry Rim

Modified by: Sovankiry
Modified Date: 30 Sep 2015
All Right Reserved Morakot Technology
Description : mktofficer refer to function officer , generate disburse sheet and disbursement.
'''

from app.mktcore.imports 	import *
from app import app,db
from sqlalchemy import or_
from decimal 					import *

import app.mktcore.autoid 		as mktautoid
import mktdate 					as mktdate
import user 					as mktuser
import mktsetting 				as mktsetting
import mktparam 				as mktparam
import mktdb 					as mktdb
import mktaudit 				as mktaudit
import mktaccounting 			as mktaccounting
import mktmoney 				as mktmoney
import collections

def getSearchOfficer():
	Branch = mktuser.getBranch(session["ChangeBranch"]).ID
	search = request.args.get('q')
	NAMES = []
	a = MKT_OFFICER.query.\
					filter(or_(or_(MKT_OFFICER.ID.like('%'+search.upper()+'%'), MKT_OFFICER.FirstName.like('%'+search+'%')), MKT_OFFICER.LastName.like('%'+search+'%'))).\
					filter(MKT_OFFICER.Branch == Branch).\
					all()
	for row in a:
		dic = { "id":row.ID, "text":row.ID + " - " + row.LastName + " " + row.FirstName }
		NAMES.append(dic)

	# app.logger.debug(NAMES)
	return jsonify(items = NAMES)

def getOfficerByBranch(BranchID=''):
	Branch = mktuser.getUser().Branch
	if  not BranchID:
		BranchID = Branch

	dic = {}
	result = MKT_OFFICER.query.filter_by(Branch = BranchID).all()
	for row in  result:
		dic[row.ID] = row.ID + ' - '+ row.LastName + ' ' +row.FirstName

	return jsonify(results=dic)
	
def getSearchOfficerByCurreny():
	Branch = mktuser.getCurrentBranch()
	search = str(request.args.get('q'))
	Currency = str(request.args.get('Currency'))
	NAMES = [{"id": "","text": ""}]
	if not search.isspace():
		search = search.strip()
		if not search is None:
			NAMES=[]
			Key 	= search[:2]
			KeyInt 	= search[:3]
			KeyInt 	= KeyInt[-1:]
			# Search by Customer ID
			if Key == "CU" and mkttool.isInteger(KeyInt):
				QueryObj = db.session.query(MKT_LOAN_CONTRACT.ID,
									MKT_CUSTOMER.FirstNameEn,
									MKT_CUSTOMER.LastNameEn).\
									filter(MKT_LOAN_CONTRACT.Branch==Branch).\
									filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
									filter(MKT_LOAN_CONTRACT.DisbursedStat=="Y").\
									filter(MKT_CUSTOMER.ID.like('%'+search.upper()+'%')).\
									filter(MKT_LOAN_CONTRACT.Currency==Currency).\
									join(MKT_CUSTOMER,MKT_CUSTOMER.ID==MKT_LOAN_CONTRACT.ContractCustomerID).limit(30).yield_per(100)
			# Search by Customer ID
			if Key == "CU" and mkttool.isInteger(KeyInt):
				QueryObj = db.session.query(MKT_LOAN_CONTRACT.ID,
										MKT_CUSTOMER.FirstNameEn,
										MKT_CUSTOMER.LastNameEn).\
										filter(MKT_LOAN_CONTRACT.Branch==Branch).\
										filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
										filter(MKT_LOAN_CONTRACT.DisbursedStat=="Y").\
										filter(MKT_LOAN_CONTRACT.ID.like('%'+search.upper()+'%')).\
										filter(MKT_LOAN_CONTRACT.Currency==Currency).\
										join(MKT_CUSTOMER,MKT_CUSTOMER.ID==MKT_LOAN_CONTRACT.ContractCustomerID).limit(30).yield_per(100)
			else:
				QueryObj = db.session.query(MKT_LOAN_CONTRACT.ID,
										MKT_CUSTOMER.FirstNameEn,
										MKT_CUSTOMER.LastNameEn).\
										filter(MKT_LOAN_CONTRACT.Branch==Branch).\
										filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
										filter(MKT_LOAN_CONTRACT.DisbursedStat=="Y").\
										filter(MKT_LOAN_CONTRACT.Currency==Currency).\
										filter(or_(or_(MKT_LOAN_CONTRACT.ID.like('%'+search.upper()+'%'), func.upper(MKT_CUSTOMER.FirstNameEn).like('%'+search.upper()+'%')), func.upper(MKT_CUSTOMER.LastNameEn).like('%'+search.upper()+'%'))).\
										join(MKT_CUSTOMER,MKT_CUSTOMER.ID==MKT_LOAN_CONTRACT.ContractCustomerID).limit(30).yield_per(100)
			for row in QueryObj:
				dic = { "id":row.ID, "text":row.ID + " - " + row.LastNameEn + " " + row.FirstNameEn }
				NAMES.append(dic)
	return jsonify(items = NAMES)
	
def loadOfficer(BranchID=''):
	Branch = mktuser.getBranch(session["ChangeBranch"]).ID
	if  not BranchID:
		BranchID = Branch
	
	Result = MKT_OFFICER.query.filter(MKT_OFFICER.Branch == str(BranchID)) # tablename.query

	return Result

def getActualAmount(DetailObj,LoanID,DisburseAmount):
	Amount = 0
	DisburseDetail 	= DetailObj.filter(MKT_DISBURSE_DE.LoanID==LoanID)
	SumActualAmount = 0
	AccID 			= ""
	for row in DisburseDetail:
		ActualAmount = float(row.ActualAmount)
		SumActualAmount+=ActualAmount
		AccID = row.Account
	
	Amount = DisburseAmount - SumActualAmount
	if Amount <0:
		Amount=0
	return Amount

def getDisburseSheet(AuthLevel,DisburseID,Officer,Currency,ValueDate="",Status=""):
	RecrodRow = []
	RecordCol = {}
	TotalDisbursement = 0
	TotalLoanContract = 0
	ValueDate=str(ValueDate).strip()
	AccObj = MKT_ACCOUNT.query
	CurrencyObj 	= mktmoney.getCurrencyObj(Currency)
	#Initialize Object Table
	if AuthLevel == 0:
		MainTable 	= MKT_DISBURSE
		DetailTable = MKT_DISBURSE_DE
	else :
		MainTable 		= MKT_DISBURSE_INAU
		DetailTable 	= MKT_DISBURSE_DE_INAU

	if Status == "Save":
		DisburseObj 		= MKT_DISBURSE.query.get(DisburseID)
		DisburseINAUObj 	= MKT_DISBURSE_INAU.query.get(DisburseID)

		if DisburseObj:
			MainTable 		= MKT_DISBURSE
			DetailTable 	= MKT_DISBURSE_DE

		if DisburseINAUObj:
			MainTable 		= MKT_DISBURSE_INAU
			DetailTable 	= MKT_DISBURSE_DE_INAU

		QueryObj = db.session.query(DetailTable.LoanID.label('ID'),
												DetailTable.Account,
												DetailTable.Amount,
												MKT_LOAN_CONTRACT.Currency,
												MKT_LOAN_CONTRACT.ValueDate,
												MKT_LOAN_CONTRACT.ContractCustomerID,
												MKT_CUSTOMER.FirstNameEn,
												MKT_CUSTOMER.LastNameEn).\
										filter(DetailTable.ID==DisburseID).\
										join(MKT_LOAN_CONTRACT,MKT_LOAN_CONTRACT.ID==DetailTable.LoanID).\
										join(MKT_ACCOUNT,MKT_ACCOUNT.ID==DetailTable.Account).\
										join(MKT_CUSTOMER,MKT_CUSTOMER.ID==MKT_LOAN_CONTRACT.ContractCustomerID).\
										order_by(MKT_LOAN_CONTRACT.ID.asc())
		
		for row in QueryObj:

			LoanID 			= str(row.ID)
			Customer 		= "%s - %s %s"%(row.ContractCustomerID,row.LastNameEn,row.FirstNameEn)
			Account 		= row.Account
			Currency 		= row.Currency
			ValueDate 		= row.ValueDate
			ActualAmount 	= float(row.Amount)
			AccFilter 		= AccObj.get(Account)
			if AccFilter:
				AvailableBal = float(AccFilter.AvailableBal)
				AvailableBal = mktmoney.toMoney(AvailableBal,mktmoney.getCurrencyObj(Currency))
			else:
				AvailableBal = 0
			if ActualAmount > 0:
				TotalLoanContract+=1
				TotalDisbursement+=ActualAmount
				RecordCol.update({'ID'			:LoanID})
				RecordCol.update({'Customer'	:Customer})
				RecordCol.update({'Account'		:Account})
				RecordCol.update({'Currency'	:Currency})
				RecordCol.update({'ValueDate'	:ValueDate})
				RecordCol.update({'Amount'		:mktmoney.toMoney(ActualAmount,mktmoney.getCurrencyObj(Currency))})
				RecordCol.update({'AvailableBal':AvailableBal})

				RecrodRow.append(RecordCol)
				RecordCol={}

	elif Status == "Hist":
		MainTable 		= MKT_DISBURSE_HIST
		DetailTable 	= MKT_DISBURSE_DE_HIST
		QueryObj = db.session.query(DetailTable.LoanID.label('ID'),
												DetailTable.Account,
												DetailTable.Amount,
												MKT_LOAN_CONTRACT.Currency,
												MKT_LOAN_CONTRACT.ValueDate,
												MKT_LOAN_CONTRACT.ContractCustomerID,
												MKT_CUSTOMER.FirstNameEn,
												MKT_CUSTOMER.LastNameEn).\
										filter(DetailTable.ID==DisburseID).\
										join(MKT_LOAN_CONTRACT,MKT_LOAN_CONTRACT.ID==DetailTable.LoanID).\
										join(MKT_ACCOUNT,MKT_ACCOUNT.ID==DetailTable.Account).\
										join(MKT_CUSTOMER,MKT_CUSTOMER.ID==MKT_LOAN_CONTRACT.ContractCustomerID).\
										order_by(MKT_LOAN_CONTRACT.ID.asc())
		
		for row in QueryObj:
			LoanID 			= str(row.ID)
			Customer 		= "%s - %s %s"%(row.ContractCustomerID,row.LastNameEn,row.FirstNameEn)
			Account 		= row.Account
			Currency 		= row.Currency
			ValueDate 		= row.ValueDate
			ActualAmount 	= float(row.Amount)
			AccFilter 		= AccObj.get(Account)
			if AccFilter:
				AvailableBal = float(AccFilter.AvailableBal)
				AvailableBal = mktmoney.toMoney(AvailableBal,mktmoney.getCurrencyObj(Currency))

			TotalLoanContract+=1
			TotalDisbursement+=ActualAmount
			RecordCol.update({'ID'			:LoanID})
			RecordCol.update({'Customer'	:Customer})
			RecordCol.update({'Account'		:Account})
			RecordCol.update({'Currency'	:Currency})
			RecordCol.update({'ValueDate'	:ValueDate})
			RecordCol.update({'Amount'		:mktmoney.toMoney(ActualAmount,mktmoney.getCurrencyObj(Currency))})
			RecordCol.update({'AvailableBal':AvailableBal})

			RecrodRow.append(RecordCol)
			RecordCol={}
	else:
		
		Branch = mktuser.getCurrentBranch()

		ValueDateRange = ValueDate.split()

		if len(ValueDateRange)==2:
			ColLeft, ColRight = ValueDateRange[0],ValueDateRange[1]
			Condition=[MKT_LOAN_CONTRACT.ValueDate.between(ColLeft, ColRight) ]
		else:
			Condition=[MKT_LOAN_CONTRACT.ValueDate==ValueDate]
		# print "ValueDate:%s."%ValueDate
		# print "Currency:%s"%Currency
		QueryObj = db.session.query(MKT_LOAN_CONTRACT.ID,
									MKT_LOAN_CONTRACT.Account,
									MKT_LOAN_CONTRACT.Disbursed,
									MKT_LOAN_CONTRACT.Currency,
									MKT_LOAN_CONTRACT.ValueDate,
									MKT_LOAN_CONTRACT.ContractCustomerID,
									MKT_CUSTOMER.FirstNameEn,
									MKT_CUSTOMER.LastNameEn).\
									filter(MKT_LOAN_CONTRACT.ContractOfficerID==Officer).\
									filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
									filter(MKT_LOAN_CONTRACT.DisbursedStat=="Y").\
									filter(MKT_ACCOUNT.Balance>0).\
									filter(MKT_ACCOUNT.Branch==Branch).\
									filter(MKT_LOAN_CONTRACT.Currency==Currency).\
									filter(*Condition).\
									join(MKT_CUSTOMER,MKT_CUSTOMER.ID==MKT_LOAN_CONTRACT.ContractCustomerID).\
									join(MKT_ACCOUNT,MKT_ACCOUNT.ID==MKT_LOAN_CONTRACT.Account).\
									order_by(MKT_LOAN_CONTRACT.ID.asc()).\
									yield_per(100)
	
		DetailObj = MKT_DISBURSE_DE.query
		for row in QueryObj:

			LoanID 			= str(row.ID)
			Customer 		= "%s - %s %s"%(row.ContractCustomerID,row.LastNameEn,row.FirstNameEn)
			Account 		= row.Account
			Currency 		= row.Currency
			ValueDate 		= row.ValueDate
			DisburseAmount 	= float(row.Disbursed)
			ActualAmount 	= getActualAmount(DetailObj,LoanID,DisburseAmount)
			AccFilter 		= AccObj.get(Account)
			if AccFilter:
				AvailableBal = float(AccFilter.AvailableBal)
				AvailableBal = mktmoney.toMoney(AvailableBal,mktmoney.getCurrencyObj(Currency))
			else:
				AvailableBal = 0
			# if ActualAmount > 0:
			TotalLoanContract+=1
			TotalDisbursement+=ActualAmount
			RecordCol.update({'ID'			:LoanID})
			RecordCol.update({'Customer'	:Customer})
			RecordCol.update({'Account'		:Account})
			RecordCol.update({'Currency'	:Currency})
			RecordCol.update({'ValueDate'	:ValueDate})
			RecordCol.update({'Amount'		:mktmoney.toMoney(ActualAmount,mktmoney.getCurrencyObj(Currency))})
			RecordCol.update({'AvailableBal':AvailableBal})

			RecrodRow.append(RecordCol)
			RecordCol={}
	# print RecrodRow
	TotalLoanContract 	= mktmoney.formatNumber(TotalLoanContract,1,0)
	TotalDisbursement 	= mktmoney.toMoney(TotalDisbursement,mktmoney.getCurrencyObj(Currency))
	OfficerBalance 		= 0
	MainObj 			= MainTable.query.get(DisburseID)
	if MainObj:
		AccObj 				= MKT_ACCOUNT.query.get(str(MainObj.OfficerAccount))
		if AccObj:
			OfficerBalance 		= float(AccObj.Balance)
			OfficerBalance 		= mktmoney.toMoney(OfficerBalance,mktmoney.getCurrencyObj(Currency))
	return {"ResultObj":RecrodRow,"Currency":Currency,"DisburseObj":MainObj,"TotalLoanContract":TotalLoanContract,"TotalDisbursement":TotalDisbursement,"OfficerBalance":OfficerBalance}

# Hot Field in Fund Withdrawal and Fund Deposit
def getOfficerAcount(Officer,Currency):
	
	TellerParam 	= mktparam.getTellerParam()
	Transaction 	= ""
	OfficerAccount 	= ""
	OfficerCategory = ""
	Message 		= ""
	if TellerParam:
		
		OfficerObj 		= MKT_OFFICER.query.get(Officer)
		if OfficerObj:
			UserID 		= OfficerObj.UserID
			UserObj  	= mktuser.getUser(UserID)
			DefaultBranch 	= mktuser.getCurrentBranch()
			if UserObj:
				CashAccountID  	= UserObj.CashAccount
				CashObj  		= MKT_CASH_ACCOUNT.query.get(CashAccountID)
				if CashObj:
					Type = CashObj.Type
					ListCashAccount = CashObj.Account.split()
					if Type == "W":
						OfficerCategory = TellerParam.WalletCategory
						AccID 	= "%s%s%s"	%(Currency,OfficerCategory,CashAccountID)
						print AccID
						AccObj 	= MKT_ACCOUNT.query.get(AccID)
						if AccObj:
							OfficerAccount = AccObj.ID
							if DefaultBranch != AccObj.Branch:
								Branch = MKT_BRANCH.query.get(DefaultBranch)
								Message = "Officer don't have Wallet Account in %s branch."%Branch.Description
						else:
							Message = "Wallet Account not found."
					else:
						Message = "Officer doesn't have Wallet Account."
				else:
					Message = "Cash Account #%s not found."%CashAccountID
			else:
				Message = "Officer don't have Wallet Account."
		else:
			Message = "Officer not found."
	else:
		Message = "Please set Teller Parameter first."
	if Message:
		app.logger.debug("Disbursement:%s"%Message)

	return jsonify(OfficerAccount=OfficerAccount,OfficerCategory=OfficerCategory,Message=Message)

def getDisburseAmount(Account):
	LoanObj = MKT_LOAN_CONTRACT.query.filter(MKT_LOAN_CONTRACT.Account==Account)


def insertDisbursementSheet(AuthLevel,DisburseID,kwargs):
	try:
	
		Officer 		= kwargs["Officer"]			if "Officer" 			in kwargs else ""
		Currency 		= kwargs["Currency"]		if "Currency" 			in kwargs else ""
		ValueDateStart  = kwargs["ValueDateStart"]	if "ValueDateStart" 	in kwargs else ""
		ValueDateEnd 	= kwargs["ValueDateEnd"]	if "ValueDateEnd" 		in kwargs else ""
		OfficerAccount 	= kwargs["OfficerAccount"]	if "OfficerAccount" 	in kwargs else ""
		OfficerCategory = kwargs["OfficerCategory"]	if "OfficerCategory" 	in kwargs else ""
		ValueDateRange 	= "%s %s"%(ValueDateStart,ValueDateEnd)
		Status 		= ""
		Message 	= ""

		Url = "/Morakot/GenerateDisbursement/Generate/%s/"%DisburseID
		Url+="?Officer=%s"%Officer
		Url+="&Currency=%s"%Currency
		Url+="&ValueDateStart=%s"%ValueDateStart
		Url+="&ValueDateEnd=%s"%ValueDateEnd
		Url+="&OfficerAccount=%s"%OfficerAccount
		Url+="&OfficerCategory=%s"%OfficerCategory
		Url+="&Save=Save"

		TotalDisbursement 	= 0
		Audit 			= mktaudit.getAuditrail()
		# Inputter 		= Audit['Inputter']
		# Createdon 		= Audit['Createdon']
		# Branch 			= Audit['Branch']
		# Authorizer 		= Audit['Authorizer']
		# Authorizeon		= Audit['Authorizeon']
		MainTableHIST 	= MKT_DISBURSE_HIST
		DetailTableHIST = MKT_DISBURSE_DE_HIST
		if AuthLevel == 1:
			Resource 	= "INAU"
			MainTable 	= MKT_DISBURSE_INAU
			DetailTable = MKT_DISBURSE_DE_INAU
			Audit.update({"Status":"INAU"})
			Audit.update({"Authorizer":""})
			Audit.update({"Authorizeon":""})
		else:
			Resource 		= "AUTH"
			MainTable 		= MKT_DISBURSE
			DetailTable 	= MKT_DISBURSE_DE
			

		if DisburseID:

			ListLoanID			= [str(kwargs[item]) for item in kwargs if "LoanID" in item]
			ListAmount 			= [item for item in kwargs if "Amount" in item]
			ListAccount 		= [str(kwargs[item]) for item in kwargs if "Account" in item]
			DuplicateListAccount= [item for item, count in collections.Counter(ListAccount).items() if count > 1]
			
			LoanObj 			= MKT_LOAN_CONTRACT.query
			AccObj 				= MKT_ACCOUNT.query
			InsertRecord 		= {}
			
			ListLoanID.sort()
			# Block validation
			if not Officer:
				Status = False

			if not Currency:
				Status = False

			if not ValueDateStart:
				Status = False

			if not OfficerAccount:
				Status = False

			if not OfficerCategory:
				Status = False

			# Before insert need clear record old.
			if Resource == "AUTH":
				mktaudit.moveAUTHDetailtoHIST(MainTable,DetailTable,DetailTableHIST,DisburseID)
			mktdb.deleteRecord(DetailTable,[DetailTable.ID==DisburseID])

			#Check Duplicate LoanID
			DuplicateLoan   = [x for x in ListLoanID if ListLoanID.count(x) > 1]
			if DuplicateLoan:
				Status = False
				Message = "Duplicate loan contract."
				return {"Status":Status,"Message":Message,"Url":Url}

			#Check  LoanID
			if len(ListLoanID)==0:
				Status = False
				Message = "Your generate disburse don't have any contract. "
				return {"Status":Status,"Message":Message,"Url":Url}
			# DetailObj = DetailTable.query
			LoanObj = MKT_LOAN_CONTRACT.query
				
			No = 1
			for row in ListLoanID:
				LoanID 		= row
				LoanFilter	= LoanObj.get(LoanID)
				Account 	= LoanFilter.Account
				Amount = float(kwargs['Amount_%s'%LoanID].replace(',',''))

				#Check Balance of Amount if one account has more loan
				if Account in DuplicateListAccount:
					DuplicateLoanObj	= LoanObj.filter(MKT_LOAN_CONTRACT.Account==Account)
					TotalAmountByAccount = 0
					AvailableBalByAccount = 0
					for item in DuplicateLoanObj:
						if 'LoanID_%s'%item.ID in kwargs:
							TotalAmountByAccount+=float(kwargs['Amount_%s'%item.ID].replace(',',''))
							AvailableBalByAccount=float(kwargs['AvailableBal_%s'%item.ID].replace(',',''))
					if TotalAmountByAccount >AvailableBalByAccount:
						getCurrencyObj = mktmoney.getCurrencyObj(item.Currency)
						Status = False
						Message = "Account#%s have doesn't have sufficient fund for total disbursement %s. Current balance is %s"%(Account,mktmoney.toMoney(TotalAmountByAccount,getCurrencyObj),mktmoney.toMoney(AvailableBalByAccount,getCurrencyObj))
						return {"Status":Status,"Message":Message,"Url":Url}
						
				if LoanFilter:
					DisburseAmount 	= float(LoanFilter.Disbursed)
					AccFilter 		= AccObj.get(LoanFilter.Account)
					if AccFilter:
						AccID 		= AccFilter.ID
						AccCategory = AccFilter.AccCategory
						Balance 	= float(AccFilter.Balance)
						if Amount > Balance:
							Status=False
							Message="Loan Contract %s amount more than available balances %s"%(LoanID,mktmoney.formatNumber(Balance,1,0))
							
							# return {"Status":Status,"Message":Message,"Url":Url}
						if Amount < 0 :
							Status=False
							Message="Amount must be more than zero."
							
							# return {"Status":Status,"Message":Message,"Url":Url}
						TotalDisbursement+=Amount
						InsertRecord = {	'ID'				:DisburseID,
											'DisburseID'		:No,
											'LoanID'			:LoanID,
											'Account'			:AccID,
											'AccCategory'		:AccCategory,
											'Amount'			:Amount,
											'ActualAmount' 		:Amount,
											'Reference' 		: ""}
						# Insert record to Disbursement sheet
						InsertRecord.update(Audit)
						mktdb.insertTable(DetailTable,InsertRecord)
						No+=1
					else:
						Message="Account #%s not found in loan contract #%s."(LoanFilter.Account,LoanID)
						
				else:
					
					Message="Loan Contract #%s not found"%LoanID
					
		Status = True
		if Message:
			Status=False
			return {"Status":Status,"Message":Message,"Url":Url}
		else:
			# Before insert need clear record old.
			if Resource == "INAU":
				CheckRestoreID = mktaudit.isRestoreID(MKT_DISBURSE,MKT_DISBURSE_INAU,MKT_DISBURSE_HIST,DisburseID)
				if CheckRestoreID[0]:
					Curr=CheckRestoreID[1]
				else:
					Curr = "0"
			else:
				mktaudit.moveAUTHtoHIST(MainTable,MainTableHIST,DisburseID)
				Curr = mktaudit.getCurrRecord(MainTable,[MainTable.ID==DisburseID])
				CheckRestoreID = mktaudit.isRestoreID(MKT_DISBURSE,MKT_DISBURSE_INAU,MKT_DISBURSE_HIST,DisburseID)
				if CheckRestoreID[0]:
					Curr=CheckRestoreID[1]

			mktaudit.deleteAUTH(MainTable, DisburseID)
			InsertRecord = {
							'Curr'				:Curr,
							'Status'			:Resource,
							'ID'				:DisburseID,
							'Officer' 			:Officer,
							'OfficerAccount'	:OfficerAccount,
							'OfficerCategory'	:OfficerCategory,
							'TotalDisbursement'	:TotalDisbursement,
							'Currency'			:Currency,
							'ValueDate'			:ValueDateRange,
							'OptionGroupby'		:"",
							'TotalActualAmount'	:0,
							'DisbursedStatus'	:"N"}
			# Update TotalDisburse
			mktdb.insertTable(MainTable,InsertRecord)
			if Resource == "AUTH":
				Message = "Record was added successfully, record id: %s"%DisburseID
			else:
				Message = "Record was added successfully, record id: %s in INAU "%DisburseID
			
			Url="/Morakot/GenerateDisbursement"

		return {"Status":Status,"Message":Message,"Url":Url}
		
	except Exception, e:
		Officer 		= kwargs["Officer"]			if "Officer" 			in kwargs else ""
		Currency 		= kwargs["Currency"]		if "Currency" 			in kwargs else ""
		ValueDateStart  = kwargs["ValueDateStart"]	if "ValueDateStart" 	in kwargs else ""
		ValueDateEnd 	= kwargs["ValueDateEnd"]	if "ValueDateEnd" 		in kwargs else ""
		OfficerAccount 	= kwargs["OfficerAccount"]	if "OfficerAccount" 	in kwargs else ""
		OfficerCategory = kwargs["OfficerCategory"]	if "OfficerCategory" 	in kwargs else ""
		Url = "/Morakot/GenerateDisbursement/Generate/%s/"%DisburseID
		Url+="?Officer=%s"%Officer
		Url+="&Currency=%s"%Currency
		Url+="&ValueDateStart=%s"%ValueDateStart
		Url+="&ValueDateEnd=%s"%ValueDateEnd
		Url+="&OfficerAccount=%s"%OfficerAccount
		Url+="&OfficerCategory=%s"%OfficerCategory
		Url+="&Save=Save"
		Url= ""
		Status= False
		Message=msg_error+" %s"%e
		db.session.rollback()
		db.session.close()
		return {"Status":Status,"Message":Message,"Url":"Url"}

def setAuthorizeDisbursementSheet(AuthLevel,ID,Status,Inputter,Createdon,Branch):
	try:
		# Authorize have two transcations INAU, RNAU 
		# print 'beforeAuthorize: %s'% AuthLevel
		Message = ""
		if AuthLevel == 1:

			Audit 		= mktaudit.getAuditrail()
			Authorizer 	= Audit['Authorizer']
			Authorizeon	= Audit['Authorizeon']

			# Cannot authorize by urseft
			if Inputter != Authorizer:

				if Status == 'RNAU':

					# Remove to history 
					mktaudit.moveAUTHtoHIST(MKT_DISBURSE,MKT_DISBURSE_HIST,ID)
					# Delete live record and Un-authorize
					mktaudit.deleteAUTH(MKT_DISBURSE, ID)
					mktaudit.deleteAUTH(MKT_DISBURSE_INAU, ID)

				
					# Remove account to history 
					mktaudit.moveAUTHDetailtoHIST(MKT_DISBURSE,MKT_DISBURSE_DE,MKT_DISBURSE_DE_HIST,ID)
				
					# Delete Detail live record and Un-authorize
					mktaudit.deleteAUTH(MKT_DISBURSE_DE, ID)
					mktaudit.deleteAUTH(MKT_DISBURSE_DE_INAU, ID)
					
					# Message = "%s Record was authorized for reverse successfully."%ID
					Message="The record was authorized successfully for reversed, record id:%s"%ID
				else:
					DisburseObj = MKT_DISBURSE.query.get(ID)
					if DisburseObj:
						# Authorize Edit record
						#Add to hist Detail
						mktaudit.moveAUTHDetailtoHIST(MKT_DISBURSE,MKT_DISBURSE_DE,MKT_DISBURSE_DE_HIST,ID)
						mktaudit.deleteAUTH(MKT_DISBURSE_DE, ID)

						#Add to hist Main
						mktaudit.moveAUTHtoHIST(MKT_DISBURSE,MKT_DISBURSE_HIST,ID)
						Curr = mktaudit.getCurrRecord(MKT_DISBURSE,[MKT_DISBURSE.ID==ID])
						mktaudit.deleteAUTH(MKT_DISBURSE, ID)

						#Add INAU to LIVE
						mktaudit.moveINAUtoAUTH(MKT_DISBURSE, MKT_DISBURSE_INAU, ID, Inputter, Createdon, Authorizer, Authorizeon,"",Curr)
						mktaudit.deleteAUTH(MKT_DISBURSE_INAU, ID)

			
						#Add INAU to LIVE
						mktaudit.moveINAUtoAUTH(MKT_DISBURSE_DE, MKT_DISBURSE_DE_INAU, ID, Inputter, Createdon, Authorizer, Authorizeon,"",Curr)
						mktaudit.deleteAUTH(MKT_DISBURSE_DE_INAU, ID)

					else:	
						#Add Live 
						mktaudit.moveINAUtoAUTH(MKT_DISBURSE, MKT_DISBURSE_INAU, ID, Inputter, Createdon, Authorizer, Authorizeon)
						#Delete INAU for Main Table
						mktaudit.deleteAUTH(MKT_DISBURSE_INAU, ID)

						#Add to delete old Detail
						mktaudit.deleteAUTH(MKT_DISBURSE_DE, ID)
						#Add to live Detail
						mktaudit.moveINAUtoAUTH(MKT_DISBURSE_DE, MKT_DISBURSE_DE_INAU, ID, Inputter, Createdon, Authorizer, Authorizeon)
						#Delete INAU for Detail Table
						mktaudit.deleteAUTH(MKT_DISBURSE_DE_INAU, ID)

					Message="The record was authorized successfully, record id:%s"%ID
			else:
				Message = msg_error+msg_cannot_authorize
				return False,Message

		return True,Message
	except Exception, e:
		db.session.rollback()
		return False,msg_error+" %s"%e


def setReverseDisbursementSheet(AuthLevel,ID):

	try:
		Message 	= ""
		Audit 		= mktaudit.getAuditrail()
		Inputter 	= Audit['Inputter']
		Createdon	= Audit['Createdon']
		Status 		= "RNAU"
		DisburseMain = MKT_DISBURSE_INAU.query.get(ID)
		if DisburseMain:
			Message = msg_error+" The record was already reversed and in list un-authorize. Cannot reverse this record"
			return False,Message
		DisburseObj = MKT_DISBURSE.query.get(ID)
		if DisburseObj:
			DisbursedStatus = DisburseObj.DisbursedStatus
			if DisbursedStatus == "Y":
				Message = msg_error+" The record was disbursemented already. Cannot reverse this record"
				return False,Message

		if AuthLevel == 1:
			moveAUTHtoINAU(MKT_DISBURSE,MKT_DISBURSE_INAU,ID,Inputter,Createdon, Status)
			moveAUTHtoINAU(MKT_DISBURSE_DE,MKT_DISBURSE_DE_INAU,ID,Inputter,Createdon, Status)
			Message="The record was reversed successfully, record id:%s in INAU."%ID
		else:
			#Remove Main to history 
			mktaudit.moveAUTHtoHIST(MKT_DISBURSE,MKT_DISBURSE_HIST,ID)
			# Delete live record
			mktaudit.deleteAUTH(MKT_DISBURSE, ID)

			#Remove Detail to history 
			mktaudit.moveAUTHDetailtoHIST(MKT_DISBURSE,MKT_DISBURSE_DE,MKT_DISBURSE_DE_HIST,ID)
			# Delete live record
			mktaudit.deleteAUTH(MKT_DISBURSE_DE, ID)
			Message="The record was reversed successfully, record id:%s"%ID
		
		return True,Message

	except Exception, e:
		return False,msg_error+" %s"%e

def setDeleteDisbursement(DisburseID):
	try:

		DisburseObj  = MKT_DISBURSE.query.get(DisburseID)
		DisburseINAUObj = MKT_DISBURSE_INAU.query.get(DisburseID)
		if DisburseObj:
			Status = DisburseObj.Status
			if Status == "AUTH":
				if not DisburseINAUObj:
					return False,msg_warning+"The record was authorized and could not be deleted."
		
		if DisburseINAUObj:
			Status = DisburseINAUObj.Status
			if Status in ["INAU","RNAU"] :
				mktdb.deleteRecord(MKT_DISBURSE_INAU,[MKT_DISBURSE_INAU.ID==DisburseID])
				mktdb.deleteRecord(MKT_DISBURSE_DE_INAU,[MKT_DISBURSE_DE_INAU.ID==DisburseID])
				return True,"The record was deleted successfully, record id:%s"%DisburseID
			else:
				return False,msg_error+" %s"%msg_reverse_0
		else:
			return False," %s"%msg_not_found_0
	except Exception, e:
		return False,msg_error+" %s"%e

def moveAUTHtoINAU(LIVE,INAU,ID,Inputter='',Createdon='', Status='',FilterField="",Accounting="N"):

	try:

		dic 			= {}
		FieldInTable 	= []
		Mapper 			= inspect(LIVE)
		for item in Mapper.attrs:						
			FieldInTable.append(item.key)
		if FilterField:
			QueryObj = INAU.query.filter(getattr(INAU, FilterField) == ID)
		else:
			QueryObj = LIVE.query.filter(LIVE.ID==ID)
		if QueryObj :
			for row in QueryObj:
				for col in FieldInTable:
					dic.update({col:getattr(row,col)})
				dic.update({'Status':Status})
				dic.update({'ID':ID})
				dic.update({'Inputter':Inputter})
				dic.update({'Createdon':Createdon})
				dic.update({'Authorizer':''})
				dic.update({'Authorizeon':''})
				AddINAUObj = INAU(**dic)
				db.session.add(AddINAUObj)
	except Exception, e:
		db.session.rollback()
		raise

#----------------Block Disbursement-------------------

def getListLive():
	Condition 	= []
	CurrentBranch = mktuser.getCurrentBranch()
	AccessBranch 	=	MKT_ACCESSBRANCH.query.filter(MKT_ACCESSBRANCH.Model=="MKT_DISBURSE").first()
	if AccessBranch:

		Condition.append([MKT_DISBURSE.Branch==CurrentBranch])
	QueryObj = MKT_DISBURSE.query.filter(MKT_DISBURSE.DisbursedStatus.in_(["Y"])).\
										filter(*Condition)
	return QueryObj

def getListAuth():

	Condition 	= []
	CurrentBranch = mktuser.getCurrentBranch()
	AccessBranch 	=	MKT_ACCESSBRANCH.query.filter(MKT_ACCESSBRANCH.Model=="MKT_DISBURSE").first()
	if AccessBranch:

		Condition.append([MKT_DISBURSE_INAU.Branch==CurrentBranch])
	QueryObj = MKT_DISBURSE_INAU.query.filter(MKT_DISBURSE_INAU.DisbursedStatus=="I").\
										filter(*Condition)
	return QueryObj

def getDisbursementSheet(DisburseID,Action=""):

	Status 			= False
	Message 		= ""
	QueryObj		= []
	OfficerObj		= []
	OfficerBalance 	= 0
	TotalDisburse 	= 0
	TotalActual 	= 0
	CountLoan 		= 0
	Currency 		= ""
	OfficerAccount  = ""
	CurrentBranch 		= mktuser.getCurrentBranch()
	DisburseObj 		= MKT_DISBURSE.query.get(DisburseID)
	DisburseINAUObj 	= MKT_DISBURSE_INAU.query.get(DisburseID)

		
	if DisburseObj:
		MainTable 		= MKT_DISBURSE
		DetailTable 	= MKT_DISBURSE_DE
		if Action == "Edit":
			DisbursedStatus = str(DisburseObj.DisbursedStatus) if DisburseObj else ""
			if DisbursedStatus == "N" and not DisburseINAUObj:
				Message = msg_warning+"Record not found..."
				return {"Status":False,"Message":Message}
			elif DisburseObj.DisbursedStatus == "Y":
				Message = msg_warning+"The record was authorized and could not be edited."
				return {"Status":False,"Message":Message}
				

	if DisburseINAUObj:
		MainTable 		= MKT_DISBURSE_INAU
		DetailTable 	= MKT_DISBURSE_DE_INAU

		if Action == "New":
			Message = msg_warning+" %s exists in list un-authorize record."%DisburseID
			return {"Status":False,"Message":Message}

		if Action == "Search":
			DisburseObj = DisburseINAUObj
		if Action == "Edit":

			DisburseObj = DisburseINAUObj


	if DisburseObj:

		Status 		= True
		Officer 	= DisburseObj.Officer
		Currency 	= DisburseObj.Currency
		ValueDate 	= DisburseObj.ValueDate
		DisbursedStatus= str(DisburseObj.DisbursedStatus)
		QueryObj 	= db.session.query(DetailTable.LoanID.label('ID'),
										DetailTable.Account,
										DetailTable.Amount,
										DetailTable.Reference,
										MKT_ACCOUNT.Currency,
										DetailTable.ActualAmount,
										MKT_LOAN_CONTRACT.ValueDate,
										MKT_LOAN_CONTRACT.ContractCustomerID,
										MKT_CUSTOMER.FirstNameEn,
										MKT_CUSTOMER.LastNameEn).\
									filter(DetailTable.ID==DisburseID).\
									join(MKT_LOAN_CONTRACT,MKT_LOAN_CONTRACT.ID==DetailTable.LoanID).\
									join(MKT_ACCOUNT,MKT_ACCOUNT.ID==DetailTable.Account).\
									join(MKT_CUSTOMER,MKT_CUSTOMER.ID==MKT_LOAN_CONTRACT.ContractCustomerID)
		OfficerAccount 	= DisburseObj.OfficerAccount
		OfficerObj		= MKT_OFFICER.query.get(Officer)
		TotalDisburse 	= sum(float(c.Amount) for c in QueryObj)
		TotalActual 	= sum(float(c.ActualAmount) for c in QueryObj)
		AccObj 			= MKT_ACCOUNT.query.get(DisburseObj.OfficerAccount)
		OfficerBalance 	= AccObj.Balance
		CountLoan 		= mktmoney.formatNumber(QueryObj.count(),1,0)
		
		Dic ={"Status":Status,
			"QueryObj":QueryObj,
			"DisburseObj":DisburseObj,
			"OfficerObj":OfficerObj,
			"OfficerAccount":OfficerAccount,
			"OfficerBalance":OfficerBalance,
			"TotalDisburse":TotalDisburse,
			"TotalActual":TotalActual,
			"Currency":Currency,
			"DisbursedStatus":DisbursedStatus,
			"CountLoan":CountLoan}
	else:
		Status = False
		Message= msg_not_found_0
		Dic={"Status":Status,
			"Message":Message,
			"QueryObj":QueryObj,
			"DisburseObj":DisburseObj,
			"OfficerObj":OfficerObj,
			"OfficerAccount":OfficerAccount,
			"OfficerBalance":OfficerBalance,
			"TotalDisburse":TotalDisburse,
			"TotalActual":TotalActual,
			"Currency":Currency,
			"DisbursedStatus":"",
			"CountLoan":CountLoan}
	return Dic

def setSerachDisbursement(DisburseID):

	return getDisbursementSheet(DisburseID)

	
def setDisbursement(AuthLevel,DisburseMain,DisburseDetail,DisburseID,DicLoan):
	
	try:
		Transaction  		= mktsetting.getAccSetting().DisbursedTran if mktsetting.getAccSetting() else ""
		TranDate 			= str(mktdate.getBankDate())
		Audit 				= mktaudit.getAuditrail()
		Inputter 			= Audit['Inputter']
		Createdon 			= Audit['Createdon']
		Authorizer 			= Audit['Authorizer']
		Authorizeon			= Audit['Authorizeon']
		Branch 				= Audit['Branch']
		TotalActualAmount 	= 0
		DicReference 		= {}
		if AuthLevel == 0:

			#Get Reference ID for FT
			for row in DicLoan:
				#Function getAutoID has db.session.commit so we need to get first before submit
				DicReference.update({row:mktautoid.getAutoID('FRM_FUND_TRANSFER')})

			#Block Update Disbursement status
			mktdb.updateRecord(MKT_DISBURSE,
						[MKT_DISBURSE.ID==DisburseID],
						{"DisbursedStatus":"Y"})
				
			#Block PostAccounting and Actual Amount
			for row in DicLoan:

				LoanID 			= row
				ActualAmount 	= DicLoan[row]

				if ActualAmount > 0:

					FilterDisburse = DisburseDetail.filter(MKT_DISBURSE_DE.LoanID == LoanID).first()

					if FilterDisburse:
						# Debit Account 
						DrAcc 	= FilterDisburse.Account
						DrCat 	= FilterDisburse.AccCategory
						DrCur 	= DisburseMain.first().Currency
						Amount 	= float(FilterDisburse.Amount)
						# Credit Co Account
						CrAcc 	= DisburseMain.first().OfficerAccount
						CrCat 	= DisburseMain.first().OfficerCategory
						CrCur 	= DisburseMain.first().Currency
						Note  	= "Generate Disbursement"
						Module 	= "FT"
						Reference = DicReference[LoanID]
						# Validation Max Min Balance
						CheckBalanceDrAcc = mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(ActualAmount), "Dr")
						if CheckBalanceDrAcc:
							return False,CheckBalanceDrAcc

						CheckBalanceCrAcc = mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(ActualAmount), "Cr")
						if CheckBalanceCrAcc:								
							return False,CheckBalanceCrAcc
								
						if ActualAmount > Amount:

							return False,"Loan Contract #%s actual amount %s is more than loan amount."% (LoanID,mktmoney.formatNumber(ActualAmount,1,0))

						TotalActualAmount+=float(ActualAmount)
						#Block Update Disbursement status
						mktdb.updateRecord(MKT_DISBURSE_DE,
											[MKT_DISBURSE_DE.LoanID==LoanID],
											{"Reference":Reference,
											"ActualAmount":Decimal(ActualAmount)})
						#Block Comming Accounting
						CheckAccounting = setDisburseCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Decimal(ActualAmount),Module,Transaction,TranDate,Reference,Note,DisburseID)
						if not CheckAccounting[0]:
							return False,CheckAccounting[1]
					else:
						return False,"Generate Disbursement don't have loan #%s"%LoanID
				else:
					return False,"Actual amount for loan contract #%s must be more than zero."%LoanID
			#Block Update Total Actual Amount
			mktdb.updateRecord(MKT_DISBURSE,
						[MKT_DISBURSE.ID==DisburseID],
						{"TotalActualAmount":TotalActualAmount})

			return True,"%s was disbursemented successfully."%DisburseID

		elif AuthLevel == 1:
			# Before insert need clear record old.
			mktdb.deleteRecord(MKT_DISBURSE_INAU,[MKT_DISBURSE_INAU.ID==DisburseID])
			mktdb.deleteRecord(MKT_DISBURSE_DE_INAU,[MKT_DISBURSE_DE_INAU.ID==DisburseID])
			Status 			= "INAU"
			#Block insert Detail Table
			dic 			= {}
			FieldInTable 	= []
			Mapper 			= inspect(MKT_DISBURSE_DE_INAU)
			for item in Mapper.attrs:						
				FieldInTable.append(item.key)
			#loop Detail value
			for row in DisburseDetail:
				for col in FieldInTable:
					dic.update({col:getattr(row,col)})
				#overwite value

				LoanID 			= row.LoanID
				ActualAmount 	= DicLoan[LoanID]
				Amount 			= float(row.Amount)

				# Debit DDA Account 
				DrAcc 			= row.Account
				DrCat 			= row.AccCategory
				DrCur 			= DisburseMain.first().Currency

				# Credit Co Account
				CrAcc 			= DisburseMain.first().OfficerAccount
				CrCat 			= DisburseMain.first().OfficerCategory
				CrCur 			= DisburseMain.first().Currency

				if ActualAmount > 0:

					# Validation Max Min Balance
					CheckBalanceDrAcc = mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(ActualAmount), "Dr")
					if CheckBalanceDrAcc:
						return False,CheckBalanceDrAcc

					CheckBalanceCrAcc = mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(ActualAmount), "Cr")
					if CheckBalanceCrAcc:
						return False,CheckBalanceCrAcc
						
				if ActualAmount > Amount:
					return False,"Loan Contract #%s actual amount %s is more than loan amount."% (LoanID,mktmoney.formatNumber(ActualAmount,1,0))

				TotalActualAmount+=ActualAmount
				dic.update({'ActualAmount':ActualAmount})
				dic.update({'Status':Status})
				dic.update({'Inputter':Inputter})
				dic.update({'Createdon':Createdon})
				dic.update({'Authorizer':''})
				dic.update({'Authorizeon':''})

				AddINAUObj = MKT_DISBURSE_DE_INAU(**dic)
				db.session.add(AddINAUObj)

			#Block insert Main Table
			dic 			= {}
			FieldInTable 	= []
			Mapper 			= inspect(MKT_DISBURSE_INAU)
			for item in Mapper.attrs:						
				FieldInTable.append(item.key)
			for row in DisburseMain:
				for col in FieldInTable:
					dic.update({col:getattr(row,col)})
				#overwite value
				dic.update({'Status':Status})
				dic.update({'Inputter':Inputter})
				dic.update({'Createdon':Createdon})
				dic.update({'Authorizer':''})
				dic.update({'Authorizeon':''})
				#For TotalActualAmount When finish insert to INAU TotalActualAmount has been update finish value
				dic.update({'TotalActualAmount':TotalActualAmount}) 
				dic.update({'DisbursedStatus':"I"})
				AddINAUObj = MKT_DISBURSE_INAU(**dic)
				db.session.add(AddINAUObj)

			return True,"Record was added successfully, record id: %s in INAU"%DisburseID
		else:
			return False," %s NAuthorize not found"%AuthLevel

	except Exception, e:
		raise
		return False," %s"%e
		
def setAuthorizeDisbursement(DisburseID):
	try:
		Message 			= ""

		Audit 				= mktaudit.getAuditrail()
		Authorizer 			= Audit['Authorizer']
		Authorizeon			= Audit['Authorizeon']

		DisburseObj 		= MKT_DISBURSE_INAU.query.get(DisburseID)
		DisburseDetail 		= MKT_DISBURSE_DE_INAU.query.filter(MKT_DISBURSE_DE_INAU.ID==DisburseID)
		DicReference		= {}
		Transaction  		= mktsetting.getAccSetting().DisbursedTran if mktsetting.getAccSetting() else ""
		TranDate 			= str(mktdate.getBankDate())
		TotalActualAmount 	= 0
		if DisburseObj:
			Inputter 	= DisburseObj.Inputter
			Createdon 	= DisburseObj.Createdon
			Branch 		= DisburseObj.Branch

			if 	Inputter != Authorizer:
				#Get Reference ID for FT
				for row in DisburseDetail:
					#Function getAutoID has db.session.commit so we need to get first before submit
					DicReference.update({str(row.LoanID):mktautoid.getAutoID('FRM_FUND_TRANSFER')})

				#Block PostAccounting and Actual Amount
				for row in DisburseDetail:

					LoanID 			= str(row.LoanID)
					ActualAmount 	= float(row.ActualAmount)
					TotalActualAmount+ActualAmount
					if ActualAmount > 0:

						# Debit Account 
						DrAcc 	= row.Account
						DrCat 	= row.AccCategory
						DrCur 	= DisburseObj.Currency

						# Credit Co Account
						CrAcc 	= DisburseObj.OfficerAccount
						CrCat 	= DisburseObj.OfficerCategory
						CrCur 	= DisburseObj.Currency
						Note  	= "Generate Disbursement"
						Module 	= "FT"
						Reference = DicReference[LoanID]
						# Validation Max Min Balance
						CheckBalanceDrAcc = mktaccounting.checkMaxMinBalance(DrAcc, DrCur, Decimal(ActualAmount), "Dr")
						if CheckBalanceDrAcc:
							return False,msg_error+CheckBalanceDrAcc

						CheckBalanceCrAcc = mktaccounting.checkMaxMinBalance(CrAcc, CrCur, Decimal(ActualAmount), "Cr")
						if CheckBalanceCrAcc:								
							return False,msg_error+CheckBalanceCrAcc
						
						#Block Comming Accounting
						CheckAccounting = setDisburseCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Decimal(ActualAmount),Module,Transaction,TranDate,Reference,Note,DisburseID)		
						if not CheckAccounting[0]:
							return False,msg_error+CheckAccounting[1]
					else:
						Reference = ""
					#Block Update Disbursement status
					mktdb.updateRecord(MKT_DISBURSE_DE,
										[MKT_DISBURSE_DE.LoanID==LoanID],{
										'Inputter' 		:	Inputter,
										'Createdon' 	: 	Createdon,
										'Authorizer' 	: 	Authorizer,
										'Authorizeon'	: 	Authorizeon,
										"Reference"		:	Reference,
										"ActualAmount"	:	Decimal(ActualAmount)
										})

				#Block Update Disbursement status
				mktdb.updateRecord(MKT_DISBURSE,
								[MKT_DISBURSE.ID==DisburseID],{	
									'Inputter' 		:	Inputter,
									'Createdon' 	: 	Createdon,
									'Authorizer' 	: 	Authorizer,
									'Authorizeon'	: 	Authorizeon,
									"DisbursedStatus"	:"Y",
									"TotalActualAmount"	:TotalActualAmount
									})

				mktdb.deleteRecord(MKT_DISBURSE_INAU,[MKT_DISBURSE_INAU.ID==DisburseID])
				mktdb.deleteRecord(MKT_DISBURSE_DE_INAU,[MKT_DISBURSE_DE_INAU.ID==DisburseID])

				Message = msg_authorize_0
				return True,Message
			else:
				Message = msg_error+msg_cannot_authorize
				return False,Message
		else:
			Message = msg_error+" No record to authorize. "
			return False,Message

	except Exception, e:
		return False,msg_error+" %s"%e
	

def setDisburseCommitAccouting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amount,Module,Tran,TranDate,Reference,Note,DisburseID,Mode=""):
	try:
		Message = ""
		CheckAccounting = mktaccounting.getValidationAccounting(Inputter,Createdon,Authorizer,Authorizeon,Branch,DrAcc,DrCat,DrCur,CrAcc,CrCat,CrCur,Amount,Module,Tran,TranDate,Reference,Note)
		if CheckAccounting[0]:

			# Insert record to Fund Transfer
			InsertRecord = {	
								'Inputter'		:Inputter,
								'Createdon'		:Createdon,
								'Authorizer'	:Authorizer,
								'Authorizeon'	:Authorizeon,
								'Branch'		:Branch,
								'ID'			:Reference,
								'DrAccount'		:DrAcc,
								'DrCategory'	:DrCat,
								'DrCurrency'	:DrCur,
								'CrAccount'		:CrAcc,
								'CrCategory'	:CrCat,
								'CrCurrency' 	:CrCur,
								'Amount'		:Amount,
								'Transaction'	:Tran,
								'TranDate'		:TranDate,
								'Reference'		:DisburseID,
								'Note'			:Note
							}
			mktdb.insertTable(MKT_FUND_TRANSFER,InsertRecord)

			#post accounting
			for i in range(0, 2):

				if i == 0:
					Account 	= DrAcc
					Category	= DrCat
					Currency 	= DrCur
					DrCr 		= "Dr"
				else:
					Account		= CrAcc
					Category	= CrCat
					Currency 	= CrCur
					DrCr 		= "Cr"

				#get Gl key	
				GL_KEYS = mktaccounting.getConsolKey(Category,Currency,"","")
					
				mktaccounting.postAccounting(		
					"AUTH", 				# Status
					"0", 					# Curr
					Inputter,				# Inputter
					Createdon, 				# Createdon
					Authorizer,				# Authorizer
					Authorizeon,			# Authorizeon
					"", 					# AEID
					Account,				# Account
					Category,				# Category
					Currency,				# Currency
					DrCr,					# DrCr
					Amount, 				# Amount
					Module,					# Module
					Tran, 					# Transaction
					TranDate, 				# TransactionDate
					Reference, 				# Reference
					Note, 					# Note
					"", 					# JNID
					Branch,					# Branch
					GL_KEYS,				# ConsolKey
					Mode,					# Mode
					"YES")					# Tell function to update LastTransaction Customer's Account
			
			#Check after post accounting
			CheckPointObj = mktaccounting.setAccoutningCheckPoint(Module,Reference)
			if not CheckPointObj[0]:
				db.session.rollback()
				return False,CheckPointObj[1]

			return True,Message
		else:
			db.session.rollback()
			return False,CheckAccounting[1]
	except Exception, e:
		db.session.rollback()
		return False,'%s'%e


# Hot Field in Fund Withdrawal and Fund Deposit
def getOfficerAcountByBranch():
	try:

		TellerParam = 	mktparam.getTellerParam()
		Branch 		= 	mktuser.getBranch(session["ChangeBranch"]).ID
		AccObj 		= 	""
		WalletCat 	=	""
		if TellerParam:
			WalletCat = TellerParam.WalletCategory

		CashObj 	=	MKT_CASH_ACCOUNT.query.\
						filter(MKT_CASH_ACCOUNT.Type == 'W').\
						all()

		AccID =	[]

		if CashObj:

			for item in CashObj:

				ID 				=	item.ID
				CurrencyList 	= 	item.Currency
				CurrencyList 	= 	CurrencyList.split()
				
				if CurrencyList:

					for cur in CurrencyList:
						NewID = str(cur) + str(WalletCat) + str(ID)
						AccID.append(NewID)

		AccObj 	=	MKT_ACCOUNT.query.\
					filter(MKT_ACCOUNT.ID.in_(AccID)).\
					filter(MKT_ACCOUNT.Branch == Branch)

		return AccObj

	except Exception, e:
		return "%s" %s
