from flask 							import flash
from app.mktcore.imports 			import *
from .. 							import app, db
from sqlalchemy 					import *
from decimal 						import *
from datetime 						import datetime, date, timedelta
import time
import calendar

# from app.LoanProduct.models 		import MKT_LOAN_PRODUCT

import loantools.rescheduletools 	as mktschedule
import user 						as mktuser
import mktdate 						as mktdate
import mktaudit 					as mktaudit
import mktmoney 					as mktmoney

def postSchedule(ContractID="", Resource="", FwdBwdKey="", FrequencyP="", FrequencyI="", RepaymentMode="", BaseDateKey="", FirstInstallmentDate=""):
	try:

		if Resource.upper() == "INAU":
			Define = MKT_SCHED_DEFINE_INAU.query.get(ContractID)
		else:
			Define = MKT_SCHED_DEFINE.query.get(ContractID)

		if not Define:
			if Resource.upper() == "INAU":
				Define = MKT_SCHED_DEFINE_INAU(
							Status 					=	"AUTH",
							Curr 					= 	"0",
							Inputter 				=	mktuser.getUser().ID,
							Createdon 				= 	mktdate.getDateTimeNow(),
							Authorizer 				= 	mktuser.getUser().ID,
							Authorizeon 			= 	mktdate.getDateTimeNow(),
							ID 						=	ContractID,
							PrincipalFreq 			=	FrequencyP,
							InterestFreq 			= 	FrequencyI,
							FwdBwdKey 				= 	FwdBwdKey,
							RepMode 				= 	RepaymentMode,
							BaseDateKey 			=	BaseDateKey,
							FirstInstallmentDate 	=	FirstInstallmentDate
						)
			else:
				Define = MKT_SCHED_DEFINE(
							Status 					=	"AUTH",
							Curr 					= 	"0",
							Inputter 				=	mktuser.getUser().ID,
							Createdon 				= 	mktdate.getDateTimeNow(),
							Authorizer 				= 	mktuser.getUser().ID,
							Authorizeon 			= 	mktdate.getDateTimeNow(),
							ID 						=	ContractID,
							PrincipalFreq 			=	FrequencyP,
							InterestFreq 			= 	FrequencyI,
							FwdBwdKey 				= 	FwdBwdKey,
							RepMode 				= 	RepaymentMode,
							BaseDateKey 			=	BaseDateKey,
							FirstInstallmentDate 	=	FirstInstallmentDate
						)

			db.session.add(Define)

		else:

			Define.Curr 				= 	int(Define.Curr) + 1
			Define.Inputter 			= 	mktuser.getUser().ID
			Define.Createdon 			= 	mktdate.getDateTimeNow()
			Define.PrincipalFreq 		=	FrequencyP
			Define.InterestFreq 		= 	FrequencyI
			Define.FwdBwdKey 			= 	FwdBwdKey
			Define.RepMode 				= 	RepaymentMode
			Define.BaseDateKey 			=	BaseDateKey
			Define.FirstInstallmentDate =	FirstInstallmentDate
			
			db.session.add(Define)

		mktaudit.deleteAUTH(MKT_SCHED_MANUAL, ContractID, "LoanID")

		return Define

	except:
		db.session.rollback()
		raise

def postRepaymentSchedule(ContractID="", Resource="", Amendment="", AmtID="", Holiday="1"):
	try:

		output 			= "NO"
		Schedule 		= ""
		MaturityDate 	= ""
		TotalInterest 	= 0
		SystemBankDate 	= str(mktdate.getBankDate())
		# Get User Branch
		Branch = mktuser.getUser().Branch

		if Resource.upper() == "INAU":

			Contract = MKT_LOAN_CONTRACT_INAU.query.get(ContractID)
			if not Contract and Amendment == "YES":
				Contract = MKT_LOAN_CONTRACT.query.get(ContractID)
		else:
			Contract = MKT_LOAN_CONTRACT.query.get(ContractID)

		if not Contract:
			output = "Loan contract not found."
		else:
			# Define Loan Contract Fields
			DisburseAmount 		= 	float(Contract.Amount)
			FirstInstallmentDate= 	""
			ValueDate 			= 	str(Contract.ValueDate)
			InterestRate 		= 	Contract.InterestRate
			InterestRate 		=	InterestRate.split()
			Rate 				= 	float(InterestRate[0])
			FrequencyType 		= 	int(Contract.FreqType)
			Frequency 			= 	int(Contract.Frequency)
			Installment 		= 	int(Contract.Installment)
			Currency 			= 	str(Contract.Currency)
			HolidayOption 		=	int(Holiday)
			AccrCurrentInt 		=	float(Contract.AccrCurrentInt) if Contract.AccrCurrentInt else float(0)
			
			# Check if today is LC collect date
			RepSchedule =	MKT_REP_SCHEDULE.query.\
							filter(MKT_REP_SCHEDULE.CollectionDate == str(SystemBankDate)).\
							filter(MKT_REP_SCHEDULE.LoanID == ContractID).\
							filter(MKT_REP_SCHEDULE.RepStatus == "0").\
							first()

			if RepSchedule:
					AccrCurrentInt = float(0)

			output 				= 	"OK"

			if AmtID:
				if Resource.upper() == "INAU":
					Amendment = MKT_LOAN_AMENDMENT_INAU.query.get(AmtID)
					if not Amendment:
						Amendment = MKT_LOAN_AMENDMENT.query.get(AmtID)
				else:
					Amendment = MKT_LOAN_AMENDMENT.query.get(AmtID)

				if Amendment:

					if Amendment.Installment:
						Installment = 	int(Amendment.Installment)

					if Amendment.Interest:
						Rate 		= 	float(Amendment.Interest)

					ValueDate 	=	Amendment.ValueDate
					AddDedAmount= 	float(Amendment.Amount) if Amendment.Amount else float(0)
					AddDed 		= 	Amendment.AddDeduct
					if int(AddDed) == 1:
						BackUpAmount = float(DisburseAmount) - float(AddDedAmount)
					else:
						BackUpAmount = float(DisburseAmount) + float(AddDedAmount)

					DisburseAmount = float(BackUpAmount)

			# Define Loan Product Fields
			ProductID 				= 	Contract.LoanProduct
			Product 				= 	MKT_LOAN_PRODUCT.query.get(ProductID)
			if Product:
				BaseDateKey 		= 	int(Product.BaseDateKey)
				InterestDayBasis 	= 	int(Product.IntDayBasis)
			else:
				output = "Loan product not found."

			# Define Schedule Define Fields
			if Resource.upper() == "INAU":
				ScheduleDefine = MKT_SCHED_DEFINE_INAU.query.get(ContractID)
			else:
				ScheduleDefine = MKT_SCHED_DEFINE.query.get(ContractID)

			if ScheduleDefine:

				FwdBwdKey       		= 	int(ScheduleDefine.FwdBwdKey)
				FrequencyP      		= 	int(ScheduleDefine.PrincipalFreq)
				FrequencyI      		= 	int(ScheduleDefine.InterestFreq)
				RepaymentMode   		= 	int(ScheduleDefine.RepMode)
				FirstInstallmentDate 	=	str(ScheduleDefine.FirstInstallmentDate) if ScheduleDefine.FirstInstallmentDate else ""
				BaseDateKey 			=	int(ScheduleDefine.BaseDateKey) if ScheduleDefine.BaseDateKey else int(1)
				output 					=	"OK"

			else:
				output = "Schedule define not found."

			if output == "OK":
				if DisburseAmount != 0:

					if RepaymentMode == 1 or RepaymentMode == 3:
						Schedule = mktschedule.getSchedulDeclining(
			            											DisburseAmount,
			            											ValueDate,
			            											Rate,
			            											FrequencyType,
			                            							Frequency,
			                            							Installment,
																	InterestDayBasis,
			                            							FwdBwdKey,
			                            							HolidayOption,
			                            							Currency,
			                            							BaseDateKey,
			                            							FrequencyP,
			                            							FrequencyI,
			                            							RepaymentMode,
			                            							None,
			                            							FirstInstallmentDate
			                            						)
						
						
						# Add Old Record to Delete transaction
						if Resource.upper() == "INAU":
							Record = db.session.query(MKT_REP_SCHEDULE_INAU).filter_by(LoanID=ContractID).delete()
						else:
							Record = db.session.query(MKT_REP_SCHEDULE).filter_by(LoanID=ContractID).delete()

						for row in Schedule:
							ID 				= ContractID
							LoanID 			= ContractID
							CollectionDate 	= u""
							Principal 		= ""
							Interest 		= ""
							Balance 		= ""
							NumDay 			= 0
							RepStatus 		= 0
							PartPaidAmt 	= 0
							No 				= 0
							i 				= 0
							for col in row:
								if i == 0:
									No = col
									ID = str(ID) + str(col)

								if i == 1:
									MaturityDate = col
									CollectionDate = col

								if i == 2:
									Principal = str(col.replace(",", ""))

								if i == 3:
									Interest = str(col.replace(",", ""))
									TotalInterest += float(Interest)

								if i == 5:
									Balance = str(col.replace(",", ""))

								if i == 6:
									NumDay = str(col)

								i += 1
							try:
								if Resource.upper() == "INAU":
									RepSchedule = MKT_REP_SCHEDULE_INAU(
													Status 			=	"INAU",
													Curr 			= 	"0",
													Inputter 		=	mktuser.getUser().ID,
													Createdon 		= 	mktdate.getDateTimeNow(),
													Authorizer 		= 	mktuser.getUser().ID,
													Authorizeon 	= 	mktdate.getDateTimeNow(),
													ID 				= 	ID,
													LoanID 			= 	LoanID,
													No 				= 	No,
													CollectionDate 	=	CollectionDate,
													Principal 		= 	Principal,
													Interest 		= 	Interest,
													Balance 		= 	Balance,
													NumDay 			= 	NumDay,
													RepStatus 		= 	RepStatus,
													PartPaidAmt 	= 	PartPaidAmt,
													Branch 			=	Branch,
													Charge 			=	0
												)
								else:
									RepSchedule = MKT_REP_SCHEDULE(
													Status 			=	"AUTH",
													Curr 			= 	"0",
													Inputter 		=	mktuser.getUser().ID,
													Createdon 		= 	mktdate.getDateTimeNow(),
													Authorizer 		= 	mktuser.getUser().ID,
													Authorizeon 	= 	mktdate.getDateTimeNow(),
													ID 				= 	ID,
													LoanID 			= 	LoanID,
													No 				= 	No,
													CollectionDate 	=	CollectionDate,
													Principal 		= 	Principal,
													Interest 		= 	Interest,
													Balance 		= 	Balance,
													NumDay 			= 	NumDay,
													RepStatus 		= 	RepStatus,
													PartPaidAmt 	= 	PartPaidAmt,
													Branch 			=	Branch,
													Charge 			=	0
												)

								db.session.add(RepSchedule)
								output = "OK"
								
							except:
								db.session.rollback()
								output = "Failed to save schedule."
								raise

					else:
						Schedule = mktschedule.getScheduleAnnuity(
			            											DisburseAmount,
			            											ValueDate,
			            											Rate,
			            											FrequencyType,
			            											Frequency,
																	Installment,
																	Currency
		                            							)
						# Add Old Record to Delete transaction
						if Resource == "INAU":
							Record = db.session.query(MKT_REP_SCHEDULE_INAU).filter_by(LoanID=ContractID).delete()
						else:
							Record = db.session.query(MKT_REP_SCHEDULE).filter_by(LoanID=ContractID).delete()

						for row in Schedule:
							ID 				= ContractID
							LoanID 			= ContractID
							CollectionDate 	= u""
							Principal 		= ""
							Interest 		= ""
							Balance 		= ""
							NumDay 			= 0
							RepStatus 		= 0
							PartPaidAmt 	= 0
							No 				= 0
							i 				= 0
							for col in row:
								if i == 0:
									No = col
									ID = str(ID) + str(col)

								if i == 1:
									MaturityDate 	= col
									CollectionDate 	= col

								if i == 2:
									Principal = str(col.replace(",", ""))

								if i == 3:
									Interest 		= str(col.replace(",", ""))
									TotalInterest 	+= float(Interest)

								if i == 5:
									Balance = str(col.replace(",", ""))

								if i == 6:
									NumDay = str(col)

								i += 1
							try:
								if Resource.upper() == "INAU":
									RepSchedule = MKT_REP_SCHEDULE_INAU(
													Status 			=	"INAU",
													Curr 			= 	"0",
													Inputter 		=	mktuser.getUser().ID,
													Createdon 		= 	mktdate.getDateTimeNow(),
													Authorizer 		= 	mktuser.getUser().ID,
													Authorizeon 	= 	mktdate.getDateTimeNow(),
													ID 				= 	ID,
													LoanID 			= 	LoanID,
													No 				= 	No,
													CollectionDate 	=	CollectionDate,
													Principal 		= 	Principal,
													Interest 		= 	Interest,
													Balance 		= 	Balance,
													NumDay 			= 	NumDay,
													RepStatus 		= 	RepStatus,
													PartPaidAmt 	= 	PartPaidAmt,
													Branch 			=	Branch,
													Charge 			=	0
												)
								else:
									RepSchedule = MKT_REP_SCHEDULE(
													Status 			=	"AUTH",
													Curr 			= 	"0",
													Inputter 		=	mktuser.getUser().ID,
													Createdon 		= 	mktdate.getDateTimeNow(),
													Authorizer 		= 	mktuser.getUser().ID,
													Authorizeon 	= 	mktdate.getDateTimeNow(),
													ID 				= 	ID,
													LoanID 			= 	LoanID,
													No 				= 	No,
													CollectionDate 	=	CollectionDate,
													Principal 		= 	Principal,
													Interest 		= 	Interest,
													Balance 		= 	Balance,
													NumDay 			= 	NumDay,
													RepStatus 		= 	RepStatus,
													PartPaidAmt 	= 	PartPaidAmt,
													Branch 			=	Branch,
													Charge 			=	0
												)

								db.session.add(RepSchedule)
								output = "OK"
							except:
								db.session.rollback()
								output = "Failed to save schedule."
								raise
				else:
					output = "Disbursed amount must not %s 0." %Currency
			
			if AmtID:

				if AccrCurrentInt > 0:
					
					if Resource.upper() == "INAU":
						Rep = MKT_REP_SCHEDULE_INAU.query.get(ContractID + "1")
					else:
						Rep = MKT_REP_SCHEDULE.query.get(ContractID + "1")

					if Rep:
						AccrCurrentInt 	= mktmoney.toMoney(float(AccrCurrentInt), mktmoney.getCurrencyObj(Currency))
						AccrCurrentInt 	= float(AccrCurrentInt.replace(",", ""))
						Rep.Interest 	= float(Rep.Interest) + float(AccrCurrentInt)
						db.session.add(Rep)

			Contract.ValueDate 		= ValueDate
			Contract.MaturityDate 	= MaturityDate
			Contract.TotalInterest 	= TotalInterest
			db.session.add(Contract)

		return output

	except:
		db.session.rollback()
		raise

def setAuthorizeScheduleDefine(LoanID):
	try:

		Audit 		= mktaudit.getAuditrail()
		Inputter	= Audit['Inputter']
		Createdon 	= Audit['Createdon']
		Authorizer 	= Audit['Authorizer']
		Authorizeon	= Audit['Authorizeon']

		# Check if existed move to HIST
		mktaudit.deleteAUTH(MKT_SCHED_DEFINE, LoanID)
		mktaudit.deleteAUTH(MKT_REP_SCHEDULE, LoanID, "LoanID")

		mktaudit.moveINAUtoAUTH(MKT_SCHED_DEFINE, MKT_SCHED_DEFINE_INAU, LoanID, Inputter, Createdon, Authorizer, Authorizeon)
		mktaudit.deleteAUTH(MKT_SCHED_DEFINE_INAU, LoanID)

		mktaudit.moveINAUtoAUTH(MKT_REP_SCHEDULE, MKT_REP_SCHEDULE_INAU, LoanID, Inputter, Createdon, Authorizer, Authorizeon, "LoanID")
		mktaudit.deleteAUTH(MKT_REP_SCHEDULE_INAU, LoanID, "LoanID")

	except:
		db.session.rollback()
		raise