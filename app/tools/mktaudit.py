'''
Created Date: 01 Apr 2015
Author: Sovankiry Rim

Modified by: Sovankiry
Modified Date: 30 Sep 2015
All Right Reserved Morakot Technology
Description : mktaudit refer to function for audit
'''
from app.mktcore.imports 		import *
from flask 						import session,flash
from .. 						import app, db
from app.AutoID.models 			import *
import app.tools.mktdate 		as mktdate
import app.tools.user 			as mktuser
import app.tools.mktparam 		as mktparam
from sqlalchemy 				import inspect
from datetime 					import datetime, date, timedelta
import time


# How to use getAuditrail()
'''

Audit = getAuditrail()
Status = Audit['Status']
Curr = Audit['Curr']
Inputter = Audit['Inputter']
Createdon = Audit['Createdon']
Authorizer = Audit['Authorizer']
Authorizeon = Audit['Authorizeon']
Branch = Audit['Branch']

'''

def getAuditrail():
	Dic = {}
	Dic.update({'Status'	: 	"AUTH"})
	Dic.update({'Curr' 		: 	"0"})
	Dic.update({'Inputter' 	:	mktuser.getUser().ID})
	Dic.update({'Createdon' : 	mktdate.getDateTimeNow()})
	Dic.update({'Authorizer' : 	mktuser.getUser().ID})
	Dic.update({'Authorizeon': 	mktdate.getDateTimeNow()})
	Dic.update({'Branch'	:mktuser.getBranch(session["ChangeBranch"]).ID})
	# Dic.update({'BranchDescription':mktuser.getBranch(session["ChangeBranch"]).Description})
	return Dic



#How to use function moveAUTHtoHIST()
'''
LIVE : refer to table live example : MKT_ACCOUNT
HIST : refer to table History example : MKT_ACCOUNT_HIST
Mode : 0 = INAU ; 1 = RNAU
Don't use paramter LIVE and HIST to string. need class
Example :

Transaction is INAU to AUTH :

	moveAUTHtoHIST(MKT_ACCOUNT,MKT_ACCOUNT_HIST,'AC200001',0,Inputter,Createdon,Authorizer,Authorizeon)

Transaction is RNAU to REVE :

	moveAUTHtoHIST(MKT_ACCOUNT,MKT_ACCOUNT_HIST,'AC200001',1,Inputter,Createdon,Authorizer,Authorizeon)

'''

def moveAUTHtoHIST(LIVE,HIST,ID, FilterField=''):
	try:
		dic 			= {}
		FieldInTable 	= []
		Mapper 			= inspect(LIVE)
		for item in Mapper.attrs:						
			FieldInTable.append(item.key)
		
		HistObj = HIST.query.\
				  filter(HIST.ID.like( ID + '%'))

		if HistObj.count() != 0:
			Curr = str(HistObj.count())
		else:
			Curr = '0'

		if FilterField:
			QueryObj = LIVE.query.filter(getattr(LIVE, FilterField) == ID)
		else:
			QueryObj = LIVE.query.filter(LIVE.ID==ID)

	
		for row in QueryObj:
			for col in FieldInTable:
				if col =='ID':
					dic.update({col:str(getattr(row,col)) + '@' + Curr})
				else:
					dic.update({col:getattr(row,col)})

			AddHistObj = HIST(**dic)
			db.session.add(AddHistObj)

	except Exception, e:
		print "moveAUTHtoHIST error: %s"%e
		db.session.rollback()
		raise

def moveAUTHDetailtoHIST(MainTable,LIVE,HIST,ID,FilterField=''):
	try:
		dic 			= {}
		FieldInTable 	= []
		Mapper 			= inspect(LIVE)
		for item in Mapper.attrs:						
			FieldInTable.append(item.key)
		
		LiveObj = MainTable.query.get(ID)
		if LiveObj:
			Curr = str(LiveObj.Curr)
		else:
			Curr = '0'

		if FilterField:
			QueryObj = LIVE.query.filter(getattr(LIVE, FilterField) == ID)
		else:
			QueryObj = LIVE.query.filter(LIVE.ID==ID)

	
		for row in QueryObj:
			for col in FieldInTable:
				if col =='ID':
					dic.update({col:str(getattr(row,col)) + '@' + Curr})
				else:
					dic.update({col:getattr(row,col)})

			AddHistObj = HIST(**dic)
			db.session.add(AddHistObj)

	except Exception, e:
		print "moveAUTHDetailtoHIST error: %s"%e
		db.session.rollback()
		raise

'''

# this function are move live record to history and not increment curr
def moveAUTHtoHIST(LIVE,HIST,ID):
	try:
		dic 			= {}
		FieldInTable 	= []
		Mapper 			= inspect(LIVE)
		for item in Mapper.attrs:						
			FieldInTable.append(item.key)
		QueryObj = LIVE.query.filter(LIVE.ID==ID)
		for row in QueryObj:
			for col in FieldInTable:
				dic.update({col:getattr(row,col)})

		AddHistObj = HIST(**dic)
		db.session.add(AddHistObj)

	except Exception, e:
		db.session.rollback()
		raise
'''

def moveAUTHtoINAU(LIVE,INAU,ID,Inputter='',Createdon='', Status='INAU', Curr='0'):

	try:
		TellerParam = mktparam.getTellerParam()
		RevTran		= TellerParam.RevTransaction
		dic 			= {}
		FieldInTable 	= []
		Mapper 			= inspect(LIVE)
		for item in Mapper.attrs:						
			FieldInTable.append(item.key)

		QueryObj = LIVE.query.filter(LIVE.ID==ID)

		if QueryObj.all() :

			for row in QueryObj:
				for col in FieldInTable:
					dic.update({col:getattr(row,col)})

				dic.update({'Status':Status})
				dic.update({'Curr':Curr})
				dic.update({'ID':ID+'R'})
				if 'Transaction' in dic:
					dic.update({'Transaction':RevTran})
				dic.update({'Inputter':Inputter})
				dic.update({'Createdon':Createdon})
				dic.update({'Authorizer':''})
				dic.update({'Authorizeon':''})


				AddINAUObj = INAU(**dic)
				db.session.add(AddINAUObj)
		else:
			print "The record %s cannot move to un-authorize. Module %s"%(ID,LIVE.__name__)
			

	except Exception, e:
		print "moveAUTHtoINAU error: %s"%e
		db.session.rollback()
		raise


def moveINAUtoAUTH(LIVE,INAU,ID,Inputter='',Createdon='',Authorizer='',Authorizeon='', FilterField='',Curr=""):

	try:
		dic 			= {}
		FieldInTable 	= []
		Mapper 			= inspect(INAU)
		for item in Mapper.attrs:						
			FieldInTable.append(item.key)

		if FilterField:
			QueryObj = INAU.query.filter(getattr(INAU, FilterField) == ID)
		else:
			QueryObj = INAU.query.filter(INAU.ID==ID)

		if QueryObj.all() :
			for row in QueryObj:
				for col in FieldInTable:
					dic.update({col:getattr(row,col)})
				if Curr:
					dic.update({'Curr':Curr})
				dic.update({'Status':'AUTH'})
				dic.update({'Inputter':Inputter})
				dic.update({'Createdon':Createdon})
				dic.update({'Authorizer':Authorizer})
				dic.update({'Authorizeon':Authorizeon})
				AddLiveObj = LIVE(**dic)
				# print 'Now move INAU to AUTH '
				db.session.add(AddLiveObj)
				# print 'Start add session INAU to AUTH '
		else:
			print "The record %s cannot move to live. Module %s"%(ID,INAU.__name__)
			# db.session.rollback()
			# raise

	except Exception, e:
		print "moveINAUtoAUTH error: %s"%e
		db.session.rollback()
		raise

def getCurrRecord(Table,arg):
	QueryObj = Table.query.filter(*arg).first()
	if QueryObj:
		Curr = str(int(QueryObj.Curr)+1)
	else:
		Curr = "0"
	return Curr

#check duplicate ID
def isDuplicate(AUTH,INAU="",HIST="",ID="",FilterAUTH=[],FilterINAU=[]):
	if FilterAUTH:
		obj_query=AUTH.query.filter(*FilterAUTH).all()
	else:
		obj_query=AUTH.query.filter(AUTH.ID==ID).all()

	if obj_query:
		return True,msg_warning+" %s already exists in live record."%ID

	if FilterINAU:
		obj_query=INAU.query.filter(*FilterINAU).all()
	else:
		obj_query=INAU.query.filter(INAU.ID==ID).all()
	if obj_query:
		return True,msg_warning+" %s already exists in un-authorize record."%ID


	return False,""

#check Restore ID
def isRestoreID(AUTH,INAU,HIST,ID):
	QueryObj = AUTH.query.get(ID)
	if not QueryObj:
		
		QueryObj = HIST.query.get("%s@0"%ID)
		if QueryObj:
			obj_subquery = HIST.query.filter(HIST.ID.like("%s@%%"%ID)).all()
			# "You're restoring a record from history with curr No: " + str(session["Curr"])
			return True,str(len(obj_subquery)) 
	return False,""

def setReverseToLive(LIVE,INAU,ID,Inputter='',Createdon='',Authorizer='',Authorizeon='',FilterField=''):

	try:
		TellerParam = mktparam.getTellerParam()
		RevTran		= TellerParam.RevTransaction

		dic 			= {}
		FieldInTable 	= []
		Mapper 			= inspect(INAU)
		for item in Mapper.attrs:						
			FieldInTable.append(item.key)

		if FilterField:
			QueryObj = LIVE.query.filter(getattr(LIVE, FilterField) == ID)
		else:
			QueryObj = LIVE.query.filter(LIVE.ID==ID)

		if QueryObj :
			for row in QueryObj:
				for col in FieldInTable:
					dic.update({col:getattr(row,col)})

				dic.update({'Status':'AUTH'})
				dic.update({'Inputter':Inputter})
				dic.update({'Createdon':Createdon})
				dic.update({'Authorizer':Authorizer})
				dic.update({'Authorizeon':Authorizeon})
				dic.update({'ID':ID+'R'})
				if 'Transaction' in dic:
					dic.update({'Transaction':RevTran}) 
				ReverseObj = LIVE(**dic)
				# print 'Now move INAU to AUTH '
				db.session.add(ReverseObj)
				# print 'Start add session INAU to AUTH '
		else:
			print "The record %s cannot setReverseToLive. Module %s"%(ID,LIVE.__name__)
			
	except Exception, e:
		print "setReverseToLive error: %s"%e
		db.session.rollback()
		raise

def deleteAUTH(LIVE,ID,FilterField=''):
	try:
		if FilterField:
			db.session.query(LIVE).filter(getattr(LIVE,FilterField)==ID).delete()
		else:
			db.session.query(LIVE).filter_by(ID=ID).delete()
	except Exception, e:
		print "deleteAUTH error: %s"%e
		db.session.rollback()
		raise

def deleteINAU(INAU,ID,FilterField=''):
	try:
		if FilterField:
			db.session.query(INAU).filter(getattr(INAU,FilterField)==ID).delete()
		else:
			db.session.query(INAU).filter_by(ID=ID).delete()
	except  Exception, e:
		print "deleteINAU error: %s"%e
		db.session.rollback()
		raise