from flask 							import flash
from app.mktcore.imports 			import *
from .. 							import app, db
from app.Language.register 			import Language
from app.config 					import *
import logging

from datetime 						import datetime, date, timedelta

def makeLoggedFile(Msg):
	try:

		dat 		= datetime.now()
		CurrentTime = '%s:%s:%s'% (dat.hour, dat.minute, dat.second)
		currentDate = '%s-%s-%s'% (dat.year, dat.month, dat.day)
		# currentDate = str(currentDate) + "_" + CurrentTime
		filename 	= VB_PATH + "log/EOD/EOD_%s.log" %currentDate
		logging.basicConfig(filename=filename,level=logging.DEBUG)
		logging.info(str(Msg))

	except:
		raise
def msgError(EOD=1, Msg="", Index=""):
	try:

		db.session.rollback()

		if Index:
			Msg = getTextMsg(Index)

		if int(EOD) == 1:
			print Msg
			return "1"
		else:
			flash(msg_error + Msg)
			return "0"

	except:
		raise

def msgOutputMsg(Msg="", Index=""):
	try:

		if Index:
			Msg = getTextMsg(Index)

		print Msg

		# makeLoggedFile(Msg)

	except:
		raise

def getTextMsg(Index=""):
	try:

		Msg = Language[Index]
		return Msg

	except:
		raise

def getLang(Index=""):
	try:

		Msg = Language[Index]
		return Msg

	except:
		raise

def getNBCLang(Language, Index=""):
	try:
		
		return Language[Index]

	except Exception, e:
		raise