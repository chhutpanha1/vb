from app.mktcore.imports 			import *
from .. 							import app
from sqlalchemy 					import *
from decimal 						import *
from datetime 						import datetime, date, timedelta
import time
import calendar


import mktdate 						as mktdate
import mktaccounting 				as mktaccounting
import mktsetting 					as mktsetting
import mktmessage 					as mktmessage
import mktmoney 					as mktmoney
import mktbjstat 					as mktbjstat

def setConsolBalance(GL_KEYS, Branch, Currency, Amount, Category, DrCr, Module, TranDate, LCYAmount):
	
	try:
		
		ReportingRate 	= 	mktaccounting.getReportingRate(Currency)
		BalType 		= 	MKT_CATEGORY.query.get(Category)
		# Amount 			=	(float(Amount) if Amount else float(0)) * (float(ReportingRate) if ReportingRate else float(0))

		BankDate 		= 	mktdate.getBankDate()
		BankDate 		= 	str(BankDate)

		if not BankDate:
			flash(msg_error + "Bank date not found in system date")
			db.session.rollback()
			return False

		if BalType:

			BalType = 	BalType.BalanceType
			GL_KEYS = 	GL_KEYS.strip()
			ConBal 	= 	MKT_CONSOL_BALANCE.query.\
						filter(MKT_CONSOL_BALANCE.ID == str(GL_KEYS)).\
						filter(MKT_CONSOL_BALANCE.Branch == str(Branch)).\
						first()

			ObjBranch 	=	MKT_BRANCH.query.get(Branch)
			
			if not ConBal:

				Insert 	= 	MKT_CONSOL_BALANCE(

								ID 					=	GL_KEYS,
								Branch 				=	Branch,
								Currency 			=	Currency,
								PrevMonthBal 		=	0,
								CurrentPrevMonthBal =	0,
								CurrentMonthBal 	=	0,
								PrevYearBal 		= 	0,
								CurrentPrevYearBal 	=	0,
								YTDBal 				=	0,
								Balance 			=	0,

								LCYPrevMonthBal 		=	0,
								LCYCurrentPrevMonthBal 	=	0,
								LCYCurrentMonthBal 		=	0,
								LCYPrevYearBal 			= 	0,
								LCYCurrentPrevYearBal 	=	0,
								LCYYTDBal 				=	0,
								LCYBalance 				=	0
							
							)
				
				db.session.add(Insert)
				
				ConBal 	= 	MKT_CONSOL_BALANCE.query.\
							filter(MKT_CONSOL_BALANCE.ID == str(GL_KEYS)).\
							filter(MKT_CONSOL_BALANCE.Branch == str(Branch)).\
							first()

			SystemBankDate 	= 	mktdate.getBankDate()
			SystemBankDate 	= 	datetime.strptime(str(SystemBankDate),'%Y-%m-%d').date()
			CheckPrevMonth 	=	mktdate.getNextMonth(SystemBankDate, -1)

			PrevMonth 		=	CheckPrevMonth.month
			PrevYear 		=	CheckPrevMonth.year

			TranDate 		= 	datetime.strptime(str(TranDate),'%Y-%m-%d').date()

			TranMonth 		=	TranDate.month
			TranYear 		=	TranDate.year

			CheckPrevYear 	=	mktdate.getNextMonth(SystemBankDate, -12)
			PrevYearMonth 	=	CheckPrevYear.month
			PrevYearYear 	=	CheckPrevYear.year

			if int(PrevMonth) == int(TranMonth) and int(PrevYear) == int(TranYear):

				if ObjBranch:

					if str(ObjBranch.ReportLocked).upper() != 'Y':

						if BalType == 'Cr':

							ConBal.PrevMonthBal 			= 	(float(ConBal.PrevMonthBal) 		+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.PrevMonthBal) 			- float(Amount))
							ConBal.CurrentPrevMonthBal 		= 	(float(ConBal.CurrentPrevMonthBal) 	+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentPrevMonthBal) 	- float(Amount))
							ConBal.Balance 					= 	(float(ConBal.Balance) 				+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.Balance) 				- float(Amount))
							ConBal.YTDBal 					= 	(float(ConBal.YTDBal) 				+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.YTDBal) 				- float(Amount))

							ConBal.LCYPrevMonthBal 			= 	(float(ConBal.LCYPrevMonthBal) 			+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYPrevMonthBal) 		- float(LCYAmount))
							ConBal.LCYCurrentPrevMonthBal 	= 	(float(ConBal.LCYCurrentPrevMonthBal) 	+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYCurrentPrevMonthBal) 	- float(LCYAmount))
							ConBal.LCYBalance 				= 	(float(ConBal.LCYBalance) 				+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYBalance) 				- float(LCYAmount))
							ConBal.LCYYTDBal 				= 	(float(ConBal.LCYYTDBal) 				+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYYTDBal) 				- float(LCYAmount))

							if int(PrevMonth) == 12 and int(TranMonth) == 12:

								ConBal.CurrentPrevYearBal 	= 	(float(ConBal.CurrentPrevYearBal) 	+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentPrevYearBal) - float(Amount))
								ConBal.PrevYearBal 			= 	(float(ConBal.PrevYearBal) 			+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.PrevYearBal) 		- float(Amount))

								ConBal.LCYCurrentPrevYearBal 	= 	(float(ConBal.LCYCurrentPrevYearBal) 	+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYCurrentPrevYearBal) 	- float(LCYAmount))
								ConBal.LCYPrevYearBal 			= 	(float(ConBal.LCYPrevYearBal) 			+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYPrevYearBal) 			- float(LCYAmount))
						
						else:

							ConBal.PrevMonthBal 			= 	(float(ConBal.PrevMonthBal) 		- float(Amount)) if DrCr == 'Cr' else (float(ConBal.PrevMonthBal) 			+ float(Amount))
							ConBal.CurrentPrevMonthBal 		= 	(float(ConBal.CurrentPrevMonthBal) 	- float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentPrevMonthBal) 	+ float(Amount))
							ConBal.Balance 					= 	(float(ConBal.Balance) 				- float(Amount)) if DrCr == 'Cr' else (float(ConBal.Balance) 				+ float(Amount))
							ConBal.YTDBal 					= 	(float(ConBal.YTDBal) 				- float(Amount)) if DrCr == 'Cr' else (float(ConBal.YTDBal) 				+ float(Amount))

							ConBal.LCYPrevMonthBal 			= 	(float(ConBal.LCYPrevMonthBal) 			- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYPrevMonthBal) 		+ float(LCYAmount))
							ConBal.LCYCurrentPrevMonthBal 	= 	(float(ConBal.LCYCurrentPrevMonthBal) 	- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYCurrentPrevMonthBal) 	+ float(LCYAmount))
							ConBal.LCYBalance 				= 	(float(ConBal.LCYBalance) 				- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYBalance) 				+ float(LCYAmount))
							ConBal.LCYYTDBal 				= 	(float(ConBal.LCYYTDBal) 				- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYYTDBal) 				+ float(LCYAmount))

							if int(PrevMonth) == 12 and int(TranMonth) == 12:

								ConBal.CurrentPrevYearBal 	= 	(float(ConBal.CurrentPrevYearBal) 	- float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentPrevYearBal) + float(Amount))
								ConBal.PrevYearBal 			= 	(float(ConBal.PrevYearBal) 			- float(Amount)) if DrCr == 'Cr' else (float(ConBal.PrevYearBal) 		+ float(Amount))

								ConBal.LCYCurrentPrevYearBal 	= 	(float(ConBal.LCYCurrentPrevYearBal) 	- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYCurrentPrevYearBal) 	+ float(LCYAmount))
								ConBal.LCYPrevYearBal 			= 	(float(ConBal.LCYPrevYearBal) 			- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYPrevYearBal) 			+ float(LCYAmount))

							# ConBal.PrevMonthBal 			= 	(float(ConBal.PrevMonthBal) 		- float(Amount)) if DrCr == 'Cr' else (float(ConBal.PrevMonthBal) 			+ float(Amount))
							# ConBal.CurrentPrevMonthBal 		= 	(float(ConBal.CurrentPrevMonthBal) 	- float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentPrevMonthBal) 	+ float(Amount))
							# ConBal.Balance 					= 	(float(ConBal.Balance) 				- float(Amount)) if DrCr == 'Cr' else (float(ConBal.Balance) 				+ float(Amount))
							# ConBal.YTDBal 					= 	(float(ConBal.YTDBal) 				- float(Amount)) if DrCr == 'Cr' else (float(ConBal.YTDBal) 				+ float(Amount))

							# if int(PrevMonth) == 12 and int(TranMonth) == 12:

							# 	ConBal.CurrentPrevYearBal 	= 	(float(ConBal.CurrentPrevYearBal) 	- float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentPrevYearBal) + float(Amount))
							# 	ConBal.PrevYearBal 			= 	(float(ConBal.PrevYearBal) 			- float(Amount)) if DrCr == 'Cr' else (float(ConBal.PrevYearBal) 		+ float(Amount))
					else:
						db.session.rollback()
						print "Branch %s was locked for previous month adjustment." %Branch
						return False
				else:

					print "Could not fitch branch %s." %Branch
			# elif int(PrevYearMonth) == int(TranMonth) and int(PrevYearYear) == int(TranYear):

			# 	if BalType == 'Cr':

			# 		ConBal.PrevYearBal 				= 	(float(ConBal.PrevYearBal) 			+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.PrevYearBal) 			- float(Amount))
			# 		ConBal.CurrentPrevYearBal 		= 	(float(ConBal.CurrentPrevYearBal) 	+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentPrevYearBal) 	- float(Amount))
			# 		ConBal.Balance 					= 	(float(ConBal.Balance) 				+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.Balance) 				- float(Amount))
			# 		ConBal.YTDBal 					= 	(float(ConBal.YTDBal) 				+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.YTDBal) 				- float(Amount))

			# 	else:

			# 		ConBal.PrevYearBal 				= 	(float(ConBal.PrevYearBal) 			- float(Amount)) if DrCr == 'Cr' else (float(ConBal.PrevYearBal) 			+ float(Amount))
			# 		ConBal.CurrentPrevYearBal 		= 	(float(ConBal.CurrentPrevYearBal) 	- float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentPrevYearBal) 	+ float(Amount))
			# 		ConBal.Balance 					= 	(float(ConBal.Balance) 				- float(Amount)) if DrCr == 'Cr' else (float(ConBal.Balance) 				+ float(Amount))
			# 		ConBal.YTDBal 					= 	(float(ConBal.YTDBal) 				- float(Amount)) if DrCr == 'Cr' else (float(ConBal.YTDBal)
			else:

				if BalType == 'Cr':

					ConBal.Balance 			= 	(float(ConBal.Balance) 			+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.Balance) 			- float(Amount))
					ConBal.YTDBal 			= 	(float(ConBal.YTDBal) 			+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.YTDBal) 			- float(Amount))
					ConBal.CurrentMonthBal 	=	(float(ConBal.CurrentMonthBal) 	+ float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentMonthBal) 	- float(Amount))

					ConBal.LCYBalance 			= 	(float(ConBal.LCYBalance) 			+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYBalance) 			- float(LCYAmount))
					ConBal.LCYYTDBal 			= 	(float(ConBal.LCYYTDBal) 			+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYYTDBal) 			- float(LCYAmount))
					ConBal.LCYCurrentMonthBal 	=	(float(ConBal.LCYCurrentMonthBal) 	+ float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYCurrentMonthBal) 	- float(LCYAmount))
				
				else:

					ConBal.Balance 			= 	(float(ConBal.Balance) 			- float(Amount)) if DrCr == 'Cr' else (float(ConBal.Balance) 			+ float(Amount))
					ConBal.YTDBal 			= 	(float(ConBal.YTDBal) 			- float(Amount)) if DrCr == 'Cr' else (float(ConBal.YTDBal) 			+ float(Amount))
					ConBal.CurrentMonthBal 	=	(float(ConBal.CurrentMonthBal) 	- float(Amount)) if DrCr == 'Cr' else (float(ConBal.CurrentMonthBal) 	+ float(Amount))

					ConBal.LCYBalance 			= 	(float(ConBal.LCYBalance) 			- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYBalance) 			+ float(LCYAmount))
					ConBal.LCYYTDBal 			= 	(float(ConBal.LCYYTDBal) 			- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYYTDBal) 			+ float(LCYAmount))
					ConBal.LCYCurrentMonthBal 	=	(float(ConBal.LCYCurrentMonthBal) 	- float(LCYAmount)) if DrCr == 'Cr' else (float(ConBal.LCYCurrentMonthBal) 	+ float(LCYAmount))
			
			return db.session.add(ConBal)
			
		else:
			print "Category not found."

	except Exception, e:
		db.session.rollback()
		return False

def updateEndMonthConsolBalance():
	try:

		BankDate 	= 	mktdate.getBankDate()
		BankDate 	= 	str(BankDate)

		BankDate 	= 	mktdate.getBankDate()
		BankDate 	= 	str(BankDate)
		Day 		= 	BankDate.replace("-", "")

		EndMonth 	= 	mktdate.getBankDateObj().NextMonthEnd
		EndMonth 	= 	str(EndMonth)
		DayEndMonth = 	EndMonth.replace("-", "")

		YearEnd 	=	mktdate.getBankDateObj().NextYearEnd
		DayEndYear 	=	str(YearEnd).replace("-", "")
		
		if int(Day) == int(DayEndMonth): # Check Month-end

			# Update current year profit/loss
			updateEndMonthProfitLoss()

		if int(Day) == int(DayEndYear): # Check Year-end

			# Update prior year profit/loss
			updateEndYearProfitLoss()

		if int(Day) == int(DayEndMonth): # Check Month-end

			EndMonthBal 	=	MKT_CONSOL_BALANCE.query.\
								order_by(MKT_CONSOL_BALANCE.ID.asc()).\
								all()

			if EndMonthBal:

				for item in EndMonthBal:

					item.PrevMonthBal 			= 	float(item.PrevMonthBal) + float(item.CurrentMonthBal)
					item.CurrentPrevMonthBal 	=	float(item.CurrentMonthBal)
					item.CurrentMonthBal 		= 	float(0)

					item.LCYPrevMonthBal 		= 	float(item.LCYPrevMonthBal) + float(item.LCYCurrentMonthBal)
					item.LCYCurrentPrevMonthBal =	float(item.LCYCurrentMonthBal)
					item.LCYCurrentMonthBal 	= 	float(0)

					db.session.add(item)

		if int(Day) == int(DayEndYear): # Check Year-end

			EndYearBal 	=	MKT_CONSOL_BALANCE.query.\
							order_by(MKT_CONSOL_BALANCE.ID.asc()).\
							all()

			if EndYearBal:

				for item in EndYearBal:

					item.PrevYearBal 			= 	float(item.PrevYearBal) + float(item.YTDBal)
					item.CurrentPrevYearBal 	=	float(item.YTDBal)
					item.YTDBal 				= 	float(0)

					item.LCYPrevYearBal 		= 	float(item.LCYPrevYearBal) + float(item.LCYYTDBal)
					item.LCYCurrentPrevYearBal 	=	float(item.LCYYTDBal)
					item.LCYYTDBal 				= 	float(0)

					db.session.add(item)

	except:
		print ""
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

# doEndMonthProfitLoss
def updateEndMonthProfitLoss(Period=0):
	try:

		AccSetting 		=	mktsetting.getAccSetting()
		Transaction 	=	""
		CurrentYearCat 	=	""
		IncomeSummaryCat=	""
		BaseCurrency 	=	""
		TrueOrFalse 	=	True
		TranDate 		=	str(mktdate.getBankDate())

		if AccSetting:

			Transaction 		=	AccSetting.CurrYearTran
			CurrentYearCat		=	AccSetting.CurrYearCat
			IncomeSummaryCat 	=	AccSetting.IntSummaryCat
			BaseCurrency 		=	AccSetting.BaseCurrency

		Currency 		=	BaseCurrency
		BranchObj 		=	MKT_BRANCH.query.\
							all()

		if BranchObj:

			for item in BranchObj:
				
				Branch 			=	item.ID
				# Update GL Balance
				print "GL is updating for %s." %Branch
				getTreeGLCode(Branch)

				GetSetting 		= 	getTransferSetting()

				if GetSetting[0] == True:
					Amount 		= 	getReportSubTotal(GetSetting[1], GetSetting[3], Currency, Branch, Period)
				else:
					Amount 		=	0

				OriAmount 		=	Amount
				Amount 			=	abs(float(Amount))

				if not Transaction:
					# Call method for error message
					TrueOrFalse 	= 	mktmessage.msgError(1, "Current year profit/loss booking transaction not found.")
				else:
					Category 	=	IncomeSummaryCat if float(OriAmount) >= 0 else CurrentYearCat
					DrCr 		=	"Dr"
					Mode 		=	"Direct"
					DateTimeNow = 	mktdate.getDateTimeNow()
					GL_KEYS 	= 	mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
					# Debit Current Year Profit/Loss \ Income Summary
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"system",				# Inputter
						DateTimeNow, 			# Createdon
						"system",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						"",						# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(Amount), 		# Amount
						"AC",					# Module
						Transaction, 			# Transaction
						TranDate, 				# TransactionDate
						"%s" %Branch, 			# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)

					Category 	=	CurrentYearCat if float(OriAmount) >= 0 else IncomeSummaryCat
					DrCr 		=	"Cr"
					Mode 		=	"Direct"
					DateTimeNow = 	mktdate.getDateTimeNow()
					GL_KEYS 	= 	mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
					# Credit Current Year Profit/Loss \ Income Summary
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"system",				# Inputter
						DateTimeNow, 			# Createdon
						"system",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						"",						# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(Amount), 		# Amount
						"AC",					# Module
						Transaction, 			# Transaction
						TranDate, 				# TransactionDate
						"%s" %Branch, 			# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)

				print "Month-end closing for branch %s." %Branch
				print "Net operating profit/loss amount is %s." %mktmoney.toMoney(float(OriAmount), mktmoney.getCurrencyObj(Currency))

			db.session.commit()
			print "Month-end was successfully closed."
			print ""

		return ""

	except Exception, e:
		db.session.rollback()
		# print "%s" %e
		mktbjstat.makeLogFileOnError('Y')
		raise "%s" %e

def updateEndYearProfitLoss(Period=6):
	try:

		AccSetting 		=	mktsetting.getAccSetting()
		Transaction 	=	""
		CurrentYearCat 	=	""
		PriorYearCat 	=	""
		BaseCurrency 	=	""
		TrueOrFalse 	=	True
		TranDate 		=	str(mktdate.getBankDate())

		if AccSetting:

			Transaction 	=	AccSetting.PriorYearTran
			CurrentYearCat	=	AccSetting.CurrYearCat
			PriorYearCat 	=	AccSetting.PriorYearCat
			BaseCurrency 	= 	AccSetting.BaseCurrency

		Currency 		=	BaseCurrency
		BranchObj 		=	MKT_BRANCH.query.\
							all()

		if BranchObj:

			for item in BranchObj:

				Branch 			=	item.ID
				# Update GL Balance
				print "GL is updating for %s." %Branch
				getTreeGLCode(Branch)
				# GetSetting 		= 	getTransferSetting()

				# Get amount from Consolidated Balance
				CurrentYearKey 	=	mktaccounting.getConsolKey(CurrentYearCat, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
				print "KEY %s Branhc %s Currency %s" %(CurrentYearKey, Branch, Currency)
				# ConsolObj 		=	MKT_CONSOL_BALANCE.query.\
				# 					filter(MKT_CONSOL_BALANCE.ID == CurrentYearKey).\
				# 					filter(MKT_CONSOL_BALANCE.Branch == Branch).\
				# 					first()

				# if ConsolObj == True:
				# 	print 'Met'
				# 	Amount 		=	ConsolObj.Balance
				# else:
				# 	Amount 		=	0
				Amount 			=	getConsolKeyBal(CurrentYearKey, Branch)

				OriAmount 		=	Amount
				Amount 			=	abs(float(Amount))
				print "Amount %s" %Amount

				if not Transaction:
					# Call method for error message
					TrueOrFalse 	= 	mktmessage.msgError(1, "Current year profit/loss booking transaction not found.")
				else:
					Category 	=	CurrentYearCat if float(OriAmount) >= 0 else PriorYearCat
					DrCr 		=	"Dr"
					Mode 		=	"Direct"
					DateTimeNow = 	mktdate.getDateTimeNow()
					GL_KEYS 	= 	mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
					# Debit Current Year Profit/Loss / Income Summary
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"system",				# Inputter
						DateTimeNow, 			# Createdon
						"system",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						"",						# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(Amount), 		# Amount
						"AC",					# Module
						Transaction, 			# Transaction
						TranDate, 				# TransactionDate
						"%s" %Branch, 			# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)

					Category 	=	PriorYearCat if float(OriAmount) >= 0 else CurrentYearCat
					DrCr 		=	"Cr"
					Mode 		=	"Direct"
					DateTimeNow = 	mktdate.getDateTimeNow()
					GL_KEYS 	= 	mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
					# Credit Prior Year Profit/Loss / Income Summary
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"system",				# Inputter
						DateTimeNow, 			# Createdon
						"system",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						"",						# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(Amount), 		# Amount
						"AC",					# Module
						Transaction, 			# Transaction
						TranDate, 				# TransactionDate
						"%s" %Branch, 			# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)

				print "Year-end closing for branch %s." %Branch
				print "Net operating profit/loss amount is %s." %mktmoney.toMoney(float(OriAmount), mktmoney.getCurrencyObj(Currency))

			db.session.commit()
			print "Year-end was successfully closed."
			print ""

		return ""

	except Exception, e:
		db.session.rollback()
		# print "%s" %e
		mktbjstat.makeLogFileOnError('Y')
		raise "%s" %e

@app.route("/Morakot/GetNetOperatingProfitLoss/", methods=['GET'])
def getTestKey():

	Currency 	=	"KHR"
	Branch 		=	request.args.get('Branch') if 'Branch' in request.args else "HO"
	Period 		=	request.args.get('Period') if 'Period' in request.args else 0
	Test = getTransferSetting()
	Good = getReportSubTotal(Test[1], Test[3], Currency, Branch, Period)
	updateEndYearProfitLoss()

	# return "%s" %mktmoney.toMoney(float(Good), mktmoney.getCurrencyObj(Currency))
	return "%s" %Test

def getTransferSetting():
	try:
		
		Result 				=	[False, "", "", "", "", "", ""]
		Transfer 			= 	mktsetting.getAccSetting().NetIncomeToEquity
		SenderReportID 		=	""
		SenderLineNumber 	=	""
		SenderLineValue 	=	""
		ReceiverReportID 	=	""
		ReceiverLineNumber 	=	""
		ReceiverLineValue 	=	""

		if Transfer.find('>') != -1:
			Str 	=	Transfer.split('>')

			if len(Str) == 2:
				Sender 		= 	Str[0]
				Receiver 	= 	Str[1]

				Sender 		=	Sender.split()

				if len(Sender) == 2:
					SenderReportID 		=	Sender[0]
					SenderLineNumber 	=	Sender[1]

					Value 	=	MKT_LINE_REPORT_DE.query.\
								filter(MKT_LINE_REPORT_DE.Report == SenderReportID).\
								filter(MKT_LINE_REPORT_DE.LineNumber == SenderLineNumber).\
								all()

					if Value:
						for item in Value:
							if item.LineValue:
								SenderLineValue = item.LineValue

				Receiver 	=	Receiver.split()

				if len(Receiver) == 2:

					ReceiverReportID 	=	Receiver[0]
					ReceiverLineNumber 	=	Receiver[1]

					ValueObj 	=	MKT_LINE_REPORT_DE.query.\
									filter(MKT_LINE_REPORT_DE.Report == ReceiverReportID).\
									filter(MKT_LINE_REPORT_DE.LineNumber == ReceiverLineNumber).\
									first()
					if ValueObj:
						ReceiverLineValue = ValueObj.LineValue

				Result 	= 	[True, SenderReportID, SenderLineNumber, SenderLineValue, ReceiverReportID, ReceiverLineNumber, ReceiverLineValue]

		return Result

	except Exception, e:
		raise

def getGLRecord(GL_Code):
	try:

		Record = MKT_GL_MAPPING.query.get(GL_Code)
		return Record

	except:
		raise

def getConsolKeyList(GL_Code):
	try:

		Record = MKT_GL_MAPPING_DE.query.\
				 filter(MKT_GL_MAPPING_DE.ID == GL_Code).\
				 all()

		Key = []
		if Record:
			for item in Record:
				Key.append(item.ConsolKey)

		return Key

	except:
		raise

def getConsolKeyBal(ConsolKey, Branch, Field="Balance"):
	try:

		Record = MKT_CONSOL_BALANCE.query.\
			  	 filter(MKT_CONSOL_BALANCE.ID == str(ConsolKey)).\
			  	 filter(MKT_CONSOL_BALANCE.Branch == Branch).\
			  	 first()

		Balance = 0
		if Record:

			Balance = float(getattr(Record, Field))

		return Balance

	except:
		raise

def getUpdateGLBalance(GL_Code, Branch, ConsolKeyObj="", GLKeyObj="", GLObj=""):
	try:

		Balance 				= 	0
		PrevMonthBal 			=	0
		CurrentPrevMonthBal 	=	0
		CurrentMonthBal 		=	0
		PrevYearBal 			=	0
		CurrentPrevYearBal 		=	0
		YTDBal 					=	0

		LCYBalance 				= 	0
		LCYPrevMonthBal 		=	0
		LCYCurrentPrevMonthBal 	=	0
		LCYCurrentMonthBal 		=	0
		LCYPrevYearBal 			=	0
		LCYCurrentPrevYearBal 	=	0
		LCYYTDBal 				=	0
		
		if GLObj:

			for row in GLObj:

				if row.ID == GL_Code:

					SubGLList = row.SubGL

					if SubGLList:

						SubGLList = str(SubGLList).split()

						for SubGLItem in SubGLList:

							getBalance 				=	getUpdateGLBalance(SubGLItem, Branch, ConsolKeyObj, GLKeyObj, GLObj)
							
							Balance 				= 	float(Balance) 				+ float(getBalance["Balance"])
							PrevMonthBal 			= 	float(PrevMonthBal) 		+ float(getBalance["PrevMonthBal"])
							CurrentPrevMonthBal 	=	float(CurrentPrevMonthBal) 	+ float(getBalance["CurrentPrevMonthBal"])
							CurrentMonthBal 		=	float(CurrentMonthBal) 		+ float(getBalance["CurrentMonthBal"])
							PrevYearBal 			=	float(PrevYearBal) 			+ float(getBalance["PrevYearBal"])
							CurrentPrevYearBal 		=	float(CurrentPrevYearBal) 	+ float(getBalance["CurrentPrevYearBal"])
							YTDBal 					=	float(YTDBal) 				+ float(getBalance["YTDBal"])

							LCYBalance 				= 	float(LCYBalance) 				+ float(getBalance["LCYBalance"])
							LCYPrevMonthBal 		= 	float(LCYPrevMonthBal) 			+ float(getBalance["LCYPrevMonthBal"])
							LCYCurrentPrevMonthBal 	=	float(LCYCurrentPrevMonthBal) 	+ float(getBalance["LCYCurrentPrevMonthBal"])
							LCYCurrentMonthBal 		=	float(LCYCurrentMonthBal) 		+ float(getBalance["LCYCurrentMonthBal"])
							LCYPrevYearBal 			=	float(LCYPrevYearBal) 			+ float(getBalance["LCYPrevYearBal"])
							LCYCurrentPrevYearBal 	=	float(LCYCurrentPrevYearBal) 	+ float(getBalance["LCYCurrentPrevYearBal"])
							LCYYTDBal 				=	float(LCYYTDBal) 				+ float(getBalance["LCYYTDBal"])

					else:

						if GLKeyObj:
							for item in GLKeyObj:
								if item.ID == GL_Code:
									for row1 in ConsolKeyObj:
										if row1.ID == item.ConsolKey and row1.Branch == Branch:

											Balance 				= 	float(Balance) 				+ float(row1.Balance)
											PrevMonthBal 			= 	float(PrevMonthBal) 		+ float(row1.PrevMonthBal)
											CurrentPrevMonthBal 	=	float(CurrentPrevMonthBal) 	+ float(row1.CurrentPrevMonthBal)
											CurrentMonthBal 		=	float(CurrentMonthBal) 		+ float(row1.CurrentMonthBal)
											PrevYearBal 			=	float(PrevYearBal) 			+ float(row1.PrevYearBal)
											CurrentPrevYearBal 		=	float(CurrentPrevYearBal) 	+ float(row1.CurrentPrevYearBal)
											YTDBal 					=	float(YTDBal) 				+ float(row1.YTDBal)

											LCYBalance 				= 	float(LCYBalance) 				+ float(row1.LCYBalance)
											LCYPrevMonthBal 		= 	float(LCYPrevMonthBal) 			+ float(row1.LCYPrevMonthBal)
											LCYCurrentPrevMonthBal 	=	float(LCYCurrentPrevMonthBal) 	+ float(row1.LCYCurrentPrevMonthBal)
											LCYCurrentMonthBal 		=	float(LCYCurrentMonthBal) 		+ float(row1.LCYCurrentMonthBal)
											LCYPrevYearBal 			=	float(LCYPrevYearBal) 			+ float(row1.LCYPrevYearBal)
											LCYCurrentPrevYearBal 	=	float(LCYCurrentPrevYearBal) 	+ float(row1.LCYCurrentPrevYearBal)
											LCYYTDBal 				=	float(LCYYTDBal) 				+ float(row1.LCYYTDBal)

		Results 	= 	{
							"Balance"				:	Balance,
							"PrevMonthBal"			:	PrevMonthBal,
							"CurrentPrevMonthBal"	:	CurrentPrevMonthBal,
							"CurrentMonthBal" 		:	CurrentMonthBal,
							"PrevYearBal" 			:	PrevYearBal,
							"CurrentPrevYearBal"	:	CurrentPrevYearBal,
							"YTDBal"				:	YTDBal,
							"LCYBalance"			:	LCYBalance,
							"LCYPrevMonthBal"		:	LCYPrevMonthBal,
							"LCYCurrentPrevMonthBal":	LCYCurrentPrevMonthBal,
							"LCYCurrentMonthBal" 	:	LCYCurrentMonthBal,
							"LCYPrevYearBal" 		:	LCYPrevYearBal,
							"LCYCurrentPrevYearBal"	:	LCYCurrentPrevYearBal,
							"LCYYTDBal"				:	LCYYTDBal
						}
		# print Results
		return Results

	except:
		raise

def getConsolBalanceDic():
	try:

		Record 	= MKT_CONSOL_BALANCE.query

		return Record

	except:
		raise

@app.route("/Morakot/TestGL/", methods=['GET'])
def getTestGL():
	try:
		Branch 		= 	session.get("ChangeBranch")
		Results 	=	[]
		getTreeGLCode(Branch)
		return ""

	except Exception, e:
		raise

def getTreeGLCode(Branch):
	try:
		
		Dic 		=	{}

		# GlObj 		= 	db.session.query(
		# 					MKT_GL_MAPPING.ID,
		# 					MKT_GL_MAPPING.SubGL,
		# 					MKT_GL_MAPPING.Currency,
		# 					MKT_GL_MAPPING_DE.ConsolKey
		# 				).\
		# 				join(
		# 					MKT_GL_MAPPING_DE,
		# 					MKT_GL_MAPPING_DE.ID == MKT_GL_MAPPING.ID
		# 				).\
		# 				filter(MKT_GL_MAPPING.SubGL == "").\
		# 				all()
		GlObj 		= 	db.session.query(
							MKT_GL_MAPPING.ID,
							MKT_GL_MAPPING.SubGL,
							MKT_GL_MAPPING.Currency
						).\
						filter(MKT_GL_MAPPING.SubGL == "").\
						all()

		if GlObj:

			for item in GlObj:
				# print "Key - %s" %item.ConsolKey
				Currency 	=	item.Currency
				ID 			=	item.ID

				DetailObj 	=	MKT_GL_MAPPING_DE.query.\
								filter(MKT_GL_MAPPING_DE.ID == ID).\
								all()

				ConsolKey 	=	[]
				if DetailObj:
					for row in DetailObj:
						ConsolKey.append(row.ConsolKey)

				ConsolObj 	=	MKT_CONSOL_BALANCE.query.\
								filter(MKT_CONSOL_BALANCE.ID.in_(ConsolKey)).\
								filter(MKT_CONSOL_BALANCE.Branch == Branch).\
								all()

				Balance 		=	0
				LCYBalance 		=	0
				CurrentMonthBal 	=	0
				LCYCurrentMonthBal 	=	0
				PrevMonthBal 		=	0
				LCYPrevMonthBal 	=	0
				CurrentPrevMonthBal 	=	0
				LCYCurrentPrevMonthBal 	=	0
				YTDBal 			=	0
				LCYYTDBal 		=	0
				CurrentPrevYearBal 		=	0
				LCYCurrentPrevYearBal 	=	0
				PrevYearBal 	=	0
				LCYPrevYearBal 	=	0

				if ConsolObj:

					for row in ConsolObj:

						Balance 				+=	float(row.Balance)
						LCYBalance 				+=	float(row.LCYBalance)
						CurrentMonthBal 		+=	float(row.CurrentMonthBal)
						LCYCurrentMonthBal 		+=	float(row.LCYCurrentMonthBal)
						PrevMonthBal 			+=	float(row.PrevMonthBal)
						LCYPrevMonthBal 		+=	float(row.LCYPrevMonthBal)
						CurrentPrevMonthBal 	+=	float(row.CurrentPrevMonthBal)
						LCYCurrentPrevMonthBal 	+=	float(row.LCYCurrentPrevMonthBal)
						YTDBal 					+=	float(row.YTDBal)
						LCYYTDBal 				+=	float(row.LCYYTDBal)
						CurrentPrevYearBal 		+=	float(row.CurrentPrevYearBal)
						LCYCurrentPrevYearBal 	+=	float(row.LCYCurrentPrevYearBal)
						PrevYearBal 			+=	float(row.PrevYearBal)
						LCYPrevYearBal 			+=	float(row.LCYPrevYearBal)

				# print "%s => %s = %s" %(item.ID, item.ConsolKey, Balance)
				# Add element to Dic
				Dic.update({ID:{
					'Currency':Currency,
					'Balance':str(Balance),
					'LCYBalance':str(LCYBalance),
					'CurrentMonthBal':str(CurrentMonthBal),
					'LCYCurrentMonthBal':str(LCYCurrentMonthBal),
					'PrevMonthBal':str(PrevMonthBal),
					'LCYPrevMonthBal':str(LCYPrevMonthBal),
					'CurrentPrevMonthBal':str(CurrentPrevMonthBal),
					'LCYCurrentPrevMonthBal':str(LCYCurrentPrevMonthBal),
					'YTDBal':str(YTDBal),
					'LCYYTDBal':str(LCYYTDBal),
					'CurrentPrevYearBal':str(CurrentPrevYearBal),
					'LCYCurrentPrevYearBal':str(LCYCurrentPrevYearBal),
					'PrevYearBal':str(PrevYearBal),
					'LCYPrevYearBal':str(LCYPrevYearBal)
				}})

				GL_BAL 	= 	MKT_GL_BALANCE.query.\
							filter(MKT_GL_BALANCE.ID == ID).\
							filter(MKT_GL_BALANCE.Branch == Branch).\
							first()

		if len(Dic) > 0:

			db.session.query(MKT_GL_BALANCE.ID).\
			filter(MKT_GL_BALANCE.Branch == Branch).\
			delete()

			for item in Dic:

				GL_BAL 	= 	MKT_GL_BALANCE.query.\
							filter(MKT_GL_BALANCE.ID == item).\
							filter(MKT_GL_BALANCE.Branch == Branch).\
							first()

				if GL_BAL:

					GL_BAL.Balance 				= 	Dic[item]['Balance']
					GL_BAL.PrevMonthBal			= 	Dic[item]['PrevMonthBal']
					GL_BAL.CurrentPrevMonthBal 	= 	Dic[item]['CurrentPrevMonthBal']
					GL_BAL.CurrentMonthBal 		= 	Dic[item]['CurrentMonthBal']
					GL_BAL.PrevYearBal 			=	Dic[item]['PrevYearBal']
					GL_BAL.CurrentPrevYearBal 	=	Dic[item]['CurrentPrevYearBal']
					GL_BAL.YTDBal 				= 	Dic[item]['YTDBal']

					GL_BAL.LCYCurrentPrevMonthBal 	=	Dic[item]['LCYCurrentPrevMonthBal']
					GL_BAL.LCYPrevYearBal			=	Dic[item]['LCYPrevYearBal']
					GL_BAL.LCYYTDBal				=	Dic[item]['LCYYTDBal']
					GL_BAL.LCYPrevMonthBal 			=	Dic[item]['LCYPrevMonthBal']
					GL_BAL.LCYCurrentMonthBal 		= 	Dic[item]['LCYCurrentMonthBal']
					GL_BAL.LCYCurrentPrevYearBal	=	Dic[item]['LCYCurrentPrevYearBal']
					GL_BAL.LCYBalance 				=	Dic[item]['LCYBalance']

					db.session.add(GL_BAL)

				else:

					ObjInsert 	= 	MKT_GL_BALANCE(
										ID 						=	item,
										Branch 					=	Branch,
										Currency 				=	Dic[item]['Currency'],
										CurrentPrevMonthBal 	=	Dic[item]['CurrentPrevMonthBal'],
										PrevYearBal				=	Dic[item]['PrevYearBal'],
										YTDBal					=	Dic[item]['YTDBal'],
										PrevMonthBal 			=	Dic[item]['PrevMonthBal'],
										CurrentMonthBal 		= 	Dic[item]['CurrentMonthBal'],
										CurrentPrevYearBal		=	Dic[item]['CurrentPrevYearBal'],
										Balance 				=	Dic[item]['Balance'],
										LCYCurrentPrevMonthBal 	=	Dic[item]['LCYCurrentPrevMonthBal'],
										LCYPrevYearBal			=	Dic[item]['LCYPrevYearBal'],
										LCYYTDBal				=	Dic[item]['LCYYTDBal'],
										LCYPrevMonthBal 		=	Dic[item]['LCYPrevMonthBal'],
										LCYCurrentMonthBal 		= 	Dic[item]['LCYCurrentMonthBal'],
										LCYCurrentPrevYearBal	=	Dic[item]['LCYCurrentPrevYearBal'],
										LCYBalance 				=	Dic[item]['LCYBalance']
									)

					db.session.add(ObjInsert)

			getSubGLUpdate(Dic, Branch)
			# Commit once updated
			print "GL was successfully updated for %s." %Branch

		return ""

	except Exception, e:
		raise

def getSubGLUpdate(Dic, Branch):
	try:
		
		GlObj 		= 	db.session.query(
							MKT_GL_MAPPING.ID,
							MKT_GL_MAPPING.SubGL,
							MKT_GL_MAPPING.Currency,
							MKT_GL_MAPPING_DE.ConsolKey
						).\
						join(
							MKT_GL_MAPPING_DE,
							MKT_GL_MAPPING_DE.ID == MKT_GL_MAPPING.ID
						).\
						filter(MKT_GL_MAPPING.SubGL != "").\
						all()

		CheckList 	=	{}
		Listed 		=	{}

		if GlObj:

			for item in GlObj:
				
				SubGLList 	=	item.SubGL
				ID 			=	item.ID
				Currency 	=	item.Currency

				Balance 				=	0
				LCYBalance 				=	0
				CurrentMonthBal 		=	0
				LCYCurrentMonthBal 		=	0
				PrevMonthBal 			=	0
				LCYPrevMonthBal 		=	0
				CurrentPrevMonthBal 	=	0
				LCYCurrentPrevMonthBal 	=	0
				YTDBal 					=	0
				LCYYTDBal 				=	0
				CurrentPrevYearBal 		=	0
				LCYCurrentPrevYearBal 	=	0
				PrevYearBal 			=	0
				LCYPrevYearBal 			=	0
				CheckList.update({ID:str(SubGLList)})
				if SubGLList:
					SubGLList 	=	SubGLList.split()
					for val in SubGLList:
						
						if val in Dic:
							# Currency 				= Dic[val]['Currency']
							Balance 				+= float(Dic[val]['Balance'])
							LCYBalance 				+= float(Dic[val]['LCYBalance'])
							CurrentMonthBal 		+= float(Dic[val]['CurrentMonthBal'])
							LCYCurrentMonthBal 		+= float(Dic[val]['LCYCurrentMonthBal'])
							PrevMonthBal 			+= float(Dic[val]['PrevMonthBal'])
							LCYPrevMonthBal 		+= float(Dic[val]['LCYPrevMonthBal'])
							CurrentPrevMonthBal 	+= float(Dic[val]['CurrentPrevMonthBal'])
							LCYCurrentPrevMonthBal 	+= float(Dic[val]['LCYCurrentPrevMonthBal'])
							YTDBal 					+= float(Dic[val]['YTDBal'])
							LCYYTDBal 				+= float(Dic[val]['LCYYTDBal'])
							CurrentPrevYearBal 		+= float(Dic[val]['CurrentPrevYearBal'])
							LCYCurrentPrevYearBal 	+= float(Dic[val]['LCYCurrentPrevYearBal'])
							PrevYearBal 			+= float(Dic[val]['PrevYearBal'])
							LCYPrevYearBal 			+= float(Dic[val]['LCYPrevYearBal'])

				# Add element to Dic
				Listed.update({ID:{
					'Currency':Currency,
					'Balance':str(Balance),
					'LCYBalance':str(LCYBalance),
					'CurrentMonthBal':str(CurrentMonthBal),
					'LCYCurrentMonthBal':str(LCYCurrentMonthBal),
					'PrevMonthBal':str(PrevMonthBal),
					'LCYPrevMonthBal':str(LCYPrevMonthBal),
					'CurrentPrevMonthBal':str(CurrentPrevMonthBal),
					'LCYCurrentPrevMonthBal':str(LCYCurrentPrevMonthBal),
					'YTDBal':str(YTDBal),
					'LCYYTDBal':str(LCYYTDBal),
					'CurrentPrevYearBal':str(CurrentPrevYearBal),
					'LCYCurrentPrevYearBal':str(LCYCurrentPrevYearBal),
					'PrevYearBal':str(PrevYearBal),
					'LCYPrevYearBal':str(LCYPrevYearBal)
				}})

		if len(Listed) > 0:

			for item in Listed:

				# print "- %s => %s = %s" %(item, CheckList[item], Listed[item]['Balance'])
				# db.session.query(MKT_GL_BALANCE.ID).\
				# filter(MKT_GL_BALANCE.ID == item).\
				# filter(MKT_GL_BALANCE.Branch == Branch).\
				# delete()
				
				SubGLListUpdate =	getLastGLBalance(item, CheckList[item], Listed)

				GL_BAL 	= 	MKT_GL_BALANCE.query.\
							filter(MKT_GL_BALANCE.ID == item).\
							filter(MKT_GL_BALANCE.Branch == Branch).\
							first()

				if GL_BAL:

					GL_BAL.Balance 				= 	Dic[item]['Balance']
					GL_BAL.PrevMonthBal			= 	Dic[item]['PrevMonthBal']
					GL_BAL.CurrentPrevMonthBal 	= 	Dic[item]['CurrentPrevMonthBal']
					GL_BAL.CurrentMonthBal 		= 	Dic[item]['CurrentMonthBal']
					GL_BAL.PrevYearBal 			=	Dic[item]['PrevYearBal']
					GL_BAL.CurrentPrevYearBal 	=	Dic[item]['CurrentPrevYearBal']
					GL_BAL.YTDBal 				= 	Dic[item]['YTDBal']

					GL_BAL.LCYCurrentPrevMonthBal 	=	Dic[item]['LCYCurrentPrevMonthBal']
					GL_BAL.LCYPrevYearBal			=	Dic[item]['LCYPrevYearBal']
					GL_BAL.LCYYTDBal				=	Dic[item]['LCYYTDBal']
					GL_BAL.LCYPrevMonthBal 			=	Dic[item]['LCYPrevMonthBal']
					GL_BAL.LCYCurrentMonthBal 		= 	Dic[item]['LCYCurrentMonthBal']
					GL_BAL.LCYCurrentPrevYearBal	=	Dic[item]['LCYCurrentPrevYearBal']
					GL_BAL.LCYBalance 				=	Dic[item]['LCYBalance']

					db.session.add(GL_BAL)

				else:

					ObjInsert 	= 	MKT_GL_BALANCE(
										ID 						=	item,
										Branch 					=	Branch,
										Currency 				=	SubGLListUpdate[item]['Currency'],
										CurrentPrevMonthBal 	=	SubGLListUpdate[item]['CurrentPrevMonthBal'],
										PrevYearBal				=	SubGLListUpdate[item]['PrevYearBal'],
										YTDBal					=	SubGLListUpdate[item]['YTDBal'],
										PrevMonthBal 			=	SubGLListUpdate[item]['PrevMonthBal'],
										CurrentMonthBal 		= 	SubGLListUpdate[item]['CurrentMonthBal'],
										CurrentPrevYearBal		=	SubGLListUpdate[item]['CurrentPrevYearBal'],
										Balance 				=	SubGLListUpdate[item]['Balance'],
										LCYCurrentPrevMonthBal 	=	SubGLListUpdate[item]['LCYCurrentPrevMonthBal'],
										LCYPrevYearBal			=	SubGLListUpdate[item]['LCYPrevYearBal'],
										LCYYTDBal				=	SubGLListUpdate[item]['LCYYTDBal'],
										LCYPrevMonthBal 		=	SubGLListUpdate[item]['LCYPrevMonthBal'],
										LCYCurrentMonthBal 		= 	SubGLListUpdate[item]['LCYCurrentMonthBal'],
										LCYCurrentPrevYearBal	=	SubGLListUpdate[item]['LCYCurrentPrevYearBal'],
										LCYBalance 				=	SubGLListUpdate[item]['LCYBalance']
									)

					db.session.add(ObjInsert)

		return Listed

	except Exception, e:
		raise

def getLastGLBalance(Value, CheckList, ListValue):
	try:

		Result 		=	{}

		if CheckList:

			Currency 				=	""
			Balance 				= 	float(ListValue[Value]['Balance'])
			LCYBalance 				= 	float(ListValue[Value]['LCYBalance'])
			CurrentMonthBal 		= 	float(ListValue[Value]['CurrentMonthBal'])
			LCYCurrentMonthBal 		= 	float(ListValue[Value]['LCYCurrentMonthBal'])
			PrevMonthBal 			= 	float(ListValue[Value]['PrevMonthBal'])
			LCYPrevMonthBal 		= 	float(ListValue[Value]['LCYPrevMonthBal'])
			CurrentPrevMonthBal 	= 	float(ListValue[Value]['CurrentPrevMonthBal'])
			LCYCurrentPrevMonthBal 	= 	float(ListValue[Value]['LCYCurrentPrevMonthBal'])
			YTDBal 					= 	float(ListValue[Value]['YTDBal'])
			LCYYTDBal 				= 	float(ListValue[Value]['LCYYTDBal'])
			CurrentPrevYearBal 		= 	float(ListValue[Value]['CurrentPrevYearBal'])
			LCYCurrentPrevYearBal 	= 	float(ListValue[Value]['LCYCurrentPrevYearBal'])
			PrevYearBal 			= 	float(ListValue[Value]['PrevYearBal'])
			LCYPrevYearBal 			= 	float(ListValue[Value]['LCYPrevYearBal'])
			CheckList 				= 	CheckList.split()
			Currency 				= 	ListValue[Value]['Currency']
			for item in CheckList:
				if item in ListValue:
					
					Balance 				+= float(ListValue[item]['Balance'])
					LCYBalance 				+= float(ListValue[item]['LCYBalance'])
					CurrentMonthBal 		+= float(ListValue[item]['CurrentMonthBal'])
					LCYCurrentMonthBal 		+= float(ListValue[item]['LCYCurrentMonthBal'])
					PrevMonthBal 			+= float(ListValue[item]['PrevMonthBal'])
					LCYPrevMonthBal 		+= float(ListValue[item]['LCYPrevMonthBal'])
					CurrentPrevMonthBal 	+= float(ListValue[item]['CurrentPrevMonthBal'])
					LCYCurrentPrevMonthBal 	+= float(ListValue[item]['LCYCurrentPrevMonthBal'])
					YTDBal 					+= float(ListValue[item]['YTDBal'])
					LCYYTDBal 				+= float(ListValue[item]['LCYYTDBal'])
					CurrentPrevYearBal 		+= float(ListValue[item]['CurrentPrevYearBal'])
					LCYCurrentPrevYearBal 	+= float(ListValue[item]['LCYCurrentPrevYearBal'])
					PrevYearBal 			+= float(ListValue[item]['PrevYearBal'])
					LCYPrevYearBal 			+= float(ListValue[item]['LCYPrevYearBal'])
					# print "==> %s = %s" %(item, ListValue[item]['Balance'])

			Result.update({Value:{
				'Currency':Currency,
				'Balance':str(Balance),
				'LCYBalance':str(LCYBalance),
				'CurrentMonthBal':str(CurrentMonthBal),
				'LCYCurrentMonthBal':str(LCYCurrentMonthBal),
				'PrevMonthBal':str(PrevMonthBal),
				'LCYPrevMonthBal':str(LCYPrevMonthBal),
				'CurrentPrevMonthBal':str(CurrentPrevMonthBal),
				'LCYCurrentPrevMonthBal':str(LCYCurrentPrevMonthBal),
				'YTDBal':str(YTDBal),
				'LCYYTDBal':str(LCYYTDBal),
				'CurrentPrevYearBal':str(CurrentPrevYearBal),
				'LCYCurrentPrevYearBal':str(LCYCurrentPrevYearBal),
				'PrevYearBal':str(PrevYearBal),
				'LCYPrevYearBal':str(LCYPrevYearBal)
			}})

		return Result

	except:
		raise

def getGLKeyObj():
	try:

		Record = MKT_GL_MAPPING_DE.query

		return Record

	except:
		raise
		
def getGLRecordObj():
	try:

		return MKT_GL_MAPPING.query

	except:
		raise

def setGLBalance(Branch=""):
	try:

		BankDate 		= 	mktdate.getBankDate()
		BankDate 		= 	str(BankDate)
		Day 			= 	BankDate.split("-")[2]
		Day 			= 	Day.strip()

		EndMonth 		= 	mktdate.getBankDateObj().NextMonthEnd
		EndMonth 		= 	str(EndMonth)
		DayEndMonth 	= 	EndMonth.split("-")[2]
		DayEndMonth 	= 	DayEndMonth.strip()

		YearEnd 		=	mktdate.getBankDateObj().NextYearEnd
		DayEndYear 		=	str(YearEnd).split("-")[2]
		DayEndYear 		=	DayEndYear.strip()

		GL_MAPPING 		= 	MKT_GL_MAPPING.query.\
							order_by(asc(MKT_GL_MAPPING.ID)).\
							all()

		ConsolKeyObj 	=	getConsolBalanceDic()
		GLKeyObj 		=	getGLKeyObj()
		GLObj 			=	getGLRecordObj()

		if GL_MAPPING:
			for row in GL_MAPPING:

				Currency 			= 	row.Currency
				GL_Code 			= 	row.ID
				# getBalance 			=	getUpdateGLBalance(GL_Code, Branch, ConsolKeyObj, GLKeyObj, GLObj)
				
				# Balance 				= 	getBalance["Balance"]
				# PrevMonthBal 			= 	getBalance["PrevMonthBal"]
				# CurrentPrevMonthBal 	=	getBalance["CurrentPrevMonthBal"]
				# CurrentMonthBal 		=	getBalance["CurrentMonthBal"]
				# PrevYearBal 			=	getBalance["PrevYearBal"]
				# CurrentPrevYearBal 		=	getBalance["CurrentPrevYearBal"]
				# YTDBal 					=	getBalance["YTDBal"]

				# LCYBalance 				= 	getBalance["LCYBalance"]
				# LCYPrevMonthBal 		= 	getBalance["LCYPrevMonthBal"]
				# LCYCurrentPrevMonthBal 	=	getBalance["LCYCurrentPrevMonthBal"]
				# LCYCurrentMonthBal 		=	getBalance["LCYCurrentMonthBal"]
				# LCYPrevYearBal 			=	getBalance["LCYPrevYearBal"]
				# LCYCurrentPrevYearBal 	=	getBalance["LCYCurrentPrevYearBal"]
				# LCYYTDBal 				=	getBalance["LCYYTDBal"]

				Balance 				= 	0
				PrevMonthBal 			= 	0
				CurrentPrevMonthBal 	=	0
				CurrentMonthBal 		=	0
				PrevYearBal 			=	0
				CurrentPrevYearBal 		=	0
				YTDBal 					=	0

				LCYBalance 				= 	0
				LCYPrevMonthBal 		= 	0
				LCYCurrentPrevMonthBal 	=	0
				LCYCurrentMonthBal 		=	0
				LCYPrevYearBal 			=	0
				LCYCurrentPrevYearBal 	=	0
				LCYYTDBal 				=	0

				GL_BAL 	= 	MKT_GL_BALANCE.query.\
							filter(MKT_GL_BALANCE.ID == GL_Code).\
							filter(MKT_GL_BALANCE.Branch == Branch).\
							first()

				if not GL_BAL:
					
					Insert = MKT_GL_BALANCE(
								ID 						=	GL_Code,
								Branch 					=	Branch,
								Currency 				=	Currency,
								CurrentPrevMonthBal 	=	float(CurrentPrevMonthBal),
								PrevYearBal				=	float(PrevYearBal),
								YTDBal					=	float(YTDBal),
								PrevMonthBal 			=	float(PrevMonthBal),
								CurrentMonthBal 		= 	float(CurrentMonthBal),
								CurrentPrevYearBal		=	float(CurrentPrevYearBal),
								Balance 				=	float(Balance),
								LCYCurrentPrevMonthBal 	=	float(LCYCurrentPrevMonthBal),
								LCYPrevYearBal			=	float(LCYPrevYearBal),
								LCYYTDBal				=	float(LCYYTDBal),
								LCYPrevMonthBal 		=	float(LCYPrevMonthBal),
								LCYCurrentMonthBal 		= 	float(LCYCurrentMonthBal),
								LCYCurrentPrevYearBal	=	float(LCYCurrentPrevYearBal),
								LCYBalance 				=	float(LCYBalance)
							 )
					db.session.add(Insert)
					
				else:

					GL_BAL.Balance 				= 	float(Balance)
					GL_BAL.PrevMonthBal			= 	float(PrevMonthBal)
					GL_BAL.CurrentPrevMonthBal 	= 	float(CurrentPrevMonthBal)
					GL_BAL.CurrentMonthBal 		= 	float(CurrentMonthBal)
					GL_BAL.PrevYearBal 			=	float(PrevYearBal)
					GL_BAL.CurrentPrevYearBal 	=	float(CurrentPrevYearBal)
					GL_BAL.YTDBal 				= 	float(YTDBal)

					GL_BAL.LCYCurrentPrevMonthBal 	=	float(LCYCurrentPrevMonthBal)
					GL_BAL.LCYPrevYearBal			=	float(LCYPrevYearBal)
					GL_BAL.LCYYTDBal				=	float(LCYYTDBal)
					GL_BAL.LCYPrevMonthBal 			=	float(LCYPrevMonthBal)
					GL_BAL.LCYCurrentMonthBal 		= 	float(LCYCurrentMonthBal)
					GL_BAL.LCYCurrentPrevYearBal	=	float(LCYCurrentPrevYearBal)
					GL_BAL.LCYBalance 				=	float(LCYBalance)

					db.session.add(GL_BAL)

				db.session.commit()
				
		else:
			print "GL mapping not found."

	except:
		db.session.rollback()
		raise

def getVerifiedGL():
	try:

		results = db.session.query(MKT_CONSOL_BALANCE.ID.label('ConsolKey'), MKT_CONSOL_BALANCE.Balance, MKT_CONSOL_BALANCE.Currency).\
				  filter(~MKT_CONSOL_BALANCE.ID.in_(db.session.query(MKT_GL_MAPPING_DE.ConsolKey))).\
				  all()

		return results

	except:
		raise