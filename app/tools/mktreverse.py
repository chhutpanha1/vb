'''
Created Date: 08 Apr 2015
Author: Theavuth NHEL + Sovankiry RIM

Modified by: 
Modified Date: 
All Right Reserved Morakot Technology Co.,Ltd.
Description : mktreverse.py Refer to use for reverse LIVE RECORD to HISTORY RECORD and DESTROY HISTORY 
'''
from flask 						import session
from app.mktcore.wtfimports 	import *
from app.urlregister 			import *
from app 						import app, db
from sqlalchemy 				import inspect

from app.User.models 			import MKT_USER_HIST

import mktaudit 				as mktaudit

import operator

class Struct:
    def __init__(self, **entries): 
        self.__dict__.update(entries)

class clsReverse():

	@staticmethod
	def setReverse(Table, ID, Table2=None):
		
		# Record = eval(Table).query.get(ID)
		# Mapper = inspect(eval(Table+"_HIST"))
		# HIST = None
		# # for row in Record:
		# Data = {}
		# FieldInTable = {}
		# for item in Mapper.attrs:
		# 	Data.update({item.key:str(getattr(Record, item.key))})
			# FieldInTable.update({item.key:''})
		
		# s = Struct(**Data)
		# insert = InsertFromSelect(eval(Table+"_HIST"), Record)
		# print insert
		# Record = eval(Table).query.get(ID)
		# if Record:
		# 	FieldInTable = []
		# 	Mapper = inspect(eval(Table))
		# 	Data = []
		# 	for item in Mapper.attrs:
		# 		FieldInTable.append(str(getattr(Record,item.key)))
		# 		# Obj = item.key
		# 		# Data[Obj] = Record.Obj
		# 		# Data.update({item.key:str(getattr(Record,item.key))})
			
		# 	# Hist = operator.attrgetter(Table + "_HIST")(FieldInTable)
		# 	# Hist = operator.attrgetter("MKT_USER_HIST")
		# 	# QueryHist = MKT_USER_HIST(ID='001',Status='AUTH')
		# 	# db.session.add(QueryHist)
		# 	# MKT_USER_HIST()
		# 	print str(MKT_USER_HIST)

		db.session.query(eval(Table)).filter_by(ID=ID).delete(synchronize_session="fetch")

def moveAUTHToHIST(LIVE, HIST):
	try:
		dic 			= {}
		FieldInTable 	= []
		Mapper 			= inspect(LIVE)
		for item in Mapper.attrs:						
			FieldInTable.append(item.key)
		
		Curr 		= '0'
		QueryObj 	= LIVE.query

		if QueryObj:
			for row in QueryObj:
				for col in FieldInTable:
					if col =='ID':
						dic.update({col:str(getattr(row, col)) + '@' + Curr})
					else:
						dic.update({col:getattr(row, col)})

				AddHistObj = HIST(**dic)
				db.session.add(AddHistObj)

	except Exception, e:
		db.session.rollback()
		raise

def deleteRecord(Table):
	try:

		db.session.query(Table).delete()

	except:
		db.session.rollback()
		raise

def clearFTJETTtoHist():
	try:

		# Move MKT_JOURNAL_ENTRY record per day to MKT_JOURNAL_ENTRY_HIST
		moveAUTHToHIST(MKT_JOURNAL_ENTRY, MKT_JOURNAL_ENTRY_HIST)
		# Delete MKT_JOURNAL_ENTRY record per day
		deleteRecord(MKT_JOURNAL_ENTRY)

		# Move MKT_FUND_TRANSFER record per day to MKT_FUND_TRANSFER_HIST; Fund Transfer, Fund Deposit, Fund Withdrawal
		moveAUTHToHIST(MKT_FUND_TRANSFER, MKT_FUND_TRANSFER_HIST)
		# Delete MKT_FUND_TRANSFER record per day; Fund Transfer, Fund Deposit, Fund Withdrawal
		deleteRecord(MKT_FUND_TRANSFER)

		# Move MKT_TELLER record per day to MKT_TELLER_HIST
		moveAUTHToHIST(MKT_TELLER, MKT_TELLER_HIST)
		# Delete MKT_TELLER record per day
		deleteRecord(MKT_TELLER)

		db.session.commit()

		return ""

	except:
		db.session.rollback()
		raise