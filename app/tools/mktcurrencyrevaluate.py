# Created by 	: Theavuth NHEL
# Date: Date 	: 17 July 2015
# Modify Date 	: 21 July 2015
# Modify by 	: Theavuth NHEL
# All Right Reserved << Morakot Technology Co,Ltd. >>

from app.mktcore.wtfimports 	import *
from .. 						import app, db
from decimal 					import *

import mktmoney 				as mktmoney 
import mktdate 					as mktdate

import mktgl 					as mktgl
import app.tools.mktautoid 		as mktAutoID
import mktsetting 				as mktsetting
import mktaccounting 			as mktaccounting
import mktmessage 				as mktmessage

# Get the Category List for Revaluation
def getRevaluateCategory():
	try:

		getAccSetting 	= 	mktsetting.getAccSetting()
		CategoryType 	= 	getAccSetting.RevCatType
		ListCategory 	=	[]

		if CategoryType:

			CategoryType = CategoryType.split()

			Category 	=	MKT_CATEGORY.query.\
							filter(MKT_CATEGORY.CategoryType.in_(CategoryType)).\
							all()

			if Category:

				for item in Category:
					ListCategory.append(str(item.ID))

		return ListCategory

	except:
		raise

def getCategoryType(Category):
	try:

		getCategory 	=	MKT_CATEGORY.query.get(Category)
		CategoryType 	=	""

		if getCategory:
			CategoryType = getCategory.CategoryType

		return CategoryType

	except:
		raise

def getPositionCategory(Currency, BaseCurrency=""):
	try:

		getAccSetting 	= 	mktsetting.getAccSetting()
		PositionCat 	=	getAccSetting.PositionCat
		PositionCat 	=	PositionCat.split()
		Category 		=	""

		if len(PositionCat) > 0:

			for item in PositionCat:

				CheckCurrency = item[:3]

				if BaseCurrency:

					if CheckCurrency.upper() == BaseCurrency.upper():

						Category = item[3:]

						break

				else:

					if CheckCurrency.upper() == Currency.upper():

						Category = item[3:]
						
						break

		return Category

	except:
		raise

# Booking Currency Revaluation
def setCategoryRevaluation(Category, Amount, Currency, Branch, LCYAmount):
	# Status, Curr, Inputter, Createdon, Authorizer, Authorizeon, JNID, Category, DrCr, Amount, Module, Transaction, TransactionDate, Reference, Currency, Branch, GL_KEYS, LCYAmount
	try:

		EOD 			= 	1
		getAccSetting 	= 	mktsetting.getAccSetting()
		Revaluation 	= 	getAccSetting.Revaluation
		BaseCurrency 	= 	getAccSetting.BaseCurrency
		Reference 		= 	Category
		PositionCat 	=	getAccSetting.PositionCat
		EquivalentCat 	=	getAccSetting.EquivalentCat

		if Revaluation.upper() in 'Y': # Check setting allow revaluation or not

			if EquivalentCat and PositionCat:

				# ListCategory 	= 	getRevaluateCategory()
				CategoryTypeList 	=	getAccSetting.RevCatType
				CategoryTypeList 	=	CategoryTypeList.split()
				CategoryType 		=	getCategoryType(Category)

				if len(CategoryTypeList) > 0:

					MatchCategory 	= 	[item for item in CategoryTypeList if CategoryType in item]

					if len(MatchCategory) > 0:

						if Currency and Currency.upper() not in BaseCurrency.upper():

							TranDate 	= mktdate.getBankDate()
							Transaction = getAccSetting.PositionTran
							DrCr 		= 'Dr'
							JNID 		= mktaccounting.getJournalAutoID()
							DateTimeNow = mktdate.getDateTimeNow()
							GL_KEYS 	= mktaccounting.getConsolKey(EquivalentCat, BaseCurrency, "", "CR", "", "", "", "", "", "", "", "", "")
							mktaccounting.insert_MKT_JOURNAL(
																"AUTH", 			# Record Status
																"0", 				# Record Current Version
																"Systme", 			# Record Inputter
																DateTimeNow, 		# Record Created On
																"Systme", 			# Authorizer Record
																DateTimeNow, 		# Record Authorized On
																JNID, 				# Record ID
																EquivalentCat, 		# Category
																DrCr, 				# Debit/Credit
																LCYAmount, 			# Amount as Decimal
																"CR", 				# Module [LC, PD, FT, ...]
																Transaction, 		# Transaction Code
																TranDate, 			# Transaction Date
																Reference, 			# Reference, Where record effected from
																Currency, 			# Currency [KHR, USD, ...]
																Branch, 			# Branch, Posting branch
																GL_KEYS, 			# GL_KEYS get from mktaccounting.getConsolKey
																LCYAmount 			# LCYAmount from the amount has calculated with reporting rate
															)
							# Calculate consolidate balance for position base currency category
							mktgl.setConsolBalance(GL_KEYS, Branch, Currency, LCYAmount, EquivalentCat, DrCr, "CR", TranDate, LCYAmount)
							JNID 		= mktaccounting.getJournalAutoID()
							DateTimeNow = mktdate.getDateTimeNow()
							DrCr 		= 'Cr'
							GL_KEYS 	= mktaccounting.getConsolKey(PositionCat, Currency, "", "CR", "", "", "", "", "", "", "", "", "")
							mktaccounting.insert_MKT_JOURNAL(
																"AUTH", 			# Record Status
																"0", 				# Record Current Version
																"Systme", 			# Record Inputter
																DateTimeNow, 		# Record Created On
																"Systme", 			# Authorizer Record
																DateTimeNow, 		# Record Authorized On
																JNID, 				# Record ID
																PositionCat, 		# Category
																DrCr, 				# Debit/Credit
																Amount, 			# Amount as Decimal
																"CR", 				# Module [LC, PD, FT, ...]
																Transaction, 		# Transaction Code
																TranDate, 			# Transaction Date
																Reference, 			# Reference, Where record effected from
																Currency, 			# Currency [KHR, USD, ...]
																Branch, 			# Branch, Posting branch
																GL_KEYS, 			# GL_KEYS get from mktaccounting.getConsolKey
																LCYAmount 			# LCYAmount from the amount has calculated with reporting rate
															)
							# Calculate consolidate balance for position by currency category
							mktgl.setConsolBalance(GL_KEYS, Branch, Currency, Amount, PositionCat, DrCr, "CR", TranDate, LCYAmount)
				else:

					# Call method for error message
					error_msg = "Category type not found."
					mktmessage.msgError(EOD, error_msg)
			else:
				# Call method for error message
				error_msg = "Equivalent or position category not found."
				mktmessage.msgError(EOD, error_msg)

		return True

	except:
		db.session.rollback()
		return False

def getAllBranch():
	try:

		return MKT_BRANCH.query

	except:
		raise

def getConsolBalanceObj():
	try:

		return MKT_CONSOL_BALANCE.query

	except:
		raise

def getAllCurrencyWithoutBaseCurrency():
	try:

		getAccSetting 	= 	mktsetting.getAccSetting()
		BaseCurrency 	= 	getAccSetting.BaseCurrency

		CurrencyObj 	= 	MKT_CURRENCY.query.\
							filter(MKT_CURRENCY.ID != str(BaseCurrency)).\
							all()

		return CurrencyObj

	except:
		raise

def setEODRevaluationCurrency():
	try:

		getAccSetting 	= 	mktsetting.getAccSetting()
		Revaluation 	= 	getAccSetting.Revaluation
		BaseCurrency 	= 	getAccSetting.BaseCurrency

		getObjBranch 	=	getAllBranch()
		BaseCategory 	=	getPositionCategory("", BaseCurrency)

		EquivalentCat 	=	getAccSetting.EquivalentCat
		PositionCat 	=	getAccSetting.PositionCat
		GainOnExcCat 	=	getAccSetting.GainOnExcCat
		LossOnExcCat 	=	getAccSetting.LossOnExcCat

		if GainOnExcCat and LossOnExcCat:

			CurrencyObj 	=	getAllCurrencyWithoutBaseCurrency()

			if CurrencyObj:

				for item in CurrencyObj:

					CheckCurrency = item.ID

					if BaseCurrency.upper() not in CheckCurrency.upper():
					
						BaseConsolKey 	= 	mktaccounting.getConsolKey(EquivalentCat, CheckCurrency, "", "CR", "", "", "", "", "", "", "", "", "")
						CheckConsolKey 	= 	mktaccounting.getConsolKey(PositionCat, CheckCurrency, "", "CR", "", "", "", "", "", "", "", "", "")

						if getObjBranch:

							NewReportingRate 	= 	mktaccounting.getReportingRate(CheckCurrency)

							for row in getObjBranch:

								Branch 			=	row.ID
								BaseConsolBal 	= 	MKT_CONSOL_BALANCE.query.\
													filter(MKT_CONSOL_BALANCE.ID == BaseConsolKey).\
													filter(MKT_CONSOL_BALANCE.Branch == Branch).\
													first()

								ConsolBal 	= 	MKT_CONSOL_BALANCE.query.\
												filter(MKT_CONSOL_BALANCE.ID == CheckConsolKey).\
												filter(MKT_CONSOL_BALANCE.Branch == Branch).\
												first()

								if BaseConsolBal and ConsolBal:
									
									FCY 		=	float(ConsolBal.Balance) if ConsolBal.Balance else float(0)
									LCYBalance 	=	float(BaseConsolBal.Balance) if BaseConsolBal.Balance else float(0)
									
									if FCY > 0:

										NewLCYBalance 	= 	float(FCY) * float(NewReportingRate)
										DiffAmount 		=	float(NewLCYBalance) - float(LCYBalance)
										
										if float(DiffAmount) != 0:

											OriginAmt 		=	DiffAmount
											
											if float(DiffAmount) > 0:
												DrCategory 	= 	EquivalentCat
												CrCategory 	= 	GainOnExcCat
											else:
												DrCategory 	= 	LossOnExcCat
												CrCategory 	= 	EquivalentCat
												DiffAmount 	= 	abs(DiffAmount)

											NewAmount 	=	mktmoney.toMoney(float(DiffAmount), mktmoney.getCurrencyObj(BaseCurrency))
											NewAmount 	=	str(NewAmount).replace(",", "")
											
											if float(NewAmount) > 0:
											
												Reference 	=	PositionCat
												Currency 	=	CheckCurrency
												TranDate 	= 	mktdate.getBankDate()
												Transaction = 	getAccSetting.RevTran
												DateTimeNow = 	mktdate.getDateTimeNow()

												DrCr 		= 	'Dr'
												JNID 		= 	mktaccounting.getJournalAutoID()
												GL_KEYS 	= 	mktaccounting.getConsolKey(DrCategory, BaseCurrency, "", "CR", "", "", "", "", "", "", "", "", "")
												# Post to Journal, Debit: Position KHR or Loss on exchange depend on DiffAmount(-, +)
												mktaccounting.insert_MKT_JOURNAL(
																					"AUTH", 			# Record Status
																					"0", 				# Record Current Version
																					"Systme", 			# Record Inputter
																					DateTimeNow, 		# Record Created On
																					"Systme", 			# Authorizer Record
																					DateTimeNow, 		# Record Authorized On
																					JNID, 				# Record ID
																					DrCategory, 		# Category
																					DrCr, 				# Debit/Credit
																					Decimal(NewAmount), # Amount as Decimal
																					"CR", 				# Module [LC, PD, FT, ...]
																					Transaction, 		# Transaction Code
																					TranDate, 			# Transaction Date
																					Reference, 			# Reference, Where record effected from
																					Currency, 			# Currency [KHR, USD, ...]
																					Branch, 			# Branch, Posting branch
																					GL_KEYS, 			# GL_KEYS get from mktaccounting.getConsolKey
																					Decimal(NewAmount) 	# LCYAmount from the amount has calculated with reporting rate
																				)

												# Calculate consolidate balance for position base currency category
												mktgl.setConsolBalance(GL_KEYS, Branch, Currency, Decimal(NewAmount), DrCategory, DrCr, "CR", TranDate, Decimal(NewAmount))

												DrCr 		= 'Cr'
												JNID 		= mktaccounting.getJournalAutoID()
												GL_KEYS 	= mktaccounting.getConsolKey(CrCategory, BaseCurrency, "", "CR", "", "", "", "", "", "", "", "", "")
												# Post to Journal, Credit: Gain on exchange or Position KHR(Base Currency)  depend on DiffAmount(-, +)
												mktaccounting.insert_MKT_JOURNAL(
																					"AUTH", 			# Record Status
																					"0", 				# Record Current Version
																					"Systme", 			# Record Inputter
																					DateTimeNow, 		# Record Created On
																					"Systme", 			# Authorizer Record
																					DateTimeNow, 		# Record Authorized On
																					JNID, 				# Record ID
																					CrCategory, 		# Category
																					DrCr, 				# Debit/Credit
																					Decimal(NewAmount), # Amount as Decimal
																					"CR", 				# Module [LC, PD, FT, ...]
																					Transaction, 		# Transaction Code
																					TranDate, 			# Transaction Date
																					Reference, 			# Reference, Where record effected from
																					Currency, 			# Currency [KHR, USD, ...]
																					Branch, 			# Branch, Posting branch
																					GL_KEYS, 			# GL_KEYS get from mktaccounting.getConsolKey
																					Decimal(NewAmount) 	# LCYAmount from the amount has calculated with reporting rate
																				)

												# Calculate consolidate balance for position base currency category
												mktgl.setConsolBalance(GL_KEYS, Branch, Currency, Decimal(NewAmount), CrCategory, DrCr, "CR", TranDate, Decimal(NewAmount))

												# db.session.commit()
												print "Revaluation on currency %s. Amount %s." %(Currency, mktmoney.toMoney(float(OriginAmt), mktmoney.getCurrencyObj(BaseCurrency), 2))

			else:

				# Call method for error message
				error_msg = "Position category not found."
				mktmessage.msgError(1, error_msg)

		else:

			# Call method for error message
			error_msg = "Gain or loss on exchange category not found."
			mktmessage.msgError(1, error_msg)

		return ""

	except:
		raise