from flask 						import flash
from .. 						import db
from app.mktcore.wtfimports 	import *

from sqlalchemy 				import *
from decimal 					import *
from datetime 					import datetime, date, timedelta

import app.tools.user 							as mktuser
import app.tools.mktmoney 						as mktmoney
import app.tools.mktdate 						as mktdate
import app.tools.loantools.rescheduletools 		as mktschedule

def getSearchLoanApplication():

	Branch 	= 	mktuser.getBranch(session["ChangeBranch"]).ID
	search 	= 	request.args.get("q")
	NAMES 	= 	[]

	Record 	= 	db.session.query(MKT_LOAN_APPLICATION.ID).\
				filter(MKT_LOAN_APPLICATION.ID.like('%'+search.upper()+'%')).\
				filter(MKT_LOAN_APPLICATION.Branch == Branch).\
				filter(~MKT_LOAN_APPLICATION.ID.in_(db.session.query(MKT_LOAN_CONTRACT.LoanApplicationID))).\
				all()

	for row in Record:
		
		dic = {"id":row.ID, "text":row.ID}
		NAMES.append(dic)

	# app.logger.debug(NAMES)
	return jsonify(items = NAMES)

def checkMaxMinAmount(Disbursed=None, Amount=None, Currency=None, LoanProduct=None):
	try:
		CurrencyObj = MKT_CURRENCY.query.get(Currency)
		
		if Disbursed:	
			# if Amount != Disbursed:
			# 	raise ValidationError("The outsanding amount must be equal disbursed")
			# else:
			LP = MKT_LOAN_PRODUCT.query.get(LoanProduct) # LP = LoanProduct record
			if LP:
				RuleID = str(LP.Rule) + str(Currency)
				Rule = MKT_LOAN_RULE_DE.query.get(RuleID)
				if Rule:
					if Amount < Decimal(Rule.MinAmount) or Amount > Decimal(Rule.MaxAmount):
						raise ValidationError("The disbursed amount must between %s and %s." %(mktmoney.toMoney(float(Rule.MinAmount), CurrencyObj, 2), mktmoney.toMoney(float(Rule.MaxAmount), CurrencyObj, 2)))
		else:
			LP = MKT_LOAN_PRODUCT.query.get(LoanProduct) # LP = LoanProduct record
			if LP:
				RuleID = str(LP.Rule) + str(Currency)
				Rule = MKT_LOAN_RULE_DE.query.get(RuleID)
				if Rule:
					if Amount < Decimal(Rule.MinAmount) or Amount > Decimal(Rule.MaxAmount):
						raise ValidationError("The loan amount must between %s and %s." %(mktmoney.toMoney(float(Rule.MinAmount), CurrencyObj, 2), mktmoney.toMoney(float(Rule.MaxAmount), CurrencyObj, 2)))

	except:
		raise

def checkMaxMinTerm(Term, Currency, LoanProduct):
	try:
		LP = MKT_LOAN_PRODUCT.query.get(LoanProduct)
		# print Term
		if LP:
			RuleID = str(LP.Rule) + str(Currency)
			Rule = MKT_LOAN_RULE_DE.query.get(RuleID)
			if Rule:
				Min = int(Rule.MinTerm) if Rule.MinTerm else int(0)
				Max = int(Rule.MaxTerm) if Rule.MaxTerm else int(0)
				if Min:
					if Min > int(0) and Max > int(0):
						if Term < int(Min) or Term > int(Max):
							raise ValidationError("Term must between %s and %s." %(Min, Max))
					
					else:
						if Term < int(Min):
							raise ValidationError("Term must start from %s and up." %Min)
	except:
		raise


def checkAuthorizeApp(AppID="", INAU="INAU"):
	try:
		if INAU == "INAU":
			App = MKT_LOAN_APPLICATION_INAU.query.get(AppID)
		else:
			App = MKT_LOAN_APPLICATION.query.get(AppID)

		if not App:
			flash(msg_error + " Loan application not found.")
			return False

		else:
			if not App.ReviewedBy:
				App.ReviewedBy = str(mktuser.getUser().ID)
				App.ReviewedDate = str(mktdate.getBankDate())

			App.AppStatus = '3'
			App.ApprovedBy = str(mktuser.getUser().ID)
			App.ApprovedDate = str(mktdate.getBankDate())

			db.session.add(App)
			return True

	except:
		db.session.rollback()
		raise

def getNumberOfInstallment(Term, Frequency, FreqType, ValueDate):
	try:

		if Frequency and FreqType and Term:

			StartDate 	=	ValueDate
			EndDate 	=	mktschedule.getNextMonth(StartDate, int(Term))
			if int(FreqType) == 1:

				Value1 = int(Term)

			else:

				dateformat 			= 	"%Y-%m-%d"
				date1 				= 	datetime.strptime(str(StartDate), dateformat)
				date2 				= 	datetime.strptime(str(EndDate), dateformat)
				diff				= 	date2 - date1
				NumberOfWeeks, Days = 	divmod(diff.days, 7)

				Value1 				=	NumberOfWeeks

			return int(Value1) / int(Frequency)

		else:

			return ""

	except:
		raise

def validateTermAndInstallment(Term, Installment, Frequency, FreqType, ValueDate):
	try:

		if Frequency and FreqType and Installment and Term:

			StartDate 	=	ValueDate
			EndDate 	=	mktschedule.getNextMonth(StartDate, int(Term))

			if int(FreqType) == 1:

				getInstallment 	= 	getNumberOfInstallment(Term, Frequency, FreqType, ValueDate)

				if int(getInstallment) == int(Installment):
					return "OK"
				else:
					return "Installment must be %s not %s." %(getInstallment, Installment)

			else:

				getInstallment 		=	getNumberOfInstallment(Term, Frequency, FreqType, ValueDate)

				if int(Installment) == int(getInstallment):
					return "OK"
				else:
					return "Installment must be %s not %s." %(getInstallment, Installment)

		else:
			return "OK"

	except:
		raise