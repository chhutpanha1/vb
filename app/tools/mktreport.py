'''
Created Date: 01 Apr 2015
Author: Sovankiry Rim

Modified by: Sovankiry
Modified Date: 04 May 2015
All Right Reserved Morakot Technology
Description : mktreport refer to function for Bulit-In function report. 
'''


from sqlalchemy 		import *
from app import app,db
from sqlalchemy.sql.expression import cast
from datetime 					import datetime, date, timedelta
from app.urlregister import *
import sys
import app.tools.user 					as mktuser
import app.tools.mktdate 				as mktdate
import app.tools.mktaddress 			as mktaddress
def getCondition(Source,Oprs,kwargs):
	# from app.urlregister import *
	Module = __import__("app.urlregister")

	mapConditioneq 	= []
	mapConditionli  = []
	if kwargs:
		# flash(Oprs)
		for key in kwargs:
			# flash(Oprs[key])
			if Oprs[key]=='EQ':   # ==
				
				mapConditioneq.append( getattr(getattr(Module,Source),key).in_(kwargs[key].split()) )
			
			elif Oprs[key]=='LK': # like
				if kwargs[key].find(',') != -1: # the value search is list
					for item in kwargs[key].split(','):
						mapConditionli.append( getattr(getattr(Module,Source),key).like('%'+item+'%' ) )
				else:
					mapConditionli.append( getattr(getattr(Module,Source),key).like('%'+kwargs[key]+'%' ) )
			
			elif Oprs[key]=='BT': # between
				if kwargs[key].find(',') != -1: # the value search is list
					ColLeft  = ''
					ColRight = ''
					for item in kwargs[key].split(','):
						if not ColLeft:
							ColLeft = str(item)
						elif not ColRight:
							ColRight = str(item)
					mapConditioneq.append( getattr(getattr(Module,Source),key).between(ColLeft, ColRight) )
			
			elif Oprs[key]=='GT': # > greater than
				if kwargs[key].find(',') != -1: # the value search is list
					for item in kwargs[key].split(','):
						mapConditioneq.append( getattr(getattr(Module,Source),key) > str(item) )
				else:
					mapConditioneq.append( getattr(getattr(Module,Source),key) > str(kwargs[key]) ) 
			
			elif Oprs[key]=='GE': # >= greater than equal	
				if kwargs[key].find(',') != -1: # the value search is list
					for item in kwargs[key].split(','):
						mapConditioneq.append( getattr(getattr(Module,Source),key) >= item )
				else:
					mapConditioneq.append( getattr(getattr(Module,Source),key) >= kwargs[key] ) 
			elif Oprs[key]=='LT':# < Less than
				if kwargs[key].find(',') != -1: # the value search is list
					for item in kwargs[key].split(','):
						mapConditioneq.append( getattr(getattr(Module,Source),key) < item )
				else:
					mapConditioneq.append( getattr(getattr(Module,Source),key) < kwargs[key] ) 
			
			elif Oprs[key]=='LE':# <= Less than equal
				if kwargs[key].find(',') != -1: # the value search is list
					for item in kwargs[key].split(','):
						mapConditioneq.append( getattr(getattr(Module,Source),key) <= item )
				else:
					mapConditioneq.append( getattr(getattr(Module,Source),key) <= kwargs[key] ) 

	return mapConditionli,mapConditioneq

def getConditionMultiTable(Oprs,kwargs):
	# from app.urlregister import *
	Module = __import__("app.urlregister")
	# Oprs:{'Currency': 'EQ', 'AccProduct': 'EQ'}
	# kwargs:{'Currency': [('MKT_ACCOUNT', 'USD,THB')], 'AccProduct': [('MKT_ACCOUNT', '003')]}

	#key[0] is Source
	#key[1] is Value

	mapConditioneq 	= []
	mapConditionli  = []
	if kwargs:
		
		for key in kwargs:
			# flash(Oprs[key])
			if Oprs[key]=='EQ':   # ==
				mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key).in_(kwargs[key][0][1].split()) )
			
			elif Oprs[key]=='NE': # Not Equal
				ValueSearch = kwargs[key][0][1].split()
				if 'None' in ValueSearch:
					ValueSearch.append('')
				mapConditioneq.append( ~ getattr(getattr(Module,kwargs[key][0][0]),key).in_(ValueSearch) )

			elif Oprs[key]=='LK': # like
				if kwargs[key][0][1].find(',') != -1: # the value search is list
					for item in kwargs[key][0][1].split(','):
						mapConditionli.append( getattr(getattr(Module,kwargs[key][0][0]),key).ilike('%'+item+'%' ) )
				else:
					mapConditionli.append( getattr(getattr(Module,kwargs[key][0][0]),key).ilike('%'+kwargs[key][0][1]+'%' ) )
			
			elif Oprs[key]=='BT': # between
				# if kwargs[key][0][1].find('\r') != -1: # the value search is list
				ColLeft  = ''
				ColRight = ''
				for item in kwargs[key][0][1].split():
					if not ColLeft:
						ColLeft = str(item)
					elif not ColRight:
						ColRight = str(item)
				mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key).between(ColLeft, ColRight) )
			
			elif Oprs[key]=='GT': # > greater than
				if kwargs[key][0][1].find(',') != -1: # the value search is list
					for item in kwargs[key][0][1].split(','):
						mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key) > str(item) )
				else:
					mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key) > str(kwargs[key][0][1]) ) 
			
			elif Oprs[key]=='GE': # >= greater than equal	
				if kwargs[key][0][1].find(',') != -1: # the value search is list
					for item in kwargs[key][0][1].split(','):
						mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key) >= item )
				else:
					mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key) >= kwargs[key][0][1] ) 

			elif Oprs[key]=='LT':# < Less than
				if kwargs[key][0][1].find(',') != -1: # the value search is list
					for item in kwargs[key][0][1].split(','):
						mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key) < item )
				else:
					mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key) < kwargs[key][0][1] ) 
			
			elif Oprs[key]=='LE':# <= Less than equal
				if kwargs[key][0][1].find(',') != -1: # the value search is list
					for item in kwargs[key][0][1].split(','):
						mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key) <= item )
				else:
					mapConditioneq.append( getattr(getattr(Module,kwargs[key][0][0]),key) <= kwargs[key][0][1] ) 
		 

	return mapConditionli,mapConditioneq

def ADDRESS(arg=""):
	ProvinceID = arg[0]
	DistrictID = arg[1] 
	CommuneID = arg[2] 
	VillageID = arg[3] 
	Locale 	 = arg[4] if len(arg) == 5 else "EN"
	return mktaddress.getAddress(ProvinceID,DistrictID,CommuneID,VillageID,Locale)

def AGE(DOB=None):
	StringDOB = ''
	for item in DOB:
		StringDOB =item
	CurrentDate = datetime.today()
	DateOfBirth = datetime.strptime(str(StringDOB),'%Y-%m-%d')

	Age = CurrentDate - DateOfBirth
	Age = Age.days / 365

	return Age

def DATETIMENOW(Date=None):
	try:
		return mktdate.getDateTimeNow()
	except:
		raise

def DATEISO(arg=''):
	try:
		return mktdate.getDateISO(arg[0][:10])
	except:
		raise
		
# Merge two or more columns
def COLUMN(Field):
	NewField=''
	for item in Field:
		NewField += item+' '

	return NewField

def SUM(arg):
	result = 0
	for item in arg:
		result+=float(item)
	return result

def ADD(arg):
	result = 0
	for item in arg:
		result+=float(item)
	return result

def SUB(arg):
	
	result = float(0)
	i = 0
	for item in arg:
		
		if i ==0:
			result=float(item)
			i=1
		else:
			result=float(result)-float(item)
	return result

def MUL(arg):
	result = float(0)
	i = 0
	for item in arg:
		# print 'item%s'%item
		if i ==0:
			result=float(item)
			i=1
		else:
			result=float(result)*float(item)
	return result

def DIV(arg):
	try:
		result = float(0)
		i = 0
		for item in arg:
			# print 'item%s'%item
			if i ==0:
				result=float(item)
				i=1
			else:
				result=float(result)/float(item)
		return result
	except Exception:
		return 0

def AVG(arg):
	try:
		result = float(0)
		i = 0
		CountArg = len(str(arg))
		# print 'arg:%s'%arg
		#Sum
		SumArg = ADD(arg)
		# print SumArg
		result=SumArg/CountArg
		return result
	except Exception:
		return 0

# MIN is return the smallest item in an iterable or the smallest of two or more arguments.
def MIN(arg):
	ListMin =[]
	for item in arg:
		ListMin.append(float(item))
	result = min(ListMin)
	return result

# MAX is return the largest item in an iterable or the largest of two or more arguments.
def MAX(arg):
	ListMin =[]
	for item in arg:
		ListMin.append(float(item))
	result = max(ListMin)
	return result

# VLOOKUP('MKT_VILLAGE','ID',row.Village,'Description')
# TableName,Field,ForeignValue,FieldLookUp
def VLOOKUP(arg):
	Module = __import__("app.urlregister")
	# from app.urlregister 	import *
	ListLookUp 		= arg
	TableName 		= getattr(Module,ListLookUp[0])
	Field 			= ListLookUp[1]
	ForeignValue 	= ListLookUp[2]
	FieldLookUp 	= ListLookUp[3]
	MultiFieldLookup = len(arg)
	ListFieldLookUp=[]
	i = 0
	if MultiFieldLookup > 4:
		for row in arg:
			if i==3:
				ListFieldLookUp.append(row)
				i = 3
			else:
				i+=1	
	

	if ForeignValue:
		karwgs = {Field:ForeignValue}
		QueryTable = TableName.query.yield_per(100).filter_by(**karwgs)
		if not QueryTable:
			return ""
		else:
			if MultiFieldLookup >4:
				Result = ''
				for row in QueryTable:
					for col in ListFieldLookUp:
						Result+= getattr(row,col) +' '
				return Result
			for row in QueryTable:
				if getattr(row,FieldLookUp):
					return getattr(row,FieldLookUp)
				else:
					return ""
	else:
		return ""

# JOIN(MKT_ACC_PRODUCT,ID,MKT_ACCOUNT,AccProduct)
def JOIN(arg):
	Module = __import__("app.urlregister")
	# from app.urlregister import *
	ForeignTable 	= arg[0]
	FieldForeignKey = arg[1]
	PrimaryTable 	= arg[2]
	FieldPrimaryKey = arg[3]
	
	mapJoin = getattr(Module,ForeignTable),getattr(getattr(Module,ForeignTable),FieldForeignKey) == getattr(getattr(Module,PrimaryTable),FieldPrimaryKey)
	
	# MKT_ACC_PRODUCT is table to ForeignTable 
	# MKT_USER is Primary Table 
	# MKT_ACC_PRODUCT,MKT_ACC_PRODUCT.ID == MKT_USER.ID

	return mapJoin,arg

'''
Function getGroupBy is refer to get Object sqlachemy with Order By.
	GroupByField : parameter is a string
'''

def getGroupBy(GroupByField=''):
	Module = __import__("app.urlregister")
	# from app.urlregister import *
	mapOrderBy = []
	# SourceType = rpt
	GroupBy=''
	if GroupByField:

		if GroupByField.find('*') != -1:
			GroupByField = GroupByField.replace('*',',')
			it = iter(GroupByField.split(","))
			GroupByFieldObj=list(zip(it,it)) ##--- Code create two dimensionals list

			for item in GroupByFieldObj:
				GroupBy=item[1]
				mapOrderBy.append(getattr(getattr(Module,item[0]),item[1]).asc()) ## Start map to object sqlachemy

	return mapOrderBy,GroupBy

def getOrderBy(OrderByField=''):
	Module = __import__("app.urlregister")
	# from app.urlregister import *
	mapOrderBy = []
	OrderByObj = []
	if OrderByField:

		# OrderByField : 'MKT_ACCOUNT*ID*ASC'
		if OrderByField.find('*') != -1:
			OrderByField = OrderByField.split('\n')
			OrderByField = filter(None,OrderByField)
			for item in OrderByField:
				OrderByObj.append(item.split('*'))
		else:
			
			it = iter(OrderByField.split('\n'))
			it = filter(None,it)
			OrderByObj=list(zip(it,it))

		for item in OrderByObj:
			mapOrderBy.append(getattr(getattr(Module,item[0]),item[1],item[2]))
			
	return mapOrderBy

'''
Function getFormulaObj refer to get Formula Object

	Formula parameter is a list.
	col1 = Name of Formula
	col2 = Function Formula
	col3 = Pararamter of Formula

	col1:Type
	col2:VLOOKUP
	col3:MKT_ACC_PRODUCT,ID,AccProduct,Description
	Output: FormulaObj :{'Age': {'getAge': ['DateOfBirth', 'Gender']}, 'Name': {'getName': ['DateOfBirth']}}

'''

def getFormulaObj(Formula=''):
	Join = 'N'
	FormulaObj = {}
	if Formula:

		# itemFormula = ['Age = getAge(DateOfBirth,Gender)', 'Name = getAge(DateOfBirth)']
		# print 'Formula: %s'%Formula

		Formula = filter(None,Formula)
		for itemFormula in Formula:

			if itemFormula:
				col1 = ''
				col2 = ''
				col3 = ''
				# itemFormula = 'Age=getAge(DateOfBirth,Gender)' 
				for col1 in itemFormula.split('=')[:1]: #loop only first element
					# print 'col1:%s'%col1
					FormulaObj.update({col1:[]})
					col1 = col1 #FieldName

				for col in itemFormula.split('=')[1:]: #loop only last element
					for col2 in col.replace(')','').split('(')[:1]:
						# print 'col2:%s'%col2
						FormulaObj.update({col1:{col2:[]}})
						col2= col2 #FunctionName

					for col3 in col.replace(')','').split('(')[1:]:
						# print 'col3:%s'%col3 #Pramater
						if col3:
							Join ='Y'
						ListField=[]
						for Field in col3.split(','):
							ListField.append(Field)
						FormulaObj.update({col1:{col2:ListField}})
						
	# After convert FormulaObj :{'Age': {'getAge': ['DateOfBirth', 'Gender']}, 'Name': {'getName': ['DateOfBirth']}}
	#print 'FormulaObj :%s'% FormulaObj
	return FormulaObj

def getRowObj(RowObj=''):
	# Output : [['ID', 'S'], ['KhmerName', 'S'], ['EnglishName', 'S'], ['Gender', 'S'], ['Age', 'S']]
	ReportRow = []
	if RowObj:
		if RowObj.find('*') != -1:
			#Remove empty strings from a list
			RowObj = filter(None, RowObj.split('\n'))
			for item in RowObj:
				ReportRow.append(item.split('*'))
		else:
			
			it = iter(RowObj.split('\n'))
			ReportRow=list(zip(it,it))
			
	return ReportRow

def getFuntion(FuntionName=''):
	FunctionObj =[]
	if FuntionName:

		if FuntionName.find('*') != -1:
			ListFuntion = FuntionName.split('\n')
			ListFuntion = filter(None,ListFuntion)
			index = 1
			for item in ListFuntion:
				func = item.split('*')
				func.pop(0)
				FunctionObj.append(func)

		else:
			it = iter(FuntionName.split('\n'))
			FunctionObj=list(zip(it,it))

	return FunctionObj

def getCriteria(Criteria=''):
	CriteriaObj 	= []
	DictCriteria 	= {}
	CriteriaFix 	= []
	CriteriaList 	= []
	mapCondition 	= [[],[]]
	Oprs 			= {}
	kwargs 			= {}

	if Criteria:
		# print 'mhere for criteria', Criteria
		Criteria = Criteria.replace('\r','').split('\n')
		Criteria = filter(None,Criteria)
		for row in Criteria :
			Listrow = row.split('*')
			if len(Listrow) == 2:
				Listrow.append("")
				CriteriaList.append(Listrow) ##--- Code to create two dimensional list
				#__kc__ testing
			elif len(Listrow) == 3:
				CriteriaList.append(Listrow) ##--- Code to create three dimensional list, 
											  ##	in case there are 2 or more fields with the same name
			else:
				# for item in Listrow:
				CriteriaFix.append(Listrow)
		# print "CriteriaFix",CriteriaFix
		# print 'mhere in criterialist:', CriteriaList
	

	 	# Break CriteriaObj to Source and Field
		for item in CriteriaList:
			# print 'mhere in here',CriteriaList
			if item[2]:
				CriteriaObj.append((item[1],item[2]))
			else:
				CriteriaObj.append((item[1],item[1]))
			# __kc__ testing
			if len(item) == 3:
				DictCriteria.update( {item[1]:item[0]} ) ##--- Store Source
				# Problem when updating dictionary, item[1] has the same name

		# print 'mhere in DictCriteria:', DictCriteria
		# print 'mhere in CriteriaObj:', CriteriaObj
		if CriteriaFix:
			mapCondition = []
			for itemCriteria in CriteriaFix:

				# 	itemCriteria[0] is Table Name
				# 	itemCriteria[1] is Field
				# 	itemCriteria[2] is Operator
				# 	itemCriteria[3] is Value


				Oprs.update({itemCriteria[1]   :  itemCriteria[2] })
				kwargs.update({itemCriteria[1] :  [( itemCriteria[0],itemCriteria[3] ) ] if itemCriteria[3] else '' })
				
				print 'Oprs:%s'%Oprs
				print 'kwargs:%s'%kwargs

			mapCondition = getConditionMultiTable(Oprs,kwargs)

			# print mapCondition

	return CriteriaObj,DictCriteria,mapCondition

def getJoinTable(FormulaObj='',RowObj=''):
	Module = __import__("app.urlregister")
	# from app.urlregister import *
	'''
						
		mapJoinTableObj[0]    is Object join Sqlachemy
		mapJoinTableObj[1][0] is ForeignTable
		mapJoinTableObj[1][1] is FieldForeignKey
		mapJoinTableObj[1][2] is PrimaryTable
		mapJoinTableObj[1][3] is FieldPrimaryKey
	'''

	mapJoinTable = []
	DicTable 	 = {}
	FieldInTable 	 = []
	ListFieldMain	 = []
	ListFieldForeign = []
	Row 		 	 = []				
	if FormulaObj:
		import collections
		FormulaObj = collections.OrderedDict(sorted(FormulaObj.items()))
		
		for itemFormulaObj in FormulaObj:
			
			for item  in FormulaObj[itemFormulaObj]:
				if item == 'JOIN':
					# print FormulaObj[item]['JOIN']
					# mapJoinTable = JOIN(['MKT_ACC_PRODUCT','ID','MKT_ACCOUNT','AccProduct'])
					mapJoinTableObj = JOIN(FormulaObj[itemFormulaObj][item])	
					# Map Column Table Main
					Mapper 			 = inspect(getattr(Module,mapJoinTableObj[1][2]))
					for item in Mapper.attrs:
						FieldInTable.append(getattr(getattr(Module,mapJoinTableObj[1][2]),item.key))
						ListFieldMain.append(item.key)

					# Map Column JoinTable
					Mapper 			= inspect(getattr(Module,mapJoinTableObj[1][0]))
					for item in Mapper.attrs:
						FieldInTable.append(getattr(getattr(Module,mapJoinTableObj[1][0]),item.key))
						ListFieldForeign.append(item.key)

					DicTable={mapJoinTableObj[1][2]:ListFieldMain,mapJoinTableObj[1][0]:ListFieldForeign}
					
					mapJoinTable.append(mapJoinTableObj[0])
	if RowObj:
		if mapJoinTable:

			FieldInTable = []
			#Remove empty strings from a list
			RowObj = filter(None, RowObj.split('\n'))
			for item in RowObj:
				ListField = item.split('*')
				if str(ListField[0]).find('MKT') == -1:
					if len(ListField)==3:
					
						if ListField[1]=='C':
	
							Row.append([ListField[0],ListField[1],ListField[2]])
						else:
							Row.append([ListField[0],ListField[1]])
					else:
						Row.append([ListField[0],ListField[1]])
				else:
					FieldInTable.append(getattr(getattr(Module,ListField[0]),ListField[1]))
					if ListField[2]=='C':
						# [1] is Field , [2] is C,S,D [3] is field that Currency use only [2]==C
						Row.append([ListField[1],ListField[2],ListField[3]])
					else:
						Row.append([ListField[1],ListField[2]])
			# print Row
			#Output Row : [['ID', 'S'], ['KhmerName', 'S'], ['EnglishName', 'S'], ['Gender', 'S'], ['Age', 'S']]
	return mapJoinTable,DicTable,FieldInTable,Row

def getMapOrderBy(OrderBy='',GroupBy=''):
	mapOrderBy = []
	if OrderBy:
		for row in OrderBy:
			mapOrderBy.append(row)
	if GroupBy:
		for row in GroupBy:
			mapOrderBy.append(row)

	return mapOrderBy

def getCompanyObj():
	try:
		
		return MKT_COMPANY.query.get('SYSTEM')

	except Exception, e:
		return e

def getObjectReport(QueryObj,DictCriteria,Criteria,request):
	'''
	Store table name of field criteria
	'''
	# DictCriteria = {'DebitCredit': 'MKT_JOURNAL', 'Branch': 'MKT_JOURNAL', 'Reference': 'MKT_JOURNAL', 'TransactionDate': 'MKT_JOURNAL', 'CategoryID': 'MKT_JOURNAL', 'Module': 'MKT_JOURNAL', 'GL_KEYS': 'MKT_JOURNAL'}
	'''
	List field Criteria 
	''' 
	# Criteria = ['Branch', 'TransactionDate', 'CategoryID', 'Reference', 'DebitCredit', 'GL_KEYS', 'Module']
	Oprs 		=	{}
	kwargs 		=	{}
	SearchAll 	=	''
	
	for itemCriteria in Criteria:

		if request.args.get(itemCriteria):
			urlRequest=str(request.url).split('?')[1] if str(request.url).find('?Opr') != -1 else ''
			Oprs.update({itemCriteria   :  str(request.args.get('Opr'+itemCriteria)) })
			kwargs.update({itemCriteria :  [( DictCriteria[itemCriteria],str(request.args.get(itemCriteria)).strip() ) ] if request.args.get(itemCriteria) else '' })
			#When user search all will be show all Branch
			if itemCriteria == 'Branch':
				if str(request.args.get(itemCriteria)).strip().upper()=='ALL':
					if isAccessAllBranch(str(request.args.get(itemCriteria)).strip().upper()):
						Oprs.pop('Branch',None)
						kwargs.pop('Branch', None)
						SearchAll='ALL'

	# Block Validation AccessBranch
	if request.args:
		if SearchAll:

			CheckAccessBranch = validationAccessBranch({'Branch': [('', 'ALL')]},Criteria)
		else:
			CheckAccessBranch = validationAccessBranch(kwargs,Criteria)
		# print 'CheckAccessBranch:%s'%CheckAccessBranch
		if CheckAccessBranch:
			return False,CheckAccessBranch[1],Oprs

	# Oprs = {'TransactionDate': 'EQ', 'Module': 'EQ', 'Branch': 'EQ'}
	# kwargs = {'TransactionDate': [('MKT_JOURNAL', '2016-03-01')], 'Module': [('MKT_JOURNAL', 'MJII')], 'Branch': [('MKT_JOURNAL', 'HO')]}

	mapCondition = getConditionMultiTable(Oprs,kwargs)

	ResultObj = QueryObj.filter(or_(*mapCondition[0])).\
						filter(*mapCondition[1])

	return True,ResultObj,Oprs

def getValueOfCriteria(Criteria,request):
	kwargs = {}
	for itemCriteria in Criteria:
		if request.args.get(itemCriteria):
			kwargs.update({itemCriteria : str(request.args.get(itemCriteria)).strip() if request.args.get(itemCriteria) else '' })
	return kwargs
	
def validationAccessBranch(kwargs,Criteria):
	AccessBranch  	= str(mktuser.getUser().AccessBranch).split()
	# Validation AccessBranch
	ListSearchBranch = []
	
	if 'Branch' in kwargs:
		for key in kwargs:
			if key == "Branch":
				ListSearchBranch=str(kwargs[key][0][1]).split()
		
		if ListSearchBranch:
			for row in ListSearchBranch:

				if not row in AccessBranch:
					if 'ALL' in AccessBranch:
						return []
					else:
						return ["Branch",msg_permission]
		
	if 'Branch' in Criteria:
		if not 'Branch' in kwargs:
			return["Branch","This field is required."]
	
	return []

def isAccessAllBranch(Branch):
	AccessBranch  	= str(mktuser.getUser().AccessBranch).split()
	if Branch in AccessBranch:
		return True
	else:
		return False

def getPrintByAndPrintDate():
	User    =   mktuser.getUser()
	Date    =   mktdate.getBankDate()
	return '''	<div style="float: right;" class="Footer">
	                <div class="print-by">Printed By : <strong>%s</strong></div>
	                <div class="print-date">Printed Date : <strong>%s</strong></div>
	            </div> '''%(User.DisplayName,str(Date))
