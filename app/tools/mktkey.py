from app.mktcore.imports 			import *
from sqlalchemy 					import *
from decimal 						import *


def getTableName(Data):
	try:

		Table = ""
		Data = Data.split(",")
		Table = Data[0]

		return Table.strip()

	except:
		raise

def getKeyValue(Data, Filter_Value):
	try:
		
		Value = ""
		Data = Data.split(",")
		Table = Data[0].strip()
		Obj = eval(Table).query.get(Filter_Value)
		if Obj:
			Field = Data[1].strip()
			Value = getattr(Obj, Field)

		return Value

	except:
		raise

def getResultKey(k, LCID, CustomerID, Resource=None):
	try:

		Table = getTableName(k) if k else ""
		KeyID = LCID if Table in "MKT_LOAN_CONTRACT" else CustomerID
		if Resource and Resource == "INAU":
			kk = str(k).split(",")
			if len(kk) == 2:
				k1 = kk[0] + "_INAU"
				k2 = kk[1]
				k = k1 + "," + k2

		k = getKeyValue(k, KeyID) if k else ""

		return k

	except:
		raise