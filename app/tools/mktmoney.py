# Created by Kiry and Sophorth
# Date: 27 January 2015
# All Right Reserved Morakot Technology

import decimal
import math
from app.Currency.models import *

def getCurrencyObj(Currency=None):
	return MKT_CURRENCY.query.get('%s' %Currency)

def formatNumber(Value, Comma=1, DecimalPlace=2):
	
	# Comma = 1 : the format with comma seperator. 0 : no comma seperator
	# DecimalPlace: by default is 2.
	
	if Comma == 0:
		strFormat = '{:.' + str(DecimalPlace) +'f}'
	else:
		strFormat = '{:,.' + str(DecimalPlace) +'f}'

	result = strFormat.format(Value)

	return result


def toMoney(Value, CurRec, Sign=0):

	NewStr=''

	if CurRec:
		ID=CurRec.ID
		RndMode=int(CurRec.RndMode)
		DecimalPlace=CurRec.DecimalPlace
		ThousandSeparator = CurRec.ThoSeparator
		DecimalSeparator = CurRec.DecSeparator
		SmallestVal = CurRec.SmallestRndTo
		Symbol = ''
		Negative = False
		
		if Sign == 1:
			Symbol = CurRec.CurrencySign
		elif Sign ==2:
			Symbol = ID
		else:
			Symbol =''

		Mylength=len(str(int(Value)))
		if str(int(Value)).find('-')!= -1:
			Mylength -= 1
			Value = abs(Value)
			Negative = True
		
		n = 0
		# To solve the issue of value less than 1 or less than 0.1, 0.01, 0.001 so on so fort
		
		absVal = abs(Value) # convert to absolute value

		while absVal < 10**n:
			n -=1
				
		Mylength = Mylength + (n)  # n is negative here

		ROUNDING_MODES = ['','ROUND_UP','ROUND_DOWN','ROUND_HALF_UP']
		
		Value = Value / float(SmallestVal) # Make it small for rounding

		with decimal.localcontext() as c:
			c.rounding = getattr(decimal, ROUNDING_MODES[RndMode])
			c.prec = DecimalPlace + Mylength - int(math.log10(SmallestVal))
			NewValue=decimal.Decimal(Value) * 1
			
		tempValue = NewValue

		# Removing Exponent and trailing zero
		NewValue = NewValue.quantize(decimal.Decimal(1)) if NewValue == NewValue.to_integral() else NewValue.normalize()

		NewValue = 1 if str(abs(NewValue))=='0' else NewValue	
		NewValue= NewValue * SmallestVal if (tempValue != 0) and (Value != 0) else 0  # Add back to normal value after rounded.

		
		if str(NewValue).find('.') != -1:
			StrValue = str(NewValue).split(".")[0]
			TempStr =str(NewValue).split(".")[1]

			# Check if the number of digit of decimal place is less than DecimalPlace.
			# If it is less, then add more trailing zero.	
			TempLen = len(TempStr)

			if TempLen < DecimalPlace:
				TrailingZeros = "0" * (DecimalPlace - TempLen)
				TempStr = TempStr + TrailingZeros 

		else:
			StrValue =str(NewValue)
			TempStr ='0'*DecimalPlace #Adding 00 to the string
			DecimalSeparator = '' if DecimalPlace == 0 else DecimalSeparator

		StrLen = len(StrValue)

		StrList = []

		while StrLen > 3:
			StrList.append(StrValue[StrLen-3:StrLen])
			StrLen -= 3
		
		StrList.append(StrValue[0:StrLen])

		i=len(StrList)

		while i > 1:
			NewStr =NewStr + StrList[i-1] + ThousandSeparator
			i-=1

		NewStr = NewStr + StrList[i-1] + DecimalSeparator + TempStr
		NewStr = "-" + NewStr if Negative and tempValue != 0 else NewStr # Add minus sign to negative number
		NewStr = Symbol + " " + NewStr

	else:
		NewStr = "Currency not found!"
		
	return NewStr.strip()

def getCurrencySymbol(Currency, Option=0):
	CurrencyObj = getCurrencyObj(Currency)
	Result 		= ""
	if CurrencyObj:
		if int(Option) == 0:
			Result = CurrencyObj.CurrencySign
		else:
			Result = CurrencyObj.ID

	return Result
