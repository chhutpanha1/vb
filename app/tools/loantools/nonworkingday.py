
from datetime import datetime, date, timedelta
import calendar
import operator


#function CheckDate Saturday
def isSaturdays(CheckDate,Holiday):

	if CheckDate.weekday() == 5 and Holiday == True: # weekday is return ; 0-6 5 is Saturday  
		return True
	return False

#function CheckDate Sunday
def isSundays(CheckDate,Holiday):

	if CheckDate.weekday() == 6 and Holiday == True: # weekday is return ; 0-6 6 is Sunday
		return True
	return False

#function
def isHoliday(CheckDate):  #define function 

	Holiday=MKT_HOLIDAY.query.get('%s' %CheckDate.year) # tablename.query where ID='2015'
	ListH=[] #List of Holiday in a year
	MonthList=['','January','February','March','April','May','June','July','August','September','October','November','December']
	
	if Holiday:

		MyMonth=operator.attrgetter('%s' % MonthList[CheckDate.month])(Holiday) # Convert string to attribute of class
		if MyMonth:
			MyMonth=MyMonth.split()
			for item in MyMonth:
				item='%s-%s-%s'%(CheckDate.year,CheckDate.month,item)
				ListH.append(datetime.strptime(str(item),'%Y-%m-%d').date()) #For get record from db insert in to list then convert like  Datetime.date(2015,1,30)

	if CheckDate in ListH :

		return True # return True mean that CheckDate is holiday

	return False # Funtion return False mean that ur day is not on holiday


def isNonWorkingDay(CheckDate,HolidayOption):

	if HolidayOption == 1 : #Saturday + Sunday + Holiday 

		SatHoliday=True
		SunHoliday=True

	elif HolidayOption == 2: #Sunday + Holiday

		SatHoliday=False
		SunHoliday=True

	elif HolidayOption == 3: #Saturday + Holiday

		SatHoliday=True
		SunHoliday=False

	else: # Only Holiday

		 SatHoliday=False
		 SunHoliday=False

	if isSaturdays(CheckDate,SatHoliday)== True or isSundays(CheckDate,SunHoliday)==True or isHoliday(CheckDate)== True :
		return True

	return	False #CheckDate is not holiday return False


