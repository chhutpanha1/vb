from flask import request, url_for, render_template,flash,redirect
from ... import app
import time

from dateutil.relativedelta import relativedelta
from datetime import datetime, date, timedelta
import calendar
import operator
import nonworkingday as work  #import funtion from nonworking
import app.tools.mktdate as mktdate
import app.tools.mktmoney as mktmoney

def getFrequencyType(Frequency=''):
    if Frequency:
        if Frequency=='D':
            return 3
        if Frequency=='M':
            return 1
        if Frequency=='Y':
            return 4
        if Frequency=='W':
            return 2

#function check day in a month if out of range in month will be generate last day in a month
def setDayInMonth(StartDate):
    # StartDate = YYYY-MM-DD
    #>> calendar.monthrange(2002,1)
    #>> (1, 31)
    try:
        ConvertToDate=datetime.strptime(str(StartDate),'%Y-%m-%d').date() #Convert string to like this datetime.date(2015, 1, 1)
        return ConvertToDate
    except Exception, e:
        
        StartDate = str(StartDate)
        yy = int(StartDate.split('-')[0])
        mm = int(StartDate.split('-')[1])
        dd = int(StartDate.split("-")[2])
        LastDayInMonth = int(calendar.monthrange(yy,mm)[1])
        YYYYMMDD    = '%s-%s-%s' % (yy,mm,LastDayInMonth)
        NewDate     = datetime.strptime(str(YYYYMMDD),'%Y-%m-%d').date()
        return NewDate

# function generate next month
# auto generate last day in month if out of range in month
def getNextMonth(StartDate,NumberOfNext):
    # Old code
    # ConvertToDate=datetime.strptime(str(StartDate),'%Y-%m-%d').date() #Convert string to like this datetime.date(2015, 1, 1)
    # mm = ConvertToDate.month - 1 + NumberOfNext #Frequency is number of next month
    # yy = ConvertToDate.year + mm / 12
    # mm = mm % 12 + 1
    # dd = min(ConvertToDate.day,calendar.monthrange(yy,mm)[1])
    # YYYYMMDD= '%s-%s-%s' % (yy,mm,dd)
    # NextMonth=datetime.strptime(str(YYYYMMDD),'%Y-%m-%d').date()
    # return NextMonth

    StartDate   = str(StartDate)
    Year        = int(StartDate.split('-')[0])
    Month       = int(StartDate.split('-')[1])
    Day         = int(StartDate.split("-")[2]) 

    mm = Month - 1 + NumberOfNext #Frequency is number of next month
    yy = Year + mm / 12
    mm = mm % 12 + 1
    dd = min(Day,calendar.monthrange(yy,mm)[1])
    YYYYMMDD    = '%s-%s-%s' % (yy,mm,dd)
    NextMonth   = datetime.strptime(str(YYYYMMDD),'%Y-%m-%d').date()
    return NextMonth

#function generate next weekend
def getNextWeek(StartDate,NumberOfNextWeek):

    # ConvertToDate=datetime.strptime(str(StartDate),'%Y-%m-%d').date() #Convert string to like this datetime.date(2015, 1, 1)
    ConvertToDate=setDayInMonth(StartDate)
    Weekend = timedelta(days=7) * NumberOfNextWeek 
    NextWeek=ConvertToDate+ Weekend

    return NextWeek

#function generate next day
def getNextDay(StartDate,NumberOfNextDay):
    # NumberOfNextDay is int
    # ConvertToDate=datetime.strptime(str(StartDate),'%Y-%m-%d').date() #Convert string to like this datetime.date(2015, 1, 1)
    ConvertToDate=setDayInMonth(StartDate)
    NumberDay = timedelta(days=NumberOfNextDay)
    NextDay=ConvertToDate + NumberDay
    return NextDay

#function generate next day
def getNextYear(StartDate,NumberOfNextYear):
    # NumberOfNextYear is int
    # ConvertToDate=datetime.strptime(str(StartDate),'%Y-%m-%d').date() #Convert string to like this datetime.date(2015, 1, 1)
    ConvertToDate=setDayInMonth(StartDate)
    NextYear = ConvertToDate + relativedelta(years=NumberOfNextYear)
    return NextYear

#function get coldate check with holiday option
def getColDate(ValueDate,FrequencyType,Frequency,ForwardBackKey,HolidayOption,BaseDateKey="",DayValueDate=""):
    
    one_day = timedelta(days=1) # one day
    # if BaseDateKey is ValueDate
    # Don't convert Value to datetime Because if day value date can be out of range in month
    # So when use function nextmonth will generate auto last day in month if out of range in month


    if FrequencyType==1: # Frequency by month

        if BaseDateKey==1:
            ValueDate = str(ValueDate)
            ValueDate = '%s-%s-%s'%(ValueDate.split('-')[0],ValueDate.split('-')[1],DayValueDate)

        NextColDate=getNextMonth(ValueDate,Frequency)#exe function getNextMonth return datetime.date(2015, 1, 1)
        tempColDate = NextColDate

        if ForwardBackKey==1: # 1 Move to back day but within a month              

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True:
                NextColDate-= one_day # - 1day

            if NextColDate.month != tempColDate.month: # if move back until within month

                NextColDate=getNextMonth(ValueDate,Frequency) #start calculator NextColDate
                while work.isNonWorkingDay(NextColDate,HolidayOption)==True:
                    NextColDate+=one_day # + 1day
                    
        elif ForwardBackKey==2: # 2 Move to Forward day but within a month 

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate+= one_day # + 1day

            if NextColDate.month != tempColDate.month: # if move back until within month

                NextColDate=getNextMonth(ValueDate,Frequency) #start calculator NextColDate
                while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                    NextColDate-=one_day # - 1day

        elif ForwardBackKey == 3: #3 Move to back

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate-= one_day  # - 1day

        elif ForwardBackKey == 4: #4 Move to Forward day

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate+= one_day # + 1day


    elif FrequencyType==2: # 2 Frequency type by weekend

        NextColDate=getNextWeek(ValueDate,Frequency)#exe function getNextMonth return datetime.date(2015, 1, 1)
        tempColDate = NextColDate

        if ForwardBackKey==1: # 1 Move to back day but within a month              
            
            while work.isNonWorkingDay(NextColDate,HolidayOption)==True:
                NextColDate-= one_day # - 1day
                

            if NextColDate.isocalendar()[1] != tempColDate.isocalendar()[1]: # if move back until within month

                NextColDate=getNextWeek(ValueDate,Frequency) #start calculator NextColDate

                while work.isNonWorkingDay(NextColDate,HolidayOption)==True:
                    NextColDate+=one_day # + 1day
                    
        elif ForwardBackKey==2: # 2 Move to Forward day but within a month 

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate+= one_day # + 1day

            if NextColDate.isocalendar()[1] != tempColDate.isocalendar()[1]: # if move back until within month
                NextColDate=getNextWeek(ValueDate,Frequency) #start calculator NextColDate
                
                while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                    NextColDate-=one_day # - 1day

            # Mean that Holiday full one weekend cannot go to next Week and still on Current Value Date
            # or Less then value date ( past date ) so need to move Forward day only
            if str(NextColDate) <= str(ValueDate):
                NextColDate=getNextWeek(ValueDate,Frequency) #start calculator NextColDate again
                while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                    NextColDate+= one_day # + 1day

        elif ForwardBackKey == 3: #3 Move to back

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate-= one_day  # - 1day
                
            # Mean that Holiday full one weekend cannot go to next Week and still on Current Value Date
            # or Less then value date ( past date ) so need to move Forward day only
            if str(NextColDate) <= str(ValueDate):
                NextColDate=getNextWeek(ValueDate,Frequency) #start calculator NextColDate again
                while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                    NextColDate+= one_day # + 1day

        elif ForwardBackKey == 4: #4 Move to Forward day

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate+= one_day # + 1day

    elif FrequencyType==3: # 3 Frequency type by Day

        NextColDate=getNextDay(ValueDate,Frequency)#exe function getNextDay return datetime.date(2015, 1, 1)
        tempColDate = NextColDate

        if ForwardBackKey==1: # 1 Move to back day but within a month              
            
            while work.isNonWorkingDay(NextColDate,HolidayOption)==True:
                NextColDate-= one_day # - 1day
                

            if NextColDate.isocalendar()[1] != tempColDate.isocalendar()[1]: # if move back until within month

                NextColDate=getNextDay(ValueDate,Frequency) #start calculator NextColDate

                while work.isNonWorkingDay(NextColDate,HolidayOption)==True:
                    NextColDate+=one_day # + 1day
                    
        elif ForwardBackKey==2: # 2 Move to Forward day but within a month 

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate+= one_day # + 1day

            if NextColDate.isocalendar()[1] != tempColDate.isocalendar()[1]: # if move back until within month

                NextColDate=getNextDay(ValueDate,Frequency) #start calculator NextColDate
                
                while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                    NextColDate-=one_day # - 1day

        elif ForwardBackKey == 3: #3 Move to back

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate-= one_day  # - 1day

        elif ForwardBackKey == 4: #4 Move to Forward day

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate+= one_day # + 1day            

    elif FrequencyType==4: # 4 Frequency type by Yearly

        if BaseDateKey==1:
            ValueDate = str(ValueDate)
            ValueDate = '%s-%s-%s'%(ValueDate.split('-')[0],ValueDate.split('-')[1],DayValueDate)

        NextColDate=getNextYear(ValueDate,Frequency)#exe function getNextYear return datetime.date(2015, 1, 1)
        tempColDate = NextColDate

        if ForwardBackKey==1: # 1 Move to back day but within a month              
            
            while work.isNonWorkingDay(NextColDate,HolidayOption)==True:
                NextColDate-= one_day # - 1day
                

            if NextColDate.isocalendar()[1] != tempColDate.isocalendar()[1]: # if move back until within month

                NextColDate=getNextYear(ValueDate,Frequency) #start calculator NextColDate

                while work.isNonWorkingDay(NextColDate,HolidayOption)==True:
                    NextColDate+=one_day # + 1day
                    
        elif ForwardBackKey==2: # 2 Move to Forward day but within a month 

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate+= one_day # + 1day

            if NextColDate.isocalendar()[1] != tempColDate.isocalendar()[1]: # if move back until within month

                NextColDate=getNextYear(ValueDate,Frequency) #start calculator NextColDate
                
                while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                    NextColDate-=one_day # - 1day

        elif ForwardBackKey == 3: #3 Move to back

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate-= one_day  # - 1day

        elif ForwardBackKey == 4: #4 Move to Forward day

            while work.isNonWorkingDay(NextColDate,HolidayOption)==True: 
                NextColDate+= one_day # + 1day 
    return NextColDate   # Finally return Function




def getDayInYear(InterestDayBasis,Date):
    # InterestDayBasis  : 1= 360/360; 2= 365/360; 3=365/365 filed must be int
    # Date              : must bedatetime.date(2015, 1, 1)
    if InterestDayBasis == 1:
        return float(360)
    elif InterestDayBasis == 2:
        return float(360)
    elif InterestDayBasis == 3:
        if mktdate.isLeapYear(str(Date)):
            # Date is leap 1-29
            return float(366)
        else:
            #year is non leap 1-28
            return float(365)


def getNumberOfDay(InterestDayBasis,DateFrom,DateTo):
    # InterestDayBasis  1= 360/360; 2= 365/360; 3=365/365
    # DateFrom  : must be is datetime.date(2015, 1, 1)
    # DateTo    : must be is datetime.date(2015, 1, 1)

    if type(DateFrom) is not datetime.date:
        DateFrom=datetime.strptime(str(DateFrom),'%Y-%m-%d').date() #datetime.date(2015, 1, 1)
    if type(DateTo) is not datetime.date:
        DateTo=datetime.strptime(str(DateTo),'%Y-%m-%d').date() #datetime.date(2015, 1, 1)

    # Calculate NumberOfDay if InterestDayBasis 1 = 360/360
    if InterestDayBasis==1: 

        NumberOfDay    =   mktdate.getDatediff360(DateFrom,DateTo)
    else:
        NumberOfDay    =  (DateTo - DateFrom).days

    return NumberOfDay

def getInterestPerYear(RatePerYear,OutstandingAmount):
    # RatePerYear       : Mean that rate per a year. Example : 1% per month = 12% per year
    # OutstandingAmount : Refer to Principal Amount

    InterestPerYear  = OutstandingAmount * RatePerYear / float(100)

    return InterestPerYear

def getInterestPerDay(RatePerYear,OutstandingAmount,InterestDayBasis,Date):
    # RatePerYear       : Mean that rate per a year. Example : 1% per month = 12% per year
    # OutstandingAmount : Refer to Principal Amount
    # InterestDayBasis  : 1= 360/360; 2= 365/360; 3=365/365
    # Date              : Refer to date to check DaysInYear must be is datetime.date(2015, 1, 1)

    if type(Date) is not datetime.date:
        Date=datetime.strptime(str(Date),'%Y-%m-%d').date() #datetime.date(2015, 1, 1)

    InterestPerYear     = getInterestPerYear(RatePerYear,OutstandingAmount)
    DaysInYear          = getDayInYear(InterestDayBasis,Date) 
    InterestPerDay      = InterestPerYear / DaysInYear

    # print "CollectionDate:%s"%DateFrom
    # print "Principal:%s"%OutstandingAmount
    # print "InterestPerYear: %s"%InterestPerYear
    # print "InterestPerDay: %s"%InterestPerDay
    # print
    
    return InterestPerDay

def getInterestAmount(RatePerYear,OutstandingAmount,InterestDayBasis,DateFrom,DateTo):
    # RatePerYear       : Mean that rate per a year. Example : 1% per month = 12% per year
    # OutstandingAmount : Refer to Principal Amount
    # InterestDayBasis  : 1= 360/360; 2= 365/360; 3=365/365
    # DateFrom          : Refer to date to check DaysInYear must be is datetime.date(2015, 1, 1)
    # DateTo            : Refer to date to get NumberOfDay must be is datetime.date(2015, 1, 1)
    if type(DateFrom) is not datetime.date:
        DateFrom=datetime.strptime(str(DateFrom),'%Y-%m-%d').date() #datetime.date(2015, 1, 1)
    if type(DateTo) is not datetime.date:
        DateTo=datetime.strptime(str(DateTo),'%Y-%m-%d').date() #datetime.date(2015, 1, 1)

    NumberOfDay         = getNumberOfDay(InterestDayBasis,DateFrom,DateTo)
    InterestPerDay      = getInterestPerDay(RatePerYear,OutstandingAmount,InterestDayBasis,DateFrom)
    Interest            = InterestPerDay * NumberOfDay

    return Interest


def getSchedulDeclining(DisburseAmount,ValueDate,Rate,FrequencyType,
                Frequency,Term,InterestDayBasis,FwdBwdKey,
                HolidayOption,Currency,BaseDateKey,FrequencyP,
                FrequencyI,RepaymentMode,Locale=None,FirstDate=None):
    
    CurrencyObj = mktmoney.getCurrencyObj(Currency)
    RowRecord=[]
    ColRecord=[]
    BalAmount=float(DisburseAmount) 


    CurColDate      = datetime.strptime(str(ValueDate),'%Y-%m-%d').date() #datetime.date(2015, 1, 1)
    BaseValueDate   = datetime.strptime(str(ValueDate),'%Y-%m-%d').date()
    DayValueDate    = str(ValueDate).split("-")[2] # split only date for BaseDateKey is value date
    if FirstDate:
        BaseFirstDate   = datetime.strptime(str(FirstDate),'%Y-%m-%d').date() 
        DayFristDate    = str(FirstDate).split("-")[2] 

    TempFrequencyP=FrequencyP
    CountFrequencyP=1
    CountFrequencyI=1
    NumberOfDay=0
    TempInterest = 0
    CurInterest = 0
    InterestTaken = False # This variable define if interest is already taken avoid for retake.
    
    InstallmentNo=1
    while (InstallmentNo<=Term) :       
        
        if BaseDateKey ==1:  # 1 Based on Value day  

            FirstColDate=getColDate(BaseValueDate, #Value date
            FrequencyType,#Frequency Type
            Frequency,#Frequency
            FwdBwdKey,#FwdBwdKey
            HolidayOption,
            BaseDateKey,
            DayValueDate)

        elif BaseDateKey ==2:  # Based On Previous day     

            FirstColDate=getColDate(CurColDate, #Value date
            FrequencyType, #Frequency Type
            Frequency, #Frequency
            FwdBwdKey, #FwdBwdKey
            HolidayOption)

        elif BaseDateKey ==3:  # Based On First Date

            if InstallmentNo == 1:
                FirstColDate    = datetime.strptime(str(FirstDate),'%Y-%m-%d').date()

            else:

                FirstColDate=getColDate(BaseFirstDate, #First date
                FrequencyType, #Frequency Type
                Frequency, #Frequency
                FwdBwdKey, #FwdBwdKey
                HolidayOption)

        DateFrom            = CurColDate
        DateTo              = FirstColDate
        
        NumberOfDay         = getNumberOfDay(InterestDayBasis,DateFrom,DateTo)
        OutstandingAmount   = (BalAmount if RepaymentMode==1 else DisburseAmount) # if Flat take DisburseAmount
        CurInterest         = getInterestAmount(Rate,OutstandingAmount,InterestDayBasis,DateFrom,DateTo)

        # Calculate Principal as per FrequencyP setting
        if FrequencyP==1: # 1 Frequency Principal 

            Principal= DisburseAmount/ float(Term)

        elif TempFrequencyP==CountFrequencyP: # CountFrequencyP Principal >1
            
            Principal= DisburseAmount/ float(Term) * FrequencyP
            CountFrequencyP=0  # Reset CountFrequency to 0 

        else:
            Principal=0

        # Calculate Interest as per FrequencyI Setting
        if FrequencyI==1:
            
            Interest = CurInterest # InterestPerDay * NumberOfDay 
            InterestTaken = True  # Interest will not be retaken if this variable is true

        elif FrequencyI == CountFrequencyI:
            
            TempInterest = TempInterest + CurInterest
            Interest = TempInterest
            TempInterest = 0
            CountFrequencyI = 0
            InterestTaken = True

        else:

            TempInterest = TempInterest + CurInterest
            Interest = 0
            InterestTaken = False

            
        #Frequency Principal >1 when last term have balance need Principal = Balance
        if InstallmentNo == int(Term): # for the last of term if Balance less then 0 need Principal= Balance
            Principal=BalAmount
            Interest = TempInterest if not InterestTaken else Interest

       

        if FrequencyType==1:
            
            BaseValueDate = '%s-%s-%s'%(BaseValueDate.year,BaseValueDate.month,DayValueDate)
            BaseValueDate=getNextMonth(BaseValueDate,Frequency)

             # if BaseDateKey is FirstDate
            if BaseDateKey == 3:
                BaseFirstDate = '%s-%s-%s'%(FirstColDate.year,FirstColDate.month,DayFristDate)

        else:
            BaseValueDate=getNextWeek(BaseValueDate,Frequency)
            if BaseDateKey == 3:
                BaseFirstDate = FirstColDate

        #Formatting Currency
        Principal       = mktmoney.toMoney(Principal,CurrencyObj) #format to default #,##0.00
        Interest        = mktmoney.toMoney(Interest,CurrencyObj)

        #The total amount must be the sum of Principal and Interest after rounding.
        TotalAmount     = float(Principal.replace(',','')) + float(Interest.replace(',','')) 
        TotalAmount     = mktmoney.toMoney(TotalAmount,CurrencyObj)

        # No deduction for last installment, it is absolutely Zero.
        BalAmount       = BalAmount - float(Principal.replace(',','')) if InstallmentNo != Term else 0
        BalAmountStr    = mktmoney.toMoney(BalAmount,CurrencyObj)

        FirstColDateFormat = mktdate.toDateShort(FirstColDate,Locale) if Locale!=None else FirstColDate
        
        ColRecord.append(InstallmentNo)
        ColRecord.append(FirstColDateFormat)
        ColRecord.append(Principal)
        ColRecord.append(Interest)
        ColRecord.append(TotalAmount)
        ColRecord.append(BalAmountStr)

        # Test
        ColRecord.append(NumberOfDay)

        RowRecord.append(ColRecord)
        ColRecord =[]

        CurColDate=FirstColDate
        InstallmentNo+=1
        CountFrequencyP+=1
        CountFrequencyI+=1

    return RowRecord # Finally return funtion


def getScheduleAnnuity(DisburseAmount,ValueDate,Rate,FrequencyType,Frequency,Term,Currency,Locale=None):
    
    CurrencyObj = mktmoney.getCurrencyObj(Currency)
    RowRecord=[]
    ColRecord=[]
    BalAmount=float(DisburseAmount) 

    CurColDate=datetime.strptime(str(ValueDate),'%Y-%m-%d').date() #datetime.date(2015, 1, 1)
    NumberOfDay=0
  
    InstallmentNo=1
    while (InstallmentNo<=Term) :       
        
        if FrequencyType==1:            
            FirstColDate=getNextMonth(CurColDate,Frequency)
            CalRate=Rate/float(100)/float(12)
            NumberOfDay=30*Frequency
        else:
            FirstColDate=getNextWeek(CurColDate,Frequency)
            CalRate=Rate/float(100)/float(12)/float(30)*7     
            NumberOfDay=7*Frequency

        CalRate         *=Frequency # Calculate with Frequency
        Payment         = (CalRate/ (1-(1+CalRate)**(-Term)))*DisburseAmount
        Interest        = BalAmount*CalRate
        Principal       = Payment - Interest
        TotalAmount     = Payment

        # No deduction for last installment, it is absolutely Zero.
        BalAmount       = BalAmount - float(Principal) if InstallmentNo != Term else 0

        #Formatting Currency
        Principal       = mktmoney.toMoney(Principal,CurrencyObj) #format to default #,##0.00
        Interest        = mktmoney.toMoney(Interest,CurrencyObj)
        TotalAmount     = mktmoney.toMoney(TotalAmount,CurrencyObj)
        BalAmountStr    = mktmoney.toMoney(BalAmount,CurrencyObj)
        FirstColDateFormat = mktdate.toDateShort(FirstColDate,Locale) if Locale!=None else FirstColDate

        ColRecord.append(InstallmentNo)
        ColRecord.append(FirstColDateFormat)
        ColRecord.append(Principal)
        ColRecord.append(Interest)
        ColRecord.append(TotalAmount)
        ColRecord.append(BalAmountStr)

        # Test
        ColRecord.append(NumberOfDay)

        RowRecord.append(ColRecord)
        ColRecord =[]

        CurColDate=FirstColDate
        InstallmentNo+=1


    return RowRecord # Finally return funtion