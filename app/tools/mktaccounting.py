# Created by Kiry and Theavuth
# Date: 01 February 2015
# Modify Date : 11 February 2015
# Modify by : Kiry
# All Right Reserved Morakot Technology
# Description :

# BalanceType = "Dr" mean that :
# 	Till Account (USD150350001,KHR150350001,THB150350001)
# 	Dr +
# 	Cr -

# BalanceType = "Cr" mean that :
# 	Customer Account (ACC150350001)
# 	Dr -
# 	Cr +

from app.mktcore.wtfimports 	import *
from .. 						import app, db
from decimal 					import *

import mktmoney 				as mktmoney 
import mktdate 					as mktdate
from app.Account.models 		import *

import app.tools.mktgl 					as mktgl
import app.tools.mktautoid 				as mktAutoID
import mktsetting 						as mktsetting
import app.tools.mktcurrencyrevaluate 	as mktcurrencyrevaluate
import app.tools.mktteller 				as mktteller

def getMatchesBalance(Amount, BalByKey, Type, DrCr):
	try:

		if Type == "Cr":
			BalByKey = (Decimal(BalByKey) + Amount)  if DrCr == "Cr" else (Decimal(BalByKey) - Amount)
		else:
			BalByKey = (Decimal(BalByKey) + Amount)  if DrCr == "Dr" else (Decimal(BalByKey) - Amount)

		return BalByKey

	except:
		raise

def getReportingRate(Currency):
	try:
		# BaseCurrency = mktsetting.getAccSetting().BaseCurrency
		CurrencyObj 	= mktmoney.getCurrencyObj(Currency)
		ReportingRate 	= CurrencyObj.ReportingRate

		return float(ReportingRate)

	except:
		db.session.rollback()
		raise

def getConsolKey(Category="", Currency="", AssClass="", Module="", Key1="", Key2="", Key3="", Key4="", Key5="", Key6="", Key7="", Key8="", Key9=""):
	try:

		Key1 = Key1 if Key1 else ""
		Key2 = Key2 if Key2 else ""
		Key3 = Key3 if Key3 else ""
		Key4 = Key4 if Key4 else ""
		Key5 = Key5 if Key5 else ""
		Key6 = Key6 if Key6 else ""
		Key7 = Key7 if Key7 else ""
		Key8 = Key8 if Key8 else ""
		Key9 = Key9 if Key9 else ""

		GL_KEYS = str(Category) + "." + str(Currency) + "." + str(AssClass) + "." + str(Key1) + "." + str(Key2) + "." + str(Key3) + "." + str(Key4) + "." + str(Key5) + "." + str(Key6) + "." + str(Key7) + "." + str(Key8) + "." + str(Key9)
		
		return GL_KEYS

	except:
		raise

def getConsolKeyBalance(ConsolKey="", Branch=""):
	try:

		Balance = float(0)

		Consol = MKT_CONSOL_BALANCE.query.\
				 filter(MKT_CONSOL_BALANCE.ID == str(ConsolKey)).\
				 filter(MKT_CONSOL_BALANCE.Branch == str(Branch)).\
				 first()

		if Consol:
			Balance = float(Consol.Balance)

		return Balance

	except:
		raise

def getAccountRule(AccID=None):
	try:

		Acc = MKT_ACCOUNT.query.get(AccID)
		if Acc:
			AccProductID = str(Acc.AccProduct)
			AccProduct = MKT_ACC_PRODUCT.query.get(AccProductID)
			if AccProduct:
				AccRuleID = str(AccProduct.Rule) + str(Acc.Currency)
				AccRule = MKT_ACC_RULE_DE.query.get(AccRuleID)
				if AccRule:
					return AccRule
			else:
				return False
		else:
			return False

	except:
		raise

def checkMaxMinBalance(AccID='',Currency='',Amount='',DrCr=''):

	MinB 	= 	''
	MaxB 	= 	''
	Msg 	=	''

	CurrencyObj = mktmoney.getCurrencyObj(Currency)
	Amo = Decimal(Amount)
	if Amo <= 0:
		return "Amount must be more than %s" % mktmoney.toMoney(float(Amo), CurrencyObj, 2)

	Acc = MKT_ACCOUNT.query.get(AccID)
	if Acc:
		BalanceType  = getAccBalanceType(Acc.AccCategory)
		AccProductID = str(Acc.AccProduct)
		AccProduct   = MKT_ACC_PRODUCT.query.get(AccProductID)

		if AccProduct:
			AccRuleID = str(AccProduct.Rule) + str(Acc.Currency)
			AccRule = MKT_ACC_RULE_DE.query.get(AccRuleID)
			if AccRule:
				MinB = Decimal(AccRule.MinBalance)
				MaxB = Decimal(AccRule.MaxBalance)
					 		
			if BalanceType == 'Cr':

				if DrCr == 'Dr':

					if Decimal(Acc.Balance) < Amount:
						return "Account#%s doesn't have sufficient fund. Current balance is %s" %(AccID, mktmoney.toMoney(float(Acc.Balance), CurrencyObj, 2))

					if MinB :	
						if ( Decimal(Acc.Balance) - Amount ) < MinB:
							return "Account#%s required minimum balance %s" %(AccID, mktmoney.toMoney(float(MinB), CurrencyObj, 2))

					if MaxB :
						if ( Decimal(Acc.Balance) - Amount ) > MaxB:
			 				return "Account#%s required maximum balance %s" %(AccID, mktmoney.toMoney(float(MaxB), CurrencyObj, 2))
			
				else:

					if MinB :	
						if (Decimal(Acc.Balance) + Amount ) < MinB:
							return "Account#%s required minimum balance %s" %(AccID, mktmoney.toMoney(float(MinB), CurrencyObj, 2))
			
					if MaxB:
						if (Decimal(Acc.Balance) + Amount) > MaxB:
				 			return "Account#%s required maximum balance %s" %(AccID, mktmoney.toMoney(float(MaxB), CurrencyObj, 2))


			else: #BalanceType == 'Dr'

				if DrCr == 'Cr':

					if Decimal(Acc.Balance) < Amount:
						return "Account#%s doesn't have sufficient fund. Current balance is %s" %(AccID, mktmoney.toMoney(float(Acc.Balance), CurrencyObj, 2))
					
					if MinB :						
						if ( Decimal(Acc.Balance) - Amount ) < MinB:
							return "Account#%s required minimum balance %s" %(AccID, mktmoney.toMoney(float(MinB), CurrencyObj, 2))
			
					if MaxB :
						if ( Decimal(Acc.Balance) - Amount ) > MaxB:
				 			return "Account#%s required maximum balance %s" %(AccID, mktmoney.toMoney(float(MaxB), CurrencyObj, 2))
			
				else:

					if MinB :
						if (Decimal(Acc.Balance) + Amount ) < MinB:
							return "Account#%s required minimum balance %s" %(AccID, mktmoney.toMoney(float(MinB), CurrencyObj, 2))
				
					if MaxB :
						if (Decimal(Acc.Balance) + Amount) > MaxB:
				 			return "Account#%s required maximum balance %s" %(AccID, mktmoney.toMoney(float(MaxB), CurrencyObj, 2))

		return Msg

	else:
		if DrCr == "Dr":
			return "Debit account %s not found."%AccID
		else:
			return "Credit account %s not found."%AccID


def getAccBalanceType(AccCategory):
	DrCr=''
	Category=MKT_CATEGORY.query.get(AccCategory)
	if Category:
		DrCr=Category.BalanceType
		return DrCr
		
	else:
		return "Category not found."

def getAccAvailableBal(AccountID):
	try:
		
		AvailableBal 	= 0
		Account 		= MKT_ACCOUNT.query.get(AccountID)
		if Account:
			AvailableBal = Account.AvailableBal

		return float(AvailableBal)

	except:
		raise

def insert_MKT_ACC_ENTRY(Status,Curr,Inputter,Createdon,Authorizer,
						Authorizeon,ID,Account,Category,Currency,
						DebitCredit,Amount,Module,Transaction,
						TransactionDate,Reference,Note, Branch, PrevBalance):
	
	try:
		AE = MKT_ACC_ENTRY(
				Status 		= Status,
				Curr 		= Curr,
				Inputter 	= Inputter,
				Createdon 	= Createdon,
				Authorizer 	= Authorizer,
				Authorizeon = Authorizeon,
				ID 			= ID,
				Account		= Account,
				Category 	= Category,
				Currency 	= Currency,
				DebitCredit = DebitCredit,
				Amount 		= Amount,
				Module 		= Module,
				Transaction = Transaction,
				TransactionDate = TransactionDate,
				Reference 	= Reference,
				Note 		= Note,
				Branch		= Branch,
				PrevBalance 	= PrevBalance)
		return db.session.add(AE)

	except:
		db.session.rollback()
		raise

def insert_MKT_JOURNAL(	Status, Curr, Inputter, Createdon, Authorizer,
						Authorizeon, ID ,CategoryID ,DebitCredit ,
						Amount, Module, Transaction, TransactionDate ,Reference,
						Currency="", Branch="", GL_KEYS="", LCYAmount="", CustomerID="",Note=""):
	
	Description 	= ""
	PrevBalance 	= 0
	LCYPrevBalance 	= 0

	ConsolBal 		= 	MKT_CONSOL_BALANCE.query.\
						filter(MKT_CONSOL_BALANCE.ID == GL_KEYS).\
						filter(MKT_CONSOL_BALANCE.Branch == Branch).\
						first()
				
	if ConsolBal:

		PrevBalance 	= ConsolBal.Balance # PrevBalance for Beggining Balance on GL Balance Detail
		LCYPrevBalance 	= ConsolBal.LCYBalance

	if Note:
		Description = Note

	try:
		Journal = 	MKT_JOURNAL(
						Status			= 	Status,
						Curr 			= 	Curr,
						Inputter 		= 	Inputter,
						Createdon 		= 	Createdon,
						Authorizer 		= 	Authorizer,
						Authorizeon 	= 	Authorizeon,
						ID 				= 	ID,
						Description 	=	Description,
						CategoryID 		= 	CategoryID,
						CustomerID 		=	CustomerID,
						DebitCredit 	= 	DebitCredit,
						Amount 			= 	Amount,
						PrevBalance 	=	PrevBalance,
						Currency		= 	Currency,
						Module 			= 	Module,
						Transaction 	=	Transaction,
						TransactionDate = 	TransactionDate,
						Reference 		= 	Reference,
						Branch 			= 	Branch,
						LCYAmount 		=	LCYAmount,
						LCYPrevBalance	=	LCYPrevBalance,
						GL_KEYS 		=	GL_KEYS
					)

		return db.session.add(Journal)

	except:
		db.session.rollback()
		raise

def insert_MKT_ACC(Curr,Inputter,Createdon,Authorizer,Authorizeon,Branch,
					ID, AccName, Currency , AccProduct, AccCategory):

	DateNow 	= mktdate.getDateISO()
	try:
		Acc = MKT_ACCOUNT(
				Status			= 'AUTH',
				Curr 			= Curr,
				Inputter 		= Inputter,
				Createdon 		= Createdon,
				Authorizer 		= Authorizer,
				Authorizeon 	= Authorizeon,
				Branch			= Branch,
				ID 				= ID,
				CustomerList 	= '',
				AccName 		= AccName,
				Currency 		= Currency,
				JAccount 		= 'N',
				AccProduct 		= AccProduct,
				AccCategory 	= AccCategory,
				InterestRate 	= 0,
				Charge 			= 0,
				OpenDate 		= DateNow,
				AccrInterest	= 0,
				AccrCurMonth 	= 0,
				AccrCurCapital	= 0,
				Balance 		= 0,
				AvailableBal 	= 0,
				AccrIntBooked 	= 0,
				AccStatus 		= 'O',
				ClosingDate 	= '',
				Blocked 		= 'N',
				LastTranDate 	= DateNow)

		return db.session.add(Acc)
	
	except:
		db.session.rollback()
		raise
		return False


def update_MKT_ACC(AccountE, TranDate, Balance, AvailableBal, LastTran):

	try:
		Acc = MKT_ACCOUNT.query.get(AccountE)
		if Acc:			
			
			Acc.Balance 		= Balance
			Acc.AvailableBal 	= AvailableBal if float(Balance) > 0 else 0
			
			if LastTran.upper() == "YES":
				Acc.LastTranDate 	= TranDate

		return db.session.add(Acc)
	
	except:
		db.session.rollback()
		raise

def calculateLCY(Currency=None, Amount=None):
	try:

		ReportingRate 	= 1
		ReportingRate 	= getReportingRate(Currency)

		LCYAmount = float(Amount) * ReportingRate

		return LCYAmount

	except:
		db.session.rollback()
		raise

def calculateBalance(AccCategory, Balance, Amount, DrCr):
	try:
		BalanceType = getAccBalanceType(AccCategory)

		if BalanceType == "Cr":
			Balance = (Decimal(Balance) + Amount)  if DrCr == "Cr" else ( Decimal(Balance) - Amount )  if DrCr == "Dr" else Amount
		else:
			Balance = (Decimal(Balance) + Amount)  if DrCr == "Dr" else ( Decimal(Balance) - Amount )  if DrCr == "Cr" else Amount

		return Balance

	except:
		db.session.rollback()
		raise

def calculateAvailableBal(AccIDID, Balance):
	try:

		AvailableBal = Balance
		Rule = getAccountRule(AccIDID)
		if Rule:
			AvailableBal = float(AvailableBal) - float(Rule.MinBalance)

		return AvailableBal

	except:
		raise

def getJournalAutoID():
	try:

		JNID = mktAutoID.setAutoID("JN", 10, "MKT_JOURNAL")
		return JNID

	except:
		db.session.rollback()
		raise

def getAccEntryAutoID():
	try:

		AEID = mktAutoID.setAutoID("AE", 6, "MKT_ACC_ENTRY")
		return AEID

	except:
		db.session.rollback()
		raise

def getValidateConsolBalance(Amount, Account, Currency, Branch, DrCr):
	try:

		ConsolKey 	= 	getConsolKey(Account, Currency, "", "JE", "", "", "", "", "", "", "", "", "")
		BalByKey 	= 	getConsolKeyBalance(ConsolKey, Branch)
		Type 		= 	getAccBalanceType(Account)
		Balance 	= 	getMatchesBalance(Amount, BalByKey, Type, DrCr)
		
		if Decimal(Balance) < 0:
			return [False, BalByKey]
		else:
			return [True, BalByKey]

	except:
		raise

def getValidationAccounting(Inputter="",Createdon="",Authorizer="",Authorizeon="",Branch="",DrAcc="",DrCat="",DrCur="",CrAcc="",CrCat="",CrCur="",Amount="",Module="",Tran="",TranDate="",Reference="",Note="",Mode=""):
	
	Message=""
	if Mode != "Direct":
		if not DrAcc:
			Message="Debit account not found for post accounting."
			return False,Message

	if not DrCat:
		Message="Debit category not found for post accounting."
		return False,Message

	if Mode != "Direct":
		if not CrAcc:
			Message="Credit account not found for post accounting."
			return False,Message

	if not CrCat:
		Message="Credit category not found for post accounting."
		return False,Message

	if not Amount:
		Message="Amount not found for post accounting."
		return False,Message

	if not Reference:
		Message="Reference not found for post accounting."
		return False,Message	

	return True,Message

def setAccoutningCheckPoint(Module,ID):

	Message 	= ""
	DrAmount 	= 0
	CrAmount 	= 0
	CashCategory 		= False
	ListModule  		= ['FT','FD','FW','FA','FS','TT','DP','WD','IP','EP']
	ListModuleDirect 	= ['JE','MJ','MJII']

	if Module in ListModule:

		# Check Journal
		JournalObj = MKT_JOURNAL.query.filter(MKT_JOURNAL.Reference==ID)
		if JournalObj.all():

			for row in JournalObj:
				if row.DebitCredit == 'Dr':
					DrAmount += Decimal(row.Amount)
				elif row.DebitCredit == 'Cr':
					CrAmount += Decimal(row.Amount)

			if DrAmount != CrAmount:
				Message = "Total Debit/Credit amount not balance, After commit in journal."
				return False,Message
		else:
			Message = "Accounting transcation was interrupted in journal."
			return False,Message

		DrAmount 	= 0
		CrAmount 	= 0
		
		# Check Account Entry
		AccEntryObj = MKT_ACC_ENTRY.query.filter(MKT_ACC_ENTRY.Reference==ID)
		if AccEntryObj.all():
			for row in AccEntryObj:
				if row.DebitCredit == 'Dr':
					DrAmount += Decimal(row.Amount)
				elif row.DebitCredit == 'Cr':
					CrAmount += Decimal(row.Amount)

			if DrAmount != CrAmount:
				Message = "Total Debit/Credit amount not balance, After commit in journal."
				return False,Message

		else:
			Message = "Accounting transcation was interrupted in account entry."
			return False,Message

	elif Module in ListModuleDirect:

		# Check Journal
		JournalObj = MKT_JOURNAL.query.filter(MKT_JOURNAL.Reference==ID)
		if JournalObj.all():
			for row in JournalObj:
				#Check Cash Category
				Category = row.CategoryID
				if Category in mktteller.getCashCategory():
					CashCategory = True

				if row.DebitCredit == 'Dr':
					DrAmount += Decimal(row.Amount)
				elif row.DebitCredit == 'Cr':
					CrAmount += Decimal(row.Amount)

			if DrAmount != CrAmount:
				Message = "Total Debit/Credit amount not balance, After commit in journal."
				return False,Message

			if CashCategory:
				AccEntryObj = MKT_ACC_ENTRY.query.filter(MKT_ACC_ENTRY.Reference==ID)
				if not AccEntryObj.all():
					Message = "Accounting transcation was interrupted in account entry."
					return False,Message
		else:
			Message = "Accounting transcation was interrupted in journal."
			return False,Message

	return True,Message



def updateCashCateogry(Status="", Curr="", Inputter="", Createdon="", Authorizer="", Authorizeon="",
						Category="",Currency="", DrCr="", Amount="", Module="", Transaction="", TransactionDate="",
						Reference="", Note="",Branch=""):
	try:
		# print "updateCashCateogry"
		if float(Amount) > 0:
			# print "Amount more then 0"
			if Category in mktteller.getCashCategory():
				# print "Category:%s"%Category
				LastTran 	= "Yes"
				Acc 		= mktteller.getCashAccountByCategory(Category,Currency,Inputter,Branch)
				if Acc:
					# print "account:%s"% Acc.ID
					Account 	= Acc.ID
					AEID 		= getAccEntryAutoID()
					# print "after get AEID"
					Balance 	= Decimal(Acc.Balance) if Acc.Balance else Decimal(0)
					Amount 		= Decimal(Amount)
					Balance 	= calculateBalance(Category,Balance, Amount, DrCr)
					# print "Balance%s"%Balance
					PrevBalance = Acc.Balance
					# print "PrevBalance%s"%PrevBalance
					AvailableBal = calculateAvailableBal(Account, Balance)
					# print "after insert_MKT_ACC_ENTRY"
					#Call function to insert into MKT_ACC_ENTRY
					ObjInsertJN 	= insert_MKT_ACC_ENTRY(
											Status, Curr, Inputter, Createdon, Authorizer,
											Authorizeon, AEID, Account, Category, Currency,
											DrCr, Amount, Module,Transaction, TransactionDate,
											Reference, Note, Branch, PrevBalance
											)
					# print "inserted insert_MKT_ACC_ENTRY"
					# Call function to update_MKT_ACC
					ObjUpdateAccountBalance = update_MKT_ACC(Account, TransactionDate, Balance, AvailableBal, LastTran)

					# print "finish update account"
					return True,''
				else:
					# print "error user don't have cash"
					return False,"User don't have cash account."
			else:
				# print "error Category not found."
				return False,"Category not found."
		else:
			# print "Amount must be more then zero."
			return False,"Amount must be more then zero."

	except Exception, e:
		db.session.rollback()
		return False,e

def getCustomerIDByAccount(Account):
	try:

		Customer 	= 	""

		Record 		=	db.session.query(
							MKT_ACCOUNT.ID,
							MKT_ACCOUNT.CustomerList
						).\
						join(
							MKT_CUSTOMER,
							MKT_CUSTOMER.ID == MKT_ACCOUNT.CustomerList
						).\
						filter(
							MKT_ACCOUNT.ID == Account
						).\
						first()

		if Record:

			Customer = Record.CustomerList

		return Customer

	except:
		db.session.rollback()
		raise

def setAmountToCurrencyFormat(Amount, Currency, Mod=""):
	try:

		NewAmount 	=	0

		Module 		=	['JE', 'MJ', 'TC', 'TO', 'WD', 'DP', 'FD', 'FW', 'FT', 'TT']

		Check 		=	[item for item in Module if Mod if item]

		if Amount and Currency:

			if len(Check) > 0:
			
				NewAmount = mktmoney.toMoney(float(Amount), mktmoney.getCurrencyObj(Currency))

				if NewAmount:

					if str(NewAmount).find(',') != -1:

						NewAmount = str(NewAmount).replace(',', '')

			else:

				NewAmount = Amount

		return Decimal(NewAmount)

	except:
		raise

def postAccounting(Status="", Curr="", Inputter="", Createdon="", Authorizer="", Authorizeon="", AEID="", Account="",Category="",
					Currency="", DrCr="", Amount="", Module="", Transaction="", TransactionDate="", Reference="", Note="",
					JNID="", Branch="", GL_KEYS="", Mode="", LastTran="No"):

	if float(Amount) > 0:

		JNID 	= 	getJournalAutoID() # get journal ID
		AEID 	= 	getAccEntryAutoID() # get account entry ID

		# LCYAmount = float(Amount) * MidRate
		LCYAmount = calculateLCY(Currency, Amount)
		CustomerID = ""
		if Mode == "Direct":
			#Call function to insert into MKT_JOURNAL
			ObjInsertJN = insert_MKT_JOURNAL(Status, Curr, Inputter, Createdon, Authorizer,
										Authorizeon, JNID, Category, DrCr, Amount, Module, Transaction,
										TransactionDate, Reference, Currency, Branch, GL_KEYS, LCYAmount, CustomerID, Note)
		else:
			Acc = MKT_ACCOUNT.query.get(Account)

			Balance = calculateBalance(Category,Acc.Balance, Amount, DrCr)
			PrevBalance = Acc.Balance

			AvailableBal = calculateAvailableBal(Account, Balance)
			#Call function to insert into MKT_ACC_ENTRY
			ObjInsertAE = insert_MKT_ACC_ENTRY(
										Status, Curr, Inputter, Createdon, Authorizer,
										Authorizeon, AEID, Account, Category, Currency,
										DrCr, Amount, Module,Transaction, TransactionDate,
										Reference, Note, Branch, PrevBalance
										)
			CustomerID 	= getCustomerIDByAccount(Account)
			#Call function to insert into MKT_JOURNAL
			ObjInsertJN = insert_MKT_JOURNAL(Status, Curr, Inputter, Createdon, Authorizer,
											Authorizeon, JNID, Category, DrCr, Amount, Module, Transaction,
											TransactionDate, Reference, Currency, Branch, GL_KEYS, LCYAmount, CustomerID,Note)

			# Call function to update_MKT_ACC
			ObjUpdateAccountBalance = update_MKT_ACC(Account, TransactionDate, Balance, AvailableBal, LastTran)

		# Check for Currency Revaluation
		ObjRevaluation = mktcurrencyrevaluate.setCategoryRevaluation(Category, Amount, Currency, Branch, LCYAmount)
		# Update Consolidate Balance
		ObjsetConsolBalance = mktgl.setConsolBalance(GL_KEYS, Branch, Currency, Amount, Category, DrCr, Module, TransactionDate, LCYAmount)