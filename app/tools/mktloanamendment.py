from flask 							import flash
from app.mktcore.imports 			import *
from .. 							import app
from sqlalchemy 					import *
from decimal 						import *
from datetime 						import datetime, date, timedelta
import time
import calendar

import app.tools.mktsetting 		as mktsetting
import app.tools.mktdate	 		as mktdate
import mktaccounting 				as mktaccounting
import app.tools.mktkey 			as mktkey
import app.tools.mktaudit 			as mktaudit
import mktmoney 					as mktmoney
import mktpdcollection 				as mktpd
import mktparam 					as mktParam
import mktmessage 					as mktmessage
import mktloan 						as mktloan
import loantools.rescheduletools 	as mktreschedule

def mvLoan(ID=None):
	try:

		LC_HIST = MKT_LOAN_CONTRACT_HIST.query.\
				  filter(MKT_LOAN_CONTRACT_HIST.ID.like(ID + '%'))

		LC_HIST = int(LC_HIST.count()) + 1

		Loan = MKT_LOAN_CONTRACT.query.get(ID)
		if Loan:

			TO_HIST = MKT_LOAN_CONTRACT_HIST(
				Status 				=	Loan.Status,
				Curr 				=	Loan.Curr,
				Inputter 			=	Loan.Inputter,
				Createdon 			=	Loan.Createdon,
				Authorizer 			=	Loan.Authorizer,
				Authorizeon 		=	Loan.Authorizeon,
				ID 					= 	Loan.ID + "@" + str(LC_HIST),
				LoanApplicationID	=	Loan.LoanApplicationID,
				ContractCustomerID 	=	Loan.ContractCustomerID,
				Account 			=	Loan.Account,
				Currency 			=	Loan.Currency,
				Amount 				=	Loan.Amount,
				Disbursed 			=	Loan.Disbursed,
				DisbursedStat 		=	Loan.DisbursedStat,
				ValueDate 			=	Loan.ValueDate,
				Installment 		=	Loan.Installment,
				Term 				=	Loan.Term,
				Cycle 				=	Loan.Cycle,
				MaturityDate 		=	Loan.MaturityDate,
				LoanProduct 		= 	Loan.LoanProduct,
				Category 			=	Loan.Category,
				FreqType 			=	Loan.FreqType,
				Frequency 			=	Loan.Frequency,
				DeliqMode 			=	Loan.DeliqMode,
				LoanPurpose 		=	Loan.LoanPurpose,
				ContractVB 			=	Loan.ContractVB,
				Group 				=	Loan.Group,
				SourceOfFund 		=	Loan.SourceOfFund,
				TotalInterest 		=	Loan.TotalInterest,
				AccrInterest 		= 	Loan.AccrInterest,
				AccrCurrentInt 		=	Loan.AccrCurrentInt,
				AccrIntCurrMonth 	=	Loan.AccrIntCurrMonth,
				AccrIntPreMonth 	=	Loan.AccrIntPreMonth,
				NextAccrDate 		=	Loan.NextAccrDate,
				ContractOfficerID 	=	Loan.ContractOfficerID,
				Branch 				=	Loan.Branch,
				AssetClass 			=	Loan.AssetClass,
				NextRunDate 		=	Loan.NextRunDate,
				InterestRate 		=	Loan.InterestRate
			)
			# Add current LC to history record
			db.session.add(TO_HIST)

		return True

	except:
		db.session.rollback()
		raise

def rmLoan(ID=None):
	try:

		# Add delete current LC
		db.session.query(MKT_LOAN_CONTRACT).filter_by(ID=ID).delete()
		db.session.query(MKT_LOAN_CONTRACT_INAU).filter_by(ID=ID).delete()

	except:
		db.session.rollback()
		raise

def synScheduleToLive(LoanID=""):
	try:

		Schedule = MKT_SCHED_DEFINE_INAU.query.get()

		return "LC1511900004"

	except:
		db.session.rollback()
		raise

def mvScheduleDefine(ID=None):
	try:

		HIST = MKT_SCHED_DEFINE_HIST.query.\
			   filter(MKT_SCHED_DEFINE_HIST.ID == ID)

		HIST = int(HIST.count()) + 1

		row = MKT_SCHED_DEFINE.query.get(ID)
		if row:

			COPY = MKT_SCHED_DEFINE_HIST(
						Status 			=	row.Status,
						Curr 			=	row.Curr,
						Inputter 		=	row.Inputter,
						Createdon 		=	row.Createdon,
						Authorizer 		= 	row.Authorizer,
						Authorizeon 	=	row.Authorizeon,
						ID 				=	row.ID + "@" + str(HIST),
						PrincipalFreq 	=	row.PrincipalFreq,
						InterestFreq 	=	row.InterestFreq,
						FwdBwdKey 		=	row.FwdBwdKey,
						RepMode			= 	row.RepMode
					)

			db.session.add(COPY)

		return True

	except:
		db.session.rollback()
		raise

def rmScheduleDefine(ID=None, Resource=None):
	try:

		# Add delete current schedule define of LC
		if Resource == "INAU":
			db.session.query(MKT_SCHED_DEFINE_INAU).filter_by(ID=ID).delete()
		else:
			db.session.query(MKT_SCHED_DEFINE).filter_by(ID=ID).delete()

	except:
		db.session.rollback()
		raise

def mvRepSchedule(ID=None):
	try:

		HIST = MKT_REP_SCHEDULE_HIST.query.\
			   filter(MKT_REP_SCHEDULE_HIST.LoanID == ID).\
			   distinct(MKT_REP_SCHEDULE_HIST.LoanID)

		HIST = int(HIST.count()) + 1

		LIVE = MKT_REP_SCHEDULE.query.\
			   filter(MKT_REP_SCHEDULE.LoanID == ID).\
			   all()

		if LIVE:
			for row in LIVE:

				COPY = MKT_REP_SCHEDULE_HIST(
							Status 			=	row.Status,
							Curr 			=	row.Curr,
							Inputter 		=	row.Inputter,
							Createdon 		=	row.Createdon,
							Authorizer 		= 	row.Authorizer,
							Authorizeon 	=	row.Authorizeon,
							ID 				=	row.ID + "@" + str(HIST),
							LoanID 			=	row.LoanID,
							No 				=	row.No,
							CollectionDate 	=	row.CollectionDate,
							Principal 		=	row.Principal,
							Interest 		=	row.Interest,
							Balance 		=	row.Balance,
							NumDay 			=	row.NumDay,
							RepStatus 		=	row.RepStatus,
							PartPaidAmt 	=	row.PartPaidAmt,
							NextRunDate 	=	row.NextRunDate
						)

				db.session.add(COPY)

		return True
		
	except:
		db.session.rollback()
		raise

def rmRepSchedule(ID=None, Resource=None):
	try:

		# Add delete current repayment schedule of LC
		if Resource == "INAU":
			db.session.query(MKT_REP_SCHEDULE_INAU).filter_by(LoanID=ID).delete()
		else:
			db.session.query(MKT_REP_SCHEDULE).filter_by(LoanID=ID).delete()

	except:
		db.session.rollback()
		raise

def syncAmendToLC(ID=None, LoanID=None, Resource=""):
	try:

		if Resource.upper() == "INAU":
			Amend = MKT_LOAN_AMENDMENT_INAU.query.get(ID)
		else:
			Amend = MKT_LOAN_AMENDMENT.query.get(ID)

		if Amend:
			row = MKT_LOAN_CONTRACT.query.get(LoanID)
			if row:

				if int(Amend.AddDeduct) == 1:

					NewAmount 		=	float(row.Amount) 	- float(Amend.Amount)
					row.Disbursed 	= 	NewAmount
					row.Amount 		= 	NewAmount

				else:

					NewAmount 		=	float(row.Amount) 	+ float(Amend.Amount)
					row.Disbursed 	= 	NewAmount
					row.Amount 		= 	NewAmount

				# Take update for back date
				IntPerDay 			=	float(0)
				TotalInt 			=	float(0)
				SystemBankDate 		=	Amend.ValueDate
				NextSystemDate 		=	str(mktdate.getBankDate())

				LoanProduct 		= 	row.LoanProduct
				Product 			= 	MKT_LOAN_PRODUCT.query.get(LoanProduct)
				if Product:
					InterestDayBasis=	Product.IntDayBasis
					NumOfDay 	= 	mktreschedule.getNumberOfDay(InterestDayBasis, SystemBankDate, NextSystemDate)
					# NumOfDay 	=	int(NumOfDay) - 1

					if int(NumOfDay) > 0:
						RatePerYear 		=	Amend.Interest
						OutstandingAmount 	=	NewAmount
						IntPerDay 			= 	mktreschedule.getInterestPerDay(float(RatePerYear), float(OutstandingAmount), int(InterestDayBasis), SystemBankDate)
						# print IntPerDay
						TotalInt = float(IntPerDay) * float(NumOfDay)

				row.InterestRate 		= 	Amend.Interest
				row.Installment  		= 	Amend.Installment
				row.Term 		 		= 	Amend.Term
				row.ValueDate 	 		= 	Amend.ValueDate
				row.LoanType 	 		= 	Amend.LoanType
				row.AccrIntCurrMonth 	= 	float(row.AccrCurrentInt) + float(TotalInt)
				row.NextRunDate 		=	Amend.ValueDate
				row.AccrInterest 		=	0
				row.AccrIntPerDay 		=	float(IntPerDay)
				row.AccrCurrentInt 		=	float(row.AccrCurrentInt) + float(TotalInt)
				# row.AccrIntCurrMonth 	=	float(TotalInt)

				db.session.add(row)

			else:
				flash("Loan contract not found.")
				return True

		else:
			flash("Loan amendment not found.")
			return False

		return True

	except:
		db.session.rollback()
		raise

def doLoanTermination(LoanID="", EOD=1, AddDeduct=1):
	try:

		TranDate 		= 	mktdate.getBankDate()
		systemDate 		= 	str(TranDate)
		LoanContract 	= 	MKT_LOAN_CONTRACT.query.get(LoanID)		# Lookup MKT_LOAN_CONTRACT record
		TrueOrFalse 	= 	"1"
		getPDParam 		=	mktParam.getPDParam()
		Param 			= 	getPDParam.RepOrder
		Param 			= 	Param.split()

		if LoanContract:
			Currency 			= 	LoanContract.Currency
			Branch 				= 	LoanContract.Branch
			LC_PRODUCT 			= 	LoanContract.LoanProduct
			CustomerID			=	LoanContract.ContractCustomerID
			LCID 				=	LoanContract.ID
			Suspend 			=	LoanContract.Suspend
			AssClass 			=	LoanContract.AssetClass
			MoreThanOneYear 	=	LoanContract.MoreThanOneYear
			LoanType 			=	LoanContract.LoanType

			Interest 	=	float(LoanContract.AccrCurrentInt) if LoanContract.AccrCurrentInt else float(0)
			Principal 	=	float(LoanContract.Amount) if LoanContract.Amount else float(0)
			AccrCurMont = 	float(LoanContract.AccrIntCurrMonth) if LoanContract.AccrIntCurrMonth else float(0)
			AccrRecInt 	=	float(LoanContract.AccrInterest) if LoanContract.AccrInterest else float(0)
			AccrPrevMon =	float(LoanContract.AccrIntPreMonth) if LoanContract.AccrIntPreMonth else float(0)

			RepSchedule =	MKT_REP_SCHEDULE.query.\
							filter(MKT_REP_SCHEDULE.CollectionDate == systemDate).\
							filter(MKT_REP_SCHEDULE.LoanID == LCID).\
							filter(MKT_REP_SCHEDULE.RepStatus == "0").\
							first()

			if RepSchedule:
				SchInt 		=	float(RepSchedule.Interest) if RepSchedule.Interest else float(0)
				Amount 		= 	SchInt
				# print "SchInt %s" %SchInt
			else:
				Amount = float(AccrRecInt) + float(AccrCurMont)

			Account = MKT_ACCOUNT.query.get(LoanContract.Account) 	# Lookup MKT_ACCOUNT record
			if Account:
				# get Account Balance
				Customer 	= Account.CustomerList
				AccBal 		= float(Account.AvailableBal) if Account.AvailableBal else float(0)
				AccountID 	= Account.ID
				Category 	= Account.AccCategory
				AccProduct 	= Account.AccProduct

				Tran = mktsetting.getAccSetting()
				if not Tran:
					# Call method for error message
					TrueOrFalse = mktmessage.msgError(EOD, "Please setting up accounting setting.")

				k1 = Tran.GL_KEY1
				k2 = Tran.GL_KEY2
				k3 = Tran.GL_KEY3
				k4 = Tran.GL_KEY4
				k5 = Tran.GL_KEY5
				k6 = Tran.GL_KEY6
				k7 = Tran.GL_KEY7
				k8 = Tran.GL_KEY8
				k9 = Tran.GL_KEY9

				k1 = mktkey.getResultKey(k1, LCID, CustomerID)
				k2 = mktkey.getResultKey(k2, LCID, CustomerID)
				k3 = mktkey.getResultKey(k3, LCID, CustomerID)
				k4 = mktkey.getResultKey(k4, LCID, CustomerID)
				k5 = mktkey.getResultKey(k5, LCID, CustomerID)
				k6 = mktkey.getResultKey(k6, LCID, CustomerID)
				k7 = mktkey.getResultKey(k7, LCID, CustomerID)
				k8 = mktkey.getResultKey(k8, LCID, CustomerID)
				k9 = mktkey.getResultKey(k9, LCID, CustomerID)

				# Amount = float(AccrRecInt) + float(AccrCurMont)

				for p in Param:

					if p == 'PE':

						if int(AddDeduct) == 2:
							# Check Pre-Termination Penalty Option
							updatePreTerminationPenalty(LCID)
							Account = MKT_ACCOUNT.query.get(LoanContract.Account)
							if Account:
								# get Account Balance
								AccBal 		= float(Account.AvailableBal) if Account.AvailableBal else float(0)

					if p == 'IN':
						
						DrCr 		= 'Dr'
						Transaction = mktsetting.getAccSetting().InterestTran
						DateTimeNow = mktdate.getDateTimeNow()
						Mode 		= ""

						Amount 		= mktmoney.toMoney(float(Amount), mktmoney.getCurrencyObj(Currency))
						Amount 		= float(Amount.replace(",", ""))

						AccrCurMont = Amount - AccrRecInt
						AccBal 		= float(AccBal) - float(Amount)

						LC_Pro = MKT_LOAN_PRODUCT.query.get(LC_PRODUCT)
						if not LC_Pro:
							# Call method for error message
							TrueOrFalse = mktmessage.msgError(EOD, "Account product not found for loan-" + str(LCID) +".")
						
						if not Transaction:
							# Call method for error message
							TrueOrFalse = mktmessage.msgError(EOD, "Interest collection transaction not found.")

						else:
							GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
							# Debit Customer Account
							mktaccounting.postAccounting(
								"AUTH", 				# Status
								"0", 					# Curr
								"system",				# Inputter
								DateTimeNow, 			# Createdon
								"system",				# Authorizer
								DateTimeNow,			# Authorizeon
								"", 					# AEID
								AccountID,				# Account
								Category,				# Category
								Currency,				# Currency
								DrCr,					# DrCr
								Decimal(Amount), 		# Amount
								"LC",					# Module
								Transaction, 			# Transaction
								TranDate, 				# TransactionDate
								LCID, 					# Reference
								"", 					# Note
								"", 					# JNID
								Branch,					# Branch
								GL_KEYS,				# GL_KEYS
								Mode 					# Mode check to insert Journal for category
							)

						if float(AccrRecInt) > 0:

							Transaction = mktsetting.getAccSetting().CrAccrIntTran
							if not Transaction:
								# Call method for error message
								TrueOrFalse = mktmessage.msgError(EOD, "Accrued interest reversal transaction not found.")
							
							else:
								DrCr 		= "Cr"
								Category 	= LC_Pro.IntReceivableCate.strip()
								DateTimeNow = mktdate.getDateTimeNow()
								Mode 		= "Direct"
								# Credit Reverse Accrued Interest Category
								GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									AccountID,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(AccrRecInt), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									LCID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

						if float(AccrCurMont) > 0:	
							Transaction = mktsetting.getAccSetting().InterestTran
							if not Transaction:
								# Call method for error message
								TrueOrFalse = mktmessage.msgError(EOD, "Accrued interest reversal transaction not found.")
							else:

								Category 	= LC_Pro.IntIncomeCate.strip()
								DateTimeNow = mktdate.getDateTimeNow()
								Mode 		= "Direct"
								DrCr 		= "Cr"
								
								if Transaction:
									GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
									# Credit Accrued Interest Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(AccrCurMont), 	# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										LCID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

								else:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Interest collection transaction not found.")

						# Check if Loan is Suspend Booking II from Suspend Account
						if Suspend.upper() == 'Y':
							Transaction = mktsetting.getAccSetting().DrAccrIntTran
							if not Transaction:
								# Call method for error message
								TrueOrFalse = mktmessage.msgError(EOD, "Interest income booking transaction not found.")

							else:
								Category 	= mktParam.getPDParam().SuspendCrCat
								if not Category:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Suspend credit category not found.")

								DateTimeNow = mktdate.getDateTimeNow()
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
								Mode 		= "Direct"
								DrCr 		= "Dr"
								# Debit Interest Income Category
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(AccrRecInt), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									LCID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

								Category = LC_Pro.IntIncomeCate.strip()
								if not Category:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Interest income category not found.")

								DateTimeNow = mktdate.getDateTimeNow()
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								Mode 		= "Direct"
								DrCr 		= "Cr"
								# Credit Interest Income Category
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(AccrRecInt), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									LCID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

					if p == 'PR':
						
						if float(Principal) > 0:
							Category = Account.AccCategory
							if AccBal >= Principal :
								Amount = Principal
							else :
								Amount = AccBal

							AccBal = float(AccBal) - float(Amount)
							Transaction = mktsetting.getAccSetting().PrincipalTran
							if Amount != 0:
								DateTimeNow = mktdate.getDateTimeNow()
								JNID 		= ""
								AEID 		= ""
								DrCr 		= "Dr"
								Mode 		= ""

								if Transaction:
									GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
									# Debit Customer Account
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										AEID, 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(Amount), 		# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										LCID, 					# Reference
										"", 					# Note
										JNID, 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

								else:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Principal collection transaction not found.")

								DateTimeNow = mktdate.getDateTimeNow()
								JNID 		= ""
								AEID 		= ""
								DrCr 		= "Cr"
								Mode 		= "Direct"
								Category 	= LoanContract.Category

								if Transaction:
									GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
									# Cradit LC Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										AEID, 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(Amount), 		# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										LCID, 					# Reference
										"", 					# Note
										JNID, 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

								else:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Principal collection transaction not found.")

				
				
				# Reverse last booked provisioning
				LoanID 		= LCID;
				AssetClass 	= MKT_ASSET_CLASS.query.get(AssClass)
				ProResvCat 	= AssetClass.ProResvCat
				if not ProResvCat:
					# Call method for error message
					error_msg = "Loan loss receivable category not found."
					mktmessage.msgError(EOD, error_msg)

				ProvExpCat 	= AssetClass.ProvExpCat
				if not ProvExpCat:
					# Call method for error message
					error_msg = "Loan loss reversal category not found."
					mktmessage.msgError(EOD, error_msg)

				# Provisioning Transfer from old Class
				mktpd.reverseProvisioning(LoanID, AssClass, ProResvCat, ProvExpCat, Currency, AccountID, TranDate, Branch)	

			else:
				# Call method for error message
				TrueOrFalse = mktmessage.msgError(EOD, "Account not found.")
		else:
			# Call method for error message
			TrueOrFalse = mktmessage.msgError(EOD, "Loan contract not found")

	except:
		db.session.rollback()
		raise

def loanTerminateWithLessAmount(LoanID, EOD=1, AddDeduct=1):
	try:

		TranDate 		= 	mktdate.getBankDate()
		systemDate 		= 	str(TranDate)
		LoanContract 	= 	MKT_LOAN_CONTRACT.query.get(LoanID)		# Lookup MKT_LOAN_CONTRACT record
		TrueOrFalse 	= 	"1"
		getPDParam 		=	mktParam.getPDParam()
		Param 			= 	getPDParam.RepOrder
		Param 			= 	Param.split()

		if LoanContract:

			Currency 			= 	LoanContract.Currency
			Branch 				= 	LoanContract.Branch
			LC_PRODUCT 			= 	LoanContract.LoanProduct
			CustomerID			=	LoanContract.ContractCustomerID
			LC_ValueDate 		=	LoanContract.ValueDate
			MaturityDate 		=	LoanContract.MaturityDate
			Officer 			=	LoanContract.ContractOfficerID
			Disbursed 			=	Decimal(LoanContract.Disbursed) if LoanContract.Disbursed else Decimal(0)
			CustomerObj 		=	MKT_CUSTOMER.query.get(CustomerID)
			CustomerName 		=	""

			if CustomerObj:
				CustomerName 	=	CustomerObj.LastNameEn + " " + CustomerObj.FirstNameEn

			LCID 				=	LoanContract.ID
			Suspend 			=	LoanContract.Suspend
			AssClass 			=	LoanContract.AssetClass
			MoreThanOneYear 	=	LoanContract.MoreThanOneYear
			LoanType 			=	LoanContract.LoanType

			Interest 	=	float(LoanContract.AccrCurrentInt) if LoanContract.AccrCurrentInt else float(0)
			Principal 	=	float(LoanContract.Amount) if LoanContract.Amount else float(0)
			AccrCurMont = 	float(LoanContract.AccrIntCurrMonth) if LoanContract.AccrIntCurrMonth else float(0)
			AccrRecInt 	=	float(LoanContract.AccrInterest) if LoanContract.AccrInterest else float(0)
			AccrPrevMon =	float(LoanContract.AccrIntPreMonth) if LoanContract.AccrIntPreMonth else float(0)

			RepSchedule =	MKT_REP_SCHEDULE.query.\
							filter(MKT_REP_SCHEDULE.CollectionDate == systemDate).\
							filter(MKT_REP_SCHEDULE.LoanID == LCID).\
							filter(MKT_REP_SCHEDULE.RepStatus == "0").\
							first()

			if RepSchedule:
				SchInt 		=	float(RepSchedule.Interest) if RepSchedule.Interest else float(0)
				Amount 		= 	SchInt
				# print "SchInt %s" %SchInt
			else:
				Amount = float(AccrRecInt) + float(AccrCurMont)

			Account = MKT_ACCOUNT.query.get(LoanContract.Account) 	# Lookup MKT_ACCOUNT record
			if Account:
				# get Account Balance
				Customer 	= Account.CustomerList
				AccBal 		= float(Account.AvailableBal) if Account.AvailableBal else float(0)
				AccountID 	= Account.ID
				Category 	= Account.AccCategory
				AccProduct 	= Account.AccProduct

				Tran = mktsetting.getAccSetting()
				if not Tran:
					# Call method for error message
					TrueOrFalse = mktmessage.msgError(EOD, "Please setting up accounting setting.")

				k1 = Tran.GL_KEY1
				k2 = Tran.GL_KEY2
				k3 = Tran.GL_KEY3
				k4 = Tran.GL_KEY4
				k5 = Tran.GL_KEY5
				k6 = Tran.GL_KEY6
				k7 = Tran.GL_KEY7
				k8 = Tran.GL_KEY8
				k9 = Tran.GL_KEY9

				k1 = mktkey.getResultKey(k1, LCID, CustomerID)
				k2 = mktkey.getResultKey(k2, LCID, CustomerID)
				k3 = mktkey.getResultKey(k3, LCID, CustomerID)
				k4 = mktkey.getResultKey(k4, LCID, CustomerID)
				k5 = mktkey.getResultKey(k5, LCID, CustomerID)
				k6 = mktkey.getResultKey(k6, LCID, CustomerID)
				k7 = mktkey.getResultKey(k7, LCID, CustomerID)
				k8 = mktkey.getResultKey(k8, LCID, CustomerID)
				k9 = mktkey.getResultKey(k9, LCID, CustomerID)

				# Amount 			= 	float(AccrRecInt) + float(AccrCurMont)
				AccrIntRecToRev	=	0
				AccrIntRecToWOF =	float(AccrRecInt)

				TotODAmount 		=	0
				TotPrincipalDue 	=	0
				TotInterestDue 		=	0
				TotPenaltyDue 		=	0
				TotChargeDue 		=	0

				PastDueID 			=	"PD" + str(LoanID)
				CheckPD 			=	MKT_PAST_DUE.query.get(PastDueID)
				
				if CheckPD:

					TotODAmount 	=	float(CheckPD.TotODAmount) if CheckPD.TotODAmount else float(0)
					TotPrincipalDue =	float(CheckPD.TotPrincipalDue) if CheckPD.TotPrincipalDue else float(0)
					TotInterestDue 	=	float(CheckPD.TotInterestDue) if CheckPD.TotInterestDue else float(0)
					TotPenaltyDue 	=	float(CheckPD.TotPenaltyDue) if CheckPD.TotPenaltyDue else float(0)
					TotChargeDue 	=	float(CheckPD.TotChargeDue) if CheckPD.TotChargeDue else float(0)

				PrincipalToWOF 		=	float(Principal) + float(TotPrincipalDue)
				InterestAmount 		=	Amount

				LC_Pro = MKT_LOAN_PRODUCT.query.get(LC_PRODUCT)
				if not LC_Pro:
					# Call method for error message
					TrueOrFalse = mktmessage.msgError(EOD, "Account product not found for loan-" + str(LCID) +".")
							

				for p in Param:

					if p == 'PE':

						if int(AddDeduct) == 2:
							# Check Pre-Termination Penalty Option
							updatePreTerminationPenalty(LCID)
							Account = MKT_ACCOUNT.query.get(LoanContract.Account)
							if Account:
								# get Account Balance
								AccBal 		= float(Account.AvailableBal) if Account.AvailableBal else float(0)

					if p == 'IN':
							
						DrCr 		= 'Dr'
						Transaction = mktsetting.getAccSetting().InterestTran
						DateTimeNow = mktdate.getDateTimeNow()
						Mode 		= ""

						Amount 		= mktmoney.toMoney(float(Amount), mktmoney.getCurrencyObj(Currency))
						Amount 		= float(Amount.replace(",", ""))

						if AccBal >= Amount:
							Amount = float(Amount)
						else:
							Amount = float(AccBal)

						AccBal 	= 	float(AccBal) 		- float(Amount)

						if float(Amount) < float(AccrRecInt):
							AccrIntRecToRev 	=	float(Amount)
							AccrIntRecToWOF 	=	float(AccrRecInt) - float(Amount)
						else:
							AccrIntRecToRev 	=	float(AccrRecInt)
							AccrIntRecToWOF 	=	float(0)

						AccrCurMont 	= 	Amount - AccrIntRecToRev
						# print "AccrIntRecToRev %s" %AccrIntRecToRev
						# print "Amount %s" %Amount
						# print "AccrIntRecToWOF %s" %AccrIntRecToWOF
						# print "Amount work %s" %Amount
						if not Transaction:
							# Call method for error message
							TrueOrFalse = mktmessage.msgError(EOD, "Interest collection transaction not found.")

						else:
							GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
							# Debit Customer Account
							mktaccounting.postAccounting(
								"AUTH", 				# Status
								"0", 					# Curr
								"system",				# Inputter
								DateTimeNow, 			# Createdon
								"system",				# Authorizer
								DateTimeNow,			# Authorizeon
								"", 					# AEID
								AccountID,				# Account
								Category,				# Category
								Currency,				# Currency
								DrCr,					# DrCr
								Decimal(Amount), 		# Amount
								"LC",					# Module
								Transaction, 			# Transaction
								TranDate, 				# TransactionDate
								LCID, 					# Reference
								"", 					# Note
								"", 					# JNID
								Branch,					# Branch
								GL_KEYS,				# GL_KEYS
								Mode 					# Mode check to insert Journal for category
							)

						# Check if customer pay; Reverse Accrued Interest Receivable
						# print AccrIntRecToRev
						if float(AccrIntRecToRev) > 0:
							# print "AccrInt work %s" %AccrIntRecToRev
							Transaction = mktsetting.getAccSetting().CrAccrIntTran
							
							if not Transaction:
								# Call method for error message
								TrueOrFalse = mktmessage.msgError(EOD, "Accrued interest reversal transaction not found.")
							else:

								DrCr 		= "Cr"
								Category 	= LC_Pro.IntReceivableCate.strip()
								
								if not Category:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Accrued interest receivable category not found.")

								DateTimeNow = mktdate.getDateTimeNow()
								Mode 		= "Direct"
								# Credit Reverse Accrued Interest Category
								GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									AccountID,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(AccrIntRecToRev), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									LCID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

						if float(AccrCurMont) > 0:
							# print "AccrCurMont work %s" %AccrCurMont	
							Transaction = mktsetting.getAccSetting().InterestTran
							if not Transaction:
								# Call method for error message
								TrueOrFalse = mktmessage.msgError(EOD, "Accrued interest reversal transaction not found.")
							
							else:

								Category 	= LC_Pro.IntIncomeCate.strip()
								DateTimeNow = mktdate.getDateTimeNow()
								Mode 		= "Direct"
								DrCr 		= "Cr"
								
								if Transaction:
									GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
									# Credit Accrued Interest Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(AccrCurMont), 	# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										LCID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

								else:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Interest collection transaction not found.")

						# Check if Loan is Suspend Booking II from Suspend Account
						if Suspend.upper() == 'Y':
							Transaction = mktsetting.getAccSetting().DrAccrIntTran
							if not Transaction:
								# Call method for error message
								TrueOrFalse = mktmessage.msgError(EOD, "Interest income booking transaction not found.")

							else:
								Category 	= mktParam.getPDParam().SuspendCrCat
								if not Category:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Suspend credit category not found.")

								DateTimeNow = mktdate.getDateTimeNow()
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
								Mode 		= "Direct"
								DrCr 		= "Dr"
								# Debit Interest Income Category
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(AccrIntRecToRev), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									LCID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

								Category = LC_Pro.IntIncomeCate.strip()
								if not Category:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Interest income category not found.")

								DateTimeNow = mktdate.getDateTimeNow()
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								Mode 		= "Direct"
								DrCr 		= "Cr"
								# Debit Interest Income Category
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									"system",				# Inputter
									DateTimeNow, 			# Createdon
									"system",				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									Account,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(AccrIntRecToRev), 	# Amount
									"LC",					# Module
									Transaction, 			# Transaction
									TranDate, 				# TransactionDate
									LCID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

					if p == 'PR':

						if float(AccBal) > 0:

							if float(Principal) > 0:

								Category = Account.AccCategory
								# Category 	=	mktParam.getPDParam().SocialLoanWriteOff
								if not Category:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Drawdown account category not found.")
									# TrueOrFalse = mktmessage.msgError(EOD, "Social loan write-off category not found.")

								Temp 	=	0
								if AccBal >= PrincipalToWOF:

									Amount 				= 	PrincipalToWOF
									Temp  				= 	float(Amount) 			- float(Principal)
									
								else:

									Amount = AccBal

									if float(Amount) >= float(Principal):

										Temp  	= 	float(Amount) - float(Principal)

								AccBal 			= float(AccBal) 			- float(Amount)
								PrincipalToWOF 	= float(Principal) 			- float(Amount)
								Principal 		= float(Principal) 			- float(Amount)
								TotPrincipalDue = float(TotPrincipalDue) 	- float(Temp)

								Transaction 	= mktsetting.getAccSetting().PrincipalTran

								if Transaction:

									DateTimeNow 	= mktdate.getDateTimeNow()
									DrCr 			= "Dr"
									Mode 			= ""

									GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
									# Debit Customer Account
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(Amount), 		# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										LCID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

								else:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Principal collection transaction not found.")

								DateTimeNow = mktdate.getDateTimeNow()
								DrCr 		= "Cr"
								Mode 		= "Direct"
								Category 	= LoanContract.Category

								if not Category:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Loan outstanding category not found.")

								if Transaction:
									GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
									# Cradit LC Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"system",				# Inputter
										DateTimeNow, 			# Createdon
										"system",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(Amount), 		# Amount
										"LC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										LCID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

								else:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Principal collection transaction not found.")

				if float(AccrIntRecToWOF) > 0:

					if Suspend.upper() == 'Y':
						Category 	= mktParam.getPDParam().SuspendCrCat
						GL_KEYS  	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
					else:
						Category 	= LC_Pro.IntIncomeCate.strip()
						GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)

					if not Category:
						# Call method for error message
						TrueOrFalse = mktmessage.msgError(EOD, "Interest income or interest in suspend category not found.")

					Transaction = mktsetting.getAccSetting().WOFTran
					
					if Transaction:

						DateTimeNow = mktdate.getDateTimeNow()
						Mode 		= "Direct"
						DrCr 		= "Dr"

						# Debit Interest Income or Interest in Suspend Category
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"system",				# Inputter
							DateTimeNow, 			# Createdon
							"system",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							Account,				# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(AccrIntRecToWOF), 	# Amount
							"LC",					# Module
							Transaction, 			# Transaction
							TranDate, 				# TransactionDate
							LCID, 					# Reference
							"", 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode 					# Mode check to insert Journal for category
						)

						Category 	= LC_Pro.IntReceivableCate.strip()
						
						if not Category:
							# Call method for error message
							TrueOrFalse = mktmessage.msgError(EOD, "Accrued interest receivable category not found.")

						DateTimeNow = mktdate.getDateTimeNow()
						Mode 		= "Direct"
						DrCr 		= "Cr"
						GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)

						# Credit AIR Category
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"system",				# Inputter
							DateTimeNow, 			# Createdon
							"system",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							Account,				# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(AccrIntRecToWOF), 	# Amount
							"LC",					# Module
							Transaction, 			# Transaction
							TranDate, 				# TransactionDate
							LCID, 					# Reference
							"", 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode 					# Mode check to insert Journal for category
						)

					else:
						# Call method for error message
						TrueOrFalse = mktmessage.msgError(EOD, "Loan write-off transaction not found.")

				
				if float(PrincipalToWOF) > 0:

					Category 	=	mktParam.getPDParam().SocialLoanWriteOff
					
					if not Category:
						# Call method for error message
						TrueOrFalse = mktmessage.msgError(EOD, "Social loan write-off category not found.")

					Transaction = mktsetting.getAccSetting().WOFTran

					if Transaction:

						DateTimeNow = mktdate.getDateTimeNow()
						DrCr 		= "Dr"
						Mode 		= "Direct"

						GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
						# Debit Customer Account
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"system",				# Inputter
							DateTimeNow, 			# Createdon
							"system",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							AccountID,				# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(PrincipalToWOF), 	# Amount
							"LC",					# Module
							Transaction, 			# Transaction
							TranDate, 				# TransactionDate
							LCID, 					# Reference
							"", 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode 					# Mode check to insert Journal for category
						)

						DateTimeNow = mktdate.getDateTimeNow()
						DrCr 		= "Cr"
						Mode 		= "Direct"
						Category 	= LoanContract.Category

						GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
						# Cradit LC Amount
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"system",				# Inputter
							DateTimeNow, 			# Createdon
							"system",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							AccountID,				# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(Principal), 	# Amount
							"LC",					# Module
							Transaction, 			# Transaction
							TranDate, 				# TransactionDate
							LCID, 					# Reference
							"", 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode 					# Mode check to insert Journal for category
						)

						# Cradit LC Amount
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"system",				# Inputter
							DateTimeNow, 			# Createdon
							"system",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							AccountID,				# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(TotPrincipalDue), 	# Amount
							"LC",					# Module
							Transaction, 			# Transaction
							TranDate, 				# TransactionDate
							PastDueID, 				# Reference
							"", 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode 					# Mode check to insert Journal for category
						)

					else:
						# Call method for error message
						TrueOrFalse = mktmessage.msgError(EOD, "Loan write-off transaction not found.")
				
				# Reverse last booked provisioning
				LoanID 		=	LCID;
				AssetClass 	= 	MKT_ASSET_CLASS.query.get(AssClass)
				ProResvCat 	= 	AssetClass.ProResvCat

				if not ProResvCat:
					# Call method for error message
					error_msg = "Loan loss receivable category not found."
					mktmessage.msgError(EOD, error_msg)

				ProvExpCat 	= AssetClass.ProvExpCat
				if not ProvExpCat:
					# Call method for error message
					error_msg = "Loan loss reversal category not found."
					mktmessage.msgError(EOD, error_msg)

				# Provisioning Transfer from old Class
				mktpd.reverseProvisioning(LoanID, AssClass, ProResvCat, ProvExpCat, Currency, AccountID, TranDate, Branch)	

				Amount 		=	float(PrincipalToWOF)
				WOFAmount 	=	float(AccrIntRecToWOF) + float(PrincipalToWOF)
				Account 	=	AccountID
				if float(WOFAmount) > 0:
					mktloan.moveLoanToWOF(LoanID, Branch, TranDate, CustomerID, CustomerName, Currency,
										  Disbursed, Amount, TotODAmount, WOFAmount, Account,
										  TotPrincipalDue, TotInterestDue, TotPenaltyDue, TotChargeDue,
										  LC_ValueDate, MaturityDate, Officer)

			else:
				# Call method for error message
				TrueOrFalse = mktmessage.msgError(EOD, "Account not found.")
		else:
			# Call method for error message
			TrueOrFalse = mktmessage.msgError(EOD, "Loan contract not found")

	except:
		db.session.rollback()
		raise

def setLoanAmendment(ID, Resource="", EOD=1):
	try:

		SystemBankDate 	= str(mktdate.getBankDate())
		TrueOrFalse 	= "1"
		if Resource == "INAU":
			Author = MKT_LOAN_AMENDMENT_INAU.query.get(ID)
		else:
			Author = MKT_LOAN_AMENDMENT.query.get(ID)

		if Author:
			OperationType 	= str(Author.Operation).strip()
			AmtID 			= Author.ID
			LoanID 			= Author.LoanID
			Principal 		= Author.Amount
			Interest 		= Author.Interest
			Installment 	= Author.Installment
			Term 			= Author.Term
			AddDeduct 		= Author.AddDeduct
			ValueDate 		= Author.ValueDate
			Interest 		= str(Interest).split()
			Interest 		= float(Interest[0])

			LC = MKT_LOAN_CONTRACT.query.get(LoanID)
			if LC:
				Disbursed 		= LC.Disbursed
				Account 		= LC.Account
				LcCategory 		= LC.Category
				Branch 			= LC.Branch
				AssClass 		= LC.AssetClass
				PreAssClass 	= LC.AssetClass
				Currency 		= LC.Currency
				Suspend 		= LC.Suspend
				CustomerID 		= LC.ContractCustomerID
				TranDate 		= SystemBankDate
				InterestRate	= LC.InterestRate
				LcAmount 		= float(LC.Amount) if LC.Amount else float(0)
				CurInstall 		= float(LC.AccrCurrentInt) if LC.AccrCurrentInt else float(0)
				DisbursedStat 	= LC.DisbursedStat
				AccrIntPreMonth = float(LC.AccrIntPreMonth) if LC.AccrIntPreMonth else float(0)
				AccrInterest 	= float(LC.AccrInterest) if LC.AccrInterest else float(0)
				LoanProduct 	= LC.LoanProduct
				Product 		= MKT_LOAN_PRODUCT.query.get(LoanProduct)

				InterestRate 	= str(InterestRate).split()
				InterestRate 	= float(InterestRate[0])

				RepSchedule 	=	MKT_REP_SCHEDULE.query.\
									filter(MKT_REP_SCHEDULE.CollectionDate == SystemBankDate).\
									filter(MKT_REP_SCHEDULE.LoanID == LoanID).\
									filter(MKT_REP_SCHEDULE.RepStatus == "0").\
									first()

				if RepSchedule:
					InterestAmount 	=	float(RepSchedule.Interest) if RepSchedule.Interest else float(0)
				else:
					InterestAmount 	=	CurInstall
			
				Acc = MKT_ACCOUNT.query.get(Account)
				if Acc:
					AccID 		= Acc.ID
					AccCategory = Acc.AccCategory
					Amount 		= 0
					toDo 		= 0
					AccBal 		= float(Acc.AvailableBal) if Acc.AvailableBal else float(0)

					Tran = mktsetting.getAccSetting()
					if not Tran:
						# Call method for error message
						TrueOrFalse = mktmessage.msgError(EOD, "Accounting setting record not found, Please go to setting.")

					else:

						k1 = Tran.GL_KEY1
						k2 = Tran.GL_KEY2
						k3 = Tran.GL_KEY3
						k4 = Tran.GL_KEY4
						k5 = Tran.GL_KEY5
						k6 = Tran.GL_KEY6
						k7 = Tran.GL_KEY7
						k8 = Tran.GL_KEY8
						k9 = Tran.GL_KEY9

						k1 = mktkey.getResultKey(k1, LoanID, CustomerID)
						k2 = mktkey.getResultKey(k2, LoanID, CustomerID)
						k3 = mktkey.getResultKey(k3, LoanID, CustomerID)
						k4 = mktkey.getResultKey(k4, LoanID, CustomerID)
						k5 = mktkey.getResultKey(k5, LoanID, CustomerID)
						k6 = mktkey.getResultKey(k6, LoanID, CustomerID)
						k7 = mktkey.getResultKey(k7, LoanID, CustomerID)
						k8 = mktkey.getResultKey(k8, LoanID, CustomerID)
						k9 = mktkey.getResultKey(k9, LoanID, CustomerID)

					TotODAmount =	0
					PastDueID 	= 	"PD" + LoanID
					CheckPD 	=	MKT_PAST_DUE.query.get(PastDueID)
					ReverseAIR 	=	""
					
					if CheckPD:

						TotODAmount 	= 	float(CheckPD.TotODAmount) if CheckPD.TotODAmount else float(0)

					TotalAmountToPay 	=	float(LcAmount) + float(TotODAmount) + float(InterestAmount)

					if OperationType.upper() == "AMT":

						# Check if amendment's date is bank date or last bank date; To do.
						if str(ValueDate) <= str(SystemBankDate):

							# Clear Loan Amendment with Past Due
							mktpd.setPDCollection(PastDueID, EOD)

							if int(AddDeduct) == 1:
								toDo = 1 # Deduct LC Amount

							Amount = float(Principal) if Principal else float(0)

							Error = 0
							if int(toDo) == 1:

								# # Check if today is LC collect date
								# RepSchedule =	MKT_REP_SCHEDULE.query.\
								# 				filter(MKT_REP_SCHEDULE.CollectionDate == str(SystemBankDate)).\
								# 				filter(MKT_REP_SCHEDULE.LoanID == LoanID).\
								# 				filter(MKT_REP_SCHEDULE.RepStatus == "0").\
								# 				first()

								if RepSchedule:
									mktloan.setLoanCollection(LoanID, EOD)
									ReverseAIR = "1"
								else:
									print ""
									# Loan Classification
									mktpd.loanClassification(LoanID)

								if float(AccBal) < float(Amount):
									Error = 1

							LCUpToDate = MKT_LOAN_CONTRACT.query.get(LoanID)
							if LCUpToDate:
								Suspend 	= LCUpToDate.Suspend
								AssClass 	= LCUpToDate.AssetClass

							if int(Error) == 0:
								Transaction = Tran.AmendmentTran
								if Transaction:
									for i in range(0, 2):
										# toDo == 0 Mean that Loan amendment deduct amount; toDo == 1 Mean that Loan amendment add amount;
										if toDo == 0:
											if i == 0:
												DrCr 		= "Dr"
												Mode 		= "Direct"
												Category 	= LcCategory
												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)

											else:
												DrCr 		= "Cr"
												Mode 		= ""
												Category 	= AccCategory
												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
										
										else:
											if i == 0:
												DrCr 		= "Dr"
												Mode 		= ""
												Category 	= AccCategory
												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")

											else:
												DrCr 		= "Cr"
												Mode 		= "Direct"
												Category 	= LcCategory
												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
										
										DateTimeNow = mktdate.getDateTimeNow()
										if not Category:
											# Call method for error message
											TrueOrFalse = mktmessage.msgError(EOD, "Drawdown account or loan outstanding category not found.")

										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"System",				# Inputter
											DateTimeNow, 			# Createdon
											"System",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(Amount), 		# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											AmtID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

									# Check if not due today.
									if ReverseAIR:

										# Reverse AIR and II or from Suspend
										if Suspend.upper() == 'Y':
											Category 	= mktParam.getPDParam().SuspendCrCat
											GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
										else:
											Category 	= Product.IntIncomeCate.strip()
											GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
										
										if not Category:
											# Call method for error message
											TrueOrFalse = mktmessage.msgError(EOD, "Interest income or suspend account category not found.")

										DateTimeNow = mktdate.getDateTimeNow()
										Mode 		= "Direct"
										DrCr 		= "Dr"

										# Debit Interest Income Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(AccrIntPreMonth), 	# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											AmtID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

										Category 	= Product.IntReceivableCate.strip()
										if not Category:
											# Call method for error message
											TrueOrFalse = mktmessage.msgError(EOD, "Accrual interest receivable category not found.")

										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
										DateTimeNow = mktdate.getDateTimeNow()
										Mode 		= "Direct"
										DrCr 		= "Cr"

										# Credit Accrued Interest Receivable Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"System",				# Inputter
											DateTimeNow, 			# Createdon
											"System",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(AccrIntPreMonth), 	# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											AmtID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

										LC_Update = MKT_LOAN_CONTRACT.query.get(LoanID)
										if LC_Update:
											LC_Update.AccrInterest 		= float(LC_Update.AccrInterest) - float(AccrIntPreMonth)
											
											if Suspend.upper() == 'N':
												LC_Update.IntIncEarned 		= float(LC_Update.IntIncEarned) - float(AccrIntPreMonth)
											
											LC_Update.AccrIntPreMonth 	= 0
											db.session.add(LC_Update)

								else:
									# Call method for error message
									TrueOrFalse = mktmessage.msgError(EOD, "Loan amendment transaction not found.")

								# Copy live LC to history LC before Update
								mktaudit.moveAUTHtoHIST(MKT_LOAN_CONTRACT, MKT_LOAN_CONTRACT_HIST, LoanID)
								
								# Update Information Amendment to Loan
								syncAmendToLC(AmtID, LoanID, Resource)

								# Check for disbursement; in case this loan not disburse yet
								mktloan.setLCDisbursement(LoanID, "AUTH", EOD)
								
								# Move MKT_SCHEDULE_DEFINE_INAU to MKT_SCHED_DEFINE
								# Audit 		= mktaudit.getAuditrail()
								DateTimeNow = mktdate.getDateTimeNow()
								Inputter	= 'System'
								Createdon 	= DateTimeNow
								Authorizer 	= 'System'
								Authorizeon	= DateTimeNow

								mktaudit.moveAUTHtoHIST(MKT_SCHED_DEFINE, MKT_SCHED_DEFINE_HIST, LoanID)
								mktaudit.deleteAUTH(MKT_SCHED_DEFINE, LoanID)
								mktaudit.moveINAUtoAUTH(MKT_SCHED_DEFINE, MKT_SCHED_DEFINE_INAU, LoanID,
														Inputter, Createdon, Authorizer, Authorizeon)
								mktaudit.deleteAUTH(MKT_SCHED_DEFINE_INAU, LoanID)

								# Move MKT_REP_SCHEDULE_INAU to MKT_REP_SCHEDULE
								mktaudit.moveAUTHtoHIST(MKT_REP_SCHEDULE, MKT_REP_SCHEDULE_HIST, LoanID, "LoanID")
								mktaudit.deleteAUTH(MKT_REP_SCHEDULE, LoanID, "LoanID")
								mktaudit.moveINAUtoAUTH(MKT_REP_SCHEDULE, MKT_REP_SCHEDULE_INAU, LoanID,
														Inputter, Createdon, Authorizer, Authorizeon, "LoanID")
								mktaudit.deleteAUTH(MKT_REP_SCHEDULE_INAU, LoanID, "LoanID")
								
								# Update Last Outstanding Amount
								mktloan.setUpdateOutstandingAmount(LoanID)

								TrueOrFalse = True

							else:
								msg = "Account#%s doesn't have sufficient fund. Current balance is %s." %(AccID, mktmoney.toMoney(float(AccBal), mktmoney.getCurrencyObj(Currency)))
								# Call method for error message
								TrueOrFalse = mktmessage.msgError(EOD, msg)

						return TrueOrFalse

					else:

						# Start Loan Termination
						AccBal 	= float(Acc.AvailableBal) if Acc.AvailableBal else float(0)

						if str(ValueDate) == str(SystemBankDate):

							if DisbursedStat.upper() == "Y": # Loan has been disbursed
								
								# Clear Loan Termination with Past Due
								PastDueID = "PD" + LoanID
								mktpd.setPDCollection(PastDueID, EOD)

								# Check class change
								mktpd.loanClassification(LoanID, EOD)

								TotalAmountToPay = float(TotalAmountToPay) - float(TotODAmount)

								CheckPreTermination 	= 	getLoanRepaymentDetail(LoanID)
								TotalAmountToPay		+= 	float(CheckPreTermination[1])

								# if float(AccBal) >= float(IncludedPreTermination):
								# 	# Check Pre-Termination Penalty
								# 	updatePreTerminationPenalty(LoanID)
								# 	# Clear Loan Termination with Loan
								# 	doLoanTermination(LoanID, EOD)
								if float(AccBal) >= float(TotalAmountToPay):
									# Clear Loan Termination with Loan
									doLoanTermination(LoanID, EOD, AddDeduct)
								else:
									loanTerminateWithLessAmount(LoanID, EOD, AddDeduct)

								mktaudit.moveAUTHtoHIST(MKT_PD_DATE, MKT_PD_DATE_HIST, PastDueID)
								mktaudit.deleteAUTH(MKT_PAST_DUE, PastDueID)
								mktaudit.deleteAUTH(MKT_PD_DATE, PastDueID)

								# Check class change
								# mktpd.loanClassification(LoanID, EOD)

								# Copy live LC to hist LC
								mktaudit.moveAUTHtoHIST(MKT_LOAN_CONTRACT, MKT_LOAN_CONTRACT_HIST, LoanID)
								
								# Remove live LC
								mktaudit.deleteAUTH(MKT_LOAN_CONTRACT, LoanID)
								
								# Remove live Schedule Define
								mktaudit.moveAUTHtoHIST(MKT_SCHED_DEFINE, MKT_SCHED_DEFINE_HIST, LoanID)
								mktaudit.deleteAUTH(MKT_SCHED_DEFINE, LoanID)
										
								# Remove live Repayment Schedule
								mktaudit.moveAUTHtoHIST(MKT_REP_SCHEDULE, MKT_REP_SCHEDULE_HIST, LoanID, "LoanID")
								mktaudit.deleteAUTH(MKT_REP_SCHEDULE, LoanID, "LoanID")
								
								TrueOrFalse = True

							else: #Loan not has been disbursed.

								# Copy live LC to hist LC
								mktaudit.moveAUTHtoHIST(MKT_LOAN_CONTRACT, MKT_LOAN_CONTRACT_HIST, LoanID)
								
								# Remove live LC
								mktaudit.deleteAUTH(MKT_LOAN_CONTRACT, LoanID)
								
								# Remove live Schedule Define
								mktaudit.moveAUTHtoHIST(MKT_SCHED_DEFINE, MKT_SCHED_DEFINE_HIST, LoanID)
								mktaudit.deleteAUTH(MKT_SCHED_DEFINE, LoanID)
								
								# Remove live Repayment Schedule
								mktaudit.moveAUTHtoHIST(MKT_REP_SCHEDULE, MKT_REP_SCHEDULE_HIST, LoanID, "LoanID")
								mktaudit.deleteAUTH(MKT_REP_SCHEDULE, LoanID, "LoanID")

								TrueOrFalse = True

						else:
							# Call method for error message
							TrueOrFalse = mktmessage.msgError(EOD, "Please check termination date.")

					# Function Ended
					return TrueOrFalse

				else:
					# Call method for error message
					TrueOrFalse = mktmessage.msgError(EOD, "Account not found.")

			else:
				# Call method for error message
				TrueOrFalse = mktmessage.msgError(EOD, "Loan contract not found.")

		else:
			# Call method for error message
			TrueOrFalse = mktmessage.msgError(EOD, "No loan amendment record.")

		if EOD == 1:
			return ""
		else:
			return TrueOrFalse

	except:
		db.session.rollback()
		raise

# Close Loan Amendment by EOD ****
# def setLoanAmendmentForEOD():
# 	try:

# 		SystemBankDate 	= str(mktdate.getBankDate())
# 		Amendment 	= 	MKT_LOAN_AMENDMENT.query.\
# 						filter(MKT_LOAN_AMENDMENT.ValueDate == SystemBankDate).\
# 						filter(MKT_LOAN_AMENDMENT.Operation == "AMT").\
# 						all()

# 		if Amendment:
# 			for item in Amendment:
# 				ID = item.ID
# 				setLoanAmendment(ID)
# 				db.session.commit()
# 				print "Contract %s was scheduled for amendment today." %ID
# 		else:
# 			print "No contract was schedule for amendment today."

# 		return ""

# 	except:
# 		db.session.rollback()
# 		raise

# Get loan term, installment, frequency, frequency type
def getLoanRepaymentDetail(ID):
	try:
		

		Result 		=	[False, 0]
		LoanObj 	= 	MKT_LOAN_CONTRACT.query.get(ID)

		if LoanObj:

			FreqType 	=	LoanObj.FreqType
			Frequency 	=	LoanObj.Frequency
			Installment =	LoanObj.Installment
			Term 		=	LoanObj.Term
			Disbursed 	=	LoanObj.Disbursed
			Outstanding =	LoanObj.OutstandingAmount

			Result 		=	getTerminationPenalty(ID, Term, Installment, Frequency, FreqType, Disbursed, Outstanding)

		return Result

	except Exception, e:
		raise

def getTerminationPenalty(ID, Term, Installment, Frequency, FreqType, Disbursed, Outstanding):
	try:
		
		PDParam 	=	mktParam.getPDParam()
		Result 		=	[False, 0]

		FirstStr 	=	""
		SecondStr 	=	""
		ThirdStr 	=	""
		RepID 		=	[]
		PreTermAmt 	=	0
		BankDate 	=	str(mktdate.getBankDate())
		ValueDate 	=	BankDate
		IntDayBasis = 	"2"

		if PDParam:
			PreTermination 	=	PDParam.PreTermination
			if PreTermination:

				NewStr 		=	PreTermination.split()
				FirstStr 	=	NewStr[0] if len(NewStr) >= 1 else 1
				SecondStr 	=	NewStr[1] if len(NewStr) >= 2 else 1
				ThirdStr 	=	NewStr[2] if len(NewStr) >= 3 else 1

				RepObj 	=	MKT_REP_SCHEDULE.query.\
							filter(MKT_REP_SCHEDULE.LoanID == ID).\
							filter(MKT_REP_SCHEDULE.RepStatus != '0')

				NumberOfPaid	=	RepObj.count()
				# Result 	=	[False, FirstStr]
				# if not RepObj:
				LoanObj 	=	db.session.query(
									MKT_LOAN_CONTRACT.ID,
									MKT_LOAN_CONTRACT.ValueDate,
									MKT_LOAN_PRODUCT.IntDayBasis
								).\
								join(
									MKT_LOAN_PRODUCT,
									MKT_LOAN_PRODUCT.ID == MKT_LOAN_CONTRACT.LoanProduct
								).\
								filter(MKT_LOAN_CONTRACT.ID == ID).\
								first()

				if LoanObj:
					ValueDate 	=	LoanObj.ValueDate
					IntDayBasis =	LoanObj.IntDayBasis

				NumOfDay 	=	mktreschedule.getNumberOfDay(ValueDate, ValueDate, BankDate)
				# print "Config %s" %FirstStr
				# print "NumOfDay %s" %NumOfDay
				if int(FirstStr) > int(NumOfDay):

					if SecondStr == "A":

						for item in range(0, int(ThirdStr)):
							item += 1
							RepID.append("%s%s" %(ID, item))

						RepObj 	=	MKT_REP_SCHEDULE.query.\
									filter(MKT_REP_SCHEDULE.ID.in_(RepID)).\
									all()

						if RepObj:

							for row in RepObj:

								PreTermAmt += float(row.Interest)

					elif SecondStr == "B":

						Perc 		=	float(ThirdStr) / float(100)
						PreTermAmt 	=	float(Outstanding) * Perc

					elif SecondStr == "C":

						Perc 		=	float(ThirdStr) / float(100)
						PreTermAmt 	=	float(Disbursed) * Perc

					Result 	=	[True, PreTermAmt]

		return Result

	except Exception, e:
		return [False, "Error: %s" %e]

def updatePreTerminationPenalty(ID):
	try:
		
		CheckTask 		=	False
		LoanObj 		= 	MKT_LOAN_CONTRACT.query.get(ID)
		Branch 			=	""
		Category 		=	""
		AccBal 			=	0

		if LoanObj:

			Branch 		=	LoanObj.Branch
			Account 	=	LoanObj.Account
			FreqType 	=	LoanObj.FreqType
			Frequency 	=	LoanObj.Frequency
			Installment =	LoanObj.Installment
			Term 		=	LoanObj.Term
			Disbursed 	=	LoanObj.Disbursed
			Outstanding =	LoanObj.OutstandingAmount
			Currency 	=	LoanObj.Currency
			CheckTask 	=	getTerminationPenalty(ID, Term, Installment, Frequency, FreqType, Disbursed, Outstanding)

			AccObj 		=	MKT_ACCOUNT.query.get(Account)
			if AccObj:
				Category 	=	AccObj.AccCategory
				AccBal 		=	AccObj.AvailableBal

		if CheckTask[0] == True:

			PDParam 	=	mktParam.getPDParam()
			Setting 	=	mktsetting.getAccSetting()
			ODPenaltyCat=	""
			Transaction =	""
			DrCr 		=	"Dr"
			TranDate 	=	mktdate.getBankDate()
			Amount 		=	CheckTask[1]
			DateTimeNow = 	mktdate.getDateTimeNow()
			Mode 		=	""

			if float(AccBal) < float(Amount):
				Amount 	=	float(AccBal)

			if Setting:
				Transaction 	=	Setting.PenaltyTran

			if PDParam:
				ODPenaltyCat 	=	PDParam.ODPenaltyCat

			if not ODPenaltyCat:
				# Call method for error message
				error_msg = "Penalty income category not found."
				return mktmessage.msgError(0, error_msg)

			if not Category:
				# Call method for error message
				error_msg = "Account#%s category not found." %Account
				return mktmessage.msgError(0, error_msg)

			if Transaction:

				GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
				# Debit Customer Account
				mktaccounting.postAccounting(
					"AUTH", 				# Status
					"0", 					# Curr
					"system",				# Inputter
					DateTimeNow, 			# Createdon
					"system",				# Authorizer
					DateTimeNow,			# Authorizeon
					"", 					# AEID
					Account,				# Account
					Category,				# Category
					Currency,				# Currency
					DrCr,					# DrCr
					Decimal(Amount), 		# Amount
					"LC",					# Module
					Transaction, 			# Transaction
					TranDate, 				# TransactionDate
					ID, 					# Reference
					"", 					# Note
					"", 					# JNID
					Branch,					# Branch
					GL_KEYS,				# GL_KEYS
					Mode 					# Mode check to insert Journal for category
				)

				# Define new parameter for Credit Penalty Category
				DateTimeNow = mktdate.getDateTimeNow()
				DrCr 		= "Cr"
				Mode 		= "Direct"
				Category 	= ODPenaltyCat
				if not Category:
					# Call method for error message
					error_msg = "Penalty income category not found."
					return mktmessage.msgError(EOD, error_msg)

				GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
				# Credit Penalty Category
				mktaccounting.postAccounting(
					"AUTH", 				# Status
					"0", 					# Curr
					"system",				# Inputter
					DateTimeNow, 			# Createdon
					"system",				# Authorizer
					DateTimeNow,			# Authorizeon
					"", 					# AEID
					Account,				# Account
					Category,				# Category
					Currency,				# Currency
					DrCr,					# DrCr
					Decimal(Amount), 		# Amount
					"LC",					# Module
					Transaction, 			# Transaction
					TranDate, 				# TransactionDate
					ID, 					# Reference
					"", 					# Note
					"", 					# JNID
					Branch,					# Branch
					GL_KEYS,				# GL_KEYS
					Mode 					# Mode check to insert Journal for category
				)

			else:
				# Call method for error message
				error_msg = "Penalty collection transaction not found."
				return mktmessage.msgError(0, error_msg)

		return True

	except Exception, e:
		db.session.rollback()
		return False