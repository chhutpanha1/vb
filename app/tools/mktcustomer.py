from app.mktcore.wtfimports 	import *
from app 						import app,db
from sqlalchemy 				import or_,func

import app.tools.mktdate 		as mktdate
import app.tools.user as mktuser

def getSearchCustomer(Filter=None):
	Branch 		= 	mktuser.getBranch(session["ChangeBranch"]).ID if mktuser.getBranch(session["ChangeBranch"]).ID else ""
	search 		= request.args.get('q')
	action 		= request.args.get('action') if 'action' in request.args else ""
	NAMES 	= []
	FilterCondition = []
	if not Filter:
		if not search.isspace():
			search = search.strip()
			if not search is None:
				if action == "view":
					FilterCondition.append(MKT_CUSTOMER.ID == search)
				else:
					FilterCondition.append(or_(or_(MKT_CUSTOMER.ID.like('%'+search.upper()+'%'), func.upper(MKT_CUSTOMER.FirstNameEn).like('%'+search.upper()+'%')), func.upper(MKT_CUSTOMER.LastNameEn).like('%'+search.upper()+'%')))
					FilterCondition.append(MKT_CUSTOMER.Branch == Branch)
					
				CustomerObj = MKT_CUSTOMER.query.filter(*FilterCondition).all()

	else:
		CustomerObj = MKT_CUSTOMER.query.\
			filter(or_(or_(MKT_CUSTOMER.ID.like('%'+search.upper()+'%'), func.upper(MKT_CUSTOMER.FirstNameEn).like('%'+search.upper()+'%')), func.upper(MKT_CUSTOMER.LastNameEn).like('%'+search.upper()+'%'))).\
			filter(MKT_CUSTOMER.AsGurantor == str(Filter)).\
			filter(MKT_CUSTOMER.Branch == Branch).\
			all()
	if CustomerObj:
		for row in CustomerObj:
			dic = {"id":row.ID, "text":row.ID + " - " + row.LastNameEn + " " + row.FirstNameEn}
			NAMES.append(dic)
			# app.logger.debug(NAMES)
			
	return jsonify(items = NAMES)

def checkAge(CustomerID=None):
	try:

		Customer = MKT_CUSTOMER.query.get(CustomerID)

		if Customer:

			Age = int(mktdate.getAge(Customer.DateOfBirth))
			LoanProduct = request.form['LoanProduct']
			LP = MKT_LOAN_PRODUCT.query.get(LoanProduct) # LP = LoanProduct record

			if LP:
				if LP.MaxAge and Age > int(LP.MaxAge):
					raise ValidationError("Customer's age must younger than or equal %s" %LP.MaxAge)

				if LP.MinAge and Age < int(LP.MinAge):
					raise ValidationError("Customer's age must older than or equal %s" %LP.MinAge)

	except:
		raise

def getCustomerAddress(ID):
	try:
		
		Address = "No Address"

		Obj 	=	db.session.query(
						MKT_CUSTOMER.ID,
						MKT_PROVINCE.Description.label('ProName'),
						MKT_DISTRICT.Description.label('DisName'),
						MKT_COMMUNE.Description.label('ComName'),
						MKT_VILLAGE.Description.label('VilProName')
					).\
					join(
						MKT_PROVINCE,
						MKT_PROVINCE.ID == MKT_CUSTOMER.Province
					).\
					join(
						MKT_DISTRICT,
						MKT_DISTRICT.ID == MKT_CUSTOMER.District
					).\
					join(
						MKT_COMMUNE,
						MKT_COMMUNE.ID == MKT_CUSTOMER.Commune
					).\
					join(
						MKT_VILLAGE,
						MKT_VILLAGE.ID == MKT_CUSTOMER.Village
					).\
					filter(MKT_CUSTOMER.ID == ID).\
					first()

		if Obj:

			Address = Obj.ProName + ", " + Obj.DisName + ", " + Obj.ComName + ", " + Obj.VilProName

		return Address

	except Exception, e:
		raise