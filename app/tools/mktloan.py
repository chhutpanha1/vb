from flask 							import flash
from app.mktcore.imports 			import *
from .. 							import app, db
from sqlalchemy 					import *
from decimal 						import *
from datetime 						import datetime, date, timedelta
import time
import calendar


import mktsetting 					as mktsetting
import user 						as mktuser
import mktdate	 					as mktdate
import mktaccounting 				as mktaccounting
import mktaccount					as mktaccount
import mktautoid 					as mktAutoID
import mktkey 						as mktkey
import mktholiday 					as mktHoliday
import mktpdcollection				as mktpd
import loantools.nonworkingday 		as mktDay
import loantools.rescheduletools 	as mktreschedule
import mktloanamendment 			as mktamt
import mktparam 					as mktparam
import mktaudit 					as mktaudit
import mktmoney 					as mktmoney
import mktautoid 					as mktAutoID
import mktmessage 					as mktmessage
import mktbjstat 					as mktbjstat
# import mktloan 						as mktloan

from app.mktcore.constant			import *

def getValueDate(LoanID, HolidayList):
	try:

		SystemBankDate 	= mktsetting.getBankDate()
		one_day 		= timedelta(days=1) # one day
		NextSystemDate 	= ""

		Record = MKT_REP_SCHEDULE.query.\
				 order_by(MKT_REP_SCHEDULE.CollectionDate.asc()).\
				 filter(MKT_REP_SCHEDULE.LoanID == LoanID).\
				 filter(MKT_REP_SCHEDULE.CollectionDate.in_(HolidayList)).\
				 all()

		if Record:
			for item in Record:
				
				NextSystemDate = item.CollectionDate
				BankDate 	= str(SystemBankDate)
				BankDate 	= BankDate.split()
				BankDate 	= str(BankDate[0]).replace("-", "")
				CollectDate = str(NextSystemDate).replace("-", "")
				
				if int(CollectDate) == int(BankDate):
					# NextSystemDate = HolidayList[-1]
					NextSystemDate = datetime.strptime(str(NextSystemDate),'%Y-%m-%d').date() + one_day
					NextSystemDate = datetime.strptime(str(NextSystemDate),'%Y-%m-%d')
					NextSystemDate = str(NextSystemDate).split()
					NextSystemDate = NextSystemDate[0]
				
				break

			if not NextSystemDate:
				NextSystemDate = HolidayList[-1]
				NextSystemDate = datetime.strptime(str(NextSystemDate),'%Y-%m-%d').date() + one_day
				NextSystemDate = datetime.strptime(str(NextSystemDate),'%Y-%m-%d')
				NextSystemDate = str(NextSystemDate).split()
				NextSystemDate = NextSystemDate[0]

		return NextSystemDate

	except:
		mktbjstat.makeLogFileOnError('Y')
		raise

def getDateEndOfMonth(AccID, HolidayList):
	try:

		Account = MKT_ACCOUNT.query.\
				  filter(MKT_ACCOUNT.NextAccrDate.in_(HolidayList)).\
				  filter(MKT_ACCOUNT.AccStatus == 'O').\
				  filter(MKT_ACC_PRODUCT.ProductType == 'E').\
				  filter(MKT_ACC_PRODUCT.ID == str(AccID)).\
				  all()

		return Account

	except:
		mktbjstat.makeLogFileOnError('Y')
		raise

def checkHolidayForEndOfMonth(HolidayList, DayEndOfMonth):
	try:

		one_day 		= timedelta(days=1) # one day
		SystemBankDate 	= mktsetting.getBankDate()
		NextSystemDate 	= ""

		for item in HolidayList:

			D 		= 	str(item).split("-")
			Month 	=	D[1]
			Day 	= 	D[2]

			E 		= 	str(DayEndOfMonth).split("-")
			MonthE 	=	E[1]
			DayEnd 	= 	E[2]
			
			if int(Month) == int(MonthE) and int(Day) == int(DayEnd):
				
				NextSystemDate = item
				BankDate 	= str(SystemBankDate)
				BankDate 	= BankDate.split()
				BankDate 	= str(BankDate[0]).replace("-", "")
				AccruedDate = str(NextSystemDate).replace("-", "")
				
				if int(AccruedDate) == int(BankDate):
					# NextSystemDate = HolidayList[-1]
					NextSystemDate = datetime.strptime(str(NextSystemDate),'%Y-%m-%d').date() + one_day
					NextSystemDate = datetime.strptime(str(NextSystemDate),'%Y-%m-%d')
					NextSystemDate = str(NextSystemDate).split()
					NextSystemDate = NextSystemDate[0]

				break

			if not NextSystemDate:

				NextSystemDate = HolidayList[-1]
				NextSystemDate = datetime.strptime(str(NextSystemDate),'%Y-%m-%d').date() + one_day
				NextSystemDate = datetime.strptime(str(NextSystemDate),'%Y-%m-%d')
				NextSystemDate = str(NextSystemDate).split()
				NextSystemDate = NextSystemDate[0]

		return NextSystemDate

	except:
		mktbjstat.makeLogFileOnError('Y')
		raise

def setAccrLoanContract(Stat="", Date=""): # This to update daily accrued interest for loan contract
	try:

		TrueOrFalse 	= "1"
		EOD 			= 1
		currentDate 	= mktdate.getDateISO()
		DayEndOfMonth 	= mktsetting.getBankDateObj().NextMonthEnd
		# SystemBankDate 	= mktsetting.getBankDate()

		if Date:
			SystemBankDate 	= Date
		else:
			SystemBankDate 	= mktdate.getBankDate()

		SysBankDate 	= mktsetting.getBankDateObj()
		NextSystemDate 	= SysBankDate.NextSystemDate
		Holiday 		= mktHoliday.getHoliday()
		WeekEndDate 	= SysBankDate.NextWeekend
		NextMonthEnd 	= SysBankDate.NextMonthEnd
		NextYearEnd 	= SysBankDate.NextYearEnd
		Setting 		= mktsetting.getAccSetting()
		BookOption 		= Setting.AccrIntBooking
		ListKeyBooking 	=	{}
		DailyBooking 	= 	False
		DailyTran 		=	""
		TranDate 		= 	SystemBankDate

		NAB 			= mktparam.getPDParam().NAB if mktparam.getPDParam().NAB else 0
		SuspPLNAB 		= mktparam.getPDParam().SuspPLNAB
		LC 				= ""
		
		if not SystemBankDate:
			# Call method for error message
			error_msg = "Bank system date not found."
			TrueOrFalse = mktmessage.msgError(EOD, error_msg)
		else:

			LC = MKT_LOAN_CONTRACT.query.\
				 order_by(asc(MKT_LOAN_CONTRACT.ID)).\
				 filter(MKT_LOAN_CONTRACT.MaturityDate >= str(SystemBankDate)).\
				 filter(MKT_LOAN_CONTRACT.DisbursedStat == 'Y').\
				 filter(MKT_LOAN_CONTRACT.NextRunDate == str(SystemBankDate))
			
			# Check for count loan contract for accr today
			LC_Count = LC.count()
			if Stat == "1":
				return LC_Count

			LC = LC.all()

			# Check if no record set progress bar done.
			mktbjstat.calCompletedPer('AL', 0, 1, 0)

			if LC:

				PD 					= 	0
				RecordNumber 		=	0
				NumberOfCompleted 	= 	0
				NumOfCompletedPerc 	=	0
				NumOfTransaction 	=	mktbjstat.setNumOfTransaction(LC_Count)

				for row in LC:

					# Block update BjStat
					RecordNumber 		+= 1
					NumberOfCompleted 	+= 1

					if int(NumberOfCompleted) == int(NumOfTransaction) or int(RecordNumber) == int(LC_Count):
						NumOfCompletedPerc += float(mktbjstat.getPercPerTransaction(NumberOfCompleted, LC_Count))
						mktbjstat.calCompletedPer('AL', NumberOfCompleted, LC_Count, NumOfCompletedPerc)
						NumberOfCompleted = 0
						
					# End Block

					ID 					= 	row.ID
					CustomerID 			= 	row.ContractCustomerID
					Suspend 			= 	row.Suspend

					Tran = mktsetting.getAccSetting()
					if not Tran:
						# Call method for error message
						error_msg = "Please mktsetting up accounting mktsetting."
						TrueOrFalse = mktmessage.msgError(EOD, error_msg)

					k1 = Tran.GL_KEY1
					k2 = Tran.GL_KEY2
					k3 = Tran.GL_KEY3
					k4 = Tran.GL_KEY4
					k5 = Tran.GL_KEY5
					k6 = Tran.GL_KEY6
					k7 = Tran.GL_KEY7
					k8 = Tran.GL_KEY8
					k9 = Tran.GL_KEY9
					# KeyID = ID if Table == "MKT_LOAN_CONTRACT" else CustomerID
					k1 = mktkey.getResultKey(k1, ID, CustomerID)
					k2 = mktkey.getResultKey(k2, ID, CustomerID)
					k3 = mktkey.getResultKey(k3, ID, CustomerID)
					k4 = mktkey.getResultKey(k4, ID, CustomerID)
					k5 = mktkey.getResultKey(k5, ID, CustomerID)
					k6 = mktkey.getResultKey(k6, ID, CustomerID)
					k7 = mktkey.getResultKey(k7, ID, CustomerID)
					k8 = mktkey.getResultKey(k8, ID, CustomerID)
					k9 = mktkey.getResultKey(k9, ID, CustomerID)

					Contract 			= 	MKT_LOAN_CONTRACT.query.get(ID)
					ProductID 			= 	Contract.LoanProduct
					Product 			= 	MKT_LOAN_PRODUCT.query.get(ProductID)
					AssClass 			= 	row.AssetClass
					ScheduleDefine 		= 	MKT_SCHED_DEFINE.query.get(ID)
					InterestDayBasis 	= 	int(Product.IntDayBasis)
					Branch 				= 	row.Branch
					Account 			= 	row.Account
					Currency 			= 	row.Currency
					ValueDate 			=	row.ValueDate
					OutstandingAmount 	= 	float(row.Amount) if row.Amount else float(0)

					RatePerYear 	= 	float(Contract.InterestRate) if Contract.InterestRate else float(0)
					IntPerDay 		= 	0

					# if not ScheduleDefine:
					# 	# Call method for error message
					# 	error_msg 	= "Schedule define not found for loan %s." %ID
					# 	TrueOrFalse = mktmessage.msgError(EOD, error_msg)
					# else:

					IntPerDay 	= 	mktreschedule.getInterestPerDay(RatePerYear, OutstandingAmount, InterestDayBasis, SystemBankDate)

					check 		= True
					one_day 	= timedelta(days=1) # one day
					StartDate 	= datetime.strptime(str(SystemBankDate),'%Y-%m-%d').date()
					HolidayList	= []
					while check:
						check = mktDay.isNonWorkingDay(StartDate, Holiday)
						if check:
							Item = StartDate
							Item = datetime.strptime(str(Item),'%Y-%m-%d')
							Item = str(Item).split()
							HolidayList.append(str(Item[0]))
							StartDate = StartDate + one_day

					if len(HolidayList) > 0:
						# print "Holiday: %s." %HolidayList
						error_msg = "Holiday: %s." %HolidayList
						mktmessage.msgOutputMsg(error_msg)

						onCollectionDate = getValueDate(ID, HolidayList)
						if onCollectionDate:
							NextSystemDate = onCollectionDate
						else:
							onHoliday = checkHolidayForEndOfMonth(HolidayList, DayEndOfMonth)
							if onHoliday:
								NextSystemDate = onHoliday

					if NextSystemDate:

						NumOfDay 	= 	mktreschedule.getNumberOfDay(InterestDayBasis, SystemBankDate, NextSystemDate)
						# NumOfDay = mktdate.getDateDiff(SystemBankDate, NextSystemDate)
					else:
						NumOfDay = "0"

					if int(NumOfDay) > 0:
						IntPerDay 		= 	float(IntPerDay) * float(NumOfDay)
						IntOfTheDay 	= 	float(IntPerDay) / float(NumOfDay)
					else:
						IntOfTheDay 	= 	float(IntPerDay)

					Contract.AccrCurrentInt 	= 	(float(Contract.AccrCurrentInt) if Contract.AccrCurrentInt else float(0)) + float(IntPerDay)
					Contract.AccrIntCurrMonth 	= 	(float(Contract.AccrIntCurrMonth) if Contract.AccrIntCurrMonth else float(0)) + float(IntPerDay)
					Contract.AccrIntPerDay 		= 	float(IntOfTheDay)
					Contract.NextRunDate 		= 	str(NextSystemDate)
					# Add update record to transaction
					db.session.add(Contract)

					D 		= str(SystemBankDate).split("-")
					Day 	= D[2]

					E 		= str(DayEndOfMonth).split("-")
					DayEnd 	= E[2]
					
					if BookOption == '1':

						if Product:

							InterestReceivable = float(Contract.AccrIntCurrMonth) if Contract.AccrIntCurrMonth else float(0)
							DailyTran 		= mktsetting.getAccSetting().DrAccrIntTran
							Category 		= Product.IntReceivableCate.strip()
							GL_Receivable 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)

							if Suspend.upper() == 'Y':
								Category 	= mktparam.getPDParam().SuspendCrCat
								Transaction = mktsetting.getAccSetting().CrAccrIntTran
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
								if not Category:
									# Call method for error message
									error_msg = "Suspend credit category not found."
									TrueOrFalse = mktmessage.msgError(EOD, error_msg)

							else:
								Category 	= Product.IntIncomeCate.strip()
								Transaction = mktsetting.getAccSetting().DrAccrIntTran
								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
								if not Category:
									# Call method for error message
									error_msg = "Interest income category not found."
									TrueOrFalse = mktmessage.msgError(EOD, error_msg)

							CombineKey = GL_KEYS + "-" + GL_Receivable + "-" + Branch
							if CombineKey in ListKeyBooking:
								BookingAmt 	=	float(IntPerDay) + float(ListKeyBooking[CombineKey])
							else:
								BookingAmt 	=	float(IntPerDay)

							ListKeyBooking.update({CombineKey:BookingAmt})
							DailyBooking = True

							UpdateLCEndMonth 					= 	MKT_LOAN_CONTRACT.query.get(ID)
							UpdateLCEndMonth.AccrIntPreMonth 	= 	UpdateLCEndMonth.AccrIntCurrMonth
							UpdateLCEndMonth.AccrIntCurrMonth 	= 	0
							UpdateLCEndMonth.AccrInterest 		=	(float(UpdateLCEndMonth.AccrInterest) if UpdateLCEndMonth.AccrInterest else float(0)) + float(InterestReceivable)
							if Suspend.upper() == 'N':
								UpdateLCEndMonth.IntIncEarned 		=	(float(UpdateLCEndMonth.IntIncEarned) if UpdateLCEndMonth.IntIncEarned else float(0)) + float(InterestReceivable)
							
							# Add update record to transaction
							db.session.add(UpdateLCEndMonth)
							TrueOrFalse = True

							error_msg = "%s accrual interest has been updated successfully." %ID
							mktmessage.msgOutputMsg(error_msg)

						else:
							# Call method for error message
							error_msg = "Interest income category not found."
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)


					else:
						DailyBooking = False
						# Check if it is end of month, 
						# if so, Recognise Interest Income and book Accrued Interest
						if int(Day) == int(DayEnd):

							InterestReceivable = float(Contract.AccrIntCurrMonth) if Contract.AccrIntCurrMonth else float(0)

							if InterestReceivable > 0:
								if Product:

									Category 	= Product.IntReceivableCate.strip()
									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
									DateTimeNow = mktdate.getDateTimeNow()
									Transaction = mktsetting.getAccSetting().DrAccrIntTran
									Mode 		= "Direct"
									DrCr 		= "Dr"

									if Transaction:
										# Debit Accrued Interest Receivable Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(InterestReceivable), 	# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)


										if Suspend.upper() == 'Y':
											Category 	= mktparam.getPDParam().SuspendCrCat
											GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
											if not Category:
												# Call method for error message
												error_msg = "Suspend credit category not found."
												TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										else:
											Category 	= Product.IntIncomeCate.strip()
											GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
											if not Category:
												# Call method for error message
												error_msg = "Interest income category not found."
												TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										
										DateTimeNow = mktdate.getDateTimeNow()
										Mode 		= "Direct"
										DrCr 		= "Cr"

										# Credit Interest Income Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(InterestReceivable), 	# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

									else:
										# Call method for error message
										error_msg = "Accrued interest booking or reversal transaction not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

								else:
									# Call method for error message
									error_msg = "Interest income category not found."
									TrueOrFalse = mktmessage.msgError(EOD, error_msg)

							UpdateLCEndMonth 					= 	MKT_LOAN_CONTRACT.query.get(ID)
							UpdateLCEndMonth.AccrIntPreMonth 	= 	UpdateLCEndMonth.AccrIntCurrMonth
							UpdateLCEndMonth.AccrIntCurrMonth 	= 	0
							UpdateLCEndMonth.AccrInterest 		=	(float(UpdateLCEndMonth.AccrInterest) if UpdateLCEndMonth.AccrInterest else float(0)) + float(InterestReceivable)
							if Suspend.upper() == 'N':
								UpdateLCEndMonth.IntIncEarned 		=	(float(UpdateLCEndMonth.IntIncEarned) if UpdateLCEndMonth.IntIncEarned else float(0)) + float(InterestReceivable)
							
							# Add update record to transaction
							db.session.add(UpdateLCEndMonth)
							TrueOrFalse = True

						error_msg = "%s accrual interest has been updated successfully." %ID
						mktmessage.msgOutputMsg(error_msg)
						# print "%s accrual interest has been updated successfully." %ID

			else:
				error_msg = "No accrual interest record for update."
				mktmessage.msgOutputMsg(error_msg)
				# print "No accrual interest record for update."
		
		if DailyBooking :
			print ""
			# print "%s" %ListKeyBooking
			for item in ListKeyBooking:
				# print "Key: %s - Amount: %s." %(item, ListKeyBooking[item])
				# print ""
				Amount 		=	ListKeyBooking[item]
				StrKey 		=	item.split("-")
				DrKey 		=	StrKey[1]
				DrCat 		=	DrKey.split(".")[0]
				CrKey 		=	StrKey[0]
				CrCat 		=	CrKey.split(".")[0]
				Currency 	=	CrKey.split(".")[1]
				Branch 		=	StrKey[2]
				Ref 		=	str(TranDate).replace("-", "")
				Ref 		= 	"AIR%s" %Ref

				# print "Dr: %s - Cat: %s - Currency: %s." %(DrKey, DrCat, Currency)
				# print "Cr: %s - Cat: %s - Currency: %s." %(CrKey, CrCat, Currency)
				# print "Amount: %s." %(Amount)
				# print "Branch: %s." %(Branch)
				# print ""

				DateTimeNow = mktdate.getDateTimeNow()
				Transaction = DailyTran

				if Transaction:
					# Debit Accrued Interest Receivable Category
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"System",				# Inputter
						DateTimeNow, 			# Createdon
						"System",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						"",						# Account
						DrCat,					# Category
						Currency,				# Currency
						"Dr",					# DrCr
						Decimal(Amount), 		# Amount
						"LC",					# Module
						Transaction, 			# Transaction
						TranDate, 				# TransactionDate
						Ref, 					# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						DrKey,					# GL_KEYS
						"Direct" 				# Mode check to insert Journal for category
					)
					
					# Credit Interest Income Category
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"System",				# Inputter
						DateTimeNow, 			# Createdon
						"System",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						"",						# Account
						CrCat,					# Category
						Currency,				# Currency
						"Cr",					# DrCr
						Decimal(Amount), 		# Amount
						"LC",					# Module
						Transaction, 			# Transaction
						TranDate, 				# TransactionDate
						Ref, 					# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						CrKey,					# GL_KEYS
						"Direct" 				# Mode check to insert Journal for category
					)

					db.session.commit()

					msg = "Daily accrued interest is successfully booked. %s - Key: %s - Amount: %s." %(Branch, CrKey, mktmoney.toMoney(float(Amount), mktmoney.getCurrencyObj(Currency)))
					mktmessage.msgOutputMsg(msg)

		# Check if no record set progress bar done for Loan Write-off.
		mktbjstat.calCompletedPer('AL', 0, 1, 0)

		# Clear query object
		del LC

		if int(EOD) == 1:
			return ""
		else:
			return TrueOrFalse

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setAccrAccount(Stat="", Date=""):
	try:

		EOD 			= 1
		TrueOrFalse 	= "1"
		DayEndOfMonth 	= mktsetting.getBankDateObj().NextMonthEnd
		NextSystemDate 	= mktsetting.getBankDateObj().NextSystemDate
		# SystemBankDate 	= mktsetting.getBankDate()

		if Date:
			SystemBankDate 	= Date
		else:
			SystemBankDate 	= mktdate.getBankDate()

		NextWeekend 	= mktsetting.getBankDateObj().NextWeekend
		NextYearEnd 	= mktsetting.getBankDateObj().NextYearEnd
		Holiday 		= mktHoliday.getHoliday()

		Account 	= 	db.session.query(MKT_ACCOUNT.ID, MKT_ACCOUNT.AccrIntBooked, MKT_ACCOUNT.NextAccrDate, MKT_ACCOUNT.Balance, MKT_ACCOUNT.AvailableBal, MKT_ACCOUNT.Currency, MKT_ACCOUNT.Branch, MKT_ACCOUNT.CustomerList, MKT_ACCOUNT.InterestRate, MKT_ACCOUNT.AccrCurMonth, MKT_ACCOUNT.AccrCurCapital, MKT_ACCOUNT.AccCategory, MKT_ACC_PRODUCT.IntDayBasis, MKT_ACC_PRODUCT.Tax, MKT_ACC_PRODUCT.IntAccrBasis, MKT_ACC_PRODUCT.IntCapitalization, MKT_ACC_PRODUCT.IntExpenseCat, MKT_ACC_PRODUCT.IntPayableCat).\
						join(MKT_ACC_PRODUCT, MKT_ACC_PRODUCT.ID == MKT_ACCOUNT.AccProduct).\
						filter(MKT_ACCOUNT.NextAccrDate == str(SystemBankDate)).\
						filter(MKT_ACCOUNT.AccStatus == 'O').\
						filter(MKT_ACC_PRODUCT.ProductType == 'E').\
						filter(MKT_ACCOUNT.InterestRate != "0").\
						filter(MKT_ACCOUNT.Dormant == "N").\
						filter(MKT_ACCOUNT.ClosingDate == "").\
						filter(MKT_ACCOUNT.Blocked == "N")

		# Check for count account for accr today
		AC_Count = Account.count()
		if Stat == "1":
			return AC_Count

		Account =	Account.all()

		# Check if no record set progress bar done.
		mktbjstat.calCompletedPer('AA', 0, 1, 0)

		if Account:

			RecordNumber 		=	0
			NumberOfCompleted 	= 	0
			NumOfCompletedPerc 	=	0
			NumOfTransaction 	=	mktbjstat.setNumOfTransaction(AC_Count)

			for row in Account:

				# Block update BjStat
				RecordNumber 		+= 1
				NumberOfCompleted 	+= 1

				if int(NumberOfCompleted) == int(NumOfTransaction) or int(RecordNumber) == int(AC_Count):
					NumOfCompletedPerc += float(mktbjstat.getPercPerTransaction(NumberOfCompleted, AC_Count))
					mktbjstat.calCompletedPer('AA', NumberOfCompleted, AC_Count, NumOfCompletedPerc)
					NumberOfCompleted = 0
					
				# End Block

				
				ID = row.ID
				InterestRate 	= 	float(row.InterestRate) if row.InterestRate else float(0)
				IntPerDay 		=	float(0)
				if InterestRate > 0:
					
					# Rule = mktaccounting.getAccountRule(ID)
					MinBalance = float(row.Balance) if row.Balance else float(0)
					
					if MinBalance > 0:
						
						IntDayBasis = int(row.IntDayBasis) if row.IntDayBasis else int(1)
						RatePerYear = float(InterestRate)
						Balance 	= float(row.Balance)
						NextAccrDate= row.NextAccrDate

						IntAccrBasis 		= int(row.IntAccrBasis) if row.IntAccrBasis else int(0)
						IntCapitalization 	= int(row.IntCapitalization) if row.IntCapitalization else int(0)
						IntExpenseCat 		= row.IntExpenseCat
						IntPayableCat 		= row.IntPayableCat
						TaxID 				= row.Tax if row.Tax else ""

						check 		= True
						one_day 	= timedelta(days=1) # one day
						StartDate 	= datetime.strptime(str(NextAccrDate),'%Y-%m-%d').date()
						HolidayList	= []
						while check:
							check = mktDay.isNonWorkingDay(StartDate, Holiday)
							if check:
								Item = StartDate
								Item = datetime.strptime(str(Item),'%Y-%m-%d')
								Item = str(Item).split()
								HolidayList.append(str(Item[0]))
								StartDate = StartDate + one_day

						if len(HolidayList) > 0:
							
							# print "Holiday: %s." %HolidayList
							error_msg = "Holiday: %s." %HolidayList
							mktmessage.msgOutputMsg(error_msg)

							if int(IntCapitalization) == 2:
								NewDate 	=	NextWeekend
							elif int(IntCapitalization) == 3:
								NewDate 	=	DayEndOfMonth
							else:
								NewDate 	=	NextYearEnd

							onHoliday = checkHolidayForEndOfMonth(HolidayList, NewDate)
							if onHoliday:
								NextSystemDate = onHoliday

						NumOfDay 	= 1
						if NextSystemDate:
							NumOfDay = mktreschedule.getNumberOfDay(IntDayBasis, NextAccrDate, NextSystemDate)
							# NumOfDay = mktdate.getDateDiff(NextAccrDate, NextSystemDate)

						IntPerDay 	= 	mktreschedule.getInterestPerDay(RatePerYear, MinBalance, IntDayBasis, SystemBankDate)
						
						if int(NumOfDay) > 0:
							IntPerDay 	= 	float(IntPerDay) * float(NumOfDay)

						Contract 	= MKT_ACCOUNT.query.get(ID)

						if Contract:
							AccrCurMonth 		= float(Contract.AccrCurMonth) if Contract.AccrCurMonth else float(0)
							AccrCurCapital 		= float(Contract.AccrCurCapital) if Contract.AccrCurCapital else float(0)
						else:
							AccrCurMonth 		= float(0)
							AccrCurCapital 		= float(0)

						# print "%s accrual interest was updated successfully." %ID
						print_msg = "%s accrual interest was updated successfully." %ID
						mktmessage.msgOutputMsg(print_msg)

						AccountID 	= ID
						Currency 	= Contract.Currency if Contract.Currency else ""
						TranDate 	= str(mktsetting.getBankDate())
						Branch 		= Contract.Branch if Contract.Branch else ""
						CustomerID 	= Contract.CustomerList if Contract.CustomerList else ""
						AccCategory = Contract.AccCategory if Contract.AccCategory else ""
						AccrIntBooked 	= float(Contract.AccrIntBooked) if Contract.AccrIntBooked else float(0)

						CapAmount 	= AccrCurCapital
						
						for i in range(1, 5):

							n 			= 0
							Amount 		= AccrCurMonth

							if i == 1:
								n = 1
							elif i == 2:
								D 	= str(SystemBankDate).split("-")
								Day = D[2]

								E 		= str(NextWeekend).split("-")
								DayEnd 	= E[2]
								
								if int(Day) == int(DayEnd):
									n = 2

							elif i == 3:
								D 	= str(SystemBankDate).split("-")
								Day = D[2]

								E 		= str(DayEndOfMonth).split("-")
								DayEnd 	= E[2]
								
								if int(Day) == int(DayEnd):
									n = 3

							else:
								
								YearEnd 	= str(NextYearEnd).replace("-", "")
								BankDate 	= str(SystemBankDate).replace("-", "")
								
								if int(YearEnd) == int(BankDate):
									n = 4

							if int(IntAccrBasis) == int(n) and Amount > 0:

								Category 	= IntExpenseCat
								if not Category:
									# Call method for error message
									error_msg 	= "Interest expense category not found."
									TrueOrFalse = mktmessage.msgError(EOD, error_msg)

								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
								DateTimeNow = mktdate.getDateTimeNow()
								Transaction = mktsetting.getAccSetting().AccrIntTran
								Mode 		= "Direct"
								DrCr 		= "Dr"

								if Transaction:
									# Debit Accrued Interest Receivable Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"System",				# Inputter
										DateTimeNow, 			# Createdon
										"System",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(Amount), 		# Amount
										"AC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									Category 	= IntPayableCat
									if not Category:
										# Call method for error message
										error_msg = "Interest payable category not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
									DateTimeNow = mktdate.getDateTimeNow()
									Transaction = mktsetting.getAccSetting().IntPayaTran
									Mode 		= "Direct"
									DrCr 		= "Cr"

									# Credit Interest Income Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"System",				# Inputter
										DateTimeNow, 			# Createdon
										"System",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(Amount), 		# Amount
										"AC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									# Update Accrued Current Capital
									UpdateAccrCurrMonth 				= MKT_ACCOUNT.query.get(ID)
									UpdateAccrCurrMonth.AccrIntBooked 	= float(UpdateAccrCurrMonth.AccrIntBooked) + float(UpdateAccrCurrMonth.AccrCurMonth)
									UpdateAccrCurrMonth.AccrCurMonth 	= 0

									AccrIntBooked = float(UpdateAccrCurrMonth.AccrIntBooked)
									db.session.add(UpdateAccrCurrMonth)

								else:
									# Call method for error message
									error_msg = "Accrued interest booking transaction not found."
									TrueOrFalse = mktmessage.msgError(EOD, error_msg)

							if int(IntCapitalization) == int(n) and CapAmount > 0:

								Category 	= IntPayableCat
								if not Category:
									# Call method for error message
									error_msg 	= "Interest payable category not found."
									TrueOrFalse = mktmessage.msgError(EOD, error_msg)

								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
								DateTimeNow = mktdate.getDateTimeNow()
								Transaction = mktsetting.getAccSetting().IntPayaTran
								Mode 		= "Direct"
								DrCr 		= "Dr"

								if Transaction:
									# Debit Accrued Interest Receivable Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"System",				# Inputter
										DateTimeNow, 			# Createdon
										"System",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(AccrIntBooked), # Amount
										"AC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									Category 	= IntExpenseCat
									if not Category:
										# Call method for error message
										error_msg 	= "Interest expense category not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
									DateTimeNow = mktdate.getDateTimeNow()
									Transaction = mktsetting.getAccSetting().AccrIntTran
									Mode 		= "Direct"
									DrCr 		= "Dr"

									IntExpAmt 	=	float(CapAmount) - float(AccrIntBooked)

									# Debit Accrued Interest Receivable Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"System",				# Inputter
										DateTimeNow, 			# Createdon
										"System",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(IntExpAmt), 	# Amount
										"AC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									Category 	= AccCategory
									if not Category:
										# Call method for error message
										error_msg 	= "Account category not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
									DateTimeNow = mktdate.getDateTimeNow()
									# Transaction = mktsetting.getAccSetting().AccrIntTran
									Mode 		= ""
									DrCr 		= "Cr"

									# Credit Interest Income Category
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"System",				# Inputter
										DateTimeNow, 			# Createdon
										"System",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										AccountID,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(CapAmount), 	# Amount
										"AC",					# Module
										Transaction, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

									# Calculate tax for saving account
									Tax = MKT_TAX.query.get(TaxID)
									TaxRate 	= 0
									TaxCategory = ""
									TaxPayment 	= 0

									if Tax:

										TaxRate 	= Tax.Rate
										TaxCategory = Tax.Category

										TaxPayment 	= (float(CapAmount) * float(TaxRate)) / float(100)

										if float(TaxPayment) > 0:
											
											Transaction = mktsetting.getAccSetting().TaxTran
											if not Transaction:
												# Call method for error message
												error_msg 	= "Withholding tax transaction not found."
												TrueOrFalse = mktmessage.msgError(EOD, error_msg)
											else:

												Category 	= AccCategory
												if not Category:
													# Call method for error message
													error_msg 	= "Account category not found."
													TrueOrFalse = mktmessage.msgError(EOD, error_msg)

												else:

													GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
													DateTimeNow = mktdate.getDateTimeNow()
													Mode 		= ""
													DrCr 		= "Dr"

													# Debit Customer Account
													mktaccounting.postAccounting(
														"AUTH", 				# Status
														"0", 					# Curr
														"System",				# Inputter
														DateTimeNow, 			# Createdon
														"System",				# Authorizer
														DateTimeNow,			# Authorizeon
														"", 					# AEID
														AccountID,				# Account
														Category,				# Category
														Currency,				# Currency
														DrCr,					# DrCr
														Decimal(TaxPayment), 	# Amount
														"AC",					# Module
														Transaction, 			# Transaction
														TranDate, 				# TransactionDate
														ID, 					# Reference
														"", 					# Note
														"", 					# JNID
														Branch,					# Branch
														GL_KEYS,				# GL_KEYS
														Mode 					# Mode check to insert Journal for category
													)

													Category 	= TaxCategory
													if not Category:
														# Call method for error message
														error_msg 	= "Withholding tax category not found."
														TrueOrFalse = mktmessage.msgError(EOD, error_msg)

													GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
													DateTimeNow = mktdate.getDateTimeNow()
													Mode 		= "Direct"
													DrCr 		= "Cr"

													# Credit Interest Income Category
													mktaccounting.postAccounting(
														"AUTH", 				# Status
														"0", 					# Curr
														"System",				# Inputter
														DateTimeNow, 			# Createdon
														"System",				# Authorizer
														DateTimeNow,			# Authorizeon
														"", 					# AEID
														AccountID,				# Account
														Category,				# Category
														Currency,				# Currency
														DrCr,					# DrCr
														Decimal(TaxPayment), 	# Amount
														"AC",					# Module
														Transaction, 			# Transaction
														TranDate, 				# TransactionDate
														ID, 					# Reference
														"", 					# Note
														"", 					# JNID
														Branch,					# Branch
														GL_KEYS,				# GL_KEYS
														Mode 					# Mode check to insert Journal for category
													)

									UpdateCapitalize 					= MKT_ACCOUNT.query.get(ID)
									UpdateCapitalize.AccrCurCapital 	= 0
									UpdateCapitalize.AccrIntBooked 		= 0
									db.session.add(UpdateCapitalize)

								else:
									# Call method for error message
									error_msg = "Interest payment transaction not found."
									TrueOrFalse = mktmessage.msgError(EOD, error_msg)

				NextAccr 				= 	MKT_ACCOUNT.query.get(ID)
				NextAccr.AccrInterest 	= (float(NextAccr.AccrInterest) if NextAccr.AccrInterest else float(0)) + IntPerDay
				NextAccr.AccrCurMonth 	= (float(NextAccr.AccrCurMonth) if NextAccr.AccrCurMonth else float(0)) + IntPerDay
				NextAccr.AccrCurCapital = (float(NextAccr.AccrCurCapital) if NextAccr.AccrCurCapital else float(0)) + IntPerDay
				# Contract.NextAccrDate = NextSystemDate
				
				# db.session.commit()
				NextAccr.NextAccrDate	=	str(NextSystemDate)
				db.session.add(NextAccr)
				# Commit Data per Account
				db.session.commit()

		else:
			# print "No account to update accrual interest."
			error_msg = "No account to update accrual interest."
			mktmessage.msgOutputMsg(error_msg)

		# Clear query object
		del Account

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setDormantAccount():
	try:

		SystemBankDate 	= mktsetting.getBankDate()
		TrueOrFalse 	= "1"
		Account = db.session.query(MKT_ACCOUNT.ID, MKT_ACCOUNT.LastTranDate, MKT_ACCOUNT.OpenDate, MKT_ACC_PRODUCT.DormantDays).\
				  join(MKT_ACC_PRODUCT, MKT_ACC_PRODUCT.ID == MKT_ACCOUNT.AccProduct).\
				  filter(MKT_ACCOUNT.AccStatus == 'O').\
				  filter(MKT_ACC_PRODUCT.ProductType == 'E').\
				  filter(MKT_ACCOUNT.ClosingDate == "").\
				  filter(MKT_ACCOUNT.Blocked == "N").\
				  filter(MKT_ACCOUNT.Dormant == "N").\
				  all()

		if Account:
			for item in Account:
				ID 						= item.ID
				Dormant 				= int(item.DormantDays) if item.DormantDays else int(0)
				LastTranDate 			= item.LastTranDate
				OpenDate 				= item.OpenDate
				LastTranDate 			= LastTranDate if LastTranDate else OpenDate
				NumberOfNonActivitiy 	= mktdate.getDateDiff(LastTranDate, SystemBankDate)

				if int(Dormant) > 0 and int(NumberOfNonActivitiy) >= int(Dormant):
					Acc 		= MKT_ACCOUNT.query.get(ID)
					if Acc:
						Acc.Dormant = "Y"
						db.session.add(Acc)
						db.session.commit()
						print_msg = "%s no activity for %s days, dormant is set to 'Yes'." %(ID, str(NumberOfNonActivitiy))
						mktmessage.msgOutputMsg(print_msg)
						# print "%s no activity for %s days, dormant is set to 'Yes'." %(ID, str(NumberOfNonActivitiy))

		# Clear query object
		del Account

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

# Update Accr Interest and Amount during Due Date
def updateAccrCurrInt(LoanID="", Principal=0):
	try:

		currentDate = mktdate.getDateISO()
		DayEndOfMonth = mktsetting.getAccSetting().MONTH_END.strip()
		SystemBankDate = mktsetting.getBankDate()

		LC = MKT_LOAN_CONTRACT.query.\
			 filter(MKT_LOAN_CONTRACT.DisbursedStat == 'Y').\
			 filter(MKT_LOAN_CONTRACT.ID == LoanID).\
			 first()

		
		if LC:
			LC.AccrCurrentInt 	= 0		# Update Accr Current Installment to 0
			LC.AccrIntCurrMonth = 0 	# Update Accr Current Month to 0
			LC.Amount 			= float(LC.Amount) - float(Principal)	# Update Loan Amount
			db.session.add(LC)

		# Clear Query Object
		del LC

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setLCDisbursement(LCID=None, Resource=None, EOD=1, Stat="", Date=""):
	try:

		print ""
		from app.LoanContract.models import MKT_LOAN_CHARGE
		TrueOrFalse = "1"
		# filter(MKT_LOAN_CONTRACT.NextRunDate == str(SystemBankDate))
		SystemBankDate 	= mktdate.getBankDate()
		NextRunDate 	= mktdate.getBankDateObj().NextSystemDate
		NextRunDate 	= NextRunDate.strip()
		
		if Date:
			SystemBankDate 	= Date
		else:
			SystemBankDate 	= mktdate.getBankDate()

		if not NextRunDate:
			# Call method for error message
			error_msg = "Next bank date not found, Please go to mktsetting."
			TrueOrFalse = mktmessage.msgError(EOD, error_msg)

		if Resource and Resource == "INAU":
			Disbursed = MKT_LOAN_CONTRACT_INAU.query.\
						filter(MKT_LOAN_CONTRACT_INAU.ValueDate <= str(SystemBankDate)).\
						filter(MKT_LOAN_CONTRACT_INAU.DisbursedStat == 'N')
						
			if LCID:
				Disbursed = Disbursed.\
							filter(MKT_LOAN_CONTRACT_INAU.ID == str(LCID))

		else:
			Disbursed = MKT_LOAN_CONTRACT.query.\
						filter(MKT_LOAN_CONTRACT.ValueDate <= str(SystemBankDate)).\
						filter(MKT_LOAN_CONTRACT.DisbursedStat == 'N')
						
			if LCID:
				Disbursed = Disbursed.\
							filter(MKT_LOAN_CONTRACT.ID == str(LCID))

		LC_Count = Disbursed.count()
		if Stat == "1":
			return LC_Count

		Disbursed = Disbursed.all()

		# Check if no record set progress bar done.
		mktbjstat.calCompletedPer('LD', 0, 1, 0)

		if Disbursed:

			RecordNumber 		=	0
			NumberOfCompleted 	= 	0
			NumOfCompletedPerc 	=	0
			NumOfTransaction 	=	mktbjstat.setNumOfTransaction(LC_Count)

			for row in Disbursed:

				# Block update BjStat
				RecordNumber 		+= 1
				NumberOfCompleted 	+= 1

				if int(NumberOfCompleted) == int(NumOfTransaction) or int(RecordNumber) == int(LC_Count):
					NumOfCompletedPerc += float(mktbjstat.getPercPerTransaction(NumberOfCompleted, LC_Count))
					mktbjstat.calCompletedPer('LD', NumberOfCompleted, LC_Count, NumOfCompletedPerc)
					NumberOfCompleted = 0
					
				# End Block

				CustomerID 	= row.ContractCustomerID
				ValueDate 	= row.ValueDate

				IntSysDate 	=	str(SystemBankDate).replace("-", "")
				IntValueDate=	str(ValueDate).replace("-", "")

				if int(IntValueDate) <= int(IntSysDate):
					# Update loan disbursement status to 'Y'
					LoanID = row.ID

					try:
						ID 			= 	row.ID
						AssClass 	=	row.AssetClass
						Currency 	= 	row.Currency
						Branch 		= 	row.Branch
						Category 	= 	row.Category
						Amount 		= 	Decimal(row.Disbursed)
						TranDate 	= 	ValueDate
						Account 	= 	row.Account
						
						Tran 		= 	mktsetting.getAccSetting()
						Transaction = 	""
						ChargeTran 	= 	""

						if Tran:
							Transaction = Tran.DisbursedTran
							if not Transaction:
								# Call method for error message
								error_msg = "Loan transaction has not been set, Please go to mktsetting."
								TrueOrFalse = mktmessage.msgError(EOD, error_msg)
							
							ChargeTran = Tran.ChargeTran
							if not ChargeTran:
								# Call method for error message
								error_msg = "Charge transaction has not been set, Please go to mktsetting."
								TrueOrFalse = mktmessage.msgError(EOD, error_msg)
							
							k1 = Tran.GL_KEY1
							k2 = Tran.GL_KEY2
							k3 = Tran.GL_KEY3
							k4 = Tran.GL_KEY4
							k5 = Tran.GL_KEY5
							k6 = Tran.GL_KEY6
							k7 = Tran.GL_KEY7
							k8 = Tran.GL_KEY8
							k9 = Tran.GL_KEY9

						else:
							# Call method for error message
							error_msg = "Please mktsetting up transaction type for loan disbursement before submit the record."
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)

						k1 = mktkey.getResultKey(k1, ID, CustomerID, Resource)
						k2 = mktkey.getResultKey(k2, ID, CustomerID, Resource)
						k3 = mktkey.getResultKey(k3, ID, CustomerID, Resource)
						k4 = mktkey.getResultKey(k4, ID, CustomerID, Resource)
						k5 = mktkey.getResultKey(k5, ID, CustomerID, Resource)
						k6 = mktkey.getResultKey(k6, ID, CustomerID, Resource)
						k7 = mktkey.getResultKey(k7, ID, CustomerID, Resource)
						k8 = mktkey.getResultKey(k8, ID, CustomerID, Resource)
						k9 = mktkey.getResultKey(k9, ID, CustomerID, Resource)

						AccCat = MKT_ACCOUNT.query.get(Account).AccCategory
						if not AccCat:
							# Call method for error message
							error_msg 	= "Account %s category not found." %Account
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)

						for i in range(0, 2):
							
							if i == 0:
								DrCr = "Dr"
								Mode = "Direct"
								Category = Category
								GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
							else:
								DrCr = "Cr"
								Mode = ""
								Category = AccCat
								GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")

							DateTimeNow = mktdate.getDateTimeNow()
							mktaccounting.postAccounting(
								"AUTH", 				# Status
								"0", 					# Curr
								"System",				# Inputter
								DateTimeNow, 			# Createdon
								"System",				# Authorizer
								DateTimeNow,			# Authorizeon
								"", 					# AEID
								Account,				# Account
								Category,				# Category
								Currency,				# Currency
								DrCr,					# DrCr
								Amount, 				# Amount
								"LC",					# Module
								Transaction, 			# Transaction
								TranDate, 				# TransactionDate
								ID, 					# Reference
								"", 					# Note
								"", 					# JNID
								Branch,					# Branch
								GL_KEYS,				# GL_KEYS
								Mode 					# Mode check to insert Journal for category
							)

						if Resource and Resource == "INAU":
							Charge = MKT_LOAN_CHARGE_INAU.query.\
									 filter(MKT_LOAN_CHARGE_INAU.ID == LoanID).\
									 all()
						else:
							Charge = MKT_LOAN_CHARGE.query.\
									 filter(MKT_LOAN_CHARGE.ID == LoanID).\
									 all()

						if Charge:

							from app.Charge.models import MKT_CHARGE
							chargeCat = mktparam.getPDParam().ChargeIncCat

							for cha in Charge:
								AddDeduct = cha.Mode
								if AddDeduct == "1":
									
									if not chargeCat:
										# Call method for error message
										error_msg = "Charge income category not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									else:

										ChargeAmount = cha.Charge

										if cha.RateFixed == "R":
											ChargeAmount = float(Amount) * (float(ChargeAmount) / float(100))

										for i in range(0, 2):
											
											if i == 0:
												DrCr = "Dr"
												Mode = ""
												Cate = AccCat
											
											else:
												DrCr = "Cr"
												Mode = "Direct"
												Cate = chargeCat

											GL_KEYS 	= mktaccounting.getConsolKey(Cate, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
											DateTimeNow = mktdate.getDateTimeNow()
											mktaccounting.postAccounting(
												"AUTH", 				# Status
												"0", 					# Curr
												"System",				# Inputter
												DateTimeNow, 			# Createdon
												"System",				# Authorizer
												DateTimeNow,			# Authorizeon
												"", 					# AEID
												Account,				# Account
												Cate,					# Category
												Currency,				# Currency
												DrCr,					# DrCr
												Decimal(ChargeAmount), 	# Amount
												"LC",					# Module
												ChargeTran, 			# Transaction
												TranDate, 				# TransactionDate
												ID, 					# Reference
												"", 					# Note
												"", 					# JNID
												Branch,					# Branch
												GL_KEYS,				# GL_KEYS
												Mode 					# Mode check to insert Journal for category
											)

					except Exception, e:
						db.session.rollback()
						# Call method for error message
						error_msg 	= "%s" %e
						TrueOrFalse = mktmessage.msgError(EOD, error_msg)

					if Resource and Resource == "INAU":
						Update = MKT_LOAN_CONTRACT_INAU.query.get(LoanID)
					else:
						Update = MKT_LOAN_CONTRACT.query.get(LoanID)

					if Update:
						Update.DisbursedStat 	= 	'Y'
						Update.NextRunDate 		= 	str(SystemBankDate)
						db.session.add(Update)

						AssClass 				=	Update.AssetClass
						OutStandingAmount		=	Update.Amount
						LoanType 				=	Update.LoanType
						MoreThanOneYear 		=	Update.MoreThanOneYear
						Account 				=	Update.Account
						Currency 				=	Update.Currency
						TranDate 				=	SystemBankDate
						ID 						=	Update.ID
						Branch 					=	Update.Branch

						ASS_CLASS 				=	MKT_ASSET_CLASS.query.get(AssClass)
						if not ASS_CLASS:
							# Call method for error message
							error_msg = "Asset class not found."
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)

						else:
							ProvExpCat			=	ASS_CLASS.ProvExpCat
							ProResvCat			=	ASS_CLASS.ProResvCat
							ProvisionAmount 	= 	mktpd.getProvisioningAmount(AssClass, OutStandingAmount, LoanType, MoreThanOneYear, 0)
							# Booking Provisioning To New Class
							mktpd.provisioningBooking(ProvisionAmount, ProvExpCat, ProResvCat, Account, Currency, TranDate, ID, Branch, AssClass)

						if not LCID:
							db.session.commit()

						# print "%s is disbursed successfully." %ID
						print_msg = "%s is disbursed successfully." %ID
						mktmessage.msgOutputMsg(print_msg)

					else:
						# Call method for error message
						error_msg = "%s failed to disburse."  %ID
						TrueOrFalse = mktmessage.msgError(EOD, error_msg)
						
		else:
			# print "No contract was scheduled for disbursement today."
			print_msg = "No contract was scheduled for disbursement today."
			mktmessage.msgOutputMsg(print_msg)

		# Clear Query Object
		del Disbursed

		if int(EOD) == 1:
			return ""
		else:
			return TrueOrFalse

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setPD(ID="", Customer="", Currency="", Category="", PenaltyType="", PenaltyRate="", DueDate="", PD_Principal="", PD_Interest="", PD_Charge="", PD_Penalty="", NextRunDate="", LoanID="", Branch=""):
	try:

		PD = MKT_PAST_DUE.query.get(ID)

		if not PD:
			PD = MKT_PAST_DUE(
					ID 				= 	ID,
					LoanID 			=	LoanID,
					Customer 		=	Customer,
					Currency 		=	Currency,
					Category 		=	Category,
					TotODAmount 	=	0,
					TotPrincipalDue	=	0,
					TotInterestDue	=	0,
					TotPenaltyDue	=	0,
					TotChargeDue	=	0,
					PenaltyType		=	PenaltyType,
					PenaltyRate		=	PenaltyRate,
					Branch 			=	Branch
				 )
			db.session.add(PD)
			db.session.commit()

			PD = MKT_PAST_DUE.query.get(ID)

		TotODAmount 		= 	float(PD_Principal) + float(PD_Interest) + float(PD_Charge) + float(PD_Penalty)
		PD.TotODAmount 		= 	float(PD.TotODAmount) if PD.TotODAmount else 0 + TotODAmount
		PD.TotPrincipalDue 	= 	float(PD.TotPrincipalDue) + float(PD_Principal)
		PD.TotInterestDue 	= 	float(PD.TotInterestDue) + float(PD_Interest)
		PD.TotPenaltyDue 	= 	float(PD.TotPenaltyDue) + float(PD_Penalty)
		PD.TotChargeDue 	= 	float(PD.TotChargeDue) + float(PD_Charge)

		db.session.add(PD)

		D = str(DueDate).split("-")
		Year = D[0][-2:]
		Month = D[1]
		Day = D[2]
		PD_DATE_ID = str(PD.ID) + str(Year) + str(Month) + str(Day)
		TotODAmount = float(PD_Principal) + float(PD_Interest) + float(PD_Charge) + float(PD_Penalty)

		PD_DATE = MKT_PD_DATE(
					ID 				= 	PD.ID,
					PDID 			=	PD_DATE_ID,
					DueDate 		=	DueDate,
					NumDayDue		=	1,
					ODStatus 		=	'PRE',
					TotODAmount 	=	TotODAmount,
					OutAmount 		=	TotODAmount,
					PrincipalDue 	=	PD_Principal,
					OutPriAmount 	=	PD_Principal,
					InterestDue 	=	PD_Interest,
					OutIntAmount 	=	PD_Interest,
					PenaltyDue 		=	PD_Penalty,
					OutPenAmount 	=	PD_Penalty,
					ChargeDue 		=	PD_Charge,
					OutChgAmount 	=	PD_Charge,
					NextRunDate 	=	NextRunDate
				  )

		db.session.add(PD_DATE)

		# return "Past due record was created for %s." %ID
		print_msg = "Past due record was created for %s." %ID
		mktmessage.msgOutputMsg(print_msg)

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

# Method for update loan outstanding amount by ID
def setUpdateOutstandingAmount(LCID=""):
	try:

		if LCID:
			LC_Contract 	=	MKT_LOAN_CONTRACT.query.\
								filter(MKT_LOAN_CONTRACT.ID == LCID)
		else:
			LC_Contract 	=	MKT_LOAN_CONTRACT.query
		
		if LC_Contract:

			for item in LC_Contract:
			
				PDID 		=	"PD" + str(item.ID)
				LC_Amount 	=	float(item.Amount) if item.Amount else float(0)
				ODAmount 	=	mktpd.getTotODAmount(PDID)

				item.OutstandingAmount = float(LC_Amount) + float(ODAmount)
				db.session.add(item)

		else:
			db.session.rollback()

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setLoanCollection(LoanID="", EOD=1, Stat="", Date=""):
	try:

		NumOfRecord 	= 0
		NumOfBadRecord 	= 0
		TrueOrFalse 	= "1"
		Setting 		= mktsetting.getAccSetting()
		BookOption 		= Setting.AccrIntBooking

		# EOD_DATE 	= 	mktdate.getBankDate()
		if Date:
			EOD_DATE 	= Date
		else:
			EOD_DATE 	= mktdate.getBankDate()

		NextRunDate = 	mktdate.getBankDateObj().NextSystemDate
		NextRunDate = 	NextRunDate.strip()
		SCH_REP 	= 	MKT_REP_SCHEDULE.query.\
					  	filter(MKT_REP_SCHEDULE.CollectionDate == str(EOD_DATE)).\
					  	filter(MKT_REP_SCHEDULE.RepStatus == '0')

		if LoanID:
			SCH_REP =	SCH_REP.\
						filter(MKT_REP_SCHEDULE.LoanID == LoanID)

		# Check if method need only count
		LC_Count = SCH_REP.count()
		if Stat == "1":
			return LC_Count

		SCH_REP =	SCH_REP.\
					all()

		# Check if no record set progress bar done.
		mktbjstat.calCompletedPer('LC', 0, 1, 0)

		if SCH_REP:

			RecordNumber 		=	0
			NumberOfCompleted 	= 	0
			NumOfCompletedPerc 	=	0
			NumOfTransaction 	=	mktbjstat.setNumOfTransaction(LC_Count)

			for row in SCH_REP:

				# Block update BjStat
				RecordNumber 		+= 1
				NumberOfCompleted 	+= 1

				if int(NumberOfCompleted) == int(NumOfTransaction) or int(RecordNumber) == int(LC_Count):
					NumOfCompletedPerc += float(mktbjstat.getPercPerTransaction(NumberOfCompleted, LC_Count))
					mktbjstat.calCompletedPer('LC', NumberOfCompleted, LC_Count, NumOfCompletedPerc)
					NumberOfCompleted = 0
					
				# End Block

				ID 			= 	row.LoanID
				Principal 	= 	float(row.Principal) if row.Principal else float(0) # Define Principal
				Interest 	= 	float(row.Interest) if row.Interest else float(0)	# Define Interest
				ChargeAmount= 	float(row.Charge) if row.Charge else float(0)		# Define Charge
				Total 		= 	Principal + Interest + ChargeAmount					# Define Total = Principal + Interest
				AccBal 		= 	0
				
				TranDate 	= 	row.CollectionDate
				Mode 		= 	""
				PayPri 	 	= 	0
				PayCharge 	=	0
				PayInt 		= 	0

				PD_Penalty 		= 0
				PD_Charge 		= 0
				PD_Interest 	= 0
				PD_Principal 	= 0
				DueDate 		= row.CollectionDate

				LoanContract = MKT_LOAN_CONTRACT.query.get(ID)		# Lookup MKT_LOAN_CONTRACT record
				if LoanContract:
					Currency 	= 	LoanContract.Currency
					Branch 		= 	LoanContract.Branch
					LC_PRODUCT 	= 	LoanContract.LoanProduct
					CustomerID	=	LoanContract.ContractCustomerID
					LCID 		=	LoanContract.ID
					AssClass 	=	LoanContract.AssetClass
					Suspend 	=	LoanContract.Suspend

					Account = MKT_ACCOUNT.query.get(LoanContract.Account) 	# Lookup MKT_ACCOUNT record
					if Account:
						# get Account Balance
						Customer 	= Account.CustomerList
						AccBal 		= float(Account.AvailableBal) if Account.AvailableBal else float(0)
						AccountID 	= Account.ID
						Category 	= Account.AccCategory
						AccProduct 	= Account.AccProduct

						NumOfRecord += 1
						Param = mktparam.getPDParam().RepOrder
						Param = Param.split()

						Tran = mktsetting.getAccSetting()
						if not Tran:
							# Call method for error message
							error_msg 	= "Please setting up accounting setting."
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)

						k1 = Tran.GL_KEY1
						k2 = Tran.GL_KEY2
						k3 = Tran.GL_KEY3
						k4 = Tran.GL_KEY4
						k5 = Tran.GL_KEY5
						k6 = Tran.GL_KEY6
						k7 = Tran.GL_KEY7
						k8 = Tran.GL_KEY8
						k9 = Tran.GL_KEY9

						k1 = mktkey.getResultKey(k1, LCID, CustomerID)
						k2 = mktkey.getResultKey(k2, LCID, CustomerID)
						k3 = mktkey.getResultKey(k3, LCID, CustomerID)
						k4 = mktkey.getResultKey(k4, LCID, CustomerID)
						k5 = mktkey.getResultKey(k5, LCID, CustomerID)
						k6 = mktkey.getResultKey(k6, LCID, CustomerID)
						k7 = mktkey.getResultKey(k7, LCID, CustomerID)
						k8 = mktkey.getResultKey(k8, LCID, CustomerID)
						k9 = mktkey.getResultKey(k9, LCID, CustomerID)

						for p in Param:

							if p.upper() == "CH":

								if AccBal >= ChargeAmount :
									Amount = float(ChargeAmount)
								else :
									Amount 		= float(AccBal)
									PD_Charge 	= float(ChargeAmount) - float(AccBal)

								PayCharge 	= float(Amount)
								AccBal 		= float(AccBal) - float(Amount)

								DrCr 		= "Dr"
								Mode 		= ""
								Cate 		= Category
								GL_KEYS 	= mktaccounting.getConsolKey(Cate, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
								DateTimeNow = mktdate.getDateTimeNow()
								Transaction = mktsetting.getAccSetting().ChargeTran
								
								if not Transaction:
									# Call method for error message
									# print "2"
									error_msg = "Charge collection transaction not found."
									TrueOrFalse = mktmessage.msgError(EOD, error_msg)

								else:
									if float(Amount) > 0:
										# Debit Customer Account Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"System",				# Inputter
											DateTimeNow, 			# Createdon
											"System",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											AccountID,				# Account
											Cate,					# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(Amount), 		# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

										DrCr 		= "Cr"
										Mode 		= "Direct"
										Cate 		= mktparam.getPDParam().ChargeIncCat
										if not Cate:
											# Call method for error message
											error_msg = "Charge income category not found, Please setting it up."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										DateTimeNow = mktdate.getDateTimeNow()
										# Credit Charge Income Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"System",				# Inputter
											DateTimeNow, 			# Createdon
											"System",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											AccountID,				# Account
											Cate,					# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(Amount), 		# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

							if p.upper() == 'IN':
								
								if float(Interest) > 0:
									if AccBal >= Interest :
										Amount = float(Interest)
									else :
										Amount 		= float(AccBal)
										PD_Interest = float(Interest) - float(AccBal)

									PayInt 		= float(Amount)
									AccBal 		= float(AccBal) - float(Amount)
									DrCr 		= 'Dr'
									Mode 		= ''
									Transaction = mktsetting.getAccSetting().InterestTran
									DateTimeNow = mktdate.getDateTimeNow()
									
									LC_Pro = MKT_LOAN_PRODUCT.query.get(LC_PRODUCT)
									if not LC_Pro:
										# Call method for error message
										error_msg = "Account product not found for loan-" + str(ID) +"."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)
									
									if not Transaction:
										# Call method for error message
										error_msg = "Interest collection transaction not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									if Amount > 0:
										GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
										# Debit Customer Account
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											AccountID,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(Amount), 			# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

									PrevMonth 	= 	float(LoanContract.AccrIntPreMonth) if LoanContract.AccrIntPreMonth else float(0)
									CurrMonth 	= 	float(LoanContract.AccrIntCurrMonth) if LoanContract.AccrIntCurrMonth else float(0)
									IntIncomeToRev = 0

									if BookOption == '1':
										PrevMonth 	= 	float(LoanContract.AccrCurrentInt) if LoanContract.AccrCurrentInt else float(0)

									if float(PrevMonth) >= float(Interest):
										DifferentAmount 	= 	float(PrevMonth) - float(Interest)
										AIRtoReverse 		= 	float(PrevMonth) if float(Amount) >= float(PrevMonth) else (float(Amount) + DifferentAmount)
										
										IntIncomeToRev 		= 	float(DifferentAmount)

										LoanContract.AccrInterest 		= 	(float(LoanContract.AccrInterest) if LoanContract.AccrInterest else float(0)) - float(DifferentAmount)
										
										if Suspend.upper() == 'N':
											LoanContract.IntIncEarned 	=	(float(LoanContract.IntIncEarned) if LoanContract.IntIncEarned else float(0)) - float(DifferentAmount)

									else:
										DifferentAmount 				= 	float(Interest) - float(PrevMonth)
										AIRtoReverse 					= 	float(PrevMonth) if float(Amount) >= float(PrevMonth) else float(Amount)

										LoanContract.AccrInterest 		= 	(float(LoanContract.AccrInterest) if LoanContract.AccrInterest else float(0)) + float(DifferentAmount)
										
										if Suspend.upper() == 'N':
											LoanContract.IntIncEarned 	=	(float(LoanContract.IntIncEarned) if LoanContract.IntIncEarned else float(0)) + float(DifferentAmount)
									
									# AIR update with paid amount
									LoanContract.AccrInterest 			= 	(float(LoanContract.AccrInterest) if LoanContract.AccrInterest else float(0)) - float(Amount)
									
									LoanContract.AccrIntPreMonth 		= 	0 # After collected move AIR Premonth to zero(0)
									db.session.add(LoanContract)

									Amount 				=	(float(Amount) 		- float(AIRtoReverse)) if float(Amount) >= float(AIRtoReverse) else float(Amount)
									CurrIntIncome 		= 	(float(Interest) 	- float(PrevMonth)) if float(Interest) >= float(PrevMonth) else float(0)
									SettledIntIncome	=	float(CurrIntIncome) if float(Amount) >= float(CurrIntIncome) else float(Amount)
									NewIntReceivable 	=   float(Interest) 	- float(PrevMonth) - float(SettledIntIncome)

									
									Category = LC_Pro.IntReceivableCate.strip()

									if AIRtoReverse > 0:
										DateTimeNow = mktdate.getDateTimeNow()
										Transaction = mktsetting.getAccSetting().CrAccrIntTran
										Mode 		= "Direct"
										DrCr 		= "Cr"
										
										if Transaction:
											# Credit Reverse Accrued Interest Category
											GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
											mktaccounting.postAccounting(
												"AUTH", 				# Status
												"0", 					# Curr
												"system",				# Inputter
												DateTimeNow, 			# Createdon
												"system",				# Authorizer
												DateTimeNow,			# Authorizeon
												"", 					# AEID
												AccountID,				# Account
												Category,				# Category
												Currency,				# Currency
												DrCr,					# DrCr
												Decimal(AIRtoReverse), 	# Amount
												"LC",					# Module
												Transaction, 			# Transaction
												TranDate, 				# TransactionDate
												ID, 					# Reference
												"", 					# Note
												"", 					# JNID
												Branch,					# Branch
												GL_KEYS,				# GL_KEYS
												Mode 					# Mode check to insert Journal for category
											)

										else:
											# Call method for error message
											error_msg = "Accrued interest reversal transaction not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)
										
									if Suspend.upper() == 'Y':
										Category 	= mktparam.getPDParam().SuspendCrCat
										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
									else:
										Category 	= LC_Pro.IntIncomeCate.strip()
										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
									
									if not Category:
										# Call method for error message
										error_msg = "Suspend credit or interest income category not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									if SettledIntIncome > 0:
										DateTimeNow = mktdate.getDateTimeNow()
										Transaction = mktsetting.getAccSetting().DrAccrIntTran
										Mode 		= "Direct"
										DrCr 		= "Cr"
										
										if Transaction:
											# Credit Interest Income Category
											mktaccounting.postAccounting(
												"AUTH", 				# Status
												"0", 					# Curr
												"system",				# Inputter
												DateTimeNow, 			# Createdon
												"system",				# Authorizer
												DateTimeNow,			# Authorizeon
												"", 					# AEID
												AccountID,				# Account
												Category,				# Category
												Currency,				# Currency
												DrCr,					# DrCr
												Decimal(SettledIntIncome), 	# Amount
												"LC",					# Module
												Transaction, 			# Transaction
												TranDate, 				# TransactionDate
												ID, 					# Reference
												"", 					# Note
												"", 					# JNID
												Branch,					# Branch
												GL_KEYS,				# GL_KEYS
												Mode 					# Mode check to insert Journal for category
											)

										else:
											# Call method for error message
											error_msg = "Interest collection transaction not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									if float(IntIncomeToRev) > 0:
										# Category 	= LC_Pro.IntIncomeCate.strip()
										if Suspend.upper() == 'Y':
											Category 	= mktparam.getPDParam().SuspendCrCat
											GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
										else:
											Category 	= LC_Pro.IntIncomeCate.strip()
											GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
										
										if not Category:
											# Call method for error message
											error_msg = "Suspend credit or interest income category not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										DateTimeNow = mktdate.getDateTimeNow()
										Transaction = mktsetting.getAccSetting().CrAccrIntTran
										Mode 		= "Direct"
										DrCr 		= "Dr"
										
										if Transaction:
											# GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
											# Credit Interest Income Category
											mktaccounting.postAccounting(
												"AUTH", 				# Status
												"0", 					# Curr
												"system",				# Inputter
												DateTimeNow, 			# Createdon
												"system",				# Authorizer
												DateTimeNow,			# Authorizeon
												"", 					# AEID
												AccountID,				# Account
												Category,				# Category
												Currency,				# Currency
												DrCr,					# DrCr
												Decimal(IntIncomeToRev), # Amount
												"LC",					# Module
												Transaction, 			# Transaction
												TranDate, 				# TransactionDate
												ID, 					# Reference
												"", 					# Note
												"", 					# JNID
												Branch,					# Branch
												GL_KEYS,				# GL_KEYS
												Mode 					# Mode check to insert Journal for category
											)

										else:
											# Call method for error message
											error_msg = "Interest collection transaction not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)
									

									if float(NewIntReceivable) > 0: # In case client has not paid enough interest

										if LC_Pro:
											Category = LC_Pro.IntReceivableCate.strip()
											if not Category:
												# Call method for error message
												error_msg = "Interest receivable category not found."
												TrueOrFalse = mktmessage.msgError(EOD, error_msg)

											DateTimeNow = mktdate.getDateTimeNow()
											Transaction = mktsetting.getAccSetting().DrAccrIntTran
											Mode 		= "Direct"
											DrCr 		= "Dr"

											if Transaction:
												GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
												# Debit Accrued Interest Receivable Category
												mktaccounting.postAccounting(
													"AUTH", 				# Status
													"0", 					# Curr
													"system",				# Inputter
													DateTimeNow, 			# Createdon
													"system",				# Authorizer
													DateTimeNow,			# Authorizeon
													"", 					# AEID
													AccountID,				# Account
													Category,				# Category
													Currency,				# Currency
													DrCr,					# DrCr
													Decimal(NewIntReceivable), 	# Amount
													"LC",					# Module
													Transaction, 			# Transaction
													TranDate, 				# TransactionDate
													ID, 					# Reference
													"", 					# Note
													"", 					# JNID
													Branch,					# Branch
													GL_KEYS,				# GL_KEYS
													Mode 					# Mode check to insert Journal for category
												)

											else:
												# Call method for error message
												error_msg = "Interest collection transaction not found."
												TrueOrFalse = mktmessage.msgError(EOD, error_msg)
										
											# Category = LC_Pro.IntIncomeCate.strip()
											# if not Category:
											# 	# Call method for error message
											# 	error_msg = "Interest income category not found."
											# 	TrueOrFalse = mktmessage.msgError(EOD, error_msg)

											if Suspend.upper() == 'Y':
												Category 	= mktparam.getPDParam().SuspendCrCat
												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
											else:
												Category 	= LC_Pro.IntIncomeCate.strip()
												GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
											
											if not Category:
												# Call method for error message
												error_msg = "Suspend credit or interest income category not found."
												TrueOrFalse = mktmessage.msgError(EOD, error_msg)

											DateTimeNow = mktdate.getDateTimeNow()
											Transaction = mktsetting.getAccSetting().DrAccrIntTran
											Mode 		= "Direct"
											DrCr 		= "Cr"

											if Transaction:
												# GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
												# Credit Interest Income Category
												mktaccounting.postAccounting(
													"AUTH", 				# Status
													"0", 					# Curr
													"system",				# Inputter
													DateTimeNow, 			# Createdon
													"system",				# Authorizer
													DateTimeNow,			# Authorizeon
													"", 					# AEID
													AccountID,				# Account
													Category,				# Category
													Currency,				# Currency
													DrCr,					# DrCr
													Decimal(NewIntReceivable), 		# Amount
													"LC",					# Module
													Transaction, 			# Transaction
													TranDate, 				# TransactionDate
													ID, 					# Reference
													"", 					# Note
													"", 					# JNID
													Branch,					# Branch
													GL_KEYS,				# GL_KEYS
													Mode 					# Mode check to insert Journal for category
												)

											else:
												# Call method for error message
												error_msg = "Accrued interest booking transaction not found."
												TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										else:
											# Call method for error message
											error_msg = "Account product not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

							if p.upper() == 'PR':
								
								if float(Principal) != 0:
									Category = Account.AccCategory
									if AccBal >= Principal :
										Amount = Principal
									else :
										Amount = AccBal
										PD_Principal = float(Principal) - float(AccBal)

									AccBal = float(AccBal) - float(Amount)
									PayPri = float(Amount)
									Transaction = mktsetting.getAccSetting().PrincipalTran

									if Amount != 0:
										DateTimeNow = mktdate.getDateTimeNow()
										DrCr 		= "Dr"
										Mode 		= ""

										if Transaction:
											GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
											# Debit Customer Account
											mktaccounting.postAccounting(
												"AUTH", 				# Status
												"0", 					# Curr
												"system",				# Inputter
												DateTimeNow, 			# Createdon
												"system",				# Authorizer
												DateTimeNow,			# Authorizeon
												"", 					# AEID
												AccountID,				# Account
												Category,				# Category
												Currency,				# Currency
												DrCr,					# DrCr
												Decimal(Amount), 			# Amount
												"LC",					# Module
												Transaction, 			# Transaction
												TranDate, 				# TransactionDate
												ID, 					# Reference
												"", 					# Note
												"", 					# JNID
												Branch,					# Branch
												GL_KEYS,				# GL_KEYS
												Mode 					# Mode check to insert Journal for category
											)

										else:
											# Call method for error message
											error_msg = "Principal collection transaction not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										DateTimeNow = mktdate.getDateTimeNow()
										DrCr 		= "Cr"
										Mode 		= "Direct"
										Category 		= LoanContract.Category

										if Transaction:
											GL_KEYS = mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
											# Cradit LC Category
											mktaccounting.postAccounting(
												"AUTH", 				# Status
												"0", 					# Curr
												"system",				# Inputter
												DateTimeNow, 			# Createdon
												"system",				# Authorizer
												DateTimeNow,			# Authorizeon
												"", 					# AEID
												AccountID,				# Account
												Category,				# Category
												Currency,				# Currency
												DrCr,					# DrCr
												Decimal(Amount), 		# Amount
												"LC",					# Module
												Transaction, 			# Transaction
												TranDate, 				# TransactionDate
												ID, 					# Reference
												"", 					# Note
												"", 					# JNID
												Branch,					# Branch
												GL_KEYS,				# GL_KEYS
												Mode 					# Mode check to insert Journal for category
											)

										else:
											# Call method for error message
											error_msg = "Principal collection transaction not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

						Total_PD 	= float(PD_Principal) + float(PD_Interest) + float(PD_Charge) + float(PD_Penalty)
						Category 	= LoanContract.Category.strip()
						PenaltyType = mktparam.getPDParam().PenType.strip()
						PenaltyRate = mktparam.getPDParam().PenaltyRate.strip()
						PDID = "PD" + str(ID)
						if Total_PD > 0:
							setPD(PDID, Customer, Currency, Category, PenaltyType,
										  PenaltyRate, DueDate, PD_Principal, PD_Interest,
										  PD_Charge, PD_Penalty, NextRunDate, ID, Branch)

						updateAccrCurrInt(ID, Principal)

						# row.NextRunDate = NextRunDate
						# Update repayment schedule status
						PaymentAm 		= float(PayPri) + float(PayInt) + float(PayCharge)
						if float(Total) == float(PaymentAm):
							row.RepStatus 	= '3'
						elif float(PaymentAm) == 0:
							row.RepStatus 	= '1'
						else:
							row.RepStatus 	= '2'

						row.PartPaidAmt 	= float(PaymentAm)
						db.session.add(row)

						# LC_Contract 	=	MKT_LOAN_CONTRACT.query.get(LCID)
						# if LC_Contract:
						# 	LC_Amount 	=	float(LC_Contract.Amount) if LC_Contract.Amount else float(0)
						# 	ODAmount 	=	mktpd.getTotODAmount(PDID)
						# 	LC_Contract.OutstandingAmount = float(LC_Amount) + float(ODAmount)
						# 	db.session.add(LC_Contract)
						# else:
						# 	db.session.rollback()
						# Update Last Outstanding Amount
						setUpdateOutstandingAmount(LCID)
						
						# Commit to DB per loan procceed
						db.session.commit()
						if float(Total) == float(PaymentAm):
							# print "%s Full amount was collected: %s." %(ID, str(PaymentAm))
							print_msg = "%s Full amount was collected: %s." %(ID, str(PaymentAm))
							mktmessage.msgOutputMsg(print_msg)
						elif float(PaymentAm) < float(Total) and float(PaymentAm) > 0:
							# print "%s Partial amount was collected: %s." %(ID, str(PaymentAm))
							print_msg = "%s Partial amount was collected: %s." %(ID, str(PaymentAm))
							mktmessage.msgOutputMsg(print_msg)
						else:
							# print "%s No amount was collected." %ID
							print_msg = "%s No amount was collected." %ID
							mktmessage.msgOutputMsg(print_msg)

						# print "%s Accrued interest current intallment was updated." %ID
						print_msg = "%s Accrued interest current intallment was updated." %ID
						mktmessage.msgOutputMsg(print_msg)

						TrueOrFalse = True

					else:
						# Call method for error message
						error_msg = "Account not found."
						TrueOrFalse = mktmessage.msgError(EOD, error_msg)
				else:
					# print "Loan contract not found."
					print_msg = "Loan contract not found."
					mktmessage.msgOutputMsg(print_msg)
		
		
		# print mktmessage.getTextMsg("000001")
		print_msg = mktmessage.getTextMsg("000001")
		mktmessage.msgOutputMsg(print_msg)
		
		print ""
		if LoanID:
			# Loan Classification
			mktpd.loanClassification(LoanID)
		else:
			# Loan Classification
			mktpd.loanClassification()

		# Clear Query Object
		del SCH_REP

		if EOD == 1:
			return ""
		else:
			return TrueOrFalse

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def moveLoanToWOF(ID, Branch, TranDate, CustomerID, CustomerName, Currency,
				  Disbursed, Amount, TotODAmount, WOFAmount, Account,
				  TotPrincipalDue, TotInterestDue, TotPenaltyDue, TotChargeDue,
				  LC_ValueDate, MaturityDate, Officer):
	try:

		DateTimeNow = mktdate.getDateTimeNow()
		WOF 		= MKT_WRITE_OFF.query.get(ID)
		# Add new record for MKT_LOAN_WRITTEN_OFF
		if not WOF:

			WOF = MKT_WRITE_OFF(
						Branch 		=	Branch,
						Status 		=	'AUTH',
						Curr 		=	'0',
						Inputter 	=	'System',
						Createdon 	=	DateTimeNow,
						Authorizer 	=	'System',
						Authorizeon =	DateTimeNow,
						ID 			=	ID,
						CustomerID 	=	CustomerID,
						CustomerName=	CustomerName,
						Currency 	=	Currency,
						Disbursed 	=	Disbursed,
						Balance 	=	Amount,
						OverdueAmt 	=	TotODAmount,
						WOFAmount 	=	WOFAmount
					)

		db.session.add(WOF)

		WrittenOf = MKT_WRITE_OFF_DE.query.get(ID)
		if not WrittenOf:

			WrittenOf = MKT_WRITE_OFF_DE(
							Branch 			=	Branch,
							Status 			=	'AUTH',
							Curr 			=	'0',
							Inputter 		=	'System',
							Createdon 		=	DateTimeNow,
							Authorizer 		=	'System',
							Authorizeon 	=	DateTimeNow,
							ID 				=	ID,
							CustomerID 		=	CustomerID,
							CustomerName	=	CustomerName,
							AccountNumber	=	Account,
							Currency 		=	Currency,
							LoanDisbursed 	=	Disbursed,
							LoanBalance 	=	Amount,
							TotODAmount 	=	TotODAmount,
							TotPrincipalDue =	TotPrincipalDue,
							TotInterestDue 	=	TotInterestDue,
							TotPenaltyDue 	=	TotPenaltyDue,
							TotChargeDue 	=	TotChargeDue,
							TotWOFAmount 	=	WOFAmount,
							ValueDate 		=	LC_ValueDate,
							MaturityDate 	=	MaturityDate,
							WOFDate 		=	TranDate,
							Officer 		=	Officer
						)

			db.session.add(WrittenOf)

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setLoanWriteOff(ID="", EOD=1, Stat="", Date=""):
	try:

		TrueOrFalse 	= "1"

		if Date:
			SystemBankDate 	= Date
		else:
			SystemBankDate 	= mktdate.getBankDate()

		WOFClass		= mktparam.getPDParam().WOFClass
		
		Loan 		= 	db.session.query(MKT_LOAN_CONTRACT.ID, MKT_LOAN_CONTRACT.Suspend, MKT_LOAN_CONTRACT.ValueDate, MKT_LOAN_CONTRACT.MaturityDate, MKT_LOAN_CONTRACT.LoanProduct, MKT_LOAN_CONTRACT.ContractCustomerID, MKT_LOAN_CONTRACT.ContractOfficerID, MKT_LOAN_CONTRACT.Disbursed, MKT_LOAN_CONTRACT.Amount, MKT_LOAN_CONTRACT.Currency, MKT_LOAN_CONTRACT.AccrInterest, MKT_LOAN_CONTRACT.AssetClass, MKT_LOAN_CONTRACT.Branch, MKT_LOAN_CONTRACT.Category, MKT_LOAN_CONTRACT.Account, MKT_CUSTOMER.LastNameEn, MKT_CUSTOMER.FirstNameEn, MKT_ASSET_CLASS.ProResvCat, MKT_ASSET_CLASS.ProvExpCat).\
						join(MKT_CUSTOMER, MKT_CUSTOMER.ID == MKT_LOAN_CONTRACT.ContractCustomerID).\
						join(MKT_ASSET_CLASS, MKT_ASSET_CLASS.ID == MKT_LOAN_CONTRACT.AssetClass).\
						filter(MKT_LOAN_CONTRACT.AssetClass == WOFClass)

		if ID:
			Loan = Loan.\
				   filter(MKT_LOAN_CONTRACT.ID == ID)

		# Check for count loan write-off for today
		LC_Count = Loan.count()
		if Stat == "1":
			return LC_Count

		FlagID 	= ID
		Loan 	= Loan.all()

		# Check if no record set progress bar done.
		mktbjstat.calCompletedPer('LW', 0, 1, 0)

		if Loan:

			NumberOfCompleted 	= 	0
			RecordNumber 		=	0
			NumOfCompletedPerc 	=	0
			NumOfTransaction 	=	mktbjstat.setNumOfTransaction(LC_Count)

			for row in Loan:

				# Block update BjStat
				RecordNumber 		+= 1
				NumberOfCompleted 	+= 1

				if int(NumberOfCompleted) == int(NumOfTransaction) or int(RecordNumber) == int(LC_Count):
					NumOfCompletedPerc += float(mktbjstat.getPercPerTransaction(NumberOfCompleted, LC_Count))
					mktbjstat.calCompletedPer('LW', NumberOfCompleted, LC_Count, NumOfCompletedPerc)
					NumberOfCompleted = 0
					
				# End Block


				ID 				= 	row.ID
				AssClass 		=	row.AssetClass
				Currency 		= 	row.Currency
				Branch 			= 	row.Branch
				Category 		= 	row.Category
				Suspend 		=	row.Suspend
				Disbursed 		=	Decimal(row.Disbursed) if row.Disbursed else Decimal(0)
				Amount 			= 	Decimal(row.Amount) if row.Amount else Decimal(0)
				Account 		= 	row.Account
				AccrInterest 	=	Decimal(row.AccrInterest) if row.AccrInterest else Decimal(0)
				CustomerID 		=	row.ContractCustomerID
				CustomerName 	=	row.LastNameEn + " " + row.FirstNameEn
				LoanProduct 	=	row.LoanProduct
				LC_ValueDate 	=	row.ValueDate
				MaturityDate 	=	row.MaturityDate
				Officer 		=	row.ContractOfficerID
				Product 		=	MKT_LOAN_PRODUCT.query.get(LoanProduct)
				TotPrincipalDue =	Decimal(0)
				TotODAmount 	=	Decimal(0)
				TotChargeDue 	=	Decimal(0)
				TotPenaltyDue	=	Decimal(0)
				PastDueID 		=	"PD" + ID
				PD 				=	MKT_PAST_DUE.query.get(PastDueID)
				if PD:
					TotPrincipalDue	=	Decimal(PD.TotPrincipalDue) if PD.TotPrincipalDue else Decimal(0)
					TotODAmount 	=	Decimal(PD.TotODAmount) if PD.TotODAmount else Decimal(0)
					TotChargeDue 	=	Decimal(PD.TotChargeDue) if PD.TotChargeDue else Decimal(0)
					TotPenaltyDue 	=	Decimal(PD.TotPenaltyDue) if PD.TotPenaltyDue else Decimal(0)
					TotInterestDue 	=	Decimal(PD.TotInterestDue) if PD.TotInterestDue else Decimal(0)

				Tran 		= 	mktsetting.getAccSetting()
				Transaction = 	""
				ChargeTran 	= 	""
				TranDate 	= 	SystemBankDate
				WOFAmount 	=	Decimal(Amount) + Decimal(TotODAmount)

				ValueDate 	=	SystemBankDate

				if not Tran:
					# Call method for error message
					error_msg = "Please mktsetting up transaction type for loan disbursement before submit the record."
					TrueOrFalse = mktmessage.msgError(EOD, error_msg)

				else:

					k1 = Tran.GL_KEY1
					k2 = Tran.GL_KEY2
					k3 = Tran.GL_KEY3
					k4 = Tran.GL_KEY4
					k5 = Tran.GL_KEY5
					k6 = Tran.GL_KEY6
					k7 = Tran.GL_KEY7
					k8 = Tran.GL_KEY8
					k9 = Tran.GL_KEY9

					k1 = mktkey.getResultKey(k1, ID, CustomerID, "") if k1 else ""
					k2 = mktkey.getResultKey(k2, ID, CustomerID, "") if k2 else ""
					k3 = mktkey.getResultKey(k3, ID, CustomerID, "") if k3 else ""
					k4 = mktkey.getResultKey(k4, ID, CustomerID, "") if k4 else ""
					k5 = mktkey.getResultKey(k5, ID, CustomerID, "") if k5 else ""
					k6 = mktkey.getResultKey(k6, ID, CustomerID, "") if k6 else ""
					k7 = mktkey.getResultKey(k7, ID, CustomerID, "") if k7 else ""
					k8 = mktkey.getResultKey(k8, ID, CustomerID, "") if k8 else ""
					k9 = mktkey.getResultKey(k9, ID, CustomerID, "") if k9 else ""

					Transaction = mktsetting.getAccSetting().WOFTran
					if not Transaction:
						# Call method for error message
						error_msg = "Loan write-off transaction not found."
						TrueOrFalse = mktmessage.msgError(EOD, error_msg)
					else:
						if not Product:
							# Call method for error message
							error_msg = "Loan product not found."
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)
						else:

							Param = mktparam.getPDParam().RepOrder
							Param = Param.split()
							for p in Param:

								if p.upper() == "PE":
					
									DrCr  		= 'Dr'
									Category 	= mktparam.getPDParam().ODPenaltyCat
									if not Category:
										# Call method for error message
										error_msg = "Penalty income category not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									if float(TotPenaltyDue) > 0:
										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
										DateTimeNow = mktdate.getDateTimeNow()
										Mode 		= "Direct"
										# Debit Penalty Income Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(TotPenaltyDue), 		# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

										DateTimeNow = mktdate.getDateTimeNow()
										DrCr 		= "Cr"
										Mode 		= "Direct"
										Category 	= mktparam.getPDParam().PenaltyRecCat
										if not Category:
											# Call method for error message
											error_msg = "Penalty receivable category not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										# Credit Penalty Accr Receivable Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(TotPenaltyDue), 		# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

								elif p.upper() == "CH":

									if float(TotChargeDue) > 0:

										DrCr  		= 'Dr'
										Category 	= mktparam.getPDParam().ChargeIncCat
										if not Category:
											# Call method for error message
											error_msg = "Income from charge category not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
										DateTimeNow = mktdate.getDateTimeNow()
										Mode 		= "Direct"
										# Debit Customer Account
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(TotChargeDue), 		# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

										DateTimeNow = mktdate.getDateTimeNow()
										Category 	= mktparam.getPDParam().ChargeRecCat
										if not Category:
											# Call method for error message
											error_msg = "Accr charge receivable category not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "PD", "", "", "", "", "", "", "", "", "")
										DrCr 	= "Cr"
										Mode 	= "Direct"

										# Credit Charge Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(TotChargeDue), 		# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

								elif p.upper() == "IN":

									if Suspend.upper() == 'Y':
										Category 	= mktparam.getPDParam().SuspendCrCat
										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
									else:
										Category 	= Product.IntIncomeCate.strip()
										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)

									if not Category:
										# Call method for error message
										error_msg = "Interest income or suspend account category not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									if float(AccrInterest) > 0:
										DateTimeNow = mktdate.getDateTimeNow()
										DrCr 		= "Dr"
										Mode 		= "Direct"
										# Debit Interest Income Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(AccrInterest), 	# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

										Category 	= Product.IntReceivableCate.strip()
										if not Category:
											# Call method for error message
											error_msg = "Accreal interest receivable category not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
										DateTimeNow = mktdate.getDateTimeNow()
										DrCr 		= "Cr"
										Mode 		= "Direct"
										# Credit Accrued Interest Receivable Category
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"system",				# Inputter
											DateTimeNow, 			# Createdon
											"system",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(AccrInterest), 	# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

								else:

									LoanAmmount = float(Amount) + float(TotPrincipalDue)
									DrCr 		= "Dr"
									Mode 		= "Direct"
									Category 	= row.ProResvCat
									if not Category:
										# Call method for error message
										error_msg = "Loan loss reserved category not found."
										TrueOrFalse = mktmessage.msgError(EOD, error_msg)

									DateTimeNow = mktdate.getDateTimeNow()
									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
									
									if float(LoanAmmount) > 0:
										# Debit Loan Loss Reserved
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"System",				# Inputter
											DateTimeNow, 			# Createdon
											"System",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(LoanAmmount), 	# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

										DrCr 		= "Cr"
										Category 	= row.Category
										if not Category:
											# Call method for error message
											error_msg = "Loan outstanding category not found."
											TrueOrFalse = mktmessage.msgError(EOD, error_msg)

										DateTimeNow = mktdate.getDateTimeNow()
										GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, AssClass, "LC", k1, k2, k3, k4, k5, k6, k7, k8, k9)
										# Credit Loan Outstanding
										mktaccounting.postAccounting(
											"AUTH", 				# Status
											"0", 					# Curr
											"System",				# Inputter
											DateTimeNow, 			# Createdon
											"System",				# Authorizer
											DateTimeNow,			# Authorizeon
											"", 					# AEID
											Account,				# Account
											Category,				# Category
											Currency,				# Currency
											DrCr,					# DrCr
											Decimal(LoanAmmount), 	# Amount
											"LC",					# Module
											Transaction, 			# Transaction
											TranDate, 				# TransactionDate
											ID, 					# Reference
											"", 					# Note
											"", 					# JNID
											Branch,					# Branch
											GL_KEYS,				# GL_KEYS
											Mode 					# Mode check to insert Journal for category
										)

							# WOF = MKT_WRITE_OFF.query.get(ID)
							# # Add new record for MKT_LOAN_WRITTEN_OFF
							# if not WOF:

							# 	WOF = MKT_WRITE_OFF(
							# 				Branch 		=	Branch,
							# 				Status 		=	'AUTH',
							# 				Curr 		=	'0',
							# 				Inputter 	=	'System',
							# 				Createdon 	=	TranDate,
							# 				Authorizer 	=	'System',
							# 				Authorizeon =	TranDate,
							# 				ID 			=	ID,
							# 				CustomerID 	=	CustomerID,
							# 				CustomerName=	CustomerName,
							# 				Currency 	=	Currency,
							# 				Disbursed 	=	Disbursed,
							# 				Balance 	=	Amount,
							# 				OverdueAmt 	=	TotODAmount,
							# 				WOFAmount 	=	WOFAmount
							# 			)

							# db.session.add(WOF)

							# WrittenOf = MKT_WRITE_OFF_DE.query.get(ID)
							# if not WrittenOf:

							# 	WrittenOf = MKT_WRITE_OFF_DE(
							# 					Branch 			=	Branch,
							# 					Status 			=	'AUTH',
							# 					Curr 			=	'0',
							# 					Inputter 		=	'System',
							# 					Createdon 		=	TranDate,
							# 					Authorizer 		=	'System',
							# 					Authorizeon 	=	TranDate,
							# 					ID 				=	ID,
							# 					CustomerID 		=	CustomerID,
							# 					CustomerName	=	CustomerName,
							# 					AccountNumber	=	Account,
							# 					Currency 		=	Currency,
							# 					LoanDisbursed 	=	Disbursed,
							# 					LoanBalance 	=	Amount,
							# 					TotODAmount 	=	TotODAmount,
							# 					TotPrincipalDue =	TotPrincipalDue,
							# 					TotInterestDue 	=	TotInterestDue,
							# 					TotPenaltyDue 	=	TotPenaltyDue,
							# 					TotChargeDue 	=	TotChargeDue,
							# 					TotWOFAmount 	=	WOFAmount,
							# 					ValueDate 		=	LC_ValueDate,
							# 					MaturityDate 	=	MaturityDate,
							# 					WOFDate 		=	TranDate,
							# 					Officer 		=	Officer
							# 				)

							# 	db.session.add(WrittenOf)
							moveLoanToWOF(ID, Branch, TranDate, CustomerID, CustomerName, Currency,
										  Disbursed, Amount, TotODAmount, WOFAmount, Account,
										  TotPrincipalDue, TotInterestDue, TotPenaltyDue, TotChargeDue,
										  LC_ValueDate, MaturityDate, Officer)

							# Move and delete loan contract to HIST file
							mktaudit.moveAUTHtoHIST(MKT_LOAN_CONTRACT, MKT_LOAN_CONTRACT_HIST, ID)
							mktaudit.deleteAUTH(MKT_LOAN_CONTRACT, ID)
							# Move and delete schedule define to HIST file
							mktaudit.moveAUTHtoHIST(MKT_SCHED_DEFINE, MKT_SCHED_DEFINE_HIST, ID)
							mktaudit.deleteAUTH(MKT_SCHED_DEFINE, ID)
							# Move and delete repayment schedule to HIST file
							mktaudit.moveAUTHtoHIST(MKT_REP_SCHEDULE, MKT_REP_SCHEDULE_HIST, ID, "LoanID")
							mktaudit.deleteAUTH(MKT_REP_SCHEDULE, ID, "LoanID")
							# Delete past due in ID
							mktaudit.deleteAUTH(MKT_PAST_DUE, PastDueID)
							# Move and delete pd date to HIST file
							mktaudit.moveAUTHtoHIST(MKT_PD_DATE, MKT_PD_DATE_HIST, PastDueID)
							mktaudit.deleteAUTH(MKT_PD_DATE, PastDueID)
							# print "%s was written-off successfully." %ID
							print_msg = "%s was written-off successfully." %ID
							mktmessage.msgOutputMsg(print_msg)

		else:
			# print "No loan for write-off today."
			print_msg = "No loan for write-off today."
			mktmessage.msgOutputMsg(print_msg)

		return ""

	except:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise

def setLoanRecovery(LoanID = "", RecID="", EOD=1):
	try:

		TrueOrFalse 	= "1"
		SystemBankDate	= 	mktdate.getBankDate()
		Recovery 		= 	db.session.query(MKT_WRITE_OFF_DE.ID, MKT_WRITE_OFF_DE.TotODAmount, MKT_WRITE_OFF_DE.TotWOFAmount, MKT_WRITE_OFF_DE.LoanBalance, MKT_WRITE_OFF_DE.Branch, MKT_WRITE_OFF_DE.Currency, MKT_ACCOUNT.AvailableBal, MKT_WRITE_OFF_DE.TotPrincipalDue, MKT_WRITE_OFF_DE.TotInterestDue, MKT_WRITE_OFF_DE.TotChargeDue, MKT_WRITE_OFF_DE.TotPenaltyDue, MKT_WRITE_OFF_DE.AccountNumber, MKT_ACCOUNT.AccCategory).\
							order_by(MKT_WRITE_OFF_DE.Authorizeon.asc()).\
							join(MKT_ACCOUNT, MKT_ACCOUNT.ID == MKT_WRITE_OFF_DE.AccountNumber).\
							filter(MKT_ACCOUNT.AvailableBal > 0)

		if LoanID:
			Recovery 	= 	Recovery.\
							filter(MKT_WRITE_OFF_DE.ID == LoanID)

		Recovery 		= 	Recovery.\
							all()

		if Recovery:
			for row in Recovery:

				RecoveryID 		= 	mktAutoID.setAutoID("SLR", 4, "MKT_LOAN_RECOVERY")
				RefID 			=	RecID
				if not RecID:
					RefID 		= 	RecoveryID
				
				ID 					=	row.ID
				Branch 				=	row.Branch
				AccCategory 		=	row.AccCategory
				Currency 			=	row.Currency
				Account 			= 	row.AccountNumber

				AccountBalance 		=	float(row.AvailableBal) if row.AvailableBal else float(0)
				Principal 			=	float(row.LoanBalance) if row.LoanBalance else float(0)
				TotPrincipalDue 	= 	float(row.TotPrincipalDue) if row.TotPrincipalDue else float(0)
				TotInterestDue 		=	float(row.TotInterestDue) if row.TotInterestDue else float(0)
				TotChargeDue 		=	float(row.TotChargeDue) if row.TotChargeDue else float(0)
				TotPenaltyDue 		=	float(row.TotPenaltyDue) if row.TotPenaltyDue else float(0)
				TotWOFAmount 		=	float(row.TotWOFAmount)

				RecoveryAmt 		=	float(0)

				PrincipalPaid 		=	0
				ODPrincipalPaid 	=	0
				ODInterestPaid 		=	0
				ODPenaltyPaid 		=	0
				ODChargePaid		=	0

				if float(AccountBalance) >= float(TotWOFAmount):
					AccountBalance = float(TotWOFAmount)

				Param = mktparam.getPDParam().FWOFRepOrder
				Param = Param.split()
				for p in Param:
					if p.upper() == "PR":

						if float(RecoveryAmt) < float(AccountBalance):
					
							DiffAmount = float(AccountBalance) - float(RecoveryAmt)

							if float(DiffAmount) >= float(TotPrincipalDue):
								ODPrincipalPaid = float(TotPrincipalDue)
							else:
								ODPrincipalPaid = float(DiffAmount)

							RecoveryAmt = float(RecoveryAmt) + float(ODPrincipalPaid)

						if float(RecoveryAmt) >= float(AccountBalance):
							break

					elif p.upper() == "IN":

						if float(RecoveryAmt) < float(AccountBalance):
					
							DiffAmount = float(AccountBalance) - float(RecoveryAmt)

							if float(DiffAmount) >= float(TotInterestDue):
								ODInterestPaid = float(TotInterestDue)
							else:
								ODInterestPaid = float(DiffAmount)

							RecoveryAmt = float(RecoveryAmt) + float(ODInterestPaid)

						if float(RecoveryAmt) >= float(AccountBalance):
							break

					elif p.upper() == "CH":

						if float(RecoveryAmt) < float(AccountBalance):
					
							DiffAmount = float(AccountBalance) - float(RecoveryAmt)

							if float(DiffAmount) >= float(TotChargeDue):
								ODChargePaid = float(TotChargeDue)
							else:
								ODChargePaid = float(DiffAmount)

							RecoveryAmt = float(RecoveryAmt) + float(ODChargePaid)

						if float(RecoveryAmt) >= float(AccountBalance):
							break

					else:

						if float(RecoveryAmt) < float(AccountBalance):
					
							DiffAmount = float(AccountBalance) - float(RecoveryAmt)

							if float(DiffAmount) >= float(TotPenaltyDue):
								ODPenaltyPaid = float(TotPenaltyDue)
							else:
								ODPenaltyPaid = float(DiffAmount)

							RecoveryAmt = float(RecoveryAmt) + float(ODPenaltyPaid)

						if float(RecoveryAmt) >= float(AccountBalance):
							break

				if float(RecoveryAmt) < float(AccountBalance):
					
					DiffAmount = float(AccountBalance) - float(RecoveryAmt)

					if float(DiffAmount) >= float(Principal):
						PrincipalPaid = float(Principal)
					else:
						PrincipalPaid = float(DiffAmount)

					RecoveryAmt = float(RecoveryAmt) + float(PrincipalPaid)

				RecoveryCat = mktparam.getPDParam().RecoveryCat

				if not RecoveryCat:
					# Call method for error message
					error_msg = "Loan recovery category not found."
					TrueOrFalse = mktmessage.msgError(EOD, error_msg)
				else:

					Transaction = mktmktsetting.getAccSetting().RecoveryTran
					if not Transaction:
						# Call method for error message
						error_msg = "Loan recovery transaction not found."
						TrueOrFalse = mktmessage.msgError(EOD, error_msg)
					else:
						TranDate 	= SystemBankDate
						DrCr 		= "Dr"
						Mode 		= ""
						Category 	= AccCategory
						DateTimeNow = mktdate.getDateTimeNow()
						GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
						# Debit Customer Account
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"System",				# Inputter
							DateTimeNow, 			# Createdon
							"System",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							Account,				# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(RecoveryAmt), 	# Amount
							"LR",					# Module
							Transaction, 			# Transaction
							TranDate, 				# TransactionDate
							RefID, 					# Reference
							"", 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode 					# Mode check to insert Journal for category
						)

						DrCr 		= "Cr"
						Category 	= RecoveryCat
						if not Category:
							# Call method for error message
							error_msg = "Loan recovery category not found."
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)

						DateTimeNow = mktdate.getDateTimeNow()
						Mode 		= "Direct"
						# Credit Income from Recovery Loan
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"System",				# Inputter
							DateTimeNow, 			# Createdon
							"System",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							Account,				# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(RecoveryAmt), 	# Amount
							"LR",					# Module
							Transaction, 			# Transaction
							TranDate, 				# TransactionDate
							RefID, 					# Reference
							"", 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode 					# Mode check to insert Journal for category
						)
						
						Update 			= 	MKT_WRITE_OFF_DE.query.get(ID)
						TotWOFAmount 	=	0
						if Update:
							TotODAmount 			= 	float(ODPrincipalPaid) + float(ODInterestPaid) + float(ODPenaltyPaid) + float(ODChargePaid)
							Update.LoanBalance 		= 	float(Update.LoanBalance) 		- float(PrincipalPaid)
							Update.TotPrincipalDue 	=	float(Update.TotPrincipalDue) 	- float(ODPrincipalPaid)
							Update.TotInterestDue 	=	float(Update.TotInterestDue) 	- float(ODInterestPaid)
							Update.TotPenaltyDue 	=	float(Update.TotPenaltyDue) 	- float(ODPenaltyPaid)
							Update.TotChargeDue 	=	float(Update.TotChargeDue)		- float(ODChargePaid)
							Update.TotODAmount 		=	float(Update.TotODAmount) 		- float(TotODAmount)
							Update.TotWOFAmount 	=	float(Update.TotWOFAmount) 		- float(RecoveryAmt)
							TotWOFAmount 			= 	float(Update.TotWOFAmount) if Update.TotWOFAmount else float(0)
							db.session.add(Update)
						
						if float(AccountBalance) > float(RecoveryAmt):
							AccBal 	= float(RecoveryAmt)
						else:
							AccBal 	= float(AccountBalance)

						if not RecID:
							AddRecovery = MKT_LOAN_RECOVERY(
												Branch 			=	Branch,
												Status 			=	'AUTH',
												Curr 			=	'0',
												Inputter 		=	'System',
												Createdon 		=	TranDate,
												Authorizer 		=	'System',
												Authorizeon 	=	TranDate,
												ID 				=	RecoveryID,
												LoanID 			=	ID,
												AccountID 		=	Account,
												Currency 		=	Currency,
												AccBalance 		=	AccBal,
												RecoveryAmt 	=	RecoveryAmt
											)

							db.session.add(AddRecovery)

						if TotWOFAmount == 0:
							mktaudit.moveAUTHtoHIST(MKT_WRITE_OFF_DE, MKT_WRITE_OFF_DE_HIST, ID)
							mktaudit.deleteAUTH(MKT_WRITE_OFF_DE, ID)

						if float(AccountBalance) >= float(RecoveryAmt):
							# print "%s Full amount was recovered: %s." %(ID, mktmoney.toMoney(float(RecoveryAmt), mktmoney.getCurrencyObj(Currency)))
							print_msg = "%s Full amount was recovered: %s." %(ID, mktmoney.toMoney(float(RecoveryAmt), mktmoney.getCurrencyObj(Currency)))
							mktmessage.msgOutputMsg(print_msg)
						elif float(AccountBalance) < float(RecoveryAmt) and float(AccountBalance) > 0:
							# print "%s Partial amount was recovered: %s." %(ID, mktmoney.toMoney(float(RecoveryAmt), mktmoney.getCurrencyObj(Currency)))
							print_msg = "%s Partial amount was recovered: %s." %(ID, mktmoney.toMoney(float(RecoveryAmt), mktmoney.getCurrencyObj(Currency)))
							mktmessage.msgOutputMsg(print_msg)
						else:
							# print "%s No amount was recovered." %ID
							print_msg = "%s No amount was recovered." %ID
							mktmessage.msgOutputMsg(print_msg)
		
		else:
			# print "No loan write-off for recovery today."
			print_msg = "No loan write-off for recovery today."
			mktmessage.msgOutputMsg(print_msg)

		return ""

	except:
		raise