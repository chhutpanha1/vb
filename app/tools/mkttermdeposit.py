from app.mktcore.imports 			import *
from .. 							import app, db
from decimal 						import *
from sqlalchemy 					import *

import mktmoney 					as mktmoney
import mktsetting 					as mktsetting
import mktaccounting 				as mktaccounting
import mktaudit 					as mktaudit
import mktdate	 					as mktdate
import mktbjstat 					as mktbjstat
import loantools.nonworkingday 		as mktDay
import loantools.rescheduletools 	as mktreschedule
import mktholiday 					as mktHoliday
import mktmessage 					as mktmessage
import mktloan 						as mktloan

def checkAmount(Account, Amount):
	try:

		Msg = [False, ""]
		AccountObj = MKT_ACCOUNT.query.get(Account)

		if not AccountObj:
			Msg = [False, "Account %s not found." %Account]

		else:

			AccBal = float(AccountObj.Balance) if AccountObj.Balance else float(0)
			Currency = AccountObj.Currency

			if float(Amount) > float(AccBal):
				Message = "Account#%s doesn't have sufficient fund. Current balance is %s." %(Account, mktmoney.toMoney(float(AccBal), mktmoney.getCurrencyObj(Currency)))
				Msg = [False, Message]

			else:
				Msg = [True, ""]

		return Msg

	except Exception, e:
		return False," %s"%e

def getRuleDetailObj(AccProduct, Currency):
	try:
		# print AccProduct
		RoleObj = ""
		
		ProductObj 	= MKT_TERM_DEPOSIT_PRODUCT.query.get(AccProduct)

		if ProductObj:

			TARole 		= ProductObj.TARole
			RoleDetail 	= '%s%s' %(TARole, Currency)
			RoleObj 	= MKT_LOAN_RULE_DE.query.get(RoleDetail)

		return RoleObj

	except Exception, e:
		return False," %s"%e

def checkRuleAmountDeposit(AccProduct, Currency, Amount):
	try:

		Msg = [True, ""]

		RoleObj = getRuleDetailObj(AccProduct, Currency)

		if RoleObj:

			MinAmount 	= 	float(RoleObj.MinAmount) if RoleObj.MinAmount else float(0)
			MaxAmount 	= 	float(RoleObj.MaxAmount) if RoleObj.MaxAmount else float(0)

			if not float(MinAmount) <= float(Amount) <= float(MaxAmount):
				Message = "The amount must be bewteen %s to %s." %(mktmoney.toMoney(float(MinAmount), mktmoney.getCurrencyObj(Currency)), mktmoney.toMoney(float(MaxAmount), mktmoney.getCurrencyObj(Currency)))
				Msg = [False, Message]
			else:
				Msg = [True, ""]

		return Msg

	except Exception, e:
		return False," %s"%e

def checkRuleTermDeposit(AccProduct, Currency, Term):
	try:

		Msg = [True, ""]

		RoleObj = getRuleDetailObj(AccProduct, Currency)

		if RoleObj:

			MinTerm 	=	RoleObj.MinTerm
			MaxTerm 	=	RoleObj.MaxTerm

			if not int(MinTerm) <= int(Term) <= int(MaxTerm):
				Message = "The term must be bewteen %s to %s." %(MinTerm, MaxTerm)
				Msg = [False, Message]
			else:
				Msg = [True, ""]

		return Msg

	except Exception, e:
		return False," %s"%e

def setTABooking(Branch, DrAcc, DrCat, CrAcc, CrCat, Currency, Amount, Tran, Module, Ref, Note="", DrMode="", CrMode="", DrYes="", CrYes="", Inputter="", Createdon="", Authorizer="", Authorizeon=""):
	try:

		TranDate 		=	str(mktdate.getBankDate())

		for i in range(0, 2):

		
			if i == 0:
				Account 	= DrAcc
				Category	= DrCat
				DrCr 		= "Dr"
				Mode 		= DrMode
				Check 		= DrYes
			else:
				Account		= CrAcc
				Category	= CrCat
				DrCr 		= "Cr"
				Mode 		= CrMode
				Check 		= CrYes


			#get Gl key	
			GL_KEYS = mktaccounting.getConsolKey(Category,Currency, "" ,"")
				
			mktaccounting.postAccounting(		

			"AUTH", 				# Status
			"0", 					# Curr
			Inputter,				# Inputter
			Createdon, 				# Createdon
			Authorizer,				# Authorizer
			Authorizeon,			# Authorizeon
			"", 					# AEID
			Account,				# Account
			Category,				# Category
			Currency,				# Currency
			DrCr,					# DrCr
			Decimal(Amount), 		# Amount
			Module,					# Module
			Tran, 					# Transaction
			TranDate, 				# TransactionDate
			Ref, 					# Reference
			Note, 					# Note
			"", 					# JNID
			Branch,					# Branch
			GL_KEYS,				# ConsolKey
			Mode,					# Mode
			Check)					# Tell function to update LastTransaction Customer's Account

		return [True, ""]

	except Exception, e:
		db.session.rollback()
		# raise
		return False," %s"%e

def getTotalInterestTD(Term, InterestDayBasis, InterestKey, ValueDate, MaturityDate, Amount):
	try:

		SystemBankDate 	=	mktdate.getBankDate()
		RatePerYear 	=	getPeriodicInterest(InterestKey, Term)
		# IntPerDay 		=	mktreschedule.getInterestPerDay(float(RatePerYear), Amount, int(InterestDayBasis), str(SystemBankDate))
		NumOfDay 		=	mktreschedule.getNumberOfDay(InterestDayBasis, ValueDate, MaturityDate)
		IntPerDay 		=	mktreschedule.getInterestPerDay(float(RatePerYear), float(Amount), int(InterestDayBasis), str(SystemBankDate))
		TotalInterest 	=	float(IntPerDay) * float(NumOfDay)

		return [IntPerDay, TotalInterest]

	except Exception, e:
		db.session.rollback()
		# raise
		return False," %s"%e

def setDailyAccrOfTA(ID="", EOD=1, Stat=""):
	try:
		dateObj 		=	mktsetting.getBankDateObj()
		SystemBankDate 	= 	mktdate.getBankDate()
		NextSystemDate 	= 	dateObj.NextSystemDate
		Holiday 		= 	mktHoliday.getHoliday()
		DayEndOfMonth 	= 	dateObj.NextMonthEnd
		TASetting 		=	mktsetting.getTASetting()
		Setting 		=	mktsetting.getAccSetting()

		RecordObj 		= 	db.session.query(
								MKT_TERM_DEPOSIT_CONTRACT.ID,
								MKT_TERM_DEPOSIT_CONTRACT.Amount,
								MKT_TERM_DEPOSIT_CONTRACT.Account,
								MKT_TERM_DEPOSIT_CONTRACT.AccountProduct,
								MKT_TERM_DEPOSIT_CONTRACT.Term,
								MKT_TERM_DEPOSIT_CONTRACT.Branch,
								MKT_TERM_DEPOSIT_CONTRACT.Currency,
								MKT_TERM_DEPOSIT_CONTRACT.AIPCurrent,
								MKT_TERM_DEPOSIT_CONTRACT.AIPBooked,
								MKT_TERM_DEPOSIT_CONTRACT.IntPaid,
								MKT_TERM_DEPOSIT_CONTRACT.MaturityDate,
								MKT_TERM_DEPOSIT_CONTRACT.RollOver,
								MKT_TERM_DEPOSIT_CONTRACT.RollOverOption,
								MKT_TERM_DEPOSIT_PRODUCT.InterestKey,
								MKT_TERM_DEPOSIT_PRODUCT.IntPostingFreq,
								MKT_TERM_DEPOSIT_PRODUCT.InterestDayBasis,
								MKT_TERM_DEPOSIT_PRODUCT.AIPCat,
								MKT_TERM_DEPOSIT_PRODUCT.IECat,
								MKT_TERM_DEPOSIT_PRODUCT.Tax
							).\
							join(
								MKT_TERM_DEPOSIT_PRODUCT,
								MKT_TERM_DEPOSIT_PRODUCT.ID == MKT_TERM_DEPOSIT_CONTRACT.AccountProduct
							).\
							filter(MKT_TERM_DEPOSIT_CONTRACT.NextRunDate == str(SystemBankDate)).\
							filter(MKT_TERM_DEPOSIT_CONTRACT.MaturityDate >= str(SystemBankDate))

		if ID:

			RecordObj 	=	RecordObj.\
							filter(MKT_TERM_DEPOSIT_CONTRACT.ID == ID)

		CountObj 		=	RecordObj.count()
		if Stat == "1":
			return CountObj

		# Check if no record set progress bar done.
		mktbjstat.calCompletedPer('TA', 0, 1, 0)

		if RecordObj:

			NumberOfCompleted 	= 	0
			RecordNumber 		=	0
			NumOfCompletedPerc 	=	0
			NumOfTransaction 	=	mktbjstat.setNumOfTransaction(CountObj)

			for item in RecordObj:

				TAID 				=	item.ID
				IntPostingFreq 		=	item.IntPostingFreq
				Branch 				=	item.Branch
				Currency 			=	item.Currency
				AIPCat 				=	item.AIPCat
				IECat 				=	item.IECat
				Account 			=	item.Account
				Tax 				=	item.Tax
				RollOver 			=	item.RollOver
				RollOverOption 		=	item.RollOverOption
				MaturityDate 		=	item.MaturityDate
				Amount 				=	float(item.Amount) if item.Amount else float(0)
				AIPCurrent 			=	float(item.AIPCurrent) if item.AIPCurrent else float(0)
				AIPBooked 			=	float(item.AIPBooked) if item.AIPBooked else float(0)
				IntPaid 			=	float(item.IntPaid) if item.IntPaid else float(0)
				InterestDayBasis 	=	item.InterestDayBasis
				Term 				=	item.Term
				InterestKey 		=	item.InterestKey
				RatePerYear 		=	getPeriodicInterest(InterestKey, Term)

				IntPerDay 	= 	mktreschedule.getInterestPerDay(float(RatePerYear), Amount, int(InterestDayBasis), str(SystemBankDate))
				# print "Amount %s" %Amount
				# print "InterestDayBasis %s" %InterestDayBasis
				# print "RatePerYear %s" %RatePerYear
				# print "SystemBankDate %s" %SystemBankDate
				# print "IntPerDay %s" %IntPerDay
				check 		= 	True
				one_day 	= 	timedelta(days=1) # one day
				StartDate 	= 	datetime.strptime(str(SystemBankDate),'%Y-%m-%d').date()
				HolidayList	= 	[]

				while check:
					check = mktDay.isNonWorkingDay(StartDate, Holiday)
					if check:
						Item = StartDate
						Item = datetime.strptime(str(Item),'%Y-%m-%d')
						Item = str(Item).split()
						HolidayList.append(str(Item[0]))
						StartDate = StartDate + one_day

				if len(HolidayList) > 0:
					msg_output = "Holiday: %s." %HolidayList
					mktmessage.msgOutputMsg(msg_output)

					if MaturityDate in HolidayList:
						NextSystemDate = MaturityDate
					else:
						onHoliday = mktloan.checkHolidayForEndOfMonth(HolidayList, DayEndOfMonth)
						if onHoliday:
							NextSystemDate = onHoliday

				NumOfDay = "0"
				if NextSystemDate:
					NumOfDay 	= 	mktreschedule.getNumberOfDay(InterestDayBasis, str(SystemBankDate), NextSystemDate)

				if int(NumOfDay) > 0:
					IntPerDay 		= 	float(IntPerDay) * float(NumOfDay)
					IntOfTheDay 	= 	float(IntPerDay) / float(NumOfDay)
				else:
					IntOfTheDay 	= 	float(IntPerDay)

				CheckDate 			= 	mktdate.getPeriodic(IntPostingFreq)
				CurrentDate 		= 	str(SystemBankDate)
				CurrentMonthEnd 	=	mktdate.getCurrentMonthEnd(CurrentDate)
				# print "CheckDate %s" %CheckDate
				# print "CurrentDate %s" %CurrentDate
				# print "CurrentMonthEnd %s" %CurrentMonthEnd
				# print "MaturityDate %s" %MaturityDate
				# print "AIPCurrent %s" %AIPCurrent
				# print "IntPerDay %s" %IntPerDay
				# print "IntOfTheDay %s" %IntOfTheDay
				# print "IntPaid %s" %IntPaid
				# Define Category Customer Account
				AccObj 	=	MKT_ACCOUNT.query.get(Account)
				if not AccObj:
					flash(msg_error + "Account %s not found." %Account)
					db.session.rollback()

				AccCategory = AccObj.AccCategory

				# Check if end month - Step 5
				if CurrentDate == CurrentMonthEnd:
					Tran 		= TASetting.AIPBookingTran
					DateTimeNow = mktdate.getDateTimeNow()
					# Dr/IE Cr/AIP
					setTABooking(
						Branch,			# Branch
						"", 			# Debit Account
						IECat, 			# Debit Category
						"", 			# Credit Account
						AIPCat, 		# Credit Category
						Currency, 		# Currency
						AIPCurrent, 	# Amount
						Tran, 			# Transaction
						"TD", 			# Module
						TAID, 			# Reference
						"", 			# Note
						"Direct", 		# Debit Mode
						"Direct", 		# Credit Mode
						"", 			# Debit "YES"
						"", 			# Crdit "YES"
						"System", 		# Inputter
						DateTimeNow, 	# Createdon
						"System", 		# Authorizer
						DateTimeNow 	# Authorizedon
					)

					UpdateEndMonth 	=	MKT_TERM_DEPOSIT_CONTRACT.query.get(TAID)
					
					if UpdateEndMonth:
						UpdateEndMonth.AIPBooked 	=	float(UpdateEndMonth.AIPBooked) + float(AIPCurrent)
						UpdateEndMonth.AIPCurrent 	=	0

						AIPBooked = float(UpdateEndMonth.AIPBooked) if UpdateEndMonth.AIPBooked else float(AIPBooked)

						db.session.add(UpdateEndMonth)

					# print "AIPBooked %s" %AIPBooked
					AIPCurrent = float(0)

				# Check if accr booking to customer account - Step 6
				if CurrentDate == CheckDate:
					Tran 		= TASetting.IEBookingTran
					DateTimeNow = mktdate.getDateTimeNow()

					CrCat = AccCategory
					# Dr/AIP Cr/Account Customer
					setTABooking(
						Branch,			# Branch
						"", 			# Debit Account
						AIPCat, 		# Debit Category
						Account, 		# Credit Account
						CrCat, 			# Credit Category
						Currency, 		# Currency
						AIPBooked, 		# Amount
						Tran, 			# Transaction
						"TD", 			# Module
						TAID, 			# Reference
						"", 			# Note
						"Direct", 		# Debit Mode
						"", 			# Credit Mode
						"", 			# Debit "YES"
						"YES", 			# Crdit "YES"
						"System", 		# Inputter
						DateTimeNow, 	# Createdon
						"System", 		# Authorizer
						DateTimeNow 	# Authorizedon
					)
					
					# Check if have tax
					TaxObj = MKT_TAX.query.get(Tax)
					TaxRate 	= 0
					TaxCategory = ""
					TaxPayment 	= 0
					if TaxObj:

						TaxRate 	= TaxObj.Rate
						TaxCategory = TaxObj.Category
						TaxPayment 	= (float(AIPBooked) * float(TaxRate)) / float(100)
						Tran 		= Setting.TaxTran
						DateTimeNow = mktdate.getDateTimeNow()
						# Dr/Account Customer Cr/Tax
						setTABooking(
							Branch,			# Branch
							Account, 		# Debit Account
							CrCat, 			# Debit Category
							"", 			# Credit Account
							TaxCategory, 	# Credit Category
							Currency, 		# Currency
							TaxPayment, 	# Amount
							Tran, 			# Transaction
							"TD", 			# Module
							TAID, 			# Reference
							"", 			# Note
							"", 			# Debit Mode
							"Direct", 		# Credit Mode
							"YES", 			# Debit "YES"
							"", 			# Crdit "YES"
							"System", 		# Inputter
							DateTimeNow, 	# Createdon
							"System", 		# Authorizer
							DateTimeNow 	# Authorizedon
						)

					UdateOnBook 	=	MKT_TERM_DEPOSIT_CONTRACT.query.get(TAID)

					if UdateOnBook:
						UdateOnBook.IntPaid 	=	float(UdateOnBook.IntPaid) + float(AIPBooked)
						UdateOnBook.AIPBooked 	=	0
						IntPaid = float(UdateOnBook.IntPaid) if UdateOnBook.IntPaid else float(0)
						
						db.session.add(UdateOnBook)

				# get Object for Update
				UpdateObj =	MKT_TERM_DEPOSIT_CONTRACT.query.get(TAID)
				if UpdateObj:

					UpdateObj.AccrIntPerDay 	=	IntOfTheDay
					UpdateObj.AIPCurrent 		=	float(UpdateObj.AIPCurrent) + float(IntPerDay)
					UpdateObj.NextRunDate 		=	NextSystemDate
					AIPCurrent = float(UpdateObj.AIPCurrent) if UpdateObj.AIPCurrent else float(0)
					Amount = float(UpdateObj.Amount) if UpdateObj.Amount else float(0)
					IntPaid = float(UpdateObj.IntPaid) if UpdateObj.IntPaid else float(0)
					db.session.add(UpdateObj)

				# Check if today is Maturity Date
				if CurrentDate == MaturityDate:
					# print "Int Paid On Maturity Date %s" %IntPaid
					# print "on matured"
					# Booking on Maturity Date
					Tran 		= TASetting.IEBookingTran
					DateTimeNow = mktdate.getDateTimeNow()

					CrCat = AccCategory
					# Dr/IE Cr/Account Customer
					setTABooking(
						Branch,			# Branch
						"", 			# Debit Account
						IECat, 			# Debit Category
						Account, 		# Credit Account
						CrCat, 			# Credit Category
						Currency, 		# Currency
						AIPCurrent, 	# Amount
						Tran, 			# Transaction
						"TD", 			# Module
						TAID, 			# Reference
						"", 			# Note
						"Direct", 		# Debit Mode
						"", 			# Credit Mode
						"", 			# Debit "YES"
						"YES", 			# Crdit "YES"
						"System", 		# Inputter
						DateTimeNow, 	# Createdon
						"System", 		# Authorizer
						DateTimeNow 	# Authorizedon
					)
					
					# Check if have tax
					TaxObj = MKT_TAX.query.get(Tax)
					TaxRate 	= 0
					TaxCategory = ""
					TaxPayment 	= 0
					if TaxObj:

						TaxRate 	= TaxObj.Rate
						TaxCategory = TaxObj.Category
						TaxPayment 	= (float(AIPCurrent) * float(TaxRate)) / float(100)
						Tran 		= Setting.TaxTran
						DateTimeNow = mktdate.getDateTimeNow()
						# Dr/Account Customer Cr/Tax
						setTABooking(
							Branch,			# Branch
							Account, 		# Debit Account
							CrCat, 			# Debit Category
							"", 			# Credit Account
							TaxCategory, 	# Credit Category
							Currency, 		# Currency
							TaxPayment, 	# Amount
							Tran, 			# Transaction
							"TD", 			# Module
							TAID, 			# Reference
							"", 			# Note
							"", 			# Debit Mode
							"Direct", 		# Credit Mode
							"YES", 			# Debit "YES"
							"", 			# Crdit "YES"
							"System", 		# Inputter
							DateTimeNow, 	# Createdon
							"System", 		# Authorizer
							DateTimeNow 	# Authorizedon
						)

					UdateOnBook 	=	MKT_TERM_DEPOSIT_CONTRACT.query.get(TAID)

					if UdateOnBook:
						UdateOnBook.IntPaid 	=	float(UdateOnBook.IntPaid) + float(AIPCurrent)
						UdateOnBook.AIPBooked 	=	0
						# AIPCurrent = float(UpdateObj.AIPCurrent) if UpdateObj.AIPCurrent else float(0)
						Amount = float(UpdateObj.Amount) if UpdateObj.Amount else float(0)
						IntPaid = float(UdateOnBook.IntPaid) if UdateOnBook.IntPaid else float(0)

						db.session.add(UdateOnBook)

					# TD Amount to Client
					DateTimeNow = mktdate.getDateTimeNow()
					DrCat 	=	TASetting.SavingTermCat
					CrCat 	=	AccCategory
					Tran 	=	TASetting.MaturedTran

					setTABooking(
						Branch,			# Branch
						"", 			# Debit Account
						DrCat, 			# Debit Category
						Account, 		# Credit Account
						CrCat, 			# Credit Category
						Currency, 		# Currency
						Amount, 		# Amount
						Tran, 			# Transaction
						"TD", 			# Module
						TAID, 			# Reference
						"", 			# Note
						"Direct", 		# Debit Mode
						"", 			# Credit Mode
						"", 			# Debit "YES"
						"YES", 			# Crdit "YES"
						"System", 		# Inputter
						DateTimeNow, 	# Createdon
						"System", 		# Authorizer
						DateTimeNow 	# Authorizedon
					)

					# Update Matured Term Deposit
					UpdateObj 	=	MKT_TERM_DEPOSIT_CONTRACT.query.get(TAID)
					if UpdateObj:
						
						# UpdateObj.AccrIntPerDay 	= 	0
						# UpdateObj.IntPaid 			=	float(UpdateObj.IntPaid) + float(AIPCurrent)
						# UpdateObj.AIPBooked 		=	0
						UpdateObj.AIPCurrent 		=	0
						# UpdateObj.Amount 			=	TDRollOverAmt
						# UpdateObj.TotalAIP 			=	0

						db.session.add(UpdateObj)

					# Check if roll-over is Y='Yes'
					# print RollOver
					if 'Y' in RollOver:
						print 'with roll-overl'
						TDRollOverAmt = float(0)
						AccObj 	=	MKT_ACCOUNT.query.get(Account)
						if not AccObj:
							db.session.rollback()
							# Call method for error message
							error_msg 	= "Account %s not found." %Account
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)

						Balance = float(AccObj.AvailableBal) if AccObj.AvailableBal else float(0)

						if RollOverOption == 'P':
							print 'P'
							TDRollOverAmt = Amount

						elif RollOverOption == 'IP':
							print 'IP'
							TDRollOverAmt = IntPaid + Amount

						elif RollOverOption == 'BP':
							print 'BP'
							TDRollOverAmt = Balance

						# Start Roll-over Transaction
						if float(Balance) >= float(TDRollOverAmt):

							DateTimeNow = mktdate.getDateTimeNow()
							DrCat 	=	AccCategory
							CrCat 	=	TASetting.SavingTermCat
							Tran 	=	TASetting.RollOverTran

							setTABooking(
								Branch,			# Branch
								Account, 		# Debit Account
								DrCat, 			# Debit Category
								"", 			# Credit Account
								CrCat, 			# Credit Category
								Currency, 		# Currency
								TDRollOverAmt, 	# Amount
								Tran, 			# Transaction
								"TD", 			# Module
								TAID, 			# Reference
								"", 			# Note
								"", 			# Debit Mode
								"Direct", 		# Credit Mode
								"YES", 			# Debit "YES"
								"", 			# Crdit "YES"
								"System", 		# Inputter
								DateTimeNow, 	# Createdon
								"System", 		# Authorizer
								DateTimeNow 	# Authorizedon
							)

							# More current term deposit to hist before start new one
							mktaudit.moveAUTHtoHIST(MKT_TERM_DEPOSIT_CONTRACT, MKT_TERM_DEPOSIT_CONTRACT_HIST, TAID)
							# Update term deposit with roll-over option
							UpdateObj 	=	MKT_TERM_DEPOSIT_CONTRACT.query.get(TAID)
							if UpdateObj:

								ValueDate 		=	mktreschedule.getNextDay(MaturityDate, 1)
								MaturityDate 	= 	mktreschedule.getNextMonth(ValueDate, int(Term))
								
								UpdateObj.Amount 		=	TDRollOverAmt
								UpdateObj.ValueDate 	= 	ValueDate
								UpdateObj.MaturityDate 	=	MaturityDate
								UpdateObj.NextRunDate 	=	ValueDate

								AIPObj 		= 	getTotalInterestTD(Term, InterestDayBasis, InterestKey, ValueDate, MaturityDate, TDRollOverAmt)
								IntPerDay 	= 	AIPObj[0]
								TotalAIP 	= 	AIPObj[1]

								UpdateObj.AccrIntPerDay =	IntPerDay
								UpdateObj.TotalAIP 		=	TotalAIP
								UpdateObj.Cycle 		=	int(UpdateObj.Cycle) + 1

								db.session.add(UpdateObj)

						else:
							mktbjstat.makeLogFileOnError('Y')
							# Call method for error message
							error_msg 	= "Account balance %s not enough to roll-over amount %s." %(Balance, TDRollOverAmt)
							TrueOrFalse = mktmessage.msgError(EOD, error_msg)
							db.session.rollback()

				db.session.commit()
				print_msg = "%s accrual interest has been updated successfully." %TAID
				mktmessage.msgOutputMsg(print_msg)
			
		print ""
		return True

	except Exception, e:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise
		# return False," %s"%e

# def getPeriodic(Frequency):
# 	try:

# 		Periodic 		= 	""
# 		BankDateObj 	=	mktsetting.getBankDateObj()
# 		TreyMeasValue 	=	{'01':['01', '02', '03'], '02':['04', '05', '06'], '03':['07', '08', '09'], '04':['10', '11', '12']}
# 		TreyMeasObj 	=	['01', '02', '03', '04']

# 		BankDate 	= 	mktdate.getBankDate()
# 		Year 		= 	BankDate.year
# 		Month 		= 	BankDate.month
# 		Month 		= 	'%02d' %Month

# 		if int(Frequency) == 1:

# 			Periodic = BankDateObj.NextWeekend

# 		elif int(Frequency) == 2:

# 			Periodic = BankDateObj.NextMonthEnd

# 		elif int(Frequency) == 3:

# 			Quarterly = ''
# 			for item in TreyMeasObj:
# 				if Month in TreyMeasValue[item]:
# 					Quarterly = item
# 					break

# 			if Quarterly == "01":
# 				CurrentDate = "%s-%s-01" %(Year, "03")
				

# 			elif Quarterly == "02":
# 				CurrentDate = "%s-%s-01" %(Year, "06")

# 			elif Quarterly == "03":
# 				CurrentDate = "%s-%s-01" %(Year, "09")

# 			else:
# 				CurrentDate = "%s-%s-01" %(Year, "12")

# 			CurrentDate = 	datetime.strptime(str(CurrentDate),'%Y-%m-%d').date()
# 			QuaDate 	= 	mktdate.getCurrentMonthEnd(CurrentDate)
# 			Periodic 	= 	QuaDate

# 		else:
# 			Periodic = BankDateObj.NextYearEnd

# 		return Periodic

# 	except Exception, e:
# 		db.session.rollback()
# 		mktbjstat.makeLogFileOnError('Y')
# 		raise

def getPeriodicInterest(InterestKey, Term):
	try:

		Rate = 0

		PeriodicObj =	MKT_PER_INTEREST_DE.query.\
						filter(MKT_PER_INTEREST_DE.ID == InterestKey).\
						all()

		if PeriodicObj:

			for item in PeriodicObj:

				StrTerm = item.Term
				StrTerm = StrTerm.split()

				if len(StrTerm) == 1:
					if int(Term) == int(StrTerm[0]):
						Rate = item.Rate

						break

				elif len(StrTerm) == 2:
					if int(StrTerm[0]) <= int(Term) <= int(StrTerm[1]):
						Rate = item.Rate

						break

		return float(Rate)

	except Exception, e:
		db.session.rollback()
		mktbjstat.makeLogFileOnError('Y')
		raise