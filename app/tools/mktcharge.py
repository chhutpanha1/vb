from flask 							import flash
from app.mktcore.imports 			import *
from .. 							import app, db
from sqlalchemy 					import *
from decimal 						import *
from datetime 						import datetime, date, timedelta
from dateutil.relativedelta 		import relativedelta
import time
import calendar

import mktsetting 					as mktsetting
import user 						as mktuser
import mktdate	 					as mktdate
import mktaccounting 				as mktaccounting
import mktkey 						as mktkey
import mktholiday 					as mktHoliday
import mktparam 					as mktparam
import mktaudit 					as mktaudit
import mktmoney 					as mktmoney
import mktautoid 					as mktAutoID
import mktmessage 					as mktmessage
import mktbjstat 					as mktbjstat
import loantools.rescheduletools 	as mktschedule

from app.RepaymentSchedule.models 	import MKT_REP_SCHEDULE
from app.LoanContract.models 		import MKT_LOAN_CONTRACT,MKT_LOAN_CONTRACT_INAU, MKT_LOAN_CHARGE, MKT_LOAN_CHARGE_INAU
from app.ChargeRate.models 			import MKT_CHARGE_RATE
from app.Transaction.models 		import MKT_TRANSACTION
from app.ScheduleDefine.models 		import MKT_SCHED_DEFINE

def updateLoanCharge(ID, Installment, Disbursed, Currency, Resouce="AUTH", Amendment="N"):
	try:
		# print 'updateLoanCharge'
		# Update Loan Charge
		RegularFee = '2'
		if Resouce == "AUTH":
			ChargeObj 	=	MKT_LOAN_CHARGE.query.\
							filter(MKT_LOAN_CHARGE.ID == ID).\
							all()
			LoanContractObj = MKT_LOAN_CONTRACT.query.get(ID)

		else:
			ChargeObj 	=	MKT_LOAN_CHARGE_INAU.query.\
							filter(MKT_LOAN_CHARGE_INAU.ID == ID).\
							all()
			LoanContractObj = MKT_LOAN_CONTRACT_INAU.query.get(ID)

		if ChargeObj:
			for item in ChargeObj:
				ChargeAmt 		=	item.Charge
				ChargeKey 		=	item.ChargeKey
				ChargePerDay 	= 	0
				ChargeEarned 	=	0
				ChargeUnearned 	=	0
				MinimumAmount 	=	0
				ChargeID 		=	ChargeKey + Currency
				ChargePerInstallment 	=	0

				# Check MKT_CHARGE_RATE
				KeyObj 			=	MKT_CHARGE_RATE.query.\
									filter(MKT_CHARGE_RATE.ID == ChargeID).\
									first()
				if KeyObj.ChargeMode != RegularFee:
					if KeyObj:
						MinimumAmount 	=	KeyObj.MinimumAmount

					if item.RateFixed == 'F':
						ChargePerInstallment = float(ChargeAmt) / float(Installment)
						ChargeUnearned 	=	float(ChargeAmt)
						
					elif item.RateFixed == 'R':
						NewChargeAmt 	=	(float(Disbursed) * float(ChargeAmt)) / 100
						ChargeAmt 		=	NewChargeAmt

						if float(ChargeAmt) < float(MinimumAmount):
							ChargeAmt 	=	MinimumAmount

						ChargePerInstallment 	=	float(ChargeAmt) / float(Installment)
						ChargeUnearned 	=	float(ChargeAmt)

					# if Amendment == 'Y':
					# ChargePerInstallment = mktmoney.toMoney(float(ChargePerInstallment), mktmoney.getCurrencyObj(Currency))
					# ChargePerInstallment = str(ChargePerInstallment).replace(',', '')
					# ChargePerInstallment = float(ChargePerInstallment)

					ChargeEarned 	= mktmoney.toMoney(float(ChargeEarned), mktmoney.getCurrencyObj(Currency))
					ChargeEarned 	= str(ChargeEarned).replace(',', '')
					ChargeEarned 	= float(ChargeEarned)

					ChargeUnearned 	= mktmoney.toMoney(float(ChargeUnearned), mktmoney.getCurrencyObj(Currency))
					ChargeUnearned 	= str(ChargeUnearned).replace(',', '')
					ChargeUnearned 	= float(ChargeUnearned)

					if Amendment == 'Y':
						ChargeAmt 		=	item.ChargeUnearned
						ChargeUnearned 	=	ChargeAmt
						ChargeEarned 	=	item.ChargeEarned
						ChargePerInstallment = float(ChargeAmt) / float(Installment)
					# check reference on bookingChargeAmortization where amortizebase = E why we add 1
					DayOfLoanLife = mktdate.getDateDiff(LoanContractObj.ValueDate,LoanContractObj.MaturityDate) + 1
					ChargePerDay  = getChargePerDay(ChargeUnearned,DayOfLoanLife)
					# print 'DayOfLoanLife',DayOfLoanLife,'ChargePerDay',ChargePerDay
					# Update Charge Object
					item.ChargePerDay 			= 	ChargePerDay 		
					item.ChargePerInstallment 	=	ChargePerInstallment
					item.ChargeEarned 			=	ChargeEarned
					item.ChargeUnearned 		=	ChargeUnearned
					item.Mode 					=	'1'
					db.session.add(item)

		return [True, ""]

	except Exception, e:
		db.session.rollback()
		return [False, "%s" %e]

def bookingLoanCharge(ID, Currency, Account, AccCategory, Branch, Resouce="AUTH", TranDate='', Authorizer=''):
	try:
		# print 'bookingLoanCharge'
		# Lookup Loan Charge by Loan ID
		if Resouce == "AUTH":
			ChargeObj 	=	MKT_LOAN_CHARGE.query.\
							filter(MKT_LOAN_CHARGE.ID == ID).\
							all()
		else:
			ChargeObj 	=	MKT_LOAN_CHARGE_INAU.query.\
							filter(MKT_LOAN_CHARGE_INAU.ID == ID).\
							all()

		if ChargeObj:
			# Defined variable for booking accounting
			UnearnedCat =	mktsetting.getAppSetting('UnEarnedIncomeCat')
			Setting 	=	mktsetting.getAccSetting()
			RegularFee  = "2"
			if not TranDate:
				TranDate 	=	mktdate.getBankDate()

			if Setting:
				ChargeTran 	= 	Setting.ChargeTran
			else:
				return [False, "Please go to config accounting setting."]

			
			for item in ChargeObj:

				ChargeKey 		=	item.ChargeKey + Currency
				ChargeAmount 	=	item.ChargeUnearned
				EarnedAmt 		=	ChargeAmount
				UnearnedAmt 	=	0

				Inputter 		= 	item.Inputter
				Authorizer		= 	Authorizer if Authorizer else item.Authorizer

				ChargeObj 		=	MKT_CHARGE_RATE.query.\
									filter(MKT_CHARGE_RATE.ID == ChargeKey).\
									first()

				if ChargeObj:

					Amortize 	=	ChargeObj.Amortize
					ChargeCat 	=	ChargeObj.IncomeCat
					ChargeMode  =   ChargeObj.ChargeMode
					if ChargeMode != RegularFee:
						if Amortize == 'Y':
							
							if not UnearnedCat:
								return [False, "Please create application setting(UnearnedCat)."]

							ChargeCat 	=	UnearnedCat
							EarnedAmt 	=	0
							UnearnedAmt =	ChargeAmount
							ChargeTran 	=	mktsetting.getAppSetting('ChargeUnearnedTran')
						else:
							ChargeTran 	= 	Setting.ChargeTran
							item.ChargePerDay = 0
							item.ChargePerInstallment = 0

						if ChargeTran:

							# Booking Charge Income with Amortization "Yes" or "No"
							for i in range(0, 2):
												
								if i == 0:
									DrCr 		= 	"Dr"
									Mode 		= 	""
									Category 	= 	AccCategory
									CusAccount 	=	Account
								else:
									DrCr 		= 	"Cr"
									Mode 		= 	"Direct"
									Category 	= 	ChargeCat
									CusAccount 	=	""

								GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
								DateTimeNow = mktdate.getDateTimeNow()
								mktaccounting.postAccounting(
									"AUTH", 				# Status
									"0", 					# Curr
									Inputter,				# Inputter
									DateTimeNow, 			# Createdon
									Authorizer,				# Authorizer
									DateTimeNow,			# Authorizeon
									"", 					# AEID
									CusAccount,				# Account
									Category,				# Category
									Currency,				# Currency
									DrCr,					# DrCr
									Decimal(ChargeAmount), 	# Amount
									"LC",					# Module
									ChargeTran, 			# Transaction
									TranDate, 				# TransactionDate
									ID, 					# Reference
									"", 					# Note
									"", 					# JNID
									Branch,					# Branch
									GL_KEYS,				# GL_KEYS
									Mode 					# Mode check to insert Journal for category
								)

							# Update Loan Charge Information
							item.ChargeEarned 	  = EarnedAmt
							item.ChargeUnearned   = UnearnedAmt
							item.ChargeLastBooked = EarnedAmt
							db.session.add(item)

						else:
							return [False, "Charge transaction not found."]

				else:
					return [False, "Charge rate %s not found." %ChargeKey]


		return [True, ""]

	except Exception, e:
		db.session.rollback()
		return [False, "%s" %e]

def bookingChargeAmortization(ID, Currency, Branch, Installment='0', Termination="N", LoanContractObj=[],IsMonthEnd=False,LastCollection=False):
	try:
		# Lookup Loan Charge by Loan ID
		ChargeObj 	=	MKT_LOAN_CHARGE.query.\
						filter(MKT_LOAN_CHARGE.ID == ID).\
						all()

		if ChargeObj:

			# Defined variable for booking accounting
			TranDate 			=	mktdate.getBankDate()
			Setting 			=	mktsetting.getAccSetting()
			UnearnedCat 		=	mktsetting.getAppSetting('UnEarnedIncomeCat')
			
			AmortizationBase 	=	mktsetting.getAppSetting('AmortizationBase')
			if not AmortizationBase :
				AmortizationBase = 'C'
				
			if Installment == True or Installment == False:
				LastInstallment = Installment
			else:
				LastInstallment 	=	getLastInstallment(ID, Installment)

			if Setting:
				ChargeTran 	= 	Setting.ChargeTran
			else:
				return [False, "Please go to config accounting setting."]

			RegularFee 	= '2'
			ChargeRateObj = MKT_CHARGE_RATE.query

			for item in ChargeObj:

				ChargeKey 		= item.ChargeKey + Currency
				ChargeEarned 	= float(item.ChargeEarned) if item.ChargeEarned else float(0)
				ChargeUnearned 	= float(item.ChargeUnearned) if item.ChargeUnearned else float(0) 
				ChargeAmount    = 0
				
				ChargeRateRecord = ChargeRateObj.filter(MKT_CHARGE_RATE.ID == ChargeKey).first()

				if ChargeRateRecord.ChargeMode != RegularFee:

					# check charge amortization if equal No we do not do amortize and continue to next record
					if ChargeRateRecord.Amortize == 'N':
						# print 'Charge Rate Amortize = No'
						continue

					if AmortizationBase == 'E': # base on end of month
						# print 'Amortization base E'
						if IsMonthEnd == True or LastCollection == True:
							BankDate 		= str(TranDate)
							ValueDate       = LoanContractObj.ValueDate
							LastDate 		= ValueDate if ValueDate[0:7] == BankDate[0:7] else BankDate[0:8]+'01' #getLastMonthEnd(BankDate)  
							OneDay 			= 1 # Add Day End Of Month 
							NumberOfDays    = mktdate.getDateDiff(LastDate,BankDate) + OneDay
							'''
								Example :
								Value Date 		= 2016-05-26
								End Of Month 	= 2016-06-31
								Day Diff 		= 5 Day
								=> The Result 	= 6 Day
								Because Add The Day End Of Month too 

								26/05   27     28     29     30     31     01/06
								|--------|------|------|------|------|------|
								1D 		2D 		3D 	   4D     5D     6D

							'''
							ChargePerDay		  = float(item.ChargePerDay)
							ChargeLastBooked 	  = ChargePerDay * float(NumberOfDays)
							ChargeAmount 		  = float(ChargeLastBooked)
							
							item.ChargeLastBooked = ChargeLastBooked
							
							if LastCollection == True:
								ChargeAmount  = float(item.ChargeUnearned)
								item.ChargeLastBooked = ChargeAmount
							'''
								Purpose of this block of code run when loan maturity date
								before end of month so we have to collect all charge on 
								last collection date
							'''
							# print 'BankDate', BankDate,'ValueDate', ValueDate,'LastDate', LastDate,'NumberOfDays', NumberOfDays
							# print 'ChargePerDay', ChargePerDay,'ChargeLastBooked', ChargeLastBooked,'ChargeAmount', ChargeAmount
							
					elif AmortizationBase == 'C': # base on collection date
						# print 'Amortization base C'
						ChargeAmount = float(item.ChargePerInstallment) if item.ChargePerInstallment else float(0)
						item.ChargeLastBooked = ChargeAmount
					
					else:
						return [False, "Amortization Base is not defined."]

					if LastInstallment == True or Termination == "Y":
						ChargeAmount = float(item.ChargeUnearned) if item.ChargeUnearned else float(0)
						item.ChargeLastBooked = ChargeAmount

					ChargeObj 		=	MKT_CHARGE_RATE.query.\
										filter(MKT_CHARGE_RATE.ID == ChargeKey).\
										first()

					if ChargeUnearned > 0:

						ChargeEarned 	+=	ChargeAmount
						ChargeUnearned 	-=	ChargeAmount

						if ChargeObj:

							ChargeCat 		=	ChargeObj.IncomeCat

							if ChargeTran:

								# Booking Charge Income with Amortization "Yes" or "No"
								for i in range(0, 2):
													
									if i == 0:
										DrCr 		= 	"Dr"
										Mode 		= 	"Direct"
										Category 	= 	UnearnedCat
									else:
										DrCr 		= 	"Cr"
										Mode 		= 	"Direct"
										Category 	= 	ChargeCat

									Account 	= ""
									GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
									DateTimeNow = mktdate.getDateTimeNow()
									mktaccounting.postAccounting(
										"AUTH", 				# Status
										"0", 					# Curr
										"System",				# Inputter
										DateTimeNow, 			# Createdon
										"System",				# Authorizer
										DateTimeNow,			# Authorizeon
										"", 					# AEID
										Account,				# Account
										Category,				# Category
										Currency,				# Currency
										DrCr,					# DrCr
										Decimal(ChargeAmount), 	# Amount
										"LC",					# Module
										ChargeTran, 			# Transaction
										TranDate, 				# TransactionDate
										ID, 					# Reference
										"", 					# Note
										"", 					# JNID
										Branch,					# Branch
										GL_KEYS,				# GL_KEYS
										Mode 					# Mode check to insert Journal for category
									)

								# Update Loan Charge Information
								item.ChargeEarned 	=	ChargeEarned
								item.ChargeUnearned =	ChargeUnearned
								db.session.add(item)

								print "Fee and charge earned for %s: %s." %(ID, mktmoney.toMoney(float(ChargeAmount), mktmoney.getCurrencyObj(Currency)))

							else:
								return [False, "Charge transaction not found."]

						else:
							return [False, "Charge rate %s not found." %ChargeKey]

		return [True, ""]

	except Exception, e:
		db.session.rollback()
		return [False, "%s" %e]

def getLastInstallment(ID, Installment):
	try:
		
		ScheduleObj 	=	MKT_REP_SCHEDULE.query.\
							order_by(MKT_REP_SCHEDULE.No.asc()).\
							filter(MKT_REP_SCHEDULE.LoanID == ID).\
							filter(MKT_REP_SCHEDULE.RepStatus == '0').\
							limit(1).\
							all()

		LastInstallment =	'0'

		if ScheduleObj:
			for item in ScheduleObj:
				LastInstallment 	=	item.No

		if int(Installment) == int(LastInstallment):
			return True
		else:
			return False

	except Exception, e:
		# return False
		db.session.rollback()
		raise

# Get total loan charge
def getLoanChargeAmount(ID, Currency):
	try:
		
		# Lookup Loan Charge by Loan ID
		ChargeObj 	=	MKT_LOAN_CHARGE.query.\
						filter(MKT_LOAN_CHARGE.ID == ID).\
						all()

		Amount 		=	0

		if ChargeObj:

			LoanObj 	=	MKT_LOAN_CONTRACT.query.get(ID)
			if not LoanObj:
				return "Loan contract %s not found." %ID
			else:

				Disbursed 	=	LoanObj.Disbursed
				for item in ChargeObj:
					if item.RateFixed == 'F':
						Amount 	+= float(item.Charge)
					elif item.RateFixed == 'R':
						Charge 	=	(float(Disbursed) * float(item.Charge)) / float(100)
						Amount 	+= float(Charge)

		Amount 	=	mktmoney.toMoney(float(Amount), mktmoney.getCurrencyObj(Currency), 1)
		return Amount

	except Exception, e:
		return "%s" %e

def getLastMonthEnd(Date):
	Year  	= int(Date[0:4])
	Month 	= int(Date[5:7])
	DateObj = date(Year, Month, 1) - relativedelta(days=1)
	return str(DateObj.year)+'-'+str(DateObj.month)+'-'+str(DateObj.day)

def getTotalCharge(item, Disbursed, ChargeID):
	KeyObj 			=	MKT_CHARGE_RATE.query.filter(MKT_CHARGE_RATE.ID == ChargeID).first()
	ChargeAmt 		=	item.Charge

	if KeyObj:
		MinimumAmount 	=	KeyObj.MinimumAmount

	if item.RateFixed == 'R':
		NewChargeAmt 	=	(float(Disbursed) * float(ChargeAmt)) / 100
		ChargeAmt 		=	NewChargeAmt

		if float(ChargeAmt) < float(MinimumAmount):
			ChargeAmt 	=	MinimumAmount

	return ChargeAmt

def getChargePerDay(TotalCharge,DayOfLoanLife):
	ChargePerDay = float(TotalCharge)/float(DayOfLoanLife)
	return ChargePerDay

'''
	Goal of this method(updateLoanChargeAmount) is for updating loan charge amount
	when client input incorrect charge amount or rate
'''
def updateLoanChargeAmount(ContractID,ChargeKey,ChargeAmountOrRate):
	try:
		LoanContractObj 	= 	MKT_LOAN_CONTRACT.query.get(ContractID)
		if LoanContractObj:
			Currency 		= 	LoanContractObj.Currency
			ChargeRateObj 	= 	MKT_CHARGE_RATE.query.get(ChargeKey+Currency)

			if ChargeRateObj:
				if ChargeRateObj.RateFixed == 'R':
					ChargeAmount 	=	float(ChargeAmountOrRate)*float(LoanContractObj.Disbursed)/100
				elif ChargeRateObj.RateFixed == 'F':
					ChargeAmount 	=	ChargeAmountOrRate 

				# check reference on bookingChargeAmortization where amortizebase = E why we add 1
				DayOfLoanLife 		= 	mktdate.getDateDiff(LoanContractObj.ValueDate,LoanContractObj.MaturityDate) + 1
				ChargePerDay  		= 	getChargePerDay(ChargeAmount,DayOfLoanLife)
				ChargePerInstallment=	float(ChargeAmount) / float(LoanContractObj.Installment)
				LoanChargeRecord 	= 	MKT_LOAN_CHARGE.query.\
										filter(MKT_LOAN_CHARGE.ID == ContractID).\
										filter(MKT_LOAN_CHARGE.ChargeKey == ChargeKey).\
										first()
				UnearnedCat 		=	mktsetting.getAppSetting('UnEarnedIncomeCat')
				AccCategory 		= 	MKT_ACCOUNT.query.get(Account).AccCategory
				ChargeCat 			=	ChargeRateObj.IncomeCat
				TranDate 			= 	LoanContractObj.ValueDate
				Setting 			=	mktsetting.getAccSetting()
				ChargeLastBooked 	=	0

				if ChargeRateObj.Amortize == 'Y':
					ChargeUnearned   	=	ChargeAmount
					ChargeEarned 	 	=	0
					ChargeCat 		 	=	UnearnedCat
					ChargeTran 			=	mktsetting.getAppSetting('ChargeUnearnedTran')
				elif ChargeRateObj.Amortize == 'N':
					ChargeTran 		 	= 	Setting.ChargeTran
					ChargeUnearned   	=	0
					ChargeEarned 	 	=	ChargeAmount
					ChargeLastBooked 	=	ChargeAmount

				if LoanChargeRecord:
					ReverseAmount = float(LoanChargeRecord.ChargeEarned) + float(LoanChargeRecord.ChargeUnearned)
					for i in range(0, 2):
						if i == 0:
							DrCr 		= 	"Cr"
							Mode 		= 	"Direct"
							Category 	= 	AccCategory
							CusAccount 	=	LoanContractObj.Account
						else:
							DrCr 		= 	"Dr"
							Mode 		= 	""
							Category 	= 	ChargeCat
							CusAccount 	=	""

						GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
						DateTimeNow = mktdate.getDateTimeNow()
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"System",				# Inputter
							DateTimeNow, 			# Createdon
							"System",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							CusAccount,				# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(ReverseAmount), # Amount
							"LC",					# Module
							ChargeTran, 			# Transaction
							TranDate, 				# TransactionDate
							ID, 					# Reference
							"", 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode 					# Mode check to insert Journal for category
						)
					
					LoanChargeRecord.ChargeUnearned 		=	ChargeUnearned
					LoanChargeRecord.ChargeEarned 			=	ChargeEarned
					LoanChargeRecord.ChargeLastBooked 		=	ChargeLastBooked
					LoanChargeRecord.Charge 			  	=	ChargeAmountOrRate
					LoanChargeRecord.RateFixed 			  	=	ChargeRateObj.RateFixed
					LoanChargeRecord.ChargePerDay 		  	=	ChargePerDay
					LoanChargeRecord.ChargePerInstallment 	= 	ChargePerInstallment

					db.session.add(LoanChargeRecord)
					Msg = 'Loan Charge is updated'

				else:
					
					Audit 	 =	mktaudit.getAuditrail()
					NewCharge = MKT_LOAN_CHARGE (
						Status      = Audit['Status'],
						Curr        = Audit['Curr'],
						Inputter    = Audit['Inputter'],
						Createdon   = Audit['Createdon'],
						Authorizer  = Audit['Authorizer'],
						Authorizeon = Audit['Authorizeon'],
						Branch 	  	= Audit['Branch'],
						ID 			= ContractID,
						ChargeKey 	= ChargeKey,
						RateFixed 	= ChargeRateObj.RateFixed,
						Charge 		= ChargeAmountOrRate,
						Mode 		= '1',
						ChargePerInstallment = ChargePerInstallment,
						ChargePerDay 		 = ChargePerDay,
						ChargeEarned 		 = ChargeEarned,
						ChargeUnearned 		 = ChargeUnearned,
						ChargeLastBooked 	 = ChargeLastBooked
					)
					db.session.add(NewCharge)
					Msg = 'New Loan Charge is created'

				for i in range(0, 2):
					if i == 0:
						DrCr 		= 	"Dr"
						Mode 		= 	""
						Category 	= 	AccCategory
						CusAccount 	=	LoanContractObj.Account
					else:
						DrCr 		= 	"Cr"
						Mode 		= 	"Direct"
						Category 	= 	ChargeCat
						CusAccount 	=	""

					GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency, "", "LC", "", "", "", "", "", "", "", "", "")
					DateTimeNow = mktdate.getDateTimeNow()
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"System",				# Inputter
						DateTimeNow, 			# Createdon
						"System",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						CusAccount,				# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(ChargeAmount), 	# Amount
						"LC",					# Module
						ChargeTran, 			# Transaction
						TranDate, 				# TransactionDate
						ID, 					# Reference
						"", 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)

				return True, Msg
			else:
				return False, 'Charge Rate not found'
				
		else:
			return False, 'Loan Contract not found'

	except Exception, e:
		return False, e

def getAccrChargeTran():
	"""Accrued Charge Receivable Booking """
	Transaction = mktsetting.getAppSetting('ACCR_CHARGE_TRAN')

	return Transaction

def getAccrChargeRevTran():
	""" Accrued Charge Receivable Reversal """
	Transaction = mktsetting.getAppSetting('ACCR_CHARGE_REV_TRAN')

	return Transaction

def getAccrChargeDaily():
	""" Get Status of Enable Accrued Charge Receivable Daily """
	EnableCharge = mktsetting.getAppSetting('ACCR_CHARGE_DAILY')

	return EnableCharge

def getChargeRegularObj(**kwargs):
	try:

		ID 		   	= kwargs['LoanID']
		Currency   	= kwargs['Currency']
		RegularFee = "2" # RegularFee Type
		
		LoanChargeObj 	=	db.session.query(MKT_LOAN_CHARGE, MKT_CHARGE_RATE).\
								filter(MKT_LOAN_CHARGE.ID ==ID).\
								filter(MKT_CHARGE_RATE.ChargeMode == RegularFee).\
								join(MKT_CHARGE_RATE,MKT_CHARGE_RATE.ID ==MKT_LOAN_CHARGE.ChargeKey+Currency).first()
		if not LoanChargeObj:
			LoanChargeObj 	=	db.session.query(MKT_LOAN_CHARGE_INAU, MKT_CHARGE_RATE).\
								filter(MKT_LOAN_CHARGE_INAU.ID ==ID).\
								filter(MKT_CHARGE_RATE.ChargeMode == RegularFee).\
								join(MKT_CHARGE_RATE,MKT_CHARGE_RATE.ID ==MKT_LOAN_CHARGE_INAU.ChargeKey+Currency).first()

		return LoanChargeObj

	except Exception as e:
		raise
	else:
		pass
	finally:
		pass

def getAccrCurrCharge(**kwargs):
	try:
		AccrCurrent 	= Decimal(0)
		LoanChargeObj 	= getChargeRegularObj(**kwargs)
		if LoanChargeObj:
			ChargeObj 	= LoanChargeObj[0]
			ChargeRate 	= LoanChargeObj[1]
			AccrCurrent = ChargeObj.AccrCurrent if ChargeObj.AccrCurrent else Decimal(0)
		return AccrCurrent

	except Exception as e:
		raise
	else:
		pass
	finally:
		pass

def getAccrChargeAmount(**kwargs):
	""" Get Accr Charge Amount """
	try:
		ID 			= kwargs['LoanID']
		Zero 		= Decimal(0)
		AccrCharge 	= Zero
		TransactionDate = str(kwargs['TransactionDate'])
		ScheduleObj 	=	MKT_REP_SCHEDULE.query.\
							order_by(MKT_REP_SCHEDULE.No.asc()).\
							filter(MKT_REP_SCHEDULE.LoanID == ID).\
							filter(MKT_REP_SCHEDULE.CollectionDate > TransactionDate).\
							limit(1).first()
		if ScheduleObj:
			ChargeCollect = Decimal(ScheduleObj.Charge)
			NumberOfDays  = Decimal(ScheduleObj.NumDay)
			if ChargeCollect > Zero:
				AccrCharge = ChargeCollect / NumberOfDays
		return AccrCharge
	except Exception as e:
		raise
	else:
		pass
	finally:
		pass

def isConfigChargeRegular():
	""" Checking if user setup charge regular fee will return True else False """
	try:
		RegularFee = "2" # RegularFee Type (Charge on repayment schedule)
		ChargeObj = MKT_CHARGE_RATE.query.filter(MKT_CHARGE_RATE.ChargeMode == RegularFee).first()
		if ChargeObj:
			return True
		else:
			return False
	except Exception as e:
		raise
	else:
		pass
	finally:
		pass

def setAccrChargeDaily(**kwargs):
	try:

		ID 		   	= kwargs['LoanID']
		Currency   	= kwargs['Currency']
		LoanBalance = kwargs['LoanBalance']
		Branch 	   	= kwargs['Branch']
		Suspend 	= kwargs['Suspend']
		TransactionDate   = kwargs['TransactionDate']
		RegularFee = "2" # RegularFee Type
		Transaction =	getAccrChargeTran()


		LoanChargeObj 	=	db.session.query(MKT_LOAN_CHARGE, MKT_CHARGE_RATE).\
										filter(MKT_LOAN_CHARGE.ID ==ID).\
										filter(MKT_CHARGE_RATE.ChargeMode == RegularFee).\
										join(MKT_CHARGE_RATE,MKT_CHARGE_RATE.ID ==MKT_LOAN_CHARGE.ChargeKey+Currency).first()

		if LoanChargeObj:
			ChargeObj 	= LoanChargeObj[0]
			ChargeRate 	= LoanChargeObj[1]

			ChargeValue	= ChargeObj.Charge
			IncomeCat   = ChargeRate.IncomeCat
			ReceiveCat  = ChargeRate.ReceiveCat
			SuspendCat  = ChargeRate.ChargeSuspendCat
			RegularFeeType = ChargeRate.RegularFeeType

			if RegularFeeType == "3":# Rate Base
				SchduleDefine = MKT_SCHED_DEFINE.query.get(ID)
				if SchduleDefine:
					ChargePerDay = mktschedule.getChargeAmountPerDay(float(LoanBalance),float(ChargeValue))
				else:
					ChargePerDay = getAccrChargeAmount(**kwargs)

			else:
				ChargePerDay = getAccrChargeAmount(**kwargs)

			# Update Accr Charge to Loan contract
			ChargeObj.ChargeUnearned = ChargeObj.ChargeUnearned + Decimal(ChargePerDay)
			AccrCurrent = ChargeObj.AccrCurrent if ChargeObj.AccrCurrent else 0
			ChargeObj.AccrCurrent 	 =  AccrCurrent + Decimal(ChargePerDay)
			ChargeObj.ChargePerDay   = Decimal(ChargePerDay)
			db.session.add(ChargeObj)

			for i in range(0, 2):
				if i == 0:
					DrCr 		= 	"Dr"
					Mode 		= 	"Direct"
					Category 	= 	ReceiveCat
				else:
					DrCr 		= 	"Cr"
					Mode 		= 	"Direct"
					Category 	= 	IncomeCat if Suspend == "N" else SuspendCat

				GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency)
				DateTimeNow = mktdate.getDateTimeNow()
				mktaccounting.postAccounting(
					"AUTH", 				# Status
					"0", 					# Curr
					"System",				# Inputter
					DateTimeNow, 			# Createdon
					"System",				# Authorizer
					DateTimeNow,			# Authorizeon
					"", 					# AEID
					"",						# Account
					Category,				# Category
					Currency,				# Currency
					DrCr,					# DrCr
					Decimal(ChargePerDay), 	# Amount
					"LC",					# Module
					Transaction, 			# Transaction
					TransactionDate, 		# TransactionDate
					ID, 					# Reference
					"", 					# Note
					"", 					# JNID
					Branch,					# Branch
					GL_KEYS,				# GL_KEYS
					Mode 					# Mode check to insert Journal for category
				)


		
	except Exception as e:
		db.session.rollback()
		raise
	else:
		pass
	finally:
		pass


def setChargeCollection(**kwargs):
	"""
		Charge Collection on repayment schdule has two mode :
			1. With Accrued Daily 
			2. Without Accrued Daily
	"""
	try:
		Branch 	   		= kwargs['Branch']
		ChargeCollect 	= Decimal(kwargs['ChargeCollect'])
		TotalCollect 	= Decimal(kwargs['TotalCollect'])
		ID 		   		= kwargs['LoanID']
		Note	   		= kwargs['Note'] if 'Note' in kwargs else ''
		ManualCollectID	= kwargs['ManualCollectID'] if 'ManualCollectID' in kwargs else ''
		Currency   		= kwargs['Currency']
		Account 		= kwargs['Account']
		Suspend 		= kwargs['Suspend']
		AccCategory 	= kwargs['AccCategory']
		Transaction 	= kwargs['Transaction']
		TransactionDate = kwargs['TransactionDate']
		Termination 	= kwargs['Termination'] if 'Termination' in kwargs else "N"
		Reference 		= kwargs['Reference'] if 'Reference' in kwargs else ID
		AccSetting 		= kwargs['AccSetting']
		PDCollect		= kwargs['PDCollect'] if 'PDCollect' in kwargs else "N"
		RegularFee 		= '2'
		AccrChargeDaily = getAccrChargeDaily()
		LoanChargeObj 	=	db.session.query(MKT_LOAN_CHARGE, MKT_CHARGE_RATE).\
										filter(MKT_LOAN_CHARGE.ID ==ID).\
										filter(MKT_CHARGE_RATE.ChargeMode == RegularFee).\
										join(MKT_CHARGE_RATE,MKT_CHARGE_RATE.ID ==MKT_LOAN_CHARGE.ChargeKey+Currency).first()

		if LoanChargeObj:
			Zero 		= Decimal(0)
			ChargeObj 	= LoanChargeObj[0]
			ChargeRate 	= LoanChargeObj[1]

			ReceiveCat  = ChargeRate.ReceiveCat
			IncomeCat   = ChargeRate.IncomeCat
			SuspendCat  = ChargeRate.ChargeSuspendCat
			# Block Update Accr Charge to Loan contract

			AccrCurrent 	= ChargeObj.AccrCurrent if ChargeObj.AccrCurrent else Zero
			ChargeUnearned 	= ChargeObj.ChargeUnearned

			# Calculate Charge Income
			# Case Add Income
			AddIncome 		= TotalCollect - AccrCurrent

			# Case Subtract Income
			SubtractIncome 	= AccrCurrent - TotalCollect


			# Update Accr Current Installment 
			if PDCollect == "N":# If loan on collection
				ChargeObj.AccrCurrent 	 = Zero
			

			# End block
			if ChargeCollect > Zero:

				# Add Accr Charge Receivable to Earned
				ChargeObj.ChargeEarned = ChargeObj.ChargeEarned + Decimal(ChargeCollect)

				for i in range(0, 2):
					if i == 0:
						DrCr 		= 	"Dr"
						Mode 		= 	""
						Category 	= 	AccCategory
						
					else:
						DrCr 		= 	"Cr"
						Mode 		= 	"Direct"
						# With Accrued Daily  
						Category 	= 	ReceiveCat if AccrChargeDaily == "Y" else IncomeCat
						Account 	= 	""

					GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency)
					DateTimeNow = mktdate.getDateTimeNow()
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"System",				# Inputter
						DateTimeNow, 			# Createdon
						"System",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						Account,				# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(ChargeCollect), # Amount
						"LC",					# Module
						Transaction, 			# Transaction
						TransactionDate, 		# TransactionDate
						Reference, 				# Reference
						Note, 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode, 					# Mode check to insert Journal for category
						UserReference=ManualCollectID 	# UserReference
					)
				print ID,'charge was collected. Amount',mktmoney.formatNumber(float(ChargeCollect))

			if AccrChargeDaily == "Y": # With Accrued Daily  
				if PDCollect == "N":

					# Case Add Income, Because Income less than schdule
					if AddIncome > Zero:
						# Add Accr Charge Receivable 
						ChargeObj.ChargeUnearned = ChargeObj.ChargeUnearned + Decimal(AddIncome)

						# Add Charge Income
						ChargeObj.ChargeUEarned = ChargeObj.ChargeEarned + Decimal(AddIncome)
						AccrChargeTran = getAccrChargeTran()

						# print ChargeCollect,Termination
						for i in range(0, 2):
							if i == 0:
								DrCr 		= 	"Dr"
								Mode 		= 	"Direct"
								Category 	= 	ReceiveCat

							else:

								DrCr 		= 	"Cr"
								Mode 		= 	"Direct"
								Category 	= 	IncomeCat if Suspend == "N" else SuspendCat
							
							GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency)
							DateTimeNow = mktdate.getDateTimeNow()
							mktaccounting.postAccounting(
								"AUTH", 				# Status
								"0", 					# Curr
								"System",				# Inputter
								DateTimeNow, 			# Createdon
								"System",				# Authorizer
								DateTimeNow,			# Authorizeon
								"", 					# AEID
								"",						# Account
								Category,				# Category
								Currency,				# Currency
								DrCr,					# DrCr
								Decimal(AddIncome), 	# Amount
								"LC",					# Module
								AccrChargeTran, 		# Transaction
								TransactionDate, 		# TransactionDate
								Reference, 				# Reference
								Note, 					# Note
								"", 					# JNID
								Branch,					# Branch
								GL_KEYS,				# GL_KEYS
								Mode, 					# Mode check to insert Journal for category
								UserReference=ManualCollectID 	# UserReference
							)
						print ID,'charge add income:',AddIncome
					# Case Subtract Income Because income more than schedule
					if SubtractIncome > Zero:
						# Subtract Accr Charge Receivable
						ChargeObj.ChargeUnearned = ChargeObj.ChargeUnearned - Decimal(SubtractIncome)

						ChargeRevTran = getAccrChargeRevTran()
						for i in range(0, 2):
							if i == 0:
								DrCr 		= 	"Dr"
								Mode 		= 	"Direct"
								Category 	= 	IncomeCat if Suspend == "N" else SuspendCat
								
							else:
								DrCr 		= 	"Cr"
								Mode 		= 	"Direct"
								Category 	= 	ReceiveCat
							

							GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency)
							DateTimeNow = mktdate.getDateTimeNow()
							mktaccounting.postAccounting(
								"AUTH", 				# Status
								"0", 					# Curr
								"System",				# Inputter
								DateTimeNow, 			# Createdon
								"System",				# Authorizer
								DateTimeNow,			# Authorizeon
								"", 					# AEID
								"",						# Account
								Category,				# Category
								Currency,				# Currency
								DrCr,					# DrCr
								Decimal(SubtractIncome), # Amount
								"LC",					# Module
								ChargeRevTran, 			# Transaction
								TransactionDate, 		# TransactionDate
								Reference, 				# Reference
								Note, 					# Note
								"", 					# JNID
								Branch,					# Branch
								GL_KEYS,				# GL_KEYS
								Mode, 					# Mode check to insert Journal for category
								UserReference=ManualCollectID 	# UserReference
							)
						print ID,'charge subtract income:',SubtractIncome
				# Case charge collection in suspense
				# We need to reverse suspend to income
				if Suspend == "Y" and ChargeCollect > Zero and ChargeUnearned > Zero:
					SuspendTran = AccSetting.SuspenseReverseTran
					Note = "Charge suspense reversal to income"
					for i in range(0, 2):
						if i == 0:
							DrCr 		= 	"Dr"
							Mode 		= 	"Direct"
							Category 	= 	SuspendCat
							
						else:
							DrCr 		= 	"Cr"
							Mode 		= 	"Direct"
							Category 	= 	IncomeCat

						GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency)
						DateTimeNow = mktdate.getDateTimeNow()
						mktaccounting.postAccounting(
							"AUTH", 				# Status
							"0", 					# Curr
							"System",				# Inputter
							DateTimeNow, 			# Createdon
							"System",				# Authorizer
							DateTimeNow,			# Authorizeon
							"", 					# AEID
							"",						# Account
							Category,				# Category
							Currency,				# Currency
							DrCr,					# DrCr
							Decimal(ChargeCollect), # Amount
							"LC",					# Module
							SuspendTran, 			# Transaction
							TransactionDate, 		# TransactionDate
							Reference, 				# Reference
							Note, 					# Note
							"", 					# JNID
							Branch,					# Branch
							GL_KEYS,				# GL_KEYS
							Mode, 					# Mode check to insert Journal for category
							UserReference=ManualCollectID 	# UserReference
						)
					print ID,'Charge suspense reversal to income. Amount',mktmoney.formatNumber(float(ChargeCollect))
				# Why I have put this code on the last func, Because finally 
				# I need to subtract Accr Charge
				if ChargeCollect > Zero:
					# Subtract Accr Charge Receivable by Collect Amount
					ChargeUnearned =  ChargeUnearned - ChargeCollect
					if ChargeUnearned > Zero: 
						ChargeObj.ChargeUnearned 	=  ChargeUnearned
					else:# Case Accr Charge less than 0 update to 0
						ChargeObj.ChargeUnearned 	= Zero 
			db.session.add(ChargeObj)

	except Exception as e:
		raise
	else:
		pass
	finally:
		pass

def setChargeClassification(**kwargs):
	try:
		Zero			= Decimal(0)
		Branch 	   		= kwargs['Branch']
		ID 		   		= kwargs['LoanID']
		Currency   		= kwargs['Currency']
		Transaction 	= kwargs['Transaction']
		TransactionDate = kwargs['TransactionDate']
		Suspend 		= kwargs['Suspend']
		AccSetting 		= kwargs['AccSetting']
		Reference 		= kwargs['Reference'] if 'Reference' in kwargs else ID
		RegularFee 		= '2'
		LoanChargeObj 	=	db.session.query(MKT_LOAN_CHARGE, MKT_CHARGE_RATE).\
										filter(MKT_CHARGE_RATE.ChargeMode == RegularFee).\
										filter(MKT_LOAN_CHARGE.ID == ID).\
										join(MKT_CHARGE_RATE,MKT_CHARGE_RATE.ID ==MKT_LOAN_CHARGE.ChargeKey+Currency).first()

		if LoanChargeObj:
			ChargeObj 	= LoanChargeObj[0]
			ChargeRate 	= LoanChargeObj[1]
			IncomeCat   = ChargeRate.IncomeCat
			SuspendCat  = ChargeRate.ChargeSuspendCat
			ChargeAmount= Decimal(ChargeObj.ChargeUnearned)

			if ChargeAmount > Zero:
				if Suspend == "Y":
					Note = "Charge income has been moved to suspend."
					Transaction = AccSetting.SuspenseBookingTran
				else:
					Note = "Charge income has been moved from suspend."
					Transaction = AccSetting.SuspenseReverseTran
				for i in range(0, 2):
					if i == 0:
						DrCr 		= 	"Dr"
						Mode 		= 	"Direct"
						Category 	= 	IncomeCat if Suspend == "Y" else SuspendCat

					else:
						DrCr 		= 	"Cr"
						Mode 		= 	"Direct"
						Category 	= 	SuspendCat if Suspend == "Y" else IncomeCat

					GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency)
					DateTimeNow = mktdate.getDateTimeNow()
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"System",				# Inputter
						DateTimeNow, 			# Createdon
						"System",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						"",						# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(ChargeAmount), 	# Amount
						"LC",					# Module
						Transaction, 			# Transaction
						TransactionDate, 		# TransactionDate
						Reference, 				# Reference
						Note, 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)
				print ID,Note,'Amount %s'%mktmoney.formatNumber(float(ChargeAmount))
	except Exception as e:
		db.session.rollback()
		raise
	else:
		pass
	finally:
		pass

def setChargeWriteOff(**kwargs):
	try:
		Zero			= Decimal(0)
		Branch 	   		= kwargs['Branch']
		ID 		   		= kwargs['LoanID']
		Currency   		= kwargs['Currency']
		Transaction 	= kwargs['Transaction']
		TransactionDate = kwargs['TransactionDate']
		Suspend 		= kwargs['Suspend']
		Reference 		= kwargs['Reference'] if 'Reference' in kwargs else ID
		RegularFee 		= '2'
		LoanChargeObj 	=	db.session.query(MKT_LOAN_CHARGE, MKT_CHARGE_RATE).\
										filter(MKT_CHARGE_RATE.ChargeMode == RegularFee).\
										filter(MKT_LOAN_CHARGE.ID == ID).\
										join(MKT_CHARGE_RATE,MKT_CHARGE_RATE.ID ==MKT_LOAN_CHARGE.ChargeKey+Currency).first()

		if LoanChargeObj:

			ChargeObj 	= LoanChargeObj[0]
			ChargeRate 	= LoanChargeObj[1]
			IncomeCat   = ChargeRate.IncomeCat
			ReceiveCat	= ChargeRate.ReceiveCat
			SuspendCat  = ChargeRate.ChargeSuspendCat
			ChargeAmount= Decimal(ChargeObj.ChargeUnearned)

			if ChargeAmount > Zero:
				
				Note = "Write Off charge"
					
				for i in range(0, 2):
					if i == 0:
						DrCr 		= 	"Dr"
						Mode 		= 	"Direct"
						Category 	= 	SuspendCat if Suspend == "Y" else IncomeCat

					else:
						DrCr 		= 	"Cr"
						Mode 		= 	"Direct"
						Category 	= 	ReceiveCat

					GL_KEYS 	= mktaccounting.getConsolKey(Category, Currency)
					DateTimeNow = mktdate.getDateTimeNow()
					mktaccounting.postAccounting(
						"AUTH", 				# Status
						"0", 					# Curr
						"System",				# Inputter
						DateTimeNow, 			# Createdon
						"System",				# Authorizer
						DateTimeNow,			# Authorizeon
						"", 					# AEID
						"",						# Account
						Category,				# Category
						Currency,				# Currency
						DrCr,					# DrCr
						Decimal(ChargeAmount), 	# Amount
						"LC",					# Module
						Transaction, 			# Transaction
						TransactionDate, 		# TransactionDate
						Reference, 				# Reference
						Note, 					# Note
						"", 					# JNID
						Branch,					# Branch
						GL_KEYS,				# GL_KEYS
						Mode 					# Mode check to insert Journal for category
					)
				print ID,Note,'Amount %s'%mktmoney.formatNumber(float(ChargeAmount))
	except Exception as e:
		db.session.rollback()
		raise
	else:
		pass
	finally:
		pass