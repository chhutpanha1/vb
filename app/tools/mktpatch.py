'''
Morakot VB

License
This program is protected by copyright law and international treaties. 
Unauthorized reproduction or distribution of this program, 
or any portions of it, may result in severe civil and criminal penalties, 
and will be prosecuted to the maximum extent possible under the law. 

Created Date: 19 Nov 2015
Author 		: Sovankiry Rim
Facebook	: facebook.com/rimsovankiry
Email 		: rimsovankiry@gmail.com

2014-2015 Morakot Technology Co.,Ltd. All Right Reserved. Liceinse and Warranty
'''
# from app.mktcore.imports import *
from app.info import __Version__,__Build__,__Release__
from app.config import *
import app.tools.mkttool	as mkttool
import collections
import os
import shutil
import zipfile
try:
	import pysftp
except Exception, e:
	print "sftp not configuration. Please run command: pip install pysftp"


#Create Temp Direcotry
TmpPath = '/tmp/morakot-update/'

def getVersion():
	return __Version__

def getBuild():
	return __Build__
	
def getInfo():
	Information = "Morakot VB, Version:%s, Build:%s, Release Date:%s"%(__Version__,__Build__,__Release__)
	return Information

def checkUpdate():
	CurrentBuild 	= getBuild()
	DicUpdatePackage = {}
	print "Checking update ..."
	print "Please wait a few minutes ..."
	if setDefaultTmp():
		
		# Download patch 
		ObjDownload = setDownload("patch.list")
		if ObjDownload:
			
			#Read file patch.list 
			ListPatch = str(mkttool.getReadFile(TmpPath+"patch.list")).splitlines()
			ListPatch = filter(None, ListPatch) # Remove Empty
			ListPatch.sort()

			DicBuild = {}
			for row in ListPatch:
				if row.strip()[:1] != "#": #exclude command line #
					DicBuild.update({int(row.split()[0]):row.split()[1]})
			DicBuild = collections.OrderedDict(sorted(DicBuild.items()))
		
			# Get List of update
			Opearation = False
			for key,value in DicBuild.iteritems():
				key = str(key)
				if Opearation:
					DicUpdatePackage.update({int(key):value})
				if key == CurrentBuild:
					Opearation = True
			#After check which patch to update need to order by patch
			DicBuild = collections.OrderedDict(sorted(DicBuild.items()))

	return DicUpdatePackage

def setUpdate():
	try:
		print ""
		
		CurrentVersion 	=  getVersion()
		
		mkttool.setChangeLog("INFO","Checking update")
		DicUpdatePackage = checkUpdate()
		if DicUpdatePackage:

			mkttool.setChangeLog("INFO","Current Version:%s"%CurrentVersion)
			mkttool.setChangeLog("INFO","Current Build:%s"%getBuild())
			SizeUpdate = mkttool.formatSize(getSizeUpdate(DicUpdatePackage)['TotalSize'])
			print "%s upgraded,Need to get %s of archives ..."%(len(DicUpdatePackage),SizeUpdate)
			mkttool.setChangeLog("INFO","%s upgraded,Need to get %s of archives."%(len(DicUpdatePackage),SizeUpdate))
			print ""
			print "Reading package list ..."
			print "Preconfiguring package ..."

			for key,value in DicUpdatePackage.iteritems():
				
				if setDownload("%s.zip"%value):
					zfile = zipfile.ZipFile('%s%s.zip'%(TmpPath,value))
					print "Preparing to unpack ..."
					zfile.extractall(TmpPath)
					PatchUrl = "%s%s/" %(TmpPath,value)
					
					print "Unpacking Morakot VB patch %s ..."%value
					print "Updating Morakot VB patch %s... "%value
					#Write to Log 
					mkttool.setChangeLog("INFO","Unpacking Morakot VB patch %s ..."%value)
					mkttool.setChangeLog("INFO","Updating Morakot VB patch %s... "%value)

					ListFileUpdate = mkttool.getReadFile("%s/file.list"%PatchUrl).splitlines()
					ListFileUpdate = filter(None,ListFileUpdate)
					#Block working on patch zip
					for text in ListFileUpdate:
						if text.strip()[:1] != "#":
							ListSyncFile = text.split()
							# print ListSyncFile
							SourcePath 		= "%s%s"%(PatchUrl,ListSyncFile[0])
							DestinationPath = "%s"% ListSyncFile[1]
							#Block Copy File
							ObjCopyFile = mkttool.setCopyFile(SourcePath,DestinationPath)
							if ObjCopyFile[0]:
								print "sending incremental file list %s done 100%%."%ListSyncFile[0]
								#Write to Log 
								mkttool.setChangeLog("INFO","File change %s"%DestinationPath)
							else:
								mkttool.setChangeLog("Error","Update file %s"%ObjCopyFile[1])
								return False,ObjCopyFile[1]
							
				# Run Script SQL
				print "Updating Database."

				ListSQLText = mkttool.getReadFile("%s/script.sql"%PatchUrl).splitlines()
				ListSQLText = filter(None,ListSQLText)
				for row in ListSQLText:
					if row.strip()[:1] != "#":
						mkttool.setRunSQL("%s"%row)
						print row
						#Write to Log 
						mkttool.setChangeLog("INFO", row +" was updated succesfully.")

				#Block Update Version
				ObjUpdateVersion = updateVersion(PatchUrl)
				if not ObjUpdateVersion[0] :
					return False,ObjUpdateVersion[1]

			clearUpdate(TmpPath)	
			print "Cleaning up..."
			print "Total Patch: %s upgraded."%len(DicUpdatePackage)
			Msg =  "Morakot VB was updated succesfully."
			print Msg
			mkttool.setChangeLog("INFO",Msg)
			return True,""
		else:
			print "Already up-to-date."
			mkttool.setChangeLog("INFO","Already up-to-date.")
			clearUpdate(TmpPath)
			return True,""

	except Exception, e:
		
		mkttool.setChangeLog("Error","%s"%e)
		return False,"Error: %s"%e

def updateVersion(PatchUrl):
	try:
		# Block Update Version
		NewInfo = mkttool.getReadFile("%s/patch.info"%PatchUrl)
		ObjUpdateVersion = mkttool.setWriteFile("%sapp/info.py"%VB_PATH,NewInfo)
		if not ObjUpdateVersion:
			Msg= "Error update version."
			print Msg
			mkttool.setChangeLog("Error",Msg)
			return False,Msg

		#Write to Log 
		mkttool.setChangeLog("INFO","Patch Version %s"%NewInfo.splitlines()[0])
		mkttool.setChangeLog("INFO","Patch Build %s"%NewInfo.splitlines()[1])
		return True,""
	except Exception, e:
		return False,"Error update version: %s"%e

def setDownload(File):
#This pysftp method is an abstraction above get() that allows you to copy all the files in a remote directory to a local path.
#Reference : http://pysftp.readthedocs.org/en/release_0.2.8/cookbook.html
	try:
		Hostname 		= PATCH_ADDRESS.split()[0]
		UserName 		= PATCH_ADDRESS.split()[1]
		Password 		= PATCH_ADDRESS.split()[2]
		# Download patch
		with pysftp.Connection(Hostname, username=UserName, password=Password) as sftp:
			os.chdir(TmpPath) # First cd to the "/tmp/morakot-update/" local directory
			sftp.get(File,preserve_mtime=True)# get file from remote server
		return True

	except Exception, e:
		mkttool.setChangeLog("Error","setDownload: %s"%e)
		raise

def setDefaultTmp():
	try:
		if not mkttool.isExistDirectory(TmpPath):
			os.makedirs(TmpPath)
			# print "Directory was created."
		return True,""
	except Exception, e:
		mkttool.setChangeLog("Error","%s"%e)
		return False,e

def clearUpdate(TmpPath):
	shutil.rmtree(TmpPath)
	return True

def getSizeUpdate(DicUpdatePackage={}):
	DicSize ={}
	Hostname 		= PATCH_ADDRESS.split()[0]
	UserName 		= PATCH_ADDRESS.split()[1]
	Password 		= PATCH_ADDRESS.split()[2]
	TotalSize 		= 0
	if DicUpdatePackage:
		ListFileUpdate = []
		for key,value in DicUpdatePackage.iteritems():
			ListFileUpdate.append(str('%s.zip'%value))
		# Connection patch
		with pysftp.Connection(Hostname, username=UserName, password=Password) as sftp:
			for attr  in sftp.listdir_attr():
				if attr.filename in ListFileUpdate:
					TotalSize+=float(attr.st_size)
					DicSize.update({str(attr.filename):attr.st_size})
					DicSize.update({'TotalSize':TotalSize})
	return DicSize