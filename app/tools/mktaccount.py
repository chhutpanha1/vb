
from flask 							import session
from .. 							import app, db
from app.mktcore.imports 			import *
from app.Account.models 			import MKT_ACCOUNT, MKT_ACCOUNT_INAU
from app.Currency.models 			import MKT_CURRENCY

from decimal 						import *
from sqlalchemy 					import or_, func

import app.tools.mktmoney 			as mktmoney
import app.tools.mktparam 			as mktparam
import app.tools.user 				as mktuser
import app.tools.mkttool 			as mkttool
import mktsetting 					as mktsetting
import mktaccounting 				as mktaccounting
import mktdate 						as mktdate
import mktaudit 					as mktaudit
import mktteller 					as mktteller
import app.mktcore.autoid 			as mktautoid
import mktdb 						as mktdb

def getSearchAccount():

	DefaultBranch 	= mktuser.getCurrentBranch()
	search 			= request.args.get('q')
	action 			= request.args.get('action') if 'action' in request.args else ""
	NAMES 			= []
	FilterCondition = []
	#check if search text all in space
	if not search.isspace():
		search = search.strip()
		if not search is None:
			if action == "view":
				FilterCondition.append(MKT_ACCOUNT.ID == search)
			else:
				FilterCondition.append(or_(MKT_ACCOUNT.ID.like('%'+search.upper()+'%'),func.upper(MKT_ACCOUNT.AccName).like('%'+search.upper()+'%')))
				FilterCondition.append(MKT_ACCOUNT.AccStatus != "C")
				FilterCondition.append(MKT_ACCOUNT.Branch==DefaultBranch)

			Acc = 	MKT_ACCOUNT.query.filter(*FilterCondition).all()

			for row in Acc:
				dic = {"id":row.ID, "text":"%s - %s - %s%%"%(row.ID,row.AccName,row.InterestRate)}
				NAMES.append(dic)

	# app.logger.debug(NAMES)
	return jsonify(items = NAMES)

def checkMaxMinBalance(AccID=None):
	Acc = MKT_ACCOUNT.query.get(AccID)
	if Acc:
		AccProductID = str(Acc.AccProduct)
		AccProduct = MKT_ACC_PRODUCT.query.get(AccProductID)
		if AccProduct:
			AccRuleID = str(AccProduct.Rule) + str(Acc.Currency)
			AccRule = MKT_ACC_RULE_DE.query.get(AccRuleID)
			if AccRule:
				return AccRule
	else:
		return False

def checkMaxMinDebitAmountunt(DrAccount, Currency, Amount):
	try:
		BalDebit = MKT_ACCOUNT.query.get(DrAccount)
		CurrencyObj = MKT_CURRENCY.query.get(Currency)

		if BalDebit:
			if Decimal(BalDebit.Balance) == 0:
				flash("Debit account#%s is %s" %(DrAccount, mktmoney.toMoney(float(BalDebit.Balance), CurrencyObj, 2)))
			elif Decimal(BalDebit.Balance) < Amount:
				flash("The amount must be smaller than debit account#%s balance %s" %(DrAccount, mktmoney.toMoney(float(BalDebit.Balance), CurrencyObj, 2)))
			else:
				Rule = checkMaxMinBalance(DrAccount)
				if Rule:
					if Decimal(BalDebit.Balance) - Amount < Decimal(Rule.MinBalance):
						flash("Debit account#%s reaches minimum balance %s" %(DrAccount, mktmoney.toMoney(float(Rule.MinBalance), CurrencyObj, 2)))
	except:
		raise

def checkMaxMinCreditAmountunt(CrAccount, Currency, Amount):
	try:
		Acc = MKT_ACCOUNT.query.get(CrAccount)
		CurrencyObj = MKT_CURRENCY.query.get(Currency)

		MinB = ""
		MaxB = ""

		if Acc:
			AccProductID = str(Acc.AccProduct)
			AccProduct = MKT_ACC_PRODUCT.query.get(AccProductID)
			if AccProduct:
				AccRuleID = str(AccProduct.Rule) + str(Acc.Currency)
				AccRule = MKT_ACC_RULE_DE.query.get(AccRuleID)
				if AccRule:
					MinB = Decimal(AccRule.MinBalance)
					MaxB = Decimal(AccRule.MaxBalance)
					if Amount + Decimal(Acc.Balance) < MinB:
						flash("Credit account#%s required minimum balance %s" %(CrAccount, mktmoney.toMoney(float(MinB), CurrencyObj, 2)))
					if Amount + Decimal(Acc.Balance) > MaxB:
						flash("Credit account#%s reaches maximum balance %s" %(CrAccount, mktmoney.toMoney(float(MaxB), CurrencyObj, 2)))
	except:
		raise

def getSearchTillAccount():
	TellerParam 	= mktparam.getTellerParam()
	TillAccPro 		= ""
	VaultAccPro 	= ""
	DefaultBranch 	= mktuser.getCurrentBranch()
	if TellerParam:
		VaultAccPro = TellerParam.VaultAccPro
		TillAccPro  = TellerParam.TillAccPro
	
	search 	= request.args.get('q')
	action 	= request.args.get('action') if 'action' in request.args else ""
	NAMES 	= []
	FilterCondition = []
	
	#check if search text all in space
	if not search.isspace():
		search = search.strip()
		if not search is None:
			
			if action == "view":
				FilterCondition.append(MKT_ACCOUNT.ID == search)
			else:
				FilterCondition.append(or_(MKT_ACCOUNT.ID.like('%'+search.upper()+'%'),func.upper(MKT_ACCOUNT.AccName).like('%'+search.upper()+'%')))

			if 'TELLER' in session and not action:
				if session['TELLER'] == 'TT':
					# print "in session teller"
					Acc = MKT_ACCOUNT.query.filter(*FilterCondition).\
												filter(MKT_ACCOUNT.AccProduct==TillAccPro).\
												filter(MKT_ACCOUNT.AccStatus != "C").\
												filter(MKT_ACCOUNT.Branch==DefaultBranch).\
												all()
				else:
					Acc = 	MKT_ACCOUNT.query.filter(*FilterCondition).\
												filter(MKT_ACCOUNT.AccStatus != "C").\
												filter(MKT_ACCOUNT.Branch==DefaultBranch).\
												all()
					# print "not session teller"
			else:
				
				Acc = 	MKT_ACCOUNT.query.filter(*FilterCondition).\
												filter(MKT_ACCOUNT.AccStatus != "C").\
												filter(MKT_ACCOUNT.Branch==DefaultBranch).\
												all()
				
			for row in Acc:
				dic = {"id":row.ID, "text":row.ID+" - "+row.AccName}
				NAMES.append(dic)

	# app.logger.debug(search)
	return jsonify(items = NAMES)

def updateAccountBranch(ID,Branch):
	try:
		UpdateBranch = MKT_ACCOUNT.query.get(ID)
		if UpdateBranch:
			UpdateBranch.Branch = Branch
			db.session.add(UpdateBranch)
			
	except:
		db.session.rollback()
		raise
		return False

def getAccount(ID):

	Acc=[]
	QueryObj 	= 	MKT_ACCOUNT.query.get(ID)
	if QueryObj :
		Acc = QueryObj
	return Acc
	
#Shortage -
def getShortageAccount(Currency='',Branch=''):
	TellerParam = mktparam.getTellerParam()
	Acc=''
	if TellerParam:
		Acc 	= 	MKT_ACCOUNT.query.filter_by(Currency 	= 	Currency,
												AccProduct 	= 	TellerParam.ShortageAccPro,
												Branch 		= 	Branch ).\
												first()

	return Acc

#Surplus +
def getSurplusAccount(Currency='',Branch=''):
	TellerParam = mktparam.getTellerParam()
	Acc=''
	if TellerParam:
		Acc 	= 	MKT_ACCOUNT.query.filter_by(Currency 	= 	Currency,
												AccProduct 	= 	TellerParam.SurplusAccPro,
												Branch 		= 	Branch ).\
												first()

	return Acc

def getSuspendAccount(Currency='',Branch=''):
	TellerParam = mktparam.getTellerParam()
	Acc=''
	if TellerParam:
		Acc 	= 	MKT_ACCOUNT.query.filter_by(Currency 	= 	Currency,
												AccProduct 	= 	TellerParam.SuspendAccPro,
												Branch 		= 	Branch ).\
												first()

	return Acc

def isSuspendAccount():
	CurrentBranch 	= mktuser.getCurrentBranch()
	TellerParam 	= mktparam.getTellerParam()
	if TellerParam:
		SuspendAccPro = TellerParam.SuspendAccPro
		if not SuspendAccPro:
			return False,"Please configuration Teller Parameter first."
		else:
			Acc 	= 	MKT_ACCOUNT.query.filter_by(
												AccProduct 	= 	TellerParam.SuspendAccPro,
												Branch 		= 	CurrentBranch ).\
												first()
			if not Acc:
				return False,"Please create Suspend account first."
			else:
				del Acc
				return True, "Pass"

	else:
		return False,"Please configuration Teller Parameter first."

def checkInterestRate(Mod, ID, InputRate, CurrencyKey):
	
	if Mod == "AC":
		Result = MKT_ACC_PRODUCT.query.get(ID)

	else:
		Result = MKT_LOAN_PRODUCT.query.get(ID)

	Msg = None
	if Result:

		IntKey = Result.InterestKey
		IntKey = IntKey + str(CurrencyKey)
		getIntKey = MKT_INTEREST_RATE.query.get(IntKey)
		if getIntKey:
			getRate = getIntKey.Rate
			getRate = getRate.split()
			NumofRate = len(getRate)

			if NumofRate == 2:
				# InputRate = InputRate.split()
				if float(InputRate) < float(getRate[0]) or float(InputRate) > float(getRate[1]):
					Msg = "Rate value must between %s to %s." %(getRate[0], getRate[1])

	return Msg

def setCloseAccount(ID, UserID="", Resource="INAU"):
	try:

		FWID 	=	mktautoid.getAutoID('FRM_FUND_WITHDRAWAL')
		# Check 	= 	mktteller.isUserVaultAccount()

		# if not Check[0]:
		# 	return [False, "%s" %Check[1]]

		Audit 			= 	mktaudit.getAuditrail()
		Inputter		= 	Audit['Inputter']
		Createdon 		= 	Audit['Createdon']
		Authorizer 		= 	Audit['Authorizer']
		Authorizeon		= 	Audit['Authorizeon']
		Setting 		=	mktsetting.getAccSetting()

		# if Resource == 'AUTH':

		AccObj 	=	db.session.query(
						MKT_ACCOUNT.ID,
						MKT_ACCOUNT.CustomerList,
						MKT_ACCOUNT.Currency,
						MKT_ACCOUNT.AccrCurMonth,
						MKT_ACCOUNT.AccrCurCapital,
						MKT_ACCOUNT.AccrIntBooked,
						MKT_ACCOUNT.Balance,
						MKT_ACCOUNT.AccCategory,
						MKT_ACCOUNT.Branch,
						MKT_ACCOUNT.Tax,
						MKT_ACCOUNT.AccProduct,
						MKT_ACC_PRODUCT.ClosingWithAccr,
						MKT_ACC_PRODUCT.IntExpenseCat,
						MKT_ACC_PRODUCT.IntPayableCat
					).\
					join(
						MKT_ACC_PRODUCT,
						MKT_ACC_PRODUCT.ID == MKT_ACCOUNT.AccProduct
					).\
					filter(MKT_ACCOUNT.ID == ID).\
					first()

		# else:

		# 	AccObj 	=	db.session.query(
		# 				MKT_ACCOUNT_INAU.ID,
		# 				MKT_ACCOUNT_INAU.CustomerList,
		# 				MKT_ACCOUNT_INAU.Currency,
		# 				MKT_ACCOUNT_INAU.AccrCurMonth,
		# 				MKT_ACCOUNT_INAU.AccrCurCapital,
		# 				MKT_ACCOUNT_INAU.Balance,
		# 				MKT_ACCOUNT_INAU.AccCategory,
		# 				MKT_ACCOUNT_INAU.Branch,
		# 				MKT_ACCOUNT_INAU.Tax,
		# 				MKT_ACCOUNT_INAU.AccProduct,
		# 				MKT_ACC_PRODUCT.ClosingWithAccr,
		# 				MKT_ACC_PRODUCT.IntExpenseCat,
		# 				MKT_ACC_PRODUCT.IntPayableCat
		# 			).\
		# 			join(
		# 				MKT_ACC_PRODUCT,
		# 				MKT_ACC_PRODUCT.ID == MKT_ACCOUNT_INAU.AccProduct
		# 			).\
		# 			filter(MKT_ACCOUNT_INAU.ID == ID).\
		# 			first()
		if not AccObj:
			return False, "Account#%s not found." %ID

		Account 		=	AccObj.ID
		ClosingAccTran 	=	Setting.CloseAccTran if Setting else ""
		Currency 		=	AccObj.Currency
		AccrCurMonth 	=	Decimal(AccObj.AccrCurMonth) if AccObj.AccrCurMonth else Decimal(0)
		AccrIntBooked 	=	Decimal(AccObj.AccrIntBooked) if AccObj.AccrIntBooked else Decimal(0)
		AccrCurCapital 	=	Decimal(AccObj.AccrCurCapital) if AccObj.AccrCurCapital else Decimal(0)
		Balance 		=	Decimal(AccObj.Balance) if AccObj.Balance else Decimal(0)
		AccCategory 	=	AccObj.AccCategory
		Branch 			=	AccObj.Branch
		Tax 			=	AccObj.Tax
		CustomerID 		=	AccObj.CustomerList
		ClosingWithAccr =	AccObj.ClosingWithAccr
		IntExpenseCat 	=	AccObj.IntExpenseCat
		IntPayableCat 	=	AccObj.IntPayableCat
		AccrCurMonthAmt =	AccrCurMonth

		# if ClosingWithAccr == 'Y':
		# 	Amount 			=	AccrCurMonth + AccrCurCapital
		# else:
		# 	Amount 			=	AccrCurCapital
		# 	AccrCurMonthAmt =	Decimal(0)

		# Debit Exspense, Debit Payables, Credit Client's A/C
		GL_KEYS 	= 	mktaccounting.getConsolKey(IntExpenseCat, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
		DateTimeNow = 	mktdate.getDateTimeNow()
		Transaction = 	ClosingAccTran
		TranDate 	= 	str(mktdate.getBankDate())
		Mode 		= 	"Direct"
		DrCr 		= 	"Dr"

		IntExpAmt 	=	Decimal(AccrCurCapital) - Decimal(AccrIntBooked)
		# Debit Int Exspense
		mktaccounting.postAccounting(
			"AUTH", 				# Status
			"0", 					# Curr
			Inputter,				# Inputter
			Createdon, 				# Createdon
			Authorizer,				# Authorizer
			Authorizeon,			# Authorizeon
			"", 					# AEID
			"",						# Account
			IntExpenseCat,			# Category
			Currency,				# Currency
			DrCr,					# DrCr
			Decimal(IntExpAmt), 	# Amount
			"AC",					# Module
			Transaction, 			# Transaction
			TranDate, 				# TransactionDate
			ID, 					# Reference
			"", 					# Note
			"", 					# JNID
			Branch,					# Branch
			GL_KEYS,				# GL_KEYS
			Mode, 					# Mode check to insert Journal for category
			""
		)

		GL_KEYS 	= 	mktaccounting.getConsolKey(IntPayableCat, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
		DateTimeNow = 	mktdate.getDateTimeNow()
		Mode 		= 	"Direct"
		DrCr 		= 	"Dr"

		# Debit Int Payables
		mktaccounting.postAccounting(
			"AUTH", 				# Status
			"0", 					# Curr
			Inputter,				# Inputter
			Createdon, 				# Createdon
			Authorizer,				# Authorizer
			Authorizeon,			# Authorizeon
			"", 					# AEID
			"",						# Account
			IntPayableCat,			# Category
			Currency,				# Currency
			DrCr,					# DrCr
			Decimal(AccrIntBooked), 	# Amount
			"AC",					# Module
			Transaction, 			# Transaction
			TranDate, 				# TransactionDate
			ID, 					# Reference
			"", 					# Note
			"", 					# JNID
			Branch,					# Branch
			GL_KEYS,				# GL_KEYS
			Mode, 					# Mode check to insert Journal for category
			""
		)

		GL_KEYS 	= 	mktaccounting.getConsolKey(AccCategory, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
		DateTimeNow = 	mktdate.getDateTimeNow()
		Mode 		= 	""
		DrCr 		= 	"Cr"

		# Credit Client's A/C
		mktaccounting.postAccounting(
			"AUTH", 				# Status
			"0", 					# Curr
			Inputter,				# Inputter
			Createdon, 				# Createdon
			Authorizer,				# Authorizer
			Authorizeon,			# Authorizeon
			"", 					# AEID
			Account,				# Account
			AccCategory,			# Category
			Currency,				# Currency
			DrCr,					# DrCr
			Decimal(AccrCurCapital), 	# Amount
			"AC",					# Module
			Transaction, 			# Transaction
			TranDate, 				# TransactionDate
			ID, 					# Reference
			"", 					# Note
			"", 					# JNID
			Branch,					# Branch
			GL_KEYS,				# GL_KEYS
			Mode, 					# Mode check to insert Journal for category
			"Yes"
		)

		# Calculate tax for saving account
		TaxObj 		= MKT_TAX.query.get(Tax)
		TaxRate 	= 0
		TaxCategory = ""
		TaxPayment 	= 0

		if TaxObj:

			TaxRate 	= TaxObj.Rate
			TaxCategory = TaxObj.Category
			TaxPayment 	= (float(AccrCurCapital) * float(TaxRate)) / float(100)
			Transaction = Setting.TaxTran if Setting else ""

			GL_KEYS 	= mktaccounting.getConsolKey(AccCategory, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
			DateTimeNow = mktdate.getDateTimeNow()
			Mode 		= ""
			DrCr 		= "Dr"

			# Debit Client's A/C
			mktaccounting.postAccounting(
				"AUTH", 				# Status
				"0", 					# Curr
				Inputter,				# Inputter
				Createdon, 				# Createdon
				Authorizer,				# Authorizer
				Authorizeon,			# Authorizeon
				"", 					# AEID
				Account,				# Account
				AccCategory,			# Category
				Currency,				# Currency
				DrCr,					# DrCr
				Decimal(TaxPayment), 	# Amount
				"AC",					# Module
				Transaction, 			# Transaction
				TranDate, 				# TransactionDate
				ID, 					# Reference
				"", 					# Note
				"", 					# JNID
				Branch,					# Branch
				GL_KEYS,				# GL_KEYS
				Mode, 					# Mode check to insert Journal for category
				"Yes"
			)

			GL_KEYS 	= mktaccounting.getConsolKey(TaxCategory, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
			DateTimeNow = mktdate.getDateTimeNow()
			Mode 		= "Direct"
			DrCr 		= "Cr"

			# Credit Tax Payables Category
			mktaccounting.postAccounting(
				"AUTH", 				# Status
				"0", 					# Curr
				Inputter,				# Inputter
				Createdon, 				# Createdon
				Authorizer,				# Authorizer
				Authorizeon,			# Authorizeon
				"", 					# AEID
				"",						# Account
				TaxCategory,			# Category
				Currency,				# Currency
				DrCr,					# DrCr
				Decimal(TaxPayment), 	# Amount
				"AC",					# Module
				Transaction, 			# Transaction
				TranDate, 				# TransactionDate
				ID, 					# Reference
				"", 					# Note
				"", 					# JNID
				Branch,					# Branch
				GL_KEYS,				# GL_KEYS
				Mode, 					# Mode check to insert Journal for category
				""
			)

		# Make withdrawal from client's A/C
		NewObj 	=	MKT_ACCOUNT.query.get(ID)
		if not NewObj:
			return False, "Account#%s not found." %ID

		Amount 		=	Decimal(NewObj.Balance) if NewObj.Balance else Decimal(0)

		DrAcc 		=	NewObj.ID
		DrCat 		=	NewObj.AccCategory
		TellerParam =	mktparam.getTellerParam()
		VaultObj 	=	mktuser.getVaultInfo(Currency, UserID)
		if not VaultObj:
			return False, "User don't have vault account." %ID

		# CrAcc 		=	"KHR10200000004"
		# CrCat 		=	"10200"
		CrAcc 		=	VaultObj.ID
		CrCat 		=	TellerParam.VaultCategory if TellerParam else ""

		if not CrCat:
			return False, "Vault category not found." %ID

		Tran 		=	Setting.CloseAccTran if Setting else ""
		Ref 		=	ID
		Note 		=	"Closed account %s." %ID
		# Add new record to fund withdrawal(MKT_FUND_TRANSFER)
		if Decimal(Amount) > 0:
			TWObj 		= 	setTTNewRecord(FWID, Amount, Branch, DrAcc, DrCat, CrAcc, CrCat, Currency, Tran, TranDate, Ref, Note, Inputter, Createdon, Authorizer, Authorizeon)

		# Post transaction fund withdrawal
		GL_KEYS 	= mktaccounting.getConsolKey(DrCat, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
		DateTimeNow = mktdate.getDateTimeNow()
		Mode 		= ""
		DrCr 		= "Dr"

		# Debit Client's A/C
		mktaccounting.postAccounting(
			"AUTH", 				# Status
			"0", 					# Curr
			Inputter,				# Inputter
			Createdon, 				# Createdon
			Authorizer,				# Authorizer
			Authorizeon,			# Authorizeon
			"", 					# AEID
			DrAcc,					# Account
			DrCat,					# Category
			Currency,				# Currency
			DrCr,					# DrCr
			Decimal(Amount), 		# Amount
			"FW",					# Module
			Tran, 					# Transaction
			TranDate, 				# TransactionDate
			FWID, 					# Reference
			"", 					# Note
			"", 					# JNID
			Branch,					# Branch
			GL_KEYS,				# GL_KEYS
			Mode, 					# Mode check to insert Journal for category
			"Yes"
		)

		GL_KEYS 	= mktaccounting.getConsolKey(CrCat, Currency, "", "AC", "", "", "", "", "", "", "", "", "")
		DateTimeNow = mktdate.getDateTimeNow()
		Mode 		= ""
		DrCr 		= "Cr"

		# Credit Vault's A/C
		mktaccounting.postAccounting(
			"AUTH", 				# Status
			"0", 					# Curr
			Inputter,				# Inputter
			Createdon, 				# Createdon
			Authorizer,				# Authorizer
			Authorizeon,			# Authorizeon
			"", 					# AEID
			CrAcc,					# Account
			CrCat,					# Category
			Currency,				# Currency
			DrCr,					# DrCr
			Decimal(Amount), 		# Amount
			"FW",					# Module
			Tran, 					# Transaction
			TranDate, 				# TransactionDate
			FWID, 					# Reference
			"", 					# Note
			"", 					# JNID
			Branch,					# Branch
			GL_KEYS,				# GL_KEYS
			Mode, 					# Mode check to insert Journal for category
			"Yes"
		)

		# print "Balance %s" %Amount
		# print "Fund Withdrawal %s" %FWID
		# print "Dr Acc %s" %DrAcc
		# print "Finish close"
		# Update account
		NewObj.AccrInterest 	=	0
		NewObj.AccrCurMonth 	=	0
		NewObj.AccrCurCapital 	=	0
		# NewObj.Balance 			=	0
		# NewObj.AvailableBal 	=	0

		db.session.add(NewObj)

		return True, ""

	except Exception, e:
		db.session.rollback()
		return [False, "%s" %e]

def setTTNewRecord(ID, Amount, Branch, DrAcc, DrCat, CrAcc, CrCat, Currency, Tran, TranDate, Ref, Note, Inputter, Createdon, Authorizer, Authorizeon):
	try:
		# Insert record to Fund Transfer
		InsertRecord = {	
							'Inputter'		:Inputter,
							'Createdon'		:Createdon,
							'Authorizer'	:Authorizer,
							'Authorizeon'	:Authorizeon,
							'Branch'		:Branch,
							'ID'			:ID,
							'DrAccount'		:DrAcc,
							'DrCategory'	:DrCat,
							'DrCurrency'	:Currency,
							'CrAccount'		:CrAcc,
							'CrCategory'	:CrCat,
							'CrCurrency' 	:Currency,
							'Amount'		:Amount,
							'Transaction'	:Tran,
							'TranDate'		:TranDate,
							'Reference'		:Ref,
							'Note'			:Note
						}
		mktdb.insertTable(MKT_FUND_TRANSFER, InsertRecord)

		return [True, ""]

	except Exception, e:
		return [False, "%s" %e]

def searchProvidentAccount():

	DefaultBranch 	= mktuser.getCurrentBranch()
	search 			= request.args.get('q')
	action 			= request.args.get('action') if 'action' in request.args else ""
	NAMES 			= []
	FilterCondition = []
	#check if search text all in space
	if not search.isspace():
		search = search.strip()
		if not search is None:
			if action == "view":
				FilterCondition.append(MKT_ACCOUNT.ID == search)
			else:
				FilterCondition.append(or_(MKT_ACCOUNT.ID.like('%' + search.upper() + '%'),func.upper(MKT_ACCOUNT.AccName).like('%'+search.upper()+'%')))
				FilterCondition.append(MKT_ACCOUNT.AccStatus != "C")
				FilterCondition.append(MKT_ACCOUNT.Branch==DefaultBranch)

			Acc = 	MKT_ACCOUNT.query.filter(*FilterCondition).all()

			for row in Acc:
				dic = {"id":row.ID, "text":"%s - %s" %(row.ID, row.AccName)}
				NAMES.append(dic)

	# app.logger.debug(NAMES)
	return jsonify(items = NAMES)
