from app.mktcore.wtfimports 	import *
from app.VB.models 				import MKT_VB
from sqlalchemy 				import *

import app.tools.user as mktuser

def getSearchVB():
	search = request.args.get('q')
	Branch = mktuser.getCurrentBranch()
	NAMES = []
	# filter(MKT_VB.Branch == Branch).\
	query = MKT_VB.query.\
			filter(or_(MKT_VB.ID.like('%'+search+'%'), func.upper(MKT_VB.Description).like('%'+search.upper()+'%'))).\
			all()
			
	for row in query:
		dic = {"id":row.ID, "text":row.ID+" - "+row.Description}
		NAMES.append(dic)

	app.logger.debug(NAMES)
	return jsonify(items = NAMES)