from app.mktcore.wtfimports import *
from app.Branch.models import MKT_BRANCH
from sqlalchemy import or_

def getSearchBranch():
	search = request.args.get('q')
	NAMES = []
	query = MKT_BRANCH.query.\
			filter(or_(MKT_BRANCH.ID.like('%'+search.upper()+'%'), MKT_BRANCH.Description.like('%'+search+'%'))).\
			all()
	for row in query:
		dic = {"id":row.ID, "text":row.ID+" - "+row.Description}
		NAMES.append(dic)

	app.logger.debug(NAMES)
	return jsonify(items = NAMES)