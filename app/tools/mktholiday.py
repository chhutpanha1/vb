import mktsetting 	as mktsetting

def getHoliday():
	try:
		
		Holiday = 1
		Setting = mktsetting.getSetting()
		if Setting:
			if str(Setting.WEEKEND_SAT) == 'FALSE' and str(Setting.WEEKEND_SUN) == 'TRUE':
				Holiday = 2
			elif str(Setting.WEEKEND_SAT) == 'TRUE' and str(Setting.WEEKEND_SUN) == 'FALSE':
				Holiday = 3
			elif str(Setting.WEEKEND_SAT) == 'TRUE' and str(Setting.WEEKEND_SUN) == 'TRUE':
				Holiday = 1
			else:
				Holiday = 4

		return Holiday

	except:
		raise