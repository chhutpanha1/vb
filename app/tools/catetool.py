from app.mktcore.wtfimports 	import *

import mktsetting 				as mktsetting

def getCategoryAsSearch():
	
	JOURNAL_ENTRY_EXCLUDED = mktsetting.getExcludedCategories()

	search 	= 	request.args.get('q')
	NAMES 	= 	[]
	Record 	= 	MKT_CATEGORY.query.\
				filter(MKT_CATEGORY.ID.like('%'+search+'%')).\
				filter(~MKT_CATEGORY.ID.in_(JOURNAL_ENTRY_EXCLUDED)).\
				all()
	
	for row in Record:
		dic = {"id":row.ID, "text":row.ID+" - "+row.Description}
		NAMES.append(dic)

	app.logger.debug(NAMES)
	return jsonify(items = NAMES)

def getCategoryAsSearch1():
	
	JOURNAL_ENTRY_EXCLUDED = mktsetting.getExcludedCategories()
	
	search 	= 	request.args.get('q')
	NAMES 	= 	[]
	Record 	= 	MKT_CATEGORY.query.\
				filter(MKT_CATEGORY.ID.like('%'+search+'%')).\
				filter(~MKT_CATEGORY.ID.in_(JOURNAL_ENTRY_EXCLUDED)).\
				all()
	
	for row in Record:
		dic = {"id":row.ID, "text":row.ID+" - "+row.Description}
		NAMES.append(dic)

	app.logger.debug(NAMES)
	return jsonify(items = NAMES)