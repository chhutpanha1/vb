# -*- coding: utf-8 -*-

'''
Created Date: 23 July 2015
Author: Sovankiry Rim

Modified by: Sovankiry
Modified Date: 01 Oct 2015
All Right Reserved Morakot Technology
Description :

'''

import sys
from app.mktcore.imports 	import *
from flask 				import flash,session,request,Response,template_rendered,request_finished,message_flashed
from flask.ext.login 		import LoginManager, login_user, logout_user, current_user, login_required
from app 				import app, db

import app.tools.mktdb 				as mktdb
import app.tools.user 				as mktuser
import app.tools.mktsetting 		as mktsetting

import time
from app.Notification.models 		import *
from app.User.models 				import *
from app.LoginUser.models 			import *


def getConfigNotification():

	# Path : Title
	ConfigNotification = {
			'Branch'				:'Branch'			,
			'Customer'				:'Customer'			,
			'Account'				:'Account'			,
			'LoanContract'			:'Loan Contract'	,
			'LoanAmendment'			:'Loan Amendment'	,
			'LoanApplication'		:'Loan Application'	,
			'JournalEntry'			:'Journal Entry'	,
			'FundTransfer'			:'Fund Transfer'	,
			'FundDeposit'			:'Fund Deposit'		,
			'FundWithdrawal'		:'Fund Withdrawal'	,
			'FundSettlement'		:'Fund Settlement'	,
			'FundAdvance'			:'Fund Advance'		,
			'TillOpen'				:'Till Open'		,
			'TillClose'				:'Till Close'		,
			'TillToTill'			:'Till To Till'		,
			'Deposit'				:'Deposit'			,
			'Withdrawal'			:'Withdrawal'		,
			'Account_SV'			:'Savings Account'	,
			'Account_DD'			:'Drawdown Account' ,
			'JoinAccount'			:'Join Account'		,
			'MultiJournal'			:'Multi Journal'	,
			'IncomePosting'			:'Income Posting'	,
			'ExpensePosting'		:'ExpensePosting'	,
			'IncomeExpenseBooking'	:'Income Expense Booking'}

	return ConfigNotification


def getEventCode():
	# if ur code have unicode 
	# reload(sys)
	# sys.setdefaultencoding('utf-8')

	# Code : Description
	MessageNotification = {
			'000':'was login.'		,
			'001':'was logout.'		,
			'100':'was created.'	,
			'101':'was created and authorized.'	,
			'200':'was authorized.'	,
			'300':'was edited.'		,
			'301':'was edited and authorized.',
			'400':'was viewed.'		,
			'500':'was deleted.'	,
			'600':'was reversed.'	,
			'601':'was reversed and authorized',
			'602':'was reversal authorized'
			}

	return MessageNotification

# Use flask signal event to upload data in db
def log_template_renders(sender, template, context, **extra):
	SettingObj 				= mktsetting.getSetting()
	SettingNotify 			= True if SettingObj.Notification == "TRUE" else False
	if SettingNotify:
		if 'notification' in context:
			if context['notification']:
				Config 	= getConfigNotification()
				# Message = getMessageNotification()
				# print str(context['path']).split('/')[2]
				Path 	= str(context['path']).split('/')[2]  if 'path' in context else context['notification']['Path']
				if Path in Config:

					InsertRecord	= {}
					ID 				= context['notification']['ID'] 
					Path 			= Path
					Title 			= Config[Path]
					Status 			= context['notification']['Status']
					Description 	= context['notification']['Code']
					AppName 		= context['notification']['Model']

					InsertRecord.update({
									'Status'	:Status,
									'GlobalID'	:ID,
									'Path'		:Path,
									'AppName'	:AppName,
									'Title'		:Title,
									'Description':Description,
									'Read'		:'',
									'ReadDetail':''
									})

					mktdb.insertTable(MKT_NOTIFICATION,InsertRecord)
					db.session.commit()
					InsertRecord	= {}
					setPushNotify()
	db.session.close()
	# sender.logger.debug('Rendering template "%s" with context %s',template.name or 'string template',context)

template_rendered.connect(log_template_renders, app)

#Use flask signal to push notification
# @template_rendered.connect_via(app)
# def when_template_rendered(sender, template, context, **extra):
# 	# print 'Template %s is rendered with %s' % (template.name, 'context')
# 	setPushNotify()

def getCode(Status='',NAuthorize=''):
	Code = ""
	if NAuthorize == 0:
		if Status == "":
			Code = "101"

		if Status == "AUTH":
			Code = "301"

	if NAuthorize == 1:

		if Status == "":
			Code = "100"

		if Status == "INAU":
			Code = "300"

		if Status == "AUTH":
			Code = "300"

		if Status == "RNAU":
			Code = "602"

	return Code
def getSignal(ID='',Model='',Status='',Code='',Path=''):
	
	dic={}
	Model=Model.__name__
	dic = {'ID':ID,'Model':Model,'Status':Status,'Code':Code,'Path':Path}

	return dic

#Genarate
def event_stream(Dic):

	yield 'data:%s\n\n' % Dic

def setPushNotify():

	Count 		= '0'
	Check 		= ''
	Dic 		= {}
	if 'getLogInID' in session:

		DefaultBranch 	= mktuser.getCurrentBranch()
		ObjUser 		= mktuser.getUser()
		CurrentUser 	= ObjUser.ID
		MuteNotification= "Y" if ObjUser.Notification == None or ObjUser.Notification == "" else ObjUser.Notification
		Check 			= 'New'
		Notification 	= MKT_NOTIFICATION.query.\
						filter(MKT_NOTIFICATION.Branch==DefaultBranch).\
						filter(MKT_NOTIFICATION.Inputter!=CurrentUser).yield_per(100)
		Count = 0
		for row in Notification:
			Read = '' if  row.Read == None else str(row.Read)
			#if user read already doesn't count
			if not CurrentUser in Read.split():
				Count += 1

		Count = str(Count)
		if 'Notification' in session:
			if Count == session['Notification']:
				Check = 'Old'
		session['Notification']=Count

		Count 				= str(Count)
		Check 				= str(Check)
		MuteNotification 	= str(MuteNotification)
		Dic ={'Count':Count,'Check':Check,'Mute':MuteNotification}
		
		#Clear variable 
		del Notification
		del ObjUser
		del MuteNotification

	return Dic

def insertNotify(notification):
	
	InsertRecord 	= {}
	Config 			= getConfigNotification()

	if notification['Path'] in Config:

		ID 				= notification['ID'] 
		Path 			= notification['Path']
		Title 			= Config[Path]
		Status 			= notification['Status']
		Description 	= notification['Code']
		AppName 		= notification['Model']
		InsertRecord.update({
						'Status'	:Status,
						'GlobalID'	:ID,
						'Path'		:Path,
						'AppName'	:AppName,
						'Title'		:Title,
						'Description':Description,
						'Read'		:'',
						'ReadDetail':''
						})

		mktdb.insertTable(MKT_NOTIFICATION,InsertRecord)
		db.session.commit()
		setPushNotify()
		db.session.close()
		
def setClearNotify(MaxRecord=''):

	BranchObj		= db.session.query(MKT_NOTIFICATION.Branch).\
										group_by(MKT_NOTIFICATION.Branch)
	Notification 	= MKT_NOTIFICATION.query
	UserObj			= MKT_USER.query
	for item in BranchObj:
		# print item.Branch
		# print item.Inputter
		ListUser		= []
		FilterUserObj	= Notification.filter(MKT_NOTIFICATION.Branch==item.Branch)
		for row in FilterUserObj:
			ListUser.append(row.Inputter)
		print ListUser
		QueryObj 	= Notification.filter(MKT_NOTIFICATION.Branch==item.Branch).\
									filter(MKT_NOTIFICATION.Inputter==item.Inputter)
		for row in QueryObj:
			# print row.ID
			# print row.Title
			UserRead	=	 str(row.Read).split()
			Check = 1
			for user in ListUser:
				if user in UserRead:
					Check *= 1
				else:
					Check *=0
			if Check ==1:		
				print '%s is exprie' %row.ID
			else:
				print '%s is not exprie' %row.ID

