'''
Created Date: 07 May 2015
Author: Sovankiry Rim

Modified by: Sovankiry
Modified Date: 06 Nov 2015
All Right Reserved Morakot Technology
Description : Dashboard widget
Notes : 
'''

from app.mktcore.imports 		import *
from app.urlregister 			import *
import app.tools.user 			as mktuser
import app.tools.mktparam 		as mktparam
import app.Dashboard.widget 	as UserWidget
import app.tools.mktmoney 		as mktmoney
import app.tools.mktdb 			as mktdb
import app.tools.mktaudit 		as mktaudit
import mktsetting 				as mktsetting


from app.Currency.models import *


Module = __import__("app.urlregister")


#Only CMI
DDCondition = [MKT_ACCOUNT_INAU.AccProduct == '101']
SVCondition = [MKT_ACCOUNT_INAU.AccProduct == '102']
ACCondition = [~MKT_ACCOUNT_INAU.AccProduct.in_(['101','102'])]

# WIDGET_UNAUTHORIZE (Label,Url,Table,Condition)
WIDGET_UNAUTHORIZE = 	[	
							('Account'				,'Account'			,'MKT_ACCOUNT_INAU' 	,	ACCondition		)
							
						]

# Configuration Widget
def setRegisterSystemWidget():
	# NameofWidget, Label of Widget , Name of Function ,Part and File html, MacroName
	Widget 	=	[	('','--None--'),
					('Unauthorize',	'Unauthorize',	getWidgetUnauthorize,	'dashboard/widget.html','Unauthorize'),
					('ChartLoan',	'Chart Loan',	getLoanProduct,	'dashboard/widget.html','ChartLoan'),
					('CourseByStudent' , 'Course by student' , getCourseByStudent , '/dashboard/widget.html' , 'CourseByStudent')
				]

	return Widget
	
def getListWidget():

	ListWidget 	= []
	# Build-In
	for row in setRegisterSystemWidget():
		ListWidget.append((row[0],row[1]))
	# User-Widget
	for row in UserWidget.setRegisterWidget():
		ListWidget.append((row[0],row[1]))

	return ListWidget

def getWidgetObj():
	WidgetObj 		= []
	# Build-In
	for row in setRegisterSystemWidget():
		WidgetObj.append(row)
	# User-Widget
	for row in UserWidget.setRegisterWidget():
		WidgetObj.append(row)
	return WidgetObj


def getDashboardObj():
	DashboardID 	= mktuser.getUser().Dashboard
	DashboardObj 	= []

	if DashboardID:
		DashboardObj 	= MKT_DASHBOARD_DE.query.\
											filter(MKT_DASHBOARD_DE.ID==DashboardID).\
											order_by(MKT_DASHBOARD_DE.ID.asc(),MKT_DASHBOARD_DE.WGID.asc())
	return DashboardObj


# Widget Unauthorize
def getWidgetUnauthorize():
	# from app.urlregister import *
	
	ListModule 	= []

	Role = mktuser.getUser().Role
	DefaultBranch 	= mktuser.getBranch(session["ChangeBranch"]).ID
	for item in WIDGET_UNAUTHORIZE:

		Label 	= item[0]
		Url 	= item[1]
		Table 	= getattr(Module,item[2])
		if Role == "99":
			QueryObj = db.session.query(func.count(Table.ID).label('Count')).yield_per(100)
		else:
			QueryObj = db.session.query(func.count(Table.ID).label('Count')).\
										filter(Table.Branch == DefaultBranch).yield_per(100)
									
		if len(item)==4:
			Condition=item[3]
			QueryObj = QueryObj.filter(*Condition).yield_per(100)
		 	
		# for row in QueryObj:
		# 	ListModule.append([Label,Url,item[2],row.Count])
		
		if QueryObj.first().Count > 0:
			ListModule.append([Label,Url,item[2],QueryObj.first().Count])

	#Clear variable 
	del QueryObj
	return ListModule

# Widget Chart Loan Type
def getLoanProduct():

	QueryObj 		= []
	DicObj 			= {}

	LoanContract 	= MKT_LOAN_CONTRACT.query.all()

	LoanProduct 	= db.session.query(MKT_LOAN_PRODUCT.ID,
									MKT_LOAN_PRODUCT.Description,
									func.count(MKT_LOAN_CONTRACT.ID).label('Count')).\
									join(MKT_LOAN_CONTRACT,MKT_LOAN_CONTRACT.LoanProduct==MKT_LOAN_PRODUCT.ID).\
									group_by(MKT_LOAN_PRODUCT.ID,MKT_LOAN_PRODUCT.Description)

	if LoanProduct:
		TotalContract = len(LoanContract)
		for row in LoanProduct:
			Percent = int(row.Count * 100 / TotalContract)
			QueryObj.append((row.Description,Percent))
			
	#Clear variable 
	del LoanProduct

	return QueryObj



def getWidgetCashPosition():
	
	UserObj 	= mktuser.getUser()
	AccObj 		= []
	ListAccount = []
	if UserObj:
		CurrentBranch 	= mktuser.getCurrentBranch()
		Role	 		= str(UserObj.Role)

		
		AccObj = MKT_ACCOUNT.query.order_by(MKT_ACCOUNT.ID.asc())
	DicObj={
			'AccObj':AccObj,
			'float':float,
			'toMoney':mktmoney.toMoney,
			'CurrencyObj':mktmoney.getCurrencyObj}
	return DicObj

def getWidgetBranchProductivity():
	UserObj = mktuser.getUser()
	DicObj 	= {}
	NumberOfCustomer = 0
	NumberOfLoan 	= 0
	TotalPortfolio 	= []
	DicTotalPortfolio = {'Currency':'','Amount':0}
	ListTotalPortfolio = []
	if UserObj:
		AccessBranch = str(UserObj.AccessBranch).split()
		AccessBranch = filter(None,AccessBranch)
		if 'ALL' in AccessBranch:
			QueryObj = MKT_REPORT_PRODUCTIVITY.query.get('ALL')
			if QueryObj:
				NumberOfCustomer 	= mktmoney.formatNumber(float(QueryObj.NumberOfCustomer),1,0)
				NumberOfLoan 		= mktmoney.formatNumber(float(QueryObj.NumberOfLoan),1,0)
				TotalPortfolio		= eval(QueryObj.TotalPortfolio)
		else:
			for row in AccessBranch:
				QueryObj = MKT_REPORT_PRODUCTIVITY.query.get(row)
				if QueryObj:
					NumberOfCustomer 	+= float(QueryObj.NumberOfCustomer)
					NumberOfLoan 		+= float(QueryObj.NumberOfLoan)
					TotalPortfolio		= eval(QueryObj.TotalPortfolio)
					for item in TotalPortfolio:
						if 'Currency' in item:
							ListTotalPortfolio.append({item['Currency']:item['Amount']})
							
			NumberOfCustomer 	= mktmoney.formatNumber(float(NumberOfCustomer),1,0)
			NumberOfLoan 		= mktmoney.formatNumber(float(NumberOfLoan),1,0)
			from collections import defaultdict
			SumTotal = defaultdict(float)
			for item in ListTotalPortfolio:
				for k,v in item.iteritems():
					SumTotal[k] += v

			TotalPortfolio = []
			for key,value in SumTotal.iteritems():
				TotalPortfolio.append({'Currency':key,'Amount':value})

		DicObj={
		'float':float,
		'toMoney':mktmoney.toMoney,
		'CurrencyObj':mktmoney.getCurrencyObj,
		'NumberOfCustomer':NumberOfCustomer,
		'NumberOfLoan':NumberOfLoan,
		'TotalPortfolio':TotalPortfolio
				}

	return DicObj

def updateBranchProductivity():
	try:
		DicObj 	= []
		NumberOfCustomer 	= 0
		NumberOfLoan 		= 0
		AmountPortfolio 	= 0
		TotalPortfolio 		= []
		UserObj = mktuser.getUser()
		if UserObj:
			AccessBranch = str(UserObj.AccessBranch).split()
			if 'ALL' in AccessBranch:
				ID="ALL"
				CustomerObj 	= db.session.query(func.count(MKT_CUSTOMER.ID).label('Customer'))

				CountLoanObj	= db.session.query(func.count(MKT_LOAN_CONTRACT.ID).label('Loan')).\
											filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
											filter(MKT_LOAN_CONTRACT.DisbursedStat=="Y")

				SumLoanObj 		= db.session.query(MKT_LOAN_CONTRACT.Currency,func.sum(MKT_LOAN_CONTRACT.OutstandingAmount).label('Amount')).\
											filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
											filter(MKT_LOAN_CONTRACT.DisbursedStat=="Y").\
											group_by(MKT_LOAN_CONTRACT.Currency)
				setBranchProductivity(CustomerObj,CountLoanObj,SumLoanObj,ID)
			else:
				for row in AccessBranch:

					CustomerObj 	= db.session.query(func.count(MKT_CUSTOMER.ID).label('Customer')).\
												filter(MKT_CUSTOMER.Branch==row)

					CountLoanObj	= db.session.query(func.count(MKT_LOAN_CONTRACT.ID).label('Loan')).\
												filter(MKT_LOAN_CONTRACT.Branch==row).\
												filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
												filter(MKT_LOAN_CONTRACT.DisbursedStat=="Y")

					SumLoanObj 		= db.session.query(MKT_LOAN_CONTRACT.Currency,func.sum(MKT_LOAN_CONTRACT.OutstandingAmount).label('Amount')).\
												filter(MKT_LOAN_CONTRACT.OutstandingAmount > 0).\
												filter(MKT_LOAN_CONTRACT.DisbursedStat=="Y").\
												filter(MKT_LOAN_CONTRACT.Branch==row).\
												group_by(MKT_LOAN_CONTRACT.Currency)
					#update to table
					setBranchProductivity(CustomerObj,CountLoanObj,SumLoanObj,row)
			db.session.commit()
		return True
	except Exception, e:
		print "Error Function updateBranchProductivity :%s"%e
		db.session.rollback()
		return False
		
def setBranchProductivity(CustomerObj,CountLoanObj,SumLoanObj,ID):

	NumberOfCustomer = 0
	NumberOfLoan = 0
	TotalPortfolio = []
	for row in CustomerObj:
		NumberOfCustomer+=int(row.Customer)

	for row in CountLoanObj:
		NumberOfLoan+=int(row.Loan)
	
	for row in SumLoanObj:
		DicTotalPortfolio = {'Currency':row.Currency,'Amount':float(row.Amount)}
		TotalPortfolio.append(DicTotalPortfolio)

	ProductObj = MKT_REPORT_PRODUCTIVITY.query.get(ID)
	Audit 		= mktaudit.getAuditrail()
	DicRecord = {
			'ID':ID,
			'NumberOfCustomer':NumberOfCustomer,
			'NumberOfLoan':NumberOfLoan,
			'TotalPortfolio':'%s'%TotalPortfolio
		}
	DicRecord.update(Audit)
	if ProductObj:			
		Condition = [MKT_REPORT_PRODUCTIVITY.ID==ID]
		mktdb.updateRecord(MKT_REPORT_PRODUCTIVITY,Condition,DicRecord)
	else:
		mktdb.insertTable(MKT_REPORT_PRODUCTIVITY,DicRecord)

def getCourseByStudent():
	CourseList 		=		[{"Course":"Math"		, "Number":10},
							 {"Course":"English"    , "Number":10},
							 {"Course":"Physic"     , "Number":30}]
	return CourseList
