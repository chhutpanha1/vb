from app.mktcore.wtfimports 		import *
import mktaccount 					as mktaccount
import catetool 					as mktcategory
import app.tools.mktaddress 		as mktaddress
import app.tools.mktbranch 			as mktbranch
import app.tools.mktofficer 		as mktofficer
import app.tools.mktcustomer 		as mktcustomer

@app.route("/Morakot/TDrAccount", methods=['GET'])	
def TDrAccount():

	result=''
	result = mktaccount.getSearchTillAccount()
	
	return result

@app.route("/Morakot/TCrAccount", methods=['GET'])
def TCrAccount():
	
	result=''
	result = mktaccount.getSearchTillAccount()
		
	return result

@app.route("/Morakot/Categories", methods=['GET'])
def Categories():
	result = ''
	result = mktcategory.getCategoryAsSearch()

	return result

@app.route("/Morakot/IntReceivableCate", methods=['GET'])
def IntReceivableCate():
	result = ''
	result = mktcategory.getCategoryAsSearch()

	return result

@app.route("/Morakot/IntIncomeCate", methods=['GET'])
def IntIncomeCate():
	result = ''
	result = mktcategory.getCategoryAsSearch()

	return result

@app.route("/Morakot/ChargeCat", methods=['GET'])
def ChargeCat():
	result = ''
	result = mktcategory.getCategoryAsSearch()

	return result

@app.route("/Morakot/VBBranch", methods=['GET'])
def VBBranch():
	return mktbranch.getSearchBranch()

@app.route('/Morakot/DistrictID',methods=['GET'])
def getDistrictID():
	Province = request.args.get('ProvinceID')

	return mktaddress.getDistrict(Province)

@app.route('/Morakot/CommuneID',methods=['GET'])
def getCommuneID():
	District = request.args.get('DistrictID')

	return mktaddress.getCommune(District)

@app.route('/Morakot/VillageID',methods=['GET'])
def getVillageID():
	Commune = request.args.get('CommuneID')

	return mktaddress.getVillage(Commune)

@app.route('/Morakot/loadOfficerByBranch',methods=['GET'])
def getOfficer():
	BranchID = request.args.get('BranchID')

	return mktofficer.getOfficerByBranch(BranchID)

@app.route('/Morakot/loadIndustryBySector',methods=['GET'])
def getIndustry():
	SectorID = request.args.get('SectorID')
	
	return mktaddress.getIndustryBySector(SectorID)

@app.route("/Morakot/CustomerID", methods=['GET'])
def CustomerID():
	return mktcustomer.getSearchCustomer()

@app.route("/Morakot/Guarantor", methods=['GET'])
def Guarantor():
	return mktcustomer.getSearchCustomer("Y")

@app.route("/Morakot/AccountID", methods=['GET'])
def AccountID():
	return mktaccount.getSearchAccount()

@app.route("/Morakot/CustomerList", methods=['GET'])
def CustomerList():
	return getSearchCustomer()

@app.route("/Morakot/UserID", methods=['GET'])
def UserID():
	return getSearchUser()

@app.route("/Morakot/OfficerID", methods=['GET'])
def OfficerID():
	return getSearchOfficer()

@app.route("/Morakot/AccountClosingDate", methods=['GET'])
def getAccountClosingDate():
	AccountStatus 	= request.args.get("AccStatus")
	ClosingDate 	= mktdate.getBankDate()
	if AccountStatus.upper() != "C":
		ClosingDate = ""

	return jsonify(ClosingDate=str(ClosingDate))

@app.route("/Morakot/BranchID", methods=['GET'])
def getBranchID():
	ID = request.args.get('UserID')
	Branch = ""
	query = MKT_USER.query.get(ID)
	if query:
		Branch = query.Branch

	return jsonify(Branch=Branch)

@app.route('/Morakot/JoinID', methods=['GET'])
def getJAccount():
	dic = False
	Choices = request.args.get('JAccountID')
	if Choices == "Y":
		dic = True
	else:
		dic = False

	return jsonify(Bool=dic)

@app.route('/Morakot/AccProductID', methods=['GET'])
def getAccProductCategory():

	Value 			= 	request.args.get('AccProductID')
	Currency 		= 	request.args.get("Currency")
	cate 			= 	""
	Rate 			= 	"0"
	ChargeRate 		= 	"0"
	FixedRate 		= 	"F"
	AccProCategory 	=	""
	InterestKey 	=	""

	if Value != "__None":
		row 		= MKT_ACC_PRODUCT.query.get(str(Value))
		cate 		= row.CategoryList
		InterestKey = row.InterestKey
		Charge 		= row.Charge

		if InterestKey:
			ID 		= str(InterestKey) + str(Currency)
			rate 	= MKT_INTEREST_RATE.query.get(ID)
			if rate:
				# inter 	= str(rate.Rate)
				Rate 	= 	rate.Rate
				Rate 	=	Rate.split()
				if len(Rate) > 0:
					Rate 	= 	Rate[0]
				else:
					Rate 	=	'0'

		if Charge:
			ID = str(Charge) + str(Currency)
			cha = MKT_CHARGE_RATE.query.get(ID)
			if cha:
				ChargeRate = str(cha.Value)
				FixedRate = str(cha.RateFixed)
		# return jsonify(AccProCategory=cate, InterestKey=inter, ChargeRate=ChargeRate, FixedRate=FixedRate)
	
	return jsonify(AccProCategory=cate, InterestKey=Rate, ChargeRate=ChargeRate, FixedRate=FixedRate)

@app.route('/Morakot/CustomerNameBySelected')
def getCustomerNameBySelected():
	try:

		CustomerID 		= 	request.args.get('CustomerList')
		Customer 		= 	MKT_CUSTOMER.query.get(CustomerID)
		CustomerName 	=	""
		if Customer:
			CustomerName = Customer.LastNameEn + " " + Customer.FirstNameEn

		return jsonify(CustomerName=CustomerName)
		
	except:
		raise

@app.route('/Morakot/CalculateInstallment')
def getCalculateInstallment():
	try:

		Term 			= 	request.args.get('Term')
		
		if 'AppDate' in request.args:
			AppDate 		= 	request.args.get('AppDate')
		else:
			AppDate 		= 	request.args.get('ValueDate')

		FreqType 		= 	request.args.get('FreqType')
		Frequency 		= 	request.args.get('Frequency')
		
		# Installment 	=	"0"

		Installment 	= 	mktloanapp.getNumberOfInstallment(Term, Frequency, FreqType, AppDate)

		return jsonify(Installment=Installment)
		
	except:
		raise

@app.route("/Morakot/ContractCustomerInfo", methods=['GET'])
def getContractCustomerInfo():
	try:
		CustomerID 	= request.args.get('CustomerID')
		dic 		= {}
		query 		= MKT_ACCOUNT.query.\
					  filter(MKT_ACCOUNT.CustomerList == CustomerID).\
					  filter(MKT_ACCOUNT.AccStatus == "O").\
					  all()
		if query:
			for row in query:
				# dic[row.ID] = row.ID + "  - " + row.Currency+ " " + row.AccName
				dic[row.ID] = row.ID

		return jsonify(results=dic)
	except:
		raise

# @app.route('/Morakot/CheckBlockReason')
# def getCheckBlockReason():
# 	try:

# 		Blocked 		= 	request.args.get('Blocked')
# 		BlockReason 	=	""
# 		if Blocked != "N":
# 			BlockReason = "display: None;"
# 		else:
# 			BlockReason = ""

# 		return jsonify(BlockReason=BlockReason)
		
# 	except:
# 		raise

@app.route("/Morakot/CollateralByClient", methods=["GET"])
def loadCollateralByClient():
	Customer = request.args.get("CustomerID") if "CustomerID" in request.args else ""
	return getLoadCollateral(Customer)

def getLoadCollateral(CustomerID):
	CustomerObj = 	MKT_COLLATERAL.query.\
					filter(MKT_COLLATERAL.CustomerID == CustomerID).\
					all()
	dic = {}
	dic["__None"] = "--None--"
	if CustomerObj:
		for row in  CustomerObj:
			dic[row.ID] = row.ID + "-" + row.Description

	return jsonify(results=dic)

@app.route("/Morakot/CustomerFullNameByID", methods=["GET"])
def getCustomerFullNameByID():
	try:
		
		CustomerName 	= 	""
		Customer 		= 	request.args.get("CoBorrowerID") if "CoBorrowerID" in request.args else ""
		Obj 			=	MKT_CUSTOMER.query.get(Customer)
		if Customer:
			if Obj:
				CustomerName 	=	Obj.LastNameEn + " " + Obj.FirstNameEn
			else:
				Customer 		=	"Customer %s not found." %Customer

		return jsonify(CustomerName=CustomerName, Customer=Customer)

	except Exception, e:
		return "%s" %e