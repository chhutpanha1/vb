from app.mktcore.wtfimports 			import *
from .. 								import app, db
from decimal 							import *

import app.tools.mktdate 				as mktdate
import app.tools.user 					as mktuser
import app.tools.mktaccounting 			as mktaccounting

from app.Branch.models 					import MKT_BRANCH
from datetime 							import datetime, date, timedelta

def authFundTransfer(ID, Amount, DrAcc, DrCat, DrCur, CrAcc, CrCat, CrCur,
					 Tran, TranDate, Noted, Branch="", Module="", Mode1="", Mode2="",
					 k1="", k2="", k3="", k4="", k5="", k6="", k7="", k8="", k9=""):
	try:

		for i in range(0, 2):
			if i == 0:
				Account 	= DrAcc
				Category 	= DrCat
				Currency 	= DrCur
				DrCr 		= "Dr"
				Mode 		= Mode1
			else:
				Account 	= CrAcc
				Category 	= CrCat
				Currency 	= CrCur
				DrCr 		= "Cr"
				Mode 		= Mode2

			print "%s - %s - %s - %s - %s - %s" %(Account, Category, Currency, DrCr, Mode, Amount)

			DateTimeNow = mktdate.getDateTimeNow()

			GL_KEYS = mktaccounting.getConsolKey(Category, Currency, "", "", k1, k2, k3, k4, k5, k6, k7, k8, k9)
			mktaccounting.postAccounting(
				"AUTH", 				# Status
				"0", 					# Curr
				"System",				# Inputter
				DateTimeNow, 			# Createdon
				"System",				# Authorizer
				DateTimeNow,			# Authorizeon
				"", 					# AEID
				Account,				# Account
				Category,				# Category
				Currency,				# Currency
				DrCr,					# DrCr
				Decimal(Amount), 		# Amount
				"JE",					# Module
				Tran, 					# Transaction
				TranDate, 				# TransactionDate
				ID, 					# Reference
				Noted, 					# Note
				"", 					# JNID
				Branch,					# Branch
				GL_KEYS,				# GL_KEYS
				Mode 					# Mode check to insert Journal for category
			)

		return True

	except:
		db.session.rollback()
		raise
		return False

def checkPreMonthDateTransaction(TranDate):
	try:

		SystemBankDate 	= 	mktdate.getBankDate()
		SystemBankDate 	= 	datetime.strptime(str(SystemBankDate),'%Y-%m-%d').date()
		CheckPrevMonth 	=	mktdate.getNextMonth(SystemBankDate, -1)
		PrevMonth 		=	CheckPrevMonth.month
		PrevYear 		=	CheckPrevMonth.year
		TranDate 		= 	datetime.strptime(str(TranDate),'%Y-%m-%d').date()
		TranMonth 		=	TranDate.month
		TranYear 		=	TranDate.year
		Branch 			= 	mktuser.getBranch(session["ChangeBranch"]).ID
		Msg 			=	""

		if int(PrevMonth) == int(TranMonth) and int(PrevYear) == int(TranYear):

			ObjBranch = MKT_BRANCH.query.get(Branch)

			if ObjBranch:

				ReportLocked = ObjBranch.ReportLocked if ObjBranch.ReportLocked else "N"

				if str(ReportLocked).upper() == 'Y':

					Msg = "Branch %s was locked previous month transaction." %Branch

			else:

				Msg = "Branch %s not found." %Branch

		return Msg

	except:
		raise