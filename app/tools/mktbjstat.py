from app.mktcore.imports 		import *
from .. 						import app
from sqlalchemy 				import *
from datetime 					import datetime, date, timedelta
from app.config 				import *

import math
import mktautoid 				as mktAutoID
import mktdate 					as mktdate
import mktpdcollection 			as mktpdcollection
import mktloan 					as mktloan
import loantools.nonworkingday 	as mktDay
import mktholiday 				as mktHoliday

def getMessageError():
	return "Oop! There is something wrong during EOD. Please check your error log, fix the issue and try again."

def getHolidayList():

	SystemDate 	=	mktdate.getBankDate()
	Holiday 	= 	mktHoliday.getHoliday()
	check 		= 	True
	one_day 	= 	timedelta(days=1) # one day
	StartDate 	= 	datetime.strptime(str(SystemDate),'%Y-%m-%d').date()
	StartDate 	= 	StartDate + one_day
	
	HolidayList	= 	[]

	while check:
		check = mktDay.isNonWorkingDay(StartDate, Holiday)
		if check:
			Item = StartDate
			Item = datetime.strptime(str(Item),'%Y-%m-%d')
			Item = str(Item).split()
			HolidayList.append(str(Item[0]))
			StartDate = StartDate + one_day

	return HolidayList

def setNumOfTransaction(NumOfRecord):
	try:

		Number = float(NumOfRecord) / 1000
		
		if Number < 1:
			return 1
			
		return math.ceil(Number)

	except:
		raise

def getUpdateRecord(NumberOfCompleted, RecordNumber, TotalRecord, NumOfTransaction):
	try:
		
		if int(NumberOfCompleted) == int(NumOfTransaction) or int(RecordNumber) == int(TotalRecord):
			return True

		return False

	except:
		raise

def formatTime(seconds):

	m, s = divmod(seconds, 60)
	h, m = divmod(m, 60)
	
	return "%s:%02d:%02d" % (h, m, s)

def getPercPerTransaction(NumberOfCompleted, TotalRecord):
	try:

		Result = (100 * NumberOfCompleted) / float(TotalRecord)

		return Result

	except:
		raise

def getCountBjStat(JobID, Module, Description, NumberOfRecord, SystemDate):
	try:

		getObj 			=	MKT_BJ_STAT.query.\
							filter(MKT_BJ_STAT.SystemDate == str(SystemDate)).\
							filter(MKT_BJ_STAT.Module == Module).\
							all()

		if not getObj:

			ID  			=	mktAutoID.setAutoID("BJ", 10, "MKT_BJ_STAT")
			Date 			= 	datetime.now()
			# StartTime 		= 	'%s:%s:%s'% (Date.hour, Date.minute, Date.second)
			CalendarDate 	= 	'%s-%s-%s'% (Date.year, Date.month, Date.day)
			StartTime 		=	""
			EndTime 		=	""
			Duration 		=	""

			BjStat 	=	MKT_BJ_STAT(
							ID 				=	ID,
							JobID 			=	JobID,
							Module 			=	Module,
							Description 	=	Description,
							StartTime 		=	StartTime,
							EndTime 		=	EndTime,
							Duration 		=	Duration,
							SystemDate 		=	SystemDate,
							CalendarDate 	=	CalendarDate,
							NumberOfRecord 	=	NumberOfRecord,
							Completed 		=	0,
							NumberOfCompleted = 0,
							Progress 		=	'N'
						)

			db.session.add(BjStat)
			# print "Inserted."

		else:
			# print "Updated."
			getObj.CalendarDate 	=	CalendarDate
			getObj.NumberOfRecord 	=	NumberOfRecord
			getObj.Completed 		=	0
			getObj.NumberOfCompleted=	0
			getObj.Progress 		=	'N'

			db.session.add(getObj)
		
		db.session.commit()

		return True

	except Exception, e:
		db.session.rollback()
		# raise
		return msg_error+"%s"%e

def startRecord():
	try:

		SystemDate 	=	mktdate.getBankDate()
		FirstDate 	=	datetime.strptime(str(SystemDate),'%Y-%m-%d')
		FirstDate 	=	str(FirstDate).split()
		Holiday 	= 	mktHoliday.getHoliday()
		check 		= 	True
		one_day 	= 	timedelta(days=1) # one day
		StartDate 	= 	datetime.strptime(str(SystemDate),'%Y-%m-%d').date()
		StartDate 	= 	StartDate + one_day
		
		HolidayList	= 	[str(FirstDate[0])]

		while check:
			check = mktDay.isNonWorkingDay(StartDate, Holiday)
			if check:
				Item = StartDate
				Item = datetime.strptime(str(Item),'%Y-%m-%d')
				Item = str(Item).split()
				HolidayList.append(str(Item[0]))
				StartDate = StartDate + one_day

		# for item in HolidayList:
		# print item
		# Insert PD BatchJob
		PD_Count = mktpdcollection.setPDCollection("", 1, "1")
		# print PD_Count
		getCountBjStat("001", "PD", "PD Collection Update", PD_Count, str(SystemDate))

		# Insert Loan Collection BatchJob
		LC_Count = mktloan.setLoanCollection("", 1, "1", str(SystemDate))
		# print LC_Count
		getCountBjStat("002", "LC", "Loan Collection Update", LC_Count, str(SystemDate))

		# Insert Loan Classification BatchJob
		LC_Count = mktpdcollection.loanClassification("", 1, "1", str(SystemDate))
		# print LC_Count
		getCountBjStat("002", "CL", "Loan Classification Update", LC_Count, str(SystemDate))

		# Insert Accr Account BatchJob
		AC_Count = mktloan.setAccrAccount("1", str(SystemDate))
		# print AC_Count
		getCountBjStat("003", "AA", "Daily Account Accrual update", AC_Count, str(SystemDate))

		# Insert Accr Loan BatchJob
		LC_Count = mktloan.setAccrLoanContract("1", str(SystemDate))
		# print LC_Count
		getCountBjStat("004", "AL", "Daily Loan Interest Accrual update", LC_Count, str(SystemDate))

		# Insert Loan Write-off BatchJob
		# LC_Count = mktloan.setLoanWriteOff("", 1, "1", item)
		# getCountBjStat("009", "LW", "Loan Write-off", LC_Count, item)

		# Insert Loan Classification BatchJob
		StartDate 	= 	datetime.strptime(str(SystemDate),'%Y-%m-%d').date()
		StartDate 	= 	StartDate + one_day
		LC_Count = mktloan.setLCDisbursement(None, None, 1, "1", StartDate)
		# print LC_Count
		getCountBjStat("007", "LD", "Loan Disbursement", LC_Count, str(SystemDate))


	except Exception, e:
		db.session.rollback()
		raise
		# return msg_error+"%s"%e

def calCompletedPer(Module, NumberOfCompleted, TotalCount, CompletedPerc):
	try:

		SystemDate 		=	mktdate.getBankDate()
		Date 			= 	datetime.now()
		CurrentTime 	= 	'%s:%s:%s'% (Date.hour, Date.minute, Date.second)

		if Module == "LD":
			one_day 	= 	timedelta(days=1) # one day
			StartDate 	= 	datetime.strptime(str(SystemDate),'%Y-%m-%d').date()
			StartDate 	= 	StartDate - one_day

			StartDate 	=	str(StartDate).split()
			SystemDate 	=	StartDate[0]

		BjStat 	=	MKT_BJ_STAT.query.\
					filter(MKT_BJ_STAT.SystemDate == str(SystemDate)).\
					filter(MKT_BJ_STAT.Module == Module).\
					first()

		if BjStat:
			StartLoop 					=	BjStat.NumberOfCompleted
			# CompletedPerc 				=	(100 * NumberOfCompleted) / float(TotalCount)
			BjStat.NumberOfCompleted 	= 	int(BjStat.NumberOfCompleted) + int(NumberOfCompleted)
			BjStat.Completed 			= 	float(CompletedPerc)

			NumberOfCompleted 			=	BjStat.NumberOfCompleted

			if int(StartLoop) == 0:
				BjStat.StartTime 	= 	CurrentTime

			elif int(NumberOfCompleted) == int(TotalCount):
				BjStat.EndTime 	= 	CurrentTime

				t1 = datetime.strptime(str(BjStat.StartTime),'%H:%M:%S')
				t2 = datetime.strptime(str(BjStat.EndTime),'%H:%M:%S')

				delta = (t2 - t1)
				delta = delta.seconds

				BjStat.Duration = 	delta

			BjStat.Progress 	=	'Y'
			
			db.session.add(BjStat)
			db.session.commit()

		# Clear query object
		del BjStat
		
		return True

	except Exception, e:
		makeLogFileOnError('Y')
		db.session.rollback()
		raise

def makeLogFileOnError(Error):
	try:

		dat 		= datetime.now()
		CurrentTime = '%s:%s:%s'% (dat.hour, dat.minute, dat.second)
		currentDate = '%s-%s-%s'% (dat.year, dat.month, dat.day)
		DateTime 	= str(currentDate) + " " + CurrentTime
		Msg = Error + " " + DateTime

		filename 	= EOD_LOG_URL+"EOD/Resume_EOD.log"
		f = open(filename, 'r+')
		f.write(Msg)
		f.close()

	except:
		raise