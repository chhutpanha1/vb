from app.mktcore.wtfimports import *
from decimal import *
from sqlalchemy import or_
from app.Province.models import *
from app.District.models import *
from app.Commune.models import *
from app.Village.models import *
def getProvince():
	return MKT_PROVINCE.query

def getDistrict(Province=None):
	dic = {}
	result = MKT_DISTRICT.query.filter_by(Province=Province).all()
	for row in  result:
		dic[row.ID] = row.Description

	return jsonify(results=dic)

def getCommune(District=None):
	dic = {}
	result = MKT_COMMUNE.query.filter_by(District=District).all()
	for row in  result:
		dic[row.ID] = row.Description

	return jsonify(results=dic)

def getVillage(Commune=None):
	dic = {}
	result = MKT_VILLAGE.query.filter_by(Commune=Commune).all()
	for row in  result:
		dic[row.ID] = row.Description

	return jsonify(results=dic)

def getIndustryBySector(SectorID=''):
	dic = {}
	result = MKT_INDUSTRY.query.filter_by(Sector = SectorID).all()
	for row in  result:
		dic[row.ID] = row.Description

	return jsonify(results=dic)


# get to load QuerySelectField
def getLoadDistrict(ProvinceID=None):
	return MKT_DISTRICT.query.filter_by(Province=ProvinceID) # tablename.query

def getLoadCommune(DistrictID=None):
	return MKT_COMMUNE.query.filter_by(District=DistrictID) # tablename.query

def getLoadVillage(CommuneID=None):
	return MKT_VILLAGE.query.filter_by(Commune=CommuneID) # tablename.query

def getAddress(ProvinceID='',DistrictID='',CommuneID='',VillageID='',Locale='en'):
	try:
		# Set Default 
		Address 	= ""
		Province 	= ""
		District 	= ""
		Commune 	= ""
		Village 	= ""
		
		ProvinceObj = MKT_PROVINCE.query.get(ProvinceID if ProvinceID else '')
		DistrictObj = MKT_DISTRICT.query.get(DistrictID if DistrictID else '')
		CommuneObj 	= MKT_COMMUNE.query.get(CommuneID if CommuneID else '')
		VillageObj 	= MKT_VILLAGE.query.get(VillageID if VillageID else '')

		if hasattr(MKT_PROVINCE, 'LocalDescription') and Locale.upper() != 'EN':
			Field = 'LocalDescription'
		else:
			Field = 'Description'

		if ProvinceObj:
			Province = getattr(ProvinceObj,Field)

		if DistrictObj:
			District = getattr(DistrictObj,Field)

		if CommuneObj:
			Commune = getattr(CommuneObj,Field)

		if VillageObj:
			Village = getattr(VillageObj,Field)

		ListAddress = [Village , Commune , District , Province ]
		
		# Remove none in the list
		ListAddress = filter(None,ListAddress)
		Address = ", ".join(ListAddress)

		return Address

	except Exception, e:
		raise