'''
Created Date: 01 Mar 2016
Author: Sovankiry Rim

Modified by: 
Modified Date: 
All Right Reserved Morakot Technology
Description : mktapi refer to api of system morakot 
Notes :


# Call lookup value by ID
Obj = getRecord('MKT_USER',Get='001')

Result :
{'Authorizer': u'ADMIN.S', 'DisplayName': u'Lim Sombo','Language': u'KH', 
'DateLastLogin': u'2015-12-21', 'ExpiryDate': u'2020-03-31', 
'TimeLastLogin': u'5:51:26',  'Email': u'', ....}

Obj['Authorizer']
Obj.Authorizer

# Call one record with condition
getRecord('MKT_USER',OptCondtion=[mktapi.MKT_USER.Branch=='HO',mktapi.MKT_USER.Male=='Male'],Get='First')
Result :
{'Authorizer': u'ADMIN.S', 'DisplayName': u'Lim Sombo','Language': u'KH', 
'DateLastLogin': u'2015-12-21', 'ExpiryDate': u'2020-03-31', 
'TimeLastLogin': u'5:51:26',  'Email': u'', ....} 

# Call hold the table
getRecord('MKT_USER')
Result :
[{'Authorizer': u'ADMIN.S', 'DisplayName': u'Lim Sombo','Language': u'KH', 
'DateLastLogin': u'2015-12-21', 'ExpiryDate': u'2020-03-31', 
'TimeLastLogin': u'5:51:26',  'Email': u'', ....},
{'Authorizer': u'ADMIN.S', 'DisplayName': u'Lim Sombo','Language': u'KH', 
'DateLastLogin': u'2015-12-21', 'ExpiryDate': u'2020-03-31', 
'TimeLastLogin': u'5:51:26',  'Email': u'', ....}]


# Call record by field with hold table
getRecord('MKT_PROVINCE',OptField=['ID','Description'])
Result :
[{'ID':u'001','Description':'Phnom Penh'},{'ID':u'002','Description':'Takeo'}....]


'''
from sqlalchemy import create_engine,inspect

def getRecord(TableName,**kwargs):

	# ListTable 			= []
	# engine 		= create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
	# inspector 	= inspect(engine)
	# for table_name in inspector.get_table_names():
	# 	ListTable.append(str(table_name))
	RowRecord = []
	ColRecord = {}
	Module 			= __import__("app.urlregister")
	TableObj  		= getattr(Module,TableName)
	Mapper 			= inspect(TableObj)
	ListColumn 		= []
	for item in Mapper.attrs:
		ListColumn.append(item.key)

	if 'ReturnType' in kwargs:
		ReturnType = kwargs['ReturnType']
	else:
		ReturnType = 'JSON'

	if 'OptField' in kwargs:
		OptField = kwargs['OptField']
	else:
		OptField = []

	if 'OptCondition' in kwargs:
		OptCondition = kwargs['OptCondition']
	else:
		OptCondition = []

	if OptCondition:
		QueryObj = TableObj.query.filter(*OptCondition)
	else:
		QueryObj = TableObj.query

	if 'Get' in kwargs:
		Get = kwargs['Get'] if kwargs['Get'] else ''
		
		if Get == "First":
			QueryObj = QueryObj.first()
			for col in ListColumn:
				ColRecord.update({col:getattr(QueryObj,col) if QueryObj else ''})
			return ColRecord
		else:
			if not OptCondition:
				QueryObj = QueryObj.get(Get)
			for col in ListColumn:
				ColRecord.update({col:getattr(QueryObj,col) if QueryObj else ''})
			return ColRecord
	
	if ReturnType == "Object":
		return QueryObj
	else:
		for row in QueryObj:
			for col in ListColumn:
				if OptField:
					if col in OptField:
						ColRecord.update({col:getattr(row,col)})
				else:
					ColRecord.update({col:getattr(row,col)})
			RowRecord.append(ColRecord)
			ColRecord = {}
		return RowRecord
