# Application Secret Key
SECRET_KEY 						= 	'F12Zr47j\3yX R~X@H!jmM]Lwf/,?KT_MKT'

# Current Path VB
VB_PATH 						=	"/home/dev/Documents/Working/Morakot/vb/"

# Profile Upload Setting
UPLOAD_FOLDER       			= 	VB_PATH + 'app/static/avatars/'
ALLOWED_EXTENSIONS 				= 	set(['png', 'jpg', 'jpeg', 'gif'])

# Attachment Configuration
ATTACHMENT_FOLDER       		= 	VB_PATH + 'app/static/attachment/'
ALLOWED_ATTACHMENT 				= 	set(['png', 'jpg', 'jpeg', 'gif', 'pdf'])

MAX_CONTENT_LENGTH 				= 	1 * 600 * 600

#EOD Log file path
EOD_LOG_URL       				= 	VB_PATH + 'log/'

# IP Address Server Patch Updating
PATCH_ADDRESS 					=	"128.199.113.111 morakot Morakot*sftp"

# Database Connection String
CONNECTION_STRING 				=	"postgresql://postgres:123@localhost:5432/morakot"	