# This will create the following models into the database

print "[--VB--]: Importing Models"
from app.Branch.models import *

print "[--VB--]: Add to the database"
print "[--VB--]: Please wait for a few minutes..."
db.create_all()
print "[--VB--]: Models are successfully created in your database. Thanks You."